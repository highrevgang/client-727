/* Class521_Sub1_Sub1_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class521_Sub1_Sub1_Sub5 extends Class521_Sub1_Sub1 implements Interface12 {
	public Class123 aClass123_10509;
	Class200 aClass200_10510;
	boolean aBool10511;
	boolean aBool10512 = true;

	public int method76() {
		return ((Class123) aClass123_10509).anInt1540 * 1535779425;
	}

	boolean method12986(int i) {
		return false;
	}

	void method13023(Class505 class505) {
		Class528 class528 = aClass123_10509.method2132(class505, 262144, true, true, -1384937123);
		if (class528 != null)
			aClass123_10509.method2119(class505, class528, method11168(), aShort9458, aShort9455, aShort9456, aShort9457, false, 1109367296);
	}

	public int method12995(int i) {
		return aClass123_10509.method2115(1252566568);
	}

	public Class200 method12992(Class505 class505, byte i) {
		return ((Class521_Sub1_Sub1_Sub5) this).aClass200_10510;
	}

	public boolean method94() {
		return aClass123_10509.method2117(1835275443);
	}

	Class285 method12990(Class505 class505, int i) {
		Class528 class528 = aClass123_10509.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub1_Sub5) this).aBool10511, (byte) -97);
		aClass123_10509.method2119(class505, class528, class294, aShort9458, aShort9455, aShort9456, aShort9457, true, -219339696);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10509).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10509).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub1_Sub5) this).aBool10512 = (class528.i() || ((Class123) aClass123_10509).aClass539_1538 != null);
		Class305 class305 = method11166();
		if (((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 == null)
			((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) -19);
		else
			Class388.method6694((((Class521_Sub1_Sub1_Sub5) this).aClass200_10510), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, 464369048);
		return class285;
	}

	void method12991(Class505 class505, int i) {
		Class528 class528 = aClass123_10509.method2132(class505, 262144, true, true, -1384937123);
		if (class528 != null)
			aClass123_10509.method2119(class505, class528, method11168(), aShort9458, aShort9455, aShort9456, aShort9457, false, 985528873);
	}

	public void method16096(Class476 class476, int i) {
		aClass123_10509.method2116(class476, 501867772);
	}

	boolean method12983(Class505 class505, int i, int i_0_, int i_1_) {
		Class528 class528 = aClass123_10509.method2132(class505, 131072, false, false, -1384937123);
		if (class528 == null)
			return false;
		return class528.method11270(i, i_0_, method11168(), false, 0);
	}

	final boolean method12985(int i) {
		return false;
	}

	boolean method13001() {
		return ((Class521_Sub1_Sub1_Sub5) this).aBool10512;
	}

	final void method12984(int i) {
		throw new IllegalStateException();
	}

	public void method97(Class505 class505) {
		aClass123_10509.method2118(class505, 1610737913);
	}

	public int method89(int i) {
		return 845010167 * ((Class123) aClass123_10509).anInt1555;
	}

	public int method92(int i) {
		return ((Class123) aClass123_10509).anInt1542 * -515017769;
	}

	public void method85(byte i) {
		/* empty */
	}

	public boolean method86(int i) {
		return aClass123_10509.method2117(-566540955);
	}

	public void method87(Class505 class505, int i) {
		aClass123_10509.method2136(class505, (byte) -107);
	}

	public void method88(Class505 class505, int i) {
		aClass123_10509.method2118(class505, 1860248692);
	}

	public int method84(int i) {
		return ((Class123) aClass123_10509).anInt1540 * 1535779425;
	}

	public int method39() {
		return ((Class123) aClass123_10509).anInt1540 * 1535779425;
	}

	boolean method12987(int i) {
		return ((Class521_Sub1_Sub1_Sub5) this).aBool10512;
	}

	public Class521_Sub1_Sub1_Sub5(Class206 class206, Class505 class505, Class474 class474, Class478 class478, int i, int i_2_, int i_3_, int i_4_, int i_5_, boolean bool, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_) {
		super(class206, i, i_2_, i_3_, i_4_, i_5_, i_6_, i_7_, i_8_, i_9_, -1062790731 * class478.anInt5687 == 1, Class461.method7705(i_10_, i_11_, (byte) -22));
		aClass123_10509 = new Class123(class505, class474, class478, i_10_, i_11_, aByte7967, i_2_, this, bool, i_12_);
		((Class521_Sub1_Sub1_Sub5) this).aBool10511 = 0 != class478.anInt5652 * -348507379 && !bool;
		method13008(1, (byte) -22);
	}

	public int method91() {
		return ((Class123) aClass123_10509).anInt1542 * -515017769;
	}

	public void method38() {
		/* empty */
	}

	public void method16097(Class476 class476) {
		aClass123_10509.method2116(class476, -281652761);
	}

	public void method90() {
		/* empty */
	}

	public void method98(Class505 class505) {
		aClass123_10509.method2118(class505, 1873794569);
	}

	public boolean method95() {
		return aClass123_10509.method2117(-102116081);
	}

	public void method93(Class505 class505) {
		aClass123_10509.method2136(class505, (byte) -105);
	}

	public void method83(Class505 class505) {
		aClass123_10509.method2136(class505, (byte) -44);
	}

	Class285 method13010(Class505 class505) {
		Class528 class528 = aClass123_10509.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub1_Sub5) this).aBool10511, (byte) -2);
		aClass123_10509.method2119(class505, class528, class294, aShort9458, aShort9455, aShort9456, aShort9457, true, 915832914);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10509).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10509).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub1_Sub5) this).aBool10512 = (class528.i() || ((Class123) aClass123_10509).aClass539_1538 != null);
		Class305 class305 = method11166();
		if (((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 == null)
			((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) 1);
		else
			Class388.method6694((((Class521_Sub1_Sub1_Sub5) this).aClass200_10510), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -1316927176);
		return class285;
	}

	boolean method12999() {
		return false;
	}

	public int method77() {
		return 845010167 * ((Class123) aClass123_10509).anInt1555;
	}

	boolean method13000() {
		return ((Class521_Sub1_Sub1_Sub5) this).aBool10512;
	}

	final void method13013(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_13_, int i_14_, boolean bool, int i_15_) {
		throw new IllegalStateException();
	}

	boolean method13002() {
		return ((Class521_Sub1_Sub1_Sub5) this).aBool10512;
	}

	static byte method16098(int i, int i_16_) {
		if (i != Class458.aClass458_5474.anInt5481 * 1109376893)
			return (byte) 0;
		if ((i_16_ & 0x1) == 0)
			return (byte) 1;
		return (byte) 2;
	}

	public int method13003() {
		return aClass123_10509.method2115(-1854230374);
	}

	public int method13017() {
		return aClass123_10509.method2115(469812250);
	}

	public int method13005() {
		return aClass123_10509.method2115(1237587241);
	}

	public int method13006() {
		return aClass123_10509.method2115(138212061);
	}

	public int method13007() {
		return aClass123_10509.method2121((byte) 1);
	}

	public int method13028() {
		return aClass123_10509.method2121((byte) 1);
	}

	Class285 method13009(Class505 class505) {
		Class528 class528 = aClass123_10509.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub1_Sub5) this).aBool10511, (byte) -19);
		aClass123_10509.method2119(class505, class528, class294, aShort9458, aShort9455, aShort9456, aShort9457, true, 1469676287);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10509).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10509).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub1_Sub5) this).aBool10512 = (class528.i() || ((Class123) aClass123_10509).aClass539_1538 != null);
		Class305 class305 = method11166();
		if (((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 == null)
			((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) 4);
		else
			Class388.method6694((((Class521_Sub1_Sub1_Sub5) this).aClass200_10510), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, 1083467352);
		return class285;
	}

	public void method96() {
		/* empty */
	}

	Class285 method12989(Class505 class505) {
		Class528 class528 = aClass123_10509.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub1_Sub5) this).aBool10511, (byte) -5);
		aClass123_10509.method2119(class505, class528, class294, aShort9458, aShort9455, aShort9456, aShort9457, true, 1385050972);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10509).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10509).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub1_Sub5) this).aBool10512 = (class528.i() || ((Class123) aClass123_10509).aClass539_1538 != null);
		Class305 class305 = method11166();
		if (((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 == null)
			((Class521_Sub1_Sub1_Sub5) this).aClass200_10510 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) -21);
		else
			Class388.method6694((((Class521_Sub1_Sub1_Sub5) this).aClass200_10510), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -640718163);
		return class285;
	}

	void method13012(Class505 class505) {
		Class528 class528 = aClass123_10509.method2132(class505, 262144, true, true, -1384937123);
		if (class528 != null)
			aClass123_10509.method2119(class505, class528, method11168(), aShort9458, aShort9455, aShort9456, aShort9457, false, 1583997112);
	}

	public int method73() {
		return 845010167 * ((Class123) aClass123_10509).anInt1555;
	}

	public Class200 method13018(Class505 class505) {
		return ((Class521_Sub1_Sub1_Sub5) this).aClass200_10510;
	}

	final boolean method13026() {
		return false;
	}

	final boolean method13011() {
		return false;
	}

	final void method13016(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_17_, int i_18_, boolean bool) {
		throw new IllegalStateException();
	}

	public Class200 method13019(Class505 class505) {
		return ((Class521_Sub1_Sub1_Sub5) this).aClass200_10510;
	}

	final void method13021() {
		throw new IllegalStateException();
	}

	public Class200 method12993(Class505 class505) {
		return ((Class521_Sub1_Sub1_Sub5) this).aClass200_10510;
	}

	boolean method13020(Class505 class505, int i, int i_19_) {
		Class528 class528 = aClass123_10509.method2132(class505, 131072, false, false, -1384937123);
		if (class528 == null)
			return false;
		return class528.method11270(i, i_19_, method11168(), false, 0);
	}

	public int method12997(int i) {
		return aClass123_10509.method2121((byte) 1);
	}

	final void method13015() {
		throw new IllegalStateException();
	}

	public static void method16099(Class118[] class118s, int i, int i_20_, int i_21_, boolean bool, byte i_22_) {
		for (int i_23_ = 0; i_23_ < class118s.length; i_23_++) {
			Class118 class118 = class118s[i_23_];
			if (class118 != null && 2110532063 * class118.anInt1305 == i) {
				Class484.method8200(class118, i_20_, i_21_, bool, -417515150);
				Class246.method4204(class118, i_20_, i_21_, -878856707);
				if (class118.anInt1311 * 276864765 > (-354780671 * class118.anInt1376 - 1506818197 * class118.anInt1301))
					class118.anInt1311 = (class118.anInt1376 * -2044497835 - -833007751 * class118.anInt1301);
				if (276864765 * class118.anInt1311 < 0)
					class118.anInt1311 = 0;
				if (class118.anInt1312 * 682782159 > (class118.anInt1314 * -37350919 - class118.anInt1429 * -492594917))
					class118.anInt1312 = (class118.anInt1314 * 1980562871 - -1716920587 * class118.anInt1429);
				if (682782159 * class118.anInt1312 < 0)
					class118.anInt1312 = 0;
				if (class118.anInt1268 * -2131393857 == 0)
					Class480.method8044(class118s, class118, bool, (byte) -82);
			}
		}
	}

	static final void method16100(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_24_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		int i_25_ = (((Class527) class527).anIntArray6999[1 + 1942118537 * ((Class527) class527).anInt7012]);
		Class437 class437 = Class125.aClass424_1573.method7069(i_25_, (byte) 0);
		if (class437.method7319(1555289189))
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = Class350_Sub1.aClass406_7757.method6828(i_24_, (byte) -99).method6882(i_25_, class437.aString5335, -1939123518);
		else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = (Class350_Sub1.aClass406_7757.method6828(i_24_, (byte) 20).method6876(i_25_, class437.anInt5337 * -1741480635, 1365877231));
	}
}
