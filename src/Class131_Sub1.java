
/* Class131_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jaclib.memory.Buffer;

public class Class131_Sub1 extends Class131 implements Interface14 {
	int anInt9016;

	public int method3() {
		return 0;
	}

	public void method107(int i, byte[] is, int i_0_) {
		method2319(is, i_0_);
		((Class131_Sub1) this).anInt9016 = i;
	}

	public int method1() {
		return 0;
	}

	Class131_Sub1(Class505_Sub1 class505_sub1, int i, byte[] is, int i_1_) {
		super(class505_sub1, is, i_1_);
		((Class131_Sub1) this).anInt9016 = i;
	}

	public long method109() {
		return ((Class131_Sub1) this).aBuffer1593.method2();
	}

	Class131_Sub1(Class505_Sub1 class505_sub1, int i, Buffer buffer) {
		super(class505_sub1, buffer);
		((Class131_Sub1) this).anInt9016 = i;
	}

	public void method106(int i, byte[] is, int i_2_) {
		method2319(is, i_2_);
		((Class131_Sub1) this).anInt9016 = i;
	}

	public int method4() {
		return 0;
	}

	public int method75() {
		return ((Class131_Sub1) this).anInt9016;
	}

	public long method6() {
		return ((Class131_Sub1) this).aBuffer1593.method2();
	}

	public int method59() {
		return ((Class131_Sub1) this).anInt9016;
	}

	public void method108(int i, byte[] is, int i_3_) {
		method2319(is, i_3_);
		((Class131_Sub1) this).anInt9016 = i;
	}
}
