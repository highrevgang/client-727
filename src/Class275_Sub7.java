/* Class275_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class275_Sub7 extends Class275 {
	Class457 aClass457_7861 = new Class457();
	static Class400 aClass400_7862;
	public Class198 aClass198_7863;

	public void method12599(int i, int i_0_, int i_1_, int i_2_) {
		Class275_Sub3 class275_sub3 = null;
		int i_3_ = 0;
		for (Class275_Sub3 class275_sub3_4_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7659(301908602); null != class275_sub3_4_; class275_sub3_4_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 12)) {
			i_3_++;
			if (-145017129 * class275_sub3_4_.anInt7811 == i) {
				class275_sub3_4_.method12566(i, i_0_, i_1_, i_2_, (byte) 70);
				return;
			}
			if (-145017129 * class275_sub3_4_.anInt7811 <= i)
				class275_sub3 = class275_sub3_4_;
		}
		if (null == class275_sub3) {
			if (i_3_ < Class58.aClass529_527.anInt7030 * -73103773)
				((Class275_Sub7) this).aClass457_7861.method7647(new Class275_Sub3(i, i_0_, i_1_, i_2_), 28760954);
		} else {
			Class516.method8866(new Class275_Sub3(i, i_0_, i_1_, i_2_), class275_sub3, 512184385);
			if (i_3_ >= -73103773 * Class58.aClass529_527.anInt7030)
				((Class275_Sub7) this).aClass457_7861.method7659(301908602).method4887((byte) -104);
		}
	}

	public Class275_Sub7(Class198 class198) {
		aClass198_7863 = class198;
	}

	public Class275_Sub3 method12600(int i, short i_5_) {
		Class275_Sub3 class275_sub3 = ((Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7659(301908602));
		if (class275_sub3 == null || -145017129 * class275_sub3.anInt7811 > i)
			return null;
		for (Class275_Sub3 class275_sub3_6_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 12); (class275_sub3_6_ != null && -145017129 * class275_sub3_6_.anInt7811 <= i); class275_sub3_6_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 86)) {
			class275_sub3.method4887((byte) 3);
			class275_sub3 = class275_sub3_6_;
		}
		if ((-145017129 * class275_sub3.anInt7811 + 28139157 * class275_sub3.anInt7812 + aClass198_7863.anInt2443 * -2073838649) > i)
			return class275_sub3;
		class275_sub3.method4887((byte) -120);
		return null;
	}

	public void method12601(int i, int i_7_, int i_8_, int i_9_, int i_10_) {
		Class275_Sub3 class275_sub3 = null;
		int i_11_ = 0;
		for (Class275_Sub3 class275_sub3_12_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7659(301908602); null != class275_sub3_12_; class275_sub3_12_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 78)) {
			i_11_++;
			if (-145017129 * class275_sub3_12_.anInt7811 == i) {
				class275_sub3_12_.method12566(i, i_7_, i_8_, i_9_, (byte) 53);
				return;
			}
			if (-145017129 * class275_sub3_12_.anInt7811 <= i)
				class275_sub3 = class275_sub3_12_;
		}
		if (null == class275_sub3) {
			if (i_11_ < Class58.aClass529_527.anInt7030 * -73103773)
				((Class275_Sub7) this).aClass457_7861.method7647(new Class275_Sub3(i, i_7_, i_8_, i_9_), -2075315301);
		} else {
			Class516.method8866(new Class275_Sub3(i, i_7_, i_8_, i_9_), class275_sub3, -653430478);
			if (i_11_ >= -73103773 * Class58.aClass529_527.anInt7030)
				((Class275_Sub7) this).aClass457_7861.method7659(301908602).method4887((byte) -101);
		}
	}

	public boolean method12602(int i) {
		return ((Class275_Sub7) this).aClass457_7861.method7666(1726407188);
	}

	public Class275_Sub3 method12603(int i) {
		Class275_Sub3 class275_sub3 = ((Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7659(301908602));
		if (class275_sub3 == null || -145017129 * class275_sub3.anInt7811 > i)
			return null;
		for (Class275_Sub3 class275_sub3_13_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 126); (class275_sub3_13_ != null && -145017129 * class275_sub3_13_.anInt7811 <= i); class275_sub3_13_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 89)) {
			class275_sub3.method4887((byte) -121);
			class275_sub3 = class275_sub3_13_;
		}
		if ((-145017129 * class275_sub3.anInt7811 + 28139157 * class275_sub3.anInt7812 + aClass198_7863.anInt2443 * -2073838649) > i)
			return class275_sub3;
		class275_sub3.method4887((byte) -29);
		return null;
	}

	public Class275_Sub3 method12604(int i) {
		Class275_Sub3 class275_sub3 = ((Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7659(301908602));
		if (class275_sub3 == null || -145017129 * class275_sub3.anInt7811 > i)
			return null;
		for (Class275_Sub3 class275_sub3_14_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 46); (class275_sub3_14_ != null && -145017129 * class275_sub3_14_.anInt7811 <= i); class275_sub3_14_ = (Class275_Sub3) ((Class275_Sub7) this).aClass457_7861.method7650((byte) 54)) {
			class275_sub3.method4887((byte) -46);
			class275_sub3 = class275_sub3_14_;
		}
		if ((-145017129 * class275_sub3.anInt7811 + 28139157 * class275_sub3.anInt7812 + aClass198_7863.anInt2443 * -2073838649) > i)
			return class275_sub3;
		class275_sub3.method4887((byte) -39);
		return null;
	}

	public boolean method12605() {
		return ((Class275_Sub7) this).aClass457_7861.method7666(2072708550);
	}

	public static final void method12606(byte i) {
		for (int i_15_ = 0; i_15_ < 5; i_15_++)
			client.aBoolArray7431[i_15_] = false;
		client.anInt7277 = 158788875;
		client.anInt7448 = -1044733407;
		Class473.anInt5606 = 0;
		Class501.anInt5828 = 0;
		Class262.anInt3240 = 385244173;
		Class86.anInt833 = -1509271845;
		Class508.anInt5864 = 987778595;
		client.anInt7429 = -914265549 * client.anInt7174;
		Class186.anInt2349 = 1545481895 * Class31.anInt361;
		Class521_Sub1_Sub2.anInt9461 = Class109_Sub1.anInt9384 * -1358126739;
		Class282_Sub15_Sub1.anInt9575 = 1724048189 * Class246.anInt3029;
		Class336.anInt3968 = Class293.anInt3512 * 39010087;
		Class424.anInt5029 = -427311385 * Class518.anInt5930;
	}
}
