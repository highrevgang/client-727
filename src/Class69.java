/* Class69 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class69 {
	static Class69 aClass69_688;
	public static Class69 aClass69_689;
	public static Class69 aClass69_690;
	public static Class69 aClass69_691;
	public static Class69 aClass69_692;
	public static Class69 aClass69_693;
	public static Class69 aClass69_694;
	public static Class69 aClass69_695 = new Class69(7, 0, 3, Class76.aClass76_758);
	static Class69 aClass69_696;
	static Class69 aClass69_697;
	public int anInt698;
	static Class69 aClass69_699;
	static Class69 aClass69_700;
	static final int anInt701 = 16;
	static int anInt702;
	int anInt703;
	public int anInt704;
	int anInt705;
	Class76 aClass76_706;

	static Class69 method1284(int i) {
		switch (i) {
		case 1:
			return aClass69_689;
		case 0:
			return aClass69_695;
		case 3:
			return aClass69_691;
		case 6:
			return aClass69_694;
		case 4:
			return aClass69_692;
		case 5:
			return aClass69_693;
		case 2:
			return aClass69_690;
		default:
			return null;
		}
	}

	static Class69 method1285(int i) {
		switch (i) {
		case 1:
			return aClass69_689;
		case 0:
			return aClass69_695;
		case 3:
			return aClass69_691;
		case 6:
			return aClass69_694;
		case 4:
			return aClass69_692;
		case 5:
			return aClass69_693;
		case 2:
			return aClass69_690;
		default:
			return null;
		}
	}

	Class69(int i, int i_0_, int i_1_, Class76 class76) {
		anInt698 = i;
		((Class69) this).anInt703 = i_0_;
		((Class69) this).anInt705 = i_1_;
		((Class69) this).aClass76_706 = class76;
		anInt704 = (((Class69) this).anInt705 * (((Class69) this).aClass76_706.anInt748 * 971905895));
		if (((Class69) this).anInt703 >= 16)
			throw new RuntimeException();
	}

	static Class69 method1286(int i) {
		switch (i) {
		case 1:
			return aClass69_689;
		case 0:
			return aClass69_695;
		case 3:
			return aClass69_691;
		case 6:
			return aClass69_694;
		case 4:
			return aClass69_692;
		case 5:
			return aClass69_693;
		case 2:
			return aClass69_690;
		default:
			return null;
		}
	}

	static {
		aClass69_689 = new Class69(0, 1, 3, Class76.aClass76_758);
		aClass69_690 = new Class69(10, 2, 4, Class76.aClass76_751);
		aClass69_691 = new Class69(8, 3, 1, Class76.aClass76_758);
		aClass69_692 = new Class69(4, 4, 2, Class76.aClass76_758);
		aClass69_693 = new Class69(9, 5, 3, Class76.aClass76_758);
		aClass69_694 = new Class69(2, 6, 4, Class76.aClass76_758);
		aClass69_696 = new Class69(1, 7, 4, Class76.aClass76_758);
		aClass69_700 = new Class69(11, 8, 4, Class76.aClass76_758);
		aClass69_699 = new Class69(3, 9, 4, Class76.aClass76_758);
		aClass69_688 = new Class69(5, 10, 3, Class76.aClass76_758);
		aClass69_697 = new Class69(6, 11, 3, Class76.aClass76_758);
		anInt702 = Class159.method2739(16, -561677665);
	}
}
