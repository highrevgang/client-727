/* Class79 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class79 implements Interface42 {
	public static Class282_Sub15_Sub4 aClass282_Sub15_Sub4_783;

	public int method237(int i) {
		Class282_Sub38 class282_sub38 = (Class282_Sub38) Class86.aClass465_823.method7754((long) i);
		if (class282_sub38 == null)
			return Class158_Sub1.aClass3_8507.method240(i, 1409956055);
		return -570797415 * class282_sub38.anInt8002;
	}

	public int method241(int i, int i_0_) {
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) Class86.aClass465_823.method7754(0x100000000L | (long) i));
		if (class282_sub38 == null)
			return Class158_Sub1.aClass3_8507.method241(i, -1812912714);
		return -570797415 * class282_sub38.anInt8002;
	}

	public int method240(int i, int i_1_) {
		Class282_Sub38 class282_sub38 = (Class282_Sub38) Class86.aClass465_823.method7754((long) i);
		if (class282_sub38 == null)
			return Class158_Sub1.aClass3_8507.method240(i, 691634431);
		return -570797415 * class282_sub38.anInt8002;
	}

	public int method239(int i) {
		Class282_Sub38 class282_sub38 = (Class282_Sub38) Class86.aClass465_823.method7754((long) i);
		if (class282_sub38 == null)
			return Class158_Sub1.aClass3_8507.method240(i, 888043676);
		return -570797415 * class282_sub38.anInt8002;
	}

	public int method238(int i) {
		Class282_Sub38 class282_sub38 = (Class282_Sub38) Class86.aClass465_823.method7754((long) i);
		if (class282_sub38 == null)
			return Class158_Sub1.aClass3_8507.method240(i, -501528240);
		return -570797415 * class282_sub38.anInt8002;
	}

	public int method236(int i) {
		Class282_Sub38 class282_sub38 = (Class282_Sub38) Class86.aClass465_823.method7754((long) i);
		if (class282_sub38 == null)
			return Class158_Sub1.aClass3_8507.method240(i, 243195428);
		return -570797415 * class282_sub38.anInt8002;
	}

	public int method242(int i) {
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) Class86.aClass465_823.method7754(0x100000000L | (long) i));
		if (class282_sub38 == null)
			return Class158_Sub1.aClass3_8507.method241(i, 1609016974);
		return -570797415 * class282_sub38.anInt8002;
	}

	public static void method1390(int i) {
		Class86.aClass465_823.method7749(1941006178);
		Class86.aClass482_827.method8118(515043749);
		Class86.aClass77Array818 = null;
		Class82.aClass75Array804 = null;
		Class86.aClass92Array820 = null;
		Class86.aClass93Array821 = null;
		Class86.aClass96Array822 = null;
		Class86.anInt819 = 523556513;
		Class86.anInt825 = 0;
		Class86.anInt824 = 0;
		Class282_Sub42.aClass85_8039 = null;
		Class86.anInt831 = -1539001787;
		if (Class86.aBool826) {
			client.aShort7214 = Class86.aShort828;
			client.aShort7474 = Class86.aShort829;
			client.aShort7276 = Class534.aShort7079;
			client.aShort7441 = Class86.aShort830;
			Class86.aBool826 = false;
		}
	}

	static final void method1391(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = -861975801 * Class427.anInt5123 + Class291.anInt3472;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 483850921 * Class475.anInt5624 + Class291.anInt3473;
	}
}
