/* Class282_Sub17_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub17_Sub9 extends Class282_Sub17 {
	int anInt10044;
	String aString10045;
	long aLong10046;
	Class348 this$0;

	Class282_Sub17_Sub9(Class348 class348) {
		((Class282_Sub17_Sub9) this).this$0 = class348;
		((Class282_Sub17_Sub9) this).aLong10046 = 3261396651546806203L;
		((Class282_Sub17_Sub9) this).aString10045 = null;
		((Class282_Sub17_Sub9) this).anInt10044 = 0;
	}

	void method12250(RsByteBuffer class282_sub35, int i) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub9) this).aLong10046 = (class282_sub35.method13087(190708592) * -3261396651546806203L);
		}
		((Class282_Sub17_Sub9) this).aString10045 = class282_sub35.method13090(1081683034);
		((Class282_Sub17_Sub9) this).anInt10044 = class282_sub35.readUnsignedShort() * -643750647;
	}

	void method12251(Class61 class61, int i) {
		class61.method1216((((Class282_Sub17_Sub9) this).aLong10046 * 1079166826874198669L), ((Class282_Sub17_Sub9) this).aString10045, 881981753 * ((Class282_Sub17_Sub9) this).anInt10044, (byte) -34);
	}

	void method12249(RsByteBuffer class282_sub35) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub9) this).aLong10046 = (class282_sub35.method13087(1103302515) * -3261396651546806203L);
		}
		((Class282_Sub17_Sub9) this).aString10045 = class282_sub35.method13090(-140462004);
		((Class282_Sub17_Sub9) this).anInt10044 = class282_sub35.readUnsignedShort() * -643750647;
	}

	void method12253(RsByteBuffer class282_sub35) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub9) this).aLong10046 = (class282_sub35.method13087(1812970180) * -3261396651546806203L);
		}
		((Class282_Sub17_Sub9) this).aString10045 = class282_sub35.method13090(2086307282);
		((Class282_Sub17_Sub9) this).anInt10044 = class282_sub35.readUnsignedShort() * -643750647;
	}

	void method12255(Class61 class61) {
		class61.method1216((((Class282_Sub17_Sub9) this).aLong10046 * 1079166826874198669L), ((Class282_Sub17_Sub9) this).aString10045, 881981753 * ((Class282_Sub17_Sub9) this).anInt10044, (byte) 23);
	}

	void method12254(Class61 class61) {
		class61.method1216((((Class282_Sub17_Sub9) this).aLong10046 * 1079166826874198669L), ((Class282_Sub17_Sub9) this).aString10045, 881981753 * ((Class282_Sub17_Sub9) this).anInt10044, (byte) -21);
	}

	void method12258(Class61 class61) {
		class61.method1216((((Class282_Sub17_Sub9) this).aLong10046 * 1079166826874198669L), ((Class282_Sub17_Sub9) this).aString10045, 881981753 * ((Class282_Sub17_Sub9) this).anInt10044, (byte) -66);
	}

	void method12256(Class61 class61) {
		class61.method1216((((Class282_Sub17_Sub9) this).aLong10046 * 1079166826874198669L), ((Class282_Sub17_Sub9) this).aString10045, 881981753 * ((Class282_Sub17_Sub9) this).anInt10044, (byte) 30);
	}

	void method12252(Class61 class61) {
		class61.method1216((((Class282_Sub17_Sub9) this).aLong10046 * 1079166826874198669L), ((Class282_Sub17_Sub9) this).aString10045, 881981753 * ((Class282_Sub17_Sub9) this).anInt10044, (byte) -12);
	}

	void method12257(RsByteBuffer class282_sub35) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub9) this).aLong10046 = (class282_sub35.method13087(1495645552) * -3261396651546806203L);
		}
		((Class282_Sub17_Sub9) this).aString10045 = class282_sub35.method13090(288410826);
		((Class282_Sub17_Sub9) this).anInt10044 = class282_sub35.readUnsignedShort() * -643750647;
	}

	static final void method15448(Class527 class527, byte i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class28.method774(class118, class98, class527, -1296322680);
	}
}
