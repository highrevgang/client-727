/* Class48_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class48_Sub2 extends Class48 {
	int[] anIntArray9233;
	static final int anInt9234 = 0;
	static final int anInt9235 = 1;
	static final int anInt9236 = 2;
	static final int anInt9237 = 18;
	static final int anInt9238 = 4;
	static final int anInt9239 = 5;
	static final int anInt9240 = 6;
	static final int anInt9241 = 7;
	static final int anInt9242 = 8;
	static final int anInt9243 = 14;
	static final int anInt9244 = 10;
	static final int anInt9245 = 11;
	static final int anInt9246 = 7;
	static final int anInt9247 = 13;
	static final int anInt9248 = 2;
	static final int anInt9249 = 15;
	static final int anInt9250 = 16;
	static final int anInt9251 = 17;
	static final int anInt9252 = 0;
	static final int anInt9253 = 1;
	Class115[] aClass115Array9254;
	static final int anInt9255 = 3;
	static final int anInt9256 = 12;
	Class101 aClass101_9257;
	int[][] anIntArrayArray9258;
	Class384 aClass384_9259 = new Class384();
	static final int anInt9260 = 12;
	static final int anInt9261 = 9;
	static final int anInt9262 = 17;
	public static byte aByte9263;

	public void method954(boolean bool) {
		if (bool)
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[1]);
		else
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[0]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2083631788)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[2]), 0, anInterface6_452, 453082583);
		((Class48_Sub2) this).aClass101_9257.method1676((((Class48_Sub2) this).anIntArray9233[4]), (((Class48_Sub2) this).aClass384_9259), -1217458582);
		((Class48_Sub2) this).aClass101_9257.method1675((((Class48_Sub2) this).anIntArray9233[5]), aClass384_454, (byte) 82);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[7], aClass303_458.aFloat3568, aClass303_458.aFloat3566, aClass303_458.aFloat3565, aClass303_458.aFloat3567, (byte) 45);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[8], aClass385_459.aFloat4671, aClass385_459.aFloat4672, aClass385_459.aFloat4673, -1433198453);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[9], aClass303_460.aFloat3568, aClass303_460.aFloat3566, aClass303_460.aFloat3565, aClass303_460.aFloat3567, (byte) 80);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[10], aClass385_457.aFloat4671, aClass385_457.aFloat4672, aClass385_457.aFloat4673, -767770113);
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method962(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[i + 7]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2072302880)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[6], aClass385_455.aFloat4671, aClass385_455.aFloat4672, aClass385_455.aFloat4673, 47104762);
		((Class48_Sub2) this).aClass101_9257.method1749((((Class48_Sub2) this).anIntArray9233[16]), aFloat456, aFloat453, 0.0F, 0.0F, (byte) 116);
		method14564(i, 1675559145);
	}

	public void method967(Class384 class384) {
		((Class48_Sub2) this).aClass384_9259.method6562(class384);
		((Class48_Sub2) this).aClass384_9259.method6523(aClass505_Sub2_449.aClass384_8729);
	}

	public void method946(boolean bool) {
		if (bool)
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[1]);
		else
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[0]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-1928036679)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[2]), 0, anInterface6_452, -956219190);
		((Class48_Sub2) this).aClass101_9257.method1676((((Class48_Sub2) this).anIntArray9233[4]), (((Class48_Sub2) this).aClass384_9259), 1888868899);
		((Class48_Sub2) this).aClass101_9257.method1675((((Class48_Sub2) this).anIntArray9233[5]), aClass384_454, (byte) 109);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[7], aClass303_458.aFloat3568, aClass303_458.aFloat3566, aClass303_458.aFloat3565, aClass303_458.aFloat3567, (byte) 118);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[8], aClass385_459.aFloat4671, aClass385_459.aFloat4672, aClass385_459.aFloat4673, -1035790927);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[9], aClass303_460.aFloat3568, aClass303_460.aFloat3566, aClass303_460.aFloat3565, aClass303_460.aFloat3567, (byte) 66);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[10], aClass385_457.aFloat4671, aClass385_457.aFloat4672, aClass385_457.aFloat4673, -1490265321);
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method950() {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[17]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2016797629)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		method14564(0, 1210821365);
	}

	void method14564(int i, int i_0_) {
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[2]), 0, anInterface6_452, 2040380246);
		((Class48_Sub2) this).aClass101_9257.method1676((((Class48_Sub2) this).anIntArray9233[4]), (((Class48_Sub2) this).aClass384_9259), 2139383283);
		((Class48_Sub2) this).aClass101_9257.method1675((((Class48_Sub2) this).anIntArray9233[5]), aClass384_454, (byte) 50);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[7], aClass303_458.aFloat3568, aClass303_458.aFloat3566, aClass303_458.aFloat3565, aClass303_458.aFloat3567, (byte) 74);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[8], aClass385_459.aFloat4671, aClass385_459.aFloat4672, aClass385_459.aFloat4673, -1782352428);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[9], aClass303_460.aFloat3568, aClass303_460.aFloat3566, aClass303_460.aFloat3565, aClass303_460.aFloat3567, (byte) 125);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[10], aClass385_457.aFloat4671, aClass385_457.aFloat4672, aClass385_457.aFloat4673, -560430495);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[11]), aClass385_466, (byte) 29);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[12]), aClass385_448, (byte) 2);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[13]), aClass385_464, (byte) 8);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[14]), aClass385_461, (byte) 98);
		if (i > 0) {
			((Class48_Sub2) this).aClass101_9257.method1673(((Class48_Sub2) this).anIntArray9233[1], aFloatArray463, i * 4, -513105283);
			((Class48_Sub2) this).aClass101_9257.method1673(((Class48_Sub2) this).anIntArray9233[0], aFloatArray450, 4 * i, -513105283);
		}
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method965(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2 + i]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-1899665000)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		method14564(i, -1166788219);
	}

	public void method948(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[i + 7]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2006880089)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[6], aClass385_455.aFloat4671, aClass385_455.aFloat4672, aClass385_455.aFloat4673, -202231546);
		((Class48_Sub2) this).aClass101_9257.method1749((((Class48_Sub2) this).anIntArray9233[16]), aFloat456, aFloat453, 0.0F, 0.0F, (byte) 50);
		method14564(i, 2089745426);
	}

	public void method949(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[12 + i]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-1924915864)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1674((((Class48_Sub2) this).anIntArray9233[15]), aClass384_465, -899265256);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[6], aClass385_455.aFloat4671, aClass385_455.aFloat4672, aClass385_455.aFloat4673, 703628895);
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[3]), 1, anInterface31_451, -432714576);
		method14564(i, 1162635311);
	}

	public void method951(Class384 class384) {
		((Class48_Sub2) this).aClass384_9259.method6562(class384);
		((Class48_Sub2) this).aClass384_9259.method6523(aClass505_Sub2_449.aClass384_8729);
	}

	public void method952(Class384 class384) {
		((Class48_Sub2) this).aClass384_9259.method6562(class384);
		((Class48_Sub2) this).aClass384_9259.method6523(aClass505_Sub2_449.aClass384_8729);
	}

	public void method953(Class384 class384) {
		((Class48_Sub2) this).aClass384_9259.method6562(class384);
		((Class48_Sub2) this).aClass384_9259.method6523(aClass505_Sub2_449.aClass384_8729);
	}

	public void method963(Class384 class384) {
		((Class48_Sub2) this).aClass384_9259.method6562(class384);
		((Class48_Sub2) this).aClass384_9259.method6523(aClass505_Sub2_449.aClass384_8729);
	}

	boolean method14565() throws Exception_Sub2 {
		((Class48_Sub2) this).aClass101_9257 = aClass505_Sub2_449.method13890("Model");
		Class282_Sub21_Sub1 class282_sub21_sub1 = ((Class48_Sub2) this).aClass101_9257.method1691("DiffuseSampler", (short) 4692);
		Class282_Sub21_Sub1 class282_sub21_sub1_1_ = ((Class48_Sub2) this).aClass101_9257.method1691("EnvironmentSampler", (short) 10678);
		Class282_Sub21_Sub1 class282_sub21_sub1_2_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsPosAndRadiusSq", (short) 29042);
		Class282_Sub21_Sub1 class282_sub21_sub1_3_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsDiffuseColour", (short) 31564);
		Class282_Sub21_Sub1 class282_sub21_sub1_4_ = ((Class48_Sub2) this).aClass101_9257.method1691("WVPMatrix", (short) 6355);
		Class282_Sub21_Sub1 class282_sub21_sub1_5_ = ((Class48_Sub2) this).aClass101_9257.method1691("TexCoordMatrix", (short) 22486);
		Class282_Sub21_Sub1 class282_sub21_sub1_6_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogPlane", (short) 22719);
		Class282_Sub21_Sub1 class282_sub21_sub1_7_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogColour", (short) 4606);
		Class282_Sub21_Sub1 class282_sub21_sub1_8_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogPlane", (short) 31340);
		Class282_Sub21_Sub1 class282_sub21_sub1_9_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogColour", (short) 7310);
		Class282_Sub21_Sub1 class282_sub21_sub1_10_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunDir", (short) 9328);
		Class282_Sub21_Sub1 class282_sub21_sub1_11_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunColour", (short) 30602);
		Class282_Sub21_Sub1 class282_sub21_sub1_12_ = ((Class48_Sub2) this).aClass101_9257.method1691("AntiSunColour", (short) 22980);
		Class282_Sub21_Sub1 class282_sub21_sub1_13_ = ((Class48_Sub2) this).aClass101_9257.method1691("AmbientColour", (short) 22472);
		Class282_Sub21_Sub1 class282_sub21_sub1_14_ = ((Class48_Sub2) this).aClass101_9257.method1691("EyePos", (short) 5701);
		Class282_Sub21_Sub1 class282_sub21_sub1_15_ = ((Class48_Sub2) this).aClass101_9257.method1691("SpecularExponent", (short) 23680);
		Class282_Sub21_Sub1 class282_sub21_sub1_16_ = ((Class48_Sub2) this).aClass101_9257.method1691("WorldMatrix", (short) 16969);
		((Class48_Sub2) this).aClass115Array9254[0] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit", -1671575960);
		((Class48_Sub2) this).aClass115Array9254[1] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit_IgnoreAlpha", 1375713639);
		((Class48_Sub2) this).aClass115Array9254[17] = ((Class48_Sub2) this).aClass101_9257.method1651("UnderwaterGround", 1129429619);
		for (int i = 0; i <= 4; i++) {
			((Class48_Sub2) this).aClass115Array9254[i + 2] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Standard_").append(i).append("PointLights").toString(), -935869470));
			((Class48_Sub2) this).aClass115Array9254[7 + i] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Specular_").append(i).append("PointLights").toString(), 563102150));
			((Class48_Sub2) this).aClass115Array9254[12 + i] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("EnvironmentalMapping_").append(i).append("PointLights").toString(), 645711813));
		}
		for (int i = 0; i < 18; i++) {
			int i_17_ = (((Class48_Sub2) this).aClass101_9257.method1653(((Class48_Sub2) this).aClass115Array9254[i], (byte) -97));
			((Class48_Sub2) this).anIntArrayArray9258[i][2] = class282_sub21_sub1.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][3] = class282_sub21_sub1_1_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][1] = class282_sub21_sub1_2_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][0] = class282_sub21_sub1_3_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][4] = class282_sub21_sub1_4_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][5] = class282_sub21_sub1_5_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][7] = class282_sub21_sub1_6_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][8] = class282_sub21_sub1_7_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][9] = class282_sub21_sub1_8_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][10] = class282_sub21_sub1_9_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][11] = class282_sub21_sub1_10_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][12] = class282_sub21_sub1_11_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][13] = class282_sub21_sub1_12_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][14] = class282_sub21_sub1_13_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][6] = class282_sub21_sub1_14_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][15] = class282_sub21_sub1_16_.method15460(i_17_);
			((Class48_Sub2) this).anIntArrayArray9258[i][16] = class282_sub21_sub1_15_.method15460(i_17_);
		}
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2]);
		return true;
	}

	public void method955(boolean bool) {
		if (bool)
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[1]);
		else
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[0]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-1898332036)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[2]), 0, anInterface6_452, 1361853962);
		((Class48_Sub2) this).aClass101_9257.method1676((((Class48_Sub2) this).anIntArray9233[4]), (((Class48_Sub2) this).aClass384_9259), 695769081);
		((Class48_Sub2) this).aClass101_9257.method1675((((Class48_Sub2) this).anIntArray9233[5]), aClass384_454, (byte) 45);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[7], aClass303_458.aFloat3568, aClass303_458.aFloat3566, aClass303_458.aFloat3565, aClass303_458.aFloat3567, (byte) 24);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[8], aClass385_459.aFloat4671, aClass385_459.aFloat4672, aClass385_459.aFloat4673, -385081679);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[9], aClass303_460.aFloat3568, aClass303_460.aFloat3566, aClass303_460.aFloat3565, aClass303_460.aFloat3567, (byte) 113);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[10], aClass385_457.aFloat4671, aClass385_457.aFloat4672, aClass385_457.aFloat4673, 803125359);
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method956(boolean bool) {
		if (bool)
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[1]);
		else
			((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[0]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2097949525)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[2]), 0, anInterface6_452, -1278912022);
		((Class48_Sub2) this).aClass101_9257.method1676((((Class48_Sub2) this).anIntArray9233[4]), (((Class48_Sub2) this).aClass384_9259), 1212549473);
		((Class48_Sub2) this).aClass101_9257.method1675((((Class48_Sub2) this).anIntArray9233[5]), aClass384_454, (byte) 70);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[7], aClass303_458.aFloat3568, aClass303_458.aFloat3566, aClass303_458.aFloat3565, aClass303_458.aFloat3567, (byte) 59);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[8], aClass385_459.aFloat4671, aClass385_459.aFloat4672, aClass385_459.aFloat4673, 1042218945);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[9], aClass303_460.aFloat3568, aClass303_460.aFloat3566, aClass303_460.aFloat3565, aClass303_460.aFloat3567, (byte) 77);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[10], aClass385_457.aFloat4671, aClass385_457.aFloat4672, aClass385_457.aFloat4673, 245640981);
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method966() {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[17]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2001535088)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		method14564(0, 1959735743);
	}

	public void method945(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2 + i]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2091713098)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		method14564(i, -136493860);
	}

	public void method959(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2 + i]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-1956674833)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		method14564(i, -1481648847);
	}

	public void method961(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[i + 7]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2127764452)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[6], aClass385_455.aFloat4671, aClass385_455.aFloat4672, aClass385_455.aFloat4673, 943954770);
		((Class48_Sub2) this).aClass101_9257.method1749((((Class48_Sub2) this).anIntArray9233[16]), aFloat456, aFloat453, 0.0F, 0.0F, (byte) 48);
		method14564(i, 342700156);
	}

	void method14566(int i) {
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[2]), 0, anInterface6_452, 2127252913);
		((Class48_Sub2) this).aClass101_9257.method1676((((Class48_Sub2) this).anIntArray9233[4]), (((Class48_Sub2) this).aClass384_9259), 2045218041);
		((Class48_Sub2) this).aClass101_9257.method1675((((Class48_Sub2) this).anIntArray9233[5]), aClass384_454, (byte) 87);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[7], aClass303_458.aFloat3568, aClass303_458.aFloat3566, aClass303_458.aFloat3565, aClass303_458.aFloat3567, (byte) 13);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[8], aClass385_459.aFloat4671, aClass385_459.aFloat4672, aClass385_459.aFloat4673, 387513847);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[9], aClass303_460.aFloat3568, aClass303_460.aFloat3566, aClass303_460.aFloat3565, aClass303_460.aFloat3567, (byte) 114);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[10], aClass385_457.aFloat4671, aClass385_457.aFloat4672, aClass385_457.aFloat4673, -631832113);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[11]), aClass385_466, (byte) 103);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[12]), aClass385_448, (byte) 112);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[13]), aClass385_464, (byte) 114);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[14]), aClass385_461, (byte) 54);
		if (i > 0) {
			((Class48_Sub2) this).aClass101_9257.method1673(((Class48_Sub2) this).anIntArray9233[1], aFloatArray463, i * 4, -513105283);
			((Class48_Sub2) this).aClass101_9257.method1673(((Class48_Sub2) this).anIntArray9233[0], aFloatArray450, 4 * i, -513105283);
		}
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public Class48_Sub2(Class505_Sub2 class505_sub2) throws Exception_Sub2 {
		super(class505_sub2);
		((Class48_Sub2) this).aClass115Array9254 = new Class115[18];
		((Class48_Sub2) this).anIntArrayArray9258 = new int[18][17];
		method14567(-2029794295);
	}

	public void method947(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[i + 7]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2022586549)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[6], aClass385_455.aFloat4671, aClass385_455.aFloat4672, aClass385_455.aFloat4673, -404792315);
		((Class48_Sub2) this).aClass101_9257.method1749((((Class48_Sub2) this).anIntArray9233[16]), aFloat456, aFloat453, 0.0F, 0.0F, (byte) 113);
		method14564(i, 2016572869);
	}

	public void method964(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[i + 7]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2105378964)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[6], aClass385_455.aFloat4671, aClass385_455.aFloat4672, aClass385_455.aFloat4673, -1125350225);
		((Class48_Sub2) this).aClass101_9257.method1749((((Class48_Sub2) this).anIntArray9233[16]), aFloat456, aFloat453, 0.0F, 0.0F, (byte) 13);
		method14564(i, 1489332268);
	}

	public void method958(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[12 + i]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2017240700)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		((Class48_Sub2) this).aClass101_9257.method1674((((Class48_Sub2) this).anIntArray9233[15]), aClass384_465, -899265256);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[6], aClass385_455.aFloat4671, aClass385_455.aFloat4672, aClass385_455.aFloat4673, 1067202636);
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[3]), 1, anInterface31_451, 2108931514);
		method14564(i, -1921268776);
	}

	boolean method14567(int i) throws Exception_Sub2 {
		((Class48_Sub2) this).aClass101_9257 = aClass505_Sub2_449.method13890("Model");
		Class282_Sub21_Sub1 class282_sub21_sub1 = ((Class48_Sub2) this).aClass101_9257.method1691("DiffuseSampler", (short) 32201);
		Class282_Sub21_Sub1 class282_sub21_sub1_18_ = ((Class48_Sub2) this).aClass101_9257.method1691("EnvironmentSampler", (short) 32001);
		Class282_Sub21_Sub1 class282_sub21_sub1_19_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsPosAndRadiusSq", (short) 15792);
		Class282_Sub21_Sub1 class282_sub21_sub1_20_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsDiffuseColour", (short) 26295);
		Class282_Sub21_Sub1 class282_sub21_sub1_21_ = ((Class48_Sub2) this).aClass101_9257.method1691("WVPMatrix", (short) 28335);
		Class282_Sub21_Sub1 class282_sub21_sub1_22_ = ((Class48_Sub2) this).aClass101_9257.method1691("TexCoordMatrix", (short) 26561);
		Class282_Sub21_Sub1 class282_sub21_sub1_23_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogPlane", (short) 7913);
		Class282_Sub21_Sub1 class282_sub21_sub1_24_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogColour", (short) 7930);
		Class282_Sub21_Sub1 class282_sub21_sub1_25_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogPlane", (short) 14106);
		Class282_Sub21_Sub1 class282_sub21_sub1_26_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogColour", (short) 22664);
		Class282_Sub21_Sub1 class282_sub21_sub1_27_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunDir", (short) 4744);
		Class282_Sub21_Sub1 class282_sub21_sub1_28_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunColour", (short) 30429);
		Class282_Sub21_Sub1 class282_sub21_sub1_29_ = ((Class48_Sub2) this).aClass101_9257.method1691("AntiSunColour", (short) 20720);
		Class282_Sub21_Sub1 class282_sub21_sub1_30_ = ((Class48_Sub2) this).aClass101_9257.method1691("AmbientColour", (short) 7520);
		Class282_Sub21_Sub1 class282_sub21_sub1_31_ = ((Class48_Sub2) this).aClass101_9257.method1691("EyePos", (short) 8667);
		Class282_Sub21_Sub1 class282_sub21_sub1_32_ = ((Class48_Sub2) this).aClass101_9257.method1691("SpecularExponent", (short) 29276);
		Class282_Sub21_Sub1 class282_sub21_sub1_33_ = ((Class48_Sub2) this).aClass101_9257.method1691("WorldMatrix", (short) 30418);
		((Class48_Sub2) this).aClass115Array9254[0] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit", -1079747106);
		((Class48_Sub2) this).aClass115Array9254[1] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit_IgnoreAlpha", 594571928);
		((Class48_Sub2) this).aClass115Array9254[17] = ((Class48_Sub2) this).aClass101_9257.method1651("UnderwaterGround", -1889112916);
		for (int i_34_ = 0; i_34_ <= 4; i_34_++) {
			((Class48_Sub2) this).aClass115Array9254[i_34_ + 2] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Standard_").append(i_34_).append("PointLights").toString(), 438281176));
			((Class48_Sub2) this).aClass115Array9254[7 + i_34_] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Specular_").append(i_34_).append("PointLights").toString(), 1570211177));
			((Class48_Sub2) this).aClass115Array9254[12 + i_34_] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("EnvironmentalMapping_").append(i_34_).append("PointLights").toString(), 1693102715));
		}
		for (int i_35_ = 0; i_35_ < 18; i_35_++) {
			int i_36_ = (((Class48_Sub2) this).aClass101_9257.method1653(((Class48_Sub2) this).aClass115Array9254[i_35_], (byte) -114));
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][2] = class282_sub21_sub1.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][3] = class282_sub21_sub1_18_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][1] = class282_sub21_sub1_19_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][0] = class282_sub21_sub1_20_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][4] = class282_sub21_sub1_21_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][5] = class282_sub21_sub1_22_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][7] = class282_sub21_sub1_23_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][8] = class282_sub21_sub1_24_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][9] = class282_sub21_sub1_25_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][10] = class282_sub21_sub1_26_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][11] = class282_sub21_sub1_27_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][12] = class282_sub21_sub1_28_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][13] = class282_sub21_sub1_29_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][14] = class282_sub21_sub1_30_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][6] = class282_sub21_sub1_31_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][15] = class282_sub21_sub1_33_.method15460(i_36_);
			((Class48_Sub2) this).anIntArrayArray9258[i_35_][16] = class282_sub21_sub1_32_.method15460(i_36_);
		}
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2]);
		return true;
	}

	boolean method14568() throws Exception_Sub2 {
		((Class48_Sub2) this).aClass101_9257 = aClass505_Sub2_449.method13890("Model");
		Class282_Sub21_Sub1 class282_sub21_sub1 = ((Class48_Sub2) this).aClass101_9257.method1691("DiffuseSampler", (short) 30708);
		Class282_Sub21_Sub1 class282_sub21_sub1_37_ = ((Class48_Sub2) this).aClass101_9257.method1691("EnvironmentSampler", (short) 21348);
		Class282_Sub21_Sub1 class282_sub21_sub1_38_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsPosAndRadiusSq", (short) 13058);
		Class282_Sub21_Sub1 class282_sub21_sub1_39_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsDiffuseColour", (short) 31817);
		Class282_Sub21_Sub1 class282_sub21_sub1_40_ = ((Class48_Sub2) this).aClass101_9257.method1691("WVPMatrix", (short) 18062);
		Class282_Sub21_Sub1 class282_sub21_sub1_41_ = ((Class48_Sub2) this).aClass101_9257.method1691("TexCoordMatrix", (short) 25194);
		Class282_Sub21_Sub1 class282_sub21_sub1_42_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogPlane", (short) 15229);
		Class282_Sub21_Sub1 class282_sub21_sub1_43_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogColour", (short) 13279);
		Class282_Sub21_Sub1 class282_sub21_sub1_44_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogPlane", (short) 26453);
		Class282_Sub21_Sub1 class282_sub21_sub1_45_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogColour", (short) 7979);
		Class282_Sub21_Sub1 class282_sub21_sub1_46_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunDir", (short) 25288);
		Class282_Sub21_Sub1 class282_sub21_sub1_47_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunColour", (short) 16717);
		Class282_Sub21_Sub1 class282_sub21_sub1_48_ = ((Class48_Sub2) this).aClass101_9257.method1691("AntiSunColour", (short) 29611);
		Class282_Sub21_Sub1 class282_sub21_sub1_49_ = ((Class48_Sub2) this).aClass101_9257.method1691("AmbientColour", (short) 10489);
		Class282_Sub21_Sub1 class282_sub21_sub1_50_ = ((Class48_Sub2) this).aClass101_9257.method1691("EyePos", (short) 20017);
		Class282_Sub21_Sub1 class282_sub21_sub1_51_ = ((Class48_Sub2) this).aClass101_9257.method1691("SpecularExponent", (short) 16134);
		Class282_Sub21_Sub1 class282_sub21_sub1_52_ = ((Class48_Sub2) this).aClass101_9257.method1691("WorldMatrix", (short) 29998);
		((Class48_Sub2) this).aClass115Array9254[0] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit", 2058185533);
		((Class48_Sub2) this).aClass115Array9254[1] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit_IgnoreAlpha", 528580131);
		((Class48_Sub2) this).aClass115Array9254[17] = ((Class48_Sub2) this).aClass101_9257.method1651("UnderwaterGround", 329754068);
		for (int i = 0; i <= 4; i++) {
			((Class48_Sub2) this).aClass115Array9254[i + 2] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Standard_").append(i).append("PointLights").toString(), 1619063203));
			((Class48_Sub2) this).aClass115Array9254[7 + i] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Specular_").append(i).append("PointLights").toString(), 240343169));
			((Class48_Sub2) this).aClass115Array9254[12 + i] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("EnvironmentalMapping_").append(i).append("PointLights").toString(), 642659845));
		}
		for (int i = 0; i < 18; i++) {
			int i_53_ = (((Class48_Sub2) this).aClass101_9257.method1653(((Class48_Sub2) this).aClass115Array9254[i], (byte) -48));
			((Class48_Sub2) this).anIntArrayArray9258[i][2] = class282_sub21_sub1.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][3] = class282_sub21_sub1_37_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][1] = class282_sub21_sub1_38_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][0] = class282_sub21_sub1_39_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][4] = class282_sub21_sub1_40_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][5] = class282_sub21_sub1_41_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][7] = class282_sub21_sub1_42_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][8] = class282_sub21_sub1_43_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][9] = class282_sub21_sub1_44_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][10] = class282_sub21_sub1_45_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][11] = class282_sub21_sub1_46_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][12] = class282_sub21_sub1_47_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][13] = class282_sub21_sub1_48_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][14] = class282_sub21_sub1_49_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][6] = class282_sub21_sub1_50_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][15] = class282_sub21_sub1_52_.method15460(i_53_);
			((Class48_Sub2) this).anIntArrayArray9258[i][16] = class282_sub21_sub1_51_.method15460(i_53_);
		}
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2]);
		return true;
	}

	boolean method14569() throws Exception_Sub2 {
		((Class48_Sub2) this).aClass101_9257 = aClass505_Sub2_449.method13890("Model");
		Class282_Sub21_Sub1 class282_sub21_sub1 = ((Class48_Sub2) this).aClass101_9257.method1691("DiffuseSampler", (short) 19902);
		Class282_Sub21_Sub1 class282_sub21_sub1_54_ = ((Class48_Sub2) this).aClass101_9257.method1691("EnvironmentSampler", (short) 12908);
		Class282_Sub21_Sub1 class282_sub21_sub1_55_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsPosAndRadiusSq", (short) 24321);
		Class282_Sub21_Sub1 class282_sub21_sub1_56_ = ((Class48_Sub2) this).aClass101_9257.method1691("PointLightsDiffuseColour", (short) 4425);
		Class282_Sub21_Sub1 class282_sub21_sub1_57_ = ((Class48_Sub2) this).aClass101_9257.method1691("WVPMatrix", (short) 21872);
		Class282_Sub21_Sub1 class282_sub21_sub1_58_ = ((Class48_Sub2) this).aClass101_9257.method1691("TexCoordMatrix", (short) 10794);
		Class282_Sub21_Sub1 class282_sub21_sub1_59_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogPlane", (short) 26541);
		Class282_Sub21_Sub1 class282_sub21_sub1_60_ = ((Class48_Sub2) this).aClass101_9257.method1691("HeightFogColour", (short) 16685);
		Class282_Sub21_Sub1 class282_sub21_sub1_61_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogPlane", (short) 7894);
		Class282_Sub21_Sub1 class282_sub21_sub1_62_ = ((Class48_Sub2) this).aClass101_9257.method1691("DistanceFogColour", (short) 7028);
		Class282_Sub21_Sub1 class282_sub21_sub1_63_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunDir", (short) 25032);
		Class282_Sub21_Sub1 class282_sub21_sub1_64_ = ((Class48_Sub2) this).aClass101_9257.method1691("SunColour", (short) 11232);
		Class282_Sub21_Sub1 class282_sub21_sub1_65_ = ((Class48_Sub2) this).aClass101_9257.method1691("AntiSunColour", (short) 18246);
		Class282_Sub21_Sub1 class282_sub21_sub1_66_ = ((Class48_Sub2) this).aClass101_9257.method1691("AmbientColour", (short) 29611);
		Class282_Sub21_Sub1 class282_sub21_sub1_67_ = ((Class48_Sub2) this).aClass101_9257.method1691("EyePos", (short) 4918);
		Class282_Sub21_Sub1 class282_sub21_sub1_68_ = ((Class48_Sub2) this).aClass101_9257.method1691("SpecularExponent", (short) 25653);
		Class282_Sub21_Sub1 class282_sub21_sub1_69_ = ((Class48_Sub2) this).aClass101_9257.method1691("WorldMatrix", (short) 25296);
		((Class48_Sub2) this).aClass115Array9254[0] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit", 1797367966);
		((Class48_Sub2) this).aClass115Array9254[1] = ((Class48_Sub2) this).aClass101_9257.method1651("Unlit_IgnoreAlpha", 1928677709);
		((Class48_Sub2) this).aClass115Array9254[17] = ((Class48_Sub2) this).aClass101_9257.method1651("UnderwaterGround", -1315713322);
		for (int i = 0; i <= 4; i++) {
			((Class48_Sub2) this).aClass115Array9254[i + 2] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Standard_").append(i).append("PointLights").toString(), -1617700953));
			((Class48_Sub2) this).aClass115Array9254[7 + i] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("Specular_").append(i).append("PointLights").toString(), 912744878));
			((Class48_Sub2) this).aClass115Array9254[12 + i] = (((Class48_Sub2) this).aClass101_9257.method1651(new StringBuilder().append("EnvironmentalMapping_").append(i).append("PointLights").toString(), -1012660437));
		}
		for (int i = 0; i < 18; i++) {
			int i_70_ = (((Class48_Sub2) this).aClass101_9257.method1653(((Class48_Sub2) this).aClass115Array9254[i], (byte) -122));
			((Class48_Sub2) this).anIntArrayArray9258[i][2] = class282_sub21_sub1.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][3] = class282_sub21_sub1_54_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][1] = class282_sub21_sub1_55_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][0] = class282_sub21_sub1_56_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][4] = class282_sub21_sub1_57_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][5] = class282_sub21_sub1_58_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][7] = class282_sub21_sub1_59_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][8] = class282_sub21_sub1_60_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][9] = class282_sub21_sub1_61_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][10] = class282_sub21_sub1_62_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][11] = class282_sub21_sub1_63_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][12] = class282_sub21_sub1_64_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][13] = class282_sub21_sub1_65_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][14] = class282_sub21_sub1_66_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][6] = class282_sub21_sub1_67_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][15] = class282_sub21_sub1_69_.method15460(i_70_);
			((Class48_Sub2) this).anIntArrayArray9258[i][16] = class282_sub21_sub1_68_.method15460(i_70_);
		}
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2]);
		return true;
	}

	public void method957(Class384 class384) {
		((Class48_Sub2) this).aClass384_9259.method6562(class384);
		((Class48_Sub2) this).aClass384_9259.method6523(aClass505_Sub2_449.aClass384_8729);
	}

	public void method943() {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[17]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2130098673)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		method14564(0, 51796199);
	}

	void method14570(int i) {
		((Class48_Sub2) this).aClass101_9257.method1677((((Class48_Sub2) this).anIntArray9233[2]), 0, anInterface6_452, 743769305);
		((Class48_Sub2) this).aClass101_9257.method1676((((Class48_Sub2) this).anIntArray9233[4]), (((Class48_Sub2) this).aClass384_9259), 446517994);
		((Class48_Sub2) this).aClass101_9257.method1675((((Class48_Sub2) this).anIntArray9233[5]), aClass384_454, (byte) 103);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[7], aClass303_458.aFloat3568, aClass303_458.aFloat3566, aClass303_458.aFloat3565, aClass303_458.aFloat3567, (byte) 110);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[8], aClass385_459.aFloat4671, aClass385_459.aFloat4672, aClass385_459.aFloat4673, 977151427);
		((Class48_Sub2) this).aClass101_9257.method1749(((Class48_Sub2) this).anIntArray9233[9], aClass303_460.aFloat3568, aClass303_460.aFloat3566, aClass303_460.aFloat3565, aClass303_460.aFloat3567, (byte) 81);
		((Class48_Sub2) this).aClass101_9257.method1670(((Class48_Sub2) this).anIntArray9233[10], aClass385_457.aFloat4671, aClass385_457.aFloat4672, aClass385_457.aFloat4673, 1618717564);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[11]), aClass385_466, (byte) 118);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[12]), aClass385_448, (byte) 22);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[13]), aClass385_464, (byte) 76);
		((Class48_Sub2) this).aClass101_9257.method1672((((Class48_Sub2) this).anIntArray9233[14]), aClass385_461, (byte) 52);
		if (i > 0) {
			((Class48_Sub2) this).aClass101_9257.method1673(((Class48_Sub2) this).anIntArray9233[1], aFloatArray463, i * 4, -513105283);
			((Class48_Sub2) this).aClass101_9257.method1673(((Class48_Sub2) this).anIntArray9233[0], aFloatArray450, 4 * i, -513105283);
		}
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method960(int i) {
		((Class48_Sub2) this).aClass101_9257.method1655(((Class48_Sub2) this).aClass115Array9254[2 + i]);
		((Class48_Sub2) this).anIntArray9233 = (((Class48_Sub2) this).anIntArrayArray9258[((Class48_Sub2) this).aClass101_9257.method1714(-2049174099)]);
		((Class48_Sub2) this).aClass101_9257.method1646();
		method14564(i, -1082037904);
	}

	public static final void method14571(byte i) {
		for (Class282_Sub31 class282_sub31 = ((Class282_Sub31) Class282_Sub31.aClass482_7775.method8097((byte) 86)); null != class282_sub31; class282_sub31 = (Class282_Sub31) Class282_Sub31.aClass482_7775.method8067(-18046490)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774) {
				((Class282_Sub31) class282_sub31).aBool7773 = true;
				if (37618455 * class282_sub31.anInt7762 >= 0 && -322610393 * class282_sub31.anInt7763 >= 0 && (class282_sub31.anInt7762 * 37618455 < client.aClass257_7353.method4424(111719128)) && (class282_sub31.anInt7763 * -322610393 < client.aClass257_7353.method4451(-1330125890)))
					Class275_Sub4.method12585(class282_sub31, -1279772486);
			} else
				class282_sub31.method4991(-371378792);
		}
		for (Class282_Sub31 class282_sub31 = ((Class282_Sub31) Class282_Sub31.aClass482_7776.method8097((byte) 50)); null != class282_sub31; class282_sub31 = (Class282_Sub31) Class282_Sub31.aClass482_7776.method8067(777905499)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774)
				((Class282_Sub31) class282_sub31).aBool7773 = true;
			else
				class282_sub31.method4991(-371378792);
		}
	}

	static void method14572(int i, int i_71_, int i_72_, int i_73_, int i_74_, short i_75_) {
		Class232.method3922(Class532_Sub1.anIntArrayArray7072[i_72_++], i, i_71_, i_74_, (byte) -27);
		Class232.method3922(Class532_Sub1.anIntArrayArray7072[i_73_--], i, i_71_, i_74_, (byte) -74);
		for (int i_76_ = i_72_; i_76_ <= i_73_; i_76_++) {
			if (i_75_ <= 1001)
				break;
			int[] is = Class532_Sub1.anIntArrayArray7072[i_76_];
			is[i] = is[i_71_] = i_74_;
		}
	}
}
