/* Class150 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class150 {
	public static Class150 aClass150_1948;
	public static Class150 aClass150_1949;
	public static Class150 aClass150_1950 = new Class150(5, 3);
	public static Class150 aClass150_1951;
	public static Class150 aClass150_1952;
	public static Class150 aClass150_1953;
	static Class150 aClass150_1954;
	static Class150 aClass150_1955;
	public static Class150 aClass150_1956;
	public static Class150 aClass150_1957;
	public int anInt1958;
	public int anInt1959;

	Class150(int i, int i_0_) {
		anInt1958 = -311970919 * i;
		anInt1959 = i_0_ * -615555291;
	}

	static {
		aClass150_1949 = new Class150(6, 4);
		aClass150_1951 = new Class150(1, 1);
		aClass150_1948 = new Class150(0, 1);
		aClass150_1952 = new Class150(4, 2);
		aClass150_1953 = new Class150(8, 1);
		aClass150_1954 = new Class150(3, 3);
		aClass150_1955 = new Class150(2, 4);
		aClass150_1956 = new Class150(7, 1);
		aClass150_1957 = new Class150(9, 1);
	}

	public static void method2580(int i, int i_1_, byte i_2_) {
		if (Class148.anInt1730 * -1423242349 != 0) {
			if (i < 0) {
				for (int i_3_ = 0; i_3_ < 16; i_3_++)
					Class453.anIntArray5449[i_3_] = i_1_;
			} else
				Class453.anIntArray5449[i] = i_1_;
		}
		Class148.aClass282_Sub15_Sub2_1735.method15095(i, i_1_, 1233853540);
	}

	static final void method2581(Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2, int i) {
		Class456_Sub3 class456_sub3 = class521_sub1_sub1_sub2.aClass456_Sub3_10337;
		if (class456_sub3.method7564(2139660919) && class456_sub3.method7627(1, -1365163818) && class456_sub3.method7580(952228354)) {
			if (class456_sub3.aBool7891) {
				class456_sub3.method7570(class521_sub1_sub1_sub2.method15855((byte) -17).method3809(386342083), false, true, -321228583);
				class456_sub3.aBool7891 = class456_sub3.method7564(-269374987);
			}
			class456_sub3.method7582((byte) -126);
		}
		for (int i_4_ = 0; i_4_ < class521_sub1_sub1_sub2.aClass161Array10339.length; i_4_++) {
			if (-1 != (class521_sub1_sub1_sub2.aClass161Array10339[i_4_].anInt2012 * 378836105)) {
				Class456 class456 = (class521_sub1_sub1_sub2.aClass161Array10339[i_4_].aClass456_2014);
				if (class456.method7573(1176831971)) {
					Class525 class525 = (Class96_Sub20.aClass515_9416.method8845((class521_sub1_sub1_sub2.aClass161Array10339[i_4_].anInt2012) * 378836105, (byte) 52));
					Class518 class518 = class456.method7565(-1632742162);
					if (class525.aBool6968) {
						if (3 == -1113882773 * class518.anInt5920) {
							if ((class521_sub1_sub1_sub2.anInt10367 * 41504957 > 0) && ((class521_sub1_sub1_sub2.anInt10342 * 403949281) <= client.anInt7174 * -1809259861) && ((class521_sub1_sub1_sub2.anInt10345 * 1277328401) < client.anInt7174 * -1809259861)) {
								class456.method7567(-1, (short) 8960);
								class521_sub1_sub1_sub2.aClass161Array10339[i_4_].anInt2012 = 1025302087;
								continue;
							}
						} else if (1 == class518.anInt5920 * -1113882773 && (class521_sub1_sub1_sub2.anInt10367 * 41504957) > 0 && ((403949281 * class521_sub1_sub1_sub2.anInt10342) <= -1809259861 * client.anInt7174) && ((1277328401 * class521_sub1_sub1_sub2.anInt10345) < -1809259861 * client.anInt7174))
							continue;
					}
				}
				if (class456.method7627(1, -1386003531) && class456.method7580(1255247674)) {
					class456.method7567(-1, (short) 8960);
					class521_sub1_sub1_sub2.aClass161Array10339[i_4_].anInt2012 = 1025302087;
				}
			}
		}
		Class456 class456 = class521_sub1_sub1_sub2.aClass456_10338;
		do {
			if (class456.method7564(1060677875)) {
				Class518 class518 = class456.method7565(-1632742162);
				if (3 == -1113882773 * class518.anInt5920) {
					if (41504957 * class521_sub1_sub1_sub2.anInt10367 > 0 && (403949281 * class521_sub1_sub1_sub2.anInt10342 <= client.anInt7174 * -1809259861) && (class521_sub1_sub1_sub2.anInt10345 * 1277328401 < -1809259861 * client.anInt7174)) {
						class521_sub1_sub1_sub2.anIntArray10350 = null;
						class456.method7567(-1, (short) 8960);
						break;
					}
				} else if (-1113882773 * class518.anInt5920 == 1) {
					if (41504957 * class521_sub1_sub1_sub2.anInt10367 > 0 && (class521_sub1_sub1_sub2.anInt10342 * 403949281 <= client.anInt7174 * -1809259861) && (1277328401 * class521_sub1_sub1_sub2.anInt10345 < -1809259861 * client.anInt7174)) {
						class456.method7575(1, 667125413);
						break;
					}
					class456.method7575(0, -1023349234);
				}
				if (class456.method7627(1, 1985078512) && class456.method7580(1773255286)) {
					class521_sub1_sub1_sub2.anIntArray10350 = null;
					class456.method7567(-1, (short) 8960);
				}
			}
		} while (false);
		for (int i_5_ = 0; (i_5_ < class521_sub1_sub1_sub2.aClass456_Sub2_Sub1Array10354.length); i_5_++) {
			Class456_Sub2_Sub1 class456_sub2_sub1 = class521_sub1_sub1_sub2.aClass456_Sub2_Sub1Array10354[i_5_];
			if (null != class456_sub2_sub1) {
				if (-918880941 * class456_sub2_sub1.anInt10065 > 0)
					class456_sub2_sub1.anInt10065 -= 872304347;
				else if (class456_sub2_sub1.method7627(1, 1857072218) && class456_sub2_sub1.method7580(921988866))
					class521_sub1_sub1_sub2.aClass456_Sub2_Sub1Array10354[i_5_] = null;
			}
		}
	}

	public static void method2582(int i, int[] is, int i_6_) {
		if (i != -1 && Class456_Sub3.method12682(i, is, -1943227865)) {
			Class118[] class118s = Class468_Sub8.aClass98Array7889[i].aClass118Array998;
			Class202.method3337(class118s, 2088119296);
		}
	}

	public static void method2583(Class397 class397, int i, int i_7_, Class520 class520, Class521_Sub1_Sub2_Sub1 class521_sub1_sub2_sub1, int i_8_) {
		Class527 class527 = Class125.method2167(1132906159);
		((Class527) class527).aClass521_Sub1_Sub2_Sub1_7014 = class521_sub1_sub2_sub1;
		Class107.method1834(class397, i, i_7_, class527, (byte) 35);
		((Class527) class527).aClass521_Sub1_Sub2_Sub1_7014 = null;
	}

	static final void method2584(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_9_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_10_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 + 1]);
		Class515.method8862(i_9_, i_10_ >> 14 & 0x3fff, i_10_ & 0x3fff, false, -1732540658);
	}

	public static RuntimeException_Sub3 method2585(Throwable throwable, String string) {
		RuntimeException_Sub3 runtimeexception_sub3;
		if (throwable instanceof RuntimeException_Sub3) {
			runtimeexception_sub3 = (RuntimeException_Sub3) throwable;
			StringBuilder stringbuilder = new StringBuilder();
			RuntimeException_Sub3 runtimeexception_sub3_11_ = runtimeexception_sub3;
			((RuntimeException_Sub3) runtimeexception_sub3_11_).aString10461 = stringbuilder.append(((RuntimeException_Sub3) runtimeexception_sub3_11_).aString10461).append(' ').append(string).toString();
		} else
			runtimeexception_sub3 = new RuntimeException_Sub3(throwable, string);
		return runtimeexception_sub3;
	}
}
