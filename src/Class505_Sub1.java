
/* Class505_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import java.awt.Canvas;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeap;

import jaggl.OpenGL;

public class Class505_Sub1 extends Class505 {
    boolean aBool8309;
    static final float aFloat8310 = 0.35F;
    static final int anInt8311 = 0;
    boolean aBool8312;
    static final int anInt8313 = 2;
    static final int anInt8314 = 4;
    float aFloat8315;
    long aLong8316;
    Class152_Sub1 aClass152_Sub1_8317;
    static final int anInt8318 = 32993;
    static final int anInt8319 = 768;
    float aFloat8320;
    static final int anInt8321 = 5126;
    static float[] aFloatArray8322;
    static final int anInt8323 = 7;
    static final int anInt8324 = 128;
    static final int anInt8325 = -1;
    static final int anInt8326 = -2;
    static final int anInt8327 = 7681;
    static final int anInt8328 = 8448;
    static final int anInt8329 = 260;
    static final int anInt8330 = 34023;
    static final int anInt8331 = 34165;
    static final int anInt8332 = 34479;
    static final int anInt8333 = 34168;
    static final int anInt8334 = 5890;
    static final int anInt8335 = 34167;
    static final int anInt8336 = 34166;
    static final int anInt8337 = 770;
    boolean aBool8338;
    static final int anInt8339 = 100663296;
    static final int anInt8340 = 8;
    static final int anInt8341 = 2;
    boolean aBool8342;
    static final int anInt8343 = 1;
    boolean aBool8344;
    static final int anInt8345 = 3;
    static final int anInt8346 = 1;
    int anInt8347;
    Class384 aClass384_8348;
    int anInt8349 = 128;
    static final int anInt8350 = 55;
    static final int anInt8351 = 7;
    OpenGL anOpenGL8352;
    boolean aBool8353;
    static final int anInt8354 = 5121;
    float aFloat8355;
    Class146 aClass146_8356;
    Class170 aClass170_8357;
    int anInt8358;
    Class134 aClass134_8359 = new Class134();
    Class384 aClass384_8360 = new Class384();
    Class384 aClass384_8361 = new Class384();
    boolean aBool8362;
    Class164 aClass164_8363;
    float aFloat8364;
    boolean aBool8365;
    int anInt8366;
    boolean aBool8367;
    int anInt8368;
    Class473 aClass473_8369;
    int anInt8370;
    int anInt8371;
    int anInt8372;
    Class473 aClass473_8373;
    static final int anInt8374 = 32;
    Class473 aClass473_8375;
    Class473 aClass473_8376;
    Class473 aClass473_8377;
    int anInt8378;
    Class473 aClass473_8379;
    Class384 aClass384_8380;
    static int[] anIntArray8381 = new int[1000];
    int anInt8382;
    static final int anInt8383 = 2;
    int anInt8384;
    Class384 aClass384_8385;
    static final int anInt8386 = 2;
    boolean aBool8387;
    boolean aBool8388;
    Class294 aClass294_8389;
    static final int anInt8390 = 0;
    Class384 aClass384_8391;
    static final int anInt8392 = 0;
    boolean aBool8393;
    Class384 aClass384_8394;
    float[][] aFloatArrayArray8395;
    float[] aFloatArray8396;
    boolean aBool8397;
    float aFloat8398;
    float aFloat8399;
    float aFloat8400;
    boolean aBool8401;
    float aFloat8402;
    float aFloat8403;
    float aFloat8404;
    Class294 aClass294_8405;
    float aFloat8406;
    Class384 aClass384_8407;
    boolean aBool8408;
    int anInt8409;
    int anInt8410;
    static final int anInt8411 = 1;
    int anInt8412;
    int anInt8413;
    float aFloat8414;
    int anInt8415;
    int anInt8416;
    int anInt8417;
    int anInt8418;
    int anInt8419;
    static final int anInt8420 = 4;
    static int anInt8421 = 4;
    boolean aBool8422;
    Class90 aClass90_8423;
    float[] aFloatArray8424;
    float[] aFloatArray8425;
    float[] aFloatArray8426;
    float[] aFloatArray8427;
    int anInt8428;
    float aFloat8429;
    float aFloat8430;
    float aFloat8431;
    float aFloat8432;
    float aFloat8433;
    float aFloat8434;
    Class282_Sub24[] aClass282_Sub24Array8435;
    int anInt8436;
    int anInt8437;
    int anInt8438;
    int anInt8439;
    boolean aBool8440;
    int anInt8441;
    Class384 aClass384_8442;
    int anInt8443;
    Class282_Sub5_Sub1 aClass282_Sub5_Sub1_8444;
    NativeHeap aNativeHeap8445;
    float aFloat8446;
    static final int anInt8447 = 16;
    boolean aBool8448;
    boolean aBool8449;
    int anInt8450;
    int anInt8451;
    Interface15 anInterface15_8452;
    int anInt8453;
    int anInt8454;
    int anInt8455;
    boolean aBool8456;
    boolean aBool8457;
    int anInt8458;
    boolean aBool8459;
    Class137_Sub1 aClass137_Sub1_8460;
    Class473 aClass473_8461;
    Class137_Sub1_Sub1 aClass137_Sub1_Sub1_8462;
    String aString8463;
    String aString8464;
    boolean aBool8465;
    int anInt8466;
    boolean aBool8467;
    static final int anInt8468 = 5123;
    int anInt8469;
    int anInt8470;
    int anInt8471;
    boolean aBool8472;
    int anInt8473;
    static final int anInt8474 = 1;
    int anInt8475;
    Class294 aClass294_8476 = new Class294();
    float[] aFloatArray8477;
    int anInt8478;
    Class528_Sub1[] aClass528_Sub1Array8479;
    boolean aBool8480;
    Class167 aClass167_8481;
    Class137[] aClass137Array8482;
    Interface14 anInterface14_8483;
    boolean aBool8484;
    boolean aBool8485;
    Class473 aClass473_8486;
    boolean aBool8487;
    boolean aBool8488;
    float aFloat8489;
    static final int anInt8490 = 8;
    static final int anInt8491 = 4;
    Class528_Sub1[] aClass528_Sub1Array8492;
    Class158_Sub1_Sub4 aClass158_Sub1_Sub4_8493;
    Class143 aClass143_8494;
    Class143 aClass143_8495;
    Interface14 anInterface14_8496;
    static float[] aFloatArray8497 = new float[4];
    boolean aBool8498;
    Class282_Sub35_Sub1 aClass282_Sub35_Sub1_8499;
    int[] anIntArray8500;
    int[] anIntArray8501;
    int[] anIntArray8502;
    byte[] aByteArray8503;

    final void method13570() {
		if (aClass158_5853 != null && (((Class505_Sub1)this).anInt8413 < ((Class505_Sub1)this).anInt8412) && (((Class505_Sub1)this).anInt8415 < ((Class505_Sub1)this).anInt8478)) {
			OpenGL.glScissor((((Class505_Sub1)this).anInt8453 + ((Class505_Sub1)this).anInt8413), (((Class505_Sub1)this).anInt8416 + aClass158_5853.method2716() - ((Class505_Sub1)this).anInt8478), (((Class505_Sub1)this).anInt8412 - ((Class505_Sub1)this).anInt8413), (((Class505_Sub1)this).anInt8478 - ((Class505_Sub1)this).anInt8415));
		} else {
			OpenGL.glScissor(0, 0, 0, 0);
		}
    }

    void method13571() {
        int i = 0;
        while (!((Class505_Sub1)this).anOpenGL8352.method2570()) {
			if (i++ > 5) {
				throw new RuntimeException("");
			}
            Class89.method1504(1000L);
        }
    }

    public void method8507() {
        if (((Class505_Sub1)this).aBool8487 && aClass158_5853 != null) {
            int i = ((Class505_Sub1)this).anInt8413;
            int i_0_ = ((Class505_Sub1)this).anInt8412;
            int i_1_ = ((Class505_Sub1)this).anInt8415;
            int i_2_ = ((Class505_Sub1)this).anInt8478;
            L();
            int i_3_ = ((Class505_Sub1)this).anInt8417;
            int i_4_ = ((Class505_Sub1)this).anInt8418;
            int i_5_ = ((Class505_Sub1)this).anInt8419;
            int i_6_ = ((Class505_Sub1)this).anInt8347;
            method8421();
            OpenGL.glReadBuffer(1028);
            OpenGL.glDrawBuffer(1029);
            method13586();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13654(null);
            method13581(-2);
            method13612(1);
            method13624(0);
            OpenGL.glMatrixMode(5889);
            OpenGL.glLoadIdentity();
            OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
            OpenGL.glMatrixMode(5888);
            OpenGL.glLoadIdentity();
            OpenGL.glRasterPos2i(0, 0);
            OpenGL.glCopyPixels(0, 0, aClass158_5853.method2714(), aClass158_5853.method2716(), 6144);
            OpenGL.glFlush();
            OpenGL.glReadBuffer(1029);
            OpenGL.glDrawBuffer(1029);
            r(i, i_1_, i_0_, i_2_);
            method8617(i_3_, i_4_, i_5_, i_6_);
        }
    }

    void method13572() {
        ((Class505_Sub1)this).aClass137Array8482 = new Class137[((Class505_Sub1)this).anInt8469];
        ((Class505_Sub1)this).aClass137_Sub1_8460 = new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        for (int i = 0; i < 7; i++) {
            ((Class505_Sub1)this).aClass528_Sub1Array8479[i] = new Class528_Sub1(this);
            ((Class505_Sub1)this).aClass528_Sub1Array8492[i] = new Class528_Sub1(this);
        }
        if (((Class505_Sub1)this).aBool8472) {
            ((Class505_Sub1)this).aClass158_Sub1_Sub4_8493 = new Class158_Sub1_Sub4(this);
            new Class158_Sub1_Sub4(this);
        }
    }

    void method13573() {
        method13581(-2);
        for (int i = ((Class505_Sub1)this).anInt8469 - 1; i >= 0; i--) {
            method13610(i);
            method13654(null);
            OpenGL.glTexEnvi(8960, 8704, 34160);
        }
        method13717(8448, 8448);
        method13595(2, 34168, 770);
        method13618();
        ((Class505_Sub1)this).anInt8366 = 1;
        OpenGL.glEnable(3042);
        OpenGL.glBlendFunc(770, 771);
        ((Class505_Sub1)this).anInt8384 = 1;
        OpenGL.glEnable(3008);
        OpenGL.glAlphaFunc(516, 0.0F);
        ((Class505_Sub1)this).aBool8408 = true;
        OpenGL.glColorMask(true, true, true, true);
        ((Class505_Sub1)this).aBool8459 = true;
        method13642(true);
        method13620(true);
        method13656(true);
        method13623(true);
        GA(0.0F, 1.0F);
        method13586();
        ((Class505_Sub1)this).anOpenGL8352.setSwapInterval(0);
        OpenGL.glShadeModel(7425);
        OpenGL.glClearDepth(1.0F);
        OpenGL.glDepthFunc(515);
        OpenGL.glPolygonMode(1028, 6914);
        OpenGL.glEnable(2884);
        OpenGL.glCullFace(1029);
        OpenGL.glMatrixMode(5888);
        OpenGL.glLoadIdentity();
        OpenGL.glColorMaterial(1028, 5634);
        OpenGL.glEnable(2903);
        float[] fs = {0.0F, 0.0F, 0.0F, 1.0F};
        for (int i = 0; i < 8; i++) {
            int i_7_ = 16384 + i;
            OpenGL.glLightfv(i_7_, 4608, fs, 0);
            OpenGL.glLightf(i_7_, 4615, 0.0F);
            OpenGL.glLightf(i_7_, 4616, 0.0F);
        }
        OpenGL.glEnable(16384);
        OpenGL.glEnable(16385);
        OpenGL.glFogf(2914, 0.95F);
        OpenGL.glFogi(2917, 9729);
        OpenGL.glHint(3156, 4353);
        ((Class505_Sub1)this).anInt8441 = -1;
        ((Class505_Sub1)this).anInt8428 = -1;
        method8421();
        L();
    }

    final void method13574() {
        if (((Class505_Sub1)this).anInt8382 != 2) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13581(-2);
            ((Class505_Sub1)this).anInt8382 = 2;
        }
    }

    void method8555(int i, int i_8_) throws Exception_Sub3 {
        try {
            aClass158_Sub2_5841.method14344();
        } catch (Exception exception) {
            /* empty */
        }
		if (anInterface22_5834 != null) {
			anInterface22_5834.method161(-1871804143);
		}
    }

    public void method8395() {
        OpenGL.glFinish();
    }

    void method8397() {
		for (Class282 class282 = ((Class505_Sub1)this).aClass473_8369.method7859(525152513); class282 != null; class282 = ((Class505_Sub1)this).aClass473_8369.method7857((byte)-115)) {
			((Class282_Sub1_Sub1)class282).method15450();
		}
		if (((Class505_Sub1)this).aClass170_8357 != null) {
			((Class505_Sub1)this).aClass170_8357.method2898();
		}
        if (((Class505_Sub1)this).aBool8367) {
            Class13.method508(false, true, -600309245);
            ((Class505_Sub1)this).aBool8367 = false;
        }
    }

    final void method13575() {
        if (((Class505_Sub1)this).anInt8382 != 8) {
            method13645();
            method13642(true);
            method13656(true);
            method13623(true);
            method13624(1);
            ((Class505_Sub1)this).anInt8382 = 8;
        }
    }

    public boolean method8465() {
        return true;
    }

    public boolean method8664() {
        return true;
    }

    public boolean method8403() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && (((Class505_Sub1)this).anInt8475 <= 1 || ((Class505_Sub1)this).aBool8344));
    }

    public boolean method8404() {
        return true;
    }

    final void method13576() {
        OpenGL.glPushMatrix();
    }

    public void fs(int i, int i_9_, int i_10_, int i_11_, int i_12_) {
        method13659();
        method13624(i_12_);
        float f = (float)i + 0.35F;
        float f_13_ = (float)i_9_ + 0.35F;
        OpenGL.glColor4ub((byte)(i_11_ >> 16), (byte)(i_11_ >> 8), (byte)i_11_, (byte)(i_11_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f(f, f_13_);
        OpenGL.glVertex2f(f + (float)i_10_, f_13_);
        OpenGL.glEnd();
    }

    public Class160 method8604(int i, int i_14_, int i_15_, int i_16_, boolean bool) {
        return new Class160_Sub2(this, i, i_14_, i_15_, i_16_);
    }

    void method13577() {
        int i = aClass158_5853.method2714();
        int i_17_ = aClass158_5853.method2716();
        ((Class505_Sub1)this).aClass384_8385.method6530(0.0F, (float)i, 0.0F, (float)i_17_, -1.0F, 1.0F);
        method8421();
        method13586();
        L();
    }

    public boolean method8454() {
        return true;
    }

    Class505_Sub1(Canvas canvas, Interface22 interface22, int i) {
        super(interface22);
        ((Class505_Sub1)this).anInt8466 = 8;
        ((Class505_Sub1)this).anInt8473 = 3;
        ((Class505_Sub1)this).aBool8367 = false;
        ((Class505_Sub1)this).aClass473_8369 = new Class473();
        ((Class505_Sub1)this).aClass473_8373 = new Class473();
        ((Class505_Sub1)this).aClass473_8486 = new Class473();
        ((Class505_Sub1)this).aClass473_8375 = new Class473();
        ((Class505_Sub1)this).aClass473_8376 = new Class473();
        ((Class505_Sub1)this).aClass473_8377 = new Class473();
        ((Class505_Sub1)this).aClass473_8461 = new Class473();
        ((Class505_Sub1)this).aClass473_8379 = new Class473();
        ((Class505_Sub1)this).aClass294_8389 = new Class294();
        ((Class505_Sub1)this).aClass384_8348 = new Class384();
        ((Class505_Sub1)this).aClass384_8391 = new Class384();
        ((Class505_Sub1)this).aClass384_8442 = new Class384();
        ((Class505_Sub1)this).aClass384_8385 = new Class384();
        ((Class505_Sub1)this).aClass384_8394 = new Class384();
        ((Class505_Sub1)this).aFloatArrayArray8395 = new float[6][4];
        ((Class505_Sub1)this).aFloatArray8396 = new float[4];
        ((Class505_Sub1)this).aFloat8364 = 0.0F;
        ((Class505_Sub1)this).aFloat8320 = 1.0F;
        ((Class505_Sub1)this).aFloat8403 = 0.0F;
        ((Class505_Sub1)this).aFloat8404 = -1.0F;
        ((Class505_Sub1)this).aClass294_8405 = new Class294();
        ((Class505_Sub1)this).aClass384_8380 = new Class384();
        ((Class505_Sub1)this).aClass384_8407 = new Class384();
        ((Class505_Sub1)this).aFloatArray8477 = new float[16];
        ((Class505_Sub1)this).aBool8353 = true;
        ((Class505_Sub1)this).anInt8415 = 0;
        ((Class505_Sub1)this).anInt8478 = 0;
        ((Class505_Sub1)this).anInt8413 = 0;
        ((Class505_Sub1)this).anInt8412 = 0;
        ((Class505_Sub1)this).anInt8453 = 0;
        ((Class505_Sub1)this).anInt8416 = 0;
        ((Class505_Sub1)this).aFloatArray8424 = new float[4];
        ((Class505_Sub1)this).aFloatArray8425 = new float[4];
        ((Class505_Sub1)this).aFloatArray8426 = new float[4];
        ((Class505_Sub1)this).aFloatArray8427 = new float[4];
        ((Class505_Sub1)this).anInt8428 = -1;
        ((Class505_Sub1)this).aFloat8429 = 1.0F;
        ((Class505_Sub1)this).aFloat8430 = 1.0F;
        ((Class505_Sub1)this).aFloat8431 = 1.0F;
        ((Class505_Sub1)this).aFloat8433 = -1.0F;
        ((Class505_Sub1)this).aFloat8434 = -1.0F;
        ((Class505_Sub1)this).aClass282_Sub24Array8435 = new Class282_Sub24[anInt8421];
        ((Class505_Sub1)this).anInt8441 = -1;
        ((Class505_Sub1)this).anInt8358 = -1;
        ((Class505_Sub1)this).anInt8378 = 0;
        ((Class505_Sub1)this).aFloat8446 = 1.0F;
        ((Class505_Sub1)this).aFloat8355 = 0.0F;
        ((Class505_Sub1)this).aBool8449 = false;
        ((Class505_Sub1)this).anInt8455 = 8448;
        ((Class505_Sub1)this).anInt8451 = 8448;
        ((Class505_Sub1)this).aFloat8489 = -1.0F;
        ((Class505_Sub1)this).aFloat8414 = -1.0F;
        ((Class505_Sub1)this).aClass528_Sub1Array8479 = new Class528_Sub1[7];
        ((Class505_Sub1)this).aClass528_Sub1Array8492 = new Class528_Sub1[7];
        ((Class505_Sub1)this).aClass282_Sub35_Sub1_8499 = new Class282_Sub35_Sub1(8192);
        ((Class505_Sub1)this).anIntArray8500 = new int[1];
        ((Class505_Sub1)this).anIntArray8501 = new int[1];
        ((Class505_Sub1)this).anIntArray8502 = new int[1];
        ((Class505_Sub1)this).aByteArray8503 = new byte[16384];
        try {
            ((Class505_Sub1)this).anInt8475 = i;
			if (!Class362.method6278(616047582).method222("jaclib", 946095611)) {
				throw new RuntimeException("");
			}
			if (!Class362.method6278(616047582).method222("jaggl", 560880433)) {
				throw new RuntimeException("");
			}
            ((Class505_Sub1)this).anOpenGL8352 = new OpenGL();
            long l = ((Class505_Sub1)this).anOpenGL8352.init(canvas, 8, 8, 8, 24, 0, ((Class505_Sub1)this).anInt8475);
			if (l == 0L) {
				throw new RuntimeException("");
			}
            method13571();
            int i_18_ = method13652();
			if (i_18_ != 0) {
				throw new RuntimeException("");
			}
            ((Class505_Sub1)this).anInt8410 = ((Class505_Sub1)this).aBool8467 ? 33639 : 5121;
            if (((Class505_Sub1)this).aString8464.indexOf("radeon") != -1) {
                int i_19_ = 0;
                boolean bool = false;
                boolean bool_20_ = false;
                String[] strings = Class456_Sub3.method12681(((Class505_Sub1)this).aString8464.replace('/', ' '), ' ', 229848533);
                for (int i_21_ = 0; i_21_ < strings.length; i_21_++) {
                    String string = strings[i_21_];
                    try {
						if (string.length() <= 0) {
							continue;
						}
                        if (string.charAt(0) == 'x' && string.length() >= 3 && Class115.method1950(string.substring(1, 3), -2077836212)) {
                            string = string.substring(1);
                            bool_20_ = true;
                        }
                        if (string.equals("hd")) {
                            bool = true;
                            continue;
                        }
                        if (string.startsWith("hd")) {
                            string = string.substring(2);
                            bool = true;
                        }
						if (string.length() < 4 || !Class115.method1950(string.substring(0, 4), 814122152)) {
							continue;
						}
                        i_19_ = Class328.method5830(string.substring(0, 4), -1887024388);
                    } catch (Exception exception) {
                        continue;
                    }
                    break;
                }
                if (!bool_20_ && !bool) {
					if (i_19_ >= 7000 && i_19_ <= 7999) {
						((Class505_Sub1)this).aBool8309 = false;
					}
					if (i_19_ >= 7000 && i_19_ <= 9250) {
						((Class505_Sub1)this).aBool8393 = false;
					}
                }
				if (!bool || i_19_ < 4000) {
					((Class505_Sub1)this).aBool8312 = false;
				}
                ((Class505_Sub1)this).aBool8401 &= ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_half_float_pixel");
                ((Class505_Sub1)this).aBool8362 = ((Class505_Sub1)this).aBool8309;
                ((Class505_Sub1)this).aBool8465 = true;
            }
			if (((Class505_Sub1)this).aString8463.indexOf("intel") != -1) {
				((Class505_Sub1)this).aBool8472 = false;
			}
            ((Class505_Sub1)this).aBool8487 = !((Class505_Sub1)this).aString8463.equals("s3 graphics");
            if (((Class505_Sub1)this).aBool8309) {
                try {
                    int[] is = new int[1];
                    OpenGL.glGenBuffersARB(1, is, 0);
                } catch (Throwable throwable) {
                    throw new RuntimeException("");
                }
            }
            Class38.method854(false, true, -930172525);
            ((Class505_Sub1)this).aBool8367 = true;
            ((Class505_Sub1)this).aClass167_8481 = new Class167(this, anInterface22_5834);
            method13572();
            ((Class505_Sub1)this).aClass164_8363 = new Class164(this);
            ((Class505_Sub1)this).aClass170_8357 = new Class170(this);
            if (((Class505_Sub1)this).aClass170_8357.method2896()) {
                ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 = new Class282_Sub5_Sub1(this);
                if (!((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method15445()) {
                    ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12120();
                    ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 = null;
                }
            }
            method8411(canvas, new Class158_Sub2_Sub1(this, canvas, l), 316223525);
            method8412(canvas, (byte)8);
            ((Class505_Sub1)this).aClass146_8356 = new Class146(this);
            method13573();
            method8420();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            method8396(-219989635);
			if (throwable instanceof OutOfMemoryError) {
				throw (OutOfMemoryError)throwable;
			}
            throw new RuntimeException("");
        }
    }

    public void eq(boolean bool) {
        ((Class505_Sub1)this).aBool8353 = bool;
        method13579();
    }

    void method13578() {
        method13639();
    }

    void method8531() {
		for (Class282 class282 = ((Class505_Sub1)this).aClass473_8369.method7859(1244123639); class282 != null; class282 = ((Class505_Sub1)this).aClass473_8369.method7857((byte)-114)) {
			((Class282_Sub1_Sub1)class282).method15450();
		}
		if (((Class505_Sub1)this).aClass170_8357 != null) {
			((Class505_Sub1)this).aClass170_8357.method2898();
		}
        if (((Class505_Sub1)this).aBool8367) {
            Class13.method508(false, true, 912496616);
            ((Class505_Sub1)this).aBool8367 = false;
        }
    }

    public int[] ab(int i, int i_22_, int i_23_, int i_24_) {
        if (aClass158_5853 != null) {
            int[] is = new int[i_23_ * i_24_];
            int i_25_ = aClass158_5853.method2716();
			for (int i_26_ = 0; i_26_ < i_24_; i_26_++) {
				OpenGL.glReadPixelsi(i, i_25_ - i_22_ - i_26_ - 1, i_23_, 1, 32993, ((Class505_Sub1)this).anInt8410, is, i_26_ * i_23_);
			}
            return is;
        }
        return null;
    }

    public void method8420() {
        if (((Class505_Sub1)this).aBool8487 && aClass158_5853 != null) {
            int i = ((Class505_Sub1)this).anInt8413;
            int i_27_ = ((Class505_Sub1)this).anInt8412;
            int i_28_ = ((Class505_Sub1)this).anInt8415;
            int i_29_ = ((Class505_Sub1)this).anInt8478;
            L();
            int i_30_ = ((Class505_Sub1)this).anInt8417;
            int i_31_ = ((Class505_Sub1)this).anInt8418;
            int i_32_ = ((Class505_Sub1)this).anInt8419;
            int i_33_ = ((Class505_Sub1)this).anInt8347;
            method8421();
            OpenGL.glReadBuffer(1028);
            OpenGL.glDrawBuffer(1029);
            method13586();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13654(null);
            method13581(-2);
            method13612(1);
            method13624(0);
            OpenGL.glMatrixMode(5889);
            OpenGL.glLoadIdentity();
            OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
            OpenGL.glMatrixMode(5888);
            OpenGL.glLoadIdentity();
            OpenGL.glRasterPos2i(0, 0);
            OpenGL.glCopyPixels(0, 0, aClass158_5853.method2714(), aClass158_5853.method2716(), 6144);
            OpenGL.glFlush();
            OpenGL.glReadBuffer(1029);
            OpenGL.glDrawBuffer(1029);
            r(i, i_28_, i_27_, i_29_);
            method8617(i_30_, i_31_, i_32_, i_33_);
        }
    }

    public void ba(int i, int i_34_) {
        int i_35_ = 0;
        if ((i & 0x1) != 0) {
            OpenGL.glClearColor((float)(i_34_ & 0xff0000) / 1.671168E7F, (float)(i_34_ & 0xff00) / 65280.0F, (float)(i_34_ & 0xff) / 255.0F, (float)(i_34_ >>> 24) / 255.0F);
            i_35_ = 16384;
        }
        if ((i & 0x2) != 0) {
            method13623(true);
            i_35_ |= 0x100;
        }
		if ((i & 0x4) != 0) {
			i_35_ |= 0x400;
		}
        OpenGL.glClear(i_35_);
    }

    public void method8430(int i, int i_36_, int i_37_, int i_38_, int i_39_, int i_40_) {
        float f = (float)i + 0.35F;
        float f_41_ = (float)i_36_ + 0.35F;
        float f_42_ = f + (float)i_37_ - 1.0F;
        float f_43_ = f_41_ + (float)i_38_ - 1.0F;
        method13659();
        method13624(i_40_);
        OpenGL.glColor4ub((byte)(i_39_ >> 16), (byte)(i_39_ >> 8), (byte)i_39_, (byte)(i_39_ >> 24));
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glDisable(32925);
		}
        OpenGL.glBegin(2);
        OpenGL.glVertex2f(f, f_41_);
        OpenGL.glVertex2f(f, f_43_);
        OpenGL.glVertex2f(f_42_, f_43_);
        OpenGL.glVertex2f(f_42_, f_41_);
        OpenGL.glEnd();
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glEnable(32925);
		}
    }

    public void B(int i, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_) {
        float f = (float)i + 0.35F;
        float f_49_ = (float)i_44_ + 0.35F;
        float f_50_ = f + (float)i_45_;
        float f_51_ = f_49_ + (float)i_46_;
        method13659();
        method13624(i_48_);
        OpenGL.glColor4ub((byte)(i_47_ >> 16), (byte)(i_47_ >> 8), (byte)i_47_, (byte)(i_47_ >> 24));
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glDisable(32925);
		}
        OpenGL.glBegin(7);
        OpenGL.glVertex2f(f, f_49_);
        OpenGL.glVertex2f(f, f_51_);
        OpenGL.glVertex2f(f_50_, f_51_);
        OpenGL.glVertex2f(f_50_, f_49_);
        OpenGL.glEnd();
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glEnable(32925);
		}
    }

    final void method13579() {
        OpenGL.glDepthMask(((Class505_Sub1)this).aBool8388 && ((Class505_Sub1)this).aBool8353);
    }

    public void method8658(int i, int i_52_, float f, int i_53_, int i_54_, float f_55_, int i_56_, int i_57_, float f_58_, int i_59_, int i_60_, int i_61_, int i_62_) {
        method13659();
        method13624(i_62_);
        OpenGL.glBegin(4);
        OpenGL.glColor4ub((byte)(i_59_ >> 16), (byte)(i_59_ >> 8), (byte)i_59_, (byte)(i_59_ >> 24));
        OpenGL.glVertex3f((float)i + 0.35F, (float)i_52_ + 0.35F, f);
        OpenGL.glColor4ub((byte)(i_60_ >> 16), (byte)(i_60_ >> 8), (byte)i_60_, (byte)(i_60_ >> 24));
        OpenGL.glVertex3f((float)i_53_ + 0.35F, (float)i_54_ + 0.35F, f_55_);
        OpenGL.glColor4ub((byte)(i_61_ >> 16), (byte)(i_61_ >> 8), (byte)i_61_, (byte)(i_61_ >> 24));
        OpenGL.glVertex3f((float)i_56_ + 0.35F, (float)i_57_ + 0.35F, f_58_);
        OpenGL.glEnd();
    }

    public void RA(boolean bool) {
        ((Class505_Sub1)this).aBool8353 = bool;
        method13579();
    }

    final void method13580(Interface15 interface15, int i, int i_63_, int i_64_) {
        method13663(interface15);
        OpenGL.glDrawElements(i, i_64_, 5123, interface15.method2() + (long)(i_63_ * 2));
    }

    public final void method8446(Class384 class384) {
        ((Class505_Sub1)this).aClass384_8442.method6562(class384);
        method13597();
        method13588();
    }

    final void method13581(int i) {
        method13608(i, true);
    }

    public void method8435(int i, int i_65_, int i_66_, int i_67_, int i_68_, int i_69_, int i_70_, int i_71_, int i_72_) {
        if (i != i_66_ || i_65_ != i_67_) {
            method13659();
            method13624(i_69_);
            float f = (float)i_66_ - (float)i;
            float f_73_ = (float)i_67_ - (float)i_65_;
            float f_74_ = (float)(1.0 / Math.sqrt((double)(f * f + f_73_ * f_73_)));
            f *= f_74_;
            f_73_ *= f_74_;
            OpenGL.glColor4ub((byte)(i_68_ >> 16), (byte)(i_68_ >> 8), (byte)i_68_, (byte)(i_68_ >> 24));
            i_72_ %= i_71_ + i_70_;
            float f_75_ = f * (float)i_70_;
            float f_76_ = f_73_ * (float)i_70_;
            float f_77_ = 0.0F;
            float f_78_ = 0.0F;
            float f_79_ = f_75_;
            float f_80_ = f_76_;
            if (i_72_ > i_70_) {
                f_77_ = f * (float)(i_70_ + i_71_ - i_72_);
                f_78_ = f_73_ * (float)(i_70_ + i_71_ - i_72_);
            } else {
                f_79_ = f * (float)(i_70_ - i_72_);
                f_80_ = f_73_ * (float)(i_70_ - i_72_);
            }
            float f_81_ = (float)i + 0.35F + f_77_;
            float f_82_ = (float)i_65_ + 0.35F + f_78_;
            float f_83_ = f * (float)i_71_;
            float f_84_ = f_73_ * (float)i_71_;
            for (; ; ) {
                if (i_66_ > i) {
					if (f_81_ > (float)i_66_ + 0.35F) {
						break;
					}
					if (f_81_ + f_79_ > (float)i_66_) {
						f_79_ = (float)i_66_ - f_81_;
					}
                } else {
					if (f_81_ < (float)i_66_ + 0.35F) {
						break;
					}
					if (f_81_ + f_79_ < (float)i_66_) {
						f_79_ = (float)i_66_ - f_81_;
					}
                }
                if (i_67_ > i_65_) {
					if (f_82_ > (float)i_67_ + 0.35F) {
						break;
					}
					if (f_82_ + f_80_ > (float)i_67_) {
						f_80_ = (float)i_67_ - f_82_;
					}
                } else {
					if (f_82_ < (float)i_67_ + 0.35F) {
						break;
					}
					if (f_82_ + f_80_ < (float)i_67_) {
						f_80_ = (float)i_67_ - f_82_;
					}
                }
                OpenGL.glBegin(1);
                OpenGL.glVertex2f(f_81_, f_82_);
                OpenGL.glVertex2f(f_81_ + f_79_, f_82_ + f_80_);
                OpenGL.glEnd();
                f_81_ += f_83_ + f_79_;
                f_82_ += f_84_ + f_80_;
                f_79_ = f_75_;
                f_80_ = f_76_;
            }
        }
    }

    public void method8669(int i, int i_85_, int i_86_, int i_87_, int i_88_, int i_89_, Class455 class455, int i_90_, int i_91_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(i_89_);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_92_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        float f_93_ = (float)i_86_ - (float)i;
        float f_94_ = (float)i_87_ - (float)i_85_;
        float f_95_ = (float)(1.0 / Math.sqrt((double)(f_93_ * f_93_ + f_94_ * f_94_)));
        f_93_ *= f_95_;
        f_94_ *= f_95_;
        OpenGL.glColor4ub((byte)(i_88_ >> 16), (byte)(i_88_ >> 8), (byte)i_88_, (byte)(i_88_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glTexCoord2f(f * (float)(i - i_90_), f_92_ * (float)(i_85_ - i_91_));
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_85_ + 0.35F);
        OpenGL.glTexCoord2f(f * (float)(i_86_ - i_90_), f_92_ * (float)(i_87_ - i_91_));
        OpenGL.glVertex2f((float)i_86_ + f_93_ + 0.35F, (float)i_87_ + f_94_ + 0.35F);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    public void method8563(int i, int i_96_, int i_97_, int i_98_, int i_99_, int i_100_, Class455 class455, int i_101_, int i_102_, int i_103_, int i_104_, int i_105_) {
        if (i != i_97_ || i_96_ != i_98_) {
            Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
            Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
            method13637();
            method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
            method13624(i_100_);
            method13717(7681, 8448);
            method13595(0, 34167, 768);
            float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)(((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136));
            float f_106_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)(((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133));
            float f_107_ = (float)i_97_ - (float)i;
            float f_108_ = (float)i_98_ - (float)i_96_;
            float f_109_ = (float)(1.0 / Math.sqrt((double)(f_107_ * f_107_ + f_108_ * f_108_)));
            f_107_ *= f_109_;
            f_108_ *= f_109_;
            OpenGL.glColor4ub((byte)(i_99_ >> 16), (byte)(i_99_ >> 8), (byte)i_99_, (byte)(i_99_ >> 24));
            i_105_ %= i_104_ + i_103_;
            float f_110_ = f_107_ * (float)i_103_;
            float f_111_ = f_108_ * (float)i_103_;
            float f_112_ = 0.0F;
            float f_113_ = 0.0F;
            float f_114_ = f_110_;
            float f_115_ = f_111_;
            if (i_105_ > i_103_) {
                f_112_ = f_107_ * (float)(i_103_ + i_104_ - i_105_);
                f_113_ = f_108_ * (float)(i_103_ + i_104_ - i_105_);
            } else {
                f_114_ = f_107_ * (float)(i_103_ - i_105_);
                f_115_ = f_108_ * (float)(i_103_ - i_105_);
            }
            float f_116_ = (float)i + 0.35F + f_112_;
            float f_117_ = (float)i_96_ + 0.35F + f_113_;
            float f_118_ = f_107_ * (float)i_104_;
            float f_119_ = f_108_ * (float)i_104_;
            for (; ; ) {
                if (i_97_ > i) {
					if (f_116_ > (float)i_97_ + 0.35F) {
						break;
					}
					if (f_116_ + f_114_ > (float)i_97_) {
						f_114_ = (float)i_97_ - f_116_;
					}
                } else {
					if (f_116_ < (float)i_97_ + 0.35F) {
						break;
					}
					if (f_116_ + f_114_ < (float)i_97_) {
						f_114_ = (float)i_97_ - f_116_;
					}
                }
                if (i_98_ > i_96_) {
					if (f_117_ > (float)i_98_ + 0.35F) {
						break;
					}
					if (f_117_ + f_115_ > (float)i_98_) {
						f_115_ = (float)i_98_ - f_117_;
					}
                } else {
					if (f_117_ < (float)i_98_ + 0.35F) {
						break;
					}
					if (f_117_ + f_115_ < (float)i_98_) {
						f_115_ = (float)i_98_ - f_117_;
					}
                }
                OpenGL.glBegin(1);
                OpenGL.glTexCoord2f(f * (f_116_ - (float)i_101_), f_106_ * (f_117_ - (float)i_102_));
                OpenGL.glVertex2f(f_116_, f_117_);
                OpenGL.glTexCoord2f(f * (f_116_ + f_114_ - (float)i_101_), f_106_ * (f_117_ + f_115_ - (float)i_102_));
                OpenGL.glVertex2f(f_116_ + f_114_, f_117_ + f_115_);
                OpenGL.glEnd();
                f_116_ += f_118_ + f_114_;
                f_117_ += f_119_ + f_115_;
                f_114_ = f_110_;
                f_115_ = f_111_;
            }
            method13595(0, 5890, 768);
        }
    }

    public void method8496(int i, int i_120_, int i_121_, int i_122_, int i_123_, int i_124_, int i_125_) {
        OpenGL.glLineWidth((float)i_124_);
        method8433(i, i_120_, i_121_, i_122_, i_123_, i_125_);
        OpenGL.glLineWidth(1.0F);
    }

    public void method8479(float f, float f_126_, float f_127_, float[] fs) {
        float f_128_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * f_126_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * f_127_));
        float f_129_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * f_126_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * f_127_));
        float f_130_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * f_126_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * f_127_));
        float f_131_ = (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[14] + ((Class505_Sub1)this).aClass384_8348.aFloatArray4667[2] * f + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[6] * f_126_) + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[10] * f_127_));
        fs[0] = (((Class505_Sub1)this).aFloat8315 + ((Class505_Sub1)this).aFloat8398 * f_129_ / f_128_);
        fs[1] = (((Class505_Sub1)this).aFloat8399 + ((Class505_Sub1)this).aFloat8400 * f_130_ / f_128_);
        fs[2] = f_131_;
    }

    public void method8515(float f, float f_132_, float f_133_, float[] fs) {
        float f_134_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * f_132_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * f_133_));
        float f_135_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * f_132_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * f_133_));
        if (f_134_ < -f_135_ || f_134_ > f_135_) {
            float[] fs_136_ = fs;
            float[] fs_137_ = fs;
            fs[2] = Float.NaN;
            fs_137_[1] = Float.NaN;
            fs_136_[0] = Float.NaN;
        } else {
            float f_138_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * f) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * f_132_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * f_133_));
            if (f_138_ < -f_135_ || f_138_ > f_135_) {
                float[] fs_139_ = fs;
                float[] fs_140_ = fs;
                fs[2] = Float.NaN;
                fs_140_[1] = Float.NaN;
                fs_139_[0] = Float.NaN;
            } else {
                float f_141_ = ((((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13]) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1]) * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5]) * f_132_ + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9]) * f_133_);
                if (f_141_ < -f_135_ || f_141_ > f_135_) {
                    float[] fs_142_ = fs;
                    float[] fs_143_ = fs;
                    fs[2] = Float.NaN;
                    fs_143_[1] = Float.NaN;
                    fs_142_[0] = Float.NaN;
                } else {
                    float f_144_ = ((((Class505_Sub1)this).aClass384_8348.aFloatArray4667[14]) + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[2]) * f + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[6]) * f_132_ + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[10]) * f_133_);
                    fs[0] = (((Class505_Sub1)this).aFloat8315 + (((Class505_Sub1)this).aFloat8398 * f_138_ / f_135_));
                    fs[1] = (((Class505_Sub1)this).aFloat8399 + (((Class505_Sub1)this).aFloat8400 * f_141_ / f_135_));
                    fs[2] = f_144_;
                }
            }
        }
    }

    public void method8433(int i, int i_145_, int i_146_, int i_147_, int i_148_, int i_149_) {
        method13659();
        method13624(i_149_);
        float f = (float)i_146_ - (float)i;
        float f_150_ = (float)i_147_ - (float)i_145_;
		if (f == 0.0F && f_150_ == 0.0F) {
			f = 1.0F;
		} else {
			float f_151_ = (float)(1.0 / Math.sqrt((double)(f * f + f_150_ * f_150_)));
			f *= f_151_;
			f_150_ *= f_151_;
		}
        OpenGL.glColor4ub((byte)(i_148_ >> 16), (byte)(i_148_ >> 8), (byte)i_148_, (byte)(i_148_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_145_ + 0.35F);
        OpenGL.glVertex2f((float)i_146_ + f + 0.35F, (float)i_147_ + f_150_ + 0.35F);
        OpenGL.glEnd();
    }

    public Class282_Sub1 method8438(int i) {
        Class282_Sub1_Sub1 class282_sub1_sub1 = new Class282_Sub1_Sub1(i);
        ((Class505_Sub1)this).aClass473_8369.method7877(class282_sub1_sub1, 111697985);
        return class282_sub1_sub1;
    }

    public void method8439(Class282_Sub1 class282_sub1) {
        ((Class505_Sub1)this).aNativeHeap8445 = (((Class282_Sub1_Sub1)(Class282_Sub1_Sub1)class282_sub1).aNativeHeap10051);
        if (((Class505_Sub1)this).anInterface14_8496 == null) {
            Class282_Sub35_Sub1 class282_sub35_sub1 = new Class282_Sub35_Sub1(80);
            if (((Class505_Sub1)this).aBool8467) {
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
            } else {
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
            }
            ((Class505_Sub1)this).anInterface14_8496 = method13599(20, class282_sub35_sub1.buffer, class282_sub35_sub1.index * -1990677291, false);
            ((Class505_Sub1)this).aClass143_8494 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 3, 0);
            ((Class505_Sub1)this).aClass143_8495 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 2, 12);
            ((Class505_Sub1)this).aClass134_8359.method2328(this);
        }
    }

    public Class160 method8654(int i, int i_152_, boolean bool, boolean bool_153_) {
        return new Class160_Sub2(this, i, i_152_, bool);
    }

    public Class160 method8444(Class91 class91, boolean bool) {
        int[] is = new int[class91.anInt957 * class91.anInt954];
        int i = 0;
        int i_154_ = 0;
        if (class91.aByteArray961 != null) {
            for (int i_155_ = 0; i_155_ < class91.anInt954; i_155_++) {
                for (int i_156_ = 0; i_156_ < class91.anInt957; i_156_++) {
                    is[i_154_++] = (class91.aByteArray961[i] << 24 | (class91.anIntArray955[class91.aByteArray960[i] & 0xff]));
                    i++;
                }
            }
        } else {
            for (int i_157_ = 0; i_157_ < class91.anInt954; i_157_++) {
                for (int i_158_ = 0; i_158_ < class91.anInt957; i_158_++) {
                    int i_159_ = (class91.anIntArray955[class91.aByteArray960[i++] & 0xff]);
                    is[i_154_++] = i_159_ != 0 ? ~0xffffff | i_159_ : 0;
                }
            }
        }
        Class160 class160 = method8549(is, 0, class91.anInt957, class91.anInt957, class91.anInt954, -1670906477);
        class160.method2743(class91.anInt956, class91.anInt959, class91.anInt958, class91.anInt953);
        return class160;
    }

    public Class160 method8442(int[] is, int i, int i_160_, int i_161_, int i_162_, boolean bool) {
        return new Class160_Sub2(this, i_161_, i_162_, is, i, i_160_);
    }

    public Class160 method8668(int i, int i_163_, int i_164_, int i_165_, boolean bool) {
        return new Class160_Sub2(this, i, i_163_, i_164_, i_165_);
    }

    public Class455 method8624(int i, int i_166_, int[] is, int[] is_167_) {
        return Class455_Sub2.method14343(this, i, i_166_, is, is_167_);
    }

    public void DA(int i, Class455 class455, int i_168_, int i_169_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(1);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_170_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        OpenGL.glColor4ub((byte)(i >> 16), (byte)(i >> 8), (byte)i, (byte)(i >> 24));
        OpenGL.glBegin(7);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_168_), f_170_ * (float)(((Class505_Sub1)this).anInt8415 - i_169_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8415);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_168_), f_170_ * (float)(((Class505_Sub1)this).anInt8478 - i_169_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_168_), f_170_ * (float)(((Class505_Sub1)this).anInt8478 - i_169_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_168_), f_170_ * (float)(((Class505_Sub1)this).anInt8415 - i_169_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8415);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    final void method13582(int i) {
		if (i == 1) {
			method13717(7681, 7681);
		} else if (i == 0) {
			method13717(8448, 8448);
		} else if (i == 2) {
			method13717(34165, 7681);
		} else if (i == 3) {
			method13717(260, 8448);
		} else if (i == 4) {
			method13717(34023, 34023);
		}
    }

    public void method8459(int i) {
        /* empty */
    }

    public Class528 method8451(Class157 class157, int i, int i_171_, int i_172_, int i_173_) {
        return new Class528_Sub1(this, class157, i, i_172_, i_173_, i_171_);
    }

    public int method8452(int i, int i_174_) {
        return i & i_174_ ^ i_174_;
    }

    public void fd(int i, int i_175_) {
        int i_176_ = 0;
        if ((i & 0x1) != 0) {
            OpenGL.glClearColor((float)(i_175_ & 0xff0000) / 1.671168E7F, (float)(i_175_ & 0xff00) / 65280.0F, (float)(i_175_ & 0xff) / 255.0F, (float)(i_175_ >>> 24) / 255.0F);
            i_176_ = 16384;
        }
        if ((i & 0x2) != 0) {
            method13623(true);
            i_176_ |= 0x100;
        }
		if ((i & 0x4) != 0) {
			i_176_ |= 0x400;
		}
        OpenGL.glClear(i_176_);
    }

    public final void c(int i, int i_177_, int i_178_) {
        if (((Class505_Sub1)this).anInt8441 != i || ((Class505_Sub1)this).anInt8358 != i_177_ || ((Class505_Sub1)this).anInt8378 != i_178_) {
            ((Class505_Sub1)this).anInt8441 = i;
            ((Class505_Sub1)this).anInt8358 = i_177_;
            ((Class505_Sub1)this).anInt8378 = i_178_;
            method13594();
            method13747();
        }
    }

    public Class384 method8449() {
        return ((Class505_Sub1)this).aClass384_8360;
    }

    public Class294 method8450() {
        return ((Class505_Sub1)this).aClass294_8476;
    }

    public void method8456(Class151 class151) {
        ((Class505_Sub1)this).aClass134_8359.method2329(this, class151);
    }

    public void method8475(int i, Class90 class90) {
        ((Class505_Sub1)this).anInt8450 = i;
        ((Class505_Sub1)this).aClass90_8423 = class90;
        ((Class505_Sub1)this).aBool8448 = true;
    }

    public void method8476(int i, Class90 class90) {
		if (!((Class505_Sub1)this).aBool8448) {
			throw new RuntimeException("");
		}
        ((Class505_Sub1)this).anInt8450 = i;
        ((Class505_Sub1)this).aClass90_8423 = class90;
        if (((Class505_Sub1)this).aBool8449) {
            ((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14427();
            ((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14428();
        }
    }

    public Interface8 method8419(int i, int i_179_) {
        return new Class282_Sub50_Sub19(this, Class150.aClass150_1953, Class76.aClass76_749, i, i_179_);
    }

    public void method8421() {
        ((Class505_Sub1)this).anInt8417 = 0;
        ((Class505_Sub1)this).anInt8418 = 0;
        ((Class505_Sub1)this).anInt8419 = aClass158_5853.method2714();
        ((Class505_Sub1)this).anInt8347 = aClass158_5853.method2716();
        method13584();
    }

    public void method8617(int i, int i_180_, int i_181_, int i_182_) {
        ((Class505_Sub1)this).anInt8417 = i;
        ((Class505_Sub1)this).anInt8418 = i_180_;
        ((Class505_Sub1)this).anInt8419 = i_181_;
        ((Class505_Sub1)this).anInt8347 = i_182_;
        method13584();
    }

    final void method13583() {
        OpenGL.glDepthMask(((Class505_Sub1)this).aBool8388 && ((Class505_Sub1)this).aBool8353);
    }

    public final void qa(int[] is) {
        is[0] = ((Class505_Sub1)this).anInt8413;
        is[1] = ((Class505_Sub1)this).anInt8415;
        is[2] = ((Class505_Sub1)this).anInt8412;
        is[3] = ((Class505_Sub1)this).anInt8478;
    }

    public final void L() {
        if (aClass158_5853 != null) {
            ((Class505_Sub1)this).anInt8413 = 0;
            ((Class505_Sub1)this).anInt8415 = 0;
            ((Class505_Sub1)this).anInt8412 = aClass158_5853.method2714();
            ((Class505_Sub1)this).anInt8478 = aClass158_5853.method2716();
            OpenGL.glDisable(3089);
        }
    }

    public final void r(int i, int i_183_, int i_184_, int i_185_) {
        if (aClass158_5853 != null) {
			if (i < 0) {
				i = 0;
			}
			if (i_184_ > aClass158_5853.method2714()) {
				i_184_ = aClass158_5853.method2714();
			}
			if (i_183_ < 0) {
				i_183_ = 0;
			}
			if (i_185_ > aClass158_5853.method2716()) {
				i_185_ = aClass158_5853.method2716();
			}
            ((Class505_Sub1)this).anInt8413 = i;
            ((Class505_Sub1)this).anInt8415 = i_183_;
            ((Class505_Sub1)this).anInt8412 = i_184_;
            ((Class505_Sub1)this).anInt8478 = i_185_;
            OpenGL.glEnable(3089);
            method13570();
        }
    }

    public final void o(int i, int i_186_, int i_187_, int i_188_) {
		if (((Class505_Sub1)this).anInt8413 < i) {
			((Class505_Sub1)this).anInt8413 = i;
		}
		if (((Class505_Sub1)this).anInt8412 > i_187_) {
			((Class505_Sub1)this).anInt8412 = i_187_;
		}
		if (((Class505_Sub1)this).anInt8415 < i_186_) {
			((Class505_Sub1)this).anInt8415 = i_186_;
		}
		if (((Class505_Sub1)this).anInt8478 > i_188_) {
			((Class505_Sub1)this).anInt8478 = i_188_;
		}
        OpenGL.glEnable(3089);
        method13570();
    }

    public void O() {
        ((Class505_Sub1)this).aBool8448 = false;
    }

    final void method13584() {
        if (aClass158_5853 != null) {
            int i;
            int i_189_;
            int i_190_;
            int i_191_;
            if (((Class505_Sub1)this).anInt8409 == 2) {
                i = ((Class505_Sub1)this).anInt8417;
                i_189_ = ((Class505_Sub1)this).anInt8418;
                i_190_ = ((Class505_Sub1)this).anInt8419;
                i_191_ = ((Class505_Sub1)this).anInt8347;
            } else {
                i = 0;
                i_189_ = 0;
                i_190_ = aClass158_5853.method2714();
                i_191_ = aClass158_5853.method2716();
            }
			if (i_190_ < 1) {
				i_190_ = 1;
			}
			if (i_191_ < 1) {
				i_191_ = 1;
			}
            OpenGL.glViewport(((Class505_Sub1)this).anInt8453 + i, (((Class505_Sub1)this).anInt8416 + aClass158_5853.method2716() - i_189_ - i_191_), i_190_, i_191_);
            ((Class505_Sub1)this).aFloat8398 = (float)((Class505_Sub1)this).anInt8419 / 2.0F;
            ((Class505_Sub1)this).aFloat8400 = (float)((Class505_Sub1)this).anInt8347 / 2.0F;
            ((Class505_Sub1)this).aFloat8315 = ((float)((Class505_Sub1)this).anInt8417 + ((Class505_Sub1)this).aFloat8398);
            ((Class505_Sub1)this).aFloat8399 = ((float)((Class505_Sub1)this).anInt8418 + ((Class505_Sub1)this).aFloat8400);
        }
    }

    public final void in(float f) {
        if (((Class505_Sub1)this).aFloat8432 != f) {
            ((Class505_Sub1)this).aFloat8432 = f;
            method13689();
        }
    }

    public final synchronized void method8398(int i) {
        int i_192_ = 0;
        i &= 0x7fffffff;
        while (!((Class505_Sub1)this).aClass473_8486.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8486.method7858((byte)-128);
            anIntArray8381[i_192_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8371 -= class282_sub38.anInt8002 * -570797415;
            if (i_192_ == 1000) {
                OpenGL.glDeleteBuffersARB(i_192_, anIntArray8381, 0);
                i_192_ = 0;
            }
        }
        if (i_192_ > 0) {
            OpenGL.glDeleteBuffersARB(i_192_, anIntArray8381, 0);
            i_192_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8375.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8375.method7858((byte)-94);
            anIntArray8381[i_192_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8370 -= class282_sub38.anInt8002 * -570797415;
            if (i_192_ == 1000) {
                OpenGL.glDeleteTextures(i_192_, anIntArray8381, 0);
                i_192_ = 0;
            }
        }
        if (i_192_ > 0) {
            OpenGL.glDeleteTextures(i_192_, anIntArray8381, 0);
            i_192_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8376.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8376.method7858((byte)-88);
            anIntArray8381[i_192_++] = class282_sub38.anInt8002 * -570797415;
            if (i_192_ == 1000) {
                OpenGL.glDeleteFramebuffersEXT(i_192_, anIntArray8381, 0);
                i_192_ = 0;
            }
        }
        if (i_192_ > 0) {
            OpenGL.glDeleteFramebuffersEXT(i_192_, anIntArray8381, 0);
            i_192_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8377.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8377.method7858((byte)-18);
            anIntArray8381[i_192_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8372 -= class282_sub38.anInt8002 * -570797415;
            if (i_192_ == 1000) {
                OpenGL.glDeleteRenderbuffersEXT(i_192_, anIntArray8381, 0);
                i_192_ = 0;
            }
        }
        if (i_192_ > 0) {
            OpenGL.glDeleteRenderbuffersEXT(i_192_, anIntArray8381, 0);
            boolean bool = false;
        }
        while (!((Class505_Sub1)this).aClass473_8373.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8373.method7858((byte)-82);
            OpenGL.glDeleteLists((int)(class282_sub38.aLong3379 * -3442165056282524525L), class282_sub38.anInt8002 * -570797415);
        }
        while (!((Class505_Sub1)this).aClass473_8461.method7861(141891001)) {
            Class282 class282 = ((Class505_Sub1)this).aClass473_8461.method7858((byte)-59);
            OpenGL.glDeleteProgramARB((int)(class282.aLong3379 * -3442165056282524525L));
        }
        while (!((Class505_Sub1)this).aClass473_8379.method7861(141891001)) {
            Class282 class282 = ((Class505_Sub1)this).aClass473_8379.method7858((byte)-53);
            OpenGL.glDeleteShader((int)(class282.aLong3379 * -3442165056282524525L));
        }
        while (!((Class505_Sub1)this).aClass473_8373.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8373.method7858((byte)-88);
            OpenGL.glDeleteLists((int)(class282_sub38.aLong3379 * -3442165056282524525L), class282_sub38.anInt8002 * -570797415);
        }
        ((Class505_Sub1)this).aClass167_8481.method2860();
        if (za() > 100663296 && (Class169.method2869(1574638631) > ((Class505_Sub1)this).aLong8316 + 60000L)) {
            System.gc();
            ((Class505_Sub1)this).aLong8316 = Class169.method2869(1953079517);
        }
        ((Class505_Sub1)this).anInt8368 = i;
    }

    void method8486() {
		for (Class282 class282 = ((Class505_Sub1)this).aClass473_8369.method7859(1074880830); class282 != null; class282 = ((Class505_Sub1)this).aClass473_8369.method7857((byte)-91)) {
			((Class282_Sub1_Sub1)class282).method15450();
		}
		if (((Class505_Sub1)this).aClass170_8357 != null) {
			((Class505_Sub1)this).aClass170_8357.method2898();
		}
        if (((Class505_Sub1)this).aBool8367) {
            Class13.method508(false, true, 253235103);
            ((Class505_Sub1)this).aBool8367 = false;
        }
    }

    void CA(int i, int i_193_, int i_194_, int i_195_, int i_196_) {
		if (i_194_ < 0) {
			i_194_ = -i_194_;
		}
        if (i + i_194_ >= ((Class505_Sub1)this).anInt8413 && i - i_194_ <= ((Class505_Sub1)this).anInt8412 && i_193_ + i_194_ >= ((Class505_Sub1)this).anInt8415 && i_193_ - i_194_ <= ((Class505_Sub1)this).anInt8478) {
            method13659();
            method13624(i_196_);
            OpenGL.glColor4ub((byte)(i_195_ >> 16), (byte)(i_195_ >> 8), (byte)i_195_, (byte)(i_195_ >> 24));
            float f = (float)i + 0.35F;
            float f_197_ = (float)i_193_ + 0.35F;
            int i_198_ = i_194_ << 1;
            if ((float)i_198_ < ((Class505_Sub1)this).aFloat8414) {
                OpenGL.glBegin(7);
                OpenGL.glVertex2f(f + 1.0F, f_197_ + 1.0F);
                OpenGL.glVertex2f(f + 1.0F, f_197_ - 1.0F);
                OpenGL.glVertex2f(f - 1.0F, f_197_ - 1.0F);
                OpenGL.glVertex2f(f - 1.0F, f_197_ + 1.0F);
                OpenGL.glEnd();
            } else if ((float)i_198_ <= ((Class505_Sub1)this).aFloat8489) {
                OpenGL.glEnable(2832);
                OpenGL.glPointSize((float)i_198_);
                OpenGL.glBegin(0);
                OpenGL.glVertex2f(f, f_197_);
                OpenGL.glEnd();
                OpenGL.glDisable(2832);
            } else {
                OpenGL.glBegin(6);
                OpenGL.glVertex2f(f, f_197_);
                int i_199_ = 262144 / (6 * i_194_);
				if (i_199_ <= 64) {
					i_199_ = 64;
				} else if (i_199_ > 512) {
					i_199_ = 512;
				}
                i_199_ = Class51.method1072(i_199_, 1399937538);
                OpenGL.glVertex2f(f + (float)i_194_, f_197_);
				for (int i_200_ = 16384 - i_199_; i_200_ > 0; i_200_ -= i_199_) {
					OpenGL.glVertex2f(f + (Class142.aFloatArray1665[i_200_] * (float)i_194_), f_197_ + (Class142.aFloatArray1666[i_200_]) * (float)i_194_);
				}
                OpenGL.glVertex2f(f + (float)i_194_, f_197_);
                OpenGL.glEnd();
            }
        }
    }

    final void method13585(Class384 class384) {
        OpenGL.glLoadMatrixf(class384.aFloatArray4667, 0);
    }

    public Class455 method8554(int i, int i_201_, int[] is, int[] is_202_) {
        return Class455_Sub2.method14343(this, i, i_201_, is, is_202_);
    }

    public final void method8457(Class294 class294) {
        ((Class505_Sub1)this).aClass294_8389.method5209(class294);
        ((Class505_Sub1)this).aClass384_8348.method6522(((Class505_Sub1)this).aClass294_8389);
        ((Class505_Sub1)this).aClass294_8405.method5209(class294);
        ((Class505_Sub1)this).aClass294_8405.method5207();
        ((Class505_Sub1)this).aClass384_8391.method6522(((Class505_Sub1)this).aClass294_8405);
        method13597();
		if (((Class505_Sub1)this).anInt8409 != 1) {
			method13748();
		}
    }

    public Class294 method8458() {
        return new Class294(((Class505_Sub1)this).aClass294_8389);
    }

    public boolean method8501() {
        return (((Class505_Sub1)this).aBool8342 && (!method8471() || ((Class505_Sub1)this).aBool8344));
    }

    public Class168 method8481() {
        int i = -1;
		if (((Class505_Sub1)this).aString8463.indexOf("nvidia") != -1) {
			i = 4318;
		} else if (((Class505_Sub1)this).aString8463.indexOf("intel") != -1) {
			i = 32902;
		} else if (((Class505_Sub1)this).aString8463.indexOf("ati") != -1) {
			i = 4098;
		}
        return new Class168(i, "OpenGL", ((Class505_Sub1)this).anInt8443, ((Class505_Sub1)this).aString8464, 0L);
    }

    public final Class384 method8453() {
        return new Class384(((Class505_Sub1)this).aClass384_8442);
    }

    final void method13586() {
        if (((Class505_Sub1)this).anInt8409 != 0) {
            ((Class505_Sub1)this).anInt8409 = 0;
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    final void method13587() {
        if (((Class505_Sub1)this).anInt8409 != 1) {
            ((Class505_Sub1)this).anInt8409 = 1;
            method13588();
            method13584();
            method13671();
            OpenGL.glMatrixMode(5888);
            OpenGL.glLoadIdentity();
            ((Class505_Sub1)this).anInt8382 &= ~0x8;
        }
    }

    public int method8566(int i, int i_203_) {
        return i & i_203_ ^ i_203_;
    }

    final void method13588() {
        ((Class505_Sub1)this).aFloat8404 = ((Class505_Sub1)this).aClass384_8442.method6587();
        ((Class505_Sub1)this).aFloat8403 = ((Class505_Sub1)this).aClass384_8442.method6533();
        method13594();
		if (((Class505_Sub1)this).anInt8409 == 2) {
			method13719(((Class505_Sub1)this).aClass384_8442.aFloatArray4667);
		} else if (((Class505_Sub1)this).anInt8409 == 1) {
			method13719(((Class505_Sub1)this).aClass384_8385.aFloatArray4667);
		}
    }

    public final int dv() {
        return (((Class505_Sub1)this).anInt8371 + ((Class505_Sub1)this).anInt8370 + ((Class505_Sub1)this).anInt8372);
    }

    final void method13589(float f, float f_204_, float f_205_, float f_206_) {
        aFloatArray8497[0] = f;
        aFloatArray8497[1] = f_204_;
        aFloatArray8497[2] = f_205_;
        aFloatArray8497[3] = f_206_;
        OpenGL.glTexEnvfv(8960, 8705, aFloatArray8497, 0);
    }

    public boolean method8399() {
        return true;
    }

    public final void method8525(int i, int i_207_, int i_208_, int i_209_) {
        ((Class505_Sub1)this).aClass170_8357.method2884(i, i_207_, i_208_, i_209_);
    }

    final synchronized void method13590(int i, int i_210_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_210_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8375.method7877(class282_sub38, 1663473593);
    }

    public Class390 method8569(int i, int i_211_, int[][] is, int[][] is_212_, int i_213_, int i_214_, int i_215_) {
        return new Class390_Sub2(this, i_214_, i_215_, i, i_211_, is, is_212_, i_213_);
    }

    public final void IA(float f) {
        if (((Class505_Sub1)this).aFloat8432 != f) {
            ((Class505_Sub1)this).aFloat8432 = f;
            method13689();
        }
    }

    public final void m(int i, float f, float f_216_, float f_217_, float f_218_, float f_219_) {
        boolean bool = ((Class505_Sub1)this).anInt8428 != i;
        if (bool || ((Class505_Sub1)this).aFloat8433 != f || ((Class505_Sub1)this).aFloat8434 != f_216_) {
            ((Class505_Sub1)this).anInt8428 = i;
            ((Class505_Sub1)this).aFloat8433 = f;
            ((Class505_Sub1)this).aFloat8434 = f_216_;
            if (bool) {
                ((Class505_Sub1)this).aFloat8429 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff0000) / 1.671168E7F);
                ((Class505_Sub1)this).aFloat8430 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff00) / 65280.0F);
                ((Class505_Sub1)this).aFloat8431 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff) / 255.0F);
                method13689();
            }
            method13592();
        }
        if (((Class505_Sub1)this).aFloatArray8424[0] != f_217_ || ((Class505_Sub1)this).aFloatArray8424[1] != f_218_ || ((Class505_Sub1)this).aFloatArray8424[2] != f_219_) {
            ((Class505_Sub1)this).aFloatArray8424[0] = f_217_;
            ((Class505_Sub1)this).aFloatArray8424[1] = f_218_;
            ((Class505_Sub1)this).aFloatArray8424[2] = f_219_;
            ((Class505_Sub1)this).aFloatArray8425[0] = -f_217_;
            ((Class505_Sub1)this).aFloatArray8425[1] = -f_218_;
            ((Class505_Sub1)this).aFloatArray8425[2] = -f_219_;
            float f_220_ = (float)(1.0 / Math.sqrt((double)(f_217_ * f_217_ + f_218_ * f_218_ + f_219_ * f_219_)));
            ((Class505_Sub1)this).aFloatArray8426[0] = f_217_ * f_220_;
            ((Class505_Sub1)this).aFloatArray8426[1] = f_218_ * f_220_;
            ((Class505_Sub1)this).aFloatArray8426[2] = f_219_ * f_220_;
            ((Class505_Sub1)this).aFloatArray8427[0] = -((Class505_Sub1)this).aFloatArray8426[0];
            ((Class505_Sub1)this).aFloatArray8427[1] = -((Class505_Sub1)this).aFloatArray8426[1];
            ((Class505_Sub1)this).aFloatArray8427[2] = -((Class505_Sub1)this).aFloatArray8426[2];
            method13593();
            ((Class505_Sub1)this).anInt8438 = (int)(f_217_ * 256.0F / f_218_);
            ((Class505_Sub1)this).anInt8439 = (int)(f_219_ * 256.0F / f_218_);
        }
    }

    public int method8437(int i, int i_221_, int i_222_, int i_223_, int i_224_, int i_225_) {
        int i_226_ = 0;
        float f = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_221_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_222_));
        float f_227_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i_223_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_224_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_225_));
        float f_228_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_221_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_222_));
        float f_229_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i_223_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_224_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_225_));
		if (f < -f_228_ && f_227_ < -f_229_) {
			i_226_ |= 0x10;
		} else if (f > f_228_ && f_227_ > f_229_) {
			i_226_ |= 0x20;
		}
        float f_230_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_221_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_222_));
        float f_231_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i_223_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_224_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_225_));
		if (f_230_ < -f_228_ && f_231_ < -f_229_) {
			i_226_ |= 0x1;
		}
		if (f_230_ > f_228_ && f_231_ > f_229_) {
			i_226_ |= 0x2;
		}
        float f_232_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_221_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_222_));
        float f_233_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i_223_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_224_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_225_));
		if (f_232_ < -f_228_ && f_233_ < -f_229_) {
			i_226_ |= 0x4;
		}
		if (f_232_ > f_228_ && f_233_ > f_229_) {
			i_226_ |= 0x8;
		}
        return i_226_;
    }

    public Class8 method8448(Class414 class414, Class91[] class91s, boolean bool) {
        return new Class8_Sub5(this, class414, class91s, bool);
    }

    final void method13591(float f, float f_234_) {
        ((Class505_Sub1)this).aFloat8446 = f;
        ((Class505_Sub1)this).aFloat8355 = f_234_;
        method13594();
    }

    public void method8618(Class282_Sub1 class282_sub1) {
        ((Class505_Sub1)this).aNativeHeap8445 = (((Class282_Sub1_Sub1)(Class282_Sub1_Sub1)class282_sub1).aNativeHeap10051);
        if (((Class505_Sub1)this).anInterface14_8496 == null) {
            Class282_Sub35_Sub1 class282_sub35_sub1 = new Class282_Sub35_Sub1(80);
            if (((Class505_Sub1)this).aBool8467) {
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
            } else {
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
            }
            ((Class505_Sub1)this).anInterface14_8496 = method13599(20, class282_sub35_sub1.buffer, class282_sub35_sub1.index * -1990677291, false);
            ((Class505_Sub1)this).aClass143_8494 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 3, 0);
            ((Class505_Sub1)this).aClass143_8495 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 2, 12);
            ((Class505_Sub1)this).aClass134_8359.method2328(this);
        }
    }

    void method13592() {
        aFloatArray8497[0] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16384, 4609, aFloatArray8497, 0);
        aFloatArray8497[0] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16385, 4609, aFloatArray8497, 0);
    }

    void method13593() {
        OpenGL.glLightfv(16384, 4611, ((Class505_Sub1)this).aFloatArray8426, 0);
        OpenGL.glLightfv(16385, 4611, ((Class505_Sub1)this).aFloatArray8427, 0);
    }

    public boolean method8402() {
        return true;
    }

    void method13594() {
        ((Class505_Sub1)this).aFloat8402 = (((Class505_Sub1)this).aFloat8404 - (float)((Class505_Sub1)this).anInt8378 - ((Class505_Sub1)this).aFloat8355);
        ((Class505_Sub1)this).aFloat8406 = (((Class505_Sub1)this).aFloat8402 - ((float)((Class505_Sub1)this).anInt8358 * ((Class505_Sub1)this).aFloat8446));
		if (((Class505_Sub1)this).aFloat8406 < ((Class505_Sub1)this).aFloat8403) {
			((Class505_Sub1)this).aFloat8406 = ((Class505_Sub1)this).aFloat8403;
		}
        OpenGL.glFogf(2915, ((Class505_Sub1)this).aFloat8406);
        OpenGL.glFogf(2916, ((Class505_Sub1)this).aFloat8402);
        aFloatArray8497[0] = (float)(((Class505_Sub1)this).anInt8441 & 0xff0000) / 1.671168E7F;
        aFloatArray8497[1] = (float)(((Class505_Sub1)this).anInt8441 & 0xff00) / 65280.0F;
        aFloatArray8497[2] = (float)(((Class505_Sub1)this).anInt8441 & 0xff) / 255.0F;
        OpenGL.glFogfv(2918, aFloatArray8497, 0);
    }

    final void method13595(int i, int i_235_, int i_236_) {
        OpenGL.glTexEnvi(8960, 34176 + i, i_235_);
        OpenGL.glTexEnvi(8960, 34192 + i, i_236_);
    }

    public Class152 method8466(int i, int i_237_, int i_238_, int i_239_, int i_240_, int i_241_) {
        return (((Class505_Sub1)this).aBool8480 ? new Class152_Sub1_Sub1(this, i, i_237_, i_238_, i_239_, i_240_, i_241_) : null);
    }

    public Class152 method8467(Class152 class152, Class152 class152_242_, float f, Class152 class152_243_) {
        if (class152 != null && class152_242_ != null && ((Class505_Sub1)this).aBool8480 && ((Class505_Sub1)this).aBool8472) {
            Class152_Sub1_Sub2 class152_sub1_sub2 = null;
            Class152_Sub1 class152_sub1 = (Class152_Sub1)class152;
            Class152_Sub1 class152_sub1_244_ = (Class152_Sub1)class152_242_;
            Class137_Sub2 class137_sub2 = class152_sub1.method13519();
            Class137_Sub2 class137_sub2_245_ = class152_sub1_244_.method13519();
            if (class137_sub2 != null && class137_sub2_245_ != null) {
                int i = ((((Class137_Sub2)class137_sub2).anInt9104 > ((Class137_Sub2)class137_sub2_245_).anInt9104) ? ((Class137_Sub2)class137_sub2).anInt9104 : ((Class137_Sub2)class137_sub2_245_).anInt9104);
                if (class152 != class152_243_ && class152_242_ != class152_243_ && class152_243_ instanceof Class152_Sub1_Sub2) {
                    Class152_Sub1_Sub2 class152_sub1_sub2_246_ = (Class152_Sub1_Sub2)class152_243_;
					if (class152_sub1_sub2_246_.method15532() == i) {
						class152_sub1_sub2 = class152_sub1_sub2_246_;
					}
                }
				if (class152_sub1_sub2 == null) {
					class152_sub1_sub2 = new Class152_Sub1_Sub2(this, i);
				}
				if (class152_sub1_sub2.method15531(class137_sub2, class137_sub2_245_, f)) {
					return class152_sub1_sub2;
				}
            }
        }
        return f < 0.5F ? class152 : class152_242_;
    }

    public final void iz(float f) {
        if (((Class505_Sub1)this).aFloat8432 != f) {
            ((Class505_Sub1)this).aFloat8432 = f;
            method13689();
        }
    }

    final Class137_Sub2 method13596() {
        return (((Class505_Sub1)this).aClass152_Sub1_8317 != null ? ((Class505_Sub1)this).aClass152_Sub1_8317.method13519() : null);
    }

    public final boolean method8469() {
        if (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null) {
            if (!((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129()) {
				if (((Class505_Sub1)this).aClass170_8357.method2900(((Class505_Sub1)this).aClass282_Sub5_Sub1_8444)) {
					((Class505_Sub1)this).aClass167_8481.method2861();
				} else {
					return false;
				}
            }
            return true;
        }
        return false;
    }

    public final void method8490() {
        if (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129()) {
            ((Class505_Sub1)this).aClass170_8357.method2885(((Class505_Sub1)this).aClass282_Sub5_Sub1_8444);
            ((Class505_Sub1)this).aClass167_8481.method2861();
        }
    }

    public final boolean method8471() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129());
    }

    final void method8592(float f, float f_247_, float f_248_, float f_249_, float f_250_, float f_251_) {
        Class282_Sub5_Sub1.aFloat10026 = f;
        Class282_Sub5_Sub1.aFloat10025 = f_247_;
        Class282_Sub5_Sub1.aFloat10024 = f_248_;
    }

    public boolean method8492() {
        return true;
    }

    public final void method8474() {
        ((Class505_Sub1)this).aClass170_8357.method2883();
    }

    public Class158_Sub1 method8418() {
        return new Class158_Sub1_Sub4(this);
    }

    final void method13597() {
        ((Class505_Sub1)this).aClass384_8394.method6562(((Class505_Sub1)this).aClass384_8348);
        ((Class505_Sub1)this).aClass384_8394.method6523(((Class505_Sub1)this).aClass384_8442);
        ((Class505_Sub1)this).aClass384_8394.method6539(((Class505_Sub1)this).aFloatArrayArray8395[0]);
        ((Class505_Sub1)this).aClass384_8394.method6540(((Class505_Sub1)this).aFloatArrayArray8395[1]);
        ((Class505_Sub1)this).aClass384_8394.method6535(((Class505_Sub1)this).aFloatArrayArray8395[2]);
        ((Class505_Sub1)this).aClass384_8394.method6536(((Class505_Sub1)this).aFloatArrayArray8395[3]);
        ((Class505_Sub1)this).aClass384_8394.method6613(((Class505_Sub1)this).aFloatArrayArray8395[4]);
        ((Class505_Sub1)this).aClass384_8394.method6538(((Class505_Sub1)this).aFloatArrayArray8395[5]);
    }

    final Interface15 method13598(int i, byte[] is, int i_252_, boolean bool) {
		if (((Class505_Sub1)this).aBool8309 && (!bool || ((Class505_Sub1)this).aBool8362)) {
			return new Class135_Sub1(this, i, is, i_252_, bool);
		}
        return new Class131_Sub2(this, i, is, i_252_);
    }

    final Interface14 method13599(int i, byte[] is, int i_253_, boolean bool) {
		if (((Class505_Sub1)this).aBool8309 && (!bool || ((Class505_Sub1)this).aBool8362)) {
			return new Class135_Sub2(this, i, is, i_253_, bool);
		}
        return new Class131_Sub1(this, i, is, i_253_);
    }

    final Interface14 method13600(int i, Buffer buffer, int i_254_, boolean bool) {
		if (((Class505_Sub1)this).aBool8309 && (!bool || ((Class505_Sub1)this).aBool8362)) {
			return new Class135_Sub2(this, i, buffer, i_254_, bool);
		}
        return new Class131_Sub1(this, i, buffer);
    }

    final void method13601(Interface14 interface14) {
        if (((Class505_Sub1)this).anInterface14_8483 != interface14) {
			if (((Class505_Sub1)this).aBool8309) {
				OpenGL.glBindBufferARB(34962, interface14.method1());
			}
            ((Class505_Sub1)this).anInterface14_8483 = interface14;
        }
    }

    public void method8568(boolean bool) {
        /* empty */
    }

    final void method13602(float f, float f_255_, float f_256_) {
        OpenGL.glMatrixMode(5890);
		if (((Class505_Sub1)this).aBool8457) {
			OpenGL.glLoadIdentity();
		}
        OpenGL.glTranslatef(f, f_255_, f_256_);
        OpenGL.glMatrixMode(5888);
        ((Class505_Sub1)this).aBool8457 = true;
    }

    final void method13603(int i, int i_257_, int i_258_) {
        OpenGL.glDrawArrays(i, i_257_, i_258_);
    }

    final synchronized void method13604(int i) {
        Class282 class282 = new Class282();
        class282.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8461.method7877(class282, 2139046755);
    }

    final void method13605(int i) {
        if (((Class505_Sub1)this).anInt8458 != i) {
            OpenGL.glActiveTexture(33984 + i);
            ((Class505_Sub1)this).anInt8458 = i;
        }
    }

    public boolean method8495() {
        return true;
    }

    final void method13606() {
        if (((Class505_Sub1)this).anInt8382 != 4) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13581(-2);
            method13624(1);
            ((Class505_Sub1)this).anInt8382 = 4;
        }
    }

    final void method13607() {
        if (((Class505_Sub1)this).anInt8382 != 8) {
            method13645();
            method13642(true);
            method13656(true);
            method13623(true);
            method13624(1);
            ((Class505_Sub1)this).anInt8382 = 8;
        }
    }

    public void method8483() {
        OpenGL.glFinish();
    }

    final void method13608(int i, boolean bool) {
        method13609(i, bool, true);
    }

    final void method13609(int i, boolean bool, boolean bool_259_) {
        if (i != ((Class505_Sub1)this).anInt8454 || (((Class505_Sub1)this).aBool8449 != ((Class505_Sub1)this).aBool8448)) {
            Class137_Sub1 class137_sub1 = null;
            int i_260_ = 0;
            byte i_261_ = 0;
            int i_262_ = 0;
            byte i_263_ = ((Class505_Sub1)this).aBool8448 ? (byte)3 : (byte)0;
			if (i >= 0) {
				class137_sub1 = ((Class505_Sub1)this).aClass167_8481.method2858(i);
				Class169 class169 = anInterface22_5834.method144(i, -1790477491);
				if (class169.aByte2081 != 0 || class169.aByte2090 != 0) {
					method13602(((float)(((Class505_Sub1)this).anInt8368 % 128000) / 1000.0F * (float)class169.aByte2081 / 64.0F % 1.0F), ((float)(((Class505_Sub1)this).anInt8368 % 128000) / 1000.0F * (float)class169.aByte2090 / 64.0F % 1.0F), 0.0F);
				} else {
					method13618();
				}
				if (!((Class505_Sub1)this).aBool8448) {
					i_261_ = class169.aByte2076;
					i_262_ = class169.anInt2077 * 1385119855;
					i_263_ = class169.aByte2064;
				}
				i_260_ = class169.anInt2091 * 1436865495;
			} else {
				method13618();
			}
            ((Class505_Sub1)this).aClass146_8356.method2462(i_263_, i_261_, i_262_, bool, bool_259_);
            if (!((Class505_Sub1)this).aClass146_8356.method2463(class137_sub1, i_260_)) {
                method13654(class137_sub1);
                method13612(i_260_);
            }
            ((Class505_Sub1)this).aBool8449 = ((Class505_Sub1)this).aBool8448;
            ((Class505_Sub1)this).anInt8454 = i;
        }
        ((Class505_Sub1)this).anInt8382 &= ~0x7;
    }

    final void method13610(int i) {
        if (((Class505_Sub1)this).anInt8458 != i) {
            OpenGL.glActiveTexture(33984 + i);
            ((Class505_Sub1)this).anInt8458 = i;
        }
    }

    final void method13611(Interface15 interface15, int i, int i_264_, int i_265_) {
        method13663(interface15);
        OpenGL.glDrawElements(i, i_265_, 5123, interface15.method2() + (long)(i_264_ * 2));
    }

    final void method13612(int i) {
		if (i == 1) {
			method13717(7681, 7681);
		} else if (i == 0) {
			method13717(8448, 8448);
		} else if (i == 2) {
			method13717(34165, 7681);
		} else if (i == 3) {
			method13717(260, 8448);
		} else if (i == 4) {
			method13717(34023, 34023);
		}
    }

    final int method13613(int i) {
		if (i == 1) {
			return 7681;
		}
		if (i == 0) {
			return 8448;
		}
		if (i == 2) {
			return 34165;
		}
		if (i == 3) {
			return 260;
		}
		if (i == 4) {
			return 34023;
		}
        throw new IllegalArgumentException();
    }

    void method13614() {
        ((Class505_Sub1)this).aFloat8402 = (((Class505_Sub1)this).aFloat8404 - (float)((Class505_Sub1)this).anInt8378 - ((Class505_Sub1)this).aFloat8355);
        ((Class505_Sub1)this).aFloat8406 = (((Class505_Sub1)this).aFloat8402 - ((float)((Class505_Sub1)this).anInt8358 * ((Class505_Sub1)this).aFloat8446));
		if (((Class505_Sub1)this).aFloat8406 < ((Class505_Sub1)this).aFloat8403) {
			((Class505_Sub1)this).aFloat8406 = ((Class505_Sub1)this).aFloat8403;
		}
        OpenGL.glFogf(2915, ((Class505_Sub1)this).aFloat8406);
        OpenGL.glFogf(2916, ((Class505_Sub1)this).aFloat8402);
        aFloatArray8497[0] = (float)(((Class505_Sub1)this).anInt8441 & 0xff0000) / 1.671168E7F;
        aFloatArray8497[1] = (float)(((Class505_Sub1)this).anInt8441 & 0xff00) / 65280.0F;
        aFloatArray8497[2] = (float)(((Class505_Sub1)this).anInt8441 & 0xff) / 255.0F;
        OpenGL.glFogfv(2918, aFloatArray8497, 0);
    }

    static int method13615(Class150 class150, Class76 class76) {
        if (class76 == Class76.aClass76_751) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 6407;
                case 4:
                    return 6410;
                case 6:
                    return 6408;
                case 0:
                    return 6409;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 6406;
            }
        }
        if (class76 == Class76.aClass76_752) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 32852;
                case 4:
                    return 36219;
                case 1:
                    return 32830;
                case 6:
                    return 32859;
                case 0:
                    return 32834;
                case 8:
                    return 33189;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_749) {
            switch (class150.anInt1958 * -1436290903) {
                case 8:
                    return 33190;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_755) {
            switch (class150.anInt1958 * -1436290903) {
                case 6:
                    return 34842;
                case 1:
                    return 34844;
                case 0:
                    return 34846;
                case 4:
                    return 34847;
                default:
                    throw new IllegalArgumentException();
                case 5:
                    return 34843;
            }
        }
        if (class76 == Class76.aClass76_758) {
            switch (class150.anInt1958 * -1436290903) {
                case 0:
                    return 34840;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 34838;
                case 5:
                    return 34837;
                case 4:
                    return 34841;
                case 6:
                    return 34836;
            }
        }
        throw new IllegalArgumentException();
    }

    final void method13616(int i, int i_266_, int i_267_) {
        OpenGL.glTexEnvi(8960, 34184 + i, i_266_);
        OpenGL.glTexEnvi(8960, 34200 + i, i_267_);
    }

    final void method13617(int i) {
        aFloatArray8497[0] = (float)(i & 0xff0000) / 1.671168E7F;
        aFloatArray8497[1] = (float)(i & 0xff00) / 65280.0F;
        aFloatArray8497[2] = (float)(i & 0xff) / 255.0F;
        aFloatArray8497[3] = (float)(i >>> 24) / 255.0F;
        OpenGL.glTexEnvfv(8960, 8705, aFloatArray8497, 0);
    }

    public final synchronized void method8488(int i) {
        int i_268_ = 0;
        i &= 0x7fffffff;
        while (!((Class505_Sub1)this).aClass473_8486.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8486.method7858((byte)-104);
            anIntArray8381[i_268_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8371 -= class282_sub38.anInt8002 * -570797415;
            if (i_268_ == 1000) {
                OpenGL.glDeleteBuffersARB(i_268_, anIntArray8381, 0);
                i_268_ = 0;
            }
        }
        if (i_268_ > 0) {
            OpenGL.glDeleteBuffersARB(i_268_, anIntArray8381, 0);
            i_268_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8375.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8375.method7858((byte)-108);
            anIntArray8381[i_268_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8370 -= class282_sub38.anInt8002 * -570797415;
            if (i_268_ == 1000) {
                OpenGL.glDeleteTextures(i_268_, anIntArray8381, 0);
                i_268_ = 0;
            }
        }
        if (i_268_ > 0) {
            OpenGL.glDeleteTextures(i_268_, anIntArray8381, 0);
            i_268_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8376.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8376.method7858((byte)-112);
            anIntArray8381[i_268_++] = class282_sub38.anInt8002 * -570797415;
            if (i_268_ == 1000) {
                OpenGL.glDeleteFramebuffersEXT(i_268_, anIntArray8381, 0);
                i_268_ = 0;
            }
        }
        if (i_268_ > 0) {
            OpenGL.glDeleteFramebuffersEXT(i_268_, anIntArray8381, 0);
            i_268_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8377.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8377.method7858((byte)-125);
            anIntArray8381[i_268_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8372 -= class282_sub38.anInt8002 * -570797415;
            if (i_268_ == 1000) {
                OpenGL.glDeleteRenderbuffersEXT(i_268_, anIntArray8381, 0);
                i_268_ = 0;
            }
        }
        if (i_268_ > 0) {
            OpenGL.glDeleteRenderbuffersEXT(i_268_, anIntArray8381, 0);
            boolean bool = false;
        }
        while (!((Class505_Sub1)this).aClass473_8373.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8373.method7858((byte)-47);
            OpenGL.glDeleteLists((int)(class282_sub38.aLong3379 * -3442165056282524525L), class282_sub38.anInt8002 * -570797415);
        }
        while (!((Class505_Sub1)this).aClass473_8461.method7861(141891001)) {
            Class282 class282 = ((Class505_Sub1)this).aClass473_8461.method7858((byte)-49);
            OpenGL.glDeleteProgramARB((int)(class282.aLong3379 * -3442165056282524525L));
        }
        while (!((Class505_Sub1)this).aClass473_8379.method7861(141891001)) {
            Class282 class282 = ((Class505_Sub1)this).aClass473_8379.method7858((byte)-79);
            OpenGL.glDeleteShader((int)(class282.aLong3379 * -3442165056282524525L));
        }
        while (!((Class505_Sub1)this).aClass473_8373.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8373.method7858((byte)-55);
            OpenGL.glDeleteLists((int)(class282_sub38.aLong3379 * -3442165056282524525L), class282_sub38.anInt8002 * -570797415);
        }
        ((Class505_Sub1)this).aClass167_8481.method2860();
        if (za() > 100663296 && (Class169.method2869(1589099144) > ((Class505_Sub1)this).aLong8316 + 60000L)) {
            System.gc();
            ((Class505_Sub1)this).aLong8316 = Class169.method2869(2020919396);
        }
        ((Class505_Sub1)this).anInt8368 = i;
    }

    public final void im(int i, float f, float f_269_, float f_270_, float f_271_, float f_272_) {
        boolean bool = ((Class505_Sub1)this).anInt8428 != i;
        if (bool || ((Class505_Sub1)this).aFloat8433 != f || ((Class505_Sub1)this).aFloat8434 != f_269_) {
            ((Class505_Sub1)this).anInt8428 = i;
            ((Class505_Sub1)this).aFloat8433 = f;
            ((Class505_Sub1)this).aFloat8434 = f_269_;
            if (bool) {
                ((Class505_Sub1)this).aFloat8429 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff0000) / 1.671168E7F);
                ((Class505_Sub1)this).aFloat8430 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff00) / 65280.0F);
                ((Class505_Sub1)this).aFloat8431 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff) / 255.0F);
                method13689();
            }
            method13592();
        }
        if (((Class505_Sub1)this).aFloatArray8424[0] != f_270_ || ((Class505_Sub1)this).aFloatArray8424[1] != f_271_ || ((Class505_Sub1)this).aFloatArray8424[2] != f_272_) {
            ((Class505_Sub1)this).aFloatArray8424[0] = f_270_;
            ((Class505_Sub1)this).aFloatArray8424[1] = f_271_;
            ((Class505_Sub1)this).aFloatArray8424[2] = f_272_;
            ((Class505_Sub1)this).aFloatArray8425[0] = -f_270_;
            ((Class505_Sub1)this).aFloatArray8425[1] = -f_271_;
            ((Class505_Sub1)this).aFloatArray8425[2] = -f_272_;
            float f_273_ = (float)(1.0 / Math.sqrt((double)(f_270_ * f_270_ + f_271_ * f_271_ + f_272_ * f_272_)));
            ((Class505_Sub1)this).aFloatArray8426[0] = f_270_ * f_273_;
            ((Class505_Sub1)this).aFloatArray8426[1] = f_271_ * f_273_;
            ((Class505_Sub1)this).aFloatArray8426[2] = f_272_ * f_273_;
            ((Class505_Sub1)this).aFloatArray8427[0] = -((Class505_Sub1)this).aFloatArray8426[0];
            ((Class505_Sub1)this).aFloatArray8427[1] = -((Class505_Sub1)this).aFloatArray8426[1];
            ((Class505_Sub1)this).aFloatArray8427[2] = -((Class505_Sub1)this).aFloatArray8426[2];
            method13593();
            ((Class505_Sub1)this).anInt8438 = (int)(f_270_ * 256.0F / f_271_);
            ((Class505_Sub1)this).anInt8439 = (int)(f_272_ * 256.0F / f_271_);
        }
    }

    final void method13618() {
        if (((Class505_Sub1)this).aBool8457) {
            OpenGL.glMatrixMode(5890);
            OpenGL.glLoadIdentity();
            OpenGL.glMatrixMode(5888);
            ((Class505_Sub1)this).aBool8457 = false;
        }
    }

    final Interface15 method13619(int i, byte[] is, int i_274_, boolean bool) {
		if (((Class505_Sub1)this).aBool8309 && (!bool || ((Class505_Sub1)this).aBool8362)) {
			return new Class135_Sub1(this, i, is, i_274_, bool);
		}
        return new Class131_Sub2(this, i, is, i_274_);
    }

    final void method13620(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8422) {
            ((Class505_Sub1)this).aBool8422 = bool;
            method13622();
            ((Class505_Sub1)this).anInt8382 &= ~0x7;
        }
    }

    final void method13621(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8397) {
            ((Class505_Sub1)this).aBool8397 = bool;
            method13622();
        }
    }

    void method13622() {
		if (((Class505_Sub1)this).aBool8422 && !((Class505_Sub1)this).aBool8397) {
			OpenGL.glEnable(2896);
		} else {
			OpenGL.glDisable(2896);
		}
    }

    public final boolean method8600() {
        if (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null) {
            if (!((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129()) {
				if (((Class505_Sub1)this).aClass170_8357.method2900(((Class505_Sub1)this).aClass282_Sub5_Sub1_8444)) {
					((Class505_Sub1)this).aClass167_8481.method2861();
				} else {
					return false;
				}
            }
            return true;
        }
        return false;
    }

    final void method13623(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8388) {
            ((Class505_Sub1)this).aBool8388 = bool;
            method13579();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    public final boolean method8628() {
        if (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null) {
            if (!((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129()) {
				if (((Class505_Sub1)this).aClass170_8357.method2900(((Class505_Sub1)this).aClass282_Sub5_Sub1_8444)) {
					((Class505_Sub1)this).aClass167_8481.method2861();
				} else {
					return false;
				}
            }
            return true;
        }
        return false;
    }

    final void method13624(int i) {
        if (((Class505_Sub1)this).anInt8366 != i) {
            int i_275_;
            boolean bool;
            boolean bool_276_;
            if (i == 1) {
                i_275_ = 1;
                bool = true;
                bool_276_ = true;
            } else if (i == 2) {
                i_275_ = 2;
                bool = true;
                bool_276_ = false;
            } else if (i == 128) {
                i_275_ = 3;
                bool = true;
                bool_276_ = true;
            } else {
                i_275_ = 0;
                bool = true;
                bool_276_ = false;
            }
            if (bool != ((Class505_Sub1)this).aBool8459) {
                OpenGL.glColorMask(bool, bool, bool, true);
                ((Class505_Sub1)this).aBool8459 = bool;
            }
            if (bool_276_ != ((Class505_Sub1)this).aBool8408) {
				if (bool_276_) {
					OpenGL.glEnable(3008);
				} else {
					OpenGL.glDisable(3008);
				}
                ((Class505_Sub1)this).aBool8408 = bool_276_;
            }
            if (i_275_ != ((Class505_Sub1)this).anInt8384) {
                if (i_275_ == 1) {
                    OpenGL.glEnable(3042);
                    OpenGL.glBlendFunc(770, 771);
                } else if (i_275_ == 2) {
                    OpenGL.glEnable(3042);
                    OpenGL.glBlendFunc(1, 1);
                } else if (i_275_ == 3) {
					OpenGL.glEnable(3042);
					OpenGL.glBlendFunc(774, 1);
				} else {
					OpenGL.glDisable(3042);
				}
                ((Class505_Sub1)this).anInt8384 = i_275_;
            }
            ((Class505_Sub1)this).anInt8366 = i;
            ((Class505_Sub1)this).anInt8382 &= ~0xc;
        }
    }

    public final int za() {
        return (((Class505_Sub1)this).anInt8371 + ((Class505_Sub1)this).anInt8370 + ((Class505_Sub1)this).anInt8372);
    }

    public boolean method8407() {
        return false;
    }

    public final void method8673(int i, int i_277_, int i_278_, int i_279_) {
        ((Class505_Sub1)this).aClass170_8357.method2884(i, i_277_, i_278_, i_279_);
    }

    final synchronized void method13625(int i, int i_280_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_280_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8375.method7877(class282_sub38, 1088910979);
    }

    void method8485() {
		for (Class282 class282 = ((Class505_Sub1)this).aClass473_8369.method7859(557753103); class282 != null; class282 = ((Class505_Sub1)this).aClass473_8369.method7857((byte)-57)) {
			((Class282_Sub1_Sub1)class282).method15450();
		}
		if (((Class505_Sub1)this).aClass170_8357 != null) {
			((Class505_Sub1)this).aClass170_8357.method2898();
		}
        if (((Class505_Sub1)this).aBool8367) {
            Class13.method508(false, true, -1169813027);
            ((Class505_Sub1)this).aBool8367 = false;
        }
    }

    final void method13626() {
        ((Class505_Sub1)this).aClass384_8394.method6562(((Class505_Sub1)this).aClass384_8348);
        ((Class505_Sub1)this).aClass384_8394.method6523(((Class505_Sub1)this).aClass384_8442);
        ((Class505_Sub1)this).aClass384_8394.method6539(((Class505_Sub1)this).aFloatArrayArray8395[0]);
        ((Class505_Sub1)this).aClass384_8394.method6540(((Class505_Sub1)this).aFloatArrayArray8395[1]);
        ((Class505_Sub1)this).aClass384_8394.method6535(((Class505_Sub1)this).aFloatArrayArray8395[2]);
        ((Class505_Sub1)this).aClass384_8394.method6536(((Class505_Sub1)this).aFloatArrayArray8395[3]);
        ((Class505_Sub1)this).aClass384_8394.method6613(((Class505_Sub1)this).aFloatArrayArray8395[4]);
        ((Class505_Sub1)this).aClass384_8394.method6538(((Class505_Sub1)this).aFloatArrayArray8395[5]);
    }

    final synchronized void method13627(long l) {
        Class282 class282 = new Class282();
        class282.aLong3379 = l * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8379.method7877(class282, 326472863);
    }

    final synchronized void method13628(int i) {
        Class282 class282 = new Class282();
        class282.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8461.method7877(class282, 1496395811);
    }

    static int method13629(Class150 class150) {
        switch (class150.anInt1958 * -1436290903) {
            case 4:
                return 6410;
            case 6:
                return 6408;
            case 5:
                return 6407;
            case 0:
                return 6409;
            case 8:
                return 6402;
            default:
                throw new IllegalStateException();
            case 1:
                return 6406;
        }
    }

    static int method13630(Class150 class150, Class76 class76) {
        if (class76 == Class76.aClass76_751) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 6407;
                case 4:
                    return 6410;
                case 6:
                    return 6408;
                case 0:
                    return 6409;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 6406;
            }
        }
        if (class76 == Class76.aClass76_752) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 32852;
                case 4:
                    return 36219;
                case 1:
                    return 32830;
                case 6:
                    return 32859;
                case 0:
                    return 32834;
                case 8:
                    return 33189;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_749) {
            switch (class150.anInt1958 * -1436290903) {
                case 8:
                    return 33190;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_755) {
            switch (class150.anInt1958 * -1436290903) {
                case 6:
                    return 34842;
                case 1:
                    return 34844;
                case 0:
                    return 34846;
                case 4:
                    return 34847;
                default:
                    throw new IllegalArgumentException();
                case 5:
                    return 34843;
            }
        }
        if (class76 == Class76.aClass76_758) {
            switch (class150.anInt1958 * -1436290903) {
                case 0:
                    return 34840;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 34838;
                case 5:
                    return 34837;
                case 4:
                    return 34841;
                case 6:
                    return 34836;
            }
        }
        throw new IllegalArgumentException();
    }

    final void method13631(int i) {
		if (i == 1) {
			method13717(7681, 7681);
		} else if (i == 0) {
			method13717(8448, 8448);
		} else if (i == 2) {
			method13717(34165, 7681);
		} else if (i == 3) {
			method13717(260, 8448);
		} else if (i == 4) {
			method13717(34023, 34023);
		}
    }

    static {
        aFloatArray8322 = new float[4];
    }

    final void method13632() {
        if (((Class505_Sub1)this).anInt8409 != 1) {
            ((Class505_Sub1)this).anInt8409 = 1;
            method13588();
            method13584();
            method13671();
            OpenGL.glMatrixMode(5888);
            OpenGL.glLoadIdentity();
            ((Class505_Sub1)this).anInt8382 &= ~0x8;
        }
    }

    void method8596(int i, int i_281_) throws Exception_Sub3 {
        try {
            aClass158_Sub2_5841.method14344();
        } catch (Exception exception) {
            /* empty */
        }
		if (anInterface22_5834 != null) {
			anInterface22_5834.method161(-2009873144);
		}
    }

    public Class384 method8571() {
        return ((Class505_Sub1)this).aClass384_8360;
    }

    public void method8484() {
        OpenGL.glFinish();
    }

    public final void method8423(Class294 class294) {
        ((Class505_Sub1)this).aClass294_8389.method5209(class294);
        ((Class505_Sub1)this).aClass384_8348.method6522(((Class505_Sub1)this).aClass294_8389);
        ((Class505_Sub1)this).aClass294_8405.method5209(class294);
        ((Class505_Sub1)this).aClass294_8405.method5207();
        ((Class505_Sub1)this).aClass384_8391.method6522(((Class505_Sub1)this).aClass294_8405);
        method13597();
		if (((Class505_Sub1)this).anInt8409 != 1) {
			method13748();
		}
    }

    public void method8573(float f, float f_282_, float f_283_, float[] fs) {
        float f_284_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * f_282_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * f_283_));
        float f_285_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * f_282_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * f_283_));
        float f_286_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * f_282_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * f_283_));
        float f_287_ = (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[14] + ((Class505_Sub1)this).aClass384_8348.aFloatArray4667[2] * f + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[6] * f_282_) + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[10] * f_283_));
        fs[0] = (((Class505_Sub1)this).aFloat8315 + ((Class505_Sub1)this).aFloat8398 * f_285_ / f_284_);
        fs[1] = (((Class505_Sub1)this).aFloat8399 + ((Class505_Sub1)this).aFloat8400 * f_286_ / f_284_);
        fs[2] = f_287_;
    }

    final void method13633(Class384 class384) {
        OpenGL.glPushMatrix();
        OpenGL.glMultMatrixf(class384.aFloatArray4667, 0);
    }

    public final int dd() {
        return (((Class505_Sub1)this).anInt8371 + ((Class505_Sub1)this).anInt8370 + ((Class505_Sub1)this).anInt8372);
    }

    public int method8538(int i, int i_288_, int i_289_, int i_290_, int i_291_, int i_292_) {
        int i_293_ = 0;
        float f = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_288_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_289_));
        float f_294_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i_290_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_291_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_292_));
        float f_295_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_288_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_289_));
        float f_296_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i_290_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_291_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_292_));
		if (f < -f_295_ && f_294_ < -f_296_) {
			i_293_ |= 0x10;
		} else if (f > f_295_ && f_294_ > f_296_) {
			i_293_ |= 0x20;
		}
        float f_297_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_288_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_289_));
        float f_298_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i_290_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_291_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_292_));
		if (f_297_ < -f_295_ && f_298_ < -f_296_) {
			i_293_ |= 0x1;
		}
		if (f_297_ > f_295_ && f_298_ > f_296_) {
			i_293_ |= 0x2;
		}
        float f_299_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_288_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_289_));
        float f_300_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i_290_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_291_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_292_));
		if (f_299_ < -f_295_ && f_300_ < -f_296_) {
			i_293_ |= 0x4;
		}
		if (f_299_ > f_295_ && f_300_ > f_296_) {
			i_293_ |= 0x8;
		}
        return i_293_;
    }

    public void en(float f, float f_301_) {
        ((Class505_Sub1)this).aFloat8364 = f;
        ((Class505_Sub1)this).aFloat8320 = f_301_;
        method13671();
    }

    final void method13634() {
        if (((Class505_Sub1)this).aBool8457) {
            OpenGL.glMatrixMode(5890);
            OpenGL.glLoadIdentity();
            OpenGL.glMatrixMode(5888);
            ((Class505_Sub1)this).aBool8457 = false;
        }
    }

    public final int df() {
        return (((Class505_Sub1)this).anInt8371 + ((Class505_Sub1)this).anInt8370 + ((Class505_Sub1)this).anInt8372);
    }

    final void method13635(float f, float f_302_, float f_303_, float f_304_) {
        aFloatArray8497[0] = f;
        aFloatArray8497[1] = f_302_;
        aFloatArray8497[2] = f_303_;
        aFloatArray8497[3] = f_304_;
        OpenGL.glTexEnvfv(8960, 8705, aFloatArray8497, 0);
    }

    public boolean method8489() {
        return true;
    }

    public boolean method8674() {
        return true;
    }

    public boolean method8528() {
        return true;
    }

    public Class8 method8625(Class414 class414, Class91[] class91s, boolean bool) {
        return new Class8_Sub5(this, class414, class91s, bool);
    }

    public boolean method8599() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && (((Class505_Sub1)this).anInt8475 <= 1 || ((Class505_Sub1)this).aBool8344));
    }

    public boolean method8464() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && (((Class505_Sub1)this).anInt8475 <= 1 || ((Class505_Sub1)this).aBool8344));
    }

    final synchronized void method13636(int i, int i_305_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_305_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8375.method7877(class282_sub38, -373446162);
    }

    public Class160 method8552(int i, int i_306_, int i_307_, int i_308_, boolean bool) {
        return new Class160_Sub2(this, i, i_306_, i_307_, i_308_);
    }

    public boolean method8649() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && (((Class505_Sub1)this).anInt8475 <= 1 || ((Class505_Sub1)this).aBool8344));
    }

    final void method13637() {
        if (((Class505_Sub1)this).anInt8382 != 2) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13581(-2);
            ((Class505_Sub1)this).anInt8382 = 2;
        }
    }

    void method13638() {
        int i;
        for (i = 0; i < ((Class505_Sub1)this).anInt8437; i++) {
            Class282_Sub24 class282_sub24 = ((Class505_Sub1)this).aClass282_Sub24Array8435[i];
            int i_309_ = 16386 + i;
            aFloatArray8322[0] = (float)class282_sub24.method12368((byte)11);
            aFloatArray8322[1] = (float)class282_sub24.method12369(1534020223);
            aFloatArray8322[2] = (float)class282_sub24.method12394(1619249215);
            aFloatArray8322[3] = 1.0F;
            OpenGL.glLightfv(i_309_, 4611, aFloatArray8322, 0);
            int i_310_ = class282_sub24.method12371(-2140071642);
            float f = class282_sub24.method12395(-227216021) / 255.0F;
            aFloatArray8322[0] = (float)(i_310_ >> 16 & 0xff) * f;
            aFloatArray8322[1] = (float)(i_310_ >> 8 & 0xff) * f;
            aFloatArray8322[2] = (float)(i_310_ & 0xff) * f;
            OpenGL.glLightfv(i_309_, 4609, aFloatArray8322, 0);
            OpenGL.glLightf(i_309_, 4617, (1.0F / (float)(class282_sub24.method12370(-789603523) * class282_sub24.method12370(-789603523))));
            OpenGL.glEnable(i_309_);
        }
		for (/**/; i < ((Class505_Sub1)this).anInt8436; i++) {
			OpenGL.glDisable(16386 + i);
		}
        ((Class505_Sub1)this).anInt8436 = ((Class505_Sub1)this).anInt8437;
    }

    void method13639() {
        int i = aClass158_5853.method2714();
        int i_311_ = aClass158_5853.method2716();
        ((Class505_Sub1)this).aClass384_8385.method6530(0.0F, (float)i, 0.0F, (float)i_311_, -1.0F, 1.0F);
        method8421();
        method13586();
        L();
    }

    final void method13640(int i, int i_312_) {
        ((Class505_Sub1)this).anInt8453 = i;
        ((Class505_Sub1)this).anInt8416 = i_312_;
        method13584();
        method13570();
    }

    public final void method8603(int i, int i_313_, int i_314_, int i_315_) {
        ((Class505_Sub1)this).aClass170_8357.method2884(i, i_313_, i_314_, i_315_);
    }

    public boolean method8502() {
        return true;
    }

    public final Class384 method8587() {
        return new Class384(((Class505_Sub1)this).aClass384_8442);
    }

    public boolean method8614() {
        return true;
    }

    final void method13641() {
        OpenGL.glPopMatrix();
    }

    public void method8491(boolean bool) {
        /* empty */
    }

    Class158_Sub2 method8558(Canvas canvas, int i, int i_316_) {
        return new Class158_Sub2_Sub1(this, canvas);
    }

    final void method13642(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8440) {
            ((Class505_Sub1)this).aBool8440 = bool;
            method13747();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    public void method8445() {
        if (((Class505_Sub1)this).aBool8487 && aClass158_5853 != null) {
            int i = ((Class505_Sub1)this).anInt8413;
            int i_317_ = ((Class505_Sub1)this).anInt8412;
            int i_318_ = ((Class505_Sub1)this).anInt8415;
            int i_319_ = ((Class505_Sub1)this).anInt8478;
            L();
            int i_320_ = ((Class505_Sub1)this).anInt8417;
            int i_321_ = ((Class505_Sub1)this).anInt8418;
            int i_322_ = ((Class505_Sub1)this).anInt8419;
            int i_323_ = ((Class505_Sub1)this).anInt8347;
            method8421();
            OpenGL.glReadBuffer(1028);
            OpenGL.glDrawBuffer(1029);
            method13586();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13654(null);
            method13581(-2);
            method13612(1);
            method13624(0);
            OpenGL.glMatrixMode(5889);
            OpenGL.glLoadIdentity();
            OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
            OpenGL.glMatrixMode(5888);
            OpenGL.glLoadIdentity();
            OpenGL.glRasterPos2i(0, 0);
            OpenGL.glCopyPixels(0, 0, aClass158_5853.method2714(), aClass158_5853.method2716(), 6144);
            OpenGL.glFlush();
            OpenGL.glReadBuffer(1029);
            OpenGL.glDrawBuffer(1029);
            r(i, i_318_, i_317_, i_319_);
            method8617(i_320_, i_321_, i_322_, i_323_);
        }
    }

    public void method8497() {
        if (((Class505_Sub1)this).aBool8487 && aClass158_5853 != null) {
            int i = ((Class505_Sub1)this).anInt8413;
            int i_324_ = ((Class505_Sub1)this).anInt8412;
            int i_325_ = ((Class505_Sub1)this).anInt8415;
            int i_326_ = ((Class505_Sub1)this).anInt8478;
            L();
            int i_327_ = ((Class505_Sub1)this).anInt8417;
            int i_328_ = ((Class505_Sub1)this).anInt8418;
            int i_329_ = ((Class505_Sub1)this).anInt8419;
            int i_330_ = ((Class505_Sub1)this).anInt8347;
            method8421();
            OpenGL.glReadBuffer(1028);
            OpenGL.glDrawBuffer(1029);
            method13586();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13654(null);
            method13581(-2);
            method13612(1);
            method13624(0);
            OpenGL.glMatrixMode(5889);
            OpenGL.glLoadIdentity();
            OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
            OpenGL.glMatrixMode(5888);
            OpenGL.glLoadIdentity();
            OpenGL.glRasterPos2i(0, 0);
            OpenGL.glCopyPixels(0, 0, aClass158_5853.method2714(), aClass158_5853.method2716(), 6144);
            OpenGL.glFlush();
            OpenGL.glReadBuffer(1029);
            OpenGL.glDrawBuffer(1029);
            r(i, i_325_, i_324_, i_326_);
            method8617(i_327_, i_328_, i_329_, i_330_);
        }
    }

    final void method13643() {
        if (((Class505_Sub1)this).anInt8409 != 0) {
            ((Class505_Sub1)this).anInt8409 = 0;
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    public void fi(int i, int i_331_) {
        int i_332_ = 0;
        if ((i & 0x1) != 0) {
            OpenGL.glClearColor((float)(i_331_ & 0xff0000) / 1.671168E7F, (float)(i_331_ & 0xff00) / 65280.0F, (float)(i_331_ & 0xff) / 255.0F, (float)(i_331_ >>> 24) / 255.0F);
            i_332_ = 16384;
        }
        if ((i & 0x2) != 0) {
            method13623(true);
            i_332_ |= 0x100;
        }
		if ((i & 0x4) != 0) {
			i_332_ |= 0x400;
		}
        OpenGL.glClear(i_332_);
    }

    public boolean method8498() {
        return true;
    }

    public final void J(int i) {
        ((Class505_Sub1)this).anInt8473 = 0;
		for (/**/; i > 1; i >>= 1) {
			((Class505_Sub1)this).anInt8473++;
		}
        ((Class505_Sub1)this).anInt8466 = 1 << ((Class505_Sub1)this).anInt8473;
    }

    public void ej(float f, float f_333_) {
        ((Class505_Sub1)this).aFloat8364 = f;
        ((Class505_Sub1)this).aFloat8320 = f_333_;
        method13671();
    }

    public final void ez() {
        if (aClass158_5853 != null) {
            ((Class505_Sub1)this).anInt8413 = 0;
            ((Class505_Sub1)this).anInt8415 = 0;
            ((Class505_Sub1)this).anInt8412 = aClass158_5853.method2714();
            ((Class505_Sub1)this).anInt8478 = aClass158_5853.method2716();
            OpenGL.glDisable(3089);
        }
    }

    public final void eh() {
        if (aClass158_5853 != null) {
            ((Class505_Sub1)this).anInt8413 = 0;
            ((Class505_Sub1)this).anInt8415 = 0;
            ((Class505_Sub1)this).anInt8412 = aClass158_5853.method2714();
            ((Class505_Sub1)this).anInt8478 = aClass158_5853.method2716();
            OpenGL.glDisable(3089);
        }
    }

    public final void fm(int i, int i_334_, int i_335_, int i_336_) {
        if (aClass158_5853 != null) {
			if (i < 0) {
				i = 0;
			}
			if (i_335_ > aClass158_5853.method2714()) {
				i_335_ = aClass158_5853.method2714();
			}
			if (i_334_ < 0) {
				i_334_ = 0;
			}
			if (i_336_ > aClass158_5853.method2716()) {
				i_336_ = aClass158_5853.method2716();
			}
            ((Class505_Sub1)this).anInt8413 = i;
            ((Class505_Sub1)this).anInt8415 = i_334_;
            ((Class505_Sub1)this).anInt8412 = i_335_;
            ((Class505_Sub1)this).anInt8478 = i_336_;
            OpenGL.glEnable(3089);
            method13570();
        }
    }

    public final void fc(int i, int i_337_, int i_338_, int i_339_) {
        if (aClass158_5853 != null) {
			if (i < 0) {
				i = 0;
			}
			if (i_338_ > aClass158_5853.method2714()) {
				i_338_ = aClass158_5853.method2714();
			}
			if (i_337_ < 0) {
				i_337_ = 0;
			}
			if (i_339_ > aClass158_5853.method2716()) {
				i_339_ = aClass158_5853.method2716();
			}
            ((Class505_Sub1)this).anInt8413 = i;
            ((Class505_Sub1)this).anInt8415 = i_337_;
            ((Class505_Sub1)this).anInt8412 = i_338_;
            ((Class505_Sub1)this).anInt8478 = i_339_;
            OpenGL.glEnable(3089);
            method13570();
        }
    }

    public final void fe(int i, int i_340_, int i_341_, int i_342_) {
		if (((Class505_Sub1)this).anInt8413 < i) {
			((Class505_Sub1)this).anInt8413 = i;
		}
		if (((Class505_Sub1)this).anInt8412 > i_341_) {
			((Class505_Sub1)this).anInt8412 = i_341_;
		}
		if (((Class505_Sub1)this).anInt8415 < i_340_) {
			((Class505_Sub1)this).anInt8415 = i_340_;
		}
		if (((Class505_Sub1)this).anInt8478 > i_342_) {
			((Class505_Sub1)this).anInt8478 = i_342_;
		}
        OpenGL.glEnable(3089);
        method13570();
    }

    public final void fk(int i, int i_343_, int i_344_, int i_345_) {
		if (((Class505_Sub1)this).anInt8413 < i) {
			((Class505_Sub1)this).anInt8413 = i;
		}
		if (((Class505_Sub1)this).anInt8412 > i_344_) {
			((Class505_Sub1)this).anInt8412 = i_344_;
		}
		if (((Class505_Sub1)this).anInt8415 < i_343_) {
			((Class505_Sub1)this).anInt8415 = i_343_;
		}
		if (((Class505_Sub1)this).anInt8478 > i_345_) {
			((Class505_Sub1)this).anInt8478 = i_345_;
		}
        OpenGL.glEnable(3089);
        method13570();
    }

    final void method13644(Interface14 interface14) {
        if (((Class505_Sub1)this).anInterface14_8483 != interface14) {
			if (((Class505_Sub1)this).aBool8309) {
				OpenGL.glBindBufferARB(34962, interface14.method1());
			}
            ((Class505_Sub1)this).anInterface14_8483 = interface14;
        }
    }

    final void method13645() {
        if (((Class505_Sub1)this).anInt8409 != 2) {
            ((Class505_Sub1)this).anInt8409 = 2;
            method13719(((Class505_Sub1)this).aClass384_8442.aFloatArray4667);
            method13748();
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0x7;
        }
    }

    public void method8480(boolean bool) {
        /* empty */
    }

    public void fu(int i, int i_346_) {
        int i_347_ = 0;
        if ((i & 0x1) != 0) {
            OpenGL.glClearColor((float)(i_346_ & 0xff0000) / 1.671168E7F, (float)(i_346_ & 0xff00) / 65280.0F, (float)(i_346_ & 0xff) / 255.0F, (float)(i_346_ >>> 24) / 255.0F);
            i_347_ = 16384;
        }
        if ((i & 0x2) != 0) {
            method13623(true);
            i_347_ |= 0x100;
        }
		if ((i & 0x4) != 0) {
			i_347_ |= 0x400;
		}
        OpenGL.glClear(i_347_);
    }

    public final synchronized void method8487(int i) {
        int i_348_ = 0;
        i &= 0x7fffffff;
        while (!((Class505_Sub1)this).aClass473_8486.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8486.method7858((byte)-39);
            anIntArray8381[i_348_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8371 -= class282_sub38.anInt8002 * -570797415;
            if (i_348_ == 1000) {
                OpenGL.glDeleteBuffersARB(i_348_, anIntArray8381, 0);
                i_348_ = 0;
            }
        }
        if (i_348_ > 0) {
            OpenGL.glDeleteBuffersARB(i_348_, anIntArray8381, 0);
            i_348_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8375.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8375.method7858((byte)-22);
            anIntArray8381[i_348_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8370 -= class282_sub38.anInt8002 * -570797415;
            if (i_348_ == 1000) {
                OpenGL.glDeleteTextures(i_348_, anIntArray8381, 0);
                i_348_ = 0;
            }
        }
        if (i_348_ > 0) {
            OpenGL.glDeleteTextures(i_348_, anIntArray8381, 0);
            i_348_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8376.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8376.method7858((byte)-92);
            anIntArray8381[i_348_++] = class282_sub38.anInt8002 * -570797415;
            if (i_348_ == 1000) {
                OpenGL.glDeleteFramebuffersEXT(i_348_, anIntArray8381, 0);
                i_348_ = 0;
            }
        }
        if (i_348_ > 0) {
            OpenGL.glDeleteFramebuffersEXT(i_348_, anIntArray8381, 0);
            i_348_ = 0;
        }
        while (!((Class505_Sub1)this).aClass473_8377.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8377.method7858((byte)-76);
            anIntArray8381[i_348_++] = (int)(class282_sub38.aLong3379 * -3442165056282524525L);
            ((Class505_Sub1)this).anInt8372 -= class282_sub38.anInt8002 * -570797415;
            if (i_348_ == 1000) {
                OpenGL.glDeleteRenderbuffersEXT(i_348_, anIntArray8381, 0);
                i_348_ = 0;
            }
        }
        if (i_348_ > 0) {
            OpenGL.glDeleteRenderbuffersEXT(i_348_, anIntArray8381, 0);
            boolean bool = false;
        }
        while (!((Class505_Sub1)this).aClass473_8373.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8373.method7858((byte)-80);
            OpenGL.glDeleteLists((int)(class282_sub38.aLong3379 * -3442165056282524525L), class282_sub38.anInt8002 * -570797415);
        }
        while (!((Class505_Sub1)this).aClass473_8461.method7861(141891001)) {
            Class282 class282 = ((Class505_Sub1)this).aClass473_8461.method7858((byte)-13);
            OpenGL.glDeleteProgramARB((int)(class282.aLong3379 * -3442165056282524525L));
        }
        while (!((Class505_Sub1)this).aClass473_8379.method7861(141891001)) {
            Class282 class282 = ((Class505_Sub1)this).aClass473_8379.method7858((byte)-70);
            OpenGL.glDeleteShader((int)(class282.aLong3379 * -3442165056282524525L));
        }
        while (!((Class505_Sub1)this).aClass473_8373.method7861(141891001)) {
            Class282_Sub38 class282_sub38 = (Class282_Sub38)((Class505_Sub1)this).aClass473_8373.method7858((byte)-1);
            OpenGL.glDeleteLists((int)(class282_sub38.aLong3379 * -3442165056282524525L), class282_sub38.anInt8002 * -570797415);
        }
        ((Class505_Sub1)this).aClass167_8481.method2860();
        if (za() > 100663296 && (Class169.method2869(1803310072) > ((Class505_Sub1)this).aLong8316 + 60000L)) {
            System.gc();
            ((Class505_Sub1)this).aLong8316 = Class169.method2869(1554670012);
        }
        ((Class505_Sub1)this).anInt8368 = i;
    }

    public void method8519(int i, int i_349_, int i_350_, int i_351_, int i_352_, int i_353_) {
        float f = (float)i + 0.35F;
        float f_354_ = (float)i_349_ + 0.35F;
        float f_355_ = f + (float)i_350_ - 1.0F;
        float f_356_ = f_354_ + (float)i_351_ - 1.0F;
        method13659();
        method13624(i_353_);
        OpenGL.glColor4ub((byte)(i_352_ >> 16), (byte)(i_352_ >> 8), (byte)i_352_, (byte)(i_352_ >> 24));
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glDisable(32925);
		}
        OpenGL.glBegin(2);
        OpenGL.glVertex2f(f, f_354_);
        OpenGL.glVertex2f(f, f_356_);
        OpenGL.glVertex2f(f_355_, f_356_);
        OpenGL.glVertex2f(f_355_, f_354_);
        OpenGL.glEnd();
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glEnable(32925);
		}
    }

    public void method8511(int i, int i_357_, int i_358_, int i_359_, int i_360_, int i_361_) {
        float f = (float)i + 0.35F;
        float f_362_ = (float)i_357_ + 0.35F;
        float f_363_ = f + (float)i_358_ - 1.0F;
        float f_364_ = f_362_ + (float)i_359_ - 1.0F;
        method13659();
        method13624(i_361_);
        OpenGL.glColor4ub((byte)(i_360_ >> 16), (byte)(i_360_ >> 8), (byte)i_360_, (byte)(i_360_ >> 24));
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glDisable(32925);
		}
        OpenGL.glBegin(2);
        OpenGL.glVertex2f(f, f_362_);
        OpenGL.glVertex2f(f, f_364_);
        OpenGL.glVertex2f(f_363_, f_364_);
        OpenGL.glVertex2f(f_363_, f_362_);
        OpenGL.glEnd();
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glEnable(32925);
		}
    }

    public void method8520(int i, int i_365_, int i_366_, int i_367_, int i_368_, int i_369_) {
        float f = (float)i + 0.35F;
        float f_370_ = (float)i_365_ + 0.35F;
        float f_371_ = f + (float)i_366_ - 1.0F;
        float f_372_ = f_370_ + (float)i_367_ - 1.0F;
        method13659();
        method13624(i_369_);
        OpenGL.glColor4ub((byte)(i_368_ >> 16), (byte)(i_368_ >> 8), (byte)i_368_, (byte)(i_368_ >> 24));
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glDisable(32925);
		}
        OpenGL.glBegin(2);
        OpenGL.glVertex2f(f, f_370_);
        OpenGL.glVertex2f(f, f_372_);
        OpenGL.glVertex2f(f_371_, f_372_);
        OpenGL.glVertex2f(f_371_, f_370_);
        OpenGL.glEnd();
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glEnable(32925);
		}
    }

    public void fp(int i, int i_373_, int i_374_, int i_375_, int i_376_, int i_377_) {
        float f = (float)i + 0.35F;
        float f_378_ = (float)i_373_ + 0.35F;
        float f_379_ = f + (float)i_374_;
        float f_380_ = f_378_ + (float)i_375_;
        method13659();
        method13624(i_377_);
        OpenGL.glColor4ub((byte)(i_376_ >> 16), (byte)(i_376_ >> 8), (byte)i_376_, (byte)(i_376_ >> 24));
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glDisable(32925);
		}
        OpenGL.glBegin(7);
        OpenGL.glVertex2f(f, f_378_);
        OpenGL.glVertex2f(f, f_380_);
        OpenGL.glVertex2f(f_379_, f_380_);
        OpenGL.glVertex2f(f_379_, f_378_);
        OpenGL.glEnd();
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glEnable(32925);
		}
    }

    public void fb(int i, int i_381_, int i_382_, int i_383_, int i_384_, int i_385_) {
        float f = (float)i + 0.35F;
        float f_386_ = (float)i_381_ + 0.35F;
        float f_387_ = f + (float)i_382_;
        float f_388_ = f_386_ + (float)i_383_;
        method13659();
        method13624(i_385_);
        OpenGL.glColor4ub((byte)(i_384_ >> 16), (byte)(i_384_ >> 8), (byte)i_384_, (byte)(i_384_ >> 24));
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glDisable(32925);
		}
        OpenGL.glBegin(7);
        OpenGL.glVertex2f(f, f_386_);
        OpenGL.glVertex2f(f, f_388_);
        OpenGL.glVertex2f(f_387_, f_388_);
        OpenGL.glVertex2f(f_387_, f_386_);
        OpenGL.glEnd();
		if (((Class505_Sub1)this).aBool8342) {
			OpenGL.glEnable(32925);
		}
    }

    public void fv(int i, int i_389_, int i_390_, int i_391_, int i_392_, int i_393_, byte[] is, int i_394_, int i_395_) {
        float f;
        float f_396_;
        if (((Class505_Sub1)this).aClass137_Sub1_Sub1_8462 == null || (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9087 < i_390_) || (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9086 < i_391_)) {
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462 = Class137_Sub1_Sub1.method15540(this, Class150.aClass150_1951, Class76.aClass76_751, i_390_, i_391_, false, is, Class150.aClass150_1951);
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462.method14445(false, false);
            f = ((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10134;
            f_396_ = (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10132);
        } else {
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462.method14455(0, 0, i_390_, i_391_, is, Class150.aClass150_1951, 0, 0, false);
            f = (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10134 * (float)i_391_ / (float)(((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9086));
            f_396_ = ((((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10132) * (float)i_390_ / (float)(((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9087));
        }
        method13637();
        method13654(((Class505_Sub1)this).aClass137_Sub1_Sub1_8462);
        method13624(i_395_);
        OpenGL.glColor4ub((byte)(i_392_ >> 16), (byte)(i_392_ >> 8), (byte)i_392_, (byte)(i_392_ >> 24));
        method13617(i_393_);
        method13717(34165, 34165);
        method13595(0, 34166, 768);
        method13595(2, 5890, 770);
        method13616(0, 34166, 770);
        method13616(2, 5890, 770);
        float f_397_ = (float)i;
        float f_398_ = (float)i_389_;
        float f_399_ = f_397_ + (float)i_390_;
        float f_400_ = f_398_ + (float)i_391_;
        OpenGL.glBegin(7);
        OpenGL.glTexCoord2f(0.0F, 0.0F);
        OpenGL.glVertex2f(f_397_, f_398_);
        OpenGL.glTexCoord2f(0.0F, f_396_);
        OpenGL.glVertex2f(f_397_, f_400_);
        OpenGL.glTexCoord2f(f, f_396_);
        OpenGL.glVertex2f(f_399_, f_400_);
        OpenGL.glTexCoord2f(f, 0.0F);
        OpenGL.glVertex2f(f_399_, f_398_);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
        method13595(2, 34166, 770);
        method13616(0, 5890, 770);
        method13616(2, 34166, 770);
    }

    public final void method8601() {
        if (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129()) {
            ((Class505_Sub1)this).aClass170_8357.method2885(((Class505_Sub1)this).aClass282_Sub5_Sub1_8444);
            ((Class505_Sub1)this).aClass167_8481.method2861();
        }
    }

    void fr(int i, int i_401_, int i_402_, int i_403_, int i_404_) {
		if (i_402_ < 0) {
			i_402_ = -i_402_;
		}
        if (i + i_402_ >= ((Class505_Sub1)this).anInt8413 && i - i_402_ <= ((Class505_Sub1)this).anInt8412 && i_401_ + i_402_ >= ((Class505_Sub1)this).anInt8415 && i_401_ - i_402_ <= ((Class505_Sub1)this).anInt8478) {
            method13659();
            method13624(i_404_);
            OpenGL.glColor4ub((byte)(i_403_ >> 16), (byte)(i_403_ >> 8), (byte)i_403_, (byte)(i_403_ >> 24));
            float f = (float)i + 0.35F;
            float f_405_ = (float)i_401_ + 0.35F;
            int i_406_ = i_402_ << 1;
            if ((float)i_406_ < ((Class505_Sub1)this).aFloat8414) {
                OpenGL.glBegin(7);
                OpenGL.glVertex2f(f + 1.0F, f_405_ + 1.0F);
                OpenGL.glVertex2f(f + 1.0F, f_405_ - 1.0F);
                OpenGL.glVertex2f(f - 1.0F, f_405_ - 1.0F);
                OpenGL.glVertex2f(f - 1.0F, f_405_ + 1.0F);
                OpenGL.glEnd();
            } else if ((float)i_406_ <= ((Class505_Sub1)this).aFloat8489) {
                OpenGL.glEnable(2832);
                OpenGL.glPointSize((float)i_406_);
                OpenGL.glBegin(0);
                OpenGL.glVertex2f(f, f_405_);
                OpenGL.glEnd();
                OpenGL.glDisable(2832);
            } else {
                OpenGL.glBegin(6);
                OpenGL.glVertex2f(f, f_405_);
                int i_407_ = 262144 / (6 * i_402_);
				if (i_407_ <= 64) {
					i_407_ = 64;
				} else if (i_407_ > 512) {
					i_407_ = 512;
				}
                i_407_ = Class51.method1072(i_407_, 960090928);
                OpenGL.glVertex2f(f + (float)i_402_, f_405_);
				for (int i_408_ = 16384 - i_407_; i_408_ > 0; i_408_ -= i_407_) {
					OpenGL.glVertex2f(f + (Class142.aFloatArray1665[i_408_] * (float)i_402_), f_405_ + (Class142.aFloatArray1666[i_408_]) * (float)i_402_);
				}
                OpenGL.glVertex2f(f + (float)i_402_, f_405_);
                OpenGL.glEnd();
            }
        }
    }

    void fw(int i, int i_409_, int i_410_, int i_411_, int i_412_) {
		if (i_410_ < 0) {
			i_410_ = -i_410_;
		}
        if (i + i_410_ >= ((Class505_Sub1)this).anInt8413 && i - i_410_ <= ((Class505_Sub1)this).anInt8412 && i_409_ + i_410_ >= ((Class505_Sub1)this).anInt8415 && i_409_ - i_410_ <= ((Class505_Sub1)this).anInt8478) {
            method13659();
            method13624(i_412_);
            OpenGL.glColor4ub((byte)(i_411_ >> 16), (byte)(i_411_ >> 8), (byte)i_411_, (byte)(i_411_ >> 24));
            float f = (float)i + 0.35F;
            float f_413_ = (float)i_409_ + 0.35F;
            int i_414_ = i_410_ << 1;
            if ((float)i_414_ < ((Class505_Sub1)this).aFloat8414) {
                OpenGL.glBegin(7);
                OpenGL.glVertex2f(f + 1.0F, f_413_ + 1.0F);
                OpenGL.glVertex2f(f + 1.0F, f_413_ - 1.0F);
                OpenGL.glVertex2f(f - 1.0F, f_413_ - 1.0F);
                OpenGL.glVertex2f(f - 1.0F, f_413_ + 1.0F);
                OpenGL.glEnd();
            } else if ((float)i_414_ <= ((Class505_Sub1)this).aFloat8489) {
                OpenGL.glEnable(2832);
                OpenGL.glPointSize((float)i_414_);
                OpenGL.glBegin(0);
                OpenGL.glVertex2f(f, f_413_);
                OpenGL.glEnd();
                OpenGL.glDisable(2832);
            } else {
                OpenGL.glBegin(6);
                OpenGL.glVertex2f(f, f_413_);
                int i_415_ = 262144 / (6 * i_410_);
				if (i_415_ <= 64) {
					i_415_ = 64;
				} else if (i_415_ > 512) {
					i_415_ = 512;
				}
                i_415_ = Class51.method1072(i_415_, 1964199200);
                OpenGL.glVertex2f(f + (float)i_410_, f_413_);
				for (int i_416_ = 16384 - i_415_; i_416_ > 0; i_416_ -= i_415_) {
					OpenGL.glVertex2f(f + (Class142.aFloatArray1665[i_416_] * (float)i_410_), f_413_ + (Class142.aFloatArray1666[i_416_]) * (float)i_410_);
				}
                OpenGL.glVertex2f(f + (float)i_410_, f_413_);
                OpenGL.glEnd();
            }
        }
    }

    public void method8516(int i, int i_417_, float f, int i_418_, int i_419_, float f_420_, int i_421_, int i_422_, float f_423_, int i_424_, int i_425_, int i_426_, int i_427_) {
        method13659();
        method13624(i_427_);
        OpenGL.glBegin(4);
        OpenGL.glColor4ub((byte)(i_424_ >> 16), (byte)(i_424_ >> 8), (byte)i_424_, (byte)(i_424_ >> 24));
        OpenGL.glVertex3f((float)i + 0.35F, (float)i_417_ + 0.35F, f);
        OpenGL.glColor4ub((byte)(i_425_ >> 16), (byte)(i_425_ >> 8), (byte)i_425_, (byte)(i_425_ >> 24));
        OpenGL.glVertex3f((float)i_418_ + 0.35F, (float)i_419_ + 0.35F, f_420_);
        OpenGL.glColor4ub((byte)(i_426_ >> 16), (byte)(i_426_ >> 8), (byte)i_426_, (byte)(i_426_ >> 24));
        OpenGL.glVertex3f((float)i_421_ + 0.35F, (float)i_422_ + 0.35F, f_423_);
        OpenGL.glEnd();
    }

    public void fq(int i, int i_428_, int i_429_, int i_430_, int i_431_) {
        method13659();
        method13624(i_431_);
        float f = (float)i + 0.35F;
        float f_432_ = (float)i_428_ + 0.35F;
        OpenGL.glColor4ub((byte)(i_430_ >> 16), (byte)(i_430_ >> 8), (byte)i_430_, (byte)(i_430_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f(f, f_432_);
        OpenGL.glVertex2f(f + (float)i_429_, f_432_);
        OpenGL.glEnd();
    }

    public boolean method8431() {
        return true;
    }

    public void fl(int i, int i_433_, int i_434_, int i_435_, int i_436_) {
        method13659();
        method13624(i_436_);
        float f = (float)i + 0.35F;
        float f_437_ = (float)i_433_ + 0.35F;
        OpenGL.glColor4ub((byte)(i_435_ >> 16), (byte)(i_435_ >> 8), (byte)i_435_, (byte)(i_435_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f(f, f_437_);
        OpenGL.glVertex2f(f, f_437_ + (float)i_434_);
        OpenGL.glEnd();
    }

    public void method8415(int i, int i_438_, int i_439_, int i_440_, int i_441_, int i_442_) {
        method13659();
        method13624(i_442_);
        float f = (float)i_439_ - (float)i;
        float f_443_ = (float)i_440_ - (float)i_438_;
		if (f == 0.0F && f_443_ == 0.0F) {
			f = 1.0F;
		} else {
			float f_444_ = (float)(1.0 / Math.sqrt((double)(f * f + f_443_ * f_443_)));
			f *= f_444_;
			f_443_ *= f_444_;
		}
        OpenGL.glColor4ub((byte)(i_441_ >> 16), (byte)(i_441_ >> 8), (byte)i_441_, (byte)(i_441_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_438_ + 0.35F);
        OpenGL.glVertex2f((float)i_439_ + f + 0.35F, (float)i_440_ + f_443_ + 0.35F);
        OpenGL.glEnd();
    }

    public void method8526(int i, int i_445_, int i_446_, int i_447_, int i_448_, int i_449_) {
        method13659();
        method13624(i_449_);
        float f = (float)i_446_ - (float)i;
        float f_450_ = (float)i_447_ - (float)i_445_;
		if (f == 0.0F && f_450_ == 0.0F) {
			f = 1.0F;
		} else {
			float f_451_ = (float)(1.0 / Math.sqrt((double)(f * f + f_450_ * f_450_)));
			f *= f_451_;
			f_450_ *= f_451_;
		}
        OpenGL.glColor4ub((byte)(i_448_ >> 16), (byte)(i_448_ >> 8), (byte)i_448_, (byte)(i_448_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_445_ + 0.35F);
        OpenGL.glVertex2f((float)i_446_ + f + 0.35F, (float)i_447_ + f_450_ + 0.35F);
        OpenGL.glEnd();
    }

    public void method8527(int i, int i_452_, int i_453_, int i_454_, int i_455_, int i_456_, int i_457_, int i_458_, int i_459_) {
        if (i != i_453_ || i_452_ != i_454_) {
            method13659();
            method13624(i_456_);
            float f = (float)i_453_ - (float)i;
            float f_460_ = (float)i_454_ - (float)i_452_;
            float f_461_ = (float)(1.0 / Math.sqrt((double)(f * f + f_460_ * f_460_)));
            f *= f_461_;
            f_460_ *= f_461_;
            OpenGL.glColor4ub((byte)(i_455_ >> 16), (byte)(i_455_ >> 8), (byte)i_455_, (byte)(i_455_ >> 24));
            i_459_ %= i_458_ + i_457_;
            float f_462_ = f * (float)i_457_;
            float f_463_ = f_460_ * (float)i_457_;
            float f_464_ = 0.0F;
            float f_465_ = 0.0F;
            float f_466_ = f_462_;
            float f_467_ = f_463_;
            if (i_459_ > i_457_) {
                f_464_ = f * (float)(i_457_ + i_458_ - i_459_);
                f_465_ = f_460_ * (float)(i_457_ + i_458_ - i_459_);
            } else {
                f_466_ = f * (float)(i_457_ - i_459_);
                f_467_ = f_460_ * (float)(i_457_ - i_459_);
            }
            float f_468_ = (float)i + 0.35F + f_464_;
            float f_469_ = (float)i_452_ + 0.35F + f_465_;
            float f_470_ = f * (float)i_458_;
            float f_471_ = f_460_ * (float)i_458_;
            for (; ; ) {
                if (i_453_ > i) {
					if (f_468_ > (float)i_453_ + 0.35F) {
						break;
					}
					if (f_468_ + f_466_ > (float)i_453_) {
						f_466_ = (float)i_453_ - f_468_;
					}
                } else {
					if (f_468_ < (float)i_453_ + 0.35F) {
						break;
					}
					if (f_468_ + f_466_ < (float)i_453_) {
						f_466_ = (float)i_453_ - f_468_;
					}
                }
                if (i_454_ > i_452_) {
					if (f_469_ > (float)i_454_ + 0.35F) {
						break;
					}
					if (f_469_ + f_467_ > (float)i_454_) {
						f_467_ = (float)i_454_ - f_469_;
					}
                } else {
					if (f_469_ < (float)i_454_ + 0.35F) {
						break;
					}
					if (f_469_ + f_467_ < (float)i_454_) {
						f_467_ = (float)i_454_ - f_469_;
					}
                }
                OpenGL.glBegin(1);
                OpenGL.glVertex2f(f_468_, f_469_);
                OpenGL.glVertex2f(f_468_ + f_466_, f_469_ + f_467_);
                OpenGL.glEnd();
                f_468_ += f_470_ + f_466_;
                f_469_ += f_471_ + f_467_;
                f_466_ = f_462_;
                f_467_ = f_463_;
            }
        }
    }

    final synchronized void method13646(long l) {
        Class282 class282 = new Class282();
        class282.aLong3379 = l * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8379.method7877(class282, -245143235);
    }

    public void method8529(int i, int i_472_, int i_473_, int i_474_, int i_475_, int i_476_, int i_477_, int i_478_, int i_479_) {
        if (i != i_473_ || i_472_ != i_474_) {
            method13659();
            method13624(i_476_);
            float f = (float)i_473_ - (float)i;
            float f_480_ = (float)i_474_ - (float)i_472_;
            float f_481_ = (float)(1.0 / Math.sqrt((double)(f * f + f_480_ * f_480_)));
            f *= f_481_;
            f_480_ *= f_481_;
            OpenGL.glColor4ub((byte)(i_475_ >> 16), (byte)(i_475_ >> 8), (byte)i_475_, (byte)(i_475_ >> 24));
            i_479_ %= i_478_ + i_477_;
            float f_482_ = f * (float)i_477_;
            float f_483_ = f_480_ * (float)i_477_;
            float f_484_ = 0.0F;
            float f_485_ = 0.0F;
            float f_486_ = f_482_;
            float f_487_ = f_483_;
            if (i_479_ > i_477_) {
                f_484_ = f * (float)(i_477_ + i_478_ - i_479_);
                f_485_ = f_480_ * (float)(i_477_ + i_478_ - i_479_);
            } else {
                f_486_ = f * (float)(i_477_ - i_479_);
                f_487_ = f_480_ * (float)(i_477_ - i_479_);
            }
            float f_488_ = (float)i + 0.35F + f_484_;
            float f_489_ = (float)i_472_ + 0.35F + f_485_;
            float f_490_ = f * (float)i_478_;
            float f_491_ = f_480_ * (float)i_478_;
            for (; ; ) {
                if (i_473_ > i) {
					if (f_488_ > (float)i_473_ + 0.35F) {
						break;
					}
					if (f_488_ + f_486_ > (float)i_473_) {
						f_486_ = (float)i_473_ - f_488_;
					}
                } else {
					if (f_488_ < (float)i_473_ + 0.35F) {
						break;
					}
					if (f_488_ + f_486_ < (float)i_473_) {
						f_486_ = (float)i_473_ - f_488_;
					}
                }
                if (i_474_ > i_472_) {
					if (f_489_ > (float)i_474_ + 0.35F) {
						break;
					}
					if (f_489_ + f_487_ > (float)i_474_) {
						f_487_ = (float)i_474_ - f_489_;
					}
                } else {
					if (f_489_ < (float)i_474_ + 0.35F) {
						break;
					}
					if (f_489_ + f_487_ < (float)i_474_) {
						f_487_ = (float)i_474_ - f_489_;
					}
                }
                OpenGL.glBegin(1);
                OpenGL.glVertex2f(f_488_, f_489_);
                OpenGL.glVertex2f(f_488_ + f_486_, f_489_ + f_487_);
                OpenGL.glEnd();
                f_488_ += f_490_ + f_486_;
                f_489_ += f_491_ + f_487_;
                f_486_ = f_482_;
                f_487_ = f_483_;
            }
        }
    }

    public void fo(int i, int i_492_, int i_493_, int i_494_, int i_495_) {
        method13659();
        method13624(i_495_);
        float f = (float)i + 0.35F;
        float f_496_ = (float)i_492_ + 0.35F;
        OpenGL.glColor4ub((byte)(i_494_ >> 16), (byte)(i_494_ >> 8), (byte)i_494_, (byte)(i_494_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f(f, f_496_);
        OpenGL.glVertex2f(f, f_496_ + (float)i_493_);
        OpenGL.glEnd();
    }

    public void method8576(int i, int i_497_, int i_498_, int i_499_, int i_500_, int i_501_, Class455 class455, int i_502_, int i_503_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(i_501_);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_504_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        float f_505_ = (float)i_498_ - (float)i;
        float f_506_ = (float)i_499_ - (float)i_497_;
        float f_507_ = (float)(1.0 / Math.sqrt((double)(f_505_ * f_505_ + f_506_ * f_506_)));
        f_505_ *= f_507_;
        f_506_ *= f_507_;
        OpenGL.glColor4ub((byte)(i_500_ >> 16), (byte)(i_500_ >> 8), (byte)i_500_, (byte)(i_500_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glTexCoord2f(f * (float)(i - i_502_), f_504_ * (float)(i_497_ - i_503_));
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_497_ + 0.35F);
        OpenGL.glTexCoord2f(f * (float)(i_498_ - i_502_), f_504_ * (float)(i_499_ - i_503_));
        OpenGL.glVertex2f((float)i_498_ + f_505_ + 0.35F, (float)i_499_ + f_506_ + 0.35F);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    public void method8532(int i, int i_508_, int i_509_, int i_510_, int i_511_, int i_512_, Class455 class455, int i_513_, int i_514_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(i_512_);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_515_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        float f_516_ = (float)i_509_ - (float)i;
        float f_517_ = (float)i_510_ - (float)i_508_;
        float f_518_ = (float)(1.0 / Math.sqrt((double)(f_516_ * f_516_ + f_517_ * f_517_)));
        f_516_ *= f_518_;
        f_517_ *= f_518_;
        OpenGL.glColor4ub((byte)(i_511_ >> 16), (byte)(i_511_ >> 8), (byte)i_511_, (byte)(i_511_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glTexCoord2f(f * (float)(i - i_513_), f_515_ * (float)(i_508_ - i_514_));
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_508_ + 0.35F);
        OpenGL.glTexCoord2f(f * (float)(i_509_ - i_513_), f_515_ * (float)(i_510_ - i_514_));
        OpenGL.glVertex2f((float)i_509_ + f_516_ + 0.35F, (float)i_510_ + f_517_ + 0.35F);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    public void method8533(int i, int i_519_, int i_520_, int i_521_, int i_522_, int i_523_, Class455 class455, int i_524_, int i_525_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(i_523_);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_526_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        float f_527_ = (float)i_520_ - (float)i;
        float f_528_ = (float)i_521_ - (float)i_519_;
        float f_529_ = (float)(1.0 / Math.sqrt((double)(f_527_ * f_527_ + f_528_ * f_528_)));
        f_527_ *= f_529_;
        f_528_ *= f_529_;
        OpenGL.glColor4ub((byte)(i_522_ >> 16), (byte)(i_522_ >> 8), (byte)i_522_, (byte)(i_522_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glTexCoord2f(f * (float)(i - i_524_), f_526_ * (float)(i_519_ - i_525_));
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_519_ + 0.35F);
        OpenGL.glTexCoord2f(f * (float)(i_520_ - i_524_), f_526_ * (float)(i_521_ - i_525_));
        OpenGL.glVertex2f((float)i_520_ + f_527_ + 0.35F, (float)i_521_ + f_528_ + 0.35F);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    public void method8575(int i, int i_530_, int i_531_, int i_532_, int i_533_, int i_534_, Class455 class455, int i_535_, int i_536_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(i_534_);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_537_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        float f_538_ = (float)i_531_ - (float)i;
        float f_539_ = (float)i_532_ - (float)i_530_;
        float f_540_ = (float)(1.0 / Math.sqrt((double)(f_538_ * f_538_ + f_539_ * f_539_)));
        f_538_ *= f_540_;
        f_539_ *= f_540_;
        OpenGL.glColor4ub((byte)(i_533_ >> 16), (byte)(i_533_ >> 8), (byte)i_533_, (byte)(i_533_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glTexCoord2f(f * (float)(i - i_535_), f_537_ * (float)(i_530_ - i_536_));
        OpenGL.glVertex2f((float)i + 0.35F, (float)i_530_ + 0.35F);
        OpenGL.glTexCoord2f(f * (float)(i_531_ - i_535_), f_537_ * (float)(i_532_ - i_536_));
        OpenGL.glVertex2f((float)i_531_ + f_538_ + 0.35F, (float)i_532_ + f_539_ + 0.35F);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    public void method8535(int i, int i_541_, int i_542_, int i_543_, int i_544_, int i_545_, Class455 class455, int i_546_, int i_547_, int i_548_, int i_549_, int i_550_) {
        if (i != i_542_ || i_541_ != i_543_) {
            Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
            Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
            method13637();
            method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
            method13624(i_545_);
            method13717(7681, 8448);
            method13595(0, 34167, 768);
            float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)(((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136));
            float f_551_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)(((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133));
            float f_552_ = (float)i_542_ - (float)i;
            float f_553_ = (float)i_543_ - (float)i_541_;
            float f_554_ = (float)(1.0 / Math.sqrt((double)(f_552_ * f_552_ + f_553_ * f_553_)));
            f_552_ *= f_554_;
            f_553_ *= f_554_;
            OpenGL.glColor4ub((byte)(i_544_ >> 16), (byte)(i_544_ >> 8), (byte)i_544_, (byte)(i_544_ >> 24));
            i_550_ %= i_549_ + i_548_;
            float f_555_ = f_552_ * (float)i_548_;
            float f_556_ = f_553_ * (float)i_548_;
            float f_557_ = 0.0F;
            float f_558_ = 0.0F;
            float f_559_ = f_555_;
            float f_560_ = f_556_;
            if (i_550_ > i_548_) {
                f_557_ = f_552_ * (float)(i_548_ + i_549_ - i_550_);
                f_558_ = f_553_ * (float)(i_548_ + i_549_ - i_550_);
            } else {
                f_559_ = f_552_ * (float)(i_548_ - i_550_);
                f_560_ = f_553_ * (float)(i_548_ - i_550_);
            }
            float f_561_ = (float)i + 0.35F + f_557_;
            float f_562_ = (float)i_541_ + 0.35F + f_558_;
            float f_563_ = f_552_ * (float)i_549_;
            float f_564_ = f_553_ * (float)i_549_;
            for (; ; ) {
                if (i_542_ > i) {
					if (f_561_ > (float)i_542_ + 0.35F) {
						break;
					}
					if (f_561_ + f_559_ > (float)i_542_) {
						f_559_ = (float)i_542_ - f_561_;
					}
                } else {
					if (f_561_ < (float)i_542_ + 0.35F) {
						break;
					}
					if (f_561_ + f_559_ < (float)i_542_) {
						f_559_ = (float)i_542_ - f_561_;
					}
                }
                if (i_543_ > i_541_) {
					if (f_562_ > (float)i_543_ + 0.35F) {
						break;
					}
					if (f_562_ + f_560_ > (float)i_543_) {
						f_560_ = (float)i_543_ - f_562_;
					}
                } else {
					if (f_562_ < (float)i_543_ + 0.35F) {
						break;
					}
					if (f_562_ + f_560_ < (float)i_543_) {
						f_560_ = (float)i_543_ - f_562_;
					}
                }
                OpenGL.glBegin(1);
                OpenGL.glTexCoord2f(f * (f_561_ - (float)i_546_), f_551_ * (f_562_ - (float)i_547_));
                OpenGL.glVertex2f(f_561_, f_562_);
                OpenGL.glTexCoord2f(f * (f_561_ + f_559_ - (float)i_546_), f_551_ * (f_562_ + f_560_ - (float)i_547_));
                OpenGL.glVertex2f(f_561_ + f_559_, f_562_ + f_560_);
                OpenGL.glEnd();
                f_561_ += f_563_ + f_559_;
                f_562_ += f_564_ + f_560_;
                f_559_ = f_555_;
                f_560_ = f_556_;
            }
            method13595(0, 5890, 768);
        }
    }

    public void method8536(int i, int i_565_, int i_566_, int i_567_, int i_568_, int i_569_, int i_570_) {
        OpenGL.glLineWidth((float)i_569_);
        method8433(i, i_565_, i_566_, i_567_, i_568_, i_570_);
        OpenGL.glLineWidth(1.0F);
    }

    public int method8537(int i, int i_571_, int i_572_, int i_573_, int i_574_, int i_575_) {
        int i_576_ = 0;
        float f = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_571_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_572_));
        float f_577_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i_573_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_574_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_575_));
        float f_578_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_571_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_572_));
        float f_579_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i_573_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_574_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_575_));
		if (f < -f_578_ && f_577_ < -f_579_) {
			i_576_ |= 0x10;
		} else if (f > f_578_ && f_577_ > f_579_) {
			i_576_ |= 0x20;
		}
        float f_580_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_571_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_572_));
        float f_581_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i_573_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_574_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_575_));
		if (f_580_ < -f_578_ && f_581_ < -f_579_) {
			i_576_ |= 0x1;
		}
		if (f_580_ > f_578_ && f_581_ > f_579_) {
			i_576_ |= 0x2;
		}
        float f_582_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_571_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_572_));
        float f_583_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i_573_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_574_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_575_));
		if (f_582_ < -f_578_ && f_583_ < -f_579_) {
			i_576_ |= 0x4;
		}
		if (f_582_ > f_578_ && f_583_ > f_579_) {
			i_576_ |= 0x8;
		}
        return i_576_;
    }

    final void method13647(Class143 class143, Class143 class143_584_, Class143 class143_585_, Class143 class143_586_) {
		if (class143 != null) {
			method13601(((Class143)class143).anInterface14_1667);
			OpenGL.glVertexPointer(((Class143)class143).aByte1670, ((Class143)class143).aShort1668, ((Class505_Sub1)this).anInterface14_8483.method59(), (((Class505_Sub1)this).anInterface14_8483.method109() + (long)((Class143)class143).aByte1669));
			OpenGL.glEnableClientState(32884);
		} else {
			OpenGL.glDisableClientState(32884);
		}
		if (class143_584_ != null) {
			method13601(((Class143)class143_584_).anInterface14_1667);
			OpenGL.glNormalPointer(((Class143)class143_584_).aShort1668, ((Class505_Sub1)this).anInterface14_8483.method59(), (((Class505_Sub1)this).anInterface14_8483.method109() + (long)((Class143)class143_584_).aByte1669));
			OpenGL.glEnableClientState(32885);
		} else {
			OpenGL.glDisableClientState(32885);
		}
		if (class143_585_ != null) {
			method13601(((Class143)class143_585_).anInterface14_1667);
			OpenGL.glColorPointer(((Class143)class143_585_).aByte1670, ((Class143)class143_585_).aShort1668, ((Class505_Sub1)this).anInterface14_8483.method59(), (((Class505_Sub1)this).anInterface14_8483.method109() + (long)((Class143)class143_585_).aByte1669));
			OpenGL.glEnableClientState(32886);
		} else {
			OpenGL.glDisableClientState(32886);
		}
		if (class143_586_ != null) {
			method13601(((Class143)class143_586_).anInterface14_1667);
			OpenGL.glTexCoordPointer(((Class143)class143_586_).aByte1670, ((Class143)class143_586_).aShort1668, ((Class505_Sub1)this).anInterface14_8483.method59(), (((Class505_Sub1)this).anInterface14_8483.method109() + (long)((Class143)class143_586_).aByte1669));
			OpenGL.glEnableClientState(32888);
		} else {
			OpenGL.glDisableClientState(32888);
		}
    }

    public int method8539(int i, int i_587_, int i_588_, int i_589_, int i_590_, int i_591_) {
        int i_592_ = 0;
        float f = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_587_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_588_));
        float f_593_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i_589_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_590_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_591_));
        float f_594_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_587_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_588_));
        float f_595_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i_589_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_590_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_591_));
		if (f < -f_594_ && f_593_ < -f_595_) {
			i_592_ |= 0x10;
		} else if (f > f_594_ && f_593_ > f_595_) {
			i_592_ |= 0x20;
		}
        float f_596_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_587_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_588_));
        float f_597_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i_589_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_590_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_591_));
		if (f_596_ < -f_594_ && f_597_ < -f_595_) {
			i_592_ |= 0x1;
		}
		if (f_596_ > f_594_ && f_597_ > f_595_) {
			i_592_ |= 0x2;
		}
        float f_598_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_587_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_588_));
        float f_599_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i_589_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_590_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_591_));
		if (f_598_ < -f_594_ && f_599_ < -f_595_) {
			i_592_ |= 0x4;
		}
		if (f_598_ > f_594_ && f_599_ > f_595_) {
			i_592_ |= 0x8;
		}
        return i_592_;
    }

    public int method8540(int i, int i_600_, int i_601_, int i_602_, int i_603_, int i_604_) {
        int i_605_ = 0;
        float f = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_600_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_601_));
        float f_606_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * (float)i_602_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * (float)i_603_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * (float)i_604_));
        float f_607_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_600_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_601_));
        float f_608_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * (float)i_602_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * (float)i_603_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * (float)i_604_));
		if (f < -f_607_ && f_606_ < -f_608_) {
			i_605_ |= 0x10;
		} else if (f > f_607_ && f_606_ > f_608_) {
			i_605_ |= 0x20;
		}
        float f_609_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_600_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_601_));
        float f_610_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * (float)i_602_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * (float)i_603_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * (float)i_604_));
		if (f_609_ < -f_607_ && f_610_ < -f_608_) {
			i_605_ |= 0x1;
		}
		if (f_609_ > f_607_ && f_610_ > f_608_) {
			i_605_ |= 0x2;
		}
        float f_611_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_600_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_601_));
        float f_612_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * (float)i_602_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * (float)i_603_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * (float)i_604_));
		if (f_611_ < -f_607_ && f_612_ < -f_608_) {
			i_605_ |= 0x4;
		}
		if (f_611_ > f_607_ && f_612_ > f_608_) {
			i_605_ |= 0x8;
		}
        return i_605_;
    }

    public Class282_Sub1 method8570(int i) {
        Class282_Sub1_Sub1 class282_sub1_sub1 = new Class282_Sub1_Sub1(i);
        ((Class505_Sub1)this).aClass473_8369.method7877(class282_sub1_sub1, -213419553);
        return class282_sub1_sub1;
    }

    public Class282_Sub1 method8541(int i) {
        Class282_Sub1_Sub1 class282_sub1_sub1 = new Class282_Sub1_Sub1(i);
        ((Class505_Sub1)this).aClass473_8369.method7877(class282_sub1_sub1, 194849073);
        return class282_sub1_sub1;
    }

    public void method8542(Class282_Sub1 class282_sub1) {
        ((Class505_Sub1)this).aNativeHeap8445 = (((Class282_Sub1_Sub1)(Class282_Sub1_Sub1)class282_sub1).aNativeHeap10051);
        if (((Class505_Sub1)this).anInterface14_8496 == null) {
            Class282_Sub35_Sub1 class282_sub35_sub1 = new Class282_Sub35_Sub1(80);
            if (((Class505_Sub1)this).aBool8467) {
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
            } else {
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
            }
            ((Class505_Sub1)this).anInterface14_8496 = method13599(20, class282_sub35_sub1.buffer, class282_sub35_sub1.index * -1990677291, false);
            ((Class505_Sub1)this).aClass143_8494 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 3, 0);
            ((Class505_Sub1)this).aClass143_8495 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 2, 12);
            ((Class505_Sub1)this).aClass134_8359.method2328(this);
        }
    }

    final void method13648(float f, float f_613_, float f_614_) {
        OpenGL.glMatrixMode(5890);
		if (((Class505_Sub1)this).aBool8457) {
			OpenGL.glLoadIdentity();
		}
        OpenGL.glTranslatef(f, f_613_, f_614_);
        OpenGL.glMatrixMode(5888);
        ((Class505_Sub1)this).aBool8457 = true;
    }

    public void method8544(Class282_Sub1 class282_sub1) {
        ((Class505_Sub1)this).aNativeHeap8445 = (((Class282_Sub1_Sub1)(Class282_Sub1_Sub1)class282_sub1).aNativeHeap10051);
        if (((Class505_Sub1)this).anInterface14_8496 == null) {
            Class282_Sub35_Sub1 class282_sub35_sub1 = new Class282_Sub35_Sub1(80);
            if (((Class505_Sub1)this).aBool8467) {
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(-1.0F);
                class282_sub35_sub1.method14688(0.0F);
                class282_sub35_sub1.method14688(1.0F);
                class282_sub35_sub1.method14688(0.0F);
            } else {
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(-1.0F);
                class282_sub35_sub1.method14685(0.0F);
                class282_sub35_sub1.method14685(1.0F);
                class282_sub35_sub1.method14685(0.0F);
            }
            ((Class505_Sub1)this).anInterface14_8496 = method13599(20, class282_sub35_sub1.buffer, class282_sub35_sub1.index * -1990677291, false);
            ((Class505_Sub1)this).aClass143_8494 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 3, 0);
            ((Class505_Sub1)this).aClass143_8495 = new Class143(((Class505_Sub1)this).anInterface14_8496, 5126, 2, 12);
            ((Class505_Sub1)this).aClass134_8359.method2328(this);
        }
    }

    public void XA(int i, int i_615_, int i_616_, int i_617_, int i_618_) {
        method13659();
        method13624(i_618_);
        float f = (float)i + 0.35F;
        float f_619_ = (float)i_615_ + 0.35F;
        OpenGL.glColor4ub((byte)(i_617_ >> 16), (byte)(i_617_ >> 8), (byte)i_617_, (byte)(i_617_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f(f, f_619_);
        OpenGL.glVertex2f(f + (float)i_616_, f_619_);
        OpenGL.glEnd();
    }

    public Class8 method8660(Class414 class414, Class91[] class91s, boolean bool) {
        return new Class8_Sub5(this, class414, class91s, bool);
    }

    public Class160 method8543(int[] is, int i, int i_620_, int i_621_, int i_622_, boolean bool) {
        return new Class160_Sub2(this, i_621_, i_622_, is, i, i_620_);
    }

    public Class160 method8548(int[] is, int i, int i_623_, int i_624_, int i_625_, boolean bool) {
        return new Class160_Sub2(this, i_624_, i_625_, is, i, i_623_);
    }

    public Class160 method8612(Class91 class91, boolean bool) {
        int[] is = new int[class91.anInt957 * class91.anInt954];
        int i = 0;
        int i_626_ = 0;
        if (class91.aByteArray961 != null) {
            for (int i_627_ = 0; i_627_ < class91.anInt954; i_627_++) {
                for (int i_628_ = 0; i_628_ < class91.anInt957; i_628_++) {
                    is[i_626_++] = (class91.aByteArray961[i] << 24 | (class91.anIntArray955[class91.aByteArray960[i] & 0xff]));
                    i++;
                }
            }
        } else {
            for (int i_629_ = 0; i_629_ < class91.anInt954; i_629_++) {
                for (int i_630_ = 0; i_630_ < class91.anInt957; i_630_++) {
                    int i_631_ = (class91.anIntArray955[class91.aByteArray960[i++] & 0xff]);
                    is[i_626_++] = i_631_ != 0 ? ~0xffffff | i_631_ : 0;
                }
            }
        }
        Class160 class160 = method8549(is, 0, class91.anInt957, class91.anInt957, class91.anInt954, 340274012);
        class160.method2743(class91.anInt956, class91.anInt959, class91.anInt958, class91.anInt953);
        return class160;
    }

    public Class160 method8518(Class91 class91, boolean bool) {
        int[] is = new int[class91.anInt957 * class91.anInt954];
        int i = 0;
        int i_632_ = 0;
        if (class91.aByteArray961 != null) {
            for (int i_633_ = 0; i_633_ < class91.anInt954; i_633_++) {
                for (int i_634_ = 0; i_634_ < class91.anInt957; i_634_++) {
                    is[i_632_++] = (class91.aByteArray961[i] << 24 | (class91.anIntArray955[class91.aByteArray960[i] & 0xff]));
                    i++;
                }
            }
        } else {
            for (int i_635_ = 0; i_635_ < class91.anInt954; i_635_++) {
                for (int i_636_ = 0; i_636_ < class91.anInt957; i_636_++) {
                    int i_637_ = (class91.anIntArray955[class91.aByteArray960[i++] & 0xff]);
                    is[i_632_++] = i_637_ != 0 ? ~0xffffff | i_637_ : 0;
                }
            }
        }
        Class160 class160 = method8549(is, 0, class91.anInt957, class91.anInt957, class91.anInt954, 639428153);
        class160.method2743(class91.anInt956, class91.anInt959, class91.anInt958, class91.anInt953);
        return class160;
    }

    public int method8434(int i, int i_638_) {
        return i | i_638_;
    }

    final void method13649(Class384 class384) {
        OpenGL.glPushMatrix();
        OpenGL.glMultMatrixf(class384.aFloatArray4667, 0);
    }

    public Class455 method8553(int i, int i_639_, int[] is, int[] is_640_) {
        return Class455_Sub2.method14343(this, i, i_639_, is, is_640_);
    }

    public Class160 method8577(int i, int i_641_, boolean bool, boolean bool_642_) {
        return new Class160_Sub2(this, i, i_641_, bool);
    }

    public void GA(float f, float f_643_) {
        ((Class505_Sub1)this).aFloat8364 = f;
        ((Class505_Sub1)this).aFloat8320 = f_643_;
        method13671();
    }

    public Class455 method8556(int i, int i_644_, int[] is, int[] is_645_) {
        return Class455_Sub2.method14343(this, i, i_644_, is, is_645_);
    }

    public void hr(int i, Class455 class455, int i_646_, int i_647_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(1);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_648_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        OpenGL.glColor4ub((byte)(i >> 16), (byte)(i >> 8), (byte)i, (byte)(i >> 24));
        OpenGL.glBegin(7);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_646_), f_648_ * (float)(((Class505_Sub1)this).anInt8415 - i_647_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8415);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_646_), f_648_ * (float)(((Class505_Sub1)this).anInt8478 - i_647_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_646_), f_648_ * (float)(((Class505_Sub1)this).anInt8478 - i_647_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_646_), f_648_ * (float)(((Class505_Sub1)this).anInt8415 - i_647_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8415);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    public void hz(int i, Class455 class455, int i_649_, int i_650_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(1);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_651_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        OpenGL.glColor4ub((byte)(i >> 16), (byte)(i >> 8), (byte)i, (byte)(i >> 24));
        OpenGL.glBegin(7);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_649_), f_651_ * (float)(((Class505_Sub1)this).anInt8415 - i_650_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8415);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_649_), f_651_ * (float)(((Class505_Sub1)this).anInt8478 - i_650_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_649_), f_651_ * (float)(((Class505_Sub1)this).anInt8478 - i_650_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_649_), f_651_ * (float)(((Class505_Sub1)this).anInt8415 - i_650_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8415);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    public void G(int i, int i_652_, int i_653_, int i_654_, int i_655_) {
        method13659();
        method13624(i_655_);
        float f = (float)i + 0.35F;
        float f_656_ = (float)i_652_ + 0.35F;
        OpenGL.glColor4ub((byte)(i_654_ >> 16), (byte)(i_654_ >> 8), (byte)i_654_, (byte)(i_654_ >> 24));
        OpenGL.glBegin(1);
        OpenGL.glVertex2f(f, f_656_);
        OpenGL.glVertex2f(f, f_656_ + (float)i_653_);
        OpenGL.glEnd();
    }

    public void method8514(int i, int i_657_, int i_658_, int i_659_) {
        ((Class505_Sub1)this).anInt8417 = i;
        ((Class505_Sub1)this).anInt8418 = i_657_;
        ((Class505_Sub1)this).anInt8419 = i_658_;
        ((Class505_Sub1)this).anInt8347 = i_659_;
        method13584();
    }

    final void method13650(Class384 class384) {
        OpenGL.glPushMatrix();
        OpenGL.glMultMatrixf(class384.aFloatArray4667, 0);
    }

    public void method8559(int i) {
        /* empty */
    }

    public void method8560(int i) {
        /* empty */
    }

    public Class528 method8561(Class157 class157, int i, int i_660_, int i_661_, int i_662_) {
        return new Class528_Sub1(this, class157, i, i_661_, i_662_, i_660_);
    }

    public Class528 method8623(Class157 class157, int i, int i_663_, int i_664_, int i_665_) {
        return new Class528_Sub1(this, class157, i, i_664_, i_665_, i_663_);
    }

    public Class528 method8505(Class157 class157, int i, int i_666_, int i_667_, int i_668_) {
        return new Class528_Sub1(this, class157, i, i_667_, i_668_, i_666_);
    }

    public Class528 method8564(Class157 class157, int i, int i_669_, int i_670_, int i_671_) {
        return new Class528_Sub1(this, class157, i, i_670_, i_671_, i_669_);
    }

    public int method8565(int i, int i_672_) {
        return i & i_672_ ^ i_672_;
    }

    public final void ft(int[] is) {
        is[0] = ((Class505_Sub1)this).anInt8413;
        is[1] = ((Class505_Sub1)this).anInt8415;
        is[2] = ((Class505_Sub1)this).anInt8412;
        is[3] = ((Class505_Sub1)this).anInt8478;
    }

    final synchronized void method13651(long l) {
        Class282 class282 = new Class282();
        class282.aLong3379 = l * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8379.method7877(class282, 315594439);
    }

    public int method8443(int i, int i_673_) {
        return i & i_673_ ^ i_673_;
    }

    int method13652() {
        int i = 0;
        ((Class505_Sub1)this).aString8463 = OpenGL.glGetString(7936).toLowerCase();
        ((Class505_Sub1)this).aString8464 = OpenGL.glGetString(7937).toLowerCase();
		if (((Class505_Sub1)this).aString8463.indexOf("microsoft") != -1) {
			i |= 0x1;
		}
		if (((Class505_Sub1)this).aString8463.indexOf("brian paul") != -1 || ((Class505_Sub1)this).aString8463.indexOf("mesa") != -1) {
			i |= 0x1;
		}
        String string = OpenGL.glGetString(7938);
        String[] strings = Class456_Sub3.method12681(string.replace('.', ' '), ' ', 229848533);
		if (strings.length >= 2) {
			try {
				int i_674_ = Class328.method5830(strings[0], 242232953);
				int i_675_ = Class328.method5830(strings[1], -1619906898);
				((Class505_Sub1)this).anInt8443 = i_674_ * 10 + i_675_;
			} catch (NumberFormatException numberformatexception) {
				i |= 0x4;
			}
		} else {
			i |= 0x4;
		}
		if (((Class505_Sub1)this).anInt8443 < 12) {
			i |= 0x2;
		}
		if (!((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_multitexture")) {
			i |= 0x8;
		}
		if (!((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_env_combine")) {
			i |= 0x20;
		}
        int[] is = new int[1];
        OpenGL.glGetIntegerv(34018, is, 0);
        ((Class505_Sub1)this).anInt8469 = is[0];
        OpenGL.glGetIntegerv(34929, is, 0);
        ((Class505_Sub1)this).anInt8470 = is[0];
        OpenGL.glGetIntegerv(34930, is, 0);
        ((Class505_Sub1)this).anInt8471 = is[0];
		if (((Class505_Sub1)this).anInt8469 < 2 || ((Class505_Sub1)this).anInt8470 < 2 || ((Class505_Sub1)this).anInt8471 < 2) {
			i |= 0x10;
		}
        ((Class505_Sub1)this).aBool8467 = Stream.method2926();
        ((Class505_Sub1)this).aBool8309 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_buffer_object");
        ((Class505_Sub1)this).aBool8342 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_multisample");
        ((Class505_Sub1)this).aBool8484 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_program");
        ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_fragment_program");
        ((Class505_Sub1)this).aBool8485 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_shader");
        ((Class505_Sub1)this).aBool8365 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_fragment_shader");
        ((Class505_Sub1)this).aBool8393 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_texture3D");
        ((Class505_Sub1)this).aBool8401 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_rectangle");
        ((Class505_Sub1)this).aBool8480 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_cube_map");
        ((Class505_Sub1)this).aBool8312 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_float");
        ((Class505_Sub1)this).aBool8498 = false;
        ((Class505_Sub1)this).aBool8472 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_object");
        ((Class505_Sub1)this).aBool8338 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_blit");
        ((Class505_Sub1)this).aBool8488 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_multisample");
        ((Class505_Sub1)this).aBool8344 = (((Class505_Sub1)this).aBool8338 & ((Class505_Sub1)this).aBool8488);
        ((Class505_Sub1)this).aBool8456 = Class396.aString4790.startsWith("mac");
        OpenGL.glGetFloatv(2834, aFloatArray8497, 0);
        ((Class505_Sub1)this).aFloat8414 = aFloatArray8497[0];
        ((Class505_Sub1)this).aFloat8489 = aFloatArray8497[1];
        return i != 0 ? i : 0;
    }

    final void method13653(Class384 class384) {
        OpenGL.glLoadMatrixf(class384.aFloatArray4667, 0);
    }

    final void method13654(Class137 class137) {
        Class137 class137_676_ = (((Class505_Sub1)this).aClass137Array8482[((Class505_Sub1)this).anInt8458]);
        if (class137_676_ != class137) {
			if (class137 != null) {
				if (class137_676_ != null) {
					if (((Class137)class137).anInt1648 != ((Class137)class137_676_).anInt1648) {
						OpenGL.glDisable(((Class137)class137_676_).anInt1648);
						OpenGL.glEnable(((Class137)class137).anInt1648);
					}
				} else {
					OpenGL.glEnable(((Class137)class137).anInt1648);
				}
				OpenGL.glBindTexture(((Class137)class137).anInt1648, ((Class137)class137).anInt1647);
			} else {
				OpenGL.glDisable(((Class137)class137_676_).anInt1648);
			}
            ((Class505_Sub1)this).aClass137Array8482[(((Class505_Sub1)this).anInt8458)] = class137;
        }
        ((Class505_Sub1)this).anInt8382 &= ~0x1;
    }

    public Class294 method8572() {
        return ((Class505_Sub1)this).aClass294_8476;
    }

    public Class294 method8626() {
        return ((Class505_Sub1)this).aClass294_8476;
    }

    public int method8574() {
        return 4;
    }

    public void method8394(int i, Class282_Sub24[] class282_sub24s) {
		for (int i_677_ = 0; i_677_ < i; i_677_++) {
			((Class505_Sub1)this).aClass282_Sub24Array8435[i_677_] = class282_sub24s[i_677_];
		}
        ((Class505_Sub1)this).anInt8437 = i;
		if (((Class505_Sub1)this).anInt8409 != 1) {
			method13638();
		}
    }

    public void method8426(int i, Class282_Sub24[] class282_sub24s) {
		for (int i_678_ = 0; i_678_ < i; i_678_++) {
			((Class505_Sub1)this).aClass282_Sub24Array8435[i_678_] = class282_sub24s[i_678_];
		}
        ((Class505_Sub1)this).anInt8437 = i;
		if (((Class505_Sub1)this).anInt8409 != 1) {
			method13638();
		}
    }

    public void method8579(int i, Class282_Sub24[] class282_sub24s) {
		for (int i_679_ = 0; i_679_ < i; i_679_++) {
			((Class505_Sub1)this).aClass282_Sub24Array8435[i_679_] = class282_sub24s[i_679_];
		}
        ((Class505_Sub1)this).anInt8437 = i;
		if (((Class505_Sub1)this).anInt8409 != 1) {
			method13638();
		}
    }

    public void method8580(Class151 class151) {
        ((Class505_Sub1)this).aClass134_8359.method2329(this, class151);
    }

    public void method8581(Class151 class151) {
        ((Class505_Sub1)this).aClass134_8359.method2329(this, class151);
    }

    final void method13655(int i) {
        method13608(i, true);
    }

    public Class294 method8583() {
        return new Class294(((Class505_Sub1)this).aClass294_8389);
    }

    public Class294 method8685() {
        return new Class294(((Class505_Sub1)this).aClass294_8389);
    }

    final void method13656(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8387) {
			if (bool) {
				OpenGL.glEnable(2929);
			} else {
				OpenGL.glDisable(2929);
			}
            ((Class505_Sub1)this).aBool8387 = bool;
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    public final void method8586(Class384 class384) {
        ((Class505_Sub1)this).aClass384_8442.method6562(class384);
        method13597();
        method13588();
    }

    final synchronized void method13657(int i, int i_680_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_680_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8486.method7877(class282_sub38, 1997801599);
    }

    public final Class384 method8588() {
        return new Class384(((Class505_Sub1)this).aClass384_8442);
    }

    final void method13658(Interface15 interface15) {
        if (((Class505_Sub1)this).anInterface15_8452 != interface15) {
			if (((Class505_Sub1)this).aBool8309) {
				OpenGL.glBindBufferARB(34963, interface15.method1());
			}
            ((Class505_Sub1)this).anInterface15_8452 = interface15;
        }
    }

    public final Class384 method8590() {
        return new Class384(((Class505_Sub1)this).aClass384_8442);
    }

    final void method13659() {
        if (((Class505_Sub1)this).anInt8382 != 1) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13654(null);
            method13581(-2);
            method13612(1);
            ((Class505_Sub1)this).anInt8382 = 1;
        }
    }

    public boolean method8578() {
        return (((Class505_Sub1)this).aBool8342 && (!method8471() || ((Class505_Sub1)this).aBool8344));
    }

    public final void iw(int i, float f, float f_681_, float f_682_, float f_683_, float f_684_) {
        boolean bool = ((Class505_Sub1)this).anInt8428 != i;
        if (bool || ((Class505_Sub1)this).aFloat8433 != f || ((Class505_Sub1)this).aFloat8434 != f_681_) {
            ((Class505_Sub1)this).anInt8428 = i;
            ((Class505_Sub1)this).aFloat8433 = f;
            ((Class505_Sub1)this).aFloat8434 = f_681_;
            if (bool) {
                ((Class505_Sub1)this).aFloat8429 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff0000) / 1.671168E7F);
                ((Class505_Sub1)this).aFloat8430 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff00) / 65280.0F);
                ((Class505_Sub1)this).aFloat8431 = ((float)(((Class505_Sub1)this).anInt8428 & 0xff) / 255.0F);
                method13689();
            }
            method13592();
        }
        if (((Class505_Sub1)this).aFloatArray8424[0] != f_682_ || ((Class505_Sub1)this).aFloatArray8424[1] != f_683_ || ((Class505_Sub1)this).aFloatArray8424[2] != f_684_) {
            ((Class505_Sub1)this).aFloatArray8424[0] = f_682_;
            ((Class505_Sub1)this).aFloatArray8424[1] = f_683_;
            ((Class505_Sub1)this).aFloatArray8424[2] = f_684_;
            ((Class505_Sub1)this).aFloatArray8425[0] = -f_682_;
            ((Class505_Sub1)this).aFloatArray8425[1] = -f_683_;
            ((Class505_Sub1)this).aFloatArray8425[2] = -f_684_;
            float f_685_ = (float)(1.0 / Math.sqrt((double)(f_682_ * f_682_ + f_683_ * f_683_ + f_684_ * f_684_)));
            ((Class505_Sub1)this).aFloatArray8426[0] = f_682_ * f_685_;
            ((Class505_Sub1)this).aFloatArray8426[1] = f_683_ * f_685_;
            ((Class505_Sub1)this).aFloatArray8426[2] = f_684_ * f_685_;
            ((Class505_Sub1)this).aFloatArray8427[0] = -((Class505_Sub1)this).aFloatArray8426[0];
            ((Class505_Sub1)this).aFloatArray8427[1] = -((Class505_Sub1)this).aFloatArray8426[1];
            ((Class505_Sub1)this).aFloatArray8427[2] = -((Class505_Sub1)this).aFloatArray8426[2];
            method13593();
            ((Class505_Sub1)this).anInt8438 = (int)(f_682_ * 256.0F / f_683_);
            ((Class505_Sub1)this).anInt8439 = (int)(f_684_ * 256.0F / f_683_);
        }
    }

    public boolean method8644() {
        return true;
    }

    public final void iq(int i) {
        ((Class505_Sub1)this).anInt8473 = 0;
		for (/**/; i > 1; i >>= 1) {
			((Class505_Sub1)this).anInt8473++;
		}
        ((Class505_Sub1)this).anInt8466 = 1 << ((Class505_Sub1)this).anInt8473;
    }

    public final void il(int i) {
        ((Class505_Sub1)this).anInt8473 = 0;
		for (/**/; i > 1; i >>= 1) {
			((Class505_Sub1)this).anInt8473++;
		}
        ((Class505_Sub1)this).anInt8466 = 1 << ((Class505_Sub1)this).anInt8473;
    }

    final synchronized void method13660(int i) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i);
        ((Class505_Sub1)this).aClass473_8376.method7877(class282_sub38, -141290667);
    }

    public final void is(int i, int i_686_, int i_687_) {
        if (((Class505_Sub1)this).anInt8441 != i || ((Class505_Sub1)this).anInt8358 != i_686_ || ((Class505_Sub1)this).anInt8378 != i_687_) {
            ((Class505_Sub1)this).anInt8441 = i;
            ((Class505_Sub1)this).anInt8358 = i_686_;
            ((Class505_Sub1)this).anInt8378 = i_687_;
            method13594();
            method13747();
        }
    }

    public final void ik(int i, int i_688_, int i_689_) {
        if (((Class505_Sub1)this).anInt8441 != i || ((Class505_Sub1)this).anInt8358 != i_688_ || ((Class505_Sub1)this).anInt8378 != i_689_) {
            ((Class505_Sub1)this).anInt8441 = i;
            ((Class505_Sub1)this).anInt8358 = i_688_;
            ((Class505_Sub1)this).anInt8378 = i_689_;
            method13594();
            method13747();
        }
    }

    public final void ib(int i, int i_690_, int i_691_) {
        if (((Class505_Sub1)this).anInt8441 != i || ((Class505_Sub1)this).anInt8358 != i_690_ || ((Class505_Sub1)this).anInt8378 != i_691_) {
            ((Class505_Sub1)this).anInt8441 = i;
            ((Class505_Sub1)this).anInt8358 = i_690_;
            ((Class505_Sub1)this).anInt8378 = i_691_;
            method13594();
            method13747();
        }
    }

    public void method8594(boolean bool) {
        /* empty */
    }

    public void method8595(boolean bool) {
        /* empty */
    }

    public Class152 method8557(Class152 class152, Class152 class152_692_, float f, Class152 class152_693_) {
        if (class152 != null && class152_692_ != null && ((Class505_Sub1)this).aBool8480 && ((Class505_Sub1)this).aBool8472) {
            Class152_Sub1_Sub2 class152_sub1_sub2 = null;
            Class152_Sub1 class152_sub1 = (Class152_Sub1)class152;
            Class152_Sub1 class152_sub1_694_ = (Class152_Sub1)class152_692_;
            Class137_Sub2 class137_sub2 = class152_sub1.method13519();
            Class137_Sub2 class137_sub2_695_ = class152_sub1_694_.method13519();
            if (class137_sub2 != null && class137_sub2_695_ != null) {
                int i = ((((Class137_Sub2)class137_sub2).anInt9104 > ((Class137_Sub2)class137_sub2_695_).anInt9104) ? ((Class137_Sub2)class137_sub2).anInt9104 : ((Class137_Sub2)class137_sub2_695_).anInt9104);
                if (class152 != class152_693_ && class152_692_ != class152_693_ && class152_693_ instanceof Class152_Sub1_Sub2) {
                    Class152_Sub1_Sub2 class152_sub1_sub2_696_ = (Class152_Sub1_Sub2)class152_693_;
					if (class152_sub1_sub2_696_.method15532() == i) {
						class152_sub1_sub2 = class152_sub1_sub2_696_;
					}
                }
				if (class152_sub1_sub2 == null) {
					class152_sub1_sub2 = new Class152_Sub1_Sub2(this, i);
				}
				if (class152_sub1_sub2.method15531(class137_sub2, class137_sub2_695_, f)) {
					return class152_sub1_sub2;
				}
            }
        }
        return f < 0.5F ? class152 : class152_692_;
    }

    public final void method8584(Class152 class152) {
        ((Class505_Sub1)this).aClass152_Sub1_8317 = (Class152_Sub1)class152;
    }

    public final void method8598(Class152 class152) {
        ((Class505_Sub1)this).aClass152_Sub1_8317 = (Class152_Sub1)class152;
    }

    public final void method8691() {
        ((Class505_Sub1)this).aClass170_8357.method2883();
    }

    final Interface15 method13661(int i, byte[] is, int i_697_, boolean bool) {
		if (((Class505_Sub1)this).aBool8309 && (!bool || ((Class505_Sub1)this).aBool8362)) {
			return new Class135_Sub1(this, i, is, i_697_, bool);
		}
        return new Class131_Sub2(this, i, is, i_697_);
    }

    public void method8635() {
        ((Class505_Sub1)this).anInt8417 = 0;
        ((Class505_Sub1)this).anInt8418 = 0;
        ((Class505_Sub1)this).anInt8419 = aClass158_5853.method2714();
        ((Class505_Sub1)this).anInt8347 = aClass158_5853.method2716();
        method13584();
    }

    public final void method8602() {
        if (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129()) {
            ((Class505_Sub1)this).aClass170_8357.method2885(((Class505_Sub1)this).aClass282_Sub5_Sub1_8444);
            ((Class505_Sub1)this).aClass167_8481.method2861();
        }
    }

    public final void method8521() {
        if (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129()) {
            ((Class505_Sub1)this).aClass170_8357.method2885(((Class505_Sub1)this).aClass282_Sub5_Sub1_8444);
            ((Class505_Sub1)this).aClass167_8481.method2861();
        }
    }

    public final boolean method8679() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129());
    }

    public final boolean method8605() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129());
    }

    final synchronized void method13662(long l) {
        Class282 class282 = new Class282();
        class282.aLong3379 = l * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8379.method7877(class282, 785288693);
    }

    public final boolean method8607() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129());
    }

    final void method8608(float f, float f_698_, float f_699_, float f_700_, float f_701_, float f_702_) {
        Class282_Sub5_Sub1.aFloat10026 = f;
        Class282_Sub5_Sub1.aFloat10025 = f_698_;
        Class282_Sub5_Sub1.aFloat10024 = f_699_;
    }

    public Class384 method8517() {
        return ((Class505_Sub1)this).aClass384_8360;
    }

    final void method13663(Interface15 interface15) {
        if (((Class505_Sub1)this).anInterface15_8452 != interface15) {
			if (((Class505_Sub1)this).aBool8309) {
				OpenGL.glBindBufferARB(34963, interface15.method1());
			}
            ((Class505_Sub1)this).anInterface15_8452 = interface15;
        }
    }

    public final void method8611() {
        ((Class505_Sub1)this).aClass170_8357.method2883();
    }

    public Class455 method8427(int i, int i_703_, int[] is, int[] is_704_) {
        return Class455_Sub2.method14343(this, i, i_703_, is, is_704_);
    }

    public final void method8613() {
        ((Class505_Sub1)this).aClass170_8357.method2883();
    }

    public void method8585(int i, Class90 class90) {
        ((Class505_Sub1)this).anInt8450 = i;
        ((Class505_Sub1)this).aClass90_8423 = class90;
        ((Class505_Sub1)this).aBool8448 = true;
    }

    public void method8615(int i, Class90 class90) {
		if (!((Class505_Sub1)this).aBool8448) {
			throw new RuntimeException("");
		}
        ((Class505_Sub1)this).anInt8450 = i;
        ((Class505_Sub1)this).aClass90_8423 = class90;
        if (((Class505_Sub1)this).aBool8449) {
            ((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14427();
            ((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14428();
        }
    }

    public void method8616(int i, Class90 class90) {
		if (!((Class505_Sub1)this).aBool8448) {
			throw new RuntimeException("");
		}
        ((Class505_Sub1)this).anInt8450 = i;
        ((Class505_Sub1)this).aClass90_8423 = class90;
        if (((Class505_Sub1)this).aBool8449) {
            ((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14427();
            ((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14428();
        }
    }

    public void jf() {
        ((Class505_Sub1)this).aBool8448 = false;
    }

    final synchronized void method13664(int i) {
        Class282 class282 = new Class282();
        class282.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8461.method7877(class282, 598291940);
    }

    public void method8513(int i, int i_705_, int i_706_, int i_707_) {
        ((Class505_Sub1)this).anInt8417 = i;
        ((Class505_Sub1)this).anInt8418 = i_705_;
        ((Class505_Sub1)this).anInt8419 = i_706_;
        ((Class505_Sub1)this).anInt8347 = i_707_;
        method13584();
    }

    public void method8619(float f, float f_708_, float f_709_, float[] fs) {
        float f_710_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[14] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[2] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[6] * f_708_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[10] * f_709_));
        float f_711_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * f_708_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * f_709_));
        if (f_710_ < -f_711_ || f_710_ > f_711_) {
            float[] fs_712_ = fs;
            float[] fs_713_ = fs;
            fs[2] = Float.NaN;
            fs_713_[1] = Float.NaN;
            fs_712_[0] = Float.NaN;
        } else {
            float f_714_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * f) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * f_708_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * f_709_));
            if (f_714_ < -f_711_ || f_714_ > f_711_) {
                float[] fs_715_ = fs;
                float[] fs_716_ = fs;
                fs[2] = Float.NaN;
                fs_716_[1] = Float.NaN;
                fs_715_[0] = Float.NaN;
            } else {
                float f_717_ = ((((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13]) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1]) * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5]) * f_708_ + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9]) * f_709_);
                if (f_717_ < -f_711_ || f_717_ > f_711_) {
                    float[] fs_718_ = fs;
                    float[] fs_719_ = fs;
                    fs[2] = Float.NaN;
                    fs_719_[1] = Float.NaN;
                    fs_718_[0] = Float.NaN;
                } else {
                    float f_720_ = ((((Class505_Sub1)this).aClass384_8348.aFloatArray4667[14]) + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[2]) * f + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[6]) * f_708_ + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[10]) * f_709_);
                    fs[0] = (((Class505_Sub1)this).aFloat8315 + (((Class505_Sub1)this).aFloat8398 * f_714_ / f_711_));
                    fs[1] = (((Class505_Sub1)this).aFloat8399 + (((Class505_Sub1)this).aFloat8400 * f_717_ / f_711_));
                    fs[2] = f_720_;
                }
            }
        }
    }

    public Class158_Sub1 method8620() {
        return new Class158_Sub1_Sub4(this);
    }

    public Interface8 method8621(int i, int i_721_) {
        return new Class282_Sub50_Sub19(this, Class150.aClass150_1953, Class76.aClass76_749, i, i_721_);
    }

    int method13665() {
        int i = 0;
        ((Class505_Sub1)this).aString8463 = OpenGL.glGetString(7936).toLowerCase();
        ((Class505_Sub1)this).aString8464 = OpenGL.glGetString(7937).toLowerCase();
		if (((Class505_Sub1)this).aString8463.indexOf("microsoft") != -1) {
			i |= 0x1;
		}
		if (((Class505_Sub1)this).aString8463.indexOf("brian paul") != -1 || ((Class505_Sub1)this).aString8463.indexOf("mesa") != -1) {
			i |= 0x1;
		}
        String string = OpenGL.glGetString(7938);
        String[] strings = Class456_Sub3.method12681(string.replace('.', ' '), ' ', 229848533);
		if (strings.length >= 2) {
			try {
				int i_722_ = Class328.method5830(strings[0], 694241397);
				int i_723_ = Class328.method5830(strings[1], 662919587);
				((Class505_Sub1)this).anInt8443 = i_722_ * 10 + i_723_;
			} catch (NumberFormatException numberformatexception) {
				i |= 0x4;
			}
		} else {
			i |= 0x4;
		}
		if (((Class505_Sub1)this).anInt8443 < 12) {
			i |= 0x2;
		}
		if (!((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_multitexture")) {
			i |= 0x8;
		}
		if (!((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_env_combine")) {
			i |= 0x20;
		}
        int[] is = new int[1];
        OpenGL.glGetIntegerv(34018, is, 0);
        ((Class505_Sub1)this).anInt8469 = is[0];
        OpenGL.glGetIntegerv(34929, is, 0);
        ((Class505_Sub1)this).anInt8470 = is[0];
        OpenGL.glGetIntegerv(34930, is, 0);
        ((Class505_Sub1)this).anInt8471 = is[0];
		if (((Class505_Sub1)this).anInt8469 < 2 || ((Class505_Sub1)this).anInt8470 < 2 || ((Class505_Sub1)this).anInt8471 < 2) {
			i |= 0x10;
		}
        ((Class505_Sub1)this).aBool8467 = Stream.method2926();
        ((Class505_Sub1)this).aBool8309 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_buffer_object");
        ((Class505_Sub1)this).aBool8342 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_multisample");
        ((Class505_Sub1)this).aBool8484 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_program");
        ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_fragment_program");
        ((Class505_Sub1)this).aBool8485 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_shader");
        ((Class505_Sub1)this).aBool8365 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_fragment_shader");
        ((Class505_Sub1)this).aBool8393 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_texture3D");
        ((Class505_Sub1)this).aBool8401 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_rectangle");
        ((Class505_Sub1)this).aBool8480 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_cube_map");
        ((Class505_Sub1)this).aBool8312 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_float");
        ((Class505_Sub1)this).aBool8498 = false;
        ((Class505_Sub1)this).aBool8472 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_object");
        ((Class505_Sub1)this).aBool8338 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_blit");
        ((Class505_Sub1)this).aBool8488 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_multisample");
        ((Class505_Sub1)this).aBool8344 = (((Class505_Sub1)this).aBool8338 & ((Class505_Sub1)this).aBool8488);
        ((Class505_Sub1)this).aBool8456 = Class396.aString4790.startsWith("mac");
        OpenGL.glGetFloatv(2834, aFloatArray8497, 0);
        ((Class505_Sub1)this).aFloat8414 = aFloatArray8497[0];
        ((Class505_Sub1)this).aFloat8489 = aFloatArray8497[1];
        return i != 0 ? i : 0;
    }

    int method13666() {
        int i = 0;
        ((Class505_Sub1)this).aString8463 = OpenGL.glGetString(7936).toLowerCase();
        ((Class505_Sub1)this).aString8464 = OpenGL.glGetString(7937).toLowerCase();
		if (((Class505_Sub1)this).aString8463.indexOf("microsoft") != -1) {
			i |= 0x1;
		}
		if (((Class505_Sub1)this).aString8463.indexOf("brian paul") != -1 || ((Class505_Sub1)this).aString8463.indexOf("mesa") != -1) {
			i |= 0x1;
		}
        String string = OpenGL.glGetString(7938);
        String[] strings = Class456_Sub3.method12681(string.replace('.', ' '), ' ', 229848533);
		if (strings.length >= 2) {
			try {
				int i_724_ = Class328.method5830(strings[0], 631934702);
				int i_725_ = Class328.method5830(strings[1], -1917846657);
				((Class505_Sub1)this).anInt8443 = i_724_ * 10 + i_725_;
			} catch (NumberFormatException numberformatexception) {
				i |= 0x4;
			}
		} else {
			i |= 0x4;
		}
		if (((Class505_Sub1)this).anInt8443 < 12) {
			i |= 0x2;
		}
		if (!((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_multitexture")) {
			i |= 0x8;
		}
		if (!((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_env_combine")) {
			i |= 0x20;
		}
        int[] is = new int[1];
        OpenGL.glGetIntegerv(34018, is, 0);
        ((Class505_Sub1)this).anInt8469 = is[0];
        OpenGL.glGetIntegerv(34929, is, 0);
        ((Class505_Sub1)this).anInt8470 = is[0];
        OpenGL.glGetIntegerv(34930, is, 0);
        ((Class505_Sub1)this).anInt8471 = is[0];
		if (((Class505_Sub1)this).anInt8469 < 2 || ((Class505_Sub1)this).anInt8470 < 2 || ((Class505_Sub1)this).anInt8471 < 2) {
			i |= 0x10;
		}
        ((Class505_Sub1)this).aBool8467 = Stream.method2926();
        ((Class505_Sub1)this).aBool8309 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_buffer_object");
        ((Class505_Sub1)this).aBool8342 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_multisample");
        ((Class505_Sub1)this).aBool8484 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_program");
        ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_fragment_program");
        ((Class505_Sub1)this).aBool8485 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_vertex_shader");
        ((Class505_Sub1)this).aBool8365 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_fragment_shader");
        ((Class505_Sub1)this).aBool8393 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_texture3D");
        ((Class505_Sub1)this).aBool8401 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_rectangle");
        ((Class505_Sub1)this).aBool8480 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_cube_map");
        ((Class505_Sub1)this).aBool8312 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_ARB_texture_float");
        ((Class505_Sub1)this).aBool8498 = false;
        ((Class505_Sub1)this).aBool8472 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_object");
        ((Class505_Sub1)this).aBool8338 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_blit");
        ((Class505_Sub1)this).aBool8488 = ((Class505_Sub1)this).anOpenGL8352.method2569("GL_EXT_framebuffer_multisample");
        ((Class505_Sub1)this).aBool8344 = (((Class505_Sub1)this).aBool8338 & ((Class505_Sub1)this).aBool8488);
        ((Class505_Sub1)this).aBool8456 = Class396.aString4790.startsWith("mac");
        OpenGL.glGetFloatv(2834, aFloatArray8497, 0);
        ((Class505_Sub1)this).aFloat8414 = aFloatArray8497[0];
        ((Class505_Sub1)this).aFloat8489 = aFloatArray8497[1];
        return i != 0 ? i : 0;
    }

    void method13667() {
        ((Class505_Sub1)this).aClass137Array8482 = new Class137[((Class505_Sub1)this).anInt8469];
        ((Class505_Sub1)this).aClass137_Sub1_8460 = new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        for (int i = 0; i < 7; i++) {
            ((Class505_Sub1)this).aClass528_Sub1Array8479[i] = new Class528_Sub1(this);
            ((Class505_Sub1)this).aClass528_Sub1Array8492[i] = new Class528_Sub1(this);
        }
        if (((Class505_Sub1)this).aBool8472) {
            ((Class505_Sub1)this).aClass158_Sub1_Sub4_8493 = new Class158_Sub1_Sub4(this);
            new Class158_Sub1_Sub4(this);
        }
    }

    void method13668() {
        ((Class505_Sub1)this).aClass137Array8482 = new Class137[((Class505_Sub1)this).anInt8469];
        ((Class505_Sub1)this).aClass137_Sub1_8460 = new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        new Class137_Sub1(this, 3553, Class150.aClass150_1949, Class76.aClass76_751, 1, 1);
        for (int i = 0; i < 7; i++) {
            ((Class505_Sub1)this).aClass528_Sub1Array8479[i] = new Class528_Sub1(this);
            ((Class505_Sub1)this).aClass528_Sub1Array8492[i] = new Class528_Sub1(this);
        }
        if (((Class505_Sub1)this).aBool8472) {
            ((Class505_Sub1)this).aClass158_Sub1_Sub4_8493 = new Class158_Sub1_Sub4(this);
            new Class158_Sub1_Sub4(this);
        }
    }

    boolean method13669() {
        return ((Class505_Sub1)this).aClass146_8356.method2465(3);
    }

    final void method13670() {
        if (((Class505_Sub1)this).anInt8382 != 4) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13581(-2);
            method13624(1);
            ((Class505_Sub1)this).anInt8382 = 4;
        }
    }

    public boolean method8609() {
        return false;
    }

    public int method8567(int i, int i_726_) {
        return i & i_726_ ^ i_726_;
    }

    public int[] kh(int i, int i_727_, int i_728_, int i_729_) {
        if (aClass158_5853 != null) {
            int[] is = new int[i_728_ * i_729_];
            int i_730_ = aClass158_5853.method2716();
			for (int i_731_ = 0; i_731_ < i_729_; i_731_++) {
				OpenGL.glReadPixelsi(i, i_730_ - i_727_ - i_731_ - 1, i_728_, 1, 32993, ((Class505_Sub1)this).anInt8410, is, i_731_ * i_728_);
			}
            return is;
        }
        return null;
    }

    public int[] ke(int i, int i_732_, int i_733_, int i_734_) {
        if (aClass158_5853 != null) {
            int[] is = new int[i_733_ * i_734_];
            int i_735_ = aClass158_5853.method2716();
			for (int i_736_ = 0; i_736_ < i_734_; i_736_++) {
				OpenGL.glReadPixelsi(i, i_735_ - i_732_ - i_736_ - 1, i_733_, 1, 32993, ((Class505_Sub1)this).anInt8410, is, i_736_ * i_733_);
			}
            return is;
        }
        return null;
    }

    public int[] kf(int i, int i_737_, int i_738_, int i_739_) {
        if (aClass158_5853 != null) {
            int[] is = new int[i_738_ * i_739_];
            int i_740_ = aClass158_5853.method2716();
			for (int i_741_ = 0; i_741_ < i_739_; i_741_++) {
				OpenGL.glReadPixelsi(i, i_740_ - i_737_ - i_741_ - 1, i_738_, 1, 32993, ((Class505_Sub1)this).anInt8410, is, i_741_ * i_738_);
			}
            return is;
        }
        return null;
    }

    public void method8627(int i, int i_742_, float f, int i_743_, int i_744_, float f_745_, int i_746_, int i_747_, float f_748_, int i_749_, int i_750_, int i_751_, int i_752_) {
        method13659();
        method13624(i_752_);
        OpenGL.glBegin(4);
        OpenGL.glColor4ub((byte)(i_749_ >> 16), (byte)(i_749_ >> 8), (byte)i_749_, (byte)(i_749_ >> 24));
        OpenGL.glVertex3f((float)i + 0.35F, (float)i_742_ + 0.35F, f);
        OpenGL.glColor4ub((byte)(i_750_ >> 16), (byte)(i_750_ >> 8), (byte)i_750_, (byte)(i_750_ >> 24));
        OpenGL.glVertex3f((float)i_743_ + 0.35F, (float)i_744_ + 0.35F, f_745_);
        OpenGL.glColor4ub((byte)(i_751_ >> 16), (byte)(i_751_ >> 8), (byte)i_751_, (byte)(i_751_ >> 24));
        OpenGL.glVertex3f((float)i_746_ + 0.35F, (float)i_747_ + 0.35F, f_748_);
        OpenGL.glEnd();
    }

    public Class390 method8478(int i, int i_753_, int[][] is, int[][] is_754_, int i_755_, int i_756_, int i_757_) {
        return new Class390_Sub2(this, i_756_, i_757_, i, i_753_, is, is_754_, i_755_);
    }

    public void method8629(int i, int i_758_, float f, int i_759_, int i_760_, float f_761_, int i_762_, int i_763_, float f_764_, int i_765_, int i_766_, int i_767_, int i_768_) {
        method13659();
        method13624(i_768_);
        OpenGL.glBegin(4);
        OpenGL.glColor4ub((byte)(i_765_ >> 16), (byte)(i_765_ >> 8), (byte)i_765_, (byte)(i_765_ >> 24));
        OpenGL.glVertex3f((float)i + 0.35F, (float)i_758_ + 0.35F, f);
        OpenGL.glColor4ub((byte)(i_766_ >> 16), (byte)(i_766_ >> 8), (byte)i_766_, (byte)(i_766_ >> 24));
        OpenGL.glVertex3f((float)i_759_ + 0.35F, (float)i_760_ + 0.35F, f_761_);
        OpenGL.glColor4ub((byte)(i_767_ >> 16), (byte)(i_767_ >> 8), (byte)i_767_, (byte)(i_767_ >> 24));
        OpenGL.glVertex3f((float)i_762_ + 0.35F, (float)i_763_ + 0.35F, f_764_);
        OpenGL.glEnd();
    }

    public int method8630(int i, int i_769_) {
        return i | i_769_;
    }

    Class158_Sub2 method8417(Canvas canvas, int i, int i_770_) {
        return new Class158_Sub2_Sub1(this, canvas);
    }

    public int method8632(int i, int i_771_) {
        return i | i_771_;
    }

    public void method8633() {
        ((Class505_Sub1)this).anInt8417 = 0;
        ((Class505_Sub1)this).anInt8418 = 0;
        ((Class505_Sub1)this).anInt8419 = aClass158_5853.method2714();
        ((Class505_Sub1)this).anInt8347 = aClass158_5853.method2716();
        method13584();
    }

    public void method8634() {
        ((Class505_Sub1)this).anInt8417 = 0;
        ((Class505_Sub1)this).anInt8418 = 0;
        ((Class505_Sub1)this).anInt8419 = aClass158_5853.method2714();
        ((Class505_Sub1)this).anInt8347 = aClass158_5853.method2716();
        method13584();
    }

    final void method13671() {
		if (((Class505_Sub1)this).anInt8409 == 2) {
			OpenGL.glDepthRange(((Class505_Sub1)this).aFloat8364, ((Class505_Sub1)this).aFloat8320);
		} else {
			OpenGL.glDepthRange(0.0F, 1.0F);
		}
    }

    final void method13672(int i, int i_772_) {
        ((Class505_Sub1)this).anInt8453 = i;
        ((Class505_Sub1)this).anInt8416 = i_772_;
        method13584();
        method13570();
    }

    final void method13673() {
		if (((Class505_Sub1)this).anInt8409 == 2) {
			OpenGL.glDepthRange(((Class505_Sub1)this).aFloat8364, ((Class505_Sub1)this).aFloat8320);
		} else {
			OpenGL.glDepthRange(0.0F, 1.0F);
		}
    }

    final void method13674() {
		if (((Class505_Sub1)this).anInt8409 == 2) {
			OpenGL.glDepthRange(((Class505_Sub1)this).aFloat8364, ((Class505_Sub1)this).aFloat8320);
		} else {
			OpenGL.glDepthRange(0.0F, 1.0F);
		}
    }

    final void method13675() {
		if (aClass158_5853 != null && (((Class505_Sub1)this).anInt8413 < ((Class505_Sub1)this).anInt8412) && (((Class505_Sub1)this).anInt8415 < ((Class505_Sub1)this).anInt8478)) {
			OpenGL.glScissor((((Class505_Sub1)this).anInt8453 + ((Class505_Sub1)this).anInt8413), (((Class505_Sub1)this).anInt8416 + aClass158_5853.method2716() - ((Class505_Sub1)this).anInt8478), (((Class505_Sub1)this).anInt8412 - ((Class505_Sub1)this).anInt8413), (((Class505_Sub1)this).anInt8478 - ((Class505_Sub1)this).anInt8415));
		} else {
			OpenGL.glScissor(0, 0, 0, 0);
		}
    }

    final void method13676() {
        OpenGL.glPushMatrix();
    }

    final void method13677() {
        OpenGL.glPushMatrix();
    }

    public final boolean method8606() {
        return (((Class505_Sub1)this).aClass282_Sub5_Sub1_8444 != null && ((Class505_Sub1)this).aClass282_Sub5_Sub1_8444.method12129());
    }

    final synchronized void method13678(int i) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i);
        ((Class505_Sub1)this).aClass473_8376.method7877(class282_sub38, 1072949766);
    }

    boolean method13679() {
        return ((Class505_Sub1)this).aClass146_8356.method2465(3);
    }

    public final int di() {
        return (((Class505_Sub1)this).anInt8371 + ((Class505_Sub1)this).anInt8370 + ((Class505_Sub1)this).anInt8372);
    }

    final void method13680(Class384 class384) {
        OpenGL.glLoadMatrixf(class384.aFloatArray4667, 0);
    }

    final void method13681() {
        OpenGL.glPopMatrix();
    }

    final void method13682() {
        OpenGL.glLoadIdentity();
        OpenGL.glMultMatrixf((((Class505_Sub1)this).aClass384_8348.aFloatArray4667), 0);
		if (((Class505_Sub1)this).aBool8449) {
			((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14427();
		}
        method13593();
        method13638();
    }

    final void method13683() {
        OpenGL.glLoadIdentity();
        OpenGL.glMultMatrixf((((Class505_Sub1)this).aClass384_8348.aFloatArray4667), 0);
		if (((Class505_Sub1)this).aBool8449) {
			((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14427();
		}
        method13593();
        method13638();
    }

    final void method13684() {
        if (((Class505_Sub1)this).anInt8409 != 0) {
            ((Class505_Sub1)this).anInt8409 = 0;
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    public Class168 method8392() {
        int i = -1;
		if (((Class505_Sub1)this).aString8463.indexOf("nvidia") != -1) {
			i = 4318;
		} else if (((Class505_Sub1)this).aString8463.indexOf("intel") != -1) {
			i = 32902;
		} else if (((Class505_Sub1)this).aString8463.indexOf("ati") != -1) {
			i = 4098;
		}
        return new Class168(i, "OpenGL", ((Class505_Sub1)this).anInt8443, ((Class505_Sub1)this).aString8464, 0L);
    }

    final void method13685() {
        if (((Class505_Sub1)this).anInt8409 != 0) {
            ((Class505_Sub1)this).anInt8409 = 0;
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    final void method13686() {
        if (((Class505_Sub1)this).anInt8409 != 0) {
            ((Class505_Sub1)this).anInt8409 = 0;
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    public Class160 method8461(int i, int i_773_, boolean bool, boolean bool_774_) {
        return new Class160_Sub2(this, i, i_773_, bool);
    }

    final void method13687() {
        if (((Class505_Sub1)this).anInt8409 != 2) {
            ((Class505_Sub1)this).anInt8409 = 2;
            method13719(((Class505_Sub1)this).aClass384_8442.aFloatArray4667);
            method13748();
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0x7;
        }
    }

    final void method13688() {
        if (((Class505_Sub1)this).anInt8409 != 2) {
            ((Class505_Sub1)this).anInt8409 = 2;
            method13719(((Class505_Sub1)this).aClass384_8442.aFloatArray4667);
            method13748();
            method13584();
            method13671();
            ((Class505_Sub1)this).anInt8382 &= ~0x7;
        }
    }

    void method13689() {
        aFloatArray8497[0] = (((Class505_Sub1)this).aFloat8432 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (((Class505_Sub1)this).aFloat8432 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (((Class505_Sub1)this).aFloat8432 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightModelfv(2899, aFloatArray8497, 0);
    }

    final void method13690(float[] fs) {
        float[] fs_775_ = new float[16];
        System.arraycopy(fs, 0, fs_775_, 0, 16);
        fs_775_[1] = -fs_775_[1];
        fs_775_[5] = -fs_775_[5];
        fs_775_[9] = -fs_775_[9];
        fs_775_[13] = -fs_775_[13];
        OpenGL.glMatrixMode(5889);
        OpenGL.glLoadMatrixf(fs_775_, 0);
        OpenGL.glMatrixMode(5888);
    }

    final void method13691(float[] fs) {
        float[] fs_776_ = new float[16];
        System.arraycopy(fs, 0, fs_776_, 0, 16);
        fs_776_[1] = -fs_776_[1];
        fs_776_[5] = -fs_776_[5];
        fs_776_[9] = -fs_776_[9];
        fs_776_[13] = -fs_776_[13];
        OpenGL.glMatrixMode(5889);
        OpenGL.glLoadMatrixf(fs_776_, 0);
        OpenGL.glMatrixMode(5888);
    }

    void method13692() {
        int i;
        for (i = 0; i < ((Class505_Sub1)this).anInt8437; i++) {
            Class282_Sub24 class282_sub24 = ((Class505_Sub1)this).aClass282_Sub24Array8435[i];
            int i_777_ = 16386 + i;
            aFloatArray8322[0] = (float)class282_sub24.method12368((byte)-59);
            aFloatArray8322[1] = (float)class282_sub24.method12369(1534020223);
            aFloatArray8322[2] = (float)class282_sub24.method12394(1995940547);
            aFloatArray8322[3] = 1.0F;
            OpenGL.glLightfv(i_777_, 4611, aFloatArray8322, 0);
            int i_778_ = class282_sub24.method12371(-2140022291);
            float f = class282_sub24.method12395(-1001462313) / 255.0F;
            aFloatArray8322[0] = (float)(i_778_ >> 16 & 0xff) * f;
            aFloatArray8322[1] = (float)(i_778_ >> 8 & 0xff) * f;
            aFloatArray8322[2] = (float)(i_778_ & 0xff) * f;
            OpenGL.glLightfv(i_777_, 4609, aFloatArray8322, 0);
            OpenGL.glLightf(i_777_, 4617, (1.0F / (float)(class282_sub24.method12370(-789603523) * class282_sub24.method12370(-789603523))));
            OpenGL.glEnable(i_777_);
        }
		for (/**/; i < ((Class505_Sub1)this).anInt8436; i++) {
			OpenGL.glDisable(16386 + i);
		}
        ((Class505_Sub1)this).anInt8436 = ((Class505_Sub1)this).anInt8437;
    }

    void method13693() {
        int i;
        for (i = 0; i < ((Class505_Sub1)this).anInt8437; i++) {
            Class282_Sub24 class282_sub24 = ((Class505_Sub1)this).aClass282_Sub24Array8435[i];
            int i_779_ = 16386 + i;
            aFloatArray8322[0] = (float)class282_sub24.method12368((byte)42);
            aFloatArray8322[1] = (float)class282_sub24.method12369(1534020223);
            aFloatArray8322[2] = (float)class282_sub24.method12394(1586441535);
            aFloatArray8322[3] = 1.0F;
            OpenGL.glLightfv(i_779_, 4611, aFloatArray8322, 0);
            int i_780_ = class282_sub24.method12371(-2139014607);
            float f = class282_sub24.method12395(-65023505) / 255.0F;
            aFloatArray8322[0] = (float)(i_780_ >> 16 & 0xff) * f;
            aFloatArray8322[1] = (float)(i_780_ >> 8 & 0xff) * f;
            aFloatArray8322[2] = (float)(i_780_ & 0xff) * f;
            OpenGL.glLightfv(i_779_, 4609, aFloatArray8322, 0);
            OpenGL.glLightf(i_779_, 4617, (1.0F / (float)(class282_sub24.method12370(-789603523) * class282_sub24.method12370(-789603523))));
            OpenGL.glEnable(i_779_);
        }
		for (/**/; i < ((Class505_Sub1)this).anInt8436; i++) {
			OpenGL.glDisable(16386 + i);
		}
        ((Class505_Sub1)this).anInt8436 = ((Class505_Sub1)this).anInt8437;
    }

    final void method13694(float f, float f_781_) {
        ((Class505_Sub1)this).aFloat8446 = f;
        ((Class505_Sub1)this).aFloat8355 = f_781_;
        method13594();
    }

    final void method13695(float f, float f_782_) {
        ((Class505_Sub1)this).aFloat8446 = f;
        ((Class505_Sub1)this).aFloat8355 = f_782_;
        method13594();
    }

    void method13696() {
        aFloatArray8497[0] = (((Class505_Sub1)this).aFloat8432 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (((Class505_Sub1)this).aFloat8432 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (((Class505_Sub1)this).aFloat8432 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightModelfv(2899, aFloatArray8497, 0);
    }

    void method13697() {
        aFloatArray8497[0] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16384, 4609, aFloatArray8497, 0);
        aFloatArray8497[0] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16385, 4609, aFloatArray8497, 0);
    }

    void method13698() {
        aFloatArray8497[0] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16384, 4609, aFloatArray8497, 0);
        aFloatArray8497[0] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16385, 4609, aFloatArray8497, 0);
    }

    void method13699() {
        aFloatArray8497[0] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (((Class505_Sub1)this).aFloat8433 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16384, 4609, aFloatArray8497, 0);
        aFloatArray8497[0] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8429);
        aFloatArray8497[1] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8430);
        aFloatArray8497[2] = (-((Class505_Sub1)this).aFloat8434 * ((Class505_Sub1)this).aFloat8431);
        aFloatArray8497[3] = 1.0F;
        OpenGL.glLightfv(16385, 4609, aFloatArray8497, 0);
    }

    final void method13700(int i, int i_783_) {
        if (((Class505_Sub1)this).anInt8458 == 0) {
            boolean bool = false;
            if (((Class505_Sub1)this).anInt8455 != i) {
                OpenGL.glTexEnvi(8960, 34161, i);
                ((Class505_Sub1)this).anInt8455 = i;
                bool = true;
            }
            if (((Class505_Sub1)this).anInt8451 != i_783_) {
                OpenGL.glTexEnvi(8960, 34162, i_783_);
                ((Class505_Sub1)this).anInt8451 = i_783_;
                bool = true;
            }
			if (bool) {
				((Class505_Sub1)this).anInt8382 &= ~0xd;
			}
        } else {
            OpenGL.glTexEnvi(8960, 34161, i);
            OpenGL.glTexEnvi(8960, 34162, i_783_);
        }
    }

    public final Class384 method8589() {
        return new Class384(((Class505_Sub1)this).aClass384_8442);
    }

    public void N(int i, int i_784_, int i_785_, int i_786_, int i_787_, int i_788_, byte[] is, int i_789_, int i_790_) {
        float f;
        float f_791_;
        if (((Class505_Sub1)this).aClass137_Sub1_Sub1_8462 == null || (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9087 < i_785_) || (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9086 < i_786_)) {
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462 = Class137_Sub1_Sub1.method15540(this, Class150.aClass150_1951, Class76.aClass76_751, i_785_, i_786_, false, is, Class150.aClass150_1951);
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462.method14445(false, false);
            f = ((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10134;
            f_791_ = (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10132);
        } else {
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462.method14455(0, 0, i_785_, i_786_, is, Class150.aClass150_1951, 0, 0, false);
            f = (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10134 * (float)i_786_ / (float)(((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9086));
            f_791_ = ((((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10132) * (float)i_785_ / (float)(((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9087));
        }
        method13637();
        method13654(((Class505_Sub1)this).aClass137_Sub1_Sub1_8462);
        method13624(i_790_);
        OpenGL.glColor4ub((byte)(i_787_ >> 16), (byte)(i_787_ >> 8), (byte)i_787_, (byte)(i_787_ >> 24));
        method13617(i_788_);
        method13717(34165, 34165);
        method13595(0, 34166, 768);
        method13595(2, 5890, 770);
        method13616(0, 34166, 770);
        method13616(2, 5890, 770);
        float f_792_ = (float)i;
        float f_793_ = (float)i_784_;
        float f_794_ = f_792_ + (float)i_785_;
        float f_795_ = f_793_ + (float)i_786_;
        OpenGL.glBegin(7);
        OpenGL.glTexCoord2f(0.0F, 0.0F);
        OpenGL.glVertex2f(f_792_, f_793_);
        OpenGL.glTexCoord2f(0.0F, f_791_);
        OpenGL.glVertex2f(f_792_, f_795_);
        OpenGL.glTexCoord2f(f, f_791_);
        OpenGL.glVertex2f(f_794_, f_795_);
        OpenGL.glTexCoord2f(f, 0.0F);
        OpenGL.glVertex2f(f_794_, f_793_);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
        method13595(2, 34166, 770);
        method13616(0, 5890, 770);
        method13616(2, 34166, 770);
    }

    public final void fh(int[] is) {
        is[0] = ((Class505_Sub1)this).anInt8413;
        is[1] = ((Class505_Sub1)this).anInt8415;
        is[2] = ((Class505_Sub1)this).anInt8412;
        is[3] = ((Class505_Sub1)this).anInt8478;
    }

    final Interface14 method13701(int i, byte[] is, int i_796_, boolean bool) {
		if (((Class505_Sub1)this).aBool8309 && (!bool || ((Class505_Sub1)this).aBool8362)) {
			return new Class135_Sub2(this, i, is, i_796_, bool);
		}
        return new Class131_Sub1(this, i, is, i_796_);
    }

    final void method13702(Interface14 interface14) {
        if (((Class505_Sub1)this).anInterface14_8483 != interface14) {
			if (((Class505_Sub1)this).aBool8309) {
				OpenGL.glBindBufferARB(34962, interface14.method1());
			}
            ((Class505_Sub1)this).anInterface14_8483 = interface14;
        }
    }

    public final void ii(int i) {
        ((Class505_Sub1)this).anInt8473 = 0;
		for (/**/; i > 1; i >>= 1) {
			((Class505_Sub1)this).anInt8473++;
		}
        ((Class505_Sub1)this).anInt8466 = 1 << ((Class505_Sub1)this).anInt8473;
    }

    public void method8547(int i, Class282_Sub24[] class282_sub24s) {
		for (int i_797_ = 0; i_797_ < i; i_797_++) {
			((Class505_Sub1)this).aClass282_Sub24Array8435[i_797_] = class282_sub24s[i_797_];
		}
        ((Class505_Sub1)this).anInt8437 = i;
		if (((Class505_Sub1)this).anInt8409 != 1) {
			method13638();
		}
    }

    final void method13703(int i, int i_798_, int i_799_) {
        OpenGL.glDrawArrays(i, i_798_, i_799_);
    }

    boolean method13704() {
        return ((Class505_Sub1)this).aClass146_8356.method2465(3);
    }

    final void method13705(int i, int i_800_, int i_801_) {
        OpenGL.glDrawArrays(i, i_800_, i_801_);
    }

    public void method8650(float f, float f_802_, float f_803_, float[] fs) {
        float f_804_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[15] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[3] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[7] * f_802_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[11] * f_803_));
        float f_805_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[12] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[0] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[4] * f_802_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[8] * f_803_));
        float f_806_ = (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[13] + ((Class505_Sub1)this).aClass384_8394.aFloatArray4667[1] * f + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[5] * f_802_) + (((Class505_Sub1)this).aClass384_8394.aFloatArray4667[9] * f_803_));
        float f_807_ = (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[14] + ((Class505_Sub1)this).aClass384_8348.aFloatArray4667[2] * f + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[6] * f_802_) + (((Class505_Sub1)this).aClass384_8348.aFloatArray4667[10] * f_803_));
        fs[0] = (((Class505_Sub1)this).aFloat8315 + ((Class505_Sub1)this).aFloat8398 * f_805_ / f_804_);
        fs[1] = (((Class505_Sub1)this).aFloat8399 + ((Class505_Sub1)this).aFloat8400 * f_806_ / f_804_);
        fs[2] = f_807_;
    }

    final void method13706(Interface15 interface15, int i, int i_808_, int i_809_) {
        method13663(interface15);
        OpenGL.glDrawElements(i, i_809_, 5123, interface15.method2() + (long)(i_808_ * 2));
    }

    final synchronized void method13707(int i, int i_810_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_810_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8377.method7877(class282_sub38, -89465938);
    }

    final void method13708() {
        if (((Class505_Sub1)this).anInt8382 != 1) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13654(null);
            method13581(-2);
            method13612(1);
            ((Class505_Sub1)this).anInt8382 = 1;
        }
    }

    final void method13709() {
        if (((Class505_Sub1)this).anInt8382 != 2) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13581(-2);
            ((Class505_Sub1)this).anInt8382 = 2;
        }
    }

    final void method13710(int i, boolean bool, boolean bool_811_) {
        if (i != ((Class505_Sub1)this).anInt8454 || (((Class505_Sub1)this).aBool8449 != ((Class505_Sub1)this).aBool8448)) {
            Class137_Sub1 class137_sub1 = null;
            int i_812_ = 0;
            byte i_813_ = 0;
            int i_814_ = 0;
            byte i_815_ = ((Class505_Sub1)this).aBool8448 ? (byte)3 : (byte)0;
			if (i >= 0) {
				class137_sub1 = ((Class505_Sub1)this).aClass167_8481.method2858(i);
				Class169 class169 = anInterface22_5834.method144(i, -1937389307);
				if (class169.aByte2081 != 0 || class169.aByte2090 != 0) {
					method13602(((float)(((Class505_Sub1)this).anInt8368 % 128000) / 1000.0F * (float)class169.aByte2081 / 64.0F % 1.0F), ((float)(((Class505_Sub1)this).anInt8368 % 128000) / 1000.0F * (float)class169.aByte2090 / 64.0F % 1.0F), 0.0F);
				} else {
					method13618();
				}
				if (!((Class505_Sub1)this).aBool8448) {
					i_813_ = class169.aByte2076;
					i_814_ = class169.anInt2077 * 1385119855;
					i_815_ = class169.aByte2064;
				}
				i_812_ = class169.anInt2091 * 1436865495;
			} else {
				method13618();
			}
            ((Class505_Sub1)this).aClass146_8356.method2462(i_815_, i_813_, i_814_, bool, bool_811_);
            if (!((Class505_Sub1)this).aClass146_8356.method2463(class137_sub1, i_812_)) {
                method13654(class137_sub1);
                method13612(i_812_);
            }
            ((Class505_Sub1)this).aBool8449 = ((Class505_Sub1)this).aBool8448;
            ((Class505_Sub1)this).anInt8454 = i;
        }
        ((Class505_Sub1)this).anInt8382 &= ~0x7;
    }

    final void method13711() {
        if (((Class505_Sub1)this).anInt8382 != 2) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13581(-2);
            ((Class505_Sub1)this).anInt8382 = 2;
        }
    }

    public void hi(int i, Class455 class455, int i_816_, int i_817_) {
        Class455_Sub2 class455_sub2 = (Class455_Sub2)class455;
        Class137_Sub1_Sub1 class137_sub1_sub1 = ((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974;
        method13637();
        method13654(((Class455_Sub2)class455_sub2).aClass137_Sub1_Sub1_8974);
        method13624(1);
        method13717(7681, 8448);
        method13595(0, 34167, 768);
        float f = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10132 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10136);
        float f_818_ = (((Class137_Sub1_Sub1)class137_sub1_sub1).aFloat10134 / (float)((Class137_Sub1_Sub1)class137_sub1_sub1).anInt10133);
        OpenGL.glColor4ub((byte)(i >> 16), (byte)(i >> 8), (byte)i, (byte)(i >> 24));
        OpenGL.glBegin(7);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_816_), f_818_ * (float)(((Class505_Sub1)this).anInt8415 - i_817_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8415);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8413 - i_816_), f_818_ * (float)(((Class505_Sub1)this).anInt8478 - i_817_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8413, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_816_), f_818_ * (float)(((Class505_Sub1)this).anInt8478 - i_817_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8478);
        OpenGL.glTexCoord2f(f * (float)(((Class505_Sub1)this).anInt8412 - i_816_), f_818_ * (float)(((Class505_Sub1)this).anInt8415 - i_817_));
        OpenGL.glVertex2i(((Class505_Sub1)this).anInt8412, ((Class505_Sub1)this).anInt8415);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
    }

    final void method13712() {
        if (((Class505_Sub1)this).anInt8382 != 8) {
            method13645();
            method13642(true);
            method13656(true);
            method13623(true);
            method13624(1);
            ((Class505_Sub1)this).anInt8382 = 8;
        }
    }

    final synchronized void method13713(int i, int i_819_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_819_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8377.method7877(class282_sub38, 429831388);
    }

    final void method13714(float f, float f_820_, float f_821_) {
        OpenGL.glMatrixMode(5890);
		if (((Class505_Sub1)this).aBool8457) {
			OpenGL.glLoadIdentity();
		}
        OpenGL.glTranslatef(f, f_820_, f_821_);
        OpenGL.glMatrixMode(5888);
        ((Class505_Sub1)this).aBool8457 = true;
    }

    public void ff(int i, int i_822_, int i_823_, int i_824_, int i_825_, int i_826_, byte[] is, int i_827_, int i_828_) {
        float f;
        float f_829_;
        if (((Class505_Sub1)this).aClass137_Sub1_Sub1_8462 == null || (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9087 < i_823_) || (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9086 < i_824_)) {
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462 = Class137_Sub1_Sub1.method15540(this, Class150.aClass150_1951, Class76.aClass76_751, i_823_, i_824_, false, is, Class150.aClass150_1951);
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462.method14445(false, false);
            f = ((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10134;
            f_829_ = (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10132);
        } else {
            ((Class505_Sub1)this).aClass137_Sub1_Sub1_8462.method14455(0, 0, i_823_, i_824_, is, Class150.aClass150_1951, 0, 0, false);
            f = (((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10134 * (float)i_824_ / (float)(((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9086));
            f_829_ = ((((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).aFloat10132) * (float)i_823_ / (float)(((Class137_Sub1_Sub1)((Class505_Sub1)this).aClass137_Sub1_Sub1_8462).anInt9087));
        }
        method13637();
        method13654(((Class505_Sub1)this).aClass137_Sub1_Sub1_8462);
        method13624(i_828_);
        OpenGL.glColor4ub((byte)(i_825_ >> 16), (byte)(i_825_ >> 8), (byte)i_825_, (byte)(i_825_ >> 24));
        method13617(i_826_);
        method13717(34165, 34165);
        method13595(0, 34166, 768);
        method13595(2, 5890, 770);
        method13616(0, 34166, 770);
        method13616(2, 5890, 770);
        float f_830_ = (float)i;
        float f_831_ = (float)i_822_;
        float f_832_ = f_830_ + (float)i_823_;
        float f_833_ = f_831_ + (float)i_824_;
        OpenGL.glBegin(7);
        OpenGL.glTexCoord2f(0.0F, 0.0F);
        OpenGL.glVertex2f(f_830_, f_831_);
        OpenGL.glTexCoord2f(0.0F, f_829_);
        OpenGL.glVertex2f(f_830_, f_833_);
        OpenGL.glTexCoord2f(f, f_829_);
        OpenGL.glVertex2f(f_832_, f_833_);
        OpenGL.glTexCoord2f(f, 0.0F);
        OpenGL.glVertex2f(f_832_, f_831_);
        OpenGL.glEnd();
        method13595(0, 5890, 768);
        method13595(2, 34166, 770);
        method13616(0, 5890, 770);
        method13616(2, 34166, 770);
    }

    final void method13715() {
        if (((Class505_Sub1)this).anInt8382 != 1) {
            method13587();
            method13642(false);
            method13620(false);
            method13656(false);
            method13623(false);
            method13654(null);
            method13581(-2);
            method13612(1);
            ((Class505_Sub1)this).anInt8382 = 1;
        }
    }

    final void method13716(Class137 class137) {
        Class137 class137_834_ = (((Class505_Sub1)this).aClass137Array8482[((Class505_Sub1)this).anInt8458]);
        if (class137_834_ != class137) {
			if (class137 != null) {
				if (class137_834_ != null) {
					if (((Class137)class137).anInt1648 != ((Class137)class137_834_).anInt1648) {
						OpenGL.glDisable(((Class137)class137_834_).anInt1648);
						OpenGL.glEnable(((Class137)class137).anInt1648);
					}
				} else {
					OpenGL.glEnable(((Class137)class137).anInt1648);
				}
				OpenGL.glBindTexture(((Class137)class137).anInt1648, ((Class137)class137).anInt1647);
			} else {
				OpenGL.glDisable(((Class137)class137_834_).anInt1648);
			}
            ((Class505_Sub1)this).aClass137Array8482[(((Class505_Sub1)this).anInt8458)] = class137;
        }
        ((Class505_Sub1)this).anInt8382 &= ~0x1;
    }

    public final void method8477(Class152 class152) {
        ((Class505_Sub1)this).aClass152_Sub1_8317 = (Class152_Sub1)class152;
    }

    final void method13717(int i, int i_835_) {
        if (((Class505_Sub1)this).anInt8458 == 0) {
            boolean bool = false;
            if (((Class505_Sub1)this).anInt8455 != i) {
                OpenGL.glTexEnvi(8960, 34161, i);
                ((Class505_Sub1)this).anInt8455 = i;
                bool = true;
            }
            if (((Class505_Sub1)this).anInt8451 != i_835_) {
                OpenGL.glTexEnvi(8960, 34162, i_835_);
                ((Class505_Sub1)this).anInt8451 = i_835_;
                bool = true;
            }
			if (bool) {
				((Class505_Sub1)this).anInt8382 &= ~0xd;
			}
        } else {
            OpenGL.glTexEnvi(8960, 34161, i);
            OpenGL.glTexEnvi(8960, 34162, i_835_);
        }
    }

    final void method13718(int i) {
        aFloatArray8497[0] = (float)(i & 0xff0000) / 1.671168E7F;
        aFloatArray8497[1] = (float)(i & 0xff00) / 65280.0F;
        aFloatArray8497[2] = (float)(i & 0xff) / 255.0F;
        aFloatArray8497[3] = (float)(i >>> 24) / 255.0F;
        OpenGL.glTexEnvfv(8960, 8705, aFloatArray8497, 0);
    }

    final void method13719(float[] fs) {
        float[] fs_836_ = new float[16];
        System.arraycopy(fs, 0, fs_836_, 0, 16);
        fs_836_[1] = -fs_836_[1];
        fs_836_[5] = -fs_836_[5];
        fs_836_[9] = -fs_836_[9];
        fs_836_[13] = -fs_836_[13];
        OpenGL.glMatrixMode(5889);
        OpenGL.glLoadMatrixf(fs_836_, 0);
        OpenGL.glMatrixMode(5888);
    }

    final void method13720(int i, int i_837_) {
        if (((Class505_Sub1)this).anInt8458 == 0) {
            boolean bool = false;
            if (((Class505_Sub1)this).anInt8455 != i) {
                OpenGL.glTexEnvi(8960, 34161, i);
                ((Class505_Sub1)this).anInt8455 = i;
                bool = true;
            }
            if (((Class505_Sub1)this).anInt8451 != i_837_) {
                OpenGL.glTexEnvi(8960, 34162, i_837_);
                ((Class505_Sub1)this).anInt8451 = i_837_;
                bool = true;
            }
			if (bool) {
				((Class505_Sub1)this).anInt8382 &= ~0xd;
			}
        } else {
            OpenGL.glTexEnvi(8960, 34161, i);
            OpenGL.glTexEnvi(8960, 34162, i_837_);
        }
    }

    final void method13721(int i, int i_838_) {
        if (((Class505_Sub1)this).anInt8458 == 0) {
            boolean bool = false;
            if (((Class505_Sub1)this).anInt8455 != i) {
                OpenGL.glTexEnvi(8960, 34161, i);
                ((Class505_Sub1)this).anInt8455 = i;
                bool = true;
            }
            if (((Class505_Sub1)this).anInt8451 != i_838_) {
                OpenGL.glTexEnvi(8960, 34162, i_838_);
                ((Class505_Sub1)this).anInt8451 = i_838_;
                bool = true;
            }
			if (bool) {
				((Class505_Sub1)this).anInt8382 &= ~0xd;
			}
        } else {
            OpenGL.glTexEnvi(8960, 34161, i);
            OpenGL.glTexEnvi(8960, 34162, i_838_);
        }
    }

    final void method13722(int i, int i_839_, int i_840_) {
        OpenGL.glTexEnvi(8960, 34176 + i, i_839_);
        OpenGL.glTexEnvi(8960, 34192 + i, i_840_);
    }

    public boolean method8462() {
        return (((Class505_Sub1)this).aBool8342 && (!method8471() || ((Class505_Sub1)this).aBool8344));
    }

    final void method13723(float f, float f_841_, float f_842_, float f_843_) {
        aFloatArray8497[0] = f;
        aFloatArray8497[1] = f_841_;
        aFloatArray8497[2] = f_842_;
        aFloatArray8497[3] = f_843_;
        OpenGL.glTexEnvfv(8960, 8705, aFloatArray8497, 0);
    }

    public boolean method8406() {
        return true;
    }

    final void method13724(float f, float f_844_, float f_845_) {
        OpenGL.glMatrixMode(5890);
		if (((Class505_Sub1)this).aBool8457) {
			OpenGL.glLoadIdentity();
		}
        OpenGL.glTranslatef(f, f_844_, f_845_);
        OpenGL.glMatrixMode(5888);
        ((Class505_Sub1)this).aBool8457 = true;
    }

    public boolean method8504() {
        return true;
    }

    public void method8494(int i, int i_846_, int i_847_, int i_848_, int i_849_, int i_850_, int i_851_, int i_852_, int i_853_) {
        if (i != i_847_ || i_846_ != i_848_) {
            method13659();
            method13624(i_850_);
            float f = (float)i_847_ - (float)i;
            float f_854_ = (float)i_848_ - (float)i_846_;
            float f_855_ = (float)(1.0 / Math.sqrt((double)(f * f + f_854_ * f_854_)));
            f *= f_855_;
            f_854_ *= f_855_;
            OpenGL.glColor4ub((byte)(i_849_ >> 16), (byte)(i_849_ >> 8), (byte)i_849_, (byte)(i_849_ >> 24));
            i_853_ %= i_852_ + i_851_;
            float f_856_ = f * (float)i_851_;
            float f_857_ = f_854_ * (float)i_851_;
            float f_858_ = 0.0F;
            float f_859_ = 0.0F;
            float f_860_ = f_856_;
            float f_861_ = f_857_;
            if (i_853_ > i_851_) {
                f_858_ = f * (float)(i_851_ + i_852_ - i_853_);
                f_859_ = f_854_ * (float)(i_851_ + i_852_ - i_853_);
            } else {
                f_860_ = f * (float)(i_851_ - i_853_);
                f_861_ = f_854_ * (float)(i_851_ - i_853_);
            }
            float f_862_ = (float)i + 0.35F + f_858_;
            float f_863_ = (float)i_846_ + 0.35F + f_859_;
            float f_864_ = f * (float)i_852_;
            float f_865_ = f_854_ * (float)i_852_;
            for (; ; ) {
                if (i_847_ > i) {
					if (f_862_ > (float)i_847_ + 0.35F) {
						break;
					}
					if (f_862_ + f_860_ > (float)i_847_) {
						f_860_ = (float)i_847_ - f_862_;
					}
                } else {
					if (f_862_ < (float)i_847_ + 0.35F) {
						break;
					}
					if (f_862_ + f_860_ < (float)i_847_) {
						f_860_ = (float)i_847_ - f_862_;
					}
                }
                if (i_848_ > i_846_) {
					if (f_863_ > (float)i_848_ + 0.35F) {
						break;
					}
					if (f_863_ + f_861_ > (float)i_848_) {
						f_861_ = (float)i_848_ - f_863_;
					}
                } else {
					if (f_863_ < (float)i_848_ + 0.35F) {
						break;
					}
					if (f_863_ + f_861_ < (float)i_848_) {
						f_861_ = (float)i_848_ - f_863_;
					}
                }
                OpenGL.glBegin(1);
                OpenGL.glVertex2f(f_862_, f_863_);
                OpenGL.glVertex2f(f_862_ + f_860_, f_863_ + f_861_);
                OpenGL.glEnd();
                f_862_ += f_864_ + f_860_;
                f_863_ += f_865_ + f_861_;
                f_860_ = f_856_;
                f_861_ = f_857_;
            }
        }
    }

    final void method13725(float f, float f_866_, float f_867_) {
        OpenGL.glMatrixMode(5890);
		if (((Class505_Sub1)this).aBool8457) {
			OpenGL.glLoadIdentity();
		}
        OpenGL.glTranslatef(f, f_866_, f_867_);
        OpenGL.glMatrixMode(5888);
        ((Class505_Sub1)this).aBool8457 = true;
    }

    final void method13726() {
        if (((Class505_Sub1)this).aBool8457) {
            OpenGL.glMatrixMode(5890);
            OpenGL.glLoadIdentity();
            OpenGL.glMatrixMode(5888);
            ((Class505_Sub1)this).aBool8457 = false;
        }
    }

    final void method13727() {
        if (((Class505_Sub1)this).aBool8457) {
            OpenGL.glMatrixMode(5890);
            OpenGL.glLoadIdentity();
            OpenGL.glMatrixMode(5888);
            ((Class505_Sub1)this).aBool8457 = false;
        }
    }

    final void method13728() {
        if (((Class505_Sub1)this).aBool8457) {
            OpenGL.glMatrixMode(5890);
            OpenGL.glLoadIdentity();
            OpenGL.glMatrixMode(5888);
            ((Class505_Sub1)this).aBool8457 = false;
        }
    }

    public int method8463() {
        return 4;
    }

    final void method13729(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8440) {
            ((Class505_Sub1)this).aBool8440 = bool;
            method13747();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    final void method13730(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8440) {
            ((Class505_Sub1)this).aBool8440 = bool;
            method13747();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    final void method13731(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8422) {
            ((Class505_Sub1)this).aBool8422 = bool;
            method13622();
            ((Class505_Sub1)this).anInt8382 &= ~0x7;
        }
    }

    final void method13732(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8422) {
            ((Class505_Sub1)this).aBool8422 = bool;
            method13622();
            ((Class505_Sub1)this).anInt8382 &= ~0x7;
        }
    }

    final void method13733(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8397) {
            ((Class505_Sub1)this).aBool8397 = bool;
            method13622();
        }
    }

    public final void method8424(Class384 class384) {
        ((Class505_Sub1)this).aClass384_8442.method6562(class384);
        method13597();
        method13588();
    }

    void method13734() {
		if (((Class505_Sub1)this).aBool8422 && !((Class505_Sub1)this).aBool8397) {
			OpenGL.glEnable(2896);
		} else {
			OpenGL.glDisable(2896);
		}
    }

    final void method13735(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8387) {
			if (bool) {
				OpenGL.glEnable(2929);
			} else {
				OpenGL.glDisable(2929);
			}
            ((Class505_Sub1)this).aBool8387 = bool;
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    final void method13736(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8388) {
            ((Class505_Sub1)this).aBool8388 = bool;
            method13579();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    final void method13737(boolean bool) {
        if (bool != ((Class505_Sub1)this).aBool8388) {
            ((Class505_Sub1)this).aBool8388 = bool;
            method13579();
            ((Class505_Sub1)this).anInt8382 &= ~0xf;
        }
    }

    final void method13738() {
        OpenGL.glDepthMask(((Class505_Sub1)this).aBool8388 && ((Class505_Sub1)this).aBool8353);
    }

    final void method13739() {
        OpenGL.glDepthMask(((Class505_Sub1)this).aBool8388 && ((Class505_Sub1)this).aBool8353);
    }

    final synchronized void method13740(int i, int i_868_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_868_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8375.method7877(class282_sub38, 786064968);
    }

    final void method13741(int i) {
        if (((Class505_Sub1)this).anInt8366 != i) {
            int i_869_;
            boolean bool;
            boolean bool_870_;
            if (i == 1) {
                i_869_ = 1;
                bool = true;
                bool_870_ = true;
            } else if (i == 2) {
                i_869_ = 2;
                bool = true;
                bool_870_ = false;
            } else if (i == 128) {
                i_869_ = 3;
                bool = true;
                bool_870_ = true;
            } else {
                i_869_ = 0;
                bool = true;
                bool_870_ = false;
            }
            if (bool != ((Class505_Sub1)this).aBool8459) {
                OpenGL.glColorMask(bool, bool, bool, true);
                ((Class505_Sub1)this).aBool8459 = bool;
            }
            if (bool_870_ != ((Class505_Sub1)this).aBool8408) {
				if (bool_870_) {
					OpenGL.glEnable(3008);
				} else {
					OpenGL.glDisable(3008);
				}
                ((Class505_Sub1)this).aBool8408 = bool_870_;
            }
            if (i_869_ != ((Class505_Sub1)this).anInt8384) {
                if (i_869_ == 1) {
                    OpenGL.glEnable(3042);
                    OpenGL.glBlendFunc(770, 771);
                } else if (i_869_ == 2) {
                    OpenGL.glEnable(3042);
                    OpenGL.glBlendFunc(1, 1);
                } else if (i_869_ == 3) {
					OpenGL.glEnable(3042);
					OpenGL.glBlendFunc(774, 1);
				} else {
					OpenGL.glDisable(3042);
				}
                ((Class505_Sub1)this).anInt8384 = i_869_;
            }
            ((Class505_Sub1)this).anInt8366 = i;
            ((Class505_Sub1)this).anInt8382 &= ~0xc;
        }
    }

    final synchronized void method13742(int i, int i_871_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_871_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8486.method7877(class282_sub38, 958095911);
    }

    final synchronized void method13743(int i, int i_872_) {
        Class282_Sub38 class282_sub38 = new Class282_Sub38(i_872_);
        class282_sub38.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8486.method7877(class282_sub38, 1617814384);
    }

    final Interface15 method13744(int i, byte[] is, int i_873_, boolean bool) {
		if (((Class505_Sub1)this).aBool8309 && (!bool || ((Class505_Sub1)this).aBool8362)) {
			return new Class135_Sub1(this, i, is, i_873_, bool);
		}
        return new Class131_Sub2(this, i, is, i_873_);
    }

    public void method8530(int i, int i_874_, int i_875_, int i_876_, int i_877_, int i_878_, int i_879_, int i_880_, int i_881_) {
        if (i != i_875_ || i_874_ != i_876_) {
            method13659();
            method13624(i_878_);
            float f = (float)i_875_ - (float)i;
            float f_882_ = (float)i_876_ - (float)i_874_;
            float f_883_ = (float)(1.0 / Math.sqrt((double)(f * f + f_882_ * f_882_)));
            f *= f_883_;
            f_882_ *= f_883_;
            OpenGL.glColor4ub((byte)(i_877_ >> 16), (byte)(i_877_ >> 8), (byte)i_877_, (byte)(i_877_ >> 24));
            i_881_ %= i_880_ + i_879_;
            float f_884_ = f * (float)i_879_;
            float f_885_ = f_882_ * (float)i_879_;
            float f_886_ = 0.0F;
            float f_887_ = 0.0F;
            float f_888_ = f_884_;
            float f_889_ = f_885_;
            if (i_881_ > i_879_) {
                f_886_ = f * (float)(i_879_ + i_880_ - i_881_);
                f_887_ = f_882_ * (float)(i_879_ + i_880_ - i_881_);
            } else {
                f_888_ = f * (float)(i_879_ - i_881_);
                f_889_ = f_882_ * (float)(i_879_ - i_881_);
            }
            float f_890_ = (float)i + 0.35F + f_886_;
            float f_891_ = (float)i_874_ + 0.35F + f_887_;
            float f_892_ = f * (float)i_880_;
            float f_893_ = f_882_ * (float)i_880_;
            for (; ; ) {
                if (i_875_ > i) {
					if (f_890_ > (float)i_875_ + 0.35F) {
						break;
					}
					if (f_890_ + f_888_ > (float)i_875_) {
						f_888_ = (float)i_875_ - f_890_;
					}
                } else {
					if (f_890_ < (float)i_875_ + 0.35F) {
						break;
					}
					if (f_890_ + f_888_ < (float)i_875_) {
						f_888_ = (float)i_875_ - f_890_;
					}
                }
                if (i_876_ > i_874_) {
					if (f_891_ > (float)i_876_ + 0.35F) {
						break;
					}
					if (f_891_ + f_889_ > (float)i_876_) {
						f_889_ = (float)i_876_ - f_891_;
					}
                } else {
					if (f_891_ < (float)i_876_ + 0.35F) {
						break;
					}
					if (f_891_ + f_889_ < (float)i_876_) {
						f_889_ = (float)i_876_ - f_891_;
					}
                }
                OpenGL.glBegin(1);
                OpenGL.glVertex2f(f_890_, f_891_);
                OpenGL.glVertex2f(f_890_ + f_888_, f_891_ + f_889_);
                OpenGL.glEnd();
                f_890_ += f_892_ + f_888_;
                f_891_ += f_893_ + f_889_;
                f_888_ = f_884_;
                f_889_ = f_885_;
            }
        }
    }

    void method13745() {
		if (((Class505_Sub1)this).aBool8422 && !((Class505_Sub1)this).aBool8397) {
			OpenGL.glEnable(2896);
		} else {
			OpenGL.glDisable(2896);
		}
    }

    public boolean method8405() {
        return (((Class505_Sub1)this).aBool8342 && (!method8471() || ((Class505_Sub1)this).aBool8344));
    }

    final void method13746(int i, int i_894_, int i_895_) {
        OpenGL.glDrawArrays(i, i_894_, i_895_);
    }

    void method13747() {
		if (((Class505_Sub1)this).aBool8440 && ((Class505_Sub1)this).anInt8358 >= 0) {
			OpenGL.glEnable(2912);
		} else {
			OpenGL.glDisable(2912);
		}
    }

    final void method13748() {
        OpenGL.glLoadIdentity();
        OpenGL.glMultMatrixf((((Class505_Sub1)this).aClass384_8348.aFloatArray4667), 0);
		if (((Class505_Sub1)this).aBool8449) {
			((Class146)((Class505_Sub1)this).aClass146_8356).aClass141_Sub4_1715.method14427();
		}
        method13593();
        method13638();
    }

    final void method13749(int i) {
		if (i == 1) {
			method13717(7681, 7681);
		} else if (i == 0) {
			method13717(8448, 8448);
		} else if (i == 2) {
			method13717(34165, 7681);
		} else if (i == 3) {
			method13717(260, 8448);
		} else if (i == 4) {
			method13717(34023, 34023);
		}
    }

    final synchronized void method13750(int i) {
        Class282 class282 = new Class282();
        class282.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8461.method7877(class282, -46921954);
    }

    public int method8546(int i, int i_896_) {
        return i | i_896_;
    }

    public boolean method8503() {
        return true;
    }

    final synchronized void method13751(int i) {
        Class282 class282 = new Class282();
        class282.aLong3379 = (long)i * -1253863389874800229L;
        ((Class505_Sub1)this).aClass473_8461.method7877(class282, -393844601);
    }

    static int method13752(Class150 class150) {
        switch (class150.anInt1958 * -1436290903) {
            case 4:
                return 6410;
            case 6:
                return 6408;
            case 5:
                return 6407;
            case 0:
                return 6409;
            case 8:
                return 6402;
            default:
                throw new IllegalStateException();
            case 1:
                return 6406;
        }
    }

    static int method13753(Class150 class150, Class76 class76) {
        if (class76 == Class76.aClass76_751) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 6407;
                case 4:
                    return 6410;
                case 6:
                    return 6408;
                case 0:
                    return 6409;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 6406;
            }
        }
        if (class76 == Class76.aClass76_752) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 32852;
                case 4:
                    return 36219;
                case 1:
                    return 32830;
                case 6:
                    return 32859;
                case 0:
                    return 32834;
                case 8:
                    return 33189;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_749) {
            switch (class150.anInt1958 * -1436290903) {
                case 8:
                    return 33190;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_755) {
            switch (class150.anInt1958 * -1436290903) {
                case 6:
                    return 34842;
                case 1:
                    return 34844;
                case 0:
                    return 34846;
                case 4:
                    return 34847;
                default:
                    throw new IllegalArgumentException();
                case 5:
                    return 34843;
            }
        }
        if (class76 == Class76.aClass76_758) {
            switch (class150.anInt1958 * -1436290903) {
                case 0:
                    return 34840;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 34838;
                case 5:
                    return 34837;
                case 4:
                    return 34841;
                case 6:
                    return 34836;
            }
        }
        throw new IllegalArgumentException();
    }

    static int method13754(Class150 class150, Class76 class76) {
        if (class76 == Class76.aClass76_751) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 6407;
                case 4:
                    return 6410;
                case 6:
                    return 6408;
                case 0:
                    return 6409;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 6406;
            }
        }
        if (class76 == Class76.aClass76_752) {
            switch (class150.anInt1958 * -1436290903) {
                case 5:
                    return 32852;
                case 4:
                    return 36219;
                case 1:
                    return 32830;
                case 6:
                    return 32859;
                case 0:
                    return 32834;
                case 8:
                    return 33189;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_749) {
            switch (class150.anInt1958 * -1436290903) {
                case 8:
                    return 33190;
                default:
                    throw new IllegalArgumentException();
            }
        }
        if (class76 == Class76.aClass76_755) {
            switch (class150.anInt1958 * -1436290903) {
                case 6:
                    return 34842;
                case 1:
                    return 34844;
                case 0:
                    return 34846;
                case 4:
                    return 34847;
                default:
                    throw new IllegalArgumentException();
                case 5:
                    return 34843;
            }
        }
        if (class76 == Class76.aClass76_758) {
            switch (class150.anInt1958 * -1436290903) {
                case 0:
                    return 34840;
                default:
                    throw new IllegalArgumentException();
                case 1:
                    return 34838;
                case 5:
                    return 34837;
                case 4:
                    return 34841;
                case 6:
                    return 34836;
            }
        }
        throw new IllegalArgumentException();
    }

    final void method13755(Class384 class384) {
        OpenGL.glPushMatrix();
        OpenGL.glMultMatrixf(class384.aFloatArray4667, 0);
    }

    public Class152 method8636(int i, int i_897_, int i_898_, int i_899_, int i_900_, int i_901_) {
        return (((Class505_Sub1)this).aBool8480 ? new Class152_Sub1_Sub1(this, i, i_897_, i_898_, i_899_, i_900_, i_901_) : null);
    }

    public Class152 method8400(int i, int i_902_, int i_903_, int i_904_, int i_905_, int i_906_) {
        return (((Class505_Sub1)this).aBool8480 ? new Class152_Sub1_Sub1(this, i, i_902_, i_903_, i_904_, i_905_, i_906_) : null);
    }
}
