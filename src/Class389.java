/* Class389 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class389 {
	public static final int anInt4724 = 32768;
	public static final int anInt4725 = 4;
	public static final int anInt4726 = 65536;
	public static final int anInt4727 = 128;
	public static final int anInt4728 = 524288;
	public static final int anInt4729 = 64;
	public static final int anInt4730 = 128;
	public static final int anInt4731 = 8;
	public static final int anInt4732 = 2048;
	public static final int anInt4733 = 32768;
	public static final int anInt4734 = 16384;
	public static final int anInt4735 = 1;
	public static final int anInt4736 = 2;
	public static final int anInt4737 = 512;
	public static final int anInt4738 = 256;
	public static final int anInt4739 = 16;
	public static final int anInt4740 = 2048;
	public static final int anInt4741 = 1048576;
	public static final int anInt4742 = 16777216;
	public static final int anInt4743 = 2097152;
	public static final int anInt4744 = 4194304;
	public static final int anInt4745 = 33554432;
	public static final int anInt4746 = 2097152;
	public static final int anInt4747 = 4;
	public static final int anInt4748 = 16;
	public static final int anInt4749 = 8192;
	public static final int anInt4750 = 8388608;
	public static final int anInt4751 = 2;
	public static final int anInt4752 = 8;
	public static final int anInt4753 = 32;
	public static final int anInt4754 = 64;
	public static final int anInt4755 = 1;
	public static final int anInt4756 = 4096;
	public static final int anInt4757 = 4096;
	public static final int anInt4758 = 1024;
	public static final int anInt4759 = 256;
	public static final int anInt4760 = 16384;
	public static final int anInt4761 = 8192;
	public static final int anInt4762 = 262144;
	public static final int anInt4763 = 8388608;
	public static final int anInt4764 = 65536;
	public static final int anInt4765 = 32;
	public static final int anInt4766 = 4194304;
	public static final int anInt4767 = 524288;
	public static final int anInt4768 = 131072;
	public static final int anInt4769 = 1048576;
	public static final int anInt4770 = 131072;
	public static final int anInt4771 = 262144;

	Class389() throws Throwable {
		throw new Error();
	}

	static final void method6696(Class527 class527, int i) {
		int i_0_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_0_, (byte) 20);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = class118.anInt1334 * 539377845;
	}

	static final void method6697(Class118 class118, Class98 class98, Class527 class527, int i) {
		class118.aBool1356 = ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1);
		Class109.method1858(class118, (byte) 17);
		if (-1 == class118.anInt1288 * 1924549737 && !class98.aBool999)
			Class96_Sub17.method14656(class118.anInt1287 * -1952846363, -768470784);
	}
}
