/* Class101_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class101_Sub2 extends Class101 {
	Class115_Sub1 aClass115_Sub1_9360;
	boolean aBool9361;
	Class505_Sub2_Sub2 aClass505_Sub2_Sub2_9362;

	public void method1690() {
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(0L);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(0L);
		((Class101_Sub2) this).aBool9361 = false;
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = null;
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(1);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(0);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
	}

	Class115 method1743(Class505_Sub2 class505_sub2, Class99 class99) {
		return new Class115_Sub1((Class505_Sub2_Sub2) class505_sub2, this, class99);
	}

	public void method1713() {
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(0L);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(0L);
		((Class101_Sub2) this).aBool9361 = false;
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = null;
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(1);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(0);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
	}

	Class101_Sub2(Class505_Sub2_Sub2 class505_sub2_sub2, Class114 class114) {
		super((Class505_Sub2) class505_sub2_sub2, class114);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362 = class505_sub2_sub2;
		((Class101_Sub2) this).aBool9361 = false;
	}

	public boolean method1655(Class115 class115) {
		if (((Class101_Sub2) this).aClass115_Sub1_9360 == class115)
			return true;
		if (!class115.method1899())
			return false;
		((Class101_Sub2) this).aClass115_Sub1_9360 = (Class115_Sub1) class115;
		anInt1015 = method1653(class115, (byte) -25) * -1224879653;
		if (anInt1015 * -401096109 == -1)
			throw new IllegalArgumentException();
		if (((Class101_Sub2) this).aBool9361) {
			((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9286);
			((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9287);
			((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = ((Class101_Sub2) this).aClass115_Sub1_9360;
		}
		return true;
	}

	public boolean method1693() {
		return ((((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251) == ((Class101_Sub2) this).aClass115_Sub1_9360);
	}

	public boolean method1648() {
		return ((((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251) == ((Class101_Sub2) this).aClass115_Sub1_9360);
	}

	public void method1647() {
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(0L);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(0L);
		((Class101_Sub2) this).aBool9361 = false;
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = null;
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(1);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(0);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
	}

	public void method1686() {
		if (((Class101_Sub2) this).aClass115_Sub1_9360 == null)
			throw new RuntimeException_Sub2();
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9286);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9287);
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = ((Class101_Sub2) this).aClass115_Sub1_9360;
		((Class101_Sub2) this).aBool9361 = true;
	}

	public void method1687() {
		if (((Class101_Sub2) this).aClass115_Sub1_9360 == null)
			throw new RuntimeException_Sub2();
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9286);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9287);
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = ((Class101_Sub2) this).aClass115_Sub1_9360;
		((Class101_Sub2) this).aBool9361 = true;
	}

	Class115 method1683(Class505_Sub2 class505_sub2, Class99 class99) {
		return new Class115_Sub1((Class505_Sub2_Sub2) class505_sub2, this, class99);
	}

	public void method1678() {
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(0L);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(0L);
		((Class101_Sub2) this).aBool9361 = false;
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = null;
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(1);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14163(0);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method14035(null);
	}

	public void method1685() {
		if (((Class101_Sub2) this).aClass115_Sub1_9360 == null)
			throw new RuntimeException_Sub2();
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9286);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9287);
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = ((Class101_Sub2) this).aClass115_Sub1_9360;
		((Class101_Sub2) this).aBool9361 = true;
	}

	public boolean method1664() {
		return ((((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251) == ((Class101_Sub2) this).aClass115_Sub1_9360);
	}

	public boolean method1692() {
		return ((((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251) == ((Class101_Sub2) this).aClass115_Sub1_9360);
	}

	Class282_Sub21_Sub1 method1765(Class122 class122) {
		return new Class282_Sub21_Sub1_Sub1(this, class122);
	}

	public boolean method1734() {
		return ((((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251) == ((Class101_Sub2) this).aClass115_Sub1_9360);
	}

	public void method1646() {
		if (((Class101_Sub2) this).aClass115_Sub1_9360 == null)
			throw new RuntimeException_Sub2();
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9286);
		((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9287);
		((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = ((Class101_Sub2) this).aClass115_Sub1_9360;
		((Class101_Sub2) this).aBool9361 = true;
	}

	Class115 method1665(Class505_Sub2 class505_sub2, Class99 class99) {
		return new Class115_Sub1((Class505_Sub2_Sub2) class505_sub2, this, class99);
	}

	Class115 method1697(Class505_Sub2 class505_sub2, Class99 class99) {
		return new Class115_Sub1((Class505_Sub2_Sub2) class505_sub2, this, class99);
	}

	public boolean method1706(Class115 class115) {
		if (((Class101_Sub2) this).aClass115_Sub1_9360 == class115)
			return true;
		if (!class115.method1899())
			return false;
		((Class101_Sub2) this).aClass115_Sub1_9360 = (Class115_Sub1) class115;
		anInt1015 = method1653(class115, (byte) -36) * -1224879653;
		if (anInt1015 * -401096109 == -1)
			throw new IllegalArgumentException();
		if (((Class101_Sub2) this).aBool9361) {
			((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9286);
			((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9287);
			((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = ((Class101_Sub2) this).aClass115_Sub1_9360;
		}
		return true;
	}

	public boolean method1707(Class115 class115) {
		if (((Class101_Sub2) this).aClass115_Sub1_9360 == class115)
			return true;
		if (!class115.method1899())
			return false;
		((Class101_Sub2) this).aClass115_Sub1_9360 = (Class115_Sub1) class115;
		anInt1015 = method1653(class115, (byte) -83) * -1224879653;
		if (anInt1015 * -401096109 == -1)
			throw new IllegalArgumentException();
		if (((Class101_Sub2) this).aBool9361) {
			((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15666(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9286);
			((Class101_Sub2) this).aClass505_Sub2_Sub2_9362.method15645(((Class115_Sub1) ((Class101_Sub2) this).aClass115_Sub1_9360).aLong9287);
			((Class505_Sub2_Sub2) ((Class101_Sub2) this).aClass505_Sub2_Sub2_9362).aClass115_Sub1_10251 = ((Class101_Sub2) this).aClass115_Sub1_9360;
		}
		return true;
	}

	Class282_Sub21_Sub1 method1694(Class122 class122) {
		return new Class282_Sub21_Sub1_Sub1(this, class122);
	}

	Class282_Sub21_Sub1 method1712(Class122 class122) {
		return new Class282_Sub21_Sub1_Sub1(this, class122);
	}
}
