/* Class369 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class369 {
	static Class369[] aClass369Array4267;
	static Class369 aClass369_4268;
	public static Class369 aClass369_4269;
	public static Class369 aClass369_4270;
	static Class369 aClass369_4271;
	public static Class369 aClass369_4272 = new Class369(14, 0);
	public static Class369 aClass369_4273;
	static Class369 aClass369_4274;
	public static Class369 aClass369_4275;
	public static Class369 aClass369_4276;
	public static Class369 aClass369_4277;
	public int anInt4278;
	public static Class369 aClass369_4279 = new Class369(15, -1);
	public static int anInt4280;

	Class369(int i, int i_0_) {
		anInt4278 = i * -1958225857;
	}

	static {
		aClass369_4277 = new Class369(16, -2);
		aClass369_4270 = new Class369(19, -2);
		aClass369_4271 = new Class369(23, 4);
		aClass369_4268 = new Class369(24, -1);
		aClass369_4273 = new Class369(26, 0);
		aClass369_4274 = new Class369(27, 0);
		aClass369_4275 = new Class369(28, -2);
		aClass369_4276 = new Class369(29, -2);
		aClass369_4269 = new Class369(30, -2);
		aClass369Array4267 = new Class369[32];
		Class369[] class369s = Class345.method6144(-1278250999);
		for (int i = 0; i < class369s.length; i++)
			aClass369Array4267[1627920319 * class369s[i].anInt4278] = class369s[i];
	}

	static Class369[] method6311() {
		return new Class369[] { aClass369_4273, aClass369_4270, aClass369_4279, aClass369_4277, aClass369_4272, aClass369_4269, aClass369_4275, aClass369_4268, aClass369_4274, aClass369_4276, aClass369_4271 };
	}

	static Class369[] method6312() {
		return new Class369[] { aClass369_4273, aClass369_4270, aClass369_4279, aClass369_4277, aClass369_4272, aClass369_4269, aClass369_4275, aClass369_4268, aClass369_4274, aClass369_4276, aClass369_4271 };
	}

	static final void method6313(Class118 class118, Class98 class98, Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 567564004;
		class118.anInt1404 = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]) * -619343517;
		class118.anInt1432 = (((Class527) class527).anIntArray6999[1 + ((Class527) class527).anInt7012 * 1942118537]) * 664420687;
		class118.anInt1433 = (((Class527) class527).anIntArray6999[2 + 1942118537 * ((Class527) class527).anInt7012]) * 656180565;
		class118.anInt1434 = 265875707 * (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 3]);
		Class109.method1858(class118, (byte) 33);
	}

	static final void method6314(Class527 class527, short i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 985215637 * class118.anInt1320;
	}

	static final void method6315(Class527 class527, byte i) {
		Class184 class184 = Class468_Sub20.method12807(-1148731547);
		Class282_Sub23 class282_sub23 = Class271.method4828(OutgoingPacket.aClass379_4591, class184.aClass432_2283, 213013153);
		class282_sub23.buffer.writeByte(0);
		int i_1_ = -1990677291 * class282_sub23.buffer.index;
		class282_sub23.buffer.writeByte(0);
		class282_sub23.buffer.writeShort(-624100047 * ((Class527) class527).aClass346_7009.anInt4048, 1417031095);
		((Class527) class527).aClass346_7009.aClass282_Sub50_Sub9_4047.method14896(class282_sub23.buffer, ((Class527) class527).aClass346_7009.anIntArray4046, -1259900340);
		class282_sub23.buffer.method13061((class282_sub23.buffer.index * -1990677291 - i_1_), 1119414501);
		class184.method3049(class282_sub23, -95892997);
	}

	static final void method6316(Class527 class527, int i) {
		int i_2_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = -1903324139 * client.aClass330Array7428[i_2_].anInt3863;
	}

	static final void method6317(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = (Class169.method2875(client.anInt7166 * -1741204137, -2059083211) ? 1 : 0);
	}

	static final void method6318(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class96_Sub18.method14664(class118, class98, true, 2, class527, (byte) -20);
	}
}
