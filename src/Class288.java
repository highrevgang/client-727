
/* Class288 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Date;

public class Class288 {
	Class317 aClass317_3436;
	Class229 aClass229_3437 = new Class229(16);
	static Class414 aClass414_3438;

	public void method5067() {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3859(-407856533);
		}
	}

	public void method5068(int i, byte i_0_) {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3858(i, (byte) -51);
		}
	}

	public void method5069() {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3859(-1832638503);
		}
	}

	public void method5070(int i) {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3859(1722121730);
		}
	}

	public Class288(Class486 class486, Class495 class495, Class317 class317) {
		((Class288) this).aClass317_3436 = class317;
		((Class288) this).aClass317_3436.method5624((-71319279 * (Class120.aClass120_1469.anInt1521)), -1821241871);
	}

	public void method5071(int i) {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3863(2056846713);
		}
	}

	Class207 method5072(int i, int i_1_) {
		Class207 class207;
		synchronized (((Class288) this).aClass229_3437) {
			class207 = ((Class207) ((Class288) this).aClass229_3437.method3865((long) i));
		}
		if (class207 != null)
			return class207;
		byte[] is;
		synchronized (((Class288) this).aClass317_3436) {
			is = (((Class288) this).aClass317_3436.method5607(-71319279 * Class120.aClass120_1469.anInt1521, i, -1168931806));
		}
		class207 = new Class207();
		if (is != null)
			class207.method3549(new RsByteBuffer(is), -2145124357);
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3856(class207, (long) i);
		}
		return class207;
	}

	Class207 method5073(int i) {
		Class207 class207;
		synchronized (((Class288) this).aClass229_3437) {
			class207 = ((Class207) ((Class288) this).aClass229_3437.method3865((long) i));
		}
		if (class207 != null)
			return class207;
		byte[] is;
		synchronized (((Class288) this).aClass317_3436) {
			is = (((Class288) this).aClass317_3436.method5607(-71319279 * Class120.aClass120_1469.anInt1521, i, -1884209588));
		}
		class207 = new Class207();
		if (is != null)
			class207.method3549(new RsByteBuffer(is), -2107395661);
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3856(class207, (long) i);
		}
		return class207;
	}

	Class207 method5074(int i) {
		Class207 class207;
		synchronized (((Class288) this).aClass229_3437) {
			class207 = ((Class207) ((Class288) this).aClass229_3437.method3865((long) i));
		}
		if (class207 != null)
			return class207;
		byte[] is;
		synchronized (((Class288) this).aClass317_3436) {
			is = (((Class288) this).aClass317_3436.method5607(-71319279 * Class120.aClass120_1469.anInt1521, i, -1684955859));
		}
		class207 = new Class207();
		if (is != null)
			class207.method3549(new RsByteBuffer(is), -2034753155);
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3856(class207, (long) i);
		}
		return class207;
	}

	Class207 method5075(int i) {
		Class207 class207;
		synchronized (((Class288) this).aClass229_3437) {
			class207 = ((Class207) ((Class288) this).aClass229_3437.method3865((long) i));
		}
		if (class207 != null)
			return class207;
		byte[] is;
		synchronized (((Class288) this).aClass317_3436) {
			is = (((Class288) this).aClass317_3436.method5607(-71319279 * Class120.aClass120_1469.anInt1521, i, -1465415322));
		}
		class207 = new Class207();
		if (is != null)
			class207.method3549(new RsByteBuffer(is), -2089272706);
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3856(class207, (long) i);
		}
		return class207;
	}

	Class207 method5076(int i) {
		Class207 class207;
		synchronized (((Class288) this).aClass229_3437) {
			class207 = ((Class207) ((Class288) this).aClass229_3437.method3865((long) i));
		}
		if (class207 != null)
			return class207;
		byte[] is;
		synchronized (((Class288) this).aClass317_3436) {
			is = (((Class288) this).aClass317_3436.method5607(-71319279 * Class120.aClass120_1469.anInt1521, i, -1597785812));
		}
		class207 = new Class207();
		if (is != null)
			class207.method3549(new RsByteBuffer(is), -2117988400);
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3856(class207, (long) i);
		}
		return class207;
	}

	public Class247 method5077(int i, int i_2_, int i_3_, int i_4_, Class45 class45, int i_5_) {
		Class334[] class334s = null;
		Class207 class207 = method5072(i, -2042609292);
		if (null != ((Class207) class207).anIntArray2655) {
			class334s = new Class334[((Class207) class207).anIntArray2655.length];
			for (int i_6_ = 0; i_6_ < class334s.length; i_6_++) {
				Class38 class38 = class45.method914((((Class207) class207).anIntArray2655[i_6_]), -1040824349);
				class334s[i_6_] = new Class334(class38.anInt395 * -1157124829, -263107191 * class38.anInt402, -133387847 * class38.anInt401, class38.anInt397 * -552296001, class38.anInt399 * 1830044949, -154069655 * class38.anInt396, 134772441 * class38.anInt404, class38.aBool400, -810822993 * class38.anInt403, class38.anInt398 * -140856677, class38.anInt405 * -250014037);
			}
		}
		return new Class247(-1089853695 * ((Class207) class207).anInt2653, class334s, ((Class207) class207).anInt2654 * -1639521939, i_2_, i_3_, i_4_, ((Class207) class207).aClass204_2656, 1502700951 * ((Class207) class207).anInt2657);
	}

	public void method5078(int i) {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3858(i, (byte) 27);
		}
	}

	public void method5079() {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3863(749667589);
		}
	}

	public void method5080() {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3863(775505015);
		}
	}

	public void method5081() {
		synchronized (((Class288) this).aClass229_3437) {
			((Class288) this).aClass229_3437.method3863(1652102955);
		}
	}

	static final void method5082(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_7_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_8_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]);
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class282_Sub14.method12221(((Class513) class513).aClass118_5886, i_7_, i_8_, 1550850781);
	}

	static void method5083(long l) {
		Class407.aCalendar4846.setTime(new Date(l));
	}

	public static Class98 method5084(int i, int[] is, Class98 class98, boolean bool, byte i_9_) {
		if (!Class388.aClass317_4721.method5647(i, -2119577317))
			return null;
		int i_10_ = Class388.aClass317_4721.method5624(i, 1474003408);
		Class118[] class118s;
		if (i_10_ == 0)
			class118s = new Class118[0];
		else if (class98 == null)
			class118s = new Class118[i_10_];
		else
			class118s = class98.aClass118Array998;
		if (null == class98)
			class98 = new Class98(bool, class118s);
		else {
			class98.aClass118Array998 = class118s;
			class98.aBool999 = bool;
		}
		for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
			if (class98.aClass118Array998[i_11_] == null) {
				byte[] is_12_ = Class388.aClass317_4721.method5643(i, i_11_, is, -440613598);
				if (null != is_12_) {
					Class118 class118 = class98.aClass118Array998[i_11_] = new Class118();
					class118.anInt1287 = ((i << 16) + i_11_) * -1255176211;
					class118.method1984(new RsByteBuffer(is_12_), 1943098120);
				}
			}
		}
		return class98;
	}
}
