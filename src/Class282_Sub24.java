/* Class282_Sub24 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub24 extends Class282 {
	int anInt7684;
	int anInt7685;
	float aFloat7686;
	public Class385 aClass385_7687 = new Class385();

	public final int method12367() {
		return (int) aClass385_7687.aFloat4672;
	}

	public final int method12368(byte i) {
		return (int) aClass385_7687.aFloat4671;
	}

	public final int method12369(int i) {
		return (int) aClass385_7687.aFloat4672;
	}

	Class282_Sub24(int i, int i_0_, int i_1_, int i_2_, int i_3_, float f) {
		aClass385_7687.method6626((float) i, (float) i_0_, (float) i_1_);
		((Class282_Sub24) this).anInt7684 = -219119419 * i_2_;
		((Class282_Sub24) this).anInt7685 = i_3_ * 1876068859;
		((Class282_Sub24) this).aFloat7686 = f;
	}

	public final int method12370(int i) {
		return ((Class282_Sub24) this).anInt7684 * 1984888333;
	}

	public final int method12371(int i) {
		return ((Class282_Sub24) this).anInt7685 * -709945037;
	}

	public void method12372(float f) {
		((Class282_Sub24) this).aFloat7686 = f;
	}

	public void method12373(float f, int i) {
		((Class282_Sub24) this).aFloat7686 = f;
	}

	public void method12374(int i, int i_4_, int i_5_, int i_6_) {
		aClass385_7687.method6626((float) i, (float) i_4_, (float) i_5_);
	}

	public final int method12375() {
		return (int) aClass385_7687.aFloat4671;
	}

	public final int method12376() {
		return (int) aClass385_7687.aFloat4671;
	}

	public final int method12377() {
		return (int) aClass385_7687.aFloat4672;
	}

	public final int method12378() {
		return ((Class282_Sub24) this).anInt7685 * -709945037;
	}

	public final int method12379() {
		return (int) aClass385_7687.aFloat4672;
	}

	public final int method12380() {
		return (int) aClass385_7687.aFloat4672;
	}

	public final int method12381() {
		return (int) aClass385_7687.aFloat4672;
	}

	public final int method12382() {
		return (int) aClass385_7687.aFloat4673;
	}

	public final int method12383() {
		return (int) aClass385_7687.aFloat4673;
	}

	public final int method12384() {
		return (int) aClass385_7687.aFloat4673;
	}

	public final int method12385() {
		return ((Class282_Sub24) this).anInt7684 * 1984888333;
	}

	public final int method12386() {
		return ((Class282_Sub24) this).anInt7684 * 1984888333;
	}

	public final int method12387() {
		return ((Class282_Sub24) this).anInt7685 * -709945037;
	}

	public final int method12388() {
		return ((Class282_Sub24) this).anInt7685 * -709945037;
	}

	public final float method12389() {
		return ((Class282_Sub24) this).aFloat7686;
	}

	public final float method12390() {
		return ((Class282_Sub24) this).aFloat7686;
	}

	public final float method12391() {
		return ((Class282_Sub24) this).aFloat7686;
	}

	public final int method12392() {
		return ((Class282_Sub24) this).anInt7685 * -709945037;
	}

	public final float method12393() {
		return ((Class282_Sub24) this).aFloat7686;
	}

	public final int method12394(int i) {
		return (int) aClass385_7687.aFloat4673;
	}

	public final float method12395(int i) {
		return ((Class282_Sub24) this).aFloat7686;
	}

	public void method12396(float f) {
		((Class282_Sub24) this).aFloat7686 = f;
	}

	public void method12397(float f) {
		((Class282_Sub24) this).aFloat7686 = f;
	}

	public void method12398(int i, int i_7_, int i_8_) {
		aClass385_7687.method6626((float) i, (float) i_7_, (float) i_8_);
	}

	public static void method12399(int i, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_) {
		if (i < Class532_Sub2_Sub1.anInt7071 * -612590951 || i > -1345107225 * Class532_Sub2_Sub1.anInt7069 || i_10_ < Class532_Sub2_Sub1.anInt7071 * -612590951 || i_10_ > -1345107225 * Class532_Sub2_Sub1.anInt7069 || i_12_ < -612590951 * Class532_Sub2_Sub1.anInt7071 || i_12_ > -1345107225 * Class532_Sub2_Sub1.anInt7069 || i_14_ < -612590951 * Class532_Sub2_Sub1.anInt7071 || i_14_ > -1345107225 * Class532_Sub2_Sub1.anInt7069 || i_9_ < Class532_Sub2_Sub1.anInt7070 * 324226563 || i_9_ > Class532_Sub2_Sub1.anInt7068 * -348932735 || i_11_ < Class532_Sub2_Sub1.anInt7070 * 324226563 || i_11_ > -348932735 * Class532_Sub2_Sub1.anInt7068 || i_13_ < Class532_Sub2_Sub1.anInt7070 * 324226563 || i_13_ > -348932735 * Class532_Sub2_Sub1.anInt7068 || i_15_ < 324226563 * Class532_Sub2_Sub1.anInt7070 || i_15_ > Class532_Sub2_Sub1.anInt7068 * -348932735)
			Class263.method4779(i, i_9_, i_10_, i_11_, i_12_, i_13_, i_14_, i_15_, i_16_, (byte) 16);
		else
			Class282_Sub4.method12117(i, i_9_, i_10_, i_11_, i_12_, i_13_, i_14_, i_15_, i_16_, -907179464);
	}
}
