/* Class282_Sub17_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub17_Sub1 extends Class282_Sub17 {
	Class348 this$0;
	long aLong9930;
	String aString9931;

	void method12257(RsByteBuffer class282_sub35) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub1) this).aLong9930 = (class282_sub35.method13087(1688907962) * -7156069632047741567L);
		}
		((Class282_Sub17_Sub1) this).aString9931 = class282_sub35.method13090(-56317092);
	}

	void method12250(RsByteBuffer class282_sub35, int i) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub1) this).aLong9930 = (class282_sub35.method13087(1312780753) * -7156069632047741567L);
		}
		((Class282_Sub17_Sub1) this).aString9931 = class282_sub35.method13090(-141336185);
	}

	void method12251(Class61 class61, int i) {
		class61.method1216((-6384031897606374783L * ((Class282_Sub17_Sub1) this).aLong9930), ((Class282_Sub17_Sub1) this).aString9931, 0, (byte) 46);
	}

	void method12249(RsByteBuffer class282_sub35) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub1) this).aLong9930 = (class282_sub35.method13087(2019975858) * -7156069632047741567L);
		}
		((Class282_Sub17_Sub1) this).aString9931 = class282_sub35.method13090(938582751);
	}

	void method12258(Class61 class61) {
		class61.method1216((-6384031897606374783L * ((Class282_Sub17_Sub1) this).aLong9930), ((Class282_Sub17_Sub1) this).aString9931, 0, (byte) -53);
	}

	void method12253(RsByteBuffer class282_sub35) {
		if (class282_sub35.readUnsignedByte() != 255) {
			class282_sub35.index -= -1115476867;
			((Class282_Sub17_Sub1) this).aLong9930 = (class282_sub35.method13087(1143481691) * -7156069632047741567L);
		}
		((Class282_Sub17_Sub1) this).aString9931 = class282_sub35.method13090(415831589);
	}

	void method12254(Class61 class61) {
		class61.method1216((-6384031897606374783L * ((Class282_Sub17_Sub1) this).aLong9930), ((Class282_Sub17_Sub1) this).aString9931, 0, (byte) -33);
	}

	void method12255(Class61 class61) {
		class61.method1216((-6384031897606374783L * ((Class282_Sub17_Sub1) this).aLong9930), ((Class282_Sub17_Sub1) this).aString9931, 0, (byte) 14);
	}

	void method12256(Class61 class61) {
		class61.method1216((-6384031897606374783L * ((Class282_Sub17_Sub1) this).aLong9930), ((Class282_Sub17_Sub1) this).aString9931, 0, (byte) 81);
	}

	void method12252(Class61 class61) {
		class61.method1216((-6384031897606374783L * ((Class282_Sub17_Sub1) this).aLong9930), ((Class282_Sub17_Sub1) this).aString9931, 0, (byte) -93);
	}

	Class282_Sub17_Sub1(Class348 class348) {
		((Class282_Sub17_Sub1) this).this$0 = class348;
		((Class282_Sub17_Sub1) this).aLong9930 = 7156069632047741567L;
		((Class282_Sub17_Sub1) this).aString9931 = null;
	}

	public static void method15402(int i, String string, boolean bool, int i_0_) {
		if (0 == i) {
			Class316.aClass505_3680 = Class320.method5732(0, Class351.aCanvas4096, Class321.anInterface22_3731, Class488.aClass317_5758, Class393.aClass282_Sub54_4783.aClass468_Sub4_8223.method12641(-1783373613) * 2, -1283280673);
			if (string != null) {
				Class316.aClass505_3680.ba(1, 0);
				Class414 class414 = Class94.method1588(Class410.aClass317_4924, Class16.anInt136 * 436671641, 0, 1150997063);
				Class8 class8 = (Class316.aClass505_3680.method8448(class414, Class91.method1514(Class211.aClass317_2673, Class16.anInt136 * 436671641, 0), true));
				Class275.method4891((byte) -128);
				Class446.method7447(string, true, Class316.aClass505_3680, class8, class414, (byte) -87);
			}
		} else {
			Class505 class505 = null;
			if (string != null) {
				class505 = Class320.method5732(0, Class351.aCanvas4096, Class321.anInterface22_3731, Class488.aClass317_5758, 0, -603391790);
				class505.ba(1, 0);
				Class414 class414 = Class94.method1588(Class410.aClass317_4924, Class16.anInt136 * 436671641, 0, 1961117540);
				Class8 class8 = (class505.method8448(class414, Class91.method1514(Class211.aClass317_2673, 436671641 * Class16.anInt136, 0), true));
				Class275.method4891((byte) -74);
				Class446.method7447(string, true, class505, class8, class414, (byte) -82);
			}
			try {
				try {
					Class316.aClass505_3680 = Class320.method5732(i, Class351.aCanvas4096, Class321.anInterface22_3731, Class488.aClass317_5758, Class393.aClass282_Sub54_4783.aClass468_Sub4_8223.method12641(169664234) * 2, -694595538);
					if (string != null) {
						class505.ba(1, 0);
						Class414 class414 = Class94.method1588(Class410.aClass317_4924, 436671641 * Class16.anInt136, 0, 1716473063);
						Class8 class8 = (class505.method8448(class414, Class91.method1514(Class211.aClass317_2673, (436671641 * Class16.anInt136), 0), true));
						Class275.method4891((byte) -30);
						Class446.method7447(string, true, class505, class8, class414, (byte) -16);
					}
					if (Class316.aClass505_3680.method8399()) {
						boolean bool_1_ = true;
						try {
							bool_1_ = (Class11.aClass282_Sub51_124.anInt8167 * -79546877) > 256;
						} catch (Throwable throwable) {
							/* empty */
						}
						Class282_Sub1 class282_sub1;
						if (bool_1_)
							class282_sub1 = Class316.aClass505_3680.method8438(146800640);
						else
							class282_sub1 = Class316.aClass505_3680.method8438(104857600);
						Class316.aClass505_3680.method8439(class282_sub1);
					}
				} catch (Throwable throwable) {
					int i_2_ = Class393.aClass282_Sub54_4783.aClass468_Sub18_8230.method12776(-1734953723);
					if (i_2_ == 2)
						client.aBool7171 = true;
					Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub18_8230, 0, -1949394777);
					method15402(i_2_, string, bool, 1368243224);
					if (null != class505) {
						try {
							class505.method8396(978390252);
						} catch (Throwable throwable_3_) {
							/* empty */
						}
					}
					return;
				}
				if (null != class505) {
					try {
						class505.method8396(1088813931);
					} catch (Throwable throwable) {
						/* empty */
					}
				}
			} catch (Exception object) {
				if (null != class505) {
					try {
						class505.method8396(-2045375529);
					} catch (Throwable throwable) {
						/* empty */
					}
				}
				throw object;
			}
		}
		if (bool)
			Class393.aClass282_Sub54_4783.method13505((Class393.aClass282_Sub54_4783.aClass468_Sub18_8230), !bool, 2032339457);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub18_8230), i, -952272736);
		if (!bool)
			Class393.aClass282_Sub54_4783.method13505((Class393.aClass282_Sub54_4783.aClass468_Sub18_8230), !bool, -1976221425);
		Class12.method482((byte) 53);
		Class316.aClass505_3680.method8459(10000);
		Class316.aClass505_3680.J(32);
		client.aClass257_7353.method4447(1353866416);
		Class316.aClass505_3680.method8568(false);
		if (Class316.aClass505_3680.method8403())
			Class115.method1952(Class393.aClass282_Sub54_4783.aClass468_Sub12_8195.method12706((byte) 97) == 1, -1437511825);
		client.aClass257_7353.method4446((byte) 78);
		client.aClass257_7353.method4435((byte) 1).method4048(1352749078);
		client.aBool7175 = false;
		client.aBool7185 = true;
		Class149_Sub3.aClass467Array9380 = null;
		Class316.aClass505_3680.GA(0.0F, 1.0F);
	}

	static final void method15403(Class527 class527, int i) {
		Class424.method7081((byte) -41);
	}
}
