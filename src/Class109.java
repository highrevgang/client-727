/* Class109 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class109 {
	Class109() {
		/* empty */
	}

	abstract void method1849(byte i);

	abstract void method1850();

	abstract void method1851();

	static final void method1852(Class118 class118, Class98 class98, Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		class118.anInt1311 = 1982656085 * (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		if (class118.anInt1311 * 276864765 > (-354780671 * class118.anInt1376 - 1506818197 * class118.anInt1301))
			class118.anInt1311 = (class118.anInt1376 * -2044497835 - -833007751 * class118.anInt1301);
		if (class118.anInt1311 * 276864765 < 0)
			class118.anInt1311 = 0;
		class118.anInt1312 = 771324207 * (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]);
		if (class118.anInt1312 * 682782159 > class118.anInt1314 * -37350919 - -492594917 * class118.anInt1429)
			class118.anInt1312 = (1980562871 * class118.anInt1314 - class118.anInt1429 * -1716920587);
		if (682782159 * class118.anInt1312 < 0)
			class118.anInt1312 = 0;
		method1858(class118, (byte) 19);
		if (class118.anInt1288 * 1924549737 == -1 && !class98.aBool999)
			Class468_Sub12.method12709(-1952846363 * class118.anInt1287, (byte) 67);
	}

	static final void method1853(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 425673003;
		int i_0_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		int i_1_ = (((Class527) class527).anIntArray6999[1 + 1942118537 * ((Class527) class527).anInt7012]);
		int i_2_ = (((Class527) class527).anIntArray6999[2 + 1942118537 * ((Class527) class527).anInt7012]);
		if (i_1_ == -1)
			throw new RuntimeException();
		Class431 class431 = Class466.aClass444_5570.method7424(i_1_, (byte) 8);
		if (i_0_ != class431.aChar5140)
			throw new RuntimeException();
		int[] is = class431.method7251(Integer.valueOf(i_2_), 2106886320);
		int i_3_ = 0;
		if (null != is)
			i_3_ = is.length;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_3_;
	}

	static final void method1854(Class527 class527, int i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		int i_4_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class414 class414 = Class94.method1588(Class410.aClass317_4924, i_4_, 0, 1361822536);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = class414.method6947(string, Class182.aClass160Array2261, -106589048);
	}

	static final void method1855(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 214125473 * Class440.anInt5357;
	}

	static final void method1856(Class527 class527, int i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		if (string != null)
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = string.length();
		else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 0;
	}

	static final void method1857(Class527 class527, int i) {
		Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub8_8219, ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) != 0) ? 1 : 0, -123995659);
		Class190.method3148((byte) 35);
	}

	public static void method1858(Class118 class118, byte i) {
		if (class118.anInt1450 * 1767918125 == -1048101161 * client.anInt7408)
			client.aBoolArray7443[-1338917705 * class118.anInt1449] = true;
	}

	static void method1859(Class317 class317, int i, int i_5_, int i_6_, boolean bool, long l, int i_7_, int i_8_) {
		Class148.anInt1730 = 800770715;
		Class75.aClass317_746 = class317;
		Class6.anInt46 = -26560111 * i;
		Class148.anInt1738 = 275076647 * i_5_;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
		Class158_Sub2_Sub3.anInt10243 = 965667275 * i_6_;
		Class152.aBool1962 = bool;
		Class96_Sub22.anInt9440 = 1610306160;
		Class148.aLong1740 = l * 5207426305341619623L;
		Class383.anInt4664 = 482849385 * i_7_;
	}
}
