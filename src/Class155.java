
/* Class155 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Date;

public final class Class155 {
	int anInt1968;
	Class465 aClass465_1969;

	public void method2623(int i, int i_0_) {
		if (((Class155) this).aClass465_1969 == null)
			((Class155) this).aClass465_1969 = new Class465(152314627 * ((Class155) this).anInt1968);
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class155) this).aClass465_1969.method7754((long) i));
		if (class282_sub38 != null)
			class282_sub38.anInt8002 = 1270866345 * i_0_;
		else {
			class282_sub38 = new Class282_Sub38(i_0_);
			((Class155) this).aClass465_1969.method7765(class282_sub38, (long) i);
		}
	}

	public void method2624(int i) {
		if (((Class155) this).aClass465_1969 != null)
			((Class155) this).aClass465_1969.method7749(-1100560301);
	}

	public void method2625(int i, int i_1_, byte i_2_) {
		if (((Class155) this).aClass465_1969 == null)
			((Class155) this).aClass465_1969 = new Class465(152314627 * ((Class155) this).anInt1968);
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class155) this).aClass465_1969.method7754((long) i));
		if (class282_sub38 != null)
			class282_sub38.anInt8002 = 1270866345 * i_1_;
		else {
			class282_sub38 = new Class282_Sub38(i_1_);
			((Class155) this).aClass465_1969.method7765(class282_sub38, (long) i);
		}
	}

	public int method2626(int i, byte i_3_) {
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class155) this).aClass465_1969.method7754((long) i));
		if (class282_sub38 != null)
			return class282_sub38.anInt8002 * -570797415;
		Class372 class372 = Class104.aClass377_1065.method6384(i, 1420964828);
		if (class372.aChar4328 != 'i')
			return -1;
		return 0;
	}

	public int method2627(int i, int i_4_) {
		Class226 class226 = Class16.aClass230_147.method3897(i, (byte) 48);
		int i_5_ = -1764754967 * class226.anInt2784;
		int i_6_ = (31 == -188179155 * class226.anInt2783 ? -1 : (1 << class226.anInt2783 * -188179155 + 1) - 1);
		return ((method2626(i_5_, (byte) 17) & i_6_) >>> class226.anInt2785 * -1174362995);
	}

	public void method2628() {
		if (((Class155) this).aClass465_1969 != null)
			((Class155) this).aClass465_1969.method7749(962446728);
	}

	public int method2629(int i) {
		Class226 class226 = Class16.aClass230_147.method3897(i, (byte) 59);
		int i_7_ = -1764754967 * class226.anInt2784;
		int i_8_ = (31 == -188179155 * class226.anInt2783 ? -1 : (1 << class226.anInt2783 * -188179155 + 1) - 1);
		return ((method2626(i_7_, (byte) 7) & i_8_) >>> class226.anInt2785 * -1174362995);
	}

	Class155(int i) {
		((Class155) this).anInt1968 = i * -760225877;
	}

	public void method2630(int i, int i_9_) {
		if (((Class155) this).aClass465_1969 == null)
			((Class155) this).aClass465_1969 = new Class465(152314627 * ((Class155) this).anInt1968);
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class155) this).aClass465_1969.method7754((long) i));
		if (class282_sub38 != null)
			class282_sub38.anInt8002 = 1270866345 * i_9_;
		else {
			class282_sub38 = new Class282_Sub38(i_9_);
			((Class155) this).aClass465_1969.method7765(class282_sub38, (long) i);
		}
	}

	public void method2631(int i, int i_10_) {
		if (((Class155) this).aClass465_1969 == null)
			((Class155) this).aClass465_1969 = new Class465(152314627 * ((Class155) this).anInt1968);
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class155) this).aClass465_1969.method7754((long) i));
		if (class282_sub38 != null)
			class282_sub38.anInt8002 = 1270866345 * i_10_;
		else {
			class282_sub38 = new Class282_Sub38(i_10_);
			((Class155) this).aClass465_1969.method7765(class282_sub38, (long) i);
		}
	}

	public void method2632(int i, int i_11_) {
		if (((Class155) this).aClass465_1969 == null)
			((Class155) this).aClass465_1969 = new Class465(152314627 * ((Class155) this).anInt1968);
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class155) this).aClass465_1969.method7754((long) i));
		if (class282_sub38 != null)
			class282_sub38.anInt8002 = 1270866345 * i_11_;
		else {
			class282_sub38 = new Class282_Sub38(i_11_);
			((Class155) this).aClass465_1969.method7765(class282_sub38, (long) i);
		}
	}

	public int method2633(int i) {
		Class226 class226 = Class16.aClass230_147.method3897(i, (byte) 90);
		int i_12_ = -1764754967 * class226.anInt2784;
		int i_13_ = (31 == -188179155 * class226.anInt2783 ? -1 : (1 << class226.anInt2783 * -188179155 + 1) - 1);
		return ((method2626(i_12_, (byte) 35) & i_13_) >>> class226.anInt2785 * -1174362995);
	}

	static void method2634(long l) {
		Class407.aCalendar4848.setTime(new Date(l));
	}

	public static void method2635(String string, String string_14_, int i) {
		if (string.length() <= 320 && Class388.method6693(1569311303)) {
			client.aClass184_7218.method3051((byte) -71);
			Class290.method5118((byte) -126);
			Class9.aString99 = string;
			Class9.aString102 = string_14_;
			Class365.method6298(14, 2000356060);
		}
	}

	static final void method2636(byte i) {
		int i_15_ = Class197.anInt2429 * -963499271;
		int[] is = Class197.anIntArray2433;
		for (int i_16_ = 0; i_16_ < i_15_; i_16_++) {
			Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1 = client.aClass521_Sub1_Sub1_Sub2_Sub1Array7314[is[i_16_]];
			if (null != class521_sub1_sub1_sub2_sub1)
				Class363.method6287(class521_sub1_sub1_sub2_sub1, false, 960427099);
		}
	}

	static final void method2637(int i, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, byte i_22_) {
		Class426.method7170(i_18_, 377314002);
		int i_23_ = 0;
		int i_24_ = i_18_ - i_21_;
		if (i_24_ < 0)
			i_24_ = 0;
		int i_25_ = i_18_;
		int i_26_ = -i_18_;
		int i_27_ = i_24_;
		int i_28_ = -i_24_;
		int i_29_ = -1;
		int i_30_ = -1;
		int[] is = Class532_Sub3.anIntArrayArray7072[i_17_];
		int i_31_ = i - i_24_;
		int i_32_ = i + i_24_;
		Class232.method3922(is, i - i_18_, i_31_, i_20_, (byte) 98);
		Class232.method3922(is, i_31_, i_32_, i_19_, (byte) 67);
		Class232.method3922(is, i_32_, i + i_18_, i_20_, (byte) -55);
		while (i_25_ > i_23_) {
			i_29_ += 2;
			i_30_ += 2;
			i_26_ += i_29_;
			i_28_ += i_30_;
			if (i_28_ >= 0 && i_27_ >= 1) {
				Class5.anIntArray36[i_27_] = i_23_;
				i_27_--;
				i_28_ -= i_27_ << 1;
			}
			i_23_++;
			if (i_26_ >= 0) {
				i_25_--;
				i_26_ -= i_25_ << 1;
				if (i_25_ >= i_24_) {
					int[] is_33_ = Class532_Sub3.anIntArrayArray7072[i_25_ + i_17_];
					int[] is_34_ = Class532_Sub3.anIntArrayArray7072[i_17_ - i_25_];
					int i_35_ = i + i_23_;
					int i_36_ = i - i_23_;
					Class232.method3922(is_33_, i_36_, i_35_, i_20_, (byte) -14);
					Class232.method3922(is_34_, i_36_, i_35_, i_20_, (byte) 93);
				} else {
					int[] is_37_ = Class532_Sub3.anIntArrayArray7072[i_17_ + i_25_];
					int[] is_38_ = Class532_Sub3.anIntArrayArray7072[i_17_ - i_25_];
					int i_39_ = Class5.anIntArray36[i_25_];
					int i_40_ = i + i_23_;
					int i_41_ = i - i_23_;
					int i_42_ = i + i_39_;
					int i_43_ = i - i_39_;
					Class232.method3922(is_37_, i_41_, i_43_, i_20_, (byte) -28);
					Class232.method3922(is_37_, i_43_, i_42_, i_19_, (byte) -29);
					Class232.method3922(is_37_, i_42_, i_40_, i_20_, (byte) -47);
					Class232.method3922(is_38_, i_41_, i_43_, i_20_, (byte) 5);
					Class232.method3922(is_38_, i_43_, i_42_, i_19_, (byte) -58);
					Class232.method3922(is_38_, i_42_, i_40_, i_20_, (byte) 9);
				}
			}
			int[] is_44_ = Class532_Sub3.anIntArrayArray7072[i_17_ + i_23_];
			int[] is_45_ = Class532_Sub3.anIntArrayArray7072[i_17_ - i_23_];
			int i_46_ = i_25_ + i;
			int i_47_ = i - i_25_;
			if (i_23_ < i_24_) {
				int i_48_ = i_27_ < i_23_ ? Class5.anIntArray36[i_23_] : i_27_;
				int i_49_ = i_48_ + i;
				int i_50_ = i - i_48_;
				Class232.method3922(is_44_, i_47_, i_50_, i_20_, (byte) -3);
				Class232.method3922(is_44_, i_50_, i_49_, i_19_, (byte) 79);
				Class232.method3922(is_44_, i_49_, i_46_, i_20_, (byte) -16);
				Class232.method3922(is_45_, i_47_, i_50_, i_20_, (byte) 40);
				Class232.method3922(is_45_, i_50_, i_49_, i_19_, (byte) -44);
				Class232.method3922(is_45_, i_49_, i_46_, i_20_, (byte) -57);
			} else {
				Class232.method3922(is_44_, i_47_, i_46_, i_20_, (byte) -47);
				Class232.method3922(is_45_, i_47_, i_46_, i_20_, (byte) 12);
			}
		}
	}

	static final void method2638(Class527 class527, byte i) {
		Class513 class513;
		if (((Class527) class527).aBool7022) {
			if (i <= 1)
				return;
			class513 = ((Class527) class527).aClass513_6994;
		} else
			class513 = ((Class527) class527).aClass513_7007;
		Class513 class513_51_ = class513;
		Class118 class118 = ((Class513) class513_51_).aClass118_5886;
		Class98 class98 = ((Class513) class513_51_).aClass98_5885;
		Class29.method786(class118, class98, class527, -1279446637);
	}

	static final void method2639(Class527 class527, short i) {
		int i_52_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2 = ((Class521_Sub1_Sub1_Sub2_Sub2) ((Class527) class527).aClass521_Sub1_Sub1_Sub2_7006);
		int i_53_ = class521_sub1_sub1_sub2_sub2.method16163(i_52_, -2060228859);
		int i_54_ = class521_sub1_sub1_sub2_sub2.method16169(i_52_, (byte) -49);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_53_;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_54_;
	}

	static Class521_Sub1_Sub1_Sub2 method2640(int i, int i_55_, int i_56_, int i_57_) {
		Class293 class293 = (client.aClass257_7353.method4430(-913455840).aClass293ArrayArrayArray2604[i][i_55_][i_56_]);
		if (class293 == null)
			return null;
		Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2 = null;
		int i_58_ = -1;
		for (Class208 class208 = class293.aClass208_3504; null != class208; class208 = class208.aClass208_2660) {
			Class521_Sub1_Sub1 class521_sub1_sub1 = class208.aClass521_Sub1_Sub1_2659;
			if (class521_sub1_sub1 instanceof Class521_Sub1_Sub1_Sub2) {
				Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2_59_ = (Class521_Sub1_Sub1_Sub2) class521_sub1_sub1;
				int i_60_ = (class521_sub1_sub1_sub2_59_.method15805(828768449) - 1) * 256 + 252;
				Class385 class385 = class521_sub1_sub1_sub2_59_.method11166().aClass385_3595;
				int i_61_ = (int) class385.aFloat4671 - i_60_ >> 9;
				int i_62_ = (int) class385.aFloat4673 - i_60_ >> 9;
				int i_63_ = (int) class385.aFloat4671 + i_60_ >> 9;
				int i_64_ = i_60_ + (int) class385.aFloat4673 >> 9;
				if (i_61_ <= i_55_ && i_62_ <= i_56_ && i_63_ >= i_55_ && i_64_ >= i_56_) {
					int i_65_ = (i_63_ + 1 - i_55_) * (i_64_ + 1 - i_56_);
					if (i_65_ > i_58_) {
						class521_sub1_sub1_sub2 = class521_sub1_sub1_sub2_59_;
						i_58_ = i_65_;
					}
				}
			}
		}
		return class521_sub1_sub1_sub2;
	}
}
