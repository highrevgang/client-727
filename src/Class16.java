/* Class16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class16 {
	static int anInt135;
	static int anInt136;
	static int anInt137;
	static Class160[] aClass160Array138;
	public static Class414 aClass414_139;
	static int anInt140;
	static int anInt141;
	static int anInt142;
	static int anInt143;
	public static Class8 aClass8_144;
	static Class160[] aClass160Array145;
	static Class160 aClass160_146;
	public static Class230 aClass230_147;

	static void method551() {
		Class540.aClass8_7138 = null;
		aClass8_144 = null;
		Class285.aClass8_3394 = null;
		Class282_Sub21.aClass160Array7673 = null;
		aClass160Array138 = null;
		Class391.aClass160Array4778 = null;
		Class282_Sub20_Sub15.aClass160Array9838 = null;
		aClass160Array145 = null;
		Class282_Sub36.aClass160Array7994 = null;
		Class250.aClass160Array3092 = null;
		Class182.aClass160Array2261 = null;
		aClass160_146 = null;
		Exception_Sub1.aClass160_10075 = null;
		Class245.aClass160Array3027 = null;
	}

	public static void method552(Class317 class317) {
		anInt140 = class317.method5610("p11_full", 1424101193) * 1669464149;
		anInt136 = class317.method5610("p12_full", -1601885459) * 1957167017;
		Class395.anInt4788 = class317.method5610("b12_full", 977480910) * 2033224767;
	}

	public static void method553(Class317 class317) {
		anInt140 = class317.method5610("p11_full", -3590070) * 1669464149;
		anInt136 = class317.method5610("p12_full", -672134768) * 1957167017;
		Class395.anInt4788 = class317.method5610("b12_full", 1362144243) * 2033224767;
	}

	public static void method554(Class317 class317) {
		Class165.anInt2035 = class317.method5610("headicons_pk", -256479951) * -821889283;
		Class475.anInt5622 = class317.method5610("headicons_prayer", -4006419) * -2055869675;
		anInt135 = class317.method5610("hint_headicons", -1105434129) * -1041729095;
		anInt142 = class317.method5610("hint_mapmarkers", -1281941584) * 850992921;
		anInt137 = class317.method5610("mapflag", -884587844) * 1055165697;
		Class468_Sub12.anInt7899 = class317.method5610("cross", 1623210615) * 395125199;
		Class13.anInt130 = class317.method5610("mapdots", -1181828372) * 453785251;
		anInt143 = class317.method5610("name_icons", -748362572) * -715854365;
		Class400.anInt4821 = class317.method5610("floorshadows", 150016034) * 237532063;
		anInt141 = class317.method5610("compass", -1908315760) * 1354470119;
		Class165.anInt2037 = class317.method5610("otherlevel", 334034203) * -1341930679;
		Class271.anInt3330 = class317.method5610("hint_mapedge", -1117715217) * 1834682789;
	}

	public static void method555(Class317 class317) {
		Class165.anInt2035 = class317.method5610("headicons_pk", -446486826) * -821889283;
		Class475.anInt5622 = class317.method5610("headicons_prayer", -1005116812) * -2055869675;
		anInt135 = class317.method5610("hint_headicons", -275215677) * -1041729095;
		anInt142 = class317.method5610("hint_mapmarkers", -1074565286) * 850992921;
		anInt137 = class317.method5610("mapflag", 1353344209) * 1055165697;
		Class468_Sub12.anInt7899 = class317.method5610("cross", 60241021) * 395125199;
		Class13.anInt130 = class317.method5610("mapdots", 723928588) * 453785251;
		anInt143 = class317.method5610("name_icons", -2032050644) * -715854365;
		Class400.anInt4821 = class317.method5610("floorshadows", -1312465905) * 237532063;
		anInt141 = class317.method5610("compass", -1239782843) * 1354470119;
		Class165.anInt2037 = class317.method5610("otherlevel", -841677680) * -1341930679;
		Class271.anInt3330 = class317.method5610("hint_mapedge", 552476645) * 1834682789;
	}

	public static int[] method556() {
		return new int[] { anInt140 * 1500183805, 436671641 * anInt136, -625646657 * Class395.anInt4788 };
	}

	public static int[] method557() {
		return new int[] { anInt140 * 1500183805, 436671641 * anInt136, -625646657 * Class395.anInt4788 };
	}

	Class16() throws Throwable {
		throw new Error();
	}

	public static int method558() {
		return 12;
	}

	public static void method559(Class505 class505) {
		Class540.aClass8_7138 = ((Class8) Class487.aClass378_5752.method6426(client.anInterface35_7206, anInt140 * 1500183805, true, true, -2065156362));
		Class282_Sub17_Sub2.aClass414_9933 = Class487.aClass378_5752.method6415(client.anInterface35_7206, 1500183805 * anInt140, 138214263);
		aClass8_144 = ((Class8) Class487.aClass378_5752.method6426(client.anInterface35_7206, 436671641 * anInt136, true, true, -2080739477));
		aClass414_139 = Class487.aClass378_5752.method6415(client.anInterface35_7206, 436671641 * anInt136, 930529751);
		Class285.aClass8_3394 = ((Class8) Class487.aClass378_5752.method6426(client.anInterface35_7206, (Class395.anInt4788 * -625646657), true, true, -2130229994));
		Class288.aClass414_3438 = Class487.aClass378_5752.method6415(client.anInterface35_7206, (Class395.anInt4788 * -625646657), -271839107);
	}

	static void method560() {
		Class540.aClass8_7138 = null;
		aClass8_144 = null;
		Class285.aClass8_3394 = null;
		Class282_Sub21.aClass160Array7673 = null;
		aClass160Array138 = null;
		Class391.aClass160Array4778 = null;
		Class282_Sub20_Sub15.aClass160Array9838 = null;
		aClass160Array145 = null;
		Class282_Sub36.aClass160Array7994 = null;
		Class250.aClass160Array3092 = null;
		Class182.aClass160Array2261 = null;
		aClass160_146 = null;
		Exception_Sub1.aClass160_10075 = null;
		Class245.aClass160Array3027 = null;
	}

	public static void method561(Class505 class505, Class317 class317) {
		Class91[] class91s = Class91.method1514(class317, Class165.anInt2035 * -1633200555, 0);
		Class282_Sub21.aClass160Array7673 = new Class160[class91s.length];
		for (int i = 0; i < class91s.length; i++)
			Class282_Sub21.aClass160Array7673[i] = class505.method8444(class91s[i], true);
		class91s = Class91.method1514(class317, -1931867075 * Class475.anInt5622, 0);
		aClass160Array138 = new Class160[class91s.length];
		for (int i = 0; i < class91s.length; i++)
			aClass160Array138[i] = class505.method8444(class91s[i], true);
		class91s = Class91.method1514(class317, anInt135 * -1739866999, 0);
		Class391.aClass160Array4778 = new Class160[class91s.length];
		for (int i = 0; i < class91s.length; i++)
			Class391.aClass160Array4778[i] = class505.method8444(class91s[i], true);
		class91s = Class91.method1514(class317, anInt142 * 460049705, 0);
		Class282_Sub20_Sub15.aClass160Array9838 = new Class160[class91s.length];
		for (int i = 0; i < class91s.length; i++)
			Class282_Sub20_Sub15.aClass160Array9838[i] = class505.method8444(class91s[i], true);
		class91s = Class91.method1514(class317, anInt137 * 566522625, 0);
		aClass160Array145 = new Class160[class91s.length];
		int i = 25;
		for (int i_0_ = 0; i_0_ < class91s.length; i_0_++) {
			class91s[i_0_].method1529(-i + (int) (Math.random() * (double) i * 2.0), -i + (int) (Math.random() * (double) i * 2.0), -i + (int) (Math.random() * (double) i * 2.0));
			aClass160Array145[i_0_] = class505.method8444(class91s[i_0_], true);
		}
		class91s = Class91.method1514(class317, 1306936623 * Class468_Sub12.anInt7899, 0);
		Class282_Sub36.aClass160Array7994 = new Class160[class91s.length];
		for (int i_1_ = 0; i_1_ < class91s.length; i_1_++)
			Class282_Sub36.aClass160Array7994[i_1_] = class505.method8444(class91s[i_1_], true);
		class91s = Class91.method1514(class317, -862638837 * Class13.anInt130, 0);
		Class250.aClass160Array3092 = new Class160[class91s.length];
		i = 12;
		for (int i_2_ = 0; i_2_ < class91s.length; i_2_++) {
			class91s[i_2_].method1529(-i + (int) (Math.random() * (double) i * 2.0), -i + (int) (Math.random() * (double) i * 2.0), -i + (int) (Math.random() * (double) i * 2.0));
			Class250.aClass160Array3092[i_2_] = class505.method8444(class91s[i_2_], true);
		}
		class91s = Class91.method1514(class317, anInt143 * 1595523019, 0);
		Class182.aClass160Array2261 = new Class160[class91s.length];
		i = 12;
		for (int i_3_ = 0; i_3_ < class91s.length; i_3_++) {
			class91s[i_3_].method1529(-i + (int) (Math.random() * (double) i * 2.0), -i + (int) (Math.random() * (double) i * 2.0), -i + (int) (Math.random() * (double) i * 2.0));
			Class182.aClass160Array2261[i_3_] = class505.method8444(class91s[i_3_], true);
		}
		aClass160_146 = class505.method8444(Class91.method1522(class317, -1636343593 * anInt141, 0), true);
		Exception_Sub1.aClass160_10075 = class505.method8444(Class91.method1522(class317, (-615473415 * Class165.anInt2037), 0), true);
		class91s = Class91.method1514(class317, 341095981 * Class271.anInt3330, 0);
		Class245.aClass160Array3027 = new Class160[class91s.length];
		for (int i_4_ = 0; i_4_ < class91s.length; i_4_++)
			Class245.aClass160Array3027[i_4_] = class505.method8444(class91s[i_4_], true);
	}

	static Class96 method562(RsByteBuffer class282_sub35, int i) {
		int i_5_ = class282_sub35.readUnsignedByte();
		Class411 class411 = Class346.method6156(i_5_, 2101068179);
		Class96 class96 = null;
		switch (class411.anInt4956 * -385427245) {
		case 13:
			class96 = new Class96_Sub3(class282_sub35);
			break;
		case 25:
			class96 = new Class96_Sub10_Sub1(class282_sub35);
			break;
		case 15:
			class96 = new Class96_Sub19(class282_sub35, 0, 0);
			break;
		case 5:
			class96 = new Class96_Sub21(class282_sub35);
			break;
		case 11:
			class96 = new Class96_Sub11(class282_sub35);
			break;
		case 14:
			class96 = new Class96_Sub10_Sub2(class282_sub35);
			break;
		case 16:
			class96 = new Class96_Sub1(class282_sub35);
			break;
		case 29:
			class96 = new Class96_Sub2(class282_sub35);
			break;
		default:
			break;
		case 2:
			class96 = new Class96_Sub14(class282_sub35);
			break;
		case 8:
			class96 = new Class96_Sub5(class282_sub35);
			break;
		case 19:
			class96 = new Class96_Sub19(class282_sub35, 1, 0);
			break;
		case 28:
			class96 = new Class96_Sub19(class282_sub35, 1, 1);
			break;
		case 20:
			class96 = new Class96_Sub6(class282_sub35);
			break;
		case 24:
			class96 = new Class96_Sub19(class282_sub35, 0, 1);
			break;
		case 9:
			class96 = new Class96_Sub22(class282_sub35);
			break;
		case 4:
			class96 = new Class96_Sub4(class282_sub35);
			break;
		case 26:
			class96 = new Class96_Sub18(class282_sub35);
			break;
		case 22:
			class96 = new Class96_Sub12(class282_sub35);
			break;
		case 23:
			class96 = new Class96_Sub13(class282_sub35);
			break;
		case 0:
			class96 = new Class96_Sub15(class282_sub35);
			break;
		case 27:
			class96 = new Class96_Sub16(class282_sub35, true);
			break;
		case 10:
			class96 = new Class96_Sub16(class282_sub35, false);
			break;
		case 17:
			class96 = new Class96_Sub17(class282_sub35);
			break;
		case 6:
			class96 = new Class96_Sub8(class282_sub35);
			break;
		case 12:
			class96 = new Class96_Sub7(class282_sub35);
			break;
		case 30:
			class96 = new Class96_Sub23(class282_sub35);
			break;
		case 18:
			class96 = new Class96_Sub9(class282_sub35);
			break;
		case 1:
			class96 = new Class96_Sub20(class282_sub35);
		}
		return class96;
	}

	static final void method563(Class527 class527, int i) {
		int i_6_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_6_, (byte) 11);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_6_ >> 16];
		RsByteBuffer.method13292(class118, class98, class527, (byte) 60);
	}

	static final void method564(Class527 class527, byte i) {
		int i_7_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_7_, (byte) 53);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_7_ >> 16];
		Class262.method4651(class118, class98, class527, (byte) 59);
	}

	static final void method565(Class527 class527, int i) {
		int i_8_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class108 class108 = Class180.method3032(i_8_, (byte) -1);
		String string = "";
		if (null != class108 && null != class108.aString1088)
			string = class108.aString1088;
		((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1] = string;
	}

	public static void method566(int i) {
		Class260.aClass268Array3232 = new Class268[50];
		Class260.anInt3219 = 0;
	}

	public static final void method567(String string, boolean bool, byte i) {
		if (string != null) {
			if (client.anInt7373 * -1754449153 >= 100)
				Class387.method6681(4, (Class433.aClass433_5289.method7273(Class223.aClass495_2772, -927458536)), (byte) 0);
			else {
				String string_9_ = Class383.method6515(string, 1942118537);
				if (null != string_9_) {
					for (int i_10_ = 0; i_10_ < client.anInt7373 * -1754449153; i_10_++) {
						Class10 class10 = client.aClass10Array7456[i_10_];
						String string_11_ = Class383.method6515(class10.aString115, 1942118537);
						if (string_11_ != null && string_11_.equals(string_9_)) {
							Class387.method6681(4, new StringBuilder().append(string).append(Class433.aClass433_5302.method7273(Class223.aClass495_2772, -794034411)).toString(), (byte) 6);
							return;
						}
						if (class10.aString116 != null) {
							String string_12_ = Class383.method6515(class10.aString116, 1942118537);
							if (null != string_12_ && string_12_.equals(string_9_)) {
								Class387.method6681(4, new StringBuilder().append(string).append(Class433.aClass433_5302.method7273((Class223.aClass495_2772), -1228902826)).toString(), (byte) 24);
								return;
							}
						}
					}
					for (int i_13_ = 0; i_13_ < client.anInt7449 * 493536965; i_13_++) {
						Class6 class6 = client.aClass6Array7452[i_13_];
						String string_14_ = Class383.method6515(class6.aString37, 1942118537);
						if (null != string_14_ && string_14_.equals(string_9_)) {
							Class387.method6681(4, new StringBuilder().append(Class433.aClass433_5308.method7273(Class223.aClass495_2772, -1154046234)).append(string).append(Class433.aClass433_5192.method7273(Class223.aClass495_2772, -627858057)).toString(), (byte) 53);
							return;
						}
						if (null != class6.aString43) {
							String string_15_ = Class383.method6515(class6.aString43, 1942118537);
							if (string_15_ != null && string_15_.equals(string_9_)) {
								Class387.method6681(4, new StringBuilder().append(Class433.aClass433_5308.method7273(Class223.aClass495_2772, -1770440532)).append(string).append(Class433.aClass433_5192.method7273(Class223.aClass495_2772, -695950851)).toString(), (byte) 47);
								return;
							}
						}
					}
					if (Class383.method6515((Class84.myPlayer.aString10546), 1942118537).equals(string_9_))
						Class387.method6681(4, (Class433.aClass433_5238.method7273(Class223.aClass495_2772, -1399059410)), (byte) -31);
					else {
						Class184 class184 = Class468_Sub20.method12807(1039135585);
						Class282_Sub23 class282_sub23 = Class271.method4828(OutgoingPacket.ADD_IGNORE_PACKET, class184.aClass432_2283, 732263274);
						class282_sub23.buffer.writeByte(Class108.method1846(string, -1697918722) + 1);
						class282_sub23.buffer.writeString(string);
						class282_sub23.buffer.writeByte(bool ? 1 : 0);
						class184.method3049(class282_sub23, -1139033433);
					}
				}
			}
		}
	}

	static final void method568(int i, int i_16_, int i_17_, int i_18_, short i_19_) {
		if (i > i_16_)
			Class232.method3922(Class532_Sub2.anIntArrayArray7072[i_17_], i_16_, i, i_18_, (byte) -16);
		else
			Class232.method3922(Class532_Sub2.anIntArrayArray7072[i_17_], i, i_16_, i_18_, (byte) 7);
	}
}
