/* Class161 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class161 {
	public int anInt2011;
	public int anInt2012 = 1025302087;
	public int anInt2013;
	public Class456 aClass456_2014;
	public int anInt2015;

	Class161(Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2) {
		aClass456_2014 = new Class456_Sub2(class521_sub1_sub1_sub2, false);
	}

	static final void method2823(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 567564004;
		Class435.method7300((((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]), (((Class527) class527).anIntArray6999[1 + 1942118537 * ((Class527) class527).anInt7012]), (((Class527) class527).anIntArray6999[2 + ((Class527) class527).anInt7012 * 1942118537]), (((Class527) class527).anIntArray6999[3 + ((Class527) class527).anInt7012 * 1942118537]), false, 256, 1449989045);
	}

	static final void method2824(Class527 class527, byte i) {
		int i_0_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class108 class108 = Class180.method3032(i_0_, (byte) -1);
		String string = "";
		if (class108 != null && null != class108.aString1083)
			string = class108.aString1083;
		((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1] = string;
	}

	static final void method2825(Class527 class527, byte i) {
		if (client.aByteArray7152 != null) {
			if (i < 1)
				((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537) - 1] = 1;
		} else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 0;
	}

	static final void method2826(Class527 class527, int i) {
		int i_1_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_1_, (byte) 25);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_1_ >> 16];
		Class282_Sub17.method12260(class118, class98, class527, 456705608);
	}

	static final void method2827(int i) {
		Class109.method1858(client.aClass118_7257, (byte) -8);
		Class96_Sub1.anInt8308 += -1463498693;
		if (!client.aBool7369 || !client.aBool7403) {
			if (2100482291 * Class96_Sub1.anInt8308 > 1) {
				client.aClass118_7257 = null;
				client.aClass118_7247 = null;
			}
		} else {
			int i_2_ = Class163.aClass209_2031.method3569(2003806769);
			int i_3_ = Class163.aClass209_2031.method3570(756095591);
			i_2_ -= 290868651 * client.anInt7361;
			i_3_ -= -554256717 * client.anInt7362;
			if (i_2_ < client.anInt7432 * 1326654489)
				i_2_ = 1326654489 * client.anInt7432;
			if (i_2_ + client.aClass118_7257.anInt1301 * 1506818197 > (1831433281 * client.anInt7367 + client.anInt7432 * 1326654489))
				i_2_ = (1831433281 * client.anInt7367 + 1326654489 * client.anInt7432 - client.aClass118_7257.anInt1301 * 1506818197);
			if (i_3_ < -2042382973 * client.anInt7265)
				i_3_ = -2042382973 * client.anInt7265;
			if (i_3_ + client.aClass118_7257.anInt1429 * -492594917 > (-2042382973 * client.anInt7265 + client.anInt7476 * -1128803311))
				i_3_ = (-1128803311 * client.anInt7476 + client.anInt7265 * -2042382973 - -492594917 * client.aClass118_7257.anInt1429);
			int i_4_;
			int i_5_;
			if (client.aClass118_7247 == Class221.aClass118_2763) {
				i_4_ = i_2_;
				i_5_ = i_3_;
			} else {
				i_4_ = (i_2_ - client.anInt7432 * 1326654489 + 276864765 * client.aClass118_7247.anInt1311);
				i_5_ = (client.aClass118_7247.anInt1312 * 682782159 + (i_3_ - client.anInt7265 * -2042382973));
			}
			if (!Class163.aClass209_2031.method3568((short) -19628)) {
				if (client.aBool7364) {
					Class60.method1170(-609337146);
					if (null != client.aClass118_7257.anObjectArray1394) {
						Class282_Sub43 class282_sub43 = new Class282_Sub43();
						class282_sub43.aClass118_8053 = client.aClass118_7257;
						class282_sub43.anInt8059 = i_4_ * 610593631;
						class282_sub43.anInt8055 = -916586071 * i_5_;
						class282_sub43.aClass118_8057 = client.aClass118_7370;
						class282_sub43.anObjectArray8054 = client.aClass118_7257.anObjectArray1394;
						Class96_Sub4.method13790(class282_sub43, 79466209);
					}
					if (client.aClass118_7370 != null && client.method11634(client.aClass118_7257) != null)
						Class119.method2076(client.aClass118_7257, client.aClass118_7370, 1527549411);
				} else if ((1 == -1718417173 * client.anInt7339 || Class327_Sub1.method12562(-285174833)) && 2144330291 * Class20.anInt169 > 2)
					Class242.method4164((client.anInt7361 * 290868651 + client.anInt7252 * -1609719385), (-833040991 * client.anInt7215 + -554256717 * client.anInt7362), -85284222);
				else if (Class216.method3677(575398960))
					Class242.method4164((client.anInt7252 * -1609719385 + client.anInt7361 * 290868651), (client.anInt7215 * -833040991 + client.anInt7362 * -554256717), 405616760);
				client.aClass118_7257 = null;
				client.aClass118_7247 = null;
			} else {
				if (Class96_Sub1.anInt8308 * 2100482291 > -2056717997 * client.aClass118_7257.anInt1381) {
					int i_6_ = i_2_ - -1609719385 * client.anInt7252;
					int i_7_ = i_3_ - client.anInt7215 * -833040991;
					if (i_6_ > 1556518327 * client.aClass118_7257.anInt1380 || i_6_ < -(client.aClass118_7257.anInt1380 * 1556518327) || i_7_ > client.aClass118_7257.anInt1380 * 1556518327 || i_7_ < -(client.aClass118_7257.anInt1380 * 1556518327))
						client.aBool7364 = true;
				}
				if (null != client.aClass118_7257.anObjectArray1451 && client.aBool7364) {
					Class282_Sub43 class282_sub43 = new Class282_Sub43();
					class282_sub43.aClass118_8053 = client.aClass118_7257;
					class282_sub43.anInt8059 = 610593631 * i_4_;
					class282_sub43.anInt8055 = i_5_ * -916586071;
					class282_sub43.anObjectArray8054 = client.aClass118_7257.anObjectArray1451;
					Class96_Sub4.method13790(class282_sub43, 814557204);
				}
			}
		}
	}
}
