
/* Interface24 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.applet.Applet;
import java.awt.Graphics;

public interface Interface24 {
	public void supplyApplet(Applet applet);

	public void destroy();

	public void method165();

	public void method166();

	public void method167();

	public void stop();

	public void start();

	public void method168(Applet applet);

	public void method169();

	public void init();

	public void method170();

	public void method171();

	public void paint(Graphics graphics);

	public void method172();

	public void method173();

	public void method174();

	public void method175();

	public void method176(Graphics graphics);

	public void method177(Graphics graphics);

	public void method178(Graphics graphics);

	public void method179(Graphics graphics);

	public void method180(Graphics graphics);

	public void method181();

	public void update(Graphics graphics);
}
