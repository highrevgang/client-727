/* Interface22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public interface Interface22 {
	public int[] method138(int i, float f, int i_0_, int i_1_, boolean bool);

	public boolean method139(int i, int i_2_);

	public int[] method140(int i, float f, int i_3_, int i_4_, boolean bool, byte i_5_);

	public int[] method141(int i, float f, int i_6_, int i_7_, boolean bool, int i_8_);

	public int[] method142(int i, float f, int i_9_, int i_10_, boolean bool);

	public float[] method143(int i, float f, int i_11_, int i_12_, boolean bool);

	public int method84(int i);

	public int method76();

	public Class169 method144(int i, int i_13_);

	public float[] method145(int i, float f, int i_14_, int i_15_, boolean bool, int i_16_);

	public boolean method146(int i);

	public boolean method147(int i);

	public boolean method148(int i);

	public int[] method149(int i, float f, int i_17_, int i_18_, boolean bool);

	public boolean method150(int i);

	public int method39();

	public int[] method151(int i, float f, int i_19_, int i_20_, boolean bool);

	public float[] method152(int i, float f, int i_21_, int i_22_, boolean bool);

	public float[] method153(int i, float f, int i_23_, int i_24_, boolean bool);

	public float[] method154(int i, float f, int i_25_, int i_26_, boolean bool);

	public float[] method155(int i, float f, int i_27_, int i_28_, boolean bool);

	public Class169 method156(int i);

	public void method157();

	public void method158();

	public void method159();

	public Class169 method160(int i);

	public void method161(int i);
}
