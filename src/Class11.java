/* Class11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class11 {
	static final int anInt119 = 63;
	static final int anInt120 = 7;
	static Class109 aClass109_121;
	public static int anInt122;
	static int anInt123;
	public static Class282_Sub51 aClass282_Sub51_124;

	static void method461(Class505 class505, int i, int i_0_, Class118 class118) {
		int i_1_ = 63;
		int i_2_ = 7;
		for (int i_3_ = 63; i_3_ >= 0; i_3_--) {
			int i_4_ = (i_3_ & 0x3f) << 10 | (i_2_ & 0x7) << 7 | i_1_ & 0x7f;
			Class38.method854(false, true, -162809483);
			int i_5_ = Class540.anIntArray7136[i_4_];
			Class13.method508(false, true, -1777806830);
			class505.B(i, i_0_ + (-492594917 * class118.anInt1429 * (63 - i_3_) >> 6), 1506818197 * class118.anInt1301, 1 + (-492594917 * class118.anInt1429 >> 6), i_5_, 0);
		}
	}

	static void method462(Class505 class505, int i, int i_6_, Class118 class118, int i_7_) {
		for (int i_8_ = 7; i_8_ >= 0; i_8_--) {
			for (int i_9_ = 127; i_9_ >= 0; i_9_--) {
				int i_10_ = (i_7_ & 0x3f) << 10 | (i_8_ & 0x7) << 7 | i_9_ & 0x7f;
				Class38.method854(false, true, -1443209041);
				int i_11_ = Class540.anIntArray7136[i_10_];
				Class13.method508(false, true, -1570286886);
				class505.B(i + (i_9_ * (1506818197 * class118.anInt1301) >> 7), i_6_ + (-492594917 * class118.anInt1429 * (7 - i_8_) >> 3), 1 + (1506818197 * class118.anInt1301 >> 7), (-492594917 * class118.anInt1429 >> 3) + 1, i_11_, 0);
			}
		}
	}

	static void method463(Class505 class505, int i, int i_12_, Class118 class118) {
		int i_13_ = 63;
		int i_14_ = 7;
		for (int i_15_ = 63; i_15_ >= 0; i_15_--) {
			int i_16_ = (i_15_ & 0x3f) << 10 | (i_14_ & 0x7) << 7 | i_13_ & 0x7f;
			Class38.method854(false, true, -1865948405);
			int i_17_ = Class540.anIntArray7136[i_16_];
			Class13.method508(false, true, 279884502);
			class505.B(i, i_12_ + (-492594917 * class118.anInt1429 * (63 - i_15_) >> 6), 1506818197 * class118.anInt1301, 1 + (-492594917 * class118.anInt1429 >> 6), i_17_, 0);
		}
	}

	static void method464(Class505 class505, int i, int i_18_, Class118 class118, int i_19_) {
		for (int i_20_ = 7; i_20_ >= 0; i_20_--) {
			for (int i_21_ = 127; i_21_ >= 0; i_21_--) {
				int i_22_ = (i_19_ & 0x3f) << 10 | (i_20_ & 0x7) << 7 | i_21_ & 0x7f;
				Class38.method854(false, true, 15349129);
				int i_23_ = Class540.anIntArray7136[i_22_];
				Class13.method508(false, true, -90294638);
				class505.B(i + (i_21_ * (1506818197 * class118.anInt1301) >> 7), i_18_ + (-492594917 * class118.anInt1429 * (7 - i_20_) >> 3), 1 + (1506818197 * class118.anInt1301 >> 7), (-492594917 * class118.anInt1429 >> 3) + 1, i_23_, 0);
			}
		}
	}

	static void method465(Class505 class505, int i, int i_24_, Class118 class118, int i_25_) {
		for (int i_26_ = 7; i_26_ >= 0; i_26_--) {
			for (int i_27_ = 127; i_27_ >= 0; i_27_--) {
				int i_28_ = (i_25_ & 0x3f) << 10 | (i_26_ & 0x7) << 7 | i_27_ & 0x7f;
				Class38.method854(false, true, -1264715883);
				int i_29_ = Class540.anIntArray7136[i_28_];
				Class13.method508(false, true, -339939330);
				class505.B(i + (i_27_ * (1506818197 * class118.anInt1301) >> 7), i_24_ + (-492594917 * class118.anInt1429 * (7 - i_26_) >> 3), 1 + (1506818197 * class118.anInt1301 >> 7), (-492594917 * class118.anInt1429 >> 3) + 1, i_29_, 0);
			}
		}
	}

	Class11() throws Throwable {
		throw new Error();
	}

	static void method466(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 1] = Class409.aClass242_4922.method4156((((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 1]), -1396181317).method4101(Class158_Sub1.aClass3_8507, (byte) -15) ? 1 : 0;
	}

	static final void method467(Class527 class527, byte i) {
		((Class527) class527).anInt7001 -= 1918006146;
		if ((((Class527) class527).aLongArray7003[1820448321 * ((Class527) class527).anInt7001]) < (((Class527) class527).aLongArray7003[1820448321 * ((Class527) class527).anInt7001 + 1]))
			((Class527) class527).anInt7020 += ((((Class527) class527).anIntArray7018[301123709 * ((Class527) class527).anInt7020]) * -1051529003);
	}

	static final void method468(Class527 class527, int i) {
		Class184 class184 = Class468_Sub20.method12807(461566707);
		Class282_Sub23 class282_sub23 = Class271.method4828(OutgoingPacket.aClass379_4591, class184.aClass432_2283, 1283884802);
		class282_sub23.buffer.writeByte(0);
		int i_30_ = class282_sub23.buffer.index * -1990677291;
		class282_sub23.buffer.writeByte(2);
		class282_sub23.buffer.writeShort(((Class527) class527).aClass346_7009.anInt4048 * -624100047, 1417031095);
		((Class527) class527).aClass346_7009.aClass282_Sub50_Sub9_4047.method14896(class282_sub23.buffer, ((Class527) class527).aClass346_7009.anIntArray4046, -463581846);
		class282_sub23.buffer.method13061((class282_sub23.buffer.index * -1990677291 - i_30_), -76404824);
		class184.method3049(class282_sub23, -206649129);
	}

	public static final void method469(int i, int i_31_, int i_32_, int i_33_, int i_34_, byte i_35_) {
		Class96_Sub13.anInt9368 = -2005398665 * i;
		Class369.anInt4280 = i_31_ * -772343735;
		Class121.anInt1527 = -366984663 * i_32_;
		Class473.anInt5606 = 2044511935 * i_33_;
		Class501.anInt5828 = -1387629705 * i_34_;
		if (1123046983 * Class501.anInt5828 >= 100) {
			int i_36_ = 75271680 * Class96_Sub13.anInt9368 + 256;
			int i_37_ = -51121664 * Class369.anInt4280 + 256;
			int i_38_ = (Class504.method8389(i_36_, i_37_, Class4.anInt35 * 675588453, (byte) 25) - Class121.anInt1527 * 654473753);
			int i_39_ = i_36_ - Class31.anInt361 * -360258135;
			int i_40_ = i_38_ - Class109_Sub1.anInt9384 * 1929945579;
			int i_41_ = i_37_ - Class246.anInt3029 * 413271601;
			int i_42_ = (int) Math.sqrt((double) (i_39_ * i_39_ + i_41_ * i_41_));
			Class293.anInt3512 = ((int) (Math.atan2((double) i_40_, (double) i_42_) * 2607.5945876176133) & 0x3fff) * -647467135;
			Class518.anInt5930 = ((int) (Math.atan2((double) i_39_, (double) i_41_) * -2607.5945876176133) & 0x3fff) * 1898253385;
			Class121.anInt1525 = 0;
			if (Class293.anInt3512 * 726126721 < 1024)
				Class293.anInt3512 = -1581382656;
			if (726126721 * Class293.anInt3512 > 3072)
				Class293.anInt3512 = -449180672;
		}
		Class262.anInt3240 = 1926220865;
		Class86.anInt833 = -1509271845;
		Class508.anInt5864 = 987778595;
	}

	public static char method470(byte i, int i_43_) {
		int i_44_ = i & 0xff;
		if (0 == i_44_)
			throw new IllegalArgumentException(new StringBuilder().append("").append(Integer.toString(i_44_, 16)).toString());
		if (i_44_ >= 128 && i_44_ < 160) {
			int i_45_ = Class490.aCharArray5766[i_44_ - 128];
			if (0 == i_45_)
				i_45_ = 63;
			i_44_ = i_45_;
		}
		return (char) i_44_;
	}

	static final void method471(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 - 1] = (((Class527) class527).aClass282_Sub4_7011.method12095(837376369)[(((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 - 1])]);
	}
}
