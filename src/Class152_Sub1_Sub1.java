/* Class152_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class152_Sub1_Sub1 extends Class152_Sub1 {
	static int[][] anIntArrayArray10076;
	Class137_Sub2 aClass137_Sub2_10077;
	int anInt10078;
	Class505_Sub1 aClass505_Sub1_10079;
	int anInt10080;
	int anInt10081;
	static int[] anIntArray10082 = new int[6];
	int anInt10083;
	int anInt10084;
	int anInt10085;

	static {
		anIntArrayArray10076 = new int[6][];
	}

	Class137_Sub2 method13519() {
		if (((Class152_Sub1_Sub1) this).aClass137_Sub2_10077 == null) {
			Interface22 interface22 = (((Class152_Sub1_Sub1) this).aClass505_Sub1_10079.anInterface22_5834);
			anIntArray10082[0] = ((Class152_Sub1_Sub1) this).anInt10081;
			anIntArray10082[1] = ((Class152_Sub1_Sub1) this).anInt10084;
			anIntArray10082[2] = ((Class152_Sub1_Sub1) this).anInt10080;
			anIntArray10082[3] = ((Class152_Sub1_Sub1) this).anInt10085;
			anIntArray10082[4] = ((Class152_Sub1_Sub1) this).anInt10078;
			anIntArray10082[5] = ((Class152_Sub1_Sub1) this).anInt10083;
			boolean bool = false;
			int i = 0;
			for (int i_0_ = 0; i_0_ < 6; i_0_++) {
				if (!interface22.method139(anIntArray10082[i_0_], -1020099398))
					return null;
				Class169 class169 = interface22.method144(anIntArray10082[i_0_], -2056544604);
				int i_1_ = class169.aBool2065 ? 64 : 128;
				if (i_1_ > i)
					i = i_1_;
				if (class169.aByte2088 > 0)
					bool = true;
			}
			for (int i_2_ = 0; i_2_ < 6; i_2_++)
				anIntArrayArray10076[i_2_] = interface22.method140(anIntArray10082[i_2_], 1.0F, i, i, false, (byte) 70);
			((Class152_Sub1_Sub1) this).aClass137_Sub2_10077 = new Class137_Sub2((((Class152_Sub1_Sub1) this).aClass505_Sub1_10079), Class150.aClass150_1950, Class76.aClass76_751, i, bool, anIntArrayArray10076);
		}
		return ((Class152_Sub1_Sub1) this).aClass137_Sub2_10077;
	}

	Class152_Sub1_Sub1(Class505_Sub1 class505_sub1, int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		((Class152_Sub1_Sub1) this).aClass505_Sub1_10079 = class505_sub1;
		((Class152_Sub1_Sub1) this).anInt10080 = i_4_;
		((Class152_Sub1_Sub1) this).anInt10085 = i_5_;
		((Class152_Sub1_Sub1) this).anInt10078 = i_6_;
		((Class152_Sub1_Sub1) this).anInt10083 = i_7_;
		((Class152_Sub1_Sub1) this).anInt10081 = i;
		((Class152_Sub1_Sub1) this).anInt10084 = i_3_;
	}

	Class137_Sub2 method13520() {
		if (((Class152_Sub1_Sub1) this).aClass137_Sub2_10077 == null) {
			Interface22 interface22 = (((Class152_Sub1_Sub1) this).aClass505_Sub1_10079.anInterface22_5834);
			anIntArray10082[0] = ((Class152_Sub1_Sub1) this).anInt10081;
			anIntArray10082[1] = ((Class152_Sub1_Sub1) this).anInt10084;
			anIntArray10082[2] = ((Class152_Sub1_Sub1) this).anInt10080;
			anIntArray10082[3] = ((Class152_Sub1_Sub1) this).anInt10085;
			anIntArray10082[4] = ((Class152_Sub1_Sub1) this).anInt10078;
			anIntArray10082[5] = ((Class152_Sub1_Sub1) this).anInt10083;
			boolean bool = false;
			int i = 0;
			for (int i_8_ = 0; i_8_ < 6; i_8_++) {
				if (!interface22.method139(anIntArray10082[i_8_], -1745982008))
					return null;
				Class169 class169 = interface22.method144(anIntArray10082[i_8_], -1803305965);
				int i_9_ = class169.aBool2065 ? 64 : 128;
				if (i_9_ > i)
					i = i_9_;
				if (class169.aByte2088 > 0)
					bool = true;
			}
			for (int i_10_ = 0; i_10_ < 6; i_10_++)
				anIntArrayArray10076[i_10_] = interface22.method140(anIntArray10082[i_10_], 1.0F, i, i, false, (byte) 74);
			((Class152_Sub1_Sub1) this).aClass137_Sub2_10077 = new Class137_Sub2((((Class152_Sub1_Sub1) this).aClass505_Sub1_10079), Class150.aClass150_1950, Class76.aClass76_751, i, bool, anIntArrayArray10076);
		}
		return ((Class152_Sub1_Sub1) this).aClass137_Sub2_10077;
	}

	Class137_Sub2 method13521() {
		if (((Class152_Sub1_Sub1) this).aClass137_Sub2_10077 == null) {
			Interface22 interface22 = (((Class152_Sub1_Sub1) this).aClass505_Sub1_10079.anInterface22_5834);
			anIntArray10082[0] = ((Class152_Sub1_Sub1) this).anInt10081;
			anIntArray10082[1] = ((Class152_Sub1_Sub1) this).anInt10084;
			anIntArray10082[2] = ((Class152_Sub1_Sub1) this).anInt10080;
			anIntArray10082[3] = ((Class152_Sub1_Sub1) this).anInt10085;
			anIntArray10082[4] = ((Class152_Sub1_Sub1) this).anInt10078;
			anIntArray10082[5] = ((Class152_Sub1_Sub1) this).anInt10083;
			boolean bool = false;
			int i = 0;
			for (int i_11_ = 0; i_11_ < 6; i_11_++) {
				if (!interface22.method139(anIntArray10082[i_11_], -2037265679))
					return null;
				Class169 class169 = interface22.method144(anIntArray10082[i_11_], -1757106479);
				int i_12_ = class169.aBool2065 ? 64 : 128;
				if (i_12_ > i)
					i = i_12_;
				if (class169.aByte2088 > 0)
					bool = true;
			}
			for (int i_13_ = 0; i_13_ < 6; i_13_++)
				anIntArrayArray10076[i_13_] = interface22.method140(anIntArray10082[i_13_], 1.0F, i, i, false, (byte) 70);
			((Class152_Sub1_Sub1) this).aClass137_Sub2_10077 = new Class137_Sub2((((Class152_Sub1_Sub1) this).aClass505_Sub1_10079), Class150.aClass150_1950, Class76.aClass76_751, i, bool, anIntArrayArray10076);
		}
		return ((Class152_Sub1_Sub1) this).aClass137_Sub2_10077;
	}
}
