
/* Class282 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Point;

public class Class282 {
	public Class282 aClass282_3378;
	public long aLong3379;
	public Class282 aClass282_3380;

	public void method4990() {
		if (null != aClass282_3380) {
			aClass282_3380.aClass282_3378 = aClass282_3378;
			aClass282_3378.aClass282_3380 = aClass282_3380;
			aClass282_3378 = null;
			aClass282_3380 = null;
		}
	}

	public void method4991(int i) {
		if (null != aClass282_3380) {
			aClass282_3380.aClass282_3378 = aClass282_3378;
			aClass282_3378.aClass282_3380 = aClass282_3380;
			aClass282_3378 = null;
			aClass282_3380 = null;
		}
	}

	public boolean method4992() {
		if (null == aClass282_3380)
			return false;
		return true;
	}

	public boolean method4993() {
		if (null == aClass282_3380)
			return false;
		return true;
	}

	public boolean method4994(int i) {
		if (null == aClass282_3380)
			return false;
		return true;
	}

	public void method4995() {
		if (null != aClass282_3380) {
			aClass282_3380.aClass282_3378 = aClass282_3378;
			aClass282_3378.aClass282_3380 = aClass282_3380;
			aClass282_3378 = null;
			aClass282_3380 = null;
		}
	}

	public void method4996() {
		if (null != aClass282_3380) {
			aClass282_3380.aClass282_3378 = aClass282_3378;
			aClass282_3378.aClass282_3380 = aClass282_3380;
			aClass282_3378 = null;
			aClass282_3380 = null;
		}
	}

	public boolean method4997() {
		if (null == aClass282_3380)
			return false;
		return true;
	}

	public boolean method4998() {
		if (null == aClass282_3380)
			return false;
		return true;
	}

	public boolean method4999() {
		if (null == aClass282_3380)
			return false;
		return true;
	}

	static final void method5000(Class527 class527, int i) {
		int i_0_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_0_, (byte) 91);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_0_ >> 16];
		Class283.method5009(class118, class98, class527, -1863768075);
	}

	static final void method5001(Class118 class118, Class527 class527, int i) {
		Class414 class414 = class118.method1989(Class487.aClass378_5752, client.anInterface35_7206, 1667104026);
		int i_1_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Point point = class414.method6954(class118.aString1391, 1506818197 * class118.anInt1301, class118.anInt1358 * -753644021, i_1_, Class182.aClass160Array2261, -1446712454);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = point.x;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = point.y;
	}

	static final void method5002(Class527 class527, int i) {
		Class93.method1573(class527, (byte) 59);
	}

	static final void method5003(Class527 class527, byte i) {
		int i_2_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_2_ & 0x3fff;
	}

	static final void method5004(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class388.method6690(class118, class98, class527, (byte) 92);
	}
}
