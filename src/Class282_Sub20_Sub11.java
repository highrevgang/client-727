/* Class282_Sub20_Sub11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub20_Sub11 extends Class282_Sub20 {
	int anInt9803;
	static final int anInt9804 = 4096;
	int anInt9805 = 0;
	static final int anInt9806 = 0;

	int[] method12327(int i) {
		int[] is = aClass320_7667.method5721(i, -1012581139);
		if (aClass320_7667.aBool3722) {
			int[] is_0_ = method12317(0, i, 2010456231);
			for (int i_1_ = 0; i_1_ < Class316.anInt3670 * -402153223; i_1_++) {
				int i_2_ = is_0_[i_1_];
				is[i_1_] = ((i_2_ < ((Class282_Sub20_Sub11) this).anInt9805 * -590764957) || i_2_ > -1619899923 * (((Class282_Sub20_Sub11) this).anInt9803)) ? 0 : 4096;
			}
		}
		return is;
	}

	int[] method12319(int i, int i_3_) {
		int[] is = aClass320_7667.method5721(i, -1057419566);
		if (aClass320_7667.aBool3722) {
			int[] is_4_ = method12317(0, i, 1980715677);
			for (int i_5_ = 0; i_5_ < Class316.anInt3670 * -402153223; i_5_++) {
				int i_6_ = is_4_[i_5_];
				is[i_5_] = ((i_6_ < ((Class282_Sub20_Sub11) this).anInt9805 * -590764957) || i_6_ > -1619899923 * (((Class282_Sub20_Sub11) this).anInt9803)) ? 0 : 4096;
			}
		}
		return is;
	}

	void method12322(int i, RsByteBuffer class282_sub35, int i_7_) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub11) this).anInt9805 = class282_sub35.readUnsignedShort() * 1625161035;
			break;
		case 1:
			((Class282_Sub20_Sub11) this).anInt9803 = class282_sub35.readUnsignedShort() * 643529701;
			break;
		}
	}

	int[] method12325(int i) {
		int[] is = aClass320_7667.method5721(i, -1722192840);
		if (aClass320_7667.aBool3722) {
			int[] is_8_ = method12317(0, i, 2007578320);
			for (int i_9_ = 0; i_9_ < Class316.anInt3670 * -402153223; i_9_++) {
				int i_10_ = is_8_[i_9_];
				is[i_9_] = ((i_10_ < ((Class282_Sub20_Sub11) this).anInt9805 * -590764957) || i_10_ > -1619899923 * (((Class282_Sub20_Sub11) this).anInt9803)) ? 0 : 4096;
			}
		}
		return is;
	}

	void method12335(int i, RsByteBuffer class282_sub35) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub11) this).anInt9805 = class282_sub35.readUnsignedShort() * 1625161035;
			break;
		case 1:
			((Class282_Sub20_Sub11) this).anInt9803 = class282_sub35.readUnsignedShort() * 643529701;
			break;
		}
	}

	int[] method12336(int i) {
		int[] is = aClass320_7667.method5721(i, -2052390221);
		if (aClass320_7667.aBool3722) {
			int[] is_11_ = method12317(0, i, 2106297309);
			for (int i_12_ = 0; i_12_ < Class316.anInt3670 * -402153223; i_12_++) {
				int i_13_ = is_11_[i_12_];
				is[i_12_] = ((i_13_ < ((Class282_Sub20_Sub11) this).anInt9805 * -590764957) || i_13_ > -1619899923 * (((Class282_Sub20_Sub11) this).anInt9803)) ? 0 : 4096;
			}
		}
		return is;
	}

	void method12332(int i, RsByteBuffer class282_sub35) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub11) this).anInt9805 = class282_sub35.readUnsignedShort() * 1625161035;
			break;
		case 1:
			((Class282_Sub20_Sub11) this).anInt9803 = class282_sub35.readUnsignedShort() * 643529701;
			break;
		}
	}

	public Class282_Sub20_Sub11() {
		super(1, true);
		((Class282_Sub20_Sub11) this).anInt9803 = -1212264448;
	}

	void method12334(int i, RsByteBuffer class282_sub35) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub11) this).anInt9805 = class282_sub35.readUnsignedShort() * 1625161035;
			break;
		case 1:
			((Class282_Sub20_Sub11) this).anInt9803 = class282_sub35.readUnsignedShort() * 643529701;
			break;
		}
	}

	public static void method15264(int i, short i_14_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(1, (long) i);
		class282_sub50_sub12.method14965((byte) 15);
	}
}
