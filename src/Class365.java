/* Class365 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class365 implements Interface3 {
	public int anInt4220;
	public int anInt4221;
	public int anInt4222;
	public int anInt4223;
	public int anInt4224;
	public Class356 aClass356_4225;
	public Class353 aClass353_4226;
	public int anInt4227;
	public int anInt4228;
	public int anInt4229;
	public boolean aBool4230;

	public static Class365 method6293(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class356 class356 = (Class350_Sub3_Sub1.method15558(1854960259)[class282_sub35.readUnsignedByte()]);
		Class353 class353 = (Class483.method8155(152314627)[class282_sub35.readUnsignedByte()]);
		int i_0_ = class282_sub35.method13081(2043906445);
		int i_1_ = class282_sub35.method13081(1811161684);
		int i_2_ = class282_sub35.readUnsignedShort();
		int i_3_ = class282_sub35.readUnsignedShort();
		int i_4_ = class282_sub35.readIntLE();
		int i_5_ = class282_sub35.readIntLE();
		int i_6_ = class282_sub35.readIntLE();
		boolean bool = class282_sub35.readUnsignedByte() == 1;
		return new Class365(i, class356, class353, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_, bool);
	}

	public static Class365 method6294(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class356 class356 = (Class350_Sub3_Sub1.method15558(549608758)[class282_sub35.readUnsignedByte()]);
		Class353 class353 = (Class483.method8155(152314627)[class282_sub35.readUnsignedByte()]);
		int i_7_ = class282_sub35.method13081(2032279624);
		int i_8_ = class282_sub35.method13081(1879678143);
		int i_9_ = class282_sub35.readUnsignedShort();
		int i_10_ = class282_sub35.readUnsignedShort();
		int i_11_ = class282_sub35.readIntLE();
		int i_12_ = class282_sub35.readIntLE();
		int i_13_ = class282_sub35.readIntLE();
		boolean bool = class282_sub35.readUnsignedByte() == 1;
		return new Class365(i, class356, class353, i_7_, i_8_, i_9_, i_10_, i_11_, i_12_, i_13_, bool);
	}

	@Override
	public Class60 method25() {
		return Class60.aClass60_605;
	}

	@Override
	public Class60 method24(int i) {
		return Class60.aClass60_605;
	}

	Class365(int i, Class356 class356, Class353 class353, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, boolean bool) {
		anInt4229 = i * 1551783443;
		aClass356_4225 = class356;
		aClass353_4226 = class353;
		anInt4221 = 368783541 * i_14_;
		anInt4223 = i_15_ * 843965525;
		anInt4227 = i_16_ * 1984931449;
		anInt4228 = -1733557607 * i_17_;
		anInt4224 = i_18_ * 764649591;
		anInt4222 = -1922122707 * i_19_;
		anInt4220 = -965307251 * i_20_;
		aBool4230 = bool;
	}

	public static Class365 method6295(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class356 class356 = (Class350_Sub3_Sub1.method15558(1993261440)[class282_sub35.readUnsignedByte()]);
		Class353 class353 = (Class483.method8155(152314627)[class282_sub35.readUnsignedByte()]);
		int i_21_ = class282_sub35.method13081(1715691742);
		int i_22_ = class282_sub35.method13081(1904199456);
		int i_23_ = class282_sub35.readUnsignedShort();
		int i_24_ = class282_sub35.readUnsignedShort();
		int i_25_ = class282_sub35.readIntLE();
		int i_26_ = class282_sub35.readIntLE();
		int i_27_ = class282_sub35.readIntLE();
		boolean bool = class282_sub35.readUnsignedByte() == 1;
		return new Class365(i, class356, class353, i_21_, i_22_, i_23_, i_24_, i_25_, i_26_, i_27_, bool);
	}

	public static Class365 method6296(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class356 class356 = (Class350_Sub3_Sub1.method15558(723447286)[class282_sub35.readUnsignedByte()]);
		Class353 class353 = (Class483.method8155(152314627)[class282_sub35.readUnsignedByte()]);
		int i_28_ = class282_sub35.method13081(1939202286);
		int i_29_ = class282_sub35.method13081(1789526211);
		int i_30_ = class282_sub35.readUnsignedShort();
		int i_31_ = class282_sub35.readUnsignedShort();
		int i_32_ = class282_sub35.readIntLE();
		int i_33_ = class282_sub35.readIntLE();
		int i_34_ = class282_sub35.readIntLE();
		boolean bool = class282_sub35.readUnsignedByte() == 1;
		return new Class365(i, class356, class353, i_28_, i_29_, i_30_, i_31_, i_32_, i_33_, i_34_, bool);
	}

	public static Class365 method6297(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class356 class356 = (Class350_Sub3_Sub1.method15558(105705767)[class282_sub35.readUnsignedByte()]);
		Class353 class353 = (Class483.method8155(152314627)[class282_sub35.readUnsignedByte()]);
		int i_35_ = class282_sub35.method13081(1786557142);
		int i_36_ = class282_sub35.method13081(1855918084);
		int i_37_ = class282_sub35.readUnsignedShort();
		int i_38_ = class282_sub35.readUnsignedShort();
		int i_39_ = class282_sub35.readIntLE();
		int i_40_ = class282_sub35.readIntLE();
		int i_41_ = class282_sub35.readIntLE();
		boolean bool = class282_sub35.readUnsignedByte() == 1;
		return new Class365(i, class356, class353, i_35_, i_36_, i_37_, i_38_, i_39_, i_40_, i_41_, bool);
	}

	public static void method6298(int i, int i_42_) {
		if (client.anInt7166 * -1741204137 != i) {
			client.anInt7396 = 0;
			if (i == 10 || 17 == i) {
				Class78.method1384(949776087);
			}
			if (i != 10 && null != Class233.aClass202_2883) {
				Class233.aClass202_2883.method3318(1798206755);
				Class233.aClass202_2883 = null;
			}
			if (5 == i) {
				Class348.method6175((-1741204137 * client.anInt7166 == 3 || -1741204137 * client.anInt7166 == 8 || ((Class58.aClass529_527.anInt7036 * 376713291) != -1699899559 * client.anInt7349)), 618699905);
			}
			if (0 == i) {
				Class346.method6161((-1699899559 * client.anInt7349 != (1411780693 * Class58.aClass529_527.anInt7027)), -56849347);
			}
			if (i == 14 || 12 == i) {
				Class331.method5921((byte) 81);
			} else if (i == 19 || i == 7 && client.anInt7166 * -1741204137 != 6) {
				Class78.method1384(949776087);
			} else if (i == 8) {
				Class247.method4251((short) 17247);
			}
			if (Class464.method7742(i, (byte) 74)) {
				client.aClass257_7353.method4445((byte) -22);
				Class122.method2111(true, 662490589);
			}
			if (18 == i || 5 == i) {
				Class60.method1172(280036334);
			}
			boolean bool = (1 == i || Class97.method1612(i, 1908805257) || Class282_Sub17.method12259(i, -2143190341));
			boolean bool_43_ = (client.anInt7166 * -1741204137 == 1 || Class97.method1612(client.anInt7166 * -1741204137, 1908805257) || Class282_Sub17.method12259((client.anInt7166 * -1741204137), -2129908537));
			if (bool != bool_43_) {
				if (bool) {
					Class260.anInt3223 = 408122991 * Class260.anInt3228;
					if (Class393.aClass282_Sub54_4783.aClass468_Sub13_8229.method12714(-1591414492) != 0) {
						Class339.method6047(2, Class512.aClass317_5884, 1712678171 * Class260.anInt3228, 0, Class393.aClass282_Sub54_4783.aClass468_Sub13_8229.method12714(695261258), false, 500361678);
						Class468_Sub6.method12658(935417586);
					} else {
						Class358.method6240(2, 1276678940);
					}
					Class119.aClass312_1462.method5523(false, (byte) 123);
				} else {
					Class358.method6240(2, 1962133181);
					Class119.aClass312_1462.method5523(true, (byte) 44);
				}
			}
			if (Class464.method7742(i, (byte) 48) || 10 == i || 17 == i) {
				Class316.aClass505_3680.method8420();
			}
			client.anInt7166 = i * 111437415;
		}
	}

	public static void method6299(int i, boolean bool, int i_44_, boolean bool_45_, byte i_46_) {
		Class52.method1086(0, Class448.aClass217_Sub1Array5426.length - 1, i, bool, i_44_, bool_45_, (byte) -100);
		Class448.anInt5430 = 0;
		Class448.aClass510_5423 = null;
	}
}
