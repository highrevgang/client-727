/* Class51 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class51 implements Interface2 {
	Class8 aClass8_484;
	Class62 aClass62_485;
	Class317 aClass317_486;
	Class317 aClass317_487;
	static int anInt488;

	public void method11(boolean bool) {
		if (bool) {
			int i = ((((Class51) this).aClass62_485.aClass356_650.method6221(2056525513 * ((Class51) this).aClass62_485.anInt652, 150480619 * client.anInt7439, 1034015325)) + -906350423 * ((Class51) this).aClass62_485.anInt651);
			int i_0_ = ((((Class51) this).aClass62_485.aClass353_654.method6198(38429607 * ((Class51) this).aClass62_485.anInt648, 1176039023 * client.anInt7312, 1192108582)) + ((Class51) this).aClass62_485.anInt655 * -601422611);
			((Class51) this).aClass8_484.method378(((Class51) this).aClass62_485.aString649, i, i_0_, 2056525513 * ((Class51) this).aClass62_485.anInt652, 38429607 * ((Class51) this).aClass62_485.anInt648, -1179509109 * ((Class51) this).aClass62_485.anInt659, ((Class51) this).aClass62_485.anInt660 * 1958427123, -533570239 * ((Class51) this).aClass62_485.anInt653, 1391502107 * ((Class51) this).aClass62_485.anInt656, -1240305753 * ((Class51) this).aClass62_485.anInt657, null, null, null, 0, 0, (byte) 60);
		}
	}

	Class51(Class317 class317, Class317 class317_1_, Class62 class62) {
		((Class51) this).aClass62_485 = class62;
		((Class51) this).aClass317_487 = class317;
		((Class51) this).aClass317_486 = class317_1_;
	}

	public void method20(boolean bool, int i) {
		if (bool) {
			int i_2_ = ((((Class51) this).aClass62_485.aClass356_650.method6221(2056525513 * ((Class51) this).aClass62_485.anInt652, 150480619 * client.anInt7439, 1847576923)) + -906350423 * ((Class51) this).aClass62_485.anInt651);
			int i_3_ = ((((Class51) this).aClass62_485.aClass353_654.method6198(38429607 * ((Class51) this).aClass62_485.anInt648, 1176039023 * client.anInt7312, 1796559558)) + ((Class51) this).aClass62_485.anInt655 * -601422611);
			((Class51) this).aClass8_484.method378(((Class51) this).aClass62_485.aString649, i_2_, i_3_, 2056525513 * ((Class51) this).aClass62_485.anInt652, 38429607 * ((Class51) this).aClass62_485.anInt648, -1179509109 * ((Class51) this).aClass62_485.anInt659, ((Class51) this).aClass62_485.anInt660 * 1958427123, -533570239 * ((Class51) this).aClass62_485.anInt653, 1391502107 * ((Class51) this).aClass62_485.anInt656, -1240305753 * ((Class51) this).aClass62_485.anInt657, null, null, null, 0, 0, (byte) 28);
		}
	}

	public boolean method21() {
		boolean bool = true;
		if (!((Class51) this).aClass317_487.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -1933108469))
			bool = false;
		if (!((Class51) this).aClass317_486.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -1620849369))
			bool = false;
		return bool;
	}

	public void method12(boolean bool) {
		if (bool) {
			int i = ((((Class51) this).aClass62_485.aClass356_650.method6221(2056525513 * ((Class51) this).aClass62_485.anInt652, 150480619 * client.anInt7439, 825441624)) + -906350423 * ((Class51) this).aClass62_485.anInt651);
			int i_4_ = ((((Class51) this).aClass62_485.aClass353_654.method6198(38429607 * ((Class51) this).aClass62_485.anInt648, 1176039023 * client.anInt7312, 2097685186)) + ((Class51) this).aClass62_485.anInt655 * -601422611);
			((Class51) this).aClass8_484.method378(((Class51) this).aClass62_485.aString649, i, i_4_, 2056525513 * ((Class51) this).aClass62_485.anInt652, 38429607 * ((Class51) this).aClass62_485.anInt648, -1179509109 * ((Class51) this).aClass62_485.anInt659, ((Class51) this).aClass62_485.anInt660 * 1958427123, -533570239 * ((Class51) this).aClass62_485.anInt653, 1391502107 * ((Class51) this).aClass62_485.anInt656, -1240305753 * ((Class51) this).aClass62_485.anInt657, null, null, null, 0, 0, (byte) 10);
		}
	}

	public void method16() {
		Class414 class414 = Class163.method2845(((Class51) this).aClass317_486, (-1949174501 * ((Class51) this).aClass62_485.anInt658), (byte) 20);
		((Class51) this).aClass8_484 = (Class316.aClass505_3680.method8448(class414, Class91.method1534(((Class51) this).aClass317_487, (-1949174501 * ((Class51) this).aClass62_485.anInt658)), true));
	}

	public boolean method10(int i) {
		boolean bool = true;
		if (!((Class51) this).aClass317_487.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -1926337814))
			bool = false;
		if (!((Class51) this).aClass317_486.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), 1793873759))
			bool = false;
		return bool;
	}

	public void method15() {
		Class414 class414 = Class163.method2845(((Class51) this).aClass317_486, (-1949174501 * ((Class51) this).aClass62_485.anInt658), (byte) 108);
		((Class51) this).aClass8_484 = (Class316.aClass505_3680.method8448(class414, Class91.method1534(((Class51) this).aClass317_487, (-1949174501 * ((Class51) this).aClass62_485.anInt658)), true));
	}

	public void method22(int i) {
		Class414 class414 = Class163.method2845(((Class51) this).aClass317_486, (-1949174501 * ((Class51) this).aClass62_485.anInt658), (byte) 81);
		((Class51) this).aClass8_484 = (Class316.aClass505_3680.method8448(class414, Class91.method1534(((Class51) this).aClass317_487, (-1949174501 * ((Class51) this).aClass62_485.anInt658)), true));
	}

	public void method23() {
		Class414 class414 = Class163.method2845(((Class51) this).aClass317_486, (-1949174501 * ((Class51) this).aClass62_485.anInt658), (byte) 60);
		((Class51) this).aClass8_484 = (Class316.aClass505_3680.method8448(class414, Class91.method1534(((Class51) this).aClass317_487, (-1949174501 * ((Class51) this).aClass62_485.anInt658)), true));
	}

	public void method18() {
		Class414 class414 = Class163.method2845(((Class51) this).aClass317_486, (-1949174501 * ((Class51) this).aClass62_485.anInt658), (byte) 122);
		((Class51) this).aClass8_484 = (Class316.aClass505_3680.method8448(class414, Class91.method1534(((Class51) this).aClass317_487, (-1949174501 * ((Class51) this).aClass62_485.anInt658)), true));
	}

	public void method19() {
		Class414 class414 = Class163.method2845(((Class51) this).aClass317_486, (-1949174501 * ((Class51) this).aClass62_485.anInt658), (byte) 65);
		((Class51) this).aClass8_484 = (Class316.aClass505_3680.method8448(class414, Class91.method1534(((Class51) this).aClass317_487, (-1949174501 * ((Class51) this).aClass62_485.anInt658)), true));
	}

	public boolean method13() {
		boolean bool = true;
		if (!((Class51) this).aClass317_487.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -871751794))
			bool = false;
		if (!((Class51) this).aClass317_486.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -194229407))
			bool = false;
		return bool;
	}

	public boolean method9() {
		boolean bool = true;
		if (!((Class51) this).aClass317_487.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -335910971))
			bool = false;
		if (!((Class51) this).aClass317_486.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -1623629823))
			bool = false;
		return bool;
	}

	public void method14(boolean bool) {
		if (bool) {
			int i = ((((Class51) this).aClass62_485.aClass356_650.method6221(2056525513 * ((Class51) this).aClass62_485.anInt652, 150480619 * client.anInt7439, 2080844430)) + -906350423 * ((Class51) this).aClass62_485.anInt651);
			int i_5_ = ((((Class51) this).aClass62_485.aClass353_654.method6198(38429607 * ((Class51) this).aClass62_485.anInt648, 1176039023 * client.anInt7312, 879742369)) + ((Class51) this).aClass62_485.anInt655 * -601422611);
			((Class51) this).aClass8_484.method378(((Class51) this).aClass62_485.aString649, i, i_5_, 2056525513 * ((Class51) this).aClass62_485.anInt652, 38429607 * ((Class51) this).aClass62_485.anInt648, -1179509109 * ((Class51) this).aClass62_485.anInt659, ((Class51) this).aClass62_485.anInt660 * 1958427123, -533570239 * ((Class51) this).aClass62_485.anInt653, 1391502107 * ((Class51) this).aClass62_485.anInt656, -1240305753 * ((Class51) this).aClass62_485.anInt657, null, null, null, 0, 0, (byte) 30);
		}
	}

	public boolean method17() {
		boolean bool = true;
		if (!((Class51) this).aClass317_487.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), -89900344))
			bool = false;
		if (!((Class51) this).aClass317_486.method5661(((((Class51) this).aClass62_485.anInt658) * -1949174501), 2010922736))
			bool = false;
		return bool;
	}

	static void method1067(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 2] = (Class409.aClass242_4922.method4156((((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 - 2]), -1396181317).method4104(Class158_Sub1.aClass3_8507, (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 1]), (short) 32683)) ? 1 : 0;
		((Class527) class527).anInt7012 -= 141891001;
	}

	static void method1068(Class282_Sub50_Sub5 class282_sub50_sub5, int i, Class527 class527, int i_6_) {
		((Class527) class527).anInt7012 = 0;
		((Class527) class527).anInt7000 = 0;
		((Class527) class527).anInt7020 = 1051529003;
		((Class527) class527).aClass282_Sub50_Sub5_7021 = class282_sub50_sub5;
		((Class527) class527).aClass522Array7005 = (((Class527) class527).aClass282_Sub50_Sub5_7021.aClass522Array9521);
		((Class527) class527).anIntArray7018 = ((Class527) class527).aClass282_Sub50_Sub5_7021.anIntArray9522;
		Class522 class522 = null;
		((Class527) class527).anInt7002 = 0;
		try {
			try {
				Class517.anInt5904 = 0;
				for (;;) {
					Class517.anInt5904 += -1084131875;
					if (Class517.anInt5904 * -1176728971 > i)
						throw new RuntimeException("");
					class522 = (((Class527) class527).aClass522Array7005[(((Class527) class527).anInt7020 += -1051529003) * 301123709]);
					if (Class517.aBool5898 && (null == Class517.aString5897 || (((((Class527) class527).aClass282_Sub50_Sub5_7021.aString9520) != null) && ((Class527) class527).aClass282_Sub50_Sub5_7021.aString9520.indexOf(Class517.aString5897) != -1)))
						System.out.println(new StringBuilder().append(((Class527) class527).aClass282_Sub50_Sub5_7021.aString9520).append(": ").append(class522).toString());
					if (1 == (((Class527) class527).anIntArray7018[301123709 * ((Class527) class527).anInt7020]))
						((Class527) class527).aBool7022 = true;
					else
						((Class527) class527).aBool7022 = false;
					if (Class522.aClass522_5959 == class522 && 0 == (((Class527) class527).anInt7002 * -1837903909)) {
						Class282_Sub50_Sub17.method15509(1969627147);
						break;
					}
					Class174.method2957(class522, class527, -1241831264);
				}
			} catch (Exception exception) {
				StringBuilder stringbuilder = new StringBuilder(30);
				stringbuilder.append("").append(-3442165056282524525L * (((Class527) class527).aClass282_Sub50_Sub5_7021.aLong3379)).append(" ");
				for (int i_7_ = ((Class527) class527).anInt7002 * -1837903909 - 1; i_7_ >= 0; i_7_--)
					stringbuilder.append("").append(-3442165056282524525L * (((Class509) ((Class527) class527).aClass509Array7016[i_7_]).aClass282_Sub50_Sub5_5869.aLong3379)).append(" ");
				stringbuilder.append("").append(Integer.valueOf(-2026890351 * class522.anInt6952));
				Class151.method2594(stringbuilder.toString(), exception, (byte) -32);
				Class282_Sub50_Sub17.method15509(1831526496);
			}
		} catch (Exception object) {
			Class282_Sub50_Sub17.method15509(105300500);
			throw object;
		}
	}

	static final void method1069(Class527 class527, int i) {
		int i_8_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_8_, (byte) 52);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_8_ >> 16];
		Class282_Sub52.method13468(class118, class98, class527, 769396750);
	}

	static final void method1070(Class527 class527, byte i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		boolean bool = ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1);
		Class76.method1360(string, bool, (byte) 1);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 560339485 * Class415.anInt4985;
	}

	static final void method1071(Class527 class527, int i) {
		Class244.method4195(-1158616482);
	}

	public static int method1072(int i, int i_9_) {
		int i_10_ = i >>> 1;
		i_10_ |= i_10_ >>> 1;
		i_10_ |= i_10_ >>> 2;
		i_10_ |= i_10_ >>> 4;
		i_10_ |= i_10_ >>> 8;
		i_10_ |= i_10_ >>> 16;
		return i & (i_10_ ^ 0xffffffff);
	}

	static final void method1073(Class527 class527, byte i) {
		int i_11_ = (((Class527) class527).anIntArray7018[((Class527) class527).anInt7020 * 301123709]);
		Class158_Sub1.aClass3_8507.method266(i_11_, (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]), -1744724277);
	}

	public static void method1074(int[] is, Object[] objects, int i, int i_12_, int i_13_) {
		if (i < i_12_) {
			int i_14_ = (i + i_12_) / 2;
			int i_15_ = i;
			int i_16_ = is[i_14_];
			is[i_14_] = is[i_12_];
			is[i_12_] = i_16_;
			Object object = objects[i_14_];
			objects[i_14_] = objects[i_12_];
			objects[i_12_] = object;
			int i_17_ = i_16_ == 2147483647 ? 0 : 1;
			for (int i_18_ = i; i_18_ < i_12_; i_18_++) {
				if (is[i_18_] < (i_18_ & i_17_) + i_16_) {
					int i_19_ = is[i_18_];
					is[i_18_] = is[i_15_];
					is[i_15_] = i_19_;
					Object object_20_ = objects[i_18_];
					objects[i_18_] = objects[i_15_];
					objects[i_15_++] = object_20_;
				}
			}
			is[i_12_] = is[i_15_];
			is[i_15_] = i_16_;
			objects[i_12_] = objects[i_15_];
			objects[i_15_] = object;
			method1074(is, objects, i, i_15_ - 1, 1230718276);
			method1074(is, objects, 1 + i_15_, i_12_, -1167576845);
		}
	}
}
