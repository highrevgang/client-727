/* Class96_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class96_Sub13 extends Class96 {
	int anInt9363;
	int anInt9364;
	int anInt9365;
	int anInt9366;
	Class268 aClass268_9367;
	public static int anInt9368;

	void method1597() {
		if (null != ((Class96_Sub13) this).aClass268_9367) {
			Class58.method1142(((Class96_Sub13) this).aClass268_9367, 1573913718);
			((Class96_Sub13) this).aClass268_9367 = null;
		}
	}

	public void method1592(int i) {
		((Class96_Sub13) this).aClass268_9367 = (Class153.method2618(-1658024425 * ((Class96_Sub13) this).anInt9363, 1033987545 * ((Class96_Sub13) this).anInt9366, 0, 183603537 * ((Class96_Sub13) this).anInt9365, ((Class96_Sub13) this).anInt9364 * -1180265103, 1753039442));
	}

	void method1593(byte i) {
		if (null != ((Class96_Sub13) this).aClass268_9367) {
			Class58.method1142(((Class96_Sub13) this).aClass268_9367, 1890417673);
			((Class96_Sub13) this).aClass268_9367 = null;
		}
	}

	public void method1601() {
		((Class96_Sub13) this).aClass268_9367 = (Class153.method2618(-1658024425 * ((Class96_Sub13) this).anInt9363, 1033987545 * ((Class96_Sub13) this).anInt9366, 0, 183603537 * ((Class96_Sub13) this).anInt9365, ((Class96_Sub13) this).anInt9364 * -1180265103, 1758960658));
	}

	void method1598() {
		if (null != ((Class96_Sub13) this).aClass268_9367) {
			Class58.method1142(((Class96_Sub13) this).aClass268_9367, 1337360009);
			((Class96_Sub13) this).aClass268_9367 = null;
		}
	}

	Class96_Sub13(RsByteBuffer class282_sub35) {
		super(class282_sub35);
		((Class96_Sub13) this).anInt9363 = class282_sub35.readUnsignedShort() * -614233689;
		((Class96_Sub13) this).anInt9365 = class282_sub35.readUnsignedByte() * -2028447823;
		((Class96_Sub13) this).anInt9364 = class282_sub35.readUnsignedByte() * -1995338863;
		((Class96_Sub13) this).anInt9366 = class282_sub35.readUnsignedByte() * 468765289;
	}

	void method1595() {
		if (null != ((Class96_Sub13) this).aClass268_9367) {
			Class58.method1142(((Class96_Sub13) this).aClass268_9367, 1930258790);
			((Class96_Sub13) this).aClass268_9367 = null;
		}
	}

	static final void method14639(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = null != Class46.anObjectArray437 ? 1 : 0;
	}

	static final void method14640(Class527 class527, int i) {
		int i_0_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_0_, (byte) 36);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_0_ >> 16];
		Class215.method3672(class118, class98, class527, (short) 9324);
	}
}
