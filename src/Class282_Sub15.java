/* Class282_Sub15 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class282_Sub15 extends Class282 {
	Class282_Sub15 aClass282_Sub15_7598;
	int anInt7599;
	volatile boolean aBool7600 = true;
	Class282_Sub26 aClass282_Sub26_7601;

	abstract int method12224();

	int method12225() {
		return 255;
	}

	abstract Class282_Sub15 method12226();

	abstract int method12227();

	abstract int method12228();

	abstract Class282_Sub15 method12229();

	abstract void method12230(int[] is, int i, int i_0_);

	abstract void method12231(int i);

	abstract Class282_Sub15 method12232();

	abstract Class282_Sub15 method12233();

	abstract void method12234(int i);

	abstract Class282_Sub15 method12235();

	abstract Class282_Sub15 method12236();

	Class282_Sub15() {
		/* empty */
	}

	final void method12237(int[] is, int i, int i_1_) {
		if (((Class282_Sub15) this).aBool7600)
			method12230(is, i, i_1_);
		else
			method12231(i_1_);
	}

	abstract int method12238();

	abstract Class282_Sub15 method12239();

	abstract void method12240(int[] is, int i, int i_2_);

	abstract void method12241(int[] is, int i, int i_3_);

	abstract void method12242(int[] is, int i, int i_4_);

	abstract void method12243(int i);

	abstract int method12244();

	int method12245() {
		return 255;
	}

	int method12246() {
		return 255;
	}

	int method12247() {
		return 255;
	}
}
