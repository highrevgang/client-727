/* Class148 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class148 {
	static int anInt1730 = 0;
	static Class317 aClass317_1731;
	static Class317 aClass317_1732;
	static final int anInt1733 = 1;
	static final int anInt1734 = 0;
	static Class282_Sub15_Sub2 aClass282_Sub15_Sub2_1735;
	static final int anInt1736 = 2;
	static Class317 aClass317_1737;
	static int anInt1738;
	static final int anInt1739 = 3;
	static long aLong1740;

	static void method2510(Class317 class317, int i, int i_0_, int i_1_, boolean bool, long l) {
		Class109.method1859(class317, i, i_0_, i_1_, bool, l, 0, 1673929006);
	}

	public static void method2511(Class116 class116, int i) {
		aClass282_Sub15_Sub2_1735.method15101((short) -11380);
		Class282_Sub15_Sub2 class282_sub15_sub2 = class116.method1954(1571931007);
		if (null != class282_sub15_sub2) {
			Class502.aClass253_5830.method4334();
			aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
			aClass282_Sub15_Sub2_1735.method15098(class116.method1955(-1170880776), false, 1786497679);
			aClass282_Sub15_Sub2_1735.method15144(i, -1179707974);
			if (Class502.aClass253_5830 != null)
				Class502.aClass253_5830.method4329(aClass282_Sub15_Sub2_1735);
			Class332.method5929(-1613522406);
		}
	}

	public static void method2512(int i) {
		anInt1730 = 800770715;
		Class75.aClass317_746 = null;
		Class6.anInt46 = 26560111;
		anInt1738 = -275076647;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
		Class158_Sub2_Sub3.anInt10243 = 0;
		Class152.aBool1962 = false;
		Class96_Sub22.anInt9440 = -465842921 * i;
		Class11.aClass109_121 = null;
	}

	public static boolean method2513() {
		return 0 != anInt1730 * -1423242349;
	}

	public static void method2514() {
		try {
			if (1 == anInt1730 * -1423242349) {
				int i = aClass282_Sub15_Sub2_1735.method15123(-1615520117);
				if (i > 0 && aClass282_Sub15_Sub2_1735.method15103(1583037658)) {
					i -= -1553319257 * Class96_Sub22.anInt9440;
					if (i < 0)
						i = 0;
					aClass282_Sub15_Sub2_1735.method15144(i, -996859673);
					return;
				}
				aClass282_Sub15_Sub2_1735.method15101((short) 2205);
				aClass282_Sub15_Sub2_1735.method15097((short) 256);
				if (null != Class75.aClass317_746)
					anInt1730 = 1601541430;
				else
					anInt1730 = 0;
				Class282_Sub44_Sub3.aClass282_Sub7_9563 = null;
				Class454.aClass250_5450 = null;
			}
			if (3 == -1423242349 * anInt1730) {
				int i = aClass282_Sub15_Sub2_1735.method15123(-1480444065);
				if (i < -609094685 * Class158_Sub2_Sub3.anInt10243 && aClass282_Sub15_Sub2_1735.method15103(1926102505)) {
					i += -2027356711 * Class383.anInt4664;
					if (i > Class158_Sub2_Sub3.anInt10243 * -609094685)
						i = -609094685 * Class158_Sub2_Sub3.anInt10243;
					aClass282_Sub15_Sub2_1735.method15144(i, -82481539);
				} else {
					Class383.anInt4664 = 0;
					anInt1730 = 0;
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			aClass282_Sub15_Sub2_1735.method15101((short) -10034);
			Class332.method5929(-1596521090);
		}
	}

	static void method2515(Class317 class317, int i, int i_2_, int i_3_, boolean bool, long l) {
		Class109.method1859(class317, i, i_2_, i_3_, bool, l, 0, 1766696722);
	}

	static void method2516(Class317 class317, int i, int i_4_, int i_5_, boolean bool, long l, int i_6_) {
		anInt1730 = 800770715;
		Class75.aClass317_746 = class317;
		Class6.anInt46 = -26560111 * i;
		anInt1738 = 275076647 * i_4_;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
		Class158_Sub2_Sub3.anInt10243 = 965667275 * i_5_;
		Class152.aBool1962 = bool;
		Class96_Sub22.anInt9440 = 1610306160;
		aLong1740 = l * 5207426305341619623L;
		Class383.anInt4664 = 482849385 * i_6_;
	}

	static void method2517(Class317 class317, int i, int i_7_, int i_8_, boolean bool, long l) {
		Class109.method1859(class317, i, i_7_, i_8_, bool, l, 0, 1766745956);
	}

	public static boolean method2518() {
		do {
			boolean bool;
			try {
				if (-1423242349 * anInt1730 != 2)
					break;
				if (Class282_Sub44_Sub3.aClass282_Sub7_9563 == null) {
					Class282_Sub44_Sub3.aClass282_Sub7_9563 = Class282_Sub7.method12164(Class75.aClass317_746, (-282122383 * Class6.anInt46), -230114921 * anInt1738);
					if (Class282_Sub44_Sub3.aClass282_Sub7_9563 == null)
						return false;
				}
				if (null == Class454.aClass250_5450)
					Class454.aClass250_5450 = new Class250(aClass317_1732, aClass317_1731);
				Class282_Sub15_Sub2 class282_sub15_sub2 = aClass282_Sub15_Sub2_1735;
				if (Class282_Sub33.aClass282_Sub15_Sub2_7836 != null)
					class282_sub15_sub2 = Class282_Sub33.aClass282_Sub15_Sub2_7836;
				if (!class282_sub15_sub2.method15182((Class282_Sub44_Sub3.aClass282_Sub7_9563), aClass317_1737, Class454.aClass250_5450, 22050, -1724384723))
					break;
				aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
				aClass282_Sub15_Sub2_1735.method15096(700253864);
				if (-2027356711 * Class383.anInt4664 > 0) {
					anInt1730 = -1892655151;
					aClass282_Sub15_Sub2_1735.method15144(((Class383.anInt4664 * -2027356711 > -609094685 * Class158_Sub2_Sub3.anInt10243) ? -609094685 * Class158_Sub2_Sub3.anInt10243 : Class383.anInt4664 * -2027356711), -746537563);
					for (int i = 0; i < Class453.anIntArray5449.length; i++) {
						aClass282_Sub15_Sub2_1735.method15095(i, (Class453.anIntArray5449[i]), 1732274584);
						Class453.anIntArray5449[i] = 255;
					}
				} else {
					anInt1730 = 0;
					aClass282_Sub15_Sub2_1735.method15144(-609094685 * Class158_Sub2_Sub3.anInt10243, -1315549929);
					for (int i = 0; i < Class453.anIntArray5449.length; i++) {
						aClass282_Sub15_Sub2_1735.method15095(i, (Class453.anIntArray5449[i]), 1872782791);
						Class453.anIntArray5449[i] = 255;
					}
				}
				if (null == Class282_Sub33.aClass282_Sub15_Sub2_7836) {
					if (-8843131616667674089L * aLong1740 > 0L)
						aClass282_Sub15_Sub2_1735.method15100(Class282_Sub44_Sub3.aClass282_Sub7_9563, Class152.aBool1962, true, -8843131616667674089L * aLong1740);
					else
						aClass282_Sub15_Sub2_1735.method15098(Class282_Sub44_Sub3.aClass282_Sub7_9563, Class152.aBool1962, 1287477455);
					if (Class11.aClass109_121 != null)
						Class11.aClass109_121.method1849((byte) -67);
				}
				if (Class502.aClass253_5830 != null)
					Class502.aClass253_5830.method4329(aClass282_Sub15_Sub2_1735);
				Class282_Sub44_Sub3.aClass282_Sub7_9563 = null;
				Class454.aClass250_5450 = null;
				Class75.aClass317_746 = null;
				aLong1740 = 0L;
				Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
				Class11.aClass109_121 = null;
				bool = true;
			} catch (Exception exception) {
				exception.printStackTrace();
				aClass282_Sub15_Sub2_1735.method15101((short) 5525);
				Class332.method5929(1603007658);
				break;
			}
			return bool;
		} while (false);
		return false;
	}

	static void method2519(Class317 class317, int i, int i_9_, int i_10_, boolean bool, long l, int i_11_) {
		anInt1730 = 800770715;
		Class75.aClass317_746 = class317;
		Class6.anInt46 = -26560111 * i;
		anInt1738 = 275076647 * i_9_;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
		Class158_Sub2_Sub3.anInt10243 = 965667275 * i_10_;
		Class152.aBool1962 = bool;
		Class96_Sub22.anInt9440 = 1610306160;
		aLong1740 = l * 5207426305341619623L;
		Class383.anInt4664 = 482849385 * i_11_;
	}

	Class148() throws Throwable {
		throw new Error();
	}

	public static void method2520() {
		aClass282_Sub15_Sub2_1735.method15101((short) 11719);
		anInt1730 = 800770715;
		Class75.aClass317_746 = null;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
	}

	public static void method2521(int i) {
		if (0 != -1423242349 * anInt1730)
			Class158_Sub2_Sub3.anInt10243 = i * 965667275;
		else
			aClass282_Sub15_Sub2_1735.method15144(i, 1530151875);
	}

	public static void method2522(int i, int i_12_) {
		if (anInt1730 * -1423242349 != 0) {
			if (i < 0) {
				for (int i_13_ = 0; i_13_ < 16; i_13_++)
					Class453.anIntArray5449[i_13_] = i_12_;
			} else
				Class453.anIntArray5449[i] = i_12_;
		}
		aClass282_Sub15_Sub2_1735.method15095(i, i_12_, 2118579711);
	}

	public static void method2523(int i, int i_14_) {
		if (anInt1730 * -1423242349 != 0) {
			if (i < 0) {
				for (int i_15_ = 0; i_15_ < 16; i_15_++)
					Class453.anIntArray5449[i_15_] = i_14_;
			} else
				Class453.anIntArray5449[i] = i_14_;
		}
		aClass282_Sub15_Sub2_1735.method15095(i, i_14_, 1168947417);
	}

	public static void method2524(int i, int i_16_) {
		if (anInt1730 * -1423242349 != 0) {
			if (i < 0) {
				for (int i_17_ = 0; i_17_ < 16; i_17_++)
					Class453.anIntArray5449[i_17_] = i_16_;
			} else
				Class453.anIntArray5449[i] = i_16_;
		}
		aClass282_Sub15_Sub2_1735.method15095(i, i_16_, 1073182697);
	}

	public static void method2525() {
		aClass282_Sub15_Sub2_1735.method15101((short) 3902);
		anInt1730 = 800770715;
		Class75.aClass317_746 = null;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
	}

	public static void method2526(Class116 class116, int i) {
		aClass282_Sub15_Sub2_1735.method15101((short) -835);
		Class282_Sub15_Sub2 class282_sub15_sub2 = class116.method1954(1398225943);
		if (null != class282_sub15_sub2) {
			Class502.aClass253_5830.method4334();
			aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
			aClass282_Sub15_Sub2_1735.method15098(class116.method1955(529937324), false, -86422750);
			aClass282_Sub15_Sub2_1735.method15144(i, -21848333);
			if (Class502.aClass253_5830 != null)
				Class502.aClass253_5830.method4329(aClass282_Sub15_Sub2_1735);
			Class332.method5929(-978959652);
		}
	}

	public static void method2527(int i, Class317 class317, int i_18_, int i_19_, int i_20_, boolean bool) {
		anInt1730 = 800770715;
		Class75.aClass317_746 = class317;
		Class6.anInt46 = -26560111 * i_18_;
		anInt1738 = 275076647 * i_19_;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
		Class158_Sub2_Sub3.anInt10243 = 965667275 * i_20_;
		Class152.aBool1962 = bool;
		Class96_Sub22.anInt9440 = -465842921 * i;
		Class11.aClass109_121 = null;
	}

	public static void method2528(int i, Class317 class317, int i_21_, int i_22_, int i_23_, boolean bool) {
		anInt1730 = 800770715;
		Class75.aClass317_746 = class317;
		Class6.anInt46 = -26560111 * i_21_;
		anInt1738 = 275076647 * i_22_;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
		Class158_Sub2_Sub3.anInt10243 = 965667275 * i_23_;
		Class152.aBool1962 = bool;
		Class96_Sub22.anInt9440 = -465842921 * i;
		Class11.aClass109_121 = null;
	}

	public static void method2529() {
		try {
			if (1 == anInt1730 * -1423242349) {
				int i = aClass282_Sub15_Sub2_1735.method15123(-1677375400);
				if (i > 0 && aClass282_Sub15_Sub2_1735.method15103(1716624216)) {
					i -= -1553319257 * Class96_Sub22.anInt9440;
					if (i < 0)
						i = 0;
					aClass282_Sub15_Sub2_1735.method15144(i, -972781227);
					return;
				}
				aClass282_Sub15_Sub2_1735.method15101((short) -3533);
				aClass282_Sub15_Sub2_1735.method15097((short) 256);
				if (null != Class75.aClass317_746)
					anInt1730 = 1601541430;
				else
					anInt1730 = 0;
				Class282_Sub44_Sub3.aClass282_Sub7_9563 = null;
				Class454.aClass250_5450 = null;
			}
			if (3 == -1423242349 * anInt1730) {
				int i = aClass282_Sub15_Sub2_1735.method15123(-2138329401);
				if (i < -609094685 * Class158_Sub2_Sub3.anInt10243 && aClass282_Sub15_Sub2_1735.method15103(1767869668)) {
					i += -2027356711 * Class383.anInt4664;
					if (i > Class158_Sub2_Sub3.anInt10243 * -609094685)
						i = -609094685 * Class158_Sub2_Sub3.anInt10243;
					aClass282_Sub15_Sub2_1735.method15144(i, 1886030097);
				} else {
					Class383.anInt4664 = 0;
					anInt1730 = 0;
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			aClass282_Sub15_Sub2_1735.method15101((short) -20527);
			Class332.method5929(-555636689);
		}
	}

	public static void method2530(int i, Class317 class317, int i_24_, int i_25_, int i_26_, boolean bool, Class109 class109) {
		if (i > 0) {
			anInt1730 = 800770715;
			Class75.aClass317_746 = class317;
			Class6.anInt46 = i_24_ * -26560111;
			anInt1738 = i_25_ * 275076647;
			Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
			Class158_Sub2_Sub3.anInt10243 = i_26_ * 965667275;
			Class152.aBool1962 = bool;
			Class96_Sub22.anInt9440 = (aClass282_Sub15_Sub2_1735.method15123(-1961593511) / i * -465842921);
			if (Class96_Sub22.anInt9440 * -1553319257 < 1)
				Class96_Sub22.anInt9440 = -465842921;
			Class11.aClass109_121 = class109;
		} else {
			if (null != class109)
				class109.method1849((byte) -81);
			Class282_Sub43.method13400(class317, i_24_, i_25_, i_26_, bool, (byte) -46);
		}
	}

	public static boolean method2531() {
		if (0 != -1423242349 * anInt1730)
			return true;
		return aClass282_Sub15_Sub2_1735.method15103(2107448185);
	}

	public static boolean method2532() {
		if (0 != -1423242349 * anInt1730)
			return true;
		return aClass282_Sub15_Sub2_1735.method15103(1807786822);
	}

	public static boolean method2533() {
		if (0 != -1423242349 * anInt1730)
			return true;
		return aClass282_Sub15_Sub2_1735.method15103(1631791587);
	}

	public static boolean method2534() {
		if (0 != -1423242349 * anInt1730)
			return true;
		return aClass282_Sub15_Sub2_1735.method15103(1512071728);
	}

	public static void method2535(int i) {
		if (0 != -1423242349 * anInt1730)
			Class158_Sub2_Sub3.anInt10243 = i * 965667275;
		else
			aClass282_Sub15_Sub2_1735.method15144(i, -1233994512);
	}

	public static void method2536() {
		try {
			if (1 == anInt1730 * -1423242349) {
				int i = aClass282_Sub15_Sub2_1735.method15123(-1819887541);
				if (i > 0 && aClass282_Sub15_Sub2_1735.method15103(1651034459)) {
					i -= -1553319257 * Class96_Sub22.anInt9440;
					if (i < 0)
						i = 0;
					aClass282_Sub15_Sub2_1735.method15144(i, 13103808);
					return;
				}
				aClass282_Sub15_Sub2_1735.method15101((short) 20377);
				aClass282_Sub15_Sub2_1735.method15097((short) 256);
				if (null != Class75.aClass317_746)
					anInt1730 = 1601541430;
				else
					anInt1730 = 0;
				Class282_Sub44_Sub3.aClass282_Sub7_9563 = null;
				Class454.aClass250_5450 = null;
			}
			if (3 == -1423242349 * anInt1730) {
				int i = aClass282_Sub15_Sub2_1735.method15123(-1559680236);
				if (i < -609094685 * Class158_Sub2_Sub3.anInt10243 && aClass282_Sub15_Sub2_1735.method15103(1875173024)) {
					i += -2027356711 * Class383.anInt4664;
					if (i > Class158_Sub2_Sub3.anInt10243 * -609094685)
						i = -609094685 * Class158_Sub2_Sub3.anInt10243;
					aClass282_Sub15_Sub2_1735.method15144(i, -309722390);
				} else {
					Class383.anInt4664 = 0;
					anInt1730 = 0;
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			aClass282_Sub15_Sub2_1735.method15101((short) -17263);
			Class332.method5929(-952690298);
		}
	}

	static void method2537() {
		anInt1730 = 0;
		Class282_Sub44_Sub3.aClass282_Sub7_9563 = null;
		Class454.aClass250_5450 = null;
		Class75.aClass317_746 = null;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
	}

	public static void method2538(Class317 class317, int i, int i_27_, int i_28_, boolean bool, Class282_Sub15_Sub2 class282_sub15_sub2) {
		Class282_Sub43.method13400(class317, i, i_27_, i_28_, bool, (byte) -96);
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = class282_sub15_sub2;
	}

	public static boolean method2539() {
		do {
			boolean bool;
			try {
				if (-1423242349 * anInt1730 != 2)
					break;
				if (Class282_Sub44_Sub3.aClass282_Sub7_9563 == null) {
					Class282_Sub44_Sub3.aClass282_Sub7_9563 = Class282_Sub7.method12164(Class75.aClass317_746, (-282122383 * Class6.anInt46), -230114921 * anInt1738);
					if (Class282_Sub44_Sub3.aClass282_Sub7_9563 == null)
						return false;
				}
				if (null == Class454.aClass250_5450)
					Class454.aClass250_5450 = new Class250(aClass317_1732, aClass317_1731);
				Class282_Sub15_Sub2 class282_sub15_sub2 = aClass282_Sub15_Sub2_1735;
				if (Class282_Sub33.aClass282_Sub15_Sub2_7836 != null)
					class282_sub15_sub2 = Class282_Sub33.aClass282_Sub15_Sub2_7836;
				if (!class282_sub15_sub2.method15182((Class282_Sub44_Sub3.aClass282_Sub7_9563), aClass317_1737, Class454.aClass250_5450, 22050, 1992586794))
					break;
				aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
				aClass282_Sub15_Sub2_1735.method15096(952184772);
				if (-2027356711 * Class383.anInt4664 > 0) {
					anInt1730 = -1892655151;
					aClass282_Sub15_Sub2_1735.method15144(((Class383.anInt4664 * -2027356711 > -609094685 * Class158_Sub2_Sub3.anInt10243) ? -609094685 * Class158_Sub2_Sub3.anInt10243 : Class383.anInt4664 * -2027356711), -1041675640);
					for (int i = 0; i < Class453.anIntArray5449.length; i++) {
						aClass282_Sub15_Sub2_1735.method15095(i, (Class453.anIntArray5449[i]), 1947959882);
						Class453.anIntArray5449[i] = 255;
					}
				} else {
					anInt1730 = 0;
					aClass282_Sub15_Sub2_1735.method15144(-609094685 * Class158_Sub2_Sub3.anInt10243, 247451202);
					for (int i = 0; i < Class453.anIntArray5449.length; i++) {
						aClass282_Sub15_Sub2_1735.method15095(i, (Class453.anIntArray5449[i]), 1939102811);
						Class453.anIntArray5449[i] = 255;
					}
				}
				if (null == Class282_Sub33.aClass282_Sub15_Sub2_7836) {
					if (-8843131616667674089L * aLong1740 > 0L)
						aClass282_Sub15_Sub2_1735.method15100(Class282_Sub44_Sub3.aClass282_Sub7_9563, Class152.aBool1962, true, -8843131616667674089L * aLong1740);
					else
						aClass282_Sub15_Sub2_1735.method15098(Class282_Sub44_Sub3.aClass282_Sub7_9563, Class152.aBool1962, -272044958);
					if (Class11.aClass109_121 != null)
						Class11.aClass109_121.method1849((byte) -31);
				}
				if (Class502.aClass253_5830 != null)
					Class502.aClass253_5830.method4329(aClass282_Sub15_Sub2_1735);
				Class282_Sub44_Sub3.aClass282_Sub7_9563 = null;
				Class454.aClass250_5450 = null;
				Class75.aClass317_746 = null;
				aLong1740 = 0L;
				Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
				Class11.aClass109_121 = null;
				bool = true;
			} catch (Exception exception) {
				exception.printStackTrace();
				aClass282_Sub15_Sub2_1735.method15101((short) 14719);
				Class332.method5929(-1522206883);
				break;
			}
			return bool;
		} while (false);
		return false;
	}

	public static void method2540(Class116 class116, int i) {
		aClass282_Sub15_Sub2_1735.method15101((short) -4041);
		Class282_Sub15_Sub2 class282_sub15_sub2 = class116.method1954(1898479293);
		if (null != class282_sub15_sub2) {
			Class502.aClass253_5830.method4334();
			aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
			aClass282_Sub15_Sub2_1735.method15098(class116.method1955(1785648580), false, 1371700032);
			aClass282_Sub15_Sub2_1735.method15144(i, 1140765610);
			if (Class502.aClass253_5830 != null)
				Class502.aClass253_5830.method4329(aClass282_Sub15_Sub2_1735);
			Class332.method5929(1776020881);
		}
	}

	public static void method2541(Class116 class116, int i) {
		aClass282_Sub15_Sub2_1735.method15101((short) -16064);
		Class282_Sub15_Sub2 class282_sub15_sub2 = class116.method1954(510488506);
		if (null != class282_sub15_sub2) {
			Class502.aClass253_5830.method4334();
			aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
			aClass282_Sub15_Sub2_1735.method15098(class116.method1955(-773736799), false, 628024770);
			aClass282_Sub15_Sub2_1735.method15144(i, 899694733);
			if (Class502.aClass253_5830 != null)
				Class502.aClass253_5830.method4329(aClass282_Sub15_Sub2_1735);
			Class332.method5929(733638950);
		}
	}

	public static void method2542(int i) {
		if (0 != -1423242349 * anInt1730)
			Class158_Sub2_Sub3.anInt10243 = i * 965667275;
		else
			aClass282_Sub15_Sub2_1735.method15144(i, -164437163);
	}

	public static boolean method2543(Class317 class317, Class317 class317_29_, Class317 class317_30_, Class282_Sub15_Sub2 class282_sub15_sub2, Class253 class253) {
		aClass317_1737 = class317;
		aClass317_1731 = class317_29_;
		aClass317_1732 = class317_30_;
		aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
		Class502.aClass253_5830 = class253;
		Class453.anIntArray5449 = new int[16];
		for (int i = 0; i < 16; i++)
			Class453.anIntArray5449[i] = 255;
		return true;
	}

	public static void method2544(Class317 class317, int i, int i_31_, int i_32_, boolean bool) {
		Class271.method4827(class317, i, i_31_, i_32_, bool, 0L);
	}

	static void method2545() {
		anInt1730 = 0;
		Class282_Sub44_Sub3.aClass282_Sub7_9563 = null;
		Class454.aClass250_5450 = null;
		Class75.aClass317_746 = null;
		Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
	}

	public static Class282_Sub15_Sub2 method2546() {
		return aClass282_Sub15_Sub2_1735;
	}

	public static Class282_Sub15_Sub2 method2547() {
		return aClass282_Sub15_Sub2_1735;
	}

	public static Class282_Sub15_Sub2 method2548() {
		return aClass282_Sub15_Sub2_1735;
	}

	static final void method2549(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class383.method6513(Class393.aClass282_Sub54_4783.aClass468_Sub18_8230.method12776(742990495), 200, 804853307);
	}

	public static String method2550(int i, int i_33_) {
		Class282_Sub37 class282_sub37 = (Class282_Sub37) Class492.aClass465_5774.method7754((long) i);
		if (class282_sub37 != null) {
			Class282_Sub41_Sub2 class282_sub41_sub2 = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4935(-1962983901);
			if (class282_sub41_sub2 != null) {
				double d = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4923(1997080101);
				if ((double) class282_sub41_sub2.method14702(588000346) <= d && (double) class282_sub41_sub2.method14705((byte) 5) >= d)
					return class282_sub41_sub2.method14704(-1724339640);
			}
		}
		return null;
	}

	public static Class225 method2551(int i, int i_34_, int i_35_, int i_36_, Class458 class458, int i_37_, int i_38_) {
		Class224.aClass225_Sub5_2775.anInt2779 = -726719813 * i;
		Class224.aClass225_Sub5_2775.anInt2780 = i_34_ * 1767388707;
		Class224.aClass225_Sub5_2775.anInt2781 = -12808295 * i_35_;
		Class224.aClass225_Sub5_2775.anInt2782 = i_36_ * 1709796035;
		((Class225_Sub5) Class224.aClass225_Sub5_2775).aClass458_7972 = class458;
		((Class225_Sub5) Class224.aClass225_Sub5_2775).anInt7971 = -845112339 * i_37_;
		return Class224.aClass225_Sub5_2775;
	}

	static final void method2552(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = client.anInt7190 * -1474737961;
	}

	static Class method2553(String string, short i) throws ClassNotFoundException {
		if (string.equals("B"))
			return Byte.TYPE;
		if (string.equals("I"))
			return Integer.TYPE;
		if (string.equals("S"))
			return Short.TYPE;
		if (string.equals("J"))
			return Long.TYPE;
		if (string.equals("Z"))
			return Boolean.TYPE;
		if (string.equals("F"))
			return Float.TYPE;
		if (string.equals("D"))
			return Double.TYPE;
		if (string.equals("C"))
			return Character.TYPE;
		if (string.equals("void"))
			return Void.TYPE;
		return Class.forName(string);
	}

	static final void method2554(Class527 class527, byte i) {
		int i_39_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		int i_40_ = Class393.aClass282_Sub54_4783.aClass468_Sub13_8228.method12714(1677203613);
		if (i_39_ != i_40_) {
			if (Class169.method2875(-1741204137 * client.anInt7166, -2096796884)) {
				if (0 == i_40_ && Class260.anInt3223 * 1293234709 != -1) {
					Class282_Sub43.method13400(Class512.aClass317_5884, 1293234709 * Class260.anInt3223, 0, i_39_, false, (byte) -40);
					Class468_Sub6.method12658(1298761444);
					Class260.aBool3220 = false;
				} else if (i_39_ == 0) {
					Class226.method3805(-1719520107);
					Class260.aBool3220 = false;
				} else
					Class87.method1491(i_39_, (byte) -25);
			}
			Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub13_8228), i_39_, 1268885388);
			Class190.method3148((byte) 29);
			client.aBool7175 = false;
		}
	}
}
