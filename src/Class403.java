/* Class403 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class403 {
	public static final long aLong4829 = 94608000L;

	Class403() throws Throwable {
		throw new Error();
	}

	public static Class336 method6801(int i, int i_0_, byte i_1_) {
		Class336 class336 = new Class336();
		class336.anInt3931 = -892178839;
		class336.anInt3964 = 1315713723;
		((Class336) class336).anInt3965 = 260756261 * (5 + (1 + i));
		((Class336) class336).anInt3966 = (5 + (i_0_ + 1)) * 2015661001;
		class336.anIntArrayArray3922 = (new int[((Class336) class336).anInt3965 * -1208414035][((Class336) class336).anInt3966 * 117800569]);
		class336.method5965((byte) 5);
		return class336;
	}

	static final void method6802(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class237.method3987(class118, class98, class527, -669419549);
	}

	public static void method6803(Class317 class317, Class317 class317_2_, Class317 class317_3_, Class317 class317_4_, int i) {
		Class388.aClass317_4721 = class317;
		Class488.aClass317_5761 = class317_2_;
		Class463.aClass317_5549 = class317_3_;
		Class468_Sub8.aClass98Array7889 = new Class98[Class388.aClass317_4721.method5618(-1686951777)];
		Class218.aBoolArray2709 = new boolean[Class388.aClass317_4721.method5618(-1593209723)];
	}
}
