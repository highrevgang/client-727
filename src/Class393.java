/* Class393 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class393 {
	public Interface3[] anInterface3Array4782;
	public static Class282_Sub54 aClass282_Sub54_4783;

	void method6742(RsByteBuffer class282_sub35) {
		anInterface3Array4782 = new Interface3[class282_sub35.readUnsignedByte()];
		Class60[] class60s = Class112.method1870(-738775405);
		for (int i = 0; i < anInterface3Array4782.length; i++)
			anInterface3Array4782[i] = method6745(class282_sub35, class60s[class282_sub35.readUnsignedByte()], 1487656629);
	}

	void method6743(RsByteBuffer class282_sub35, int i) {
		anInterface3Array4782 = new Interface3[class282_sub35.readUnsignedByte()];
		Class60[] class60s = Class112.method1870(-738775405);
		for (int i_0_ = 0; i_0_ < anInterface3Array4782.length; i_0_++)
			anInterface3Array4782[i_0_] = method6745(class282_sub35, class60s[class282_sub35.readUnsignedByte()], 1487656629);
	}

	void method6744(RsByteBuffer class282_sub35) {
		anInterface3Array4782 = new Interface3[class282_sub35.readUnsignedByte()];
		Class60[] class60s = Class112.method1870(-738775405);
		for (int i = 0; i < anInterface3Array4782.length; i++)
			anInterface3Array4782[i] = method6745(class282_sub35, class60s[class282_sub35.readUnsignedByte()], 1487656629);
	}

	Class393() {
		/* empty */
	}

	Interface3 method6745(RsByteBuffer class282_sub35, Class60 class60, int i) {
		if (Class60.aClass60_609 == class60)
			return Class224.method3784(class282_sub35, 1656057564);
		if (class60 == Class60.aClass60_602)
			return Class411.method6915(class282_sub35, 1563512392);
		if (Class60.aClass60_605 == class60)
			return Class366.method6303(class282_sub35, (byte) 77);
		if (Class60.aClass60_607 == class60)
			return Class530.method11354(class282_sub35, -2136819343);
		if (class60 == Class60.aClass60_606)
			return Class258.method4570(class282_sub35, 2086694291);
		if (Class60.aClass60_603 == class60)
			return Class257.method4564(class282_sub35, 1637673154);
		if (Class60.aClass60_608 == class60)
			return Class163.method2843(class282_sub35, 1348878542);
		if (class60 == Class60.aClass60_604)
			return Class377.method6401(class282_sub35, (byte) -44);
		if (Class60.aClass60_601 == class60)
			return Class214.method3668(class282_sub35, (byte) -11);
		if (Class60.aClass60_610 == class60)
			return Class332.method5928(class282_sub35, -1017315787);
		return null;
	}

	void method6746(RsByteBuffer class282_sub35) {
		anInterface3Array4782 = new Interface3[class282_sub35.readUnsignedByte()];
		Class60[] class60s = Class112.method1870(-738775405);
		for (int i = 0; i < anInterface3Array4782.length; i++)
			anInterface3Array4782[i] = method6745(class282_sub35, class60s[class282_sub35.readUnsignedByte()], 1487656629);
	}

	Interface3 method6747(RsByteBuffer class282_sub35, Class60 class60) {
		if (Class60.aClass60_609 == class60)
			return Class224.method3784(class282_sub35, 1097369728);
		if (class60 == Class60.aClass60_602)
			return Class411.method6915(class282_sub35, 1431347590);
		if (Class60.aClass60_605 == class60)
			return Class366.method6303(class282_sub35, (byte) 17);
		if (Class60.aClass60_607 == class60)
			return Class530.method11354(class282_sub35, -1746753556);
		if (class60 == Class60.aClass60_606)
			return Class258.method4570(class282_sub35, 2126561065);
		if (Class60.aClass60_603 == class60)
			return Class257.method4564(class282_sub35, 359735582);
		if (Class60.aClass60_608 == class60)
			return Class163.method2843(class282_sub35, 165152360);
		if (class60 == Class60.aClass60_604)
			return Class377.method6401(class282_sub35, (byte) -94);
		if (Class60.aClass60_601 == class60)
			return Class214.method3668(class282_sub35, (byte) -30);
		if (Class60.aClass60_610 == class60)
			return Class332.method5928(class282_sub35, 240839813);
		return null;
	}

	Interface3 method6748(RsByteBuffer class282_sub35, Class60 class60) {
		if (Class60.aClass60_609 == class60)
			return Class224.method3784(class282_sub35, 785316199);
		if (class60 == Class60.aClass60_602)
			return Class411.method6915(class282_sub35, 1875424534);
		if (Class60.aClass60_605 == class60)
			return Class366.method6303(class282_sub35, (byte) -42);
		if (Class60.aClass60_607 == class60)
			return Class530.method11354(class282_sub35, -1985369411);
		if (class60 == Class60.aClass60_606)
			return Class258.method4570(class282_sub35, 2109594193);
		if (Class60.aClass60_603 == class60)
			return Class257.method4564(class282_sub35, 2084412066);
		if (Class60.aClass60_608 == class60)
			return Class163.method2843(class282_sub35, 1871368438);
		if (class60 == Class60.aClass60_604)
			return Class377.method6401(class282_sub35, (byte) -98);
		if (Class60.aClass60_601 == class60)
			return Class214.method3668(class282_sub35, (byte) -36);
		if (Class60.aClass60_610 == class60)
			return Class332.method5928(class282_sub35, 207861671);
		return null;
	}

	Interface3 method6749(RsByteBuffer class282_sub35, Class60 class60) {
		if (Class60.aClass60_609 == class60)
			return Class224.method3784(class282_sub35, 2088564103);
		if (class60 == Class60.aClass60_602)
			return Class411.method6915(class282_sub35, 2134208872);
		if (Class60.aClass60_605 == class60)
			return Class366.method6303(class282_sub35, (byte) 25);
		if (Class60.aClass60_607 == class60)
			return Class530.method11354(class282_sub35, -1766225492);
		if (class60 == Class60.aClass60_606)
			return Class258.method4570(class282_sub35, 1991677679);
		if (Class60.aClass60_603 == class60)
			return Class257.method4564(class282_sub35, 1598101013);
		if (Class60.aClass60_608 == class60)
			return Class163.method2843(class282_sub35, -418847969);
		if (class60 == Class60.aClass60_604)
			return Class377.method6401(class282_sub35, (byte) -15);
		if (Class60.aClass60_601 == class60)
			return Class214.method3668(class282_sub35, (byte) -126);
		if (Class60.aClass60_610 == class60)
			return Class332.method5928(class282_sub35, -1905809760);
		return null;
	}

	static final void method6750(Class527 class527, int i) {
		if (1358864261 * Class11.aClass282_Sub51_124.anInt8147 < 6)
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 0;
		else if (6 == Class11.aClass282_Sub51_124.anInt8147 * 1358864261 && -399173307 * Class11.aClass282_Sub51_124.anInt8149 < 10)
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 0;
		else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 1;
	}

	static void method6751(int i, int i_1_, int i_2_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(1, (long) i);
		class282_sub50_sub12.method14995(1483198520);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_1_ * -1773141545;
	}
}
