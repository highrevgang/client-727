/* Class521_Sub1_Sub4_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class521_Sub1_Sub4_Sub2 extends Class521_Sub1_Sub4 implements Interface12 {
	public Class123 aClass123_10501;
	boolean aBool10502;
	Class200 aClass200_10503;
	boolean aBool10504 = true;

	void method13012(Class505 class505) {
		Class528 class528 = aClass123_10501.method2132(class505, 262144, false, true, -1384937123);
		if (class528 != null) {
			Class294 class294 = method11168();
			Class305 class305 = method11166();
			int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
			int i_0_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
			aClass123_10501.method2119(class505, class528, class294, i, i, i_0_, i_0_, false, 481625826);
		}
	}

	boolean method12987(int i) {
		return ((Class521_Sub1_Sub4_Sub2) this).aBool10504;
	}

	public int method77() {
		return ((Class123) aClass123_10501).anInt1555 * 845010167;
	}

	public Class200 method12992(Class505 class505, byte i) {
		return ((Class521_Sub1_Sub4_Sub2) this).aClass200_10503;
	}

	public int method13017() {
		return aClass123_10501.method2115(-2054050626);
	}

	public int method12997(int i) {
		return aClass123_10501.method2121((byte) 1);
	}

	public void method16082(Class476 class476, int i) {
		aClass123_10501.method2116(class476, -786129672);
	}

	Class285 method12990(Class505 class505, int i) {
		Class528 class528 = aClass123_10501.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = class505.method8450();
		class294.method5209(method11168());
		class294.method5219((float) aShort9611, 0.0F, (float) aShort9612);
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub4_Sub2) this).aBool10502, (byte) -19);
		int i_1_ = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_2_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10501.method2119(class505, class528, class294, i_1_, i_1_, i_2_, i_2_, true, 2117778784);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (null != ((Class123) aClass123_10501).aClass539_1538) {
			Class151 class151 = ((Class123) aClass123_10501).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub4_Sub2) this).aBool10504 = (class528.i() || ((Class123) aClass123_10501).aClass539_1538 != null);
		if (((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 == null)
			((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) 64);
		else
			Class388.method6694((((Class521_Sub1_Sub4_Sub2) this).aClass200_10503), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -862136190);
		return class285;
	}

	void method12991(Class505 class505, int i) {
		Class528 class528 = aClass123_10501.method2132(class505, 262144, false, true, -1384937123);
		if (class528 != null) {
			Class294 class294 = method11168();
			Class305 class305 = method11166();
			int i_3_ = (int) class305.aClass385_3595.aFloat4671 >> 9;
			int i_4_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
			aClass123_10501.method2119(class505, class528, class294, i_3_, i_3_, i_4_, i_4_, false, 810474788);
		}
	}

	boolean method12983(Class505 class505, int i, int i_5_, int i_6_) {
		Class528 class528 = aClass123_10501.method2132(class505, 131072, false, false, -1384937123);
		if (null == class528)
			return false;
		return class528.method11270(i, i_5_, method11168(), false, 0);
	}

	void method13023(Class505 class505) {
		Class528 class528 = aClass123_10501.method2132(class505, 262144, false, true, -1384937123);
		if (class528 != null) {
			Class294 class294 = method11168();
			Class305 class305 = method11166();
			int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
			int i_7_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
			aClass123_10501.method2119(class505, class528, class294, i, i, i_7_, i_7_, false, 499948673);
		}
	}

	public int method12995(int i) {
		return aClass123_10501.method2115(-542086847);
	}

	public int method91() {
		return -515017769 * ((Class123) aClass123_10501).anInt1542;
	}

	public void method85(byte i) {
		/* empty */
	}

	public boolean method86(int i) {
		return aClass123_10501.method2117(-48783891);
	}

	public void method87(Class505 class505, int i) {
		aClass123_10501.method2136(class505, (byte) -53);
	}

	public void method88(Class505 class505, int i) {
		aClass123_10501.method2118(class505, 1878976654);
	}

	public int method76() {
		return ((Class123) aClass123_10501).anInt1540 * 1535779425;
	}

	public int method39() {
		return ((Class123) aClass123_10501).anInt1540 * 1535779425;
	}

	public int method73() {
		return ((Class123) aClass123_10501).anInt1555 * 845010167;
	}

	public void method16083(Class476 class476) {
		aClass123_10501.method2116(class476, 1329072422);
	}

	public Class200 method12993(Class505 class505) {
		return ((Class521_Sub1_Sub4_Sub2) this).aClass200_10503;
	}

	public void method38() {
		/* empty */
	}

	public int method92(int i) {
		return -515017769 * ((Class123) aClass123_10501).anInt1542;
	}

	public void method90() {
		/* empty */
	}

	public boolean method94() {
		return aClass123_10501.method2117(-1109544704);
	}

	public Class521_Sub1_Sub4_Sub2(Class206 class206, Class505 class505, Class474 class474, Class478 class478, int i, int i_8_, int i_9_, int i_10_, int i_11_, boolean bool, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_) {
		super(class206, i_9_, i_10_, i_11_, i, i_8_, i_12_, i_13_);
		aClass123_10501 = new Class123(class505, class474, class478, i_14_, i_15_, aByte7967, i_8_, this, bool, i_16_);
		((Class521_Sub1_Sub4_Sub2) this).aBool10502 = -348507379 * class478.anInt5652 != 0 && !bool;
		method13008(1, (byte) -98);
	}

	public void method93(Class505 class505) {
		aClass123_10501.method2136(class505, (byte) -96);
	}

	public void method83(Class505 class505) {
		aClass123_10501.method2136(class505, (byte) -94);
	}

	public void method97(Class505 class505) {
		aClass123_10501.method2118(class505, 1845753757);
	}

	public void method98(Class505 class505) {
		aClass123_10501.method2118(class505, 2026906554);
	}

	boolean method12986(int i) {
		return false;
	}

	boolean method13000() {
		return ((Class521_Sub1_Sub4_Sub2) this).aBool10504;
	}

	boolean method13001() {
		return ((Class521_Sub1_Sub4_Sub2) this).aBool10504;
	}

	public void method96() {
		/* empty */
	}

	public int method13003() {
		return aClass123_10501.method2115(-829042136);
	}

	public int method84(int i) {
		return ((Class123) aClass123_10501).anInt1540 * 1535779425;
	}

	Class285 method13009(Class505 class505) {
		Class528 class528 = aClass123_10501.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = class505.method8450();
		class294.method5209(method11168());
		class294.method5219((float) aShort9611, 0.0F, (float) aShort9612);
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub4_Sub2) this).aBool10502, (byte) -29);
		int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_17_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10501.method2119(class505, class528, class294, i, i, i_17_, i_17_, true, 801514354);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (null != ((Class123) aClass123_10501).aClass539_1538) {
			Class151 class151 = ((Class123) aClass123_10501).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub4_Sub2) this).aBool10504 = (class528.i() || ((Class123) aClass123_10501).aClass539_1538 != null);
		if (((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 == null)
			((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) -95);
		else
			Class388.method6694((((Class521_Sub1_Sub4_Sub2) this).aClass200_10503), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, 478482041);
		return class285;
	}

	public int method13006() {
		return aClass123_10501.method2115(1308504799);
	}

	public int method13005() {
		return aClass123_10501.method2115(746810719);
	}

	public int method13028() {
		return aClass123_10501.method2121((byte) 1);
	}

	public void method16084(Class476 class476) {
		aClass123_10501.method2116(class476, 59981888);
	}

	Class285 method13010(Class505 class505) {
		Class528 class528 = aClass123_10501.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = class505.method8450();
		class294.method5209(method11168());
		class294.method5219((float) aShort9611, 0.0F, (float) aShort9612);
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub4_Sub2) this).aBool10502, (byte) -73);
		int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_18_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10501.method2119(class505, class528, class294, i, i, i_18_, i_18_, true, 1608810000);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (null != ((Class123) aClass123_10501).aClass539_1538) {
			Class151 class151 = ((Class123) aClass123_10501).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub4_Sub2) this).aBool10504 = (class528.i() || ((Class123) aClass123_10501).aClass539_1538 != null);
		if (((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 == null)
			((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) -44);
		else
			Class388.method6694((((Class521_Sub1_Sub4_Sub2) this).aClass200_10503), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -369350425);
		return class285;
	}

	Class285 method12989(Class505 class505) {
		Class528 class528 = aClass123_10501.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = class505.method8450();
		class294.method5209(method11168());
		class294.method5219((float) aShort9611, 0.0F, (float) aShort9612);
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub4_Sub2) this).aBool10502, (byte) -12);
		int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_19_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10501.method2119(class505, class528, class294, i, i, i_19_, i_19_, true, 2103867839);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (null != ((Class123) aClass123_10501).aClass539_1538) {
			Class151 class151 = ((Class123) aClass123_10501).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub4_Sub2) this).aBool10504 = (class528.i() || ((Class123) aClass123_10501).aClass539_1538 != null);
		if (((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 == null)
			((Class521_Sub1_Sub4_Sub2) this).aClass200_10503 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) -14);
		else
			Class388.method6694((((Class521_Sub1_Sub4_Sub2) this).aClass200_10503), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -1553157461);
		return class285;
	}

	boolean method13002() {
		return ((Class521_Sub1_Sub4_Sub2) this).aBool10504;
	}

	public int method89(int i) {
		return ((Class123) aClass123_10501).anInt1555 * 845010167;
	}

	public Class200 method13019(Class505 class505) {
		return ((Class521_Sub1_Sub4_Sub2) this).aClass200_10503;
	}

	public Class200 method13018(Class505 class505) {
		return ((Class521_Sub1_Sub4_Sub2) this).aClass200_10503;
	}

	public int method13007() {
		return aClass123_10501.method2121((byte) 1);
	}

	boolean method13020(Class505 class505, int i, int i_20_) {
		Class528 class528 = aClass123_10501.method2132(class505, 131072, false, false, -1384937123);
		if (null == class528)
			return false;
		return class528.method11270(i, i_20_, method11168(), false, 0);
	}

	public void method16085(Class476 class476) {
		aClass123_10501.method2116(class476, 953820091);
	}

	public boolean method95() {
		return aClass123_10501.method2117(317422168);
	}

	boolean method12999() {
		return false;
	}

	public static void method16086(int i, int i_21_, int i_22_, int i_23_, int i_24_) {
		Class532.anInt7071 = 1357715881 * i;
		Class532.anInt7069 = 2087200983 * i_22_;
		Class532.anInt7070 = 1835420843 * i_21_;
		Class532.anInt7068 = i_23_ * 739936897;
	}

	static final void method16087(Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2, int i) {
		if (class521_sub1_sub1_sub2.anIntArray10335 != null || class521_sub1_sub1_sub2.anIntArray10362 != null) {
			boolean bool = true;
			Class219 class219 = client.aClass257_7353.method4519(1583283390);
			for (int i_25_ = 0; i_25_ < class521_sub1_sub1_sub2.anIntArray10335.length; i_25_++) {
				int i_26_ = -1;
				if (null != class521_sub1_sub1_sub2.anIntArray10335)
					i_26_ = class521_sub1_sub1_sub2.anIntArray10335[i_25_];
				if (-1 == i_26_) {
					if (!class521_sub1_sub1_sub2.method15798(i_25_, -1, (short) 256))
						bool = false;
				} else {
					bool = false;
					boolean bool_27_ = false;
					boolean bool_28_ = false;
					Class385 class385 = class521_sub1_sub1_sub2.method11166().aClass385_3595;
					int i_29_;
					int i_30_;
					if ((i_26_ & ~0x3fffffff) == -1073741824) {
						int i_31_ = i_26_ & 0xfffffff;
						int i_32_ = i_31_ >> 14;
						int i_33_ = i_31_ & 0x3fff;
						i_29_ = ((int) class385.aFloat4671 - (512 * (i_32_ - class219.anInt2711 * 1948093437) + 256));
						i_30_ = ((int) class385.aFloat4673 - (256 + 512 * (i_33_ - (class219.anInt2712 * -1002240017))));
					} else if ((i_26_ & 0x8000) != 0) {
						int i_34_ = i_26_ & 0x7fff;
						Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1 = (client.aClass521_Sub1_Sub1_Sub2_Sub1Array7314[i_34_]);
						if (null != class521_sub1_sub1_sub2_sub1) {
							Class385 class385_35_ = (class521_sub1_sub1_sub2_sub1.method11166().aClass385_3595);
							i_29_ = ((int) class385.aFloat4671 - (int) class385_35_.aFloat4671);
							i_30_ = ((int) class385.aFloat4673 - (int) class385_35_.aFloat4673);
						} else {
							class521_sub1_sub1_sub2.method15798(i_25_, -1, (short) 256);
							continue;
						}
					} else {
						Class282_Sub47 class282_sub47 = ((Class282_Sub47) client.aClass465_7208.method7754((long) i_26_));
						if (null != class282_sub47) {
							Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2 = ((Class521_Sub1_Sub1_Sub2_Sub2) class282_sub47.anObject8068);
							Class385 class385_36_ = (class521_sub1_sub1_sub2_sub2.method11166().aClass385_3595);
							i_29_ = ((int) class385.aFloat4671 - (int) class385_36_.aFloat4671);
							i_30_ = ((int) class385.aFloat4673 - (int) class385_36_.aFloat4673);
						} else {
							class521_sub1_sub1_sub2.method15798(i_25_, -1, (short) 256);
							continue;
						}
					}
					if (i_29_ != 0 || i_30_ != 0)
						class521_sub1_sub1_sub2.method15798(i_25_, (int) (Math.atan2((double) i_29_, (double) i_30_) * 2607.5945876176133) & 0x3fff, (short) 256);
				}
			}
			if (bool) {
				class521_sub1_sub1_sub2.anIntArray10335 = null;
				class521_sub1_sub1_sub2.anIntArray10362 = null;
			}
		}
	}

	public static void method16088(int i, byte i_37_) {
		if (3 != client.anInt7166 * -1741204137) {
			if (i_37_ != 1) {
				/* empty */
			}
		} else {
			Class282_Sub23 class282_sub23 = Class271.method4828(OutgoingPacket.aClass379_4606, client.aClass184_7218.aClass432_2283, -396054970);
			class282_sub23.buffer.writeByte(i);
			client.aClass184_7218.method3049(class282_sub23, 820598049);
		}
	}

	static final void method16089(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		Class282_Sub50_Sub18 class282_sub50_sub18 = Class96_Sub10.aClass392_9298.method6735((((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012) * 1942118537)]), (short) 24055);
		int i_38_ = (((Class527) class527).anIntArray6999[1 + ((Class527) class527).anInt7012 * 1942118537]);
		int i_39_ = -1;
		for (int i_40_ = 0; i_40_ < -1471207409 * class282_sub50_sub18.anInt10276; i_40_++) {
			if (i_38_ == class282_sub50_sub18.anIntArray10274[i_40_]) {
				i_39_ = class282_sub50_sub18.anIntArray10275[i_40_];
				break;
			}
		}
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_39_;
	}
}
