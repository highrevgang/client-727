
/* Interface4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jaclib.memory.Source;

public interface Interface4 extends Interface5, Interface7 {
	public void method26();

	public boolean method27(int i, int i_0_, Source source);

	public boolean method28(int i, int i_1_);

	public boolean method29(int i, int i_2_);

	public boolean method30(int i, int i_3_);

	public boolean method31(int i, int i_4_);

	public void method32();

	public boolean method33(int i, int i_5_, Source source);

	public boolean method34(int i, int i_6_, Source source);
}
