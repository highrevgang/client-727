/* Class60 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class60 {
	public static Class60 aClass60_601;
	public static Class60 aClass60_602;
	public static Class60 aClass60_603;
	public static Class60 aClass60_604;
	public static Class60 aClass60_605;
	public static Class60 aClass60_606;
	public static Class60 aClass60_607;
	public static Class60 aClass60_608;
	public static Class60 aClass60_609 = new Class60(1);
	public static Class60 aClass60_610;
	public int anInt611;
	public static Class160 aClass160_612;

	Class60(int i) {
		anInt611 = i * -103195257;
	}

	static {
		aClass60_602 = new Class60(2);
		aClass60_603 = new Class60(2);
		aClass60_608 = new Class60(2);
		aClass60_605 = new Class60(1);
		aClass60_606 = new Class60(1);
		aClass60_607 = new Class60(1);
		aClass60_604 = new Class60(2);
		aClass60_601 = new Class60(1);
		aClass60_610 = new Class60(2);
	}

	public static Class60[] method1164() {
		return (new Class60[] { aClass60_609, aClass60_602, aClass60_603, aClass60_608, aClass60_605, aClass60_606, aClass60_607, aClass60_604, aClass60_601, aClass60_610 });
	}

	public static Class60[] method1165() {
		return (new Class60[] { aClass60_609, aClass60_602, aClass60_603, aClass60_608, aClass60_605, aClass60_606, aClass60_607, aClass60_604, aClass60_601, aClass60_610 });
	}

	public static boolean method1166(int i, int i_0_) {
		return 1 == i || i == 3 || 5 == i;
	}

	static final void method1167(Class527 class527, int i) {
		int i_1_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_1_, (byte) 23);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_1_ >> 16];
		Class8.method403(class118, class98, class527, 2136495218);
	}

	public static IncommingPacket[] getIncommingPacket() {
		return (new IncommingPacket[] { IncommingPacket.aClass375_4454, IncommingPacket.aClass375_4352, IncommingPacket.aClass375_4504, IncommingPacket.aClass375_4354, IncommingPacket.aClass375_4450, IncommingPacket.aClass375_4356, IncommingPacket.SMALL_NPC_UPDATE_PACKET, IncommingPacket.aClass375_4366, IncommingPacket.aClass375_4381, IncommingPacket.aClass375_4360, IncommingPacket.aClass375_4361, IncommingPacket.aClass375_4362, IncommingPacket.aClass375_4363, IncommingPacket.aClass375_4365, IncommingPacket.aClass375_4473, IncommingPacket.aClass375_4392, IncommingPacket.aClass375_4367, IncommingPacket.aClass375_4368, IncommingPacket.aClass375_4433, IncommingPacket.aClass375_4370, IncommingPacket.aClass375_4402, IncommingPacket.aClass375_4372, IncommingPacket.aClass375_4510, IncommingPacket.aClass375_4374, IncommingPacket.aClass375_4391, IncommingPacket.aClass375_4376, IncommingPacket.aClass375_4377, IncommingPacket.aClass375_4460, IncommingPacket.aClass375_4480, IncommingPacket.aClass375_4380, IncommingPacket.aClass375_4427, IncommingPacket.aClass375_4382, IncommingPacket.aClass375_4394, IncommingPacket.aClass375_4384, IncommingPacket.aClass375_4385, IncommingPacket.aClass375_4386, IncommingPacket.aClass375_4397, IncommingPacket.WINDOW_PANE_PACKET, IncommingPacket.aClass375_4389, IncommingPacket.aClass375_4371, IncommingPacket.aClass375_4399, IncommingPacket.aClass375_4447, IncommingPacket.aClass375_4393, IncommingPacket.PLAYER_UPDATE_PACKET, IncommingPacket.aClass375_4395, IncommingPacket.aClass375_4440, IncommingPacket.aClass375_4502, IncommingPacket.LARGE_NPC_UPDATE_PACKET, IncommingPacket.aClass375_4424, IncommingPacket.aClass375_4400, IncommingPacket.aClass375_4401, IncommingPacket.aClass375_4503, IncommingPacket.aClass375_4499, IncommingPacket.aClass375_4478, IncommingPacket.aClass375_4511, IncommingPacket.aClass375_4406, IncommingPacket.aClass375_4429, IncommingPacket.aClass375_4408, IncommingPacket.aClass375_4409, IncommingPacket.aClass375_4410, IncommingPacket.aClass375_4411, IncommingPacket.aClass375_4412, IncommingPacket.aClass375_4413, IncommingPacket.aClass375_4414, IncommingPacket.aClass375_4415, IncommingPacket.aClass375_4416, IncommingPacket.aClass375_4390, IncommingPacket.aClass375_4373, IncommingPacket.aClass375_4419, IncommingPacket.aClass375_4420, IncommingPacket.RESET_MINIMAP_FLAG_PACKET, IncommingPacket.aClass375_4422, IncommingPacket.aClass375_4423, IncommingPacket.aClass375_4396, IncommingPacket.aClass375_4425, IncommingPacket.aClass375_4426, IncommingPacket.aClass375_4492, IncommingPacket.aClass375_4428, IncommingPacket.aClass375_4407, IncommingPacket.aClass375_4430, IncommingPacket.aClass375_4431, IncommingPacket.aClass375_4432, IncommingPacket.aClass375_4417, IncommingPacket.aClass375_4434, IncommingPacket.aClass375_4435, IncommingPacket.aClass375_4436, IncommingPacket.aClass375_4437, IncommingPacket.aClass375_4438, IncommingPacket.aClass375_4439, IncommingPacket.aClass375_4387, IncommingPacket.aClass375_4441, IncommingPacket.aClass375_4442, IncommingPacket.aClass375_4443, IncommingPacket.aClass375_4359, IncommingPacket.aClass375_4445, IncommingPacket.aClass375_4446, IncommingPacket.aClass375_4418, IncommingPacket.aClass375_4448, IncommingPacket.aClass375_4449, IncommingPacket.aClass375_4403, IncommingPacket.aClass375_4451, IncommingPacket.aClass375_4452, IncommingPacket.aClass375_4491, IncommingPacket.aClass375_4358, IncommingPacket.aClass375_4455, IncommingPacket.aClass375_4456, IncommingPacket.aClass375_4457, IncommingPacket.aClass375_4458, IncommingPacket.aClass375_4459, IncommingPacket.aClass375_4378, IncommingPacket.aClass375_4461, IncommingPacket.aClass375_4462, IncommingPacket.aClass375_4463, IncommingPacket.aClass375_4464, IncommingPacket.aClass375_4465, IncommingPacket.aClass375_4466, IncommingPacket.aClass375_4467, IncommingPacket.aClass375_4468, IncommingPacket.aClass375_4469, IncommingPacket.aClass375_4470, IncommingPacket.aClass375_4404, IncommingPacket.aClass375_4472, IncommingPacket.aClass375_4509, IncommingPacket.aClass375_4474, IncommingPacket.aClass375_4475, IncommingPacket.aClass375_4476, IncommingPacket.aClass375_4477, IncommingPacket.aClass375_4500, IncommingPacket.aClass375_4479, IncommingPacket.aClass375_4364, IncommingPacket.aClass375_4481, IncommingPacket.aClass375_4353, IncommingPacket.aClass375_4483, IncommingPacket.aClass375_4355, IncommingPacket.aClass375_4453, IncommingPacket.aClass375_4486, IncommingPacket.aClass375_4487, IncommingPacket.aClass375_4488, IncommingPacket.aClass375_4489, IncommingPacket.aClass375_4351, IncommingPacket.aClass375_4490, IncommingPacket.aClass375_4484, IncommingPacket.aClass375_4493, IncommingPacket.aClass375_4494, IncommingPacket.aClass375_4495, IncommingPacket.aClass375_4496, IncommingPacket.aClass375_4497, IncommingPacket.aClass375_4498, IncommingPacket.aClass375_4383, IncommingPacket.aClass375_4375, IncommingPacket.aClass375_4501, IncommingPacket.aClass375_4444, IncommingPacket.aClass375_4369, IncommingPacket.aClass375_4482, IncommingPacket.aClass375_4505, IncommingPacket.aClass375_4506, IncommingPacket.aClass375_4507, IncommingPacket.aClass375_4508, IncommingPacket.aClass375_4379, IncommingPacket.aClass375_4405, IncommingPacket.aClass375_4471 });
	}

	public static boolean method1169(char c, byte i) {
		return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z';
	}

	public static void method1170(int i) {
		if (client.aBool7344) {
			Class118 class118 = Class317.method5694(728544879 * Class7.anInt56, client.anInt7345 * -1673073865, 1160609713);
			if (class118 != null && class118.anObjectArray1396 != null) {
				Class282_Sub43 class282_sub43 = new Class282_Sub43();
				class282_sub43.aClass118_8053 = class118;
				class282_sub43.anObjectArray8054 = class118.anObjectArray1396;
				Class96_Sub4.method13790(class282_sub43, 1386646091);
			}
			client.anInt7427 = 434551523;
			client.anInt7346 = -2109153983;
			client.aBool7344 = false;
			if (null != class118)
				Class109.method1858(class118, (byte) 30);
		}
	}

	static final void method1171(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 487511911 * ((Class527) class527).aClass61_7010.anInt641;
	}

	static void method1172(int i) {
		if (Class90.aClass496_952 != Class496.aClass496_5810)
			Class361.aClass361_4174.method6257(-84783453);
	}

	static final void method1173(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 1334854505 * client.anInt7422;
	}

	static final void method1174(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		Class426.method7170(i_3_, 377314002);
		int i_8_ = 0;
		int i_9_ = i_3_ - i_6_;
		if (i_9_ < 0)
			i_9_ = 0;
		int i_10_ = i_3_;
		int i_11_ = -i_3_;
		int i_12_ = i_9_;
		int i_13_ = -i_9_;
		int i_14_ = -1;
		int i_15_ = -1;
		if (i_2_ >= Class532_Sub3.anInt7070 * 324226563 && i_2_ <= Class532_Sub3.anInt7068 * -348932735) {
			int[] is = Class532_Sub3.anIntArrayArray7072[i_2_];
			int i_16_ = Class275.method4890(i - i_3_, Class532_Sub3.anInt7071 * -612590951, -1345107225 * Class532_Sub3.anInt7069, 1149911370);
			int i_17_ = Class275.method4890(i_3_ + i, -612590951 * Class532_Sub3.anInt7071, -1345107225 * Class532_Sub3.anInt7069, -1752676227);
			int i_18_ = Class275.method4890(i - i_9_, -612590951 * Class532_Sub3.anInt7071, -1345107225 * Class532_Sub3.anInt7069, 543795752);
			int i_19_ = Class275.method4890(i_9_ + i, Class532_Sub3.anInt7071 * -612590951, Class532_Sub3.anInt7069 * -1345107225, 1403864905);
			Class232.method3922(is, i_16_, i_18_, i_5_, (byte) -39);
			Class232.method3922(is, i_18_, i_19_, i_4_, (byte) -10);
			Class232.method3922(is, i_19_, i_17_, i_5_, (byte) -23);
		}
		while (i_10_ > i_8_) {
			i_14_ += 2;
			i_15_ += 2;
			i_11_ += i_14_;
			i_13_ += i_15_;
			if (i_13_ >= 0 && i_12_ >= 1) {
				i_12_--;
				i_13_ -= i_12_ << 1;
				Class5.anIntArray36[i_12_] = i_8_;
			}
			i_8_++;
			if (i_11_ >= 0) {
				i_10_--;
				i_11_ -= i_10_ << 1;
				int i_20_ = i_2_ - i_10_;
				int i_21_ = i_10_ + i_2_;
				if (i_21_ >= 324226563 * Class532_Sub3.anInt7070 && i_20_ <= -348932735 * Class532_Sub3.anInt7068) {
					if (i_10_ >= i_9_) {
						int i_22_ = Class275.method4890(i_8_ + i, (Class532_Sub3.anInt7071 * -612590951), (Class532_Sub3.anInt7069 * -1345107225), -617603444);
						int i_23_ = Class275.method4890(i - i_8_, (Class532_Sub3.anInt7071 * -612590951), (Class532_Sub3.anInt7069 * -1345107225), -1801446753);
						if (i_21_ <= -348932735 * Class532_Sub3.anInt7068)
							Class232.method3922((Class532_Sub3.anIntArrayArray7072[i_21_]), i_23_, i_22_, i_5_, (byte) -23);
						if (i_20_ >= Class532_Sub3.anInt7070 * 324226563)
							Class232.method3922((Class532_Sub3.anIntArrayArray7072[i_20_]), i_23_, i_22_, i_5_, (byte) 29);
					} else {
						int i_24_ = Class5.anIntArray36[i_10_];
						int i_25_ = Class275.method4890(i + i_8_, (Class532_Sub3.anInt7071 * -612590951), (-1345107225 * Class532_Sub3.anInt7069), 877789143);
						int i_26_ = Class275.method4890(i - i_8_, (Class532_Sub3.anInt7071 * -612590951), (-1345107225 * Class532_Sub3.anInt7069), 58227539);
						int i_27_ = Class275.method4890(i + i_24_, (Class532_Sub3.anInt7071 * -612590951), (Class532_Sub3.anInt7069 * -1345107225), -1073712474);
						int i_28_ = Class275.method4890(i - i_24_, (Class532_Sub3.anInt7071 * -612590951), (-1345107225 * Class532_Sub3.anInt7069), 87726144);
						if (i_21_ <= -348932735 * Class532_Sub3.anInt7068) {
							int[] is = Class532_Sub3.anIntArrayArray7072[i_21_];
							Class232.method3922(is, i_26_, i_28_, i_5_, (byte) 31);
							Class232.method3922(is, i_28_, i_27_, i_4_, (byte) -76);
							Class232.method3922(is, i_27_, i_25_, i_5_, (byte) 42);
						}
						if (i_20_ >= Class532_Sub3.anInt7070 * 324226563) {
							int[] is = Class532_Sub3.anIntArrayArray7072[i_20_];
							Class232.method3922(is, i_26_, i_28_, i_5_, (byte) -28);
							Class232.method3922(is, i_28_, i_27_, i_4_, (byte) -31);
							Class232.method3922(is, i_27_, i_25_, i_5_, (byte) -10);
						}
					}
				}
			}
			int i_29_ = i_2_ - i_8_;
			int i_30_ = i_2_ + i_8_;
			if (i_30_ >= Class532_Sub3.anInt7070 * 324226563 && i_29_ <= -348932735 * Class532_Sub3.anInt7068) {
				int i_31_ = i + i_10_;
				int i_32_ = i - i_10_;
				if (i_31_ >= -612590951 * Class532_Sub3.anInt7071 && i_32_ <= Class532_Sub3.anInt7069 * -1345107225) {
					i_31_ = Class275.method4890(i_31_, (Class532_Sub3.anInt7071 * -612590951), (-1345107225 * Class532_Sub3.anInt7069), 686110117);
					i_32_ = Class275.method4890(i_32_, (-612590951 * Class532_Sub3.anInt7071), (Class532_Sub3.anInt7069 * -1345107225), -716811882);
					if (i_8_ < i_9_) {
						int i_33_ = i_12_ < i_8_ ? Class5.anIntArray36[i_8_] : i_12_;
						int i_34_ = Class275.method4890(i + i_33_, (-612590951 * Class532_Sub3.anInt7071), (Class532_Sub3.anInt7069 * -1345107225), -569174694);
						int i_35_ = Class275.method4890(i - i_33_, (-612590951 * Class532_Sub3.anInt7071), (-1345107225 * Class532_Sub3.anInt7069), -277030614);
						if (i_30_ <= Class532_Sub3.anInt7068 * -348932735) {
							int[] is = Class532_Sub3.anIntArrayArray7072[i_30_];
							Class232.method3922(is, i_32_, i_35_, i_5_, (byte) -20);
							Class232.method3922(is, i_35_, i_34_, i_4_, (byte) 51);
							Class232.method3922(is, i_34_, i_31_, i_5_, (byte) -7);
						}
						if (i_29_ >= 324226563 * Class532_Sub3.anInt7070) {
							int[] is = Class532_Sub3.anIntArrayArray7072[i_29_];
							Class232.method3922(is, i_32_, i_35_, i_5_, (byte) 32);
							Class232.method3922(is, i_35_, i_34_, i_4_, (byte) -49);
							Class232.method3922(is, i_34_, i_31_, i_5_, (byte) -4);
						}
					} else {
						if (i_30_ <= -348932735 * Class532_Sub3.anInt7068)
							Class232.method3922((Class532_Sub3.anIntArrayArray7072[i_30_]), i_32_, i_31_, i_5_, (byte) 63);
						if (i_29_ >= 324226563 * Class532_Sub3.anInt7070)
							Class232.method3922((Class532_Sub3.anIntArrayArray7072[i_29_]), i_32_, i_31_, i_5_, (byte) 67);
					}
				}
			}
		}
	}
}
