/* Class420 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class420 implements Interface18 {
	int anInt5005;
	int anInt5006;
	int anInt5007;
	int anInt5008;
	int anInt5009;
	int anInt5010;
	boolean aBool5011;

	public boolean method132(Interface18 interface18) {
		if (!(interface18 instanceof Class420))
			return false;
		Class420 class420_0_ = (Class420) interface18;
		if (((Class420) class420_0_).anInt5008 * 362003861 != ((Class420) this).anInt5008 * 362003861)
			return false;
		if (-2122717931 * ((Class420) class420_0_).anInt5007 != -2122717931 * ((Class420) this).anInt5007)
			return false;
		if (((Class420) class420_0_).anInt5006 * 716754807 != 716754807 * ((Class420) this).anInt5006)
			return false;
		if (((Class420) this).anInt5005 * -1886226917 != -1886226917 * ((Class420) class420_0_).anInt5005)
			return false;
		if (442868743 * ((Class420) class420_0_).anInt5009 != ((Class420) this).anInt5009 * 442868743)
			return false;
		if (((Class420) class420_0_).anInt5010 * -1317952433 != -1317952433 * ((Class420) this).anInt5010)
			return false;
		if (((Class420) class420_0_).aBool5011 != ((Class420) this).aBool5011)
			return false;
		return true;
	}

	Class420() {
		/* empty */
	}

	public boolean method131(Interface18 interface18) {
		if (!(interface18 instanceof Class420))
			return false;
		Class420 class420_1_ = (Class420) interface18;
		if (((Class420) class420_1_).anInt5008 * 362003861 != ((Class420) this).anInt5008 * 362003861)
			return false;
		if (-2122717931 * ((Class420) class420_1_).anInt5007 != -2122717931 * ((Class420) this).anInt5007)
			return false;
		if (((Class420) class420_1_).anInt5006 * 716754807 != 716754807 * ((Class420) this).anInt5006)
			return false;
		if (((Class420) this).anInt5005 * -1886226917 != -1886226917 * ((Class420) class420_1_).anInt5005)
			return false;
		if (442868743 * ((Class420) class420_1_).anInt5009 != ((Class420) this).anInt5009 * 442868743)
			return false;
		if (((Class420) class420_1_).anInt5010 * -1317952433 != -1317952433 * ((Class420) this).anInt5010)
			return false;
		if (((Class420) class420_1_).aBool5011 != ((Class420) this).aBool5011)
			return false;
		return true;
	}

	public long method109() {
		long[] ls = RsByteBuffer.aLongArray7979;
		long l = -1L;
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (362003861 * ((Class420) this).anInt5008)) & 0xffL)];
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (-2122717931 * ((Class420) this).anInt5007 >> 8)) & 0xffL)]);
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (-2122717931 * ((Class420) this).anInt5007)) & 0xffL)];
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (716754807 * ((Class420) this).anInt5006 >> 24)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (716754807 * ((Class420) this).anInt5006 >> 16)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (716754807 * ((Class420) this).anInt5006 >> 8)) & 0xffL)]);
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (((Class420) this).anInt5006 * 716754807)) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (-1886226917 * ((Class420) this).anInt5005)) & 0xffL)];
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (442868743 * ((Class420) this).anInt5009 >> 24)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (((Class420) this).anInt5009 * 442868743 >> 16)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (442868743 * ((Class420) this).anInt5009 >> 8)) & 0xffL)]);
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (442868743 * ((Class420) this).anInt5009)) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (-1317952433 * ((Class420) this).anInt5010)) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (((Class420) this).aBool5011 ? 1 : 0)) & 0xffL)];
		return l;
	}

	public long method130() {
		long[] ls = RsByteBuffer.aLongArray7979;
		long l = -1L;
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (362003861 * ((Class420) this).anInt5008)) & 0xffL)];
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (-2122717931 * ((Class420) this).anInt5007 >> 8)) & 0xffL)]);
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (-2122717931 * ((Class420) this).anInt5007)) & 0xffL)];
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (716754807 * ((Class420) this).anInt5006 >> 24)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (716754807 * ((Class420) this).anInt5006 >> 16)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (716754807 * ((Class420) this).anInt5006 >> 8)) & 0xffL)]);
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (((Class420) this).anInt5006 * 716754807)) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (-1886226917 * ((Class420) this).anInt5005)) & 0xffL)];
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (442868743 * ((Class420) this).anInt5009 >> 24)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (((Class420) this).anInt5009 * 442868743 >> 16)) & 0xffL)]);
		l = (l >>> 8 ^ ls[(int) ((l ^ (long) (442868743 * ((Class420) this).anInt5009 >> 8)) & 0xffL)]);
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (442868743 * ((Class420) this).anInt5009)) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (-1317952433 * ((Class420) this).anInt5010)) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((l ^ (long) (((Class420) this).aBool5011 ? 1 : 0)) & 0xffL)];
		return l;
	}

	public boolean method133(Interface18 interface18) {
		if (!(interface18 instanceof Class420))
			return false;
		Class420 class420_2_ = (Class420) interface18;
		if (((Class420) class420_2_).anInt5008 * 362003861 != ((Class420) this).anInt5008 * 362003861)
			return false;
		if (-2122717931 * ((Class420) class420_2_).anInt5007 != -2122717931 * ((Class420) this).anInt5007)
			return false;
		if (((Class420) class420_2_).anInt5006 * 716754807 != 716754807 * ((Class420) this).anInt5006)
			return false;
		if (((Class420) this).anInt5005 * -1886226917 != -1886226917 * ((Class420) class420_2_).anInt5005)
			return false;
		if (442868743 * ((Class420) class420_2_).anInt5009 != ((Class420) this).anInt5009 * 442868743)
			return false;
		if (((Class420) class420_2_).anInt5010 * -1317952433 != -1317952433 * ((Class420) this).anInt5010)
			return false;
		if (((Class420) class420_2_).aBool5011 != ((Class420) this).aBool5011)
			return false;
		return true;
	}

	public boolean method134(Interface18 interface18) {
		if (!(interface18 instanceof Class420))
			return false;
		Class420 class420_3_ = (Class420) interface18;
		if (((Class420) class420_3_).anInt5008 * 362003861 != ((Class420) this).anInt5008 * 362003861)
			return false;
		if (-2122717931 * ((Class420) class420_3_).anInt5007 != -2122717931 * ((Class420) this).anInt5007)
			return false;
		if (((Class420) class420_3_).anInt5006 * 716754807 != 716754807 * ((Class420) this).anInt5006)
			return false;
		if (((Class420) this).anInt5005 * -1886226917 != -1886226917 * ((Class420) class420_3_).anInt5005)
			return false;
		if (442868743 * ((Class420) class420_3_).anInt5009 != ((Class420) this).anInt5009 * 442868743)
			return false;
		if (((Class420) class420_3_).anInt5010 * -1317952433 != -1317952433 * ((Class420) this).anInt5010)
			return false;
		if (((Class420) class420_3_).aBool5011 != ((Class420) this).aBool5011)
			return false;
		return true;
	}

	static final void method7032(Class527 class527, short i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = class118.anInt1301 * 1506818197;
	}

	static Class149_Sub4 method7033(RsByteBuffer class282_sub35, int i) {
		return new Class149_Sub4(class282_sub35.method13081(1848939200), class282_sub35.method13081(1780715141), class282_sub35.method13081(1644491611), class282_sub35.method13081(1586203891), class282_sub35.method13082((short) 4319), class282_sub35.method13082((short) 18735), class282_sub35.readUnsignedByte());
	}

	public static void method7034(Class518 class518, int i, int i_4_) {
		if (-458827259 * Class260.anInt3219 < 50 && (class518 != null && null != class518.anIntArrayArray5913 && i < class518.anIntArrayArray5913.length && null != class518.anIntArrayArray5913[i])) {
			int i_5_ = class518.anIntArrayArray5913[i][0];
			int i_6_ = i_5_ >> 8;
			int i_7_ = i_5_ >> 5 & 0x7;
			if (class518.anIntArrayArray5913[i].length > 1) {
				int i_8_ = (int) (Math.random() * (double) (class518.anIntArrayArray5913[i]).length);
				if (i_8_ > 0)
					i_6_ = class518.anIntArrayArray5913[i][i_8_];
			}
			int i_9_ = 256;
			if (class518.anIntArray5927 != null && null != class518.anIntArray5919)
				i_9_ = Class76.method1356(class518.anIntArray5927[i], class518.anIntArray5919[i], -1021175029);
			int i_10_ = (class518.anIntArray5926 == null ? 255 : class518.anIntArray5926[i]);
			if (class518.aBool5928)
				Class435.method7300(i_6_, i_7_, 0, i_10_, false, i_9_, 1449989045);
			else
				Class153.method2618(i_6_, i_7_, 0, i_10_, i_9_, 1648413322);
		}
	}

	static final boolean method7035(char c, int i) {
		return '\u00a0' == c || c == ' ' || '_' == c || c == '-';
	}
}
