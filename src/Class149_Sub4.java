
/* Class149_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Iterator;

public class Class149_Sub4 extends Class149 {
	static final int anInt9390 = 2;
	int anInt9391;
	int anInt9392;
	int anInt9393;
	int anInt9394;

	void method2558(int i, int i_0_) {
		int i_1_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_2_ = i * (232719965 * ((Class149_Sub4) this).anInt9392) >> 12;
		int i_3_ = ((Class149_Sub4) this).anInt9393 * -1685653473 * i_0_ >> 12;
		int i_4_ = i_0_ * (((Class149_Sub4) this).anInt9394 * -1856308777) >> 12;
		Class395.method6772(i_1_, i_2_, i_3_, i_4_, 1340859839 * ((Class149_Sub4) this).anInt1743, -1670932530);
	}

	void method2561(int i, int i_5_, int i_6_) {
		int i_7_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_8_ = ((Class149_Sub4) this).anInt9392 * 232719965 * i >> 12;
		int i_9_ = i_5_ * (-1685653473 * ((Class149_Sub4) this).anInt9393) >> 12;
		int i_10_ = ((Class149_Sub4) this).anInt9394 * -1856308777 * i_5_ >> 12;
		Class105.method1805(i_7_, i_8_, i_9_, i_10_, -1525176857 * ((Class149_Sub4) this).anInt1741, ((Class149_Sub4) this).anInt1742 * -1125689331, (byte) 9);
	}

	void method2556(int i, int i_11_, int i_12_) {
		int i_13_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_14_ = i * (232719965 * ((Class149_Sub4) this).anInt9392) >> 12;
		int i_15_ = ((Class149_Sub4) this).anInt9393 * -1685653473 * i_11_ >> 12;
		int i_16_ = i_11_ * (((Class149_Sub4) this).anInt9394 * -1856308777) >> 12;
		Class395.method6772(i_13_, i_14_, i_15_, i_16_, 1340859839 * ((Class149_Sub4) this).anInt1743, -975151690);
	}

	void method2557(int i, int i_17_, byte i_18_) {
		int i_19_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_20_ = i * (232719965 * ((Class149_Sub4) this).anInt9392) >> 12;
		int i_21_ = i_17_ * (-1685653473 * ((Class149_Sub4) this).anInt9393) >> 12;
		int i_22_ = i_17_ * (((Class149_Sub4) this).anInt9394 * -1856308777) >> 12;
		Class390.method6731(i_19_, i_20_, i_21_, i_22_, 1340859839 * ((Class149_Sub4) this).anInt1743, -1525176857 * ((Class149_Sub4) this).anInt1741, ((Class149_Sub4) this).anInt1742 * -1125689331, 1235101523);
	}

	void method2555(int i, int i_23_) {
		int i_24_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_25_ = ((Class149_Sub4) this).anInt9392 * 232719965 * i >> 12;
		int i_26_ = i_23_ * (-1685653473 * ((Class149_Sub4) this).anInt9393) >> 12;
		int i_27_ = ((Class149_Sub4) this).anInt9394 * -1856308777 * i_23_ >> 12;
		Class105.method1805(i_24_, i_25_, i_26_, i_27_, -1525176857 * ((Class149_Sub4) this).anInt1741, ((Class149_Sub4) this).anInt1742 * -1125689331, (byte) 9);
	}

	void method2559(int i, int i_28_) {
		int i_29_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_30_ = ((Class149_Sub4) this).anInt9392 * 232719965 * i >> 12;
		int i_31_ = i_28_ * (-1685653473 * ((Class149_Sub4) this).anInt9393) >> 12;
		int i_32_ = ((Class149_Sub4) this).anInt9394 * -1856308777 * i_28_ >> 12;
		Class105.method1805(i_29_, i_30_, i_31_, i_32_, -1525176857 * ((Class149_Sub4) this).anInt1741, ((Class149_Sub4) this).anInt1742 * -1125689331, (byte) 9);
	}

	void method2562(int i, int i_33_) {
		int i_34_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_35_ = i * (232719965 * ((Class149_Sub4) this).anInt9392) >> 12;
		int i_36_ = i_33_ * (-1685653473 * ((Class149_Sub4) this).anInt9393) >> 12;
		int i_37_ = i_33_ * (((Class149_Sub4) this).anInt9394 * -1856308777) >> 12;
		Class390.method6731(i_34_, i_35_, i_36_, i_37_, 1340859839 * ((Class149_Sub4) this).anInt1743, -1525176857 * ((Class149_Sub4) this).anInt1741, ((Class149_Sub4) this).anInt1742 * -1125689331, 2071365081);
	}

	void method2560(int i, int i_38_) {
		int i_39_ = 252323111 * ((Class149_Sub4) this).anInt9391 * i >> 12;
		int i_40_ = i * (232719965 * ((Class149_Sub4) this).anInt9392) >> 12;
		int i_41_ = ((Class149_Sub4) this).anInt9393 * -1685653473 * i_38_ >> 12;
		int i_42_ = i_38_ * (((Class149_Sub4) this).anInt9394 * -1856308777) >> 12;
		Class395.method6772(i_39_, i_40_, i_41_, i_42_, 1340859839 * ((Class149_Sub4) this).anInt1743, -1875945632);
	}

	static Class149_Sub4 method14657(RsByteBuffer class282_sub35) {
		return new Class149_Sub4(class282_sub35.method13081(1875401982), class282_sub35.method13081(1719604902), class282_sub35.method13081(1933139461), class282_sub35.method13081(1726619922), class282_sub35.method13082((short) 28404), class282_sub35.method13082((short) 32666), class282_sub35.readUnsignedByte());
	}

	Class149_Sub4(int i, int i_43_, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_) {
		super(i_46_, i_47_, i_48_);
		((Class149_Sub4) this).anInt9391 = i * 249887383;
		((Class149_Sub4) this).anInt9393 = i_43_ * -724925473;
		((Class149_Sub4) this).anInt9392 = i_44_ * 50986485;
		((Class149_Sub4) this).anInt9394 = 954206695 * i_45_;
	}

	static Class149_Sub4 method14658(RsByteBuffer class282_sub35) {
		return new Class149_Sub4(class282_sub35.method13081(1764154776), class282_sub35.method13081(1648192049), class282_sub35.method13081(1698991931), class282_sub35.method13081(2085932950), class282_sub35.method13082((short) 9314), class282_sub35.method13082((short) 3899), class282_sub35.readUnsignedByte());
	}

	static final void method14659(Class527 class527, byte i) {
		int i_49_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_49_, (byte) 45);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = -37350919 * class118.anInt1314;
	}

	static final void method14660(Class527 class527, int i) {
		int i_50_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_50_, (byte) 122);
		Class88.method1494(class118, class527, (byte) -43);
	}

	static final void method14661(byte i) {
		Iterator iterator = client.aClass465_7334.iterator();
		while (iterator.hasNext()) {
			Class282_Sub50_Sub10 class282_sub50_sub10 = (Class282_Sub50_Sub10) iterator.next();
			Class521_Sub1_Sub1_Sub4 class521_sub1_sub1_sub4 = (((Class282_Sub50_Sub10) class282_sub50_sub10).aClass521_Sub1_Sub1_Sub4_9636);
			class521_sub1_sub1_sub4.method15926(1, (byte) 16);
			if (class521_sub1_sub1_sub4.method15928((short) 11184)) {
				class282_sub50_sub10.method4991(-371378792);
				class521_sub1_sub1_sub4.method15931(-2018214290);
			}
		}
	}

	static String method14662(int i, int i_51_, boolean bool, int i_52_) {
		if (i_51_ < 2 || i_51_ > 36)
			throw new IllegalArgumentException(new StringBuilder().append("").append(i_51_).toString());
		if (!bool || i < 0)
			return Integer.toString(i, i_51_);
		int i_53_ = 2;
		int i_54_ = i / i_51_;
		while (i_54_ != 0) {
			i_54_ /= i_51_;
			i_53_++;
		}
		char[] cs = new char[i_53_];
		cs[0] = '+';
		for (int i_55_ = i_53_ - 1; i_55_ > 0; i_55_--) {
			int i_56_ = i;
			i /= i_51_;
			int i_57_ = i_56_ - i_51_ * i;
			if (i_57_ >= 10)
				cs[i_55_] = (char) (i_57_ + 87);
			else
				cs[i_55_] = (char) (48 + i_57_);
		}
		return new String(cs);
	}

	static void method14663(int i, int i_58_, byte i_59_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14995(2079559306);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_58_;
	}
}
