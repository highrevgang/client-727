/* Class249 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class249 implements Interface43 {
	public static final int anInt3081 = 4;
	public static Class249 aClass249_3082;
	public static Class249 aClass249_3083 = new Class249((byte) -1);
	public static Class249 aClass249_3084;
	public byte aByte3085;
	public static Class249 aClass249_3086;

	public static Class249[] method4267() {
		return new Class249[] { aClass249_3082, aClass249_3084, aClass249_3086, aClass249_3083 };
	}

	public int method243(byte i) {
		return aByte3085;
	}

	public int method4268(int i) {
		return 1 + aByte3085;
	}

	static {
		aClass249_3082 = new Class249((byte) 0);
		aClass249_3084 = new Class249((byte) 1);
		aClass249_3086 = new Class249((byte) 2);
	}

	public int method76() {
		return aByte3085;
	}

	public int method4269() {
		return 1 + aByte3085;
	}

	public int method75() {
		return aByte3085;
	}

	public static Class249[] method4270() {
		return new Class249[] { aClass249_3082, aClass249_3084, aClass249_3086, aClass249_3083 };
	}

	public static Class249[] method4271() {
		return new Class249[] { aClass249_3082, aClass249_3084, aClass249_3086, aClass249_3083 };
	}

	Class249(byte i) {
		aByte3085 = i;
	}

	public static Class249[] method4272() {
		return new Class249[] { aClass249_3082, aClass249_3084, aClass249_3086, aClass249_3083 };
	}

	public int method4273() {
		return 1 + aByte3085;
	}

	public int method4274() {
		return 1 + aByte3085;
	}

	public int method4275() {
		return 1 + aByte3085;
	}

	static final void method4276(Class118 class118, Class98 class98, Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 851346006;
		class118.anInt1293 = -1312401539 * (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		class118.anInt1334 = ((((Class527) class527).anIntArray6999[1 + 1942118537 * ((Class527) class527).anInt7012]) * -2054115939);
		class118.anInt1385 = ((((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 2]) * 1229969471);
		class118.anInt1336 = ((((Class527) class527).anIntArray6999[3 + ((Class527) class527).anInt7012 * 1942118537]) * -140036259);
		class118.anInt1337 = (-906963849 * (((Class527) class527).anIntArray6999[4 + 1942118537 * ((Class527) class527).anInt7012]));
		class118.anInt1343 = ((((Class527) class527).anIntArray6999[5 + ((Class527) class527).anInt7012 * 1942118537]) * -807852535);
		Class109.method1858(class118, (byte) 60);
		if (-1 == 1924549737 * class118.anInt1288 && !class98.aBool999) {
			Class106.method1818(-1952846363 * class118.anInt1287, -1101705065);
			Class282_Sub32.method12575(class118.anInt1287 * -1952846363, (byte) 9);
		}
	}

	static final void method4277(Class527 class527, byte i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = -56249735 * class118.anInt1426;
	}

	static final void method4278(Class527 class527, int i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		Class402.method6798(string, (short) -15782);
	}

	static final void method4279(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class293.method5206(-2045199626);
	}

	static final void method4280(Class118 class118, Class98 class98, Class527 class527, byte i) {
		Class118 class118_0_ = class118;
		boolean bool;
		if ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1) {
			if (i <= 32)
				return;
			bool = true;
		} else
			bool = false;
		class118_0_.aBool1325 = bool;
		Class109.method1858(class118, (byte) 40);
	}

	static final void method4281(Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2, int i) {
		int i_1_ = (class521_sub1_sub1_sub2.anInt10342 * 403949281 - -1809259861 * client.anInt7174);
		int i_2_ = (class521_sub1_sub1_sub2.anInt10326 * 1204164096 + class521_sub1_sub1_sub2.method15805(828768449) * 256);
		int i_3_ = (-699068928 * class521_sub1_sub1_sub2.anInt10328 + class521_sub1_sub1_sub2.method15805(828768449) * 256);
		Class385 class385 = class521_sub1_sub1_sub2.method11166().aClass385_3595;
		class521_sub1_sub1_sub2.method11172((float) ((int) class385.aFloat4671 + (i_2_ - (int) class385.aFloat4671) / i_1_), (float) (int) class385.aFloat4672, (float) ((int) class385.aFloat4673 + (i_3_ - (int) class385.aFloat4673) / i_1_));
		class521_sub1_sub1_sub2.anInt10366 = 0;
		class521_sub1_sub1_sub2.method15863((-445026593 * (class521_sub1_sub1_sub2.anInt10346)), 1692779087);
	}

	public static boolean method4282(String string, byte i) {
		if (string == null)
			return false;
		for (int i_4_ = 0; i_4_ < 493536965 * client.anInt7449; i_4_++) {
			if (string.equalsIgnoreCase(client.aClass6Array7452[i_4_].aString37))
				return true;
		}
		if (string.equalsIgnoreCase(Class84.myPlayer.aString10546))
			return true;
		return false;
	}

	static final void method4283(Class527 class527, short i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class524.method11221(class118, class98, class527, -2007218506);
	}
}
