/* Class511 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class511 {
	public Class232 aClass232_5873;
	public Interface17 anInterface17_5874;
	public Interface17 anInterface17_5875;
	public boolean aBool5876;
	public Class232 aClass232_5877;
	public Interface17 anInterface17_5878;
	public Class232 aClass232_5879;
	public int anInt5880;
	public int anInt5881;
	public Interface17 anInterface17_5882;
	static Class160 aClass160_5883;

	void method8745(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (i == 0)
				break;
			if (1 == i)
				anInterface17_5878 = Class197.method3202(class282_sub35, 470106489);
			else if (i == 2)
				anInterface17_5874 = Class197.method3202(class282_sub35, 470106489);
			else if (3 == i)
				anInterface17_5875 = Class197.method3202(class282_sub35, 470106489);
			else if (i == 4)
				anInterface17_5882 = Class197.method3202(class282_sub35, 470106489);
			else if (5 == i)
				aClass232_5877 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 6)
				aClass232_5873 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 7)
				aClass232_5879 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 8)
				Class197.method3202(class282_sub35, 470106489);
			else if (9 == i)
				Class197.method3202(class282_sub35, 470106489);
			else if (10 == i)
				Class197.method3202(class282_sub35, 470106489);
			else if (11 == i)
				aBool5876 = true;
			else if (12 == i)
				anInt5880 = class282_sub35.readIntLE() * 1569484857;
			else if (13 == i)
				anInt5881 = class282_sub35.readIntLE() * 2053453887;
		}
	}

	public Class511(Class317 class317) {
		byte[] is = class317.method5615((-1719912253 * Class524.aClass524_6964.anInt6966), -1307124908);
		method8746(new RsByteBuffer(is), -525270939);
	}

	void method8746(RsByteBuffer class282_sub35, int i) {
		for (;;) {
			int i_0_ = class282_sub35.readUnsignedByte();
			if (i_0_ == 0)
				break;
			if (1 == i_0_)
				anInterface17_5878 = Class197.method3202(class282_sub35, 470106489);
			else if (i_0_ == 2)
				anInterface17_5874 = Class197.method3202(class282_sub35, 470106489);
			else if (3 == i_0_)
				anInterface17_5875 = Class197.method3202(class282_sub35, 470106489);
			else if (i_0_ == 4)
				anInterface17_5882 = Class197.method3202(class282_sub35, 470106489);
			else if (5 == i_0_)
				aClass232_5877 = Class336.method6009(class282_sub35, -1710988237);
			else if (i_0_ == 6)
				aClass232_5873 = Class336.method6009(class282_sub35, -1710988237);
			else if (i_0_ == 7)
				aClass232_5879 = Class336.method6009(class282_sub35, -1710988237);
			else if (i_0_ == 8)
				Class197.method3202(class282_sub35, 470106489);
			else if (9 == i_0_)
				Class197.method3202(class282_sub35, 470106489);
			else if (10 == i_0_)
				Class197.method3202(class282_sub35, 470106489);
			else if (11 == i_0_)
				aBool5876 = true;
			else if (12 == i_0_)
				anInt5880 = class282_sub35.readIntLE() * 1569484857;
			else if (13 == i_0_)
				anInt5881 = class282_sub35.readIntLE() * 2053453887;
		}
	}

	void method8747(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (i == 0)
				break;
			if (1 == i)
				anInterface17_5878 = Class197.method3202(class282_sub35, 470106489);
			else if (i == 2)
				anInterface17_5874 = Class197.method3202(class282_sub35, 470106489);
			else if (3 == i)
				anInterface17_5875 = Class197.method3202(class282_sub35, 470106489);
			else if (i == 4)
				anInterface17_5882 = Class197.method3202(class282_sub35, 470106489);
			else if (5 == i)
				aClass232_5877 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 6)
				aClass232_5873 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 7)
				aClass232_5879 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 8)
				Class197.method3202(class282_sub35, 470106489);
			else if (9 == i)
				Class197.method3202(class282_sub35, 470106489);
			else if (10 == i)
				Class197.method3202(class282_sub35, 470106489);
			else if (11 == i)
				aBool5876 = true;
			else if (12 == i)
				anInt5880 = class282_sub35.readIntLE() * 1569484857;
			else if (13 == i)
				anInt5881 = class282_sub35.readIntLE() * 2053453887;
		}
	}

	void method8748(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (i == 0)
				break;
			if (1 == i)
				anInterface17_5878 = Class197.method3202(class282_sub35, 470106489);
			else if (i == 2)
				anInterface17_5874 = Class197.method3202(class282_sub35, 470106489);
			else if (3 == i)
				anInterface17_5875 = Class197.method3202(class282_sub35, 470106489);
			else if (i == 4)
				anInterface17_5882 = Class197.method3202(class282_sub35, 470106489);
			else if (5 == i)
				aClass232_5877 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 6)
				aClass232_5873 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 7)
				aClass232_5879 = Class336.method6009(class282_sub35, -1710988237);
			else if (i == 8)
				Class197.method3202(class282_sub35, 470106489);
			else if (9 == i)
				Class197.method3202(class282_sub35, 470106489);
			else if (10 == i)
				Class197.method3202(class282_sub35, 470106489);
			else if (11 == i)
				aBool5876 = true;
			else if (12 == i)
				anInt5880 = class282_sub35.readIntLE() * 1569484857;
			else if (13 == i)
				anInt5881 = class282_sub35.readIntLE() * 2053453887;
		}
	}

	static final void method8749(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		if ((((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]) < (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]))
			((Class527) class527).anInt7020 += ((((Class527) class527).anIntArray7018[301123709 * ((Class527) class527).anInt7020]) * -1051529003);
	}

	static final void method8750(Class527 class527, int i) {
		int i_1_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_1_, (byte) 23);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_1_ >> 16];
		Class369.method6313(class118, class98, class527, 435475117);
	}

	public static void method8751(Class282_Sub50 class282_sub50, Class282_Sub50 class282_sub50_2_, int i) {
		if (null != class282_sub50.aClass282_Sub50_8118)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = class282_sub50_2_.aClass282_Sub50_8118;
		class282_sub50.aClass282_Sub50_8119 = class282_sub50_2_;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	public static Class528 method8752(Class505 class505, int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		long l = (long) i_6_;
		Class528 class528 = (Class528) Class180.aClass229_2245.method3865(l);
		int i_8_ = 2055;
		if (null == class528) {
			Class157 class157 = Class157.method2689(Class110.aClass317_1106, i_6_, 0);
			if (null == class157)
				return null;
			if (class157.anInt1986 < 13)
				class157.method2679(2);
			class528 = class505.method8451(class157, i_8_, 2079347217 * Class180.anInt2246, 64, 768);
			Class180.aClass229_2245.method3856(class528, l);
		}
		class528 = class528.method11289((byte) 6, i_8_, true);
		if (i != 0)
			class528.f(i);
		if (i_3_ != 0)
			class528.t(i_3_);
		if (0 != i_4_)
			class528.EA(i_4_);
		if (0 != i_5_)
			class528.ia(0, i_5_, 0);
		return class528;
	}

	static final void method8753(Class527 class527, byte i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		((Class527) class527).anInt7012 -= 283782002;
		int i_9_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_10_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]);
		Class437 class437 = Class125.aClass424_1573.method7069(i_9_, (byte) 0);
		if (i_10_ != -1741480635 * class437.anInt5337)
			class118.method1999(i_9_, i_10_, -76076232);
		else
			class118.method1995(i_9_, -1265527536);
	}

	static final void method8754(Class118 class118, Class98 class98, Class527 class527, int i) {
		class118.aBool1316 = ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1);
		Class109.method1858(class118, (byte) 19);
	}

	public static String method8755(RsByteBuffer class282_sub35, byte i) {
		return Class282_Sub33.method12582(class282_sub35, 32767, -2080210989);
	}

	static final void method8756(Class527 class527, int i) {
		int i_11_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		if (client.aString7426 != null && i_11_ < -1772444859 * Class459.anInt5534)
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = Class467.aClass173Array5575[i_11_].aByte2126;
		else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 0;
	}

	static final void method8757(Class527 class527, int i) {
		int i_12_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		if (i_12_ == -1)
			throw new RuntimeException();
		Class431 class431 = Class466.aClass444_5570.method7424(i_12_, (byte) 8);
		if ('s' != class431.aChar5140)
			throw new RuntimeException();
		int[] is = class431.method7251(string, 1883860951);
		int i_13_ = 0;
		if (null != is)
			i_13_ = is.length;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_13_;
	}

	static final void method8758(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_14_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		int i_15_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]);
		Class437 class437 = Class125.aClass424_1573.method7069(i_15_, (byte) 0);
		if (class437.method7319(2036753920))
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = Class397.aClass218_4813.method3700(i_14_, 882562729).method3722(i_15_, class437.aString5335, -379285425);
		else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = (Class397.aClass218_4813.method3700(i_14_, 884887679).method3723(i_15_, -1741480635 * class437.anInt5337, 1942118537));
	}
}
