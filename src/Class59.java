/* Class59 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class59 {
	public short aShort530;
	public static final int anInt531 = 1;
	public static final int anInt532 = 2;
	static Class229 aClass229_533 = new Class229(64);
	public boolean aBool534;
	public int anInt535;
	public short aShort536;
	public int anInt537;
	public int anInt538;
	public int anInt539;
	public int anInt540;
	public boolean aBool541;
	public int anInt542 = 0;
	int anInt543;
	public int anInt544;
	public int anInt545;
	public int anInt546;
	public int anInt547 = 34253233;
	int anInt548;
	int anInt549;
	public boolean aBool550;
	public int anInt551;
	int anInt552;
	int anInt553;
	public int anInt554;
	public int anInt555;
	public int anInt556;
	public int anInt557;
	public int anInt558;
	public int[] anIntArray559;
	static final int anInt560 = 0;
	public int[] anIntArray561;
	public int[] anIntArray562;
	public static final int anInt563 = -1;
	public int anInt564;
	public int anInt565;
	int anInt566;
	public boolean aBool567;
	public boolean aBool568;
	public int anInt569;
	public short aShort570;
	public int anInt571;
	public boolean aBool572;
	public int anInt573;
	public boolean aBool574;
	int anInt575;
	public boolean aBool576;
	public int anInt577;
	public boolean aBool578;
	public short aShort579;
	public int anInt580;
	public int anInt581;
	public int[] anIntArray582;
	public int anInt583;
	public int anInt584;
	public int anInt585;
	int anInt586;
	public int anInt587;
	public int anInt588;
	int anInt589;
	public int anInt590;
	public int anInt591;
	public int anInt592;
	public int anInt593;
	public int anInt594;
	public int anInt595;
	public int anInt596;
	public int anInt597;
	public int anInt598;
	int anInt599;
	public int anInt600;

	void method1143() {
		if (157076069 * anInt591 > -2 || 1307285259 * anInt600 > -2)
			aBool578 = true;
		anInt565 = 1591282751 * (2139574449 * ((Class59) this).anInt548 >> 16 & 0xff);
		((Class59) this).anInt566 = ((1739843937 * ((Class59) this).anInt549 >> 16 & 0xff) * 1828342823);
		anInt581 = -1563028295 * ((Class59) this).anInt566 - 1652210705 * anInt565;
		anInt551 = (((Class59) this).anInt548 * 2139574449 >> 8 & 0xff) * 721201205;
		((Class59) this).anInt599 = ((1739843937 * ((Class59) this).anInt549 >> 8 & 0xff) * 1804934911);
		anInt584 = ((Class59) this).anInt599 * -559103595 - anInt551 * -365125857;
		anInt585 = -1535367401 * (2139574449 * ((Class59) this).anInt548 & 0xff);
		((Class59) this).anInt586 = 1856863003 * (1739843937 * ((Class59) this).anInt549 & 0xff);
		anInt587 = 49359941 * ((Class59) this).anInt586 - anInt585 * -909726639;
		anInt588 = ((2139574449 * ((Class59) this).anInt548 >> 24 & 0xff) * -1649224695);
		((Class59) this).anInt575 = -174974489 * (((Class59) this).anInt549 * 1739843937 >> 24 & 0xff);
		anInt590 = ((Class59) this).anInt575 * 1958297035 - 384968965 * anInt588;
		if (0 != 1726042923 * anInt564) {
			anInt558 = -969486017 * (35835201 * ((Class59) this).anInt552 * (anInt556 * 1730929631) / 100);
			anInt592 = (((Class59) this).anInt553 * 1217315751 * (anInt556 * 1730929631) / 100 * -567299913);
			if (anInt558 * -439640385 == 0)
				anInt558 = -969486017;
			anInt593 = (((anInt564 * 1726042923 >> 16 & 0xff) - (anInt581 * 1058779855 / 2 + -53681217 * anInt565)) << 8) / (anInt558 * -439640385) * 1238347305;
			anInt594 = (((anInt564 * 1726042923 >> 8 & 0xff) - (1513056797 * anInt551 + anInt584 * -87647421 / 2)) << 8) / (-439640385 * anInt558) * 1102495059;
			anInt595 = -507267083 * ((((anInt564 * 1726042923 & 0xff) - (-1314769033 * anInt587 / 2 + -2036236121 * anInt585)) << 8) / (-439640385 * anInt558));
			if (1148071175 * anInt592 == 0)
				anInt592 = -567299913;
			anInt596 = 2103138733 * ((((1726042923 * anInt564 >> 24 & 0xff) - (59671205 * anInt590 / 2 + -1834026439 * anInt588)) << 8) / (anInt592 * 1148071175));
			Class59 class59_0_ = this;
			class59_0_.anInt593 = (class59_0_.anInt593 + (609996313 * anInt593 > 0 ? -4 : 4) * 1238347305);
			Class59 class59_1_ = this;
			class59_1_.anInt594 = class59_1_.anInt594 + 1102495059 * (anInt594 * 1220054747 > 0 ? -4 : 4);
			Class59 class59_2_ = this;
			class59_2_.anInt595 = class59_2_.anInt595 + -507267083 * (anInt595 * -772932515 > 0 ? -4 : 4);
			Class59 class59_3_ = this;
			class59_3_.anInt596 = (class59_3_.anInt596 + (-386829787 * anInt596 > 0 ? -4 : 4) * 2103138733);
		}
		if (-1 != anInt547 * 1033240751) {
			anInt597 = (anInt556 * 1730929631 * (-1228463773 * ((Class59) this).anInt543) / 100 * -229097333);
			if (-1114757341 * anInt597 == 0)
				anInt597 = -229097333;
			anInt598 = 1071432759 * ((1033240751 * anInt547 - ((-1453312369 * anInt539 - anInt538 * -768945749) / 2 + anInt538 * -768945749)) / (anInt597 * -1114757341));
		}
		if (anInt583 * -1575212911 != -1) {
			anInt580 = -729297961 * (((Class59) this).anInt589 * -454067577 * (anInt556 * 1730929631) / 100);
			if (-130590745 * anInt580 == 0)
				anInt580 = -729297961;
			anInt540 = ((anInt583 * -1575212911 - (1234374815 * anInt544 + (anInt577 * 197487861 - 1234374815 * anInt544) / 2)) / (anInt580 * -130590745) * 781691689);
		}
	}

	public static void method1144() {
		aClass229_533.method3859(-663605587);
	}

	void method1145(RsByteBuffer class282_sub35, int i, int i_4_) {
		if (1 == i) {
			aShort530 = (short) class282_sub35.readUnsignedShort();
			aShort570 = (short) class282_sub35.readUnsignedShort();
			aShort536 = (short) class282_sub35.readUnsignedShort();
			aShort579 = (short) class282_sub35.readUnsignedShort();
			int i_5_ = 3;
			aShort530 <<= i_5_;
			aShort570 <<= i_5_;
			aShort536 <<= i_5_;
			aShort579 <<= i_5_;
		} else if (i == 2)
			class282_sub35.readUnsignedByte();
		else if (i == 3) {
			anInt538 = class282_sub35.readIntLE() * 1441374467;
			anInt539 = class282_sub35.readIntLE() * -1105319825;
		} else if (i == 4) {
			anInt542 = class282_sub35.readUnsignedByte() * 939781303;
			anInt569 = class282_sub35.method13236((short) -6816) * 1178282087;
		} else if (5 == i)
			anInt544 = (anInt577 = (class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797) * -821824021;
		else if (i == 6) {
			((Class59) this).anInt548 = class282_sub35.readIntLE() * 1900345425;
			((Class59) this).anInt549 = class282_sub35.readIntLE() * -471696735;
		} else if (i == 7) {
			anInt555 = class282_sub35.readUnsignedShort() * 1851666849;
			anInt556 = class282_sub35.readUnsignedShort() * -1577215969;
		} else if (i == 8) {
			anInt546 = class282_sub35.readUnsignedShort() * 900684163;
			anInt545 = class282_sub35.readUnsignedShort() * 392654125;
		} else if (i == 9) {
			int i_6_ = class282_sub35.readUnsignedByte();
			anIntArray559 = new int[i_6_];
			for (int i_7_ = 0; i_7_ < i_6_; i_7_++)
				anIntArray559[i_7_] = class282_sub35.readUnsignedShort();
		} else if (10 == i) {
			int i_8_ = class282_sub35.readUnsignedByte();
			anIntArray561 = new int[i_8_];
			for (int i_9_ = 0; i_9_ < i_8_; i_9_++)
				anIntArray561[i_9_] = class282_sub35.readUnsignedShort();
		} else if (12 == i)
			anInt591 = class282_sub35.method13236((short) -5565) * 768071533;
		else if (13 == i)
			anInt600 = class282_sub35.method13236((short) -1229) * -1594819421;
		else if (14 == i)
			anInt557 = class282_sub35.readUnsignedShort() * -1601265113;
		else if (15 == i)
			anInt554 = class282_sub35.readUnsignedShort() * -301948289;
		else if (i == 16) {
			aBool567 = class282_sub35.readUnsignedByte() == 1;
			anInt537 = class282_sub35.readUnsignedShort() * -1410156733;
			anInt535 = class282_sub35.readUnsignedShort() * 1879519373;
			aBool568 = class282_sub35.readUnsignedByte() == 1;
		} else if (17 == i)
			anInt573 = class282_sub35.readUnsignedShort() * 898464473;
		else if (18 == i)
			anInt564 = class282_sub35.readIntLE() * -831335549;
		else if (19 == i)
			anInt571 = class282_sub35.readUnsignedByte() * 1015974473;
		else if (i == 20)
			((Class59) this).anInt552 = class282_sub35.readUnsignedByte() * -1640447295;
		else if (i == 21)
			((Class59) this).anInt553 = class282_sub35.readUnsignedByte() * 1223537687;
		else if (i == 22)
			anInt547 = class282_sub35.readIntLE() * -34253233;
		else if (i == 23)
			((Class59) this).anInt543 = class282_sub35.readUnsignedByte() * -917280693;
		else if (i == 24)
			aBool550 = false;
		else if (25 == i) {
			int i_10_ = class282_sub35.readUnsignedByte();
			anIntArray582 = new int[i_10_];
			for (int i_11_ = 0; i_11_ < i_10_; i_11_++)
				anIntArray582[i_11_] = class282_sub35.readUnsignedShort();
		} else if (i == 26)
			aBool572 = false;
		else if (27 == i)
			anInt583 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 2143066225);
		else if (i == 28)
			((Class59) this).anInt589 = class282_sub35.readUnsignedByte() * 390605623;
		else if (i == 29)
			class282_sub35.method13081(1689772952);
		else if (30 == i)
			aBool574 = true;
		else if (i == 31) {
			anInt544 = ((class282_sub35.readUnsignedShort() << 12 << 2) * -1249244321);
			anInt577 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797);
		} else if (i == 32)
			aBool534 = false;
		else if (i == 33)
			aBool576 = true;
		else if (34 == i)
			aBool541 = false;
	}

	void method1146(byte i) {
		if (157076069 * anInt591 > -2 || 1307285259 * anInt600 > -2)
			aBool578 = true;
		anInt565 = 1591282751 * (2139574449 * ((Class59) this).anInt548 >> 16 & 0xff);
		((Class59) this).anInt566 = ((1739843937 * ((Class59) this).anInt549 >> 16 & 0xff) * 1828342823);
		anInt581 = -1563028295 * ((Class59) this).anInt566 - 1652210705 * anInt565;
		anInt551 = (((Class59) this).anInt548 * 2139574449 >> 8 & 0xff) * 721201205;
		((Class59) this).anInt599 = ((1739843937 * ((Class59) this).anInt549 >> 8 & 0xff) * 1804934911);
		anInt584 = ((Class59) this).anInt599 * -559103595 - anInt551 * -365125857;
		anInt585 = -1535367401 * (2139574449 * ((Class59) this).anInt548 & 0xff);
		((Class59) this).anInt586 = 1856863003 * (1739843937 * ((Class59) this).anInt549 & 0xff);
		anInt587 = 49359941 * ((Class59) this).anInt586 - anInt585 * -909726639;
		anInt588 = ((2139574449 * ((Class59) this).anInt548 >> 24 & 0xff) * -1649224695);
		((Class59) this).anInt575 = -174974489 * (((Class59) this).anInt549 * 1739843937 >> 24 & 0xff);
		anInt590 = ((Class59) this).anInt575 * 1958297035 - 384968965 * anInt588;
		if (0 != 1726042923 * anInt564) {
			anInt558 = -969486017 * (35835201 * ((Class59) this).anInt552 * (anInt556 * 1730929631) / 100);
			anInt592 = (((Class59) this).anInt553 * 1217315751 * (anInt556 * 1730929631) / 100 * -567299913);
			if (anInt558 * -439640385 == 0)
				anInt558 = -969486017;
			anInt593 = (((anInt564 * 1726042923 >> 16 & 0xff) - (anInt581 * 1058779855 / 2 + -53681217 * anInt565)) << 8) / (anInt558 * -439640385) * 1238347305;
			anInt594 = (((anInt564 * 1726042923 >> 8 & 0xff) - (1513056797 * anInt551 + anInt584 * -87647421 / 2)) << 8) / (-439640385 * anInt558) * 1102495059;
			anInt595 = -507267083 * ((((anInt564 * 1726042923 & 0xff) - (-1314769033 * anInt587 / 2 + -2036236121 * anInt585)) << 8) / (-439640385 * anInt558));
			if (1148071175 * anInt592 == 0)
				anInt592 = -567299913;
			anInt596 = 2103138733 * ((((1726042923 * anInt564 >> 24 & 0xff) - (59671205 * anInt590 / 2 + -1834026439 * anInt588)) << 8) / (anInt592 * 1148071175));
			Class59 class59_12_ = this;
			class59_12_.anInt593 = (class59_12_.anInt593 + (609996313 * anInt593 > 0 ? -4 : 4) * 1238347305);
			Class59 class59_13_ = this;
			class59_13_.anInt594 = (class59_13_.anInt594 + 1102495059 * (anInt594 * 1220054747 > 0 ? -4 : 4));
			Class59 class59_14_ = this;
			class59_14_.anInt595 = (class59_14_.anInt595 + -507267083 * (anInt595 * -772932515 > 0 ? -4 : 4));
			Class59 class59_15_ = this;
			class59_15_.anInt596 = (class59_15_.anInt596 + (-386829787 * anInt596 > 0 ? -4 : 4) * 2103138733);
		}
		if (-1 != anInt547 * 1033240751) {
			anInt597 = (anInt556 * 1730929631 * (-1228463773 * ((Class59) this).anInt543) / 100 * -229097333);
			if (-1114757341 * anInt597 == 0)
				anInt597 = -229097333;
			anInt598 = 1071432759 * ((1033240751 * anInt547 - ((-1453312369 * anInt539 - anInt538 * -768945749) / 2 + anInt538 * -768945749)) / (anInt597 * -1114757341));
		}
		if (anInt583 * -1575212911 != -1) {
			anInt580 = -729297961 * (((Class59) this).anInt589 * -454067577 * (anInt556 * 1730929631) / 100);
			if (-130590745 * anInt580 == 0)
				anInt580 = -729297961;
			anInt540 = ((anInt583 * -1575212911 - (1234374815 * anInt544 + (anInt577 * 197487861 - 1234374815 * anInt544) / 2)) / (anInt580 * -130590745) * 781691689);
		}
	}

	void method1147(RsByteBuffer class282_sub35, int i) {
		if (1 == i) {
			aShort530 = (short) class282_sub35.readUnsignedShort();
			aShort570 = (short) class282_sub35.readUnsignedShort();
			aShort536 = (short) class282_sub35.readUnsignedShort();
			aShort579 = (short) class282_sub35.readUnsignedShort();
			int i_16_ = 3;
			aShort530 <<= i_16_;
			aShort570 <<= i_16_;
			aShort536 <<= i_16_;
			aShort579 <<= i_16_;
		} else if (i == 2)
			class282_sub35.readUnsignedByte();
		else if (i == 3) {
			anInt538 = class282_sub35.readIntLE() * 1441374467;
			anInt539 = class282_sub35.readIntLE() * -1105319825;
		} else if (i == 4) {
			anInt542 = class282_sub35.readUnsignedByte() * 939781303;
			anInt569 = class282_sub35.method13236((short) -18146) * 1178282087;
		} else if (5 == i)
			anInt544 = (anInt577 = (class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797) * -821824021;
		else if (i == 6) {
			((Class59) this).anInt548 = class282_sub35.readIntLE() * 1900345425;
			((Class59) this).anInt549 = class282_sub35.readIntLE() * -471696735;
		} else if (i == 7) {
			anInt555 = class282_sub35.readUnsignedShort() * 1851666849;
			anInt556 = class282_sub35.readUnsignedShort() * -1577215969;
		} else if (i == 8) {
			anInt546 = class282_sub35.readUnsignedShort() * 900684163;
			anInt545 = class282_sub35.readUnsignedShort() * 392654125;
		} else if (i == 9) {
			int i_17_ = class282_sub35.readUnsignedByte();
			anIntArray559 = new int[i_17_];
			for (int i_18_ = 0; i_18_ < i_17_; i_18_++)
				anIntArray559[i_18_] = class282_sub35.readUnsignedShort();
		} else if (10 == i) {
			int i_19_ = class282_sub35.readUnsignedByte();
			anIntArray561 = new int[i_19_];
			for (int i_20_ = 0; i_20_ < i_19_; i_20_++)
				anIntArray561[i_20_] = class282_sub35.readUnsignedShort();
		} else if (12 == i)
			anInt591 = class282_sub35.method13236((short) -14615) * 768071533;
		else if (13 == i)
			anInt600 = class282_sub35.method13236((short) 238) * -1594819421;
		else if (14 == i)
			anInt557 = class282_sub35.readUnsignedShort() * -1601265113;
		else if (15 == i)
			anInt554 = class282_sub35.readUnsignedShort() * -301948289;
		else if (i == 16) {
			aBool567 = class282_sub35.readUnsignedByte() == 1;
			anInt537 = class282_sub35.readUnsignedShort() * -1410156733;
			anInt535 = class282_sub35.readUnsignedShort() * 1879519373;
			aBool568 = class282_sub35.readUnsignedByte() == 1;
		} else if (17 == i)
			anInt573 = class282_sub35.readUnsignedShort() * 898464473;
		else if (18 == i)
			anInt564 = class282_sub35.readIntLE() * -831335549;
		else if (19 == i)
			anInt571 = class282_sub35.readUnsignedByte() * 1015974473;
		else if (i == 20)
			((Class59) this).anInt552 = class282_sub35.readUnsignedByte() * -1640447295;
		else if (i == 21)
			((Class59) this).anInt553 = class282_sub35.readUnsignedByte() * 1223537687;
		else if (i == 22)
			anInt547 = class282_sub35.readIntLE() * -34253233;
		else if (i == 23)
			((Class59) this).anInt543 = class282_sub35.readUnsignedByte() * -917280693;
		else if (i == 24)
			aBool550 = false;
		else if (25 == i) {
			int i_21_ = class282_sub35.readUnsignedByte();
			anIntArray582 = new int[i_21_];
			for (int i_22_ = 0; i_22_ < i_21_; i_22_++)
				anIntArray582[i_22_] = class282_sub35.readUnsignedShort();
		} else if (i == 26)
			aBool572 = false;
		else if (27 == i)
			anInt583 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 2143066225);
		else if (i == 28)
			((Class59) this).anInt589 = class282_sub35.readUnsignedByte() * 390605623;
		else if (i == 29)
			class282_sub35.method13081(1683622921);
		else if (30 == i)
			aBool574 = true;
		else if (i == 31) {
			anInt544 = ((class282_sub35.readUnsignedShort() << 12 << 2) * -1249244321);
			anInt577 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797);
		} else if (i == 32)
			aBool534 = false;
		else if (i == 33)
			aBool576 = true;
		else if (34 == i)
			aBool541 = false;
	}

	public static void method1148(Class317 class317) {
		Class219.aClass317_2710 = class317;
	}

	public static Class59 method1149(int i) {
		Class59 class59 = (Class59) aClass229_533.method3865((long) i);
		if (class59 != null)
			return class59;
		byte[] is = Class219.aClass317_2710.method5607(0, i, -1194205238);
		class59 = new Class59();
		if (null != is)
			class59.method1150(new RsByteBuffer(is), -613926670);
		class59.method1146((byte) 1);
		aClass229_533.method3856(class59, (long) i);
		return class59;
	}

	Class59() {
		((Class59) this).anInt543 = -1533756084;
		anInt583 = -2143066225;
		((Class59) this).anInt589 = 405856636;
		aBool550 = true;
		((Class59) this).anInt552 = -835972252;
		((Class59) this).anInt553 = 2094684412;
		anInt554 = 301948289;
		anInt591 = -1536143066;
		anInt600 = -1105328454;
		anInt557 = 0;
		aBool567 = true;
		aBool568 = true;
		anInt537 = 1410156733;
		anInt535 = -1879519373;
		anInt571 = 0;
		aBool572 = true;
		anInt573 = -898464473;
		aBool574 = false;
		aBool534 = true;
		aBool576 = false;
		aBool541 = true;
		aBool578 = false;
	}

	void method1150(RsByteBuffer class282_sub35, int i) {
		for (;;) {
			int i_23_ = class282_sub35.readUnsignedByte();
			if (0 == i_23_)
				break;
			method1145(class282_sub35, i_23_, -762162667);
		}
	}

	void method1151(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (0 == i)
				break;
			method1145(class282_sub35, i, -294762727);
		}
	}

	void method1152(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (0 == i)
				break;
			method1145(class282_sub35, i, -1928606246);
		}
	}

	public static void method1153() {
		aClass229_533.method3859(1606570900);
	}

	void method1154(RsByteBuffer class282_sub35, int i) {
		if (1 == i) {
			aShort530 = (short) class282_sub35.readUnsignedShort();
			aShort570 = (short) class282_sub35.readUnsignedShort();
			aShort536 = (short) class282_sub35.readUnsignedShort();
			aShort579 = (short) class282_sub35.readUnsignedShort();
			int i_24_ = 3;
			aShort530 <<= i_24_;
			aShort570 <<= i_24_;
			aShort536 <<= i_24_;
			aShort579 <<= i_24_;
		} else if (i == 2)
			class282_sub35.readUnsignedByte();
		else if (i == 3) {
			anInt538 = class282_sub35.readIntLE() * 1441374467;
			anInt539 = class282_sub35.readIntLE() * -1105319825;
		} else if (i == 4) {
			anInt542 = class282_sub35.readUnsignedByte() * 939781303;
			anInt569 = class282_sub35.method13236((short) -2161) * 1178282087;
		} else if (5 == i)
			anInt544 = (anInt577 = (class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797) * -821824021;
		else if (i == 6) {
			((Class59) this).anInt548 = class282_sub35.readIntLE() * 1900345425;
			((Class59) this).anInt549 = class282_sub35.readIntLE() * -471696735;
		} else if (i == 7) {
			anInt555 = class282_sub35.readUnsignedShort() * 1851666849;
			anInt556 = class282_sub35.readUnsignedShort() * -1577215969;
		} else if (i == 8) {
			anInt546 = class282_sub35.readUnsignedShort() * 900684163;
			anInt545 = class282_sub35.readUnsignedShort() * 392654125;
		} else if (i == 9) {
			int i_25_ = class282_sub35.readUnsignedByte();
			anIntArray559 = new int[i_25_];
			for (int i_26_ = 0; i_26_ < i_25_; i_26_++)
				anIntArray559[i_26_] = class282_sub35.readUnsignedShort();
		} else if (10 == i) {
			int i_27_ = class282_sub35.readUnsignedByte();
			anIntArray561 = new int[i_27_];
			for (int i_28_ = 0; i_28_ < i_27_; i_28_++)
				anIntArray561[i_28_] = class282_sub35.readUnsignedShort();
		} else if (12 == i)
			anInt591 = class282_sub35.method13236((short) -18888) * 768071533;
		else if (13 == i)
			anInt600 = class282_sub35.method13236((short) -19031) * -1594819421;
		else if (14 == i)
			anInt557 = class282_sub35.readUnsignedShort() * -1601265113;
		else if (15 == i)
			anInt554 = class282_sub35.readUnsignedShort() * -301948289;
		else if (i == 16) {
			aBool567 = class282_sub35.readUnsignedByte() == 1;
			anInt537 = class282_sub35.readUnsignedShort() * -1410156733;
			anInt535 = class282_sub35.readUnsignedShort() * 1879519373;
			aBool568 = class282_sub35.readUnsignedByte() == 1;
		} else if (17 == i)
			anInt573 = class282_sub35.readUnsignedShort() * 898464473;
		else if (18 == i)
			anInt564 = class282_sub35.readIntLE() * -831335549;
		else if (19 == i)
			anInt571 = class282_sub35.readUnsignedByte() * 1015974473;
		else if (i == 20)
			((Class59) this).anInt552 = class282_sub35.readUnsignedByte() * -1640447295;
		else if (i == 21)
			((Class59) this).anInt553 = class282_sub35.readUnsignedByte() * 1223537687;
		else if (i == 22)
			anInt547 = class282_sub35.readIntLE() * -34253233;
		else if (i == 23)
			((Class59) this).anInt543 = class282_sub35.readUnsignedByte() * -917280693;
		else if (i == 24)
			aBool550 = false;
		else if (25 == i) {
			int i_29_ = class282_sub35.readUnsignedByte();
			anIntArray582 = new int[i_29_];
			for (int i_30_ = 0; i_30_ < i_29_; i_30_++)
				anIntArray582[i_30_] = class282_sub35.readUnsignedShort();
		} else if (i == 26)
			aBool572 = false;
		else if (27 == i)
			anInt583 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 2143066225);
		else if (i == 28)
			((Class59) this).anInt589 = class282_sub35.readUnsignedByte() * 390605623;
		else if (i == 29)
			class282_sub35.method13081(1630013030);
		else if (30 == i)
			aBool574 = true;
		else if (i == 31) {
			anInt544 = ((class282_sub35.readUnsignedShort() << 12 << 2) * -1249244321);
			anInt577 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797);
		} else if (i == 32)
			aBool534 = false;
		else if (i == 33)
			aBool576 = true;
		else if (34 == i)
			aBool541 = false;
	}

	void method1155(RsByteBuffer class282_sub35, int i) {
		if (1 == i) {
			aShort530 = (short) class282_sub35.readUnsignedShort();
			aShort570 = (short) class282_sub35.readUnsignedShort();
			aShort536 = (short) class282_sub35.readUnsignedShort();
			aShort579 = (short) class282_sub35.readUnsignedShort();
			int i_31_ = 3;
			aShort530 <<= i_31_;
			aShort570 <<= i_31_;
			aShort536 <<= i_31_;
			aShort579 <<= i_31_;
		} else if (i == 2)
			class282_sub35.readUnsignedByte();
		else if (i == 3) {
			anInt538 = class282_sub35.readIntLE() * 1441374467;
			anInt539 = class282_sub35.readIntLE() * -1105319825;
		} else if (i == 4) {
			anInt542 = class282_sub35.readUnsignedByte() * 939781303;
			anInt569 = class282_sub35.method13236((short) -11693) * 1178282087;
		} else if (5 == i)
			anInt544 = (anInt577 = (class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797) * -821824021;
		else if (i == 6) {
			((Class59) this).anInt548 = class282_sub35.readIntLE() * 1900345425;
			((Class59) this).anInt549 = class282_sub35.readIntLE() * -471696735;
		} else if (i == 7) {
			anInt555 = class282_sub35.readUnsignedShort() * 1851666849;
			anInt556 = class282_sub35.readUnsignedShort() * -1577215969;
		} else if (i == 8) {
			anInt546 = class282_sub35.readUnsignedShort() * 900684163;
			anInt545 = class282_sub35.readUnsignedShort() * 392654125;
		} else if (i == 9) {
			int i_32_ = class282_sub35.readUnsignedByte();
			anIntArray559 = new int[i_32_];
			for (int i_33_ = 0; i_33_ < i_32_; i_33_++)
				anIntArray559[i_33_] = class282_sub35.readUnsignedShort();
		} else if (10 == i) {
			int i_34_ = class282_sub35.readUnsignedByte();
			anIntArray561 = new int[i_34_];
			for (int i_35_ = 0; i_35_ < i_34_; i_35_++)
				anIntArray561[i_35_] = class282_sub35.readUnsignedShort();
		} else if (12 == i)
			anInt591 = class282_sub35.method13236((short) -15734) * 768071533;
		else if (13 == i)
			anInt600 = class282_sub35.method13236((short) -32719) * -1594819421;
		else if (14 == i)
			anInt557 = class282_sub35.readUnsignedShort() * -1601265113;
		else if (15 == i)
			anInt554 = class282_sub35.readUnsignedShort() * -301948289;
		else if (i == 16) {
			aBool567 = class282_sub35.readUnsignedByte() == 1;
			anInt537 = class282_sub35.readUnsignedShort() * -1410156733;
			anInt535 = class282_sub35.readUnsignedShort() * 1879519373;
			aBool568 = class282_sub35.readUnsignedByte() == 1;
		} else if (17 == i)
			anInt573 = class282_sub35.readUnsignedShort() * 898464473;
		else if (18 == i)
			anInt564 = class282_sub35.readIntLE() * -831335549;
		else if (19 == i)
			anInt571 = class282_sub35.readUnsignedByte() * 1015974473;
		else if (i == 20)
			((Class59) this).anInt552 = class282_sub35.readUnsignedByte() * -1640447295;
		else if (i == 21)
			((Class59) this).anInt553 = class282_sub35.readUnsignedByte() * 1223537687;
		else if (i == 22)
			anInt547 = class282_sub35.readIntLE() * -34253233;
		else if (i == 23)
			((Class59) this).anInt543 = class282_sub35.readUnsignedByte() * -917280693;
		else if (i == 24)
			aBool550 = false;
		else if (25 == i) {
			int i_36_ = class282_sub35.readUnsignedByte();
			anIntArray582 = new int[i_36_];
			for (int i_37_ = 0; i_37_ < i_36_; i_37_++)
				anIntArray582[i_37_] = class282_sub35.readUnsignedShort();
		} else if (i == 26)
			aBool572 = false;
		else if (27 == i)
			anInt583 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 2143066225);
		else if (i == 28)
			((Class59) this).anInt589 = class282_sub35.readUnsignedByte() * 390605623;
		else if (i == 29)
			class282_sub35.method13081(2105369865);
		else if (30 == i)
			aBool574 = true;
		else if (i == 31) {
			anInt544 = ((class282_sub35.readUnsignedShort() << 12 << 2) * -1249244321);
			anInt577 = ((class282_sub35.readUnsignedShort() << 12 << 2) * 1730600797);
		} else if (i == 32)
			aBool534 = false;
		else if (i == 33)
			aBool576 = true;
		else if (34 == i)
			aBool541 = false;
	}

	public static Class59 method1156(int i) {
		Class59 class59 = (Class59) aClass229_533.method3865((long) i);
		if (class59 != null)
			return class59;
		byte[] is = Class219.aClass317_2710.method5607(0, i, -1943902052);
		class59 = new Class59();
		if (null != is)
			class59.method1150(new RsByteBuffer(is), -1658973926);
		class59.method1146((byte) 1);
		aClass229_533.method3856(class59, (long) i);
		return class59;
	}

	void method1157(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (0 == i)
				break;
			method1145(class282_sub35, i, -1611074335);
		}
	}

	public static void method1158() {
		aClass229_533.method3859(1607534583);
	}

	public static int method1159(Class317 class317, int i) {
		int i_38_ = 0;
		if (class317.method5661(-1633200555 * Class165.anInt2035, 1772789270))
			i_38_++;
		if (class317.method5661(Class475.anInt5622 * -1931867075, 1087630651))
			i_38_++;
		if (class317.method5661(-1739866999 * Class16.anInt135, -1445417233))
			i_38_++;
		if (class317.method5661(460049705 * Class16.anInt142, 1290057314))
			i_38_++;
		if (class317.method5661(566522625 * Class16.anInt137, -1474020229))
			i_38_++;
		if (class317.method5661(Class468_Sub12.anInt7899 * 1306936623, 704936626))
			i_38_++;
		if (class317.method5661(Class13.anInt130 * -862638837, 602886024))
			i_38_++;
		if (class317.method5661(Class16.anInt143 * 1595523019, -1990269462))
			i_38_++;
		if (class317.method5661(Class400.anInt4821 * 1168894047, 245812600))
			i_38_++;
		if (class317.method5661(Class16.anInt141 * -1636343593, -1961759001))
			i_38_++;
		if (class317.method5661(Class165.anInt2037 * -615473415, -521696107))
			i_38_++;
		if (class317.method5661(341095981 * Class271.anInt3330, -80851855))
			i_38_++;
		return i_38_;
	}

	public static void method1160(int i) {
		if (-1 != Class51.anInt488 * -1437926185) {
			Class515.method8862(Class51.anInt488 * -1437926185, -1, -1, false, -1732540658);
			Class51.anInt488 = 562841881;
		}
	}

	public static void method1161(int i, int i_39_, int i_40_, int i_41_, int i_42_, int i_43_, String string, short i_44_) {
		Class275_Sub6 class275_sub6 = new Class275_Sub6();
		((Class275_Sub6) class275_sub6).anInt7857 = -1659492691 * i;
		((Class275_Sub6) class275_sub6).anInt7859 = i_39_ * -1638762805;
		((Class275_Sub6) class275_sub6).anInt7854 = 1979467929 * i_40_;
		((Class275_Sub6) class275_sub6).anInt7858 = (i_42_ + -1809259861 * client.anInt7174) * -1661679617;
		((Class275_Sub6) class275_sub6).anInt7855 = i_41_ * 133782487;
		((Class275_Sub6) class275_sub6).aString7860 = string;
		((Class275_Sub6) class275_sub6).anInt7856 = -442407271 * i_43_;
		client.aClass457_7335.method7649(class275_sub6, 1112580220);
	}

	static final void method1162(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 567564004;
		int i_45_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_46_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]);
		int i_47_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 2]);
		int i_48_ = (((Class527) class527).anIntArray6999[3 + ((Class527) class527).anInt7012 * 1942118537]);
		Class219 class219 = client.aClass257_7353.method4519(988818864);
		Class11.method469(((i_45_ >> 14 & 0x3fff) - 1948093437 * class219.anInt2711), (i_45_ & 0x3fff) - class219.anInt2712 * -1002240017, i_46_ << 2, i_47_, i_48_, (byte) -62);
	}

	public static String method1163(int i, int i_49_) {
		return new StringBuilder().append("<col=").append(Integer.toHexString(i)).append(">").toString();
	}
}
