
/* Class282_Sub53_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Point;

public class Class282_Sub53_Sub1 extends Class282_Sub53 {
	static final int anInt9483 = 169;
	static final int anInt9484 = 161;
	static final int anInt9485 = 162;
	static final int anInt9486 = 163;
	static final int anInt9487 = 164;
	static final int anInt9488 = 519;
	static final int anInt9489 = 166;
	static final int anInt9490 = 168;
	static final int anInt9491 = 174;
	static final int anInt9492 = 513;
	static final int anInt9493 = 170;
	static final int anInt9494 = 171;
	static final int anInt9495 = 173;
	static final int anInt9496 = 167;
	static Class282_Sub53_Sub1[] aClass282_Sub53_Sub1Array9497 = new Class282_Sub53_Sub1[0];
	static final int anInt9498 = 512;
	static final int anInt9499 = 160;
	static final int anInt9500 = 514;
	static final int anInt9501 = 515;
	static final int anInt9502 = 516;
	static final int anInt9503 = 517;
	static final int anInt9504 = 518;
	static final int anInt9505 = 172;
	static final int anInt9506 = 520;
	static final int anInt9507 = 521;
	static final int anInt9508 = 522;
	static final int anInt9509 = 165;
	static final int anInt9510 = 525;
	static final int anInt9511 = 674;
	static final int anInt9512 = 672;
	int anInt9513;
	int anInt9514;
	long aLong9515;
	int anInt9516;
	int anInt9517;

	public int method14729(int i) {
		return 1095341483 * ((Class282_Sub53_Sub1) this).anInt9514;
	}

	public void method13475(int i) {
		synchronized (aClass282_Sub53_Sub1Array9497) {
			if (Class81.anInt803 * 1803498861 < Class268.anInt3307 * -1238258851 - 1)
				aClass282_Sub53_Sub1Array9497[(Class81.anInt803 += 1574444133) * 1803498861 - 1] = this;
		}
	}

	public int method13481(int i) {
		return ((Class282_Sub53_Sub1) this).anInt9516 * -432890295;
	}

	public int method14730(int i) {
		return ((Class282_Sub53_Sub1) this).anInt9513 * -1900902685;
	}

	public boolean method14731(int i) {
		switch (((Class282_Sub53_Sub1) this).anInt9513 * -1900902685) {
		default:
			return false;
		case 160:
		case 161:
		case 162:
		case 163:
		case 164:
		case 165:
		case 166:
		case 167:
		case 168:
		case 169:
		case 170:
		case 171:
		case 172:
		case 173:
		case 174:
		case 672:
		case 674:
			return true;
		}
	}

	public int method13473(byte i) {
		switch (-1900902685 * ((Class282_Sub53_Sub1) this).anInt9513) {
		case 163:
		case 166:
		case 169:
		case 173:
		case 515:
		case 518:
		case 521:
		case 525:
			return 2;
		case 160:
		case 512:
			return 0;
		default:
			return 1;
		}
	}

	public long method13471(int i) {
		return ((Class282_Sub53_Sub1) this).aLong9515 * -4936437369125508737L;
	}

	public void method14732(Point point, int i) {
		((Class282_Sub53_Sub1) this).anInt9516 -= point.x * 1063236601;
		((Class282_Sub53_Sub1) this).anInt9517 -= point.y * -986906997;
	}

	public int method14733() {
		return 1095341483 * ((Class282_Sub53_Sub1) this).anInt9514;
	}

	public int method13470(int i) {
		switch (-1900902685 * ((Class282_Sub53_Sub1) this).anInt9513) {
		case 167:
		case 169:
		case 519:
		case 521:
			return 1;
		case 160:
		case 512:
			return -1;
		case 170:
		case 522:
			return 6;
		case 168:
		case 520:
			return 4;
		case 165:
		case 517:
			return 5;
		case 161:
		case 163:
		case 513:
		case 515:
			return 0;
		case 162:
		case 514:
			return 3;
		case 164:
		case 166:
		case 516:
		case 518:
			return 2;
		default:
			return -2;
		}
	}

	public int method13469(int i) {
		return -498271965 * ((Class282_Sub53_Sub1) this).anInt9517;
	}

	public long method13479() {
		return ((Class282_Sub53_Sub1) this).aLong9515 * -4936437369125508737L;
	}

	Class282_Sub53_Sub1() {
		/* empty */
	}

	public int method13472() {
		return ((Class282_Sub53_Sub1) this).anInt9516 * -432890295;
	}

	public int method13478() {
		return -498271965 * ((Class282_Sub53_Sub1) this).anInt9517;
	}

	public int method13484() {
		return -498271965 * ((Class282_Sub53_Sub1) this).anInt9517;
	}

	public long method13480() {
		return ((Class282_Sub53_Sub1) this).aLong9515 * -4936437369125508737L;
	}

	public int method13483() {
		switch (-1900902685 * ((Class282_Sub53_Sub1) this).anInt9513) {
		case 163:
		case 166:
		case 169:
		case 173:
		case 515:
		case 518:
		case 521:
		case 525:
			return 2;
		case 160:
		case 512:
			return 0;
		default:
			return 1;
		}
	}

	public long method13482() {
		return ((Class282_Sub53_Sub1) this).aLong9515 * -4936437369125508737L;
	}

	public void method13485() {
		synchronized (aClass282_Sub53_Sub1Array9497) {
			if (Class81.anInt803 * 1803498861 < Class268.anInt3307 * -1238258851 - 1)
				aClass282_Sub53_Sub1Array9497[(Class81.anInt803 += 1574444133) * 1803498861 - 1] = this;
		}
	}

	public int method13477() {
		switch (-1900902685 * ((Class282_Sub53_Sub1) this).anInt9513) {
		case 163:
		case 166:
		case 169:
		case 173:
		case 515:
		case 518:
		case 521:
		case 525:
			return 2;
		case 160:
		case 512:
			return 0;
		default:
			return 1;
		}
	}

	public void method13486() {
		synchronized (aClass282_Sub53_Sub1Array9497) {
			if (Class81.anInt803 * 1803498861 < Class268.anInt3307 * -1238258851 - 1)
				aClass282_Sub53_Sub1Array9497[(Class81.anInt803 += 1574444133) * 1803498861 - 1] = this;
		}
	}

	public void method13487() {
		synchronized (aClass282_Sub53_Sub1Array9497) {
			if (Class81.anInt803 * 1803498861 < Class268.anInt3307 * -1238258851 - 1)
				aClass282_Sub53_Sub1Array9497[(Class81.anInt803 += 1574444133) * 1803498861 - 1] = this;
		}
	}

	public int method13488() {
		switch (-1900902685 * ((Class282_Sub53_Sub1) this).anInt9513) {
		case 167:
		case 169:
		case 519:
		case 521:
			return 1;
		case 160:
		case 512:
			return -1;
		case 170:
		case 522:
			return 6;
		case 168:
		case 520:
			return 4;
		case 165:
		case 517:
			return 5;
		case 161:
		case 163:
		case 513:
		case 515:
			return 0;
		case 162:
		case 514:
			return 3;
		case 164:
		case 166:
		case 516:
		case 518:
			return 2;
		default:
			return -2;
		}
	}

	public int method13489() {
		switch (-1900902685 * ((Class282_Sub53_Sub1) this).anInt9513) {
		case 167:
		case 169:
		case 519:
		case 521:
			return 1;
		case 160:
		case 512:
			return -1;
		case 170:
		case 522:
			return 6;
		case 168:
		case 520:
			return 4;
		case 165:
		case 517:
			return 5;
		case 161:
		case 163:
		case 513:
		case 515:
			return 0;
		case 162:
		case 514:
			return 3;
		case 164:
		case 166:
		case 516:
		case 518:
			return 2;
		default:
			return -2;
		}
	}

	public static void method14734(int i) {
		Class268.anInt3307 = i * 2078474485;
		aClass282_Sub53_Sub1Array9497 = new Class282_Sub53_Sub1[i];
		Class81.anInt803 = 0;
	}

	public static void method14735(int i) {
		Class268.anInt3307 = i * 2078474485;
		aClass282_Sub53_Sub1Array9497 = new Class282_Sub53_Sub1[i];
		Class81.anInt803 = 0;
	}

	public static void method14736(int i) {
		Class268.anInt3307 = i * 2078474485;
		aClass282_Sub53_Sub1Array9497 = new Class282_Sub53_Sub1[i];
		Class81.anInt803 = 0;
	}

	public static void method14737(int i) {
		Class268.anInt3307 = i * 2078474485;
		aClass282_Sub53_Sub1Array9497 = new Class282_Sub53_Sub1[i];
		Class81.anInt803 = 0;
	}

	public boolean method14738() {
		switch (((Class282_Sub53_Sub1) this).anInt9513 * -1900902685) {
		default:
			return false;
		case 160:
		case 161:
		case 162:
		case 163:
		case 164:
		case 165:
		case 166:
		case 167:
		case 168:
		case 169:
		case 170:
		case 171:
		case 172:
		case 173:
		case 174:
		case 672:
		case 674:
			return true;
		}
	}

	public void method14739(Point point) {
		((Class282_Sub53_Sub1) this).anInt9516 -= point.x * 1063236601;
		((Class282_Sub53_Sub1) this).anInt9517 -= point.y * -986906997;
	}

	public static Class282_Sub53_Sub1 method14740(int i, int i_0_, int i_1_, int i_2_) {
		synchronized (aClass282_Sub53_Sub1Array9497) {
			Class282_Sub53_Sub1 class282_sub53_sub1;
			if (0 == 1803498861 * Class81.anInt803)
				class282_sub53_sub1 = new Class282_Sub53_Sub1();
			else
				class282_sub53_sub1 = (aClass282_Sub53_Sub1Array9497[(Class81.anInt803 -= 1574444133) * 1803498861]);
			((Class282_Sub53_Sub1) class282_sub53_sub1).anInt9516 = 1063236601 * i;
			((Class282_Sub53_Sub1) class282_sub53_sub1).anInt9517 = i_0_ * -986906997;
			((Class282_Sub53_Sub1) class282_sub53_sub1).anInt9513 = 1264119499 * i_1_;
			((Class282_Sub53_Sub1) class282_sub53_sub1).anInt9514 = -840123133 * i_2_;
			((Class282_Sub53_Sub1) class282_sub53_sub1).aLong9515 = Class169.method2869(1902766739) * -4703652849465796993L;
			Class282_Sub53_Sub1 class282_sub53_sub1_3_ = class282_sub53_sub1;
			return class282_sub53_sub1_3_;
		}
	}

	public int method13476() {
		return ((Class282_Sub53_Sub1) this).anInt9516 * -432890295;
	}

	public int method14741() {
		return 1095341483 * ((Class282_Sub53_Sub1) this).anInt9514;
	}

	public boolean method14742() {
		switch (((Class282_Sub53_Sub1) this).anInt9513 * -1900902685) {
		default:
			return false;
		case 160:
		case 161:
		case 162:
		case 163:
		case 164:
		case 165:
		case 166:
		case 167:
		case 168:
		case 169:
		case 170:
		case 171:
		case 172:
		case 173:
		case 174:
		case 672:
		case 674:
			return true;
		}
	}

	public boolean method14743() {
		switch (((Class282_Sub53_Sub1) this).anInt9513 * -1900902685) {
		default:
			return false;
		case 160:
		case 161:
		case 162:
		case 163:
		case 164:
		case 165:
		case 166:
		case 167:
		case 168:
		case 169:
		case 170:
		case 171:
		case 172:
		case 173:
		case 174:
		case 672:
		case 674:
			return true;
		}
	}

	public void method14744(Point point) {
		((Class282_Sub53_Sub1) this).anInt9516 -= point.x * 1063236601;
		((Class282_Sub53_Sub1) this).anInt9517 -= point.y * -986906997;
	}

	public int method14745() {
		return ((Class282_Sub53_Sub1) this).anInt9513 * -1900902685;
	}

	static final void method14746(Class527 class527, byte i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class151.method2593(class118, class98, class527, -1669147013);
	}
}
