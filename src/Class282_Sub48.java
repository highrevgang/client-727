/* Class282_Sub48 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub48 extends Class282 {
	int anInt8072;
	static Class482 aClass482_8073 = new Class482();
	static Class482 aClass482_8074 = new Class482();
	static Class465 aClass465_8075 = new Class465(16);
	int anInt8076;
	int anInt8077;
	int anInt8078;
	int anInt8079;
	int anInt8080;
	int anInt8081;
	Class282_Sub26_Sub1_Sub1 aClass282_Sub26_Sub1_Sub1_8082;
	static final int anInt8083 = 2;
	static final int anInt8084 = 0;
	Class521_Sub1_Sub1_Sub2_Sub2 aClass521_Sub1_Sub1_Sub2_Sub2_8085;
	Class521_Sub1_Sub1_Sub2_Sub1 aClass521_Sub1_Sub1_Sub2_Sub1_8086;
	Class282_Sub18 aClass282_Sub18_8087;
	static final int anInt8088 = 1;
	int anInt8089;
	static final int anInt8090 = 3;
	int anInt8091 = 0;
	boolean aBool8092;
	int anInt8093;
	int anInt8094;
	int anInt8095;
	Class282_Sub15_Sub5 aClass282_Sub15_Sub5_8096;
	Class282_Sub18 aClass282_Sub18_8097;
	boolean aBool8098;
	Class282_Sub15_Sub5 aClass282_Sub15_Sub5_8099;
	static final int anInt8100 = 512;
	int anInt8101;
	int[] anIntArray8102;
	boolean aBool8103;
	Class478 aClass478_8104;
	int anInt8105;
	Class282_Sub26_Sub1_Sub1 aClass282_Sub26_Sub1_Sub1_8106;
	int anInt8107;

	public static void method13423(boolean bool) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 25); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(1604804541)) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = null;
			}
			class282_sub48.method4991(-371378792);
		}
		if (bool) {
			for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8074.method8097((byte) 33); null != class282_sub48; class282_sub48 = (Class282_Sub48) aClass482_8074.method8067(404069961)) {
				if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				class282_sub48.method4991(-371378792);
			}
			for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass465_8075.method7750(378870532); class282_sub48 != null; class282_sub48 = ((Class282_Sub48) aClass465_8075.method7751((byte) 101))) {
				if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				class282_sub48.method4991(-371378792);
			}
		}
	}

	public static void method13424(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1) {
		Class282_Sub48 class282_sub48 = ((Class282_Sub48) aClass465_8075.method7754((long) ((class521_sub1_sub1_sub2_sub1.anInt10314) * -1691508299)));
		if (class282_sub48 != null) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			class282_sub48.method4991(-371378792);
		}
	}

	public static void method13425(boolean bool) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 88); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(1038575490)) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = null;
			}
			class282_sub48.method4991(-371378792);
		}
		if (bool) {
			for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8074.method8097((byte) 52); null != class282_sub48; class282_sub48 = ((Class282_Sub48) aClass482_8074.method8067(-614816819))) {
				if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				class282_sub48.method4991(-371378792);
			}
			for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass465_8075.method7750(-381668995); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass465_8075.method7751((byte) 53)) {
				if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				class282_sub48.method4991(-371378792);
			}
		}
	}

	void method13426(byte i) {
		int i_0_ = ((Class282_Sub48) this).anInt8095 * 1399704573;
		boolean bool = ((Class282_Sub48) this).aBool8098;
		if (((Class282_Sub48) this).aClass478_8104 != null) {
			Class478 class478 = (((Class282_Sub48) this).aClass478_8104.method8013((-891719545 * client.anInt7341 == 4 ? (Interface42) Class86.anInterface42_832 : Class158_Sub1.aClass3_8507), (byte) 46));
			if (null != class478) {
				((Class282_Sub48) this).anInt8095 = -516170849 * class478.anInt5653;
				((Class282_Sub48) this).aBool8098 = class478.aBool5696;
				((Class282_Sub48) this).anInt8105 = 1837689437 * (-983013495 * class478.anInt5693 << 9);
				((Class282_Sub48) this).anInt8089 = class478.anInt5695 * 1174101755;
				((Class282_Sub48) this).anInt8072 = 1036400279 * class478.anInt5667;
				((Class282_Sub48) this).anInt8101 = -1954755723 * class478.anInt5698;
				((Class282_Sub48) this).anIntArray8102 = class478.anIntArray5688;
				((Class282_Sub48) this).aBool8103 = class478.aBool5700;
				((Class282_Sub48) this).anInt8094 = class478.anInt5709 * 1144293369;
				((Class282_Sub48) this).anInt8093 = class478.anInt5708 * 1225461397;
			} else {
				((Class282_Sub48) this).anInt8095 = 569710251;
				((Class282_Sub48) this).aBool8098 = false;
				((Class282_Sub48) this).anInt8105 = 0;
				((Class282_Sub48) this).anInt8089 = 0;
				((Class282_Sub48) this).anInt8072 = 0;
				((Class282_Sub48) this).anInt8101 = 0;
				((Class282_Sub48) this).anIntArray8102 = null;
				((Class282_Sub48) this).aBool8103 = false;
				((Class282_Sub48) this).anInt8094 = 1769645824;
				((Class282_Sub48) this).anInt8093 = 211241216;
				((Class282_Sub48) this).anInt8081 = 0;
			}
		} else if (null != (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085)) {
			int i_1_ = (Class282_Sub11_Sub1.method15433(((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085, 912866554));
			if (i_0_ != i_1_) {
				((Class282_Sub48) this).anInt8095 = i_1_ * -569710251;
				Class409 class409 = (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580);
				if (class409.anIntArray4886 != null)
					class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
				if (null != class409) {
					((Class282_Sub48) this).anInt8105 = (877155897 * class409.anInt4907 << 9) * 1837689437;
					((Class282_Sub48) this).anInt8081 = 1624181085 * (1412189553 * class409.anInt4908 << 9);
					((Class282_Sub48) this).anInt8089 = class409.anInt4909 * 51937289;
					((Class282_Sub48) this).aBool8098 = class409.aBool4872;
					((Class282_Sub48) this).anInt8094 = 367163663 * class409.anInt4919;
					((Class282_Sub48) this).anInt8093 = 779306973 * class409.anInt4911;
				} else {
					((Class282_Sub48) this).anInt8081 = 0;
					((Class282_Sub48) this).anInt8105 = 0;
					((Class282_Sub48) this).anInt8089 = 0;
					((Class282_Sub48) this).aBool8098 = (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580.aBool4872);
					((Class282_Sub48) this).anInt8094 = 1769645824;
					((Class282_Sub48) this).anInt8093 = 211241216;
				}
			}
		} else if (null != (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086)) {
			((Class282_Sub48) this).anInt8095 = (Class149_Sub2.method14610(((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086, 2122332185)) * -569710251;
			((Class282_Sub48) this).aBool8098 = (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564);
			((Class282_Sub48) this).anInt8105 = ((((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10567) * 1304574447 << 9) * 1837689437;
			((Class282_Sub48) this).anInt8081 = 0;
			((Class282_Sub48) this).anInt8089 = -1517226111 * (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566);
			((Class282_Sub48) this).anInt8094 = 1769645824;
			((Class282_Sub48) this).anInt8093 = 211241216;
		}
		if ((i_0_ != ((Class282_Sub48) this).anInt8095 * 1399704573 || ((Class282_Sub48) this).aBool8098 != bool) && ((Class282_Sub48) this).aClass282_Sub15_Sub5_8099 != null) {
			Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) this).aClass282_Sub15_Sub5_8099);
			((Class282_Sub48) this).aClass282_Sub15_Sub5_8099 = null;
			((Class282_Sub48) this).aClass282_Sub26_Sub1_Sub1_8082 = null;
			((Class282_Sub48) this).aClass282_Sub18_8097 = null;
		}
	}

	static int method13427(Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2) {
		Class409 class409 = class521_sub1_sub1_sub2_sub2.aClass409_10580;
		if (class409.anIntArray4886 != null) {
			class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
			if (class409 == null)
				return -1;
		}
		int i = -1955311273 * class409.anInt4876;
		Class227 class227 = class521_sub1_sub1_sub2_sub2.method15855((byte) -17);
		int i_2_ = class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.method7597(-951708059);
		if (-1 == i_2_ || class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.aBool7891)
			i = -1910117775 * class409.anInt4903;
		else if (class227.anInt2797 * -474675041 == i_2_ || i_2_ == 486149589 * class227.anInt2817 || class227.anInt2800 * -833477781 == i_2_ || 1642803439 * class227.anInt2799 == i_2_)
			i = class409.anInt4906 * -1427347049;
		else if (2055956425 * class227.anInt2801 == i_2_ || class227.anInt2828 * 1053306035 == i_2_ || 1489597113 * class227.anInt2812 == i_2_ || 1879075923 * class227.anInt2803 == i_2_)
			i = 1863998163 * class409.anInt4921;
		return i;
	}

	void method13428() {
		int i = ((Class282_Sub48) this).anInt8095 * 1399704573;
		boolean bool = ((Class282_Sub48) this).aBool8098;
		if (((Class282_Sub48) this).aClass478_8104 != null) {
			Class478 class478 = (((Class282_Sub48) this).aClass478_8104.method8013((-891719545 * client.anInt7341 == 4 ? (Interface42) Class86.anInterface42_832 : Class158_Sub1.aClass3_8507), (byte) 20));
			if (null != class478) {
				((Class282_Sub48) this).anInt8095 = -516170849 * class478.anInt5653;
				((Class282_Sub48) this).aBool8098 = class478.aBool5696;
				((Class282_Sub48) this).anInt8105 = 1837689437 * (-983013495 * class478.anInt5693 << 9);
				((Class282_Sub48) this).anInt8089 = class478.anInt5695 * 1174101755;
				((Class282_Sub48) this).anInt8072 = 1036400279 * class478.anInt5667;
				((Class282_Sub48) this).anInt8101 = -1954755723 * class478.anInt5698;
				((Class282_Sub48) this).anIntArray8102 = class478.anIntArray5688;
				((Class282_Sub48) this).aBool8103 = class478.aBool5700;
				((Class282_Sub48) this).anInt8094 = class478.anInt5709 * 1144293369;
				((Class282_Sub48) this).anInt8093 = class478.anInt5708 * 1225461397;
			} else {
				((Class282_Sub48) this).anInt8095 = 569710251;
				((Class282_Sub48) this).aBool8098 = false;
				((Class282_Sub48) this).anInt8105 = 0;
				((Class282_Sub48) this).anInt8089 = 0;
				((Class282_Sub48) this).anInt8072 = 0;
				((Class282_Sub48) this).anInt8101 = 0;
				((Class282_Sub48) this).anIntArray8102 = null;
				((Class282_Sub48) this).aBool8103 = false;
				((Class282_Sub48) this).anInt8094 = 1769645824;
				((Class282_Sub48) this).anInt8093 = 211241216;
				((Class282_Sub48) this).anInt8081 = 0;
			}
		} else if (null != (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085)) {
			int i_3_ = (Class282_Sub11_Sub1.method15433(((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085, 912866554));
			if (i != i_3_) {
				((Class282_Sub48) this).anInt8095 = i_3_ * -569710251;
				Class409 class409 = (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580);
				if (class409.anIntArray4886 != null)
					class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
				if (null != class409) {
					((Class282_Sub48) this).anInt8105 = (877155897 * class409.anInt4907 << 9) * 1837689437;
					((Class282_Sub48) this).anInt8081 = 1624181085 * (1412189553 * class409.anInt4908 << 9);
					((Class282_Sub48) this).anInt8089 = class409.anInt4909 * 51937289;
					((Class282_Sub48) this).aBool8098 = class409.aBool4872;
					((Class282_Sub48) this).anInt8094 = 367163663 * class409.anInt4919;
					((Class282_Sub48) this).anInt8093 = 779306973 * class409.anInt4911;
				} else {
					((Class282_Sub48) this).anInt8081 = 0;
					((Class282_Sub48) this).anInt8105 = 0;
					((Class282_Sub48) this).anInt8089 = 0;
					((Class282_Sub48) this).aBool8098 = (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580.aBool4872);
					((Class282_Sub48) this).anInt8094 = 1769645824;
					((Class282_Sub48) this).anInt8093 = 211241216;
				}
			}
		} else if (null != (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086)) {
			((Class282_Sub48) this).anInt8095 = (Class149_Sub2.method14610(((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086, 1923287930)) * -569710251;
			((Class282_Sub48) this).aBool8098 = (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564);
			((Class282_Sub48) this).anInt8105 = ((((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10567) * 1304574447 << 9) * 1837689437;
			((Class282_Sub48) this).anInt8081 = 0;
			((Class282_Sub48) this).anInt8089 = -1517226111 * (((Class282_Sub48) this).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566);
			((Class282_Sub48) this).anInt8094 = 1769645824;
			((Class282_Sub48) this).anInt8093 = 211241216;
		}
		if ((i != ((Class282_Sub48) this).anInt8095 * 1399704573 || ((Class282_Sub48) this).aBool8098 != bool) && ((Class282_Sub48) this).aClass282_Sub15_Sub5_8099 != null) {
			Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) this).aClass282_Sub15_Sub5_8099);
			((Class282_Sub48) this).aClass282_Sub15_Sub5_8099 = null;
			((Class282_Sub48) this).aClass282_Sub26_Sub1_Sub1_8082 = null;
			((Class282_Sub48) this).aClass282_Sub18_8097 = null;
		}
	}

	public static void method13429(int i, int i_4_, int i_5_, int i_6_, Class478 class478, Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2, Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1) {
		Class282_Sub48 class282_sub48 = new Class282_Sub48();
		((Class282_Sub48) class282_sub48).anInt8076 = 1589843113 * i;
		((Class282_Sub48) class282_sub48).anInt8107 = (i_4_ << 9) * 80768833;
		((Class282_Sub48) class282_sub48).anInt8078 = 1225707357 * (i_5_ << 9);
		if (class478 != null) {
			((Class282_Sub48) class282_sub48).aClass478_8104 = class478;
			int i_7_ = class478.anInt5648 * -752356381;
			int i_8_ = -1610844647 * class478.anInt5649;
			if (1 == i_6_ || i_6_ == 3) {
				i_7_ = class478.anInt5649 * -1610844647;
				i_8_ = class478.anInt5648 * -752356381;
			}
			((Class282_Sub48) class282_sub48).anInt8079 = (i_7_ + i_4_ << 9) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8077 = (i_8_ + i_5_ << 9) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8095 = -516170849 * class478.anInt5653;
			((Class282_Sub48) class282_sub48).aBool8098 = class478.aBool5696;
			((Class282_Sub48) class282_sub48).anInt8105 = (-983013495 * class478.anInt5693 << 9) * 1837689437;
			((Class282_Sub48) class282_sub48).anInt8089 = 1174101755 * class478.anInt5695;
			((Class282_Sub48) class282_sub48).anInt8072 = class478.anInt5667 * 1036400279;
			((Class282_Sub48) class282_sub48).anInt8101 = -1954755723 * class478.anInt5698;
			((Class282_Sub48) class282_sub48).anIntArray8102 = class478.anIntArray5688;
			((Class282_Sub48) class282_sub48).aBool8103 = class478.aBool5700;
			((Class282_Sub48) class282_sub48).anInt8094 = class478.anInt5709 * 1144293369;
			((Class282_Sub48) class282_sub48).anInt8093 = class478.anInt5708 * 1225461397;
			((Class282_Sub48) class282_sub48).anInt8081 = 1624181085 * (class478.anInt5694 * -1702005631 << 9);
			if (null != class478.anIntArray5650) {
				((Class282_Sub48) class282_sub48).aBool8092 = true;
				class282_sub48.method13426((byte) 1);
			}
			if (null != ((Class282_Sub48) class282_sub48).anIntArray8102)
				((Class282_Sub48) class282_sub48).anInt8080 = (1317424347 * ((Class282_Sub48) class282_sub48).anInt8072 + (int) (Math.random() * (double) (-1084771983 * (((Class282_Sub48) class282_sub48).anInt8101) - (((Class282_Sub48) class282_sub48).anInt8072 * 1317424347)))) * 401671005;
			aClass482_8073.method8059(class282_sub48, -1001316355);
		} else if (class521_sub1_sub1_sub2_sub2 != null) {
			((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085 = class521_sub1_sub1_sub2_sub2;
			Class409 class409 = class521_sub1_sub1_sub2_sub2.aClass409_10580;
			if (null != class409.anIntArray4886) {
				((Class282_Sub48) class282_sub48).aBool8092 = true;
				class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
			}
			if (class409 != null) {
				((Class282_Sub48) class282_sub48).anInt8079 = 1480845005 * (1203434505 * class409.anInt4858 + i_4_ << 9);
				((Class282_Sub48) class282_sub48).anInt8077 = 1710236473 * (class409.anInt4858 * 1203434505 + i_5_ << 9);
				((Class282_Sub48) class282_sub48).anInt8095 = (Class282_Sub11_Sub1.method15433(class521_sub1_sub1_sub2_sub2, 912866554)) * -569710251;
				((Class282_Sub48) class282_sub48).aBool8098 = class409.aBool4872;
				((Class282_Sub48) class282_sub48).anInt8105 = (877155897 * class409.anInt4907 << 9) * 1837689437;
				((Class282_Sub48) class282_sub48).anInt8089 = class409.anInt4909 * 51937289;
				((Class282_Sub48) class282_sub48).anInt8094 = class409.anInt4919 * 367163663;
				((Class282_Sub48) class282_sub48).anInt8093 = 779306973 * class409.anInt4911;
				((Class282_Sub48) class282_sub48).anInt8081 = 1624181085 * (class409.anInt4908 * 1412189553 << 9);
			}
			aClass482_8074.method8059(class282_sub48, 423714533);
		} else if (null != class521_sub1_sub1_sub2_sub1) {
			((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086 = class521_sub1_sub1_sub2_sub1;
			((Class282_Sub48) class282_sub48).anInt8079 = (i_4_ + class521_sub1_sub1_sub2_sub1.method15805(828768449) << 9) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8077 = (i_5_ + class521_sub1_sub1_sub2_sub1.method15805(828768449) << 9) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8095 = Class149_Sub2.method14610(class521_sub1_sub1_sub2_sub1, 1674895020) * -569710251;
			((Class282_Sub48) class282_sub48).aBool8098 = class521_sub1_sub1_sub2_sub1.aBool10564;
			((Class282_Sub48) class282_sub48).anInt8105 = (1837689437 * (1304574447 * class521_sub1_sub1_sub2_sub1.anInt10567 << 9));
			((Class282_Sub48) class282_sub48).anInt8089 = -1517226111 * class521_sub1_sub1_sub2_sub1.anInt10566;
			((Class282_Sub48) class282_sub48).anInt8094 = 1769645824;
			((Class282_Sub48) class282_sub48).anInt8093 = 211241216;
			((Class282_Sub48) class282_sub48).anInt8081 = 0;
			aClass465_8075.method7765(class282_sub48, (long) (-1691508299 * (class521_sub1_sub1_sub2_sub1.anInt10314)));
		}
	}

	public static void method13430(int i, int i_9_, int i_10_, Class478 class478) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 46); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(-81664747)) {
			if (i == -23801959 * ((Class282_Sub48) class282_sub48).anInt8076 && i_9_ << 9 == (-2014043967 * ((Class282_Sub48) class282_sub48).anInt8107) && (((Class282_Sub48) class282_sub48).anInt8078 * 1548025077 == i_10_ << 9) && (((Class282_Sub48) class282_sub48).aClass478_8104.anInt5633 * -2132690865) == -2132690865 * class478.anInt5633) {
				if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 != null) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = null;
				}
				class282_sub48.method4991(-371378792);
				break;
			}
		}
	}

	public static void method13431(int i, int i_11_, int i_12_, Class478 class478) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 69); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(1640557280)) {
			if (i == -23801959 * ((Class282_Sub48) class282_sub48).anInt8076 && i_11_ << 9 == -2014043967 * ((Class282_Sub48) class282_sub48).anInt8107 && (((Class282_Sub48) class282_sub48).anInt8078 * 1548025077 == i_12_ << 9) && (((Class282_Sub48) class282_sub48).aClass478_8104.anInt5633 * -2132690865) == -2132690865 * class478.anInt5633) {
				if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 != null) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = null;
				}
				class282_sub48.method4991(-371378792);
				break;
			}
		}
	}

	public static void method13432(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1) {
		Class282_Sub48 class282_sub48 = ((Class282_Sub48) aClass465_8075.method7754((long) ((class521_sub1_sub1_sub2_sub1.anInt10314) * -1691508299)));
		if (class282_sub48 != null) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			class282_sub48.method4991(-371378792);
		}
	}

	public static void method13433(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1) {
		Class282_Sub48 class282_sub48 = ((Class282_Sub48) aClass465_8075.method7754((long) ((class521_sub1_sub1_sub2_sub1.anInt10314) * -1691508299)));
		if (class282_sub48 != null) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			class282_sub48.method4991(-371378792);
		}
	}

	public static void method13434(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1) {
		Class282_Sub48 class282_sub48 = ((Class282_Sub48) aClass465_8075.method7754((long) ((class521_sub1_sub1_sub2_sub1.anInt10314) * -1691508299)));
		if (class282_sub48 != null) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			class282_sub48.method4991(-371378792);
		}
	}

	static int method13435(Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2) {
		Class409 class409 = class521_sub1_sub1_sub2_sub2.aClass409_10580;
		if (class409.anIntArray4886 != null) {
			class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
			if (class409 == null)
				return -1;
		}
		int i = -1955311273 * class409.anInt4876;
		Class227 class227 = class521_sub1_sub1_sub2_sub2.method15855((byte) -17);
		int i_13_ = class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.method7597(-1531664002);
		if (-1 == i_13_ || class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.aBool7891)
			i = -1910117775 * class409.anInt4903;
		else if (class227.anInt2797 * -474675041 == i_13_ || i_13_ == 486149589 * class227.anInt2817 || class227.anInt2800 * -833477781 == i_13_ || 1642803439 * class227.anInt2799 == i_13_)
			i = class409.anInt4906 * -1427347049;
		else if (2055956425 * class227.anInt2801 == i_13_ || class227.anInt2828 * 1053306035 == i_13_ || 1489597113 * class227.anInt2812 == i_13_ || 1879075923 * class227.anInt2803 == i_13_)
			i = 1863998163 * class409.anInt4921;
		return i;
	}

	public static void method13436(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1) {
		Class282_Sub48 class282_sub48 = ((Class282_Sub48) aClass465_8075.method7754((long) ((class521_sub1_sub1_sub2_sub1.anInt10314) * -1691508299)));
		if (class282_sub48 != null) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			class282_sub48.method4991(-371378792);
		}
	}

	public static void method13437(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1) {
		Class282_Sub48 class282_sub48 = ((Class282_Sub48) (aClass465_8075.method7754((long) (-1691508299 * class521_sub1_sub1_sub2_sub1.anInt10314))));
		if (null == class282_sub48)
			Class397.method6775(class521_sub1_sub1_sub2_sub1.aByte7967, (class521_sub1_sub1_sub2_sub1.anIntArray10356[0]), (class521_sub1_sub1_sub2_sub1.anIntArray10336[0]), 0, null, null, class521_sub1_sub1_sub2_sub1, 1174507006);
		else
			class282_sub48.method13426((byte) 1);
	}

	static int method13438(Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2) {
		Class409 class409 = class521_sub1_sub1_sub2_sub2.aClass409_10580;
		if (class409.anIntArray4886 != null) {
			class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
			if (class409 == null)
				return -1;
		}
		int i = -1955311273 * class409.anInt4876;
		Class227 class227 = class521_sub1_sub1_sub2_sub2.method15855((byte) -17);
		int i_14_ = class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.method7597(-1142631244);
		if (-1 == i_14_ || class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.aBool7891)
			i = -1910117775 * class409.anInt4903;
		else if (class227.anInt2797 * -474675041 == i_14_ || i_14_ == 486149589 * class227.anInt2817 || class227.anInt2800 * -833477781 == i_14_ || 1642803439 * class227.anInt2799 == i_14_)
			i = class409.anInt4906 * -1427347049;
		else if (2055956425 * class227.anInt2801 == i_14_ || class227.anInt2828 * 1053306035 == i_14_ || 1489597113 * class227.anInt2812 == i_14_ || 1879075923 * class227.anInt2803 == i_14_)
			i = 1863998163 * class409.anInt4921;
		return i;
	}

	Class282_Sub48() {
		/* empty */
	}

	public static void method13439(boolean bool) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 112); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(1257613790)) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = null;
			}
			class282_sub48.method4991(-371378792);
		}
		if (bool) {
			for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8074.method8097((byte) 61); null != class282_sub48; class282_sub48 = (Class282_Sub48) aClass482_8074.method8067(264655265)) {
				if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				class282_sub48.method4991(-371378792);
			}
			for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass465_8075.method7750(-121213585); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass465_8075.method7751((byte) 34)) {
				if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
				}
				class282_sub48.method4991(-371378792);
			}
		}
	}

	static int method13440(Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2) {
		Class409 class409 = class521_sub1_sub1_sub2_sub2.aClass409_10580;
		if (class409.anIntArray4886 != null) {
			class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
			if (class409 == null)
				return -1;
		}
		int i = -1955311273 * class409.anInt4876;
		Class227 class227 = class521_sub1_sub1_sub2_sub2.method15855((byte) -17);
		int i_15_ = class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.method7597(-909681611);
		if (-1 == i_15_ || class521_sub1_sub1_sub2_sub2.aClass456_Sub3_10337.aBool7891)
			i = -1910117775 * class409.anInt4903;
		else if (class227.anInt2797 * -474675041 == i_15_ || i_15_ == 486149589 * class227.anInt2817 || class227.anInt2800 * -833477781 == i_15_ || 1642803439 * class227.anInt2799 == i_15_)
			i = class409.anInt4906 * -1427347049;
		else if (2055956425 * class227.anInt2801 == i_15_ || class227.anInt2828 * 1053306035 == i_15_ || 1489597113 * class227.anInt2812 == i_15_ || 1879075923 * class227.anInt2803 == i_15_)
			i = 1863998163 * class409.anInt4921;
		return i;
	}

	public static void method13441(int i, int i_16_, int i_17_, int i_18_) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 117); null != class282_sub48; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(894765277))
			Class175.method2964(class282_sub48, i, i_16_, i_17_, i_18_, -1331651831);
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8074.method8097((byte) 25); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8074.method8067(-69095060)) {
			int i_19_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15855((byte) -17);
			int i_20_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.method7597(-947728005);
			if (i_20_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.aBool7891))
				i_19_ = 0;
			else if (i_20_ == class227.anInt2797 * -474675041 || i_20_ == 486149589 * class227.anInt2817 || -833477781 * class227.anInt2800 == i_20_ || 1642803439 * class227.anInt2799 == i_20_)
				i_19_ = 2;
			else if (class227.anInt2801 * 2055956425 == i_20_ || i_20_ == class227.anInt2828 * 1053306035 || class227.anInt2812 * 1489597113 == i_20_ || i_20_ == 1879075923 * class227.anInt2803)
				i_19_ = 3;
			if (((Class282_Sub48) class282_sub48).anInt8091 * -1881404979 != i_19_) {
				int i_21_ = (Class282_Sub11_Sub1.method15433((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085), 912866554));
				Class409 class409 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580);
				if (null != class409.anIntArray4886)
					class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
				if (class409 == null || i_21_ == -1) {
					((Class282_Sub48) class282_sub48).anInt8095 = 569710251;
					((Class282_Sub48) class282_sub48).aBool8098 = false;
					((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_19_;
				} else if (1399704573 * (((Class282_Sub48) class282_sub48).anInt8095) != i_21_ || (((Class282_Sub48) class282_sub48).aBool8098 != class409.aBool4872)) {
					boolean bool = false;
					if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((-301211853 * ((Class282_Sub48) class282_sub48).anInt8089) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = class409.anInt4909 * 51937289;
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = -569710251 * i_21_;
						((Class282_Sub48) class282_sub48).aBool8098 = class409.aBool4872;
						((Class282_Sub48) class282_sub48).anInt8091 = i_19_ * -983037179;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8091 = i_19_ * -983037179;
					((Class282_Sub48) class282_sub48).anInt8089 = 51937289 * class409.anInt4909;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = (int) class385.aFloat4671 * 80768833;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = (int) class385.aFloat4673 * 1225707357;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = 1589843113 * (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aByte7967);
			Class175.method2964(class282_sub48, i, i_16_, i_17_, i_18_, -1661060558);
		}
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass465_8075.method7750(-1385496462); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass465_8075.method7751((byte) 11)) {
			int i_22_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15855((byte) -17);
			int i_23_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.method7597(-1521111844);
			if (i_23_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.aBool7891))
				i_22_ = 0;
			else if (class227.anInt2797 * -474675041 == i_23_ || i_23_ == 486149589 * class227.anInt2817 || i_23_ == class227.anInt2800 * -833477781 || 1642803439 * class227.anInt2799 == i_23_)
				i_22_ = 2;
			else if (i_23_ == class227.anInt2801 * 2055956425 || class227.anInt2828 * 1053306035 == i_23_ || 1489597113 * class227.anInt2812 == i_23_ || i_23_ == 1879075923 * class227.anInt2803)
				i_22_ = 3;
			if (i_22_ != -1881404979 * ((Class282_Sub48) class282_sub48).anInt8091) {
				int i_24_ = (Class149_Sub2.method14610((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086), 1727963843));
				if (i_24_ != (1399704573 * ((Class282_Sub48) class282_sub48).anInt8095) || (((Class282_Sub48) class282_sub48).aBool8098 != (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564))) {
					boolean bool = false;
					if ((((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099) != null) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((((Class282_Sub48) class282_sub48).anInt8089 * -301211853) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = i_24_ * -569710251;
						((Class282_Sub48) class282_sub48).aBool8098 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564);
						((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_22_;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
					((Class282_Sub48) class282_sub48).anInt8091 = i_22_ * -983037179;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = 80768833 * (int) class385.aFloat4671;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = 1225707357 * (int) class385.aFloat4673;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aByte7967) * 1589843113;
			Class175.method2964(class282_sub48, i, i_16_, i_17_, i_18_, -1693662930);
		}
	}

	public static void method13442(int i, int i_25_, int i_26_, int i_27_) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 123); null != class282_sub48; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(-853625312))
			Class175.method2964(class282_sub48, i, i_25_, i_26_, i_27_, -1001943374);
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8074.method8097((byte) 34); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8074.method8067(1887963284)) {
			int i_28_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15855((byte) -17);
			int i_29_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.method7597(-968468450);
			if (i_29_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.aBool7891))
				i_28_ = 0;
			else if (i_29_ == class227.anInt2797 * -474675041 || i_29_ == 486149589 * class227.anInt2817 || -833477781 * class227.anInt2800 == i_29_ || 1642803439 * class227.anInt2799 == i_29_)
				i_28_ = 2;
			else if (class227.anInt2801 * 2055956425 == i_29_ || i_29_ == class227.anInt2828 * 1053306035 || class227.anInt2812 * 1489597113 == i_29_ || i_29_ == 1879075923 * class227.anInt2803)
				i_28_ = 3;
			if (((Class282_Sub48) class282_sub48).anInt8091 * -1881404979 != i_28_) {
				int i_30_ = (Class282_Sub11_Sub1.method15433((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085), 912866554));
				Class409 class409 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580);
				if (null != class409.anIntArray4886)
					class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
				if (class409 == null || i_30_ == -1) {
					((Class282_Sub48) class282_sub48).anInt8095 = 569710251;
					((Class282_Sub48) class282_sub48).aBool8098 = false;
					((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_28_;
				} else if (1399704573 * (((Class282_Sub48) class282_sub48).anInt8095) != i_30_ || (((Class282_Sub48) class282_sub48).aBool8098 != class409.aBool4872)) {
					boolean bool = false;
					if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((-301211853 * ((Class282_Sub48) class282_sub48).anInt8089) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = class409.anInt4909 * 51937289;
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = -569710251 * i_30_;
						((Class282_Sub48) class282_sub48).aBool8098 = class409.aBool4872;
						((Class282_Sub48) class282_sub48).anInt8091 = i_28_ * -983037179;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8091 = i_28_ * -983037179;
					((Class282_Sub48) class282_sub48).anInt8089 = 51937289 * class409.anInt4909;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = (int) class385.aFloat4671 * 80768833;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = (int) class385.aFloat4673 * 1225707357;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = 1589843113 * (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aByte7967);
			Class175.method2964(class282_sub48, i, i_25_, i_26_, i_27_, -1080566209);
		}
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass465_8075.method7750(1452029025); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass465_8075.method7751((byte) 14)) {
			int i_31_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15855((byte) -17);
			int i_32_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.method7597(-1347124338);
			if (i_32_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.aBool7891))
				i_31_ = 0;
			else if (class227.anInt2797 * -474675041 == i_32_ || i_32_ == 486149589 * class227.anInt2817 || i_32_ == class227.anInt2800 * -833477781 || 1642803439 * class227.anInt2799 == i_32_)
				i_31_ = 2;
			else if (i_32_ == class227.anInt2801 * 2055956425 || class227.anInt2828 * 1053306035 == i_32_ || 1489597113 * class227.anInt2812 == i_32_ || i_32_ == 1879075923 * class227.anInt2803)
				i_31_ = 3;
			if (i_31_ != -1881404979 * ((Class282_Sub48) class282_sub48).anInt8091) {
				int i_33_ = (Class149_Sub2.method14610((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086), 1767386395));
				if (i_33_ != (1399704573 * ((Class282_Sub48) class282_sub48).anInt8095) || (((Class282_Sub48) class282_sub48).aBool8098 != (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564))) {
					boolean bool = false;
					if ((((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099) != null) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((((Class282_Sub48) class282_sub48).anInt8089 * -301211853) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = i_33_ * -569710251;
						((Class282_Sub48) class282_sub48).aBool8098 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564);
						((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_31_;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
					((Class282_Sub48) class282_sub48).anInt8091 = i_31_ * -983037179;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = 80768833 * (int) class385.aFloat4671;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = 1225707357 * (int) class385.aFloat4673;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aByte7967) * 1589843113;
			Class175.method2964(class282_sub48, i, i_25_, i_26_, i_27_, -1393636192);
		}
	}

	public static void method13443(int i, int i_34_, int i_35_, int i_36_) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 33); null != class282_sub48; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(-512772170))
			Class175.method2964(class282_sub48, i, i_34_, i_35_, i_36_, -1937339058);
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8074.method8097((byte) 16); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8074.method8067(32501752)) {
			int i_37_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15855((byte) -17);
			int i_38_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.method7597(-893659153);
			if (i_38_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.aBool7891))
				i_37_ = 0;
			else if (i_38_ == class227.anInt2797 * -474675041 || i_38_ == 486149589 * class227.anInt2817 || -833477781 * class227.anInt2800 == i_38_ || 1642803439 * class227.anInt2799 == i_38_)
				i_37_ = 2;
			else if (class227.anInt2801 * 2055956425 == i_38_ || i_38_ == class227.anInt2828 * 1053306035 || class227.anInt2812 * 1489597113 == i_38_ || i_38_ == 1879075923 * class227.anInt2803)
				i_37_ = 3;
			if (((Class282_Sub48) class282_sub48).anInt8091 * -1881404979 != i_37_) {
				int i_39_ = (Class282_Sub11_Sub1.method15433((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085), 912866554));
				Class409 class409 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580);
				if (null != class409.anIntArray4886)
					class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
				if (class409 == null || i_39_ == -1) {
					((Class282_Sub48) class282_sub48).anInt8095 = 569710251;
					((Class282_Sub48) class282_sub48).aBool8098 = false;
					((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_37_;
				} else if (1399704573 * (((Class282_Sub48) class282_sub48).anInt8095) != i_39_ || (((Class282_Sub48) class282_sub48).aBool8098 != class409.aBool4872)) {
					boolean bool = false;
					if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((-301211853 * ((Class282_Sub48) class282_sub48).anInt8089) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = class409.anInt4909 * 51937289;
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = -569710251 * i_39_;
						((Class282_Sub48) class282_sub48).aBool8098 = class409.aBool4872;
						((Class282_Sub48) class282_sub48).anInt8091 = i_37_ * -983037179;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8091 = i_37_ * -983037179;
					((Class282_Sub48) class282_sub48).anInt8089 = 51937289 * class409.anInt4909;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = (int) class385.aFloat4671 * 80768833;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = (int) class385.aFloat4673 * 1225707357;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = 1589843113 * (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aByte7967);
			Class175.method2964(class282_sub48, i, i_34_, i_35_, i_36_, -1295686790);
		}
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass465_8075.method7750(-1655872839); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass465_8075.method7751((byte) 80)) {
			int i_40_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15855((byte) -17);
			int i_41_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.method7597(-2074622430);
			if (i_41_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.aBool7891))
				i_40_ = 0;
			else if (class227.anInt2797 * -474675041 == i_41_ || i_41_ == 486149589 * class227.anInt2817 || i_41_ == class227.anInt2800 * -833477781 || 1642803439 * class227.anInt2799 == i_41_)
				i_40_ = 2;
			else if (i_41_ == class227.anInt2801 * 2055956425 || class227.anInt2828 * 1053306035 == i_41_ || 1489597113 * class227.anInt2812 == i_41_ || i_41_ == 1879075923 * class227.anInt2803)
				i_40_ = 3;
			if (i_40_ != -1881404979 * ((Class282_Sub48) class282_sub48).anInt8091) {
				int i_42_ = (Class149_Sub2.method14610((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086), 1967074335));
				if (i_42_ != (1399704573 * ((Class282_Sub48) class282_sub48).anInt8095) || (((Class282_Sub48) class282_sub48).aBool8098 != (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564))) {
					boolean bool = false;
					if ((((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099) != null) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((((Class282_Sub48) class282_sub48).anInt8089 * -301211853) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = i_42_ * -569710251;
						((Class282_Sub48) class282_sub48).aBool8098 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564);
						((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_40_;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
					((Class282_Sub48) class282_sub48).anInt8091 = i_40_ * -983037179;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = 80768833 * (int) class385.aFloat4671;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = 1225707357 * (int) class385.aFloat4673;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aByte7967) * 1589843113;
			Class175.method2964(class282_sub48, i, i_34_, i_35_, i_36_, -1772084925);
		}
	}

	public static void method13444(int i, int i_43_, int i_44_, int i_45_) {
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8073.method8097((byte) 116); null != class282_sub48; class282_sub48 = (Class282_Sub48) aClass482_8073.method8067(550765168))
			Class175.method2964(class282_sub48, i, i_43_, i_44_, i_45_, -2124717343);
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass482_8074.method8097((byte) 30); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass482_8074.method8067(203519697)) {
			int i_46_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15855((byte) -17);
			int i_47_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.method7597(-1019960407);
			if (i_47_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass456_Sub3_10337.aBool7891))
				i_46_ = 0;
			else if (i_47_ == class227.anInt2797 * -474675041 || i_47_ == 486149589 * class227.anInt2817 || -833477781 * class227.anInt2800 == i_47_ || 1642803439 * class227.anInt2799 == i_47_)
				i_46_ = 2;
			else if (class227.anInt2801 * 2055956425 == i_47_ || i_47_ == class227.anInt2828 * 1053306035 || class227.anInt2812 * 1489597113 == i_47_ || i_47_ == 1879075923 * class227.anInt2803)
				i_46_ = 3;
			if (((Class282_Sub48) class282_sub48).anInt8091 * -1881404979 != i_46_) {
				int i_48_ = (Class282_Sub11_Sub1.method15433((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085), 912866554));
				Class409 class409 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aClass409_10580);
				if (null != class409.anIntArray4886)
					class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
				if (class409 == null || i_48_ == -1) {
					((Class282_Sub48) class282_sub48).anInt8095 = 569710251;
					((Class282_Sub48) class282_sub48).aBool8098 = false;
					((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_46_;
				} else if (1399704573 * (((Class282_Sub48) class282_sub48).anInt8095) != i_48_ || (((Class282_Sub48) class282_sub48).aBool8098 != class409.aBool4872)) {
					boolean bool = false;
					if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((-301211853 * ((Class282_Sub48) class282_sub48).anInt8089) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = class409.anInt4909 * 51937289;
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = -569710251 * i_48_;
						((Class282_Sub48) class282_sub48).aBool8098 = class409.aBool4872;
						((Class282_Sub48) class282_sub48).anInt8091 = i_46_ * -983037179;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8091 = i_46_ * -983037179;
					((Class282_Sub48) class282_sub48).anInt8089 = 51937289 * class409.anInt4909;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = (int) class385.aFloat4671 * 80768833;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = (int) class385.aFloat4673 * 1225707357;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = 1589843113 * (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub2_8085.aByte7967);
			Class175.method2964(class282_sub48, i, i_43_, i_44_, i_45_, -1718610841);
		}
		for (Class282_Sub48 class282_sub48 = (Class282_Sub48) aClass465_8075.method7750(1762842587); class282_sub48 != null; class282_sub48 = (Class282_Sub48) aClass465_8075.method7751((byte) 126)) {
			int i_49_ = 1;
			Class227 class227 = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15855((byte) -17);
			int i_50_ = ((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.method7597(-1863836788);
			if (i_50_ == -1 || (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aClass456_Sub3_10337.aBool7891))
				i_49_ = 0;
			else if (class227.anInt2797 * -474675041 == i_50_ || i_50_ == 486149589 * class227.anInt2817 || i_50_ == class227.anInt2800 * -833477781 || 1642803439 * class227.anInt2799 == i_50_)
				i_49_ = 2;
			else if (i_50_ == class227.anInt2801 * 2055956425 || class227.anInt2828 * 1053306035 == i_50_ || 1489597113 * class227.anInt2812 == i_50_ || i_50_ == 1879075923 * class227.anInt2803)
				i_49_ = 3;
			if (i_49_ != -1881404979 * ((Class282_Sub48) class282_sub48).anInt8091) {
				int i_51_ = (Class149_Sub2.method14610((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086), 1430857228));
				if (i_51_ != (1399704573 * ((Class282_Sub48) class282_sub48).anInt8095) || (((Class282_Sub48) class282_sub48).aBool8098 != (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564))) {
					boolean bool = false;
					if ((((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099) != null) {
						((Class282_Sub48) class282_sub48).anInt8089 -= 241694208;
						if ((((Class282_Sub48) class282_sub48).anInt8089 * -301211853) <= 0) {
							Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
							((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
							bool = true;
						}
					} else
						bool = true;
					if (bool) {
						((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
						((Class282_Sub48) class282_sub48).anInt8095 = i_51_ * -569710251;
						((Class282_Sub48) class282_sub48).aBool8098 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aBool10564);
						((Class282_Sub48) class282_sub48).anInt8091 = -983037179 * i_49_;
					}
				} else {
					((Class282_Sub48) class282_sub48).anInt8089 = ((((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.anInt10566) * -1517226111);
					((Class282_Sub48) class282_sub48).anInt8091 = i_49_ * -983037179;
				}
			}
			Class385 class385 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method11166().aClass385_3595);
			((Class282_Sub48) class282_sub48).anInt8107 = 80768833 * (int) class385.aFloat4671;
			((Class282_Sub48) class282_sub48).anInt8079 = ((int) class385.aFloat4671 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1480845005;
			((Class282_Sub48) class282_sub48).anInt8078 = 1225707357 * (int) class385.aFloat4673;
			((Class282_Sub48) class282_sub48).anInt8077 = ((int) class385.aFloat4673 + (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.method15805(828768449) << 8)) * 1710236473;
			((Class282_Sub48) class282_sub48).anInt8076 = (((Class282_Sub48) class282_sub48).aClass521_Sub1_Sub1_Sub2_Sub1_8086.aByte7967) * 1589843113;
			Class175.method2964(class282_sub48, i, i_43_, i_44_, i_45_, -403457460);
		}
	}

	static void method13445(Class282_Sub48 class282_sub48, int i, int i_52_, int i_53_, int i_54_) {
		if (1399704573 * ((Class282_Sub48) class282_sub48).anInt8095 != -1 || null != ((Class282_Sub48) class282_sub48).anIntArray8102) {
			int i_55_ = 0;
			int i_56_ = ((((Class282_Sub48) class282_sub48).anInt8089 * -301211853 * Class393.aClass282_Sub54_4783.aClass468_Sub13_8193.method12714(-682442569)) >> 8);
			if (i_52_ > ((Class282_Sub48) class282_sub48).anInt8079 * -9275899)
				i_55_ += i_52_ - (((Class282_Sub48) class282_sub48).anInt8079 * -9275899);
			else if (i_52_ < (((Class282_Sub48) class282_sub48).anInt8107 * -2014043967))
				i_55_ += -2014043967 * (((Class282_Sub48) class282_sub48).anInt8107) - i_52_;
			if (i_53_ > 517331721 * ((Class282_Sub48) class282_sub48).anInt8077)
				i_55_ += i_53_ - 517331721 * (((Class282_Sub48) class282_sub48).anInt8077);
			else if (i_53_ < (((Class282_Sub48) class282_sub48).anInt8078 * 1548025077))
				i_55_ += (((Class282_Sub48) class282_sub48).anInt8078 * 1548025077) - i_53_;
			if (0 == -60999179 * ((Class282_Sub48) class282_sub48).anInt8105 || (i_55_ - 256 > -60999179 * ((Class282_Sub48) class282_sub48).anInt8105) || Class393.aClass282_Sub54_4783.aClass468_Sub13_8193.method12714(1494801218) == 0 || (((Class282_Sub48) class282_sub48).anInt8076 * -23801959 != i)) {
				if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
					((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = null;
					((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = null;
				}
				if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 != null) {
					Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = null;
					((Class282_Sub48) class282_sub48).aClass282_Sub18_8087 = null;
					((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8106 = null;
				}
			} else {
				i_55_ -= 256;
				if (i_55_ < 0)
					i_55_ = 0;
				int i_57_ = (((Class282_Sub48) class282_sub48).anInt8105 * -60999179 - (1464439541 * ((Class282_Sub48) class282_sub48).anInt8081));
				if (i_57_ < 0)
					i_57_ = (((Class282_Sub48) class282_sub48).anInt8105 * -60999179);
				int i_58_ = i_56_;
				int i_59_ = i_55_ - (((Class282_Sub48) class282_sub48).anInt8081 * 1464439541);
				if (i_59_ > 0 && i_57_ > 0)
					i_58_ = i_56_ * (i_57_ - i_59_) / i_57_;
				Class84.myPlayer.method15805(828768449);
				int i_60_ = 8192;
				int i_61_ = (((-2014043967 * ((Class282_Sub48) class282_sub48).anInt8107) + -9275899 * (((Class282_Sub48) class282_sub48).anInt8079)) / 2 - i_52_);
				int i_62_ = (1548025077 * ((Class282_Sub48) class282_sub48).anInt8078 + 517331721 * (((Class282_Sub48) class282_sub48).anInt8077)) / 2 - i_53_;
				if (i_61_ != 0 || i_62_ != 0) {
					int i_63_ = ((-(1236051449 * Class518.anInt5930) - (int) (Math.atan2((double) i_61_, (double) i_62_) * 2607.5945876176133) - 4096) & 0x3fff);
					if (i_63_ > 8192)
						i_63_ = 16384 - i_63_;
					int i_64_;
					if (i_55_ <= 0)
						i_64_ = 8192;
					else if (i_55_ >= 4096)
						i_64_ = 16384;
					else
						i_64_ = 8192 * i_55_ / 4096 + 8192;
					i_60_ = i_63_ * i_64_ / 8192 + (16384 - i_64_ >> 1);
				}
				if (null == (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099)) {
					if ((1399704573 * ((Class282_Sub48) class282_sub48).anInt8095) >= 0) {
						i_61_ = ((256 == (((Class282_Sub48) class282_sub48).anInt8094) * -280457961 && (-937948423 * ((Class282_Sub48) class282_sub48).anInt8093 == 256)) ? 256 : Class76.method1356((-937948423 * (((Class282_Sub48) class282_sub48).anInt8093)), (-280457961 * (((Class282_Sub48) class282_sub48).anInt8094)), -1021175029));
						if (((Class282_Sub48) class282_sub48).aBool8098) {
							if ((((Class282_Sub48) class282_sub48).aClass282_Sub18_8097) == null)
								((Class282_Sub48) class282_sub48).aClass282_Sub18_8097 = (Class282_Sub18.method12270(Class313.aClass317_3665, (1399704573 * (((Class282_Sub48) class282_sub48).anInt8095))));
							if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub18_8097)) {
								if (null == (((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082))
									((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082 = ((Class282_Sub48) class282_sub48).aClass282_Sub18_8097.method12272();
								if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082)) {
									Class282_Sub15_Sub5_Sub1 class282_sub15_sub5_sub1 = ((Class282_Sub15_Sub5_Sub1) (((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8082.method15225(i_61_, i_58_ << 6, i_60_)));
									class282_sub15_sub5_sub1.method15325(-1, (byte) 8);
									Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub5_sub1);
									((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = class282_sub15_sub5_sub1;
								}
							}
						} else {
							Class343 class343 = Class343.method6094(Class219.aClass317_2714, (1399704573 * (((Class282_Sub48) class282_sub48).anInt8095)), 0);
							if (null != class343) {
								Class282_Sub26_Sub1_Sub2 class282_sub26_sub1_sub2 = (class343.method6089().method16062(Class119.aClass344_1460));
								Class282_Sub15_Sub5_Sub2 class282_sub15_sub5_sub2 = ((Class282_Sub15_Sub5_Sub2) (class282_sub26_sub1_sub2.method15225(i_61_, i_58_ << 6, i_60_)));
								class282_sub15_sub5_sub2.method15325(-1, (byte) 8);
								Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub5_sub2);
								((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = class282_sub15_sub5_sub2;
							}
						}
					}
				} else {
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099.method15312(i_58_, (byte) 92);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099.method15340(i_60_, -1989047012);
				}
				if (null == (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096)) {
					if ((null != ((Class282_Sub48) class282_sub48).anIntArray8102) && (((Class282_Sub48) class282_sub48).anInt8080 -= i_54_ * 401671005) * -1690146571 <= 0) {
						i_61_ = ((-280457961 * (((Class282_Sub48) class282_sub48).anInt8094) == 256 && (((Class282_Sub48) class282_sub48).anInt8093 * -937948423) == 256) ? 256 : ((int) (Math.random() * (double) ((-280457961 * (((Class282_Sub48) class282_sub48).anInt8094)) - ((((Class282_Sub48) class282_sub48).anInt8093) * -937948423))) + -937948423 * ((Class282_Sub48) class282_sub48).anInt8093));
						if (((Class282_Sub48) class282_sub48).aBool8103) {
							if (null == (((Class282_Sub48) class282_sub48).aClass282_Sub18_8087)) {
								i_62_ = (int) (Math.random() * (double) (((Class282_Sub48) class282_sub48).anIntArray8102).length);
								((Class282_Sub48) class282_sub48).aClass282_Sub18_8087 = (Class282_Sub18.method12270(Class313.aClass317_3665, (((Class282_Sub48) class282_sub48).anIntArray8102[i_62_])));
							}
							if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub18_8087)) {
								if (null == (((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8106))
									((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8106 = ((Class282_Sub48) class282_sub48).aClass282_Sub18_8087.method12272();
								if (null != (((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8106)) {
									Class282_Sub15_Sub5_Sub1 class282_sub15_sub5_sub1 = ((Class282_Sub15_Sub5_Sub1) (((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8106.method15225(i_61_, i_58_ << 6, i_60_)));
									class282_sub15_sub5_sub1.method15325(0, (byte) 8);
									Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub5_sub1);
									((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = class282_sub15_sub5_sub1;
									((Class282_Sub48) class282_sub48).anInt8080 = (((((Class282_Sub48) class282_sub48).anInt8072) * 1317424347 + (int) (Math.random() * (double) (((((Class282_Sub48) class282_sub48).anInt8101) * -1084771983) - (1317424347 * (((Class282_Sub48) class282_sub48).anInt8072))))) * 401671005);
								}
							}
						} else {
							i_62_ = (int) (Math.random() * (double) (((Class282_Sub48) class282_sub48).anIntArray8102).length);
							Class343 class343 = Class343.method6094(Class219.aClass317_2714, (((Class282_Sub48) class282_sub48).anIntArray8102[i_62_]), 0);
							if (null != class343) {
								Class282_Sub26_Sub1_Sub2 class282_sub26_sub1_sub2 = (class343.method6089().method16062(Class119.aClass344_1460));
								Class282_Sub15_Sub5_Sub2 class282_sub15_sub5_sub2 = ((Class282_Sub15_Sub5_Sub2) (class282_sub26_sub1_sub2.method15225(i_61_, i_58_ << 6, i_60_)));
								class282_sub15_sub5_sub2.method15325(0, (byte) 8);
								Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub5_sub2);
								((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = class282_sub15_sub5_sub2;
								((Class282_Sub48) class282_sub48).anInt8080 = ((1317424347 * ((Class282_Sub48) class282_sub48).anInt8072 + (int) (Math.random() * (double) (((((Class282_Sub48) class282_sub48).anInt8101) * -1084771983) - ((((Class282_Sub48) class282_sub48).anInt8072) * 1317424347)))) * 401671005);
							}
						}
					}
				} else {
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096.method15312(i_58_, (byte) 57);
					((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096.method15340(i_60_, -1422746822);
					if (!((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096.method4994(-318410028)) {
						((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8096 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub18_8087 = null;
						((Class282_Sub48) class282_sub48).aClass282_Sub26_Sub1_Sub1_8106 = null;
					}
				}
			}
		}
	}

	static final void method13446(Class527 class527, int i) {
		int i_65_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_65_ ^ 0xffffffff;
	}
}
