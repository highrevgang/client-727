/* Class282_Sub28 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub28 extends Class282 {
	int anInt7697;
	int anInt7698;
	int anInt7699;
	int anInt7700;
	int anInt7701;
	int anInt7702;
	int anInt7703;
	int anInt7704;
	int anInt7705;

	Class282_Sub28(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		((Class282_Sub28) this).anInt7697 = -2093148961 * i;
		((Class282_Sub28) this).anInt7698 = -1493837045 * i_0_;
		((Class282_Sub28) this).anInt7699 = i_1_ * -1346920747;
		((Class282_Sub28) this).anInt7700 = -566013323 * i_2_;
		((Class282_Sub28) this).anInt7702 = i_3_ * -1993934447;
		((Class282_Sub28) this).anInt7701 = -1301778087 * i_4_;
		((Class282_Sub28) this).anInt7703 = 2078204165 * i_5_;
		((Class282_Sub28) this).anInt7704 = -702390921 * i_6_;
		((Class282_Sub28) this).anInt7705 = 1584843087 * i_7_;
	}

	void method12407(int i, int i_8_, int[] is) {
		is[0] = 0;
		is[1] = i + (313968361 * ((Class282_Sub28) this).anInt7701 - ((Class282_Sub28) this).anInt7698 * -292185949);
		is[2] = (-355220531 * ((Class282_Sub28) this).anInt7703 - -1889097091 * ((Class282_Sub28) this).anInt7699 + i_8_);
	}

	boolean method12408(int i, int i_9_, int i_10_, int i_11_) {
		if (665036575 * ((Class282_Sub28) this).anInt7697 == i && i_9_ >= ((Class282_Sub28) this).anInt7698 * -292185949 && i_9_ <= -101814819 * ((Class282_Sub28) this).anInt7700 && i_10_ >= ((Class282_Sub28) this).anInt7699 * -1889097091 && i_10_ <= ((Class282_Sub28) this).anInt7702 * -470096015)
			return true;
		return false;
	}

	boolean method12409(int i, int i_12_, byte i_13_) {
		if (i >= 313968361 * ((Class282_Sub28) this).anInt7701 && i <= ((Class282_Sub28) this).anInt7704 * -734163897 && i_12_ >= ((Class282_Sub28) this).anInt7703 * -355220531 && i_12_ <= ((Class282_Sub28) this).anInt7705 * -1734431313)
			return true;
		return false;
	}

	void method12410(int i, int i_14_, int[] is, int i_15_) {
		is[0] = 665036575 * ((Class282_Sub28) this).anInt7697;
		is[1] = (-292185949 * ((Class282_Sub28) this).anInt7698 - ((Class282_Sub28) this).anInt7701 * 313968361 + i);
		is[2] = (-1889097091 * ((Class282_Sub28) this).anInt7699 - -355220531 * ((Class282_Sub28) this).anInt7703 + i_14_);
	}

	void method12411(int i, int i_16_, int[] is) {
		is[0] = 0;
		is[1] = i + (313968361 * ((Class282_Sub28) this).anInt7701 - ((Class282_Sub28) this).anInt7698 * -292185949);
		is[2] = (-355220531 * ((Class282_Sub28) this).anInt7703 - -1889097091 * ((Class282_Sub28) this).anInt7699 + i_16_);
	}

	boolean method12412(int i, int i_17_) {
		if (i >= ((Class282_Sub28) this).anInt7698 * -292185949 && i <= -101814819 * ((Class282_Sub28) this).anInt7700 && i_17_ >= ((Class282_Sub28) this).anInt7699 * -1889097091 && i_17_ <= ((Class282_Sub28) this).anInt7702 * -470096015)
			return true;
		return false;
	}

	boolean method12413(int i, int i_18_) {
		if (i >= 313968361 * ((Class282_Sub28) this).anInt7701 && i <= ((Class282_Sub28) this).anInt7704 * -734163897 && i_18_ >= ((Class282_Sub28) this).anInt7703 * -355220531 && i_18_ <= ((Class282_Sub28) this).anInt7705 * -1734431313)
			return true;
		return false;
	}

	void method12414(int i, int i_19_, int[] is, int i_20_) {
		is[0] = 0;
		is[1] = i + (313968361 * ((Class282_Sub28) this).anInt7701 - ((Class282_Sub28) this).anInt7698 * -292185949);
		is[2] = (-355220531 * ((Class282_Sub28) this).anInt7703 - -1889097091 * ((Class282_Sub28) this).anInt7699 + i_19_);
	}

	boolean method12415(int i, int i_21_, int i_22_) {
		if (i >= ((Class282_Sub28) this).anInt7698 * -292185949 && i <= -101814819 * ((Class282_Sub28) this).anInt7700 && i_21_ >= ((Class282_Sub28) this).anInt7699 * -1889097091 && i_21_ <= ((Class282_Sub28) this).anInt7702 * -470096015)
			return true;
		return false;
	}

	void method12416(int i, int i_23_, int[] is) {
		is[0] = 665036575 * ((Class282_Sub28) this).anInt7697;
		is[1] = (-292185949 * ((Class282_Sub28) this).anInt7698 - ((Class282_Sub28) this).anInt7701 * 313968361 + i);
		is[2] = (-1889097091 * ((Class282_Sub28) this).anInt7699 - -355220531 * ((Class282_Sub28) this).anInt7703 + i_23_);
	}

	void method12417(int i, int i_24_, int[] is) {
		is[0] = 0;
		is[1] = i + (313968361 * ((Class282_Sub28) this).anInt7701 - ((Class282_Sub28) this).anInt7698 * -292185949);
		is[2] = (-355220531 * ((Class282_Sub28) this).anInt7703 - -1889097091 * ((Class282_Sub28) this).anInt7699 + i_24_);
	}

	void method12418(int i, int i_25_, int[] is) {
		is[0] = 665036575 * ((Class282_Sub28) this).anInt7697;
		is[1] = (-292185949 * ((Class282_Sub28) this).anInt7698 - ((Class282_Sub28) this).anInt7701 * 313968361 + i);
		is[2] = (-1889097091 * ((Class282_Sub28) this).anInt7699 - -355220531 * ((Class282_Sub28) this).anInt7703 + i_25_);
	}

	void method12419(int i, int i_26_, int[] is) {
		is[0] = 665036575 * ((Class282_Sub28) this).anInt7697;
		is[1] = (-292185949 * ((Class282_Sub28) this).anInt7698 - ((Class282_Sub28) this).anInt7701 * 313968361 + i);
		is[2] = (-1889097091 * ((Class282_Sub28) this).anInt7699 - -355220531 * ((Class282_Sub28) this).anInt7703 + i_26_);
	}

	static void method12420(int i, int i_27_, int i_28_, int i_29_, int i_30_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_27_ << 32 | (long) i);
		class282_sub50_sub12.method14995(956319706);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_28_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_29_;
	}
}
