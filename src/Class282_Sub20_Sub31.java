/* Class282_Sub20_Sub31 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub20_Sub31 extends Class282_Sub20 {
	static final boolean aBool9917 = false;

	int[][] method12320(int i, int i_0_) {
		int[][] is = aClass308_7670.method5463(i, 1106148078);
		if (aClass308_7670.aBool3619) {
			int[][] is_1_ = method12333(0, i, (byte) 3);
			int[] is_2_ = is_1_[0];
			int[] is_3_ = is_1_[1];
			int[] is_4_ = is_1_[2];
			int[] is_5_ = is[0];
			int[] is_6_ = is[1];
			int[] is_7_ = is[2];
			for (int i_8_ = 0; i_8_ < Class316.anInt3670 * -402153223; i_8_++) {
				is_5_[i_8_] = 4096 - is_2_[i_8_];
				is_6_[i_8_] = 4096 - is_3_[i_8_];
				is_7_[i_8_] = 4096 - is_4_[i_8_];
			}
		}
		return is;
	}

	int[] method12319(int i, int i_9_) {
		int[] is = aClass320_7667.method5721(i, -1476130867);
		if (aClass320_7667.aBool3722) {
			int[] is_10_ = method12317(0, i, 2029672467);
			for (int i_11_ = 0; i_11_ < -402153223 * Class316.anInt3670; i_11_++)
				is[i_11_] = 4096 - is_10_[i_11_];
		}
		return is;
	}

	void method12334(int i, RsByteBuffer class282_sub35) {
		if (0 == i)
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
	}

	void method12322(int i, RsByteBuffer class282_sub35, int i_12_) {
		if (0 == i)
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
	}

	int[] method12325(int i) {
		int[] is = aClass320_7667.method5721(i, -1122949707);
		if (aClass320_7667.aBool3722) {
			int[] is_13_ = method12317(0, i, 2103248671);
			for (int i_14_ = 0; i_14_ < -402153223 * Class316.anInt3670; i_14_++)
				is[i_14_] = 4096 - is_13_[i_14_];
		}
		return is;
	}

	int[] method12336(int i) {
		int[] is = aClass320_7667.method5721(i, -2036853226);
		if (aClass320_7667.aBool3722) {
			int[] is_15_ = method12317(0, i, 2095217934);
			for (int i_16_ = 0; i_16_ < -402153223 * Class316.anInt3670; i_16_++)
				is[i_16_] = 4096 - is_15_[i_16_];
		}
		return is;
	}

	public Class282_Sub20_Sub31() {
		super(1, false);
	}

	void method12332(int i, RsByteBuffer class282_sub35) {
		if (0 == i)
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
	}

	void method12335(int i, RsByteBuffer class282_sub35) {
		if (0 == i)
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
	}

	int[] method12327(int i) {
		int[] is = aClass320_7667.method5721(i, -529296881);
		if (aClass320_7667.aBool3722) {
			int[] is_17_ = method12317(0, i, 2012118007);
			for (int i_18_ = 0; i_18_ < -402153223 * Class316.anInt3670; i_18_++)
				is[i_18_] = 4096 - is_17_[i_18_];
		}
		return is;
	}

	int[][] method12339(int i) {
		int[][] is = aClass308_7670.method5463(i, 1307392024);
		if (aClass308_7670.aBool3619) {
			int[][] is_19_ = method12333(0, i, (byte) 3);
			int[] is_20_ = is_19_[0];
			int[] is_21_ = is_19_[1];
			int[] is_22_ = is_19_[2];
			int[] is_23_ = is[0];
			int[] is_24_ = is[1];
			int[] is_25_ = is[2];
			for (int i_26_ = 0; i_26_ < Class316.anInt3670 * -402153223; i_26_++) {
				is_23_[i_26_] = 4096 - is_20_[i_26_];
				is_24_[i_26_] = 4096 - is_21_[i_26_];
				is_25_[i_26_] = 4096 - is_22_[i_26_];
			}
		}
		return is;
	}
}
