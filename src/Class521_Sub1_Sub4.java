/* Class521_Sub1_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class521_Sub1_Sub4 extends Class521_Sub1 {
	protected short aShort9611;
	protected short aShort9612;

	final void method12984(int i) {
		throw new IllegalStateException();
	}

	int method13036(Class282_Sub24[] class282_sub24s, int i) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	boolean method12988(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3273(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), method12995(-949503051)));
	}

	boolean method13029(byte i) {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((Class206) aClass206_7970).anInt2652 * 1459994833 + (((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643))][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}

	int method13024(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	final void method13013(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_0_, int i_1_, boolean bool, int i_2_) {
		throw new IllegalStateException();
	}

	boolean method13037(Class505 class505, int i) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3273(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), method12995(-1991070986)));
	}

	final boolean method13026() {
		return false;
	}

	final boolean method13011() {
		return false;
	}

	final void method13016(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_3_, int i_4_, boolean bool) {
		throw new IllegalStateException();
	}

	int method13025(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	final void method13015() {
		throw new IllegalStateException();
	}

	boolean method13034() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((Class206) aClass206_7970).anInt2652 * 1459994833 + (((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643))][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}

	final boolean method12985(int i) {
		return false;
	}

	int method13031(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	int method12982(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	boolean method13022(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3273(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), method12995(-1441304110)));
	}

	Class521_Sub1_Sub4(Class206 class206, int i, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_) {
		super(class206);
		aByte7967 = (byte) i_7_;
		aByte7968 = (byte) i_8_;
		aShort9611 = (short) i_9_;
		aShort9612 = (short) i_10_;
		method11171(new Class385((float) i, (float) i_5_, (float) i_6_));
	}

	final void method13021() {
		throw new IllegalStateException();
	}

	boolean method12998(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3273(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), method12995(-308469814)));
	}

	boolean method13032() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((Class206) aClass206_7970).anInt2652 * 1459994833 + (((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643))][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}

	boolean method13033() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((Class206) aClass206_7970).anInt2652 * 1459994833 + (((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643))][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}

	boolean method13030(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3273(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), method12995(815041790)));
	}

	static final void method14892(Class118 class118, Class98 class98, Class527 class527, byte i) {
		class118.anInt1377 = ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) * -1802729151);
		Class109.method1858(class118, (byte) -21);
	}
}
