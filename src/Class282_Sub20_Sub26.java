/* Class282_Sub20_Sub26 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub20_Sub26 extends Class282_Sub20 {
	int anInt9886 = -474599424;
	static final int anInt9887 = 4096;

	@Override
	void method12334(int i, RsByteBuffer class282_sub35) {
		if (i == 0) {
			this.anInt9886 = class282_sub35.readUnsignedShort() * -1801569437;
		}
	}

	@Override
	int[] method12319(int i, int i_0_) {
		int[] is = aClass320_7667.method5721(i, 798975247);
		if (aClass320_7667.aBool3722) {
			int[] is_1_ = method12317(0, i - 1 & 543225399 * Class316.anInt3673, 2146057922);
			int[] is_2_ = method12317(0, i, 1965777622);
			int[] is_3_ = method12317(0, 1 + i & 543225399 * Class316.anInt3673, 2055887552);
			for (int i_4_ = 0; i_4_ < Class316.anInt3670 * -402153223; i_4_++) {
				int i_5_ = ((is_3_[i_4_] - is_1_[i_4_]) * (1128603211 * this.anInt9886));
				int i_6_ = ((is_2_[i_4_ + 1 & Class316.anInt3669 * 1201532175] - is_2_[i_4_ - 1 & 1201532175 * Class316.anInt3669]) * (this.anInt9886 * 1128603211));
				int i_7_ = i_6_ >> 12;
				int i_8_ = i_5_ >> 12;
				int i_9_ = i_7_ * i_7_ >> 12;
				int i_10_ = i_8_ * i_8_ >> 12;
				int i_11_ = (int) (Math.sqrt((4096 + (i_10_ + i_9_)) / 4096.0F) * 4096.0);
				int i_12_ = i_11_ != 0 ? 16777216 / i_11_ : 0;
				is[i_4_] = 4096 - i_12_;
			}
		}
		return is;
	}

	@Override
	void method12322(int i, RsByteBuffer class282_sub35, int i_13_) {
		if (i == 0) {
			this.anInt9886 = class282_sub35.readUnsignedShort() * -1801569437;
		}
	}

	@Override
	int[] method12325(int i) {
		int[] is = aClass320_7667.method5721(i, 98207941);
		if (aClass320_7667.aBool3722) {
			int[] is_14_ = method12317(0, i - 1 & 543225399 * Class316.anInt3673, 2014034399);
			int[] is_15_ = method12317(0, i, 2087712896);
			int[] is_16_ = method12317(0, 1 + i & 543225399 * Class316.anInt3673, 1938453154);
			for (int i_17_ = 0; i_17_ < Class316.anInt3670 * -402153223; i_17_++) {
				int i_18_ = ((is_16_[i_17_] - is_14_[i_17_]) * (1128603211 * this.anInt9886));
				int i_19_ = ((is_15_[i_17_ + 1 & Class316.anInt3669 * 1201532175] - is_15_[i_17_ - 1 & 1201532175 * Class316.anInt3669]) * (this.anInt9886 * 1128603211));
				int i_20_ = i_19_ >> 12;
				int i_21_ = i_18_ >> 12;
				int i_22_ = i_20_ * i_20_ >> 12;
				int i_23_ = i_21_ * i_21_ >> 12;
				int i_24_ = (int) (Math.sqrt((4096 + (i_23_ + i_22_)) / 4096.0F) * 4096.0);
				int i_25_ = i_24_ != 0 ? 16777216 / i_24_ : 0;
				is[i_17_] = 4096 - i_25_;
			}
		}
		return is;
	}

	@Override
	int[] method12336(int i) {
		int[] is = aClass320_7667.method5721(i, -1376933208);
		if (aClass320_7667.aBool3722) {
			int[] is_26_ = method12317(0, i - 1 & 543225399 * Class316.anInt3673, 1979628933);
			int[] is_27_ = method12317(0, i, 1928552779);
			int[] is_28_ = method12317(0, 1 + i & 543225399 * Class316.anInt3673, 1971387528);
			for (int i_29_ = 0; i_29_ < Class316.anInt3670 * -402153223; i_29_++) {
				int i_30_ = ((is_28_[i_29_] - is_26_[i_29_]) * (1128603211 * this.anInt9886));
				int i_31_ = ((is_27_[i_29_ + 1 & Class316.anInt3669 * 1201532175] - is_27_[i_29_ - 1 & 1201532175 * Class316.anInt3669]) * (this.anInt9886 * 1128603211));
				int i_32_ = i_31_ >> 12;
				int i_33_ = i_30_ >> 12;
				int i_34_ = i_32_ * i_32_ >> 12;
				int i_35_ = i_33_ * i_33_ >> 12;
				int i_36_ = (int) (Math.sqrt((4096 + (i_35_ + i_34_)) / 4096.0F) * 4096.0);
				int i_37_ = i_36_ != 0 ? 16777216 / i_36_ : 0;
				is[i_29_] = 4096 - i_37_;
			}
		}
		return is;
	}

	@Override
	void method12332(int i, RsByteBuffer class282_sub35) {
		if (i == 0) {
			this.anInt9886 = class282_sub35.readUnsignedShort() * -1801569437;
		}
	}

	public Class282_Sub20_Sub26() {
		super(1, true);
	}

	@Override
	void method12335(int i, RsByteBuffer class282_sub35) {
		if (i == 0) {
			this.anInt9886 = class282_sub35.readUnsignedShort() * -1801569437;
		}
	}

	@Override
	int[] method12327(int i) {
		int[] is = aClass320_7667.method5721(i, -117270749);
		if (aClass320_7667.aBool3722) {
			int[] is_38_ = method12317(0, i - 1 & 543225399 * Class316.anInt3673, 2093947508);
			int[] is_39_ = method12317(0, i, 2044972069);
			int[] is_40_ = method12317(0, 1 + i & 543225399 * Class316.anInt3673, 1996413851);
			for (int i_41_ = 0; i_41_ < Class316.anInt3670 * -402153223; i_41_++) {
				int i_42_ = ((is_40_[i_41_] - is_38_[i_41_]) * (1128603211 * this.anInt9886));
				int i_43_ = ((is_39_[i_41_ + 1 & Class316.anInt3669 * 1201532175] - is_39_[i_41_ - 1 & 1201532175 * Class316.anInt3669]) * (this.anInt9886 * 1128603211));
				int i_44_ = i_43_ >> 12;
				int i_45_ = i_42_ >> 12;
				int i_46_ = i_44_ * i_44_ >> 12;
				int i_47_ = i_45_ * i_45_ >> 12;
				int i_48_ = (int) (Math.sqrt((4096 + (i_47_ + i_46_)) / 4096.0F) * 4096.0);
				int i_49_ = i_48_ != 0 ? 16777216 / i_48_ : 0;
				is[i_41_] = 4096 - i_49_;
			}
		}
		return is;
	}

	static void lobbyLogin(String string, String string_50_, byte i) {
		if (!Loader.LOBBY_ENABLED) {
			Class345.worldLogin(string, string_50_, i);
			return;
		}
		Class9.anInt92 = -1543542220;
		Class9.aClass184_73 = client.aClass184_7218;
		Class455.method7558(false, false, string, string_50_, -1L);// hmm
	}
}
