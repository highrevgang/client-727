/* Class521_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class521_Sub1 extends Class521 {
	public Class275_Sub5[] aClass275_Sub5Array7965;
	Class521_Sub1 aClass521_Sub1_7966;
	public byte aByte7967;
	public byte aByte7968;
	int anInt7969;
	public Class206 aClass206_7970;

	abstract int method12982(Class282_Sub24[] class282_sub24s);

	abstract boolean method12983(Class505 class505, int i, int i_0_, int i_1_);

	abstract void method12984(int i);

	abstract boolean method12985(int i);

	abstract boolean method12986(int i);

	abstract boolean method12987(int i);

	abstract boolean method12988(Class505 class505);

	abstract Class285 method12989(Class505 class505);

	abstract Class285 method12990(Class505 class505, int i);

	abstract void method12991(Class505 class505, int i);

	public abstract Class200 method12992(Class505 class505, byte i);

	public abstract Class200 method12993(Class505 class505);

	Class521_Sub1(Class206 class206) {
		aClass206_7970 = class206;
	}

	static void method12994(Class200 class200, int i, int i_2_, int i_3_, Class528 class528) {
		if (null != class528)
			class200.method3253(i, i_2_, i_3_, class528.N(), class528.RA(), class528.ya(), class528.YA(), class528.o(), class528.AA(), class528.ha());
	}

	public abstract int method12995(int i);

	int method12996(int i) {
		return 0;
	}

	public int method12997(int i) {
		return -method12995(-812710006);
	}

	abstract boolean method12998(Class505 class505);

	abstract boolean method12999();

	abstract boolean method13000();

	abstract boolean method13001();

	abstract boolean method13002();

	public abstract int method13003();

	int method13004(int i, int i_4_, Class282_Sub24[] class282_sub24s, int i_5_) {
		long l = (((Class206) aClass206_7970).aLongArrayArrayArray2645[aByte7967][i][i_4_]);
		long l_6_ = 0L;
		int i_7_ = 0;
		for (/**/; l_6_ <= 48L; l_6_ += 16L) {
			int i_8_ = (int) (l >> (int) l_6_ & 0xffffL);
			if (i_8_ <= 0)
				break;
			class282_sub24s[i_7_++] = (((Class206) aClass206_7970).aClass287Array2646[i_8_ - 1].aClass282_Sub24_3425);
		}
		for (int i_9_ = i_7_; i_9_ < 4; i_9_++)
			class282_sub24s[i_9_] = null;
		return i_7_;
	}

	public abstract int method13005();

	public abstract int method13006();

	public int method13007() {
		return -method12995(-348576668);
	}

	void method13008(int i, byte i_10_) {
		aClass275_Sub5Array7965 = new Class275_Sub5[i];
		for (int i_11_ = 0; i_11_ < aClass275_Sub5Array7965.length; i_11_++)
			aClass275_Sub5Array7965[i_11_] = new Class275_Sub5();
	}

	abstract Class285 method13009(Class505 class505);

	abstract Class285 method13010(Class505 class505);

	abstract boolean method13011();

	abstract void method13012(Class505 class505);

	abstract void method13013(Class505 class505, Class521_Sub1 class521_sub1_12_, int i, int i_13_, int i_14_, boolean bool, int i_15_);

	int method13014(int i, int i_16_, Class282_Sub24[] class282_sub24s) {
		long l = (((Class206) aClass206_7970).aLongArrayArrayArray2645[aByte7967][i][i_16_]);
		long l_17_ = 0L;
		int i_18_ = 0;
		for (/**/; l_17_ <= 48L; l_17_ += 16L) {
			int i_19_ = (int) (l >> (int) l_17_ & 0xffffL);
			if (i_19_ <= 0)
				break;
			class282_sub24s[i_18_++] = (((Class206) aClass206_7970).aClass287Array2646[i_19_ - 1].aClass282_Sub24_3425);
		}
		for (int i_20_ = i_18_; i_20_ < 4; i_20_++)
			class282_sub24s[i_20_] = null;
		return i_18_;
	}

	abstract void method13015();

	abstract void method13016(Class505 class505, Class521_Sub1 class521_sub1_21_, int i, int i_22_, int i_23_, boolean bool);

	public abstract int method13017();

	public abstract Class200 method13018(Class505 class505);

	public abstract Class200 method13019(Class505 class505);

	abstract boolean method13020(Class505 class505, int i, int i_24_);

	abstract void method13021();

	abstract boolean method13022(Class505 class505);

	abstract void method13023(Class505 class505);

	abstract int method13024(Class282_Sub24[] class282_sub24s);

	abstract int method13025(Class282_Sub24[] class282_sub24s);

	abstract boolean method13026();

	int method13027() {
		return 0;
	}

	public int method13028() {
		return -method12995(-1268783438);
	}

	abstract boolean method13029(byte i);

	abstract boolean method13030(Class505 class505);

	abstract int method13031(Class282_Sub24[] class282_sub24s);

	abstract boolean method13032();

	abstract boolean method13033();

	abstract boolean method13034();

	int method13035(int i, int i_25_, Class282_Sub24[] class282_sub24s) {
		long l = (((Class206) aClass206_7970).aLongArrayArrayArray2645[aByte7967][i][i_25_]);
		long l_26_ = 0L;
		int i_27_ = 0;
		for (/**/; l_26_ <= 48L; l_26_ += 16L) {
			int i_28_ = (int) (l >> (int) l_26_ & 0xffffL);
			if (i_28_ <= 0)
				break;
			class282_sub24s[i_27_++] = (((Class206) aClass206_7970).aClass287Array2646[i_28_ - 1].aClass282_Sub24_3425);
		}
		for (int i_29_ = i_27_; i_29_ < 4; i_29_++)
			class282_sub24s[i_29_] = null;
		return i_27_;
	}

	abstract int method13036(Class282_Sub24[] class282_sub24s, int i);

	abstract boolean method13037(Class505 class505, int i);

	static void method13038(Class200 class200, int i, int i_30_, int i_31_, Class528 class528) {
		if (null != class528)
			class200.method3253(i, i_30_, i_31_, class528.N(), class528.RA(), class528.ya(), class528.YA(), class528.o(), class528.AA(), class528.ha());
	}

	void method13039(int i) {
		aClass275_Sub5Array7965 = new Class275_Sub5[i];
		for (int i_32_ = 0; i_32_ < aClass275_Sub5Array7965.length; i_32_++)
			aClass275_Sub5Array7965[i_32_] = new Class275_Sub5();
	}

	static final void method13040(Class527 class527, byte i) {
		int i_33_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = client.aClass330Array7428[i_33_].anInt3866 * -1611209891;
	}

	static final void method13041(Class527 class527, int i) {
		int i_34_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_34_, (byte) 107);
		((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1] = class118.aString1391;
	}
}
