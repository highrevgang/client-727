/* Class282_Sub20_Sub30 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub20_Sub30 extends Class282_Sub20 {
	int[] method12336(int i) {
		int[] is = aClass320_7667.method5721(i, -2072025875);
		if (aClass320_7667.aBool3722) {
			int[][] is_0_ = method12333(0, i, (byte) 3);
			int[] is_1_ = is_0_[0];
			int[] is_2_ = is_0_[1];
			int[] is_3_ = is_0_[2];
			for (int i_4_ = 0; i_4_ < -402153223 * Class316.anInt3670; i_4_++)
				is[i_4_] = (is_3_[i_4_] + (is_2_[i_4_] + is_1_[i_4_])) / 3;
		}
		return is;
	}

	int[] method12319(int i, int i_5_) {
		int[] is = aClass320_7667.method5721(i, -1856155747);
		if (aClass320_7667.aBool3722) {
			int[][] is_6_ = method12333(0, i, (byte) 3);
			int[] is_7_ = is_6_[0];
			int[] is_8_ = is_6_[1];
			int[] is_9_ = is_6_[2];
			for (int i_10_ = 0; i_10_ < -402153223 * Class316.anInt3670; i_10_++)
				is[i_10_] = (is_9_[i_10_] + (is_8_[i_10_] + is_7_[i_10_])) / 3;
		}
		return is;
	}

	int[] method12325(int i) {
		int[] is = aClass320_7667.method5721(i, -1556248594);
		if (aClass320_7667.aBool3722) {
			int[][] is_11_ = method12333(0, i, (byte) 3);
			int[] is_12_ = is_11_[0];
			int[] is_13_ = is_11_[1];
			int[] is_14_ = is_11_[2];
			for (int i_15_ = 0; i_15_ < -402153223 * Class316.anInt3670; i_15_++)
				is[i_15_] = (is_14_[i_15_] + (is_13_[i_15_] + is_12_[i_15_])) / 3;
		}
		return is;
	}

	public Class282_Sub20_Sub30() {
		super(1, true);
	}

	int[] method12327(int i) {
		int[] is = aClass320_7667.method5721(i, -220974753);
		if (aClass320_7667.aBool3722) {
			int[][] is_16_ = method12333(0, i, (byte) 3);
			int[] is_17_ = is_16_[0];
			int[] is_18_ = is_16_[1];
			int[] is_19_ = is_16_[2];
			for (int i_20_ = 0; i_20_ < -402153223 * Class316.anInt3670; i_20_++)
				is[i_20_] = (is_19_[i_20_] + (is_18_[i_20_] + is_17_[i_20_])) / 3;
		}
		return is;
	}

	static final void method15399(Class527 class527, int i) {
		Class242.method4167((String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]), 1836583632);
	}
}
