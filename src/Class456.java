/* Class456 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class456 {
	Class518 aClass518_5453;
	public static final int anInt5454 = 1;
	public static final int anInt5455 = 2;
	boolean aBool5456;
	int anInt5457;
	int anInt5458;
	int anInt5459;
	int anInt5460;
	int anInt5461;
	boolean aBool5462 = false;
	boolean aBool5463 = false;
	Class462 aClass462_5464;
	public static final int anInt5465 = 0;
	int anInt5466;
	Class462 aClass462_5467;

	final boolean method7562(int i) {
		if (((Class456) this).aClass518_5453 != null) {
			boolean bool = (((Class456) this).aClass462_5464.method7707(Class40.aClass523_411, ((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 1844755981 * ((Class456) this).anInt5466, ((Class456) this).aClass518_5453.anIntArray5922, -514647052));
			if (bool && ((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null)
				((Class456) this).aClass462_5467.method7707(Class40.aClass523_411, ((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, ((Class456) this).anInt5466 * 1844755981, ((Class456) this).aClass518_5453.anIntArray5911, -1635915089);
			return bool;
		}
		return false;
	}

	public final void method7563(Class456 class456_0_, int i) {
		((Class456) this).aClass518_5453 = ((Class456) class456_0_).aClass518_5453;
		((Class456) this).aBool5462 = ((Class456) class456_0_).aBool5462;
		((Class456) this).aBool5463 = ((Class456) class456_0_).aBool5463;
		((Class456) this).anInt5458 = 1 * ((Class456) class456_0_).anInt5458;
		((Class456) this).anInt5459 = ((Class456) class456_0_).anInt5459 * 1;
		((Class456) this).anInt5460 = ((Class456) class456_0_).anInt5460 * 1;
		((Class456) this).anInt5466 = ((Class456) class456_0_).anInt5466 * 1;
		((Class456) this).anInt5457 = ((Class456) class456_0_).anInt5457 * 1;
		method7588((byte) 78);
	}

	public final boolean method7564(int i) {
		return null != ((Class456) this).aClass518_5453;
	}

	public final Class518 method7565(int i) {
		return ((Class456) this).aClass518_5453;
	}

	public final int method7566() {
		return ((Class456) this).anInt5458 * 1072784051;
	}

	public final void method7567(int i, short i_1_) {
		method7571(i, 0, 0, false, -629553578);
	}

	public final void method7568(int i) {
		method7571(i, 0, 0, false, -433885893);
	}

	public final void method7569(int i, boolean bool, int i_2_) {
		method7571(i, 0, 0, bool, -731012703);
	}

	public final void method7570(int i, boolean bool, boolean bool_3_, int i_4_) {
		method7572(i, 0, 0, bool, bool_3_, (byte) 104);
	}

	public final void method7571(int i, int i_5_, int i_6_, boolean bool, int i_7_) {
		method7572(i, i_5_, i_6_, bool, false, (byte) 62);
	}

	final void method7572(int i, int i_8_, int i_9_, boolean bool, boolean bool_10_, byte i_11_) {
		if (i != method7597(-1779648283)) {
			if (-1 != i) {
				if (((Class456) this).aClass518_5453 != null && (((Class456) this).aClass518_5453.anInt5909 * -2014062571) == i) {
					if (0 == (((Class456) this).aClass518_5453.anInt5907 * 554947543))
						return;
				} else
					((Class456) this).aClass518_5453 = Class40.aClass523_411.method11205(i, (byte) 11);
				((Class456) this).anInt5459 = 0;
				((Class456) this).anInt5458 = i_8_ * 333323387;
				((Class456) this).anInt5461 = -1783646677 * i_9_;
				((Class456) this).aBool5456 = bool_10_;
				if (bool) {
					((Class456) this).anInt5460 = ((int) (Math.random() * (double) (((Class456) this).aClass518_5453.anIntArray5922).length) * 723443617);
					((Class456) this).anInt5457 = (int) (Math.random() * (double) (((Class456) this).aClass518_5453.anIntArray5912[(((Class456) this).anInt5460 * 643220577)])) * 1755763003;
				} else {
					((Class456) this).anInt5460 = 0;
					((Class456) this).anInt5457 = 0;
				}
				((Class456) this).anInt5466 = 1017168069 + 212721317 * ((Class456) this).anInt5460;
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
				if (1072784051 * ((Class456) this).anInt5458 == 0)
					method7586(((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 395624971);
				((Class456) this).aBool5462 = false;
			} else
				((Class456) this).aClass518_5453 = null;
			method7588((byte) 42);
		}
	}

	public final boolean method7573(int i) {
		return ((Class456) this).anInt5458 * 1072784051 != 0;
	}

	public final int method7574(int i) {
		return ((Class456) this).anInt5458 * 1072784051;
	}

	public final void method7575(int i, int i_12_) {
		((Class456) this).anInt5458 = 333323387 * i;
	}

	public static final void method7576(Class528 class528, Class456 class456, Class456 class456_13_) {
		if (class456.method7562(1278048524) && class456_13_.method7562(1436301107)) {
			Class518 class518 = ((Class456) class456).aClass518_5453;
			Class518 class518_14_ = ((Class456) class456_13_).aClass518_5453;
			class528.method11264((((Class462) ((Class456) class456).aClass462_5464).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) ((Class456) class456).aClass462_5464).anInt5542, (((Class462) ((Class456) class456).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) class456).aClass462_5464).anInt5547 * -775231561), ((Class456) class456).anInt5457 * -706748429, (class518.anIntArray5912[643220577 * ((Class456) class456).anInt5460]), (((Class462) ((Class456) class456_13_).aClass462_5464).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) (((Class456) class456_13_).aClass462_5464)).anInt5542, (((Class462) ((Class456) class456_13_).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) class456_13_).aClass462_5464).anInt5547) * -775231561, ((Class456) class456_13_).anInt5457 * -706748429, (class518_14_.anIntArray5912[643220577 * ((Class456) class456_13_).anInt5460]), class518.aBoolArray5915, class518.aBool5923 | class518_14_.aBool5923);
		}
	}

	public final void method7577(Class528 class528, int i, int i_15_) {
		if (null != ((Class456) this).aClass518_5453) {
			if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(2049608469)) {
				class528.method11262((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5464).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5464).anInt5547 * -775231561), -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
				if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
					class528.method11262((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) (((Class456) this).aClass462_5467)).anInt5542, (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547) * -775231561, -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
			}
		}
	}

	public final void method7578(Class528 class528, int i, int i_16_, int i_17_) {
		if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1399567490)) {
			class528.method11258((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (-104209121 * ((Class462) ((Class456) this).aClass462_5464).anInt5542), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (-775231561 * ((Class462) ((Class456) this).aClass462_5464).anInt5547), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, i_16_, ((Class456) this).aClass518_5453.aBool5923, null);
			if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
				class528.method11258((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5467).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547 * -775231561), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[643220577 * ((Class456) this).anInt5460]), i, i_16_, ((Class456) this).aClass518_5453.aBool5923, null);
		}
	}

	public final void method7579(Class528 class528, int i) {
		if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1730078988)) {
			class528.method11284((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5464).anInt5542) * -104209121);
			if (((Class456) this).aBool5463 && null != ((Class456) this).aClass518_5453.anIntArray5911 && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
				class528.method11284((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), (((Class462) (((Class456) this).aClass462_5467)).anInt5542 * -104209121));
		}
	}

	public final boolean method7580(int i) {
		return ((Class456) this).aBool5462;
	}

	final boolean method7581() {
		if (((Class456) this).aClass518_5453 != null) {
			boolean bool = (((Class456) this).aClass462_5464.method7707(Class40.aClass523_411, ((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 1844755981 * ((Class456) this).anInt5466, ((Class456) this).aClass518_5453.anIntArray5922, -193306578));
			if (bool && ((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null)
				((Class456) this).aClass462_5467.method7707(Class40.aClass523_411, ((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, ((Class456) this).anInt5466 * 1844755981, ((Class456) this).aClass518_5453.anIntArray5911, -1697429264);
			return bool;
		}
		return false;
	}

	public final void method7582(byte i) {
		method7583(0, (byte) 0);
	}

	public final void method7583(int i, byte i_18_) {
		((Class456) this).anInt5460 = 0;
		((Class456) this).anInt5466 = (1017168069 * (((Class456) this).aClass518_5453.anIntArray5922.length > 1 ? 1 : -1));
		((Class456) this).anInt5457 = 0;
		((Class456) this).aBool5462 = false;
		((Class456) this).anInt5458 = 333323387 * i;
		((Class456) this).anInt5459 = 0;
		if (((Class456) this).aClass518_5453 != null & ((Class456) this).aClass518_5453.anIntArray5922 != null) {
			method7586(((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, -722856799);
			method7588((byte) 59);
		}
	}

	public final void method7584(int i) {
		((Class456) this).anInt5459 = 0;
	}

	public final boolean method7585(int i, int i_19_) {
		if (((Class456) this).aClass518_5453 == null | (i -= ((Class456) this).anInt5458 * 1072784051) <= 0)
			return false;
		return (((Class456) this).aClass518_5453.aBool5924 | (((Class456) this).anInt5457 * -706748429 + i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])));
	}

	void method7586(Class518 class518, int i, int i_20_) {
		/* empty */
	}

	public final boolean method7587() {
		return null != ((Class456) this).aClass518_5453;
	}

	final void method7588(byte i) {
		((Class456) this).aClass462_5464.method7708(13720241);
		if (((Class456) this).aBool5463)
			((Class456) this).aClass462_5467.method7708(13720241);
	}

	public final void method7589(Class528 class528, int i) {
		if (null != ((Class456) this).aClass518_5453) {
			if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1450710719)) {
				class528.method11262((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5464).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5464).anInt5547 * -775231561), -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
				if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
					class528.method11262((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) (((Class456) this).aClass462_5467)).anInt5542, (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547) * -775231561, -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
			}
		}
	}

	void method7590(Class518 class518, int i) {
		/* empty */
	}

	void method7591(Class518 class518, int i) {
		/* empty */
	}

	void method7592(Class518 class518, int i) {
		/* empty */
	}

	final void method7593(int i, int i_21_, int i_22_, boolean bool, boolean bool_23_) {
		if (i != method7597(-803187390)) {
			if (-1 != i) {
				if (((Class456) this).aClass518_5453 != null && (((Class456) this).aClass518_5453.anInt5909 * -2014062571) == i) {
					if (0 == (((Class456) this).aClass518_5453.anInt5907 * 554947543))
						return;
				} else
					((Class456) this).aClass518_5453 = Class40.aClass523_411.method11205(i, (byte) -22);
				((Class456) this).anInt5459 = 0;
				((Class456) this).anInt5458 = i_21_ * 333323387;
				((Class456) this).anInt5461 = -1783646677 * i_22_;
				((Class456) this).aBool5456 = bool_23_;
				if (bool) {
					((Class456) this).anInt5460 = ((int) (Math.random() * (double) (((Class456) this).aClass518_5453.anIntArray5922).length) * 723443617);
					((Class456) this).anInt5457 = (int) (Math.random() * (double) (((Class456) this).aClass518_5453.anIntArray5912[(((Class456) this).anInt5460 * 643220577)])) * 1755763003;
				} else {
					((Class456) this).anInt5460 = 0;
					((Class456) this).anInt5457 = 0;
				}
				((Class456) this).anInt5466 = 1017168069 + 212721317 * ((Class456) this).anInt5460;
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
				if (1072784051 * ((Class456) this).anInt5458 == 0)
					method7586(((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 375192779);
				((Class456) this).aBool5462 = false;
			} else
				((Class456) this).aClass518_5453 = null;
			method7588((byte) 100);
		}
	}

	void method7594(Class518 class518, int i) {
		/* empty */
	}

	public final void method7595(int i) {
		((Class456) this).anInt5458 = 333323387 * i;
	}

	public final void method7596(Class456 class456_24_) {
		((Class456) this).aClass518_5453 = ((Class456) class456_24_).aClass518_5453;
		((Class456) this).aBool5462 = ((Class456) class456_24_).aBool5462;
		((Class456) this).aBool5463 = ((Class456) class456_24_).aBool5463;
		((Class456) this).anInt5458 = 1 * ((Class456) class456_24_).anInt5458;
		((Class456) this).anInt5459 = ((Class456) class456_24_).anInt5459 * 1;
		((Class456) this).anInt5460 = ((Class456) class456_24_).anInt5460 * 1;
		((Class456) this).anInt5466 = ((Class456) class456_24_).anInt5466 * 1;
		((Class456) this).anInt5457 = ((Class456) class456_24_).anInt5457 * 1;
		method7588((byte) 79);
	}

	public final int method7597(int i) {
		return (null != ((Class456) this).aClass518_5453 ? -2014062571 * ((Class456) this).aClass518_5453.anInt5909 : -1);
	}

	public final void method7598(Class456 class456_25_) {
		((Class456) this).aClass518_5453 = ((Class456) class456_25_).aClass518_5453;
		((Class456) this).aBool5462 = ((Class456) class456_25_).aBool5462;
		((Class456) this).aBool5463 = ((Class456) class456_25_).aBool5463;
		((Class456) this).anInt5458 = 1 * ((Class456) class456_25_).anInt5458;
		((Class456) this).anInt5459 = ((Class456) class456_25_).anInt5459 * 1;
		((Class456) this).anInt5460 = ((Class456) class456_25_).anInt5460 * 1;
		((Class456) this).anInt5466 = ((Class456) class456_25_).anInt5466 * 1;
		((Class456) this).anInt5457 = ((Class456) class456_25_).anInt5457 * 1;
		method7588((byte) 59);
	}

	public final boolean method7599() {
		return null != ((Class456) this).aClass518_5453;
	}

	public final void method7600(int i, int i_26_, int i_27_, boolean bool) {
		method7572(i, i_26_, i_27_, bool, false, (byte) 84);
	}

	public final Class518 method7601() {
		return ((Class456) this).aClass518_5453;
	}

	public final int method7602() {
		return (null != ((Class456) this).aClass518_5453 ? -2014062571 * ((Class456) this).aClass518_5453.anInt5909 : -1);
	}

	public final int method7603() {
		return (null != ((Class456) this).aClass518_5453 ? -2014062571 * ((Class456) this).aClass518_5453.anInt5909 : -1);
	}

	public final void method7604(int i) {
		method7571(i, 0, 0, false, 1590546828);
	}

	public static final void method7605(Class528 class528, Class456 class456, Class456 class456_28_) {
		if (class456.method7562(1960893510) && class456_28_.method7562(1278290543)) {
			Class518 class518 = ((Class456) class456).aClass518_5453;
			Class518 class518_29_ = ((Class456) class456_28_).aClass518_5453;
			class528.method11264((((Class462) ((Class456) class456).aClass462_5464).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) ((Class456) class456).aClass462_5464).anInt5542, (((Class462) ((Class456) class456).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) class456).aClass462_5464).anInt5547 * -775231561), ((Class456) class456).anInt5457 * -706748429, (class518.anIntArray5912[643220577 * ((Class456) class456).anInt5460]), (((Class462) ((Class456) class456_28_).aClass462_5464).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) (((Class456) class456_28_).aClass462_5464)).anInt5542, (((Class462) ((Class456) class456_28_).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) class456_28_).aClass462_5464).anInt5547) * -775231561, ((Class456) class456_28_).anInt5457 * -706748429, (class518_29_.anIntArray5912[643220577 * ((Class456) class456_28_).anInt5460]), class518.aBoolArray5915, class518.aBool5923 | class518_29_.aBool5923);
		}
	}

	public final void method7606(int i, int i_30_) {
		method7571(i, i_30_, 0, false, -1383376355);
	}

	public final boolean method7607(int i) {
		if (((Class456) this).aClass518_5453 == null | (i -= ((Class456) this).anInt5458 * 1072784051) <= 0)
			return false;
		return (((Class456) this).aClass518_5453.aBool5924 | (((Class456) this).anInt5457 * -706748429 + i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])));
	}

	public final void method7608(int i, boolean bool, boolean bool_31_) {
		method7572(i, 0, 0, bool, bool_31_, (byte) 41);
	}

	public final void method7609(int i, int i_32_, int i_33_, boolean bool) {
		method7572(i, i_32_, i_33_, bool, false, (byte) 23);
	}

	public final void method7610(Class456 class456_34_) {
		((Class456) this).aClass518_5453 = ((Class456) class456_34_).aClass518_5453;
		((Class456) this).aBool5462 = ((Class456) class456_34_).aBool5462;
		((Class456) this).aBool5463 = ((Class456) class456_34_).aBool5463;
		((Class456) this).anInt5458 = 1 * ((Class456) class456_34_).anInt5458;
		((Class456) this).anInt5459 = ((Class456) class456_34_).anInt5459 * 1;
		((Class456) this).anInt5460 = ((Class456) class456_34_).anInt5460 * 1;
		((Class456) this).anInt5466 = ((Class456) class456_34_).anInt5466 * 1;
		((Class456) this).anInt5457 = ((Class456) class456_34_).anInt5457 * 1;
		method7588((byte) 85);
	}

	public final void method7611(int i, int i_35_, int i_36_, boolean bool) {
		method7572(i, i_35_, i_36_, bool, false, (byte) 66);
	}

	Class456(boolean bool) {
		((Class456) this).anInt5461 = 0;
		((Class456) this).aBool5456 = false;
		((Class456) this).aBool5463 = bool;
		((Class456) this).aClass462_5464 = new Class462();
		if (((Class456) this).aBool5463)
			((Class456) this).aClass462_5467 = new Class462();
		else
			((Class456) this).aClass462_5467 = null;
	}

	final void method7612(int i, int i_37_, int i_38_, boolean bool, boolean bool_39_) {
		if (i != method7597(-592905508)) {
			if (-1 != i) {
				if (((Class456) this).aClass518_5453 != null && (((Class456) this).aClass518_5453.anInt5909 * -2014062571) == i) {
					if (0 == (((Class456) this).aClass518_5453.anInt5907 * 554947543))
						return;
				} else
					((Class456) this).aClass518_5453 = Class40.aClass523_411.method11205(i, (byte) -97);
				((Class456) this).anInt5459 = 0;
				((Class456) this).anInt5458 = i_37_ * 333323387;
				((Class456) this).anInt5461 = -1783646677 * i_38_;
				((Class456) this).aBool5456 = bool_39_;
				if (bool) {
					((Class456) this).anInt5460 = ((int) (Math.random() * (double) (((Class456) this).aClass518_5453.anIntArray5922).length) * 723443617);
					((Class456) this).anInt5457 = (int) (Math.random() * (double) (((Class456) this).aClass518_5453.anIntArray5912[(((Class456) this).anInt5460 * 643220577)])) * 1755763003;
				} else {
					((Class456) this).anInt5460 = 0;
					((Class456) this).anInt5457 = 0;
				}
				((Class456) this).anInt5466 = 1017168069 + 212721317 * ((Class456) this).anInt5460;
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
				if (1072784051 * ((Class456) this).anInt5458 == 0)
					method7586(((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 1016890700);
				((Class456) this).aBool5462 = false;
			} else
				((Class456) this).aClass518_5453 = null;
			method7588((byte) 24);
		}
	}

	public final boolean method7613() {
		return ((Class456) this).anInt5458 * 1072784051 != 0;
	}

	public final boolean method7614() {
		return ((Class456) this).anInt5458 * 1072784051 != 0;
	}

	public final void method7615(int i, int i_40_, int i_41_) {
		method7571(i, i_40_, 0, false, 1549105687);
	}

	void method7616(Class518 class518, int i) {
		/* empty */
	}

	public final int method7617() {
		return ((Class456) this).anInt5458 * 1072784051;
	}

	public final void method7618(Class528 class528, int i) {
		if (null != ((Class456) this).aClass518_5453) {
			if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1958824690)) {
				class528.method11262((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5464).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5464).anInt5547 * -775231561), -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
				if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
					class528.method11262((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) (((Class456) this).aClass462_5467)).anInt5542, (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547) * -775231561, -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
			}
		}
	}

	public final void method7619(int i, boolean bool) {
		method7571(i, 0, 0, bool, -512925645);
	}

	public final void method7620(Class528 class528, int i) {
		if (null != ((Class456) this).aClass518_5453) {
			if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1731645273)) {
				class528.method11262((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5464).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5464).anInt5547 * -775231561), -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
				if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
					class528.method11262((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) (((Class456) this).aClass462_5467)).anInt5542, (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547) * -775231561, -706748429 * ((Class456) this).anInt5457, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, ((Class456) this).aClass518_5453.aBool5923);
			}
		}
	}

	public final void method7621(Class528 class528, int i, int i_42_) {
		if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1700559618)) {
			class528.method11258((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (-104209121 * ((Class462) ((Class456) this).aClass462_5464).anInt5542), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (-775231561 * ((Class462) ((Class456) this).aClass462_5464).anInt5547), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, i_42_, ((Class456) this).aClass518_5453.aBool5923, null);
			if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
				class528.method11258((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5467).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547 * -775231561), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[643220577 * ((Class456) this).anInt5460]), i, i_42_, ((Class456) this).aClass518_5453.aBool5923, null);
		}
	}

	public final void method7622(Class528 class528, int i, int i_43_) {
		if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1469252242)) {
			class528.method11258((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (-104209121 * ((Class462) ((Class456) this).aClass462_5464).anInt5542), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (-775231561 * ((Class462) ((Class456) this).aClass462_5464).anInt5547), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, i_43_, ((Class456) this).aClass518_5453.aBool5923, null);
			if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
				class528.method11258((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5467).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547 * -775231561), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[643220577 * ((Class456) this).anInt5460]), i, i_43_, ((Class456) this).aClass518_5453.aBool5923, null);
		}
	}

	public final void method7623(Class528 class528, int i, int i_44_) {
		if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1636305638)) {
			class528.method11258((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (-104209121 * ((Class462) ((Class456) this).aClass462_5464).anInt5542), (((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5546), (-775231561 * ((Class462) ((Class456) this).aClass462_5464).anInt5547), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577]), i, i_44_, ((Class456) this).aClass518_5453.aBool5923, null);
			if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
				class528.method11258((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5467).anInt5542 * -104209121), (((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) this).aClass462_5467).anInt5547 * -775231561), ((Class456) this).anInt5457 * -706748429, (((Class456) this).aClass518_5453.anIntArray5912[643220577 * ((Class456) this).anInt5460]), i, i_44_, ((Class456) this).aClass518_5453.aBool5923, null);
		}
	}

	public final void method7624(Class528 class528) {
		if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(2104150474)) {
			class528.method11284((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5464).anInt5542) * -104209121);
			if (((Class456) this).aBool5463 && null != ((Class456) this).aClass518_5453.anIntArray5911 && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
				class528.method11284((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), (((Class462) (((Class456) this).aClass462_5467)).anInt5542 * -104209121));
		}
	}

	public final void method7625(Class528 class528) {
		if (null != ((Class456) this).aClass518_5453.anIntArray5922 && method7562(1582887445)) {
			class528.method11284((((Class462) ((Class456) this).aClass462_5464).aClass282_Sub50_Sub13_5545), (((Class462) ((Class456) this).aClass462_5464).anInt5542) * -104209121);
			if (((Class456) this).aBool5463 && null != ((Class456) this).aClass518_5453.anIntArray5911 && ((Class462) ((Class456) this).aClass462_5467).aBool5544)
				class528.method11284((((Class462) ((Class456) this).aClass462_5467).aClass282_Sub50_Sub13_5545), (((Class462) (((Class456) this).aClass462_5467)).anInt5542 * -104209121));
		}
	}

	void method7626(Class518 class518, int i) {
		/* empty */
	}

	public final boolean method7627(int i, int i_45_) {
		if (null == ((Class456) this).aClass518_5453 || 0 == i)
			return false;
		if (((Class456) this).anInt5458 * 1072784051 > 0) {
			if (((Class456) this).anInt5458 * 1072784051 >= i) {
				((Class456) this).anInt5458 -= i * 333323387;
				return false;
			}
			i -= 1072784051 * ((Class456) this).anInt5458;
			((Class456) this).anInt5458 = 0;
			method7586(((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 61864237);
		}
		i += ((Class456) this).anInt5457 * -706748429;
		boolean bool = ((Class456) this).aClass518_5453.aBool5924 | Class518.aBool5925;
		if (i > 100 && -542281047 * ((Class456) this).aClass518_5453.anInt5914 > 0) {
			int i_46_;
			for (i_46_ = (((Class456) this).aClass518_5453.anIntArray5922.length - (((Class456) this).aClass518_5453.anInt5914 * -542281047)); (643220577 * ((Class456) this).anInt5460 < i_46_ && i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])); ((Class456) this).anInt5460 += 723443617)
				i -= (((Class456) this).aClass518_5453.anIntArray5912[643220577 * ((Class456) this).anInt5460]);
			if (((Class456) this).anInt5460 * 643220577 >= i_46_) {
				int i_47_ = 0;
				for (int i_48_ = i_46_; i_48_ < (((Class456) this).aClass518_5453.anIntArray5922).length; i_48_++)
					i_47_ += (((Class456) this).aClass518_5453.anIntArray5912[i_48_]);
				if (((Class456) this).anInt5461 * 619010179 == 0)
					((Class456) this).anInt5459 += i / i_47_ * -1013682765;
				i %= i_47_;
			}
			((Class456) this).anInt5466 = ((Class456) this).anInt5460 * 212721317 + 1017168069;
			if (1844755981 * ((Class456) this).anInt5466 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if (-1 == (((Class456) this).aClass518_5453.anInt5914 * -542281047) && ((Class456) this).aBool5456)
					((Class456) this).anInt5466 = 0;
				else
					((Class456) this).anInt5466 -= (2081946637 * ((Class456) this).aClass518_5453.anInt5914);
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
			}
			bool = true;
		}
		while (i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])) {
			bool = true;
			i -= (((Class456) this).aClass518_5453.anIntArray5912[((((Class456) this).anInt5460 += 723443617) * 643220577 - 1)]);
			if (643220577 * ((Class456) this).anInt5460 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if (-1 != (-542281047 * ((Class456) this).aClass518_5453.anInt5914) && ((Class456) this).anInt5461 * 619010179 != 2) {
					((Class456) this).anInt5460 -= (((Class456) this).aClass518_5453.anInt5914 * -1782295735);
					if (0 == ((Class456) this).anInt5461 * 619010179)
						((Class456) this).anInt5459 += -1013682765;
				}
				if ((((Class456) this).anInt5459 * 1323043195 >= ((Class456) this).aClass518_5453.anInt5929 * 820353795) || ((Class456) this).anInt5460 * 643220577 < 0 || (((Class456) this).anInt5460 * 643220577 >= (((Class456) this).aClass518_5453.anIntArray5922).length)) {
					((Class456) this).aBool5462 = true;
					break;
				}
			}
			method7586(((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, -469139589);
			((Class456) this).anInt5466 = 1017168069 + 212721317 * ((Class456) this).anInt5460;
			if (1844755981 * ((Class456) this).anInt5466 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if ((((Class456) this).aClass518_5453.anInt5914 * -542281047 == -1) && ((Class456) this).aBool5456)
					((Class456) this).anInt5466 = 0;
				else
					((Class456) this).anInt5466 -= (((Class456) this).aClass518_5453.anInt5914 * 2081946637);
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
			}
		}
		((Class456) this).anInt5457 = i * 1755763003;
		if (bool)
			method7588((byte) 30);
		return bool;
	}

	public final boolean method7628() {
		return ((Class456) this).aBool5462;
	}

	public final boolean method7629() {
		return ((Class456) this).aBool5462;
	}

	public final void method7630() {
		((Class456) this).anInt5459 = 0;
	}

	public final void method7631() {
		((Class456) this).anInt5459 = 0;
	}

	public final void method7632(int i) {
		((Class456) this).anInt5460 = 0;
		((Class456) this).anInt5466 = (1017168069 * (((Class456) this).aClass518_5453.anIntArray5922.length > 1 ? 1 : -1));
		((Class456) this).anInt5457 = 0;
		((Class456) this).aBool5462 = false;
		((Class456) this).anInt5458 = 333323387 * i;
		((Class456) this).anInt5459 = 0;
		if (((Class456) this).aClass518_5453 != null & ((Class456) this).aClass518_5453.anIntArray5922 != null) {
			method7586(((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, -2098259418);
			method7588((byte) 98);
		}
	}

	public final void method7633(int i) {
		((Class456) this).anInt5460 = 0;
		((Class456) this).anInt5466 = (1017168069 * (((Class456) this).aClass518_5453.anIntArray5922.length > 1 ? 1 : -1));
		((Class456) this).anInt5457 = 0;
		((Class456) this).aBool5462 = false;
		((Class456) this).anInt5458 = 333323387 * i;
		((Class456) this).anInt5459 = 0;
		if (((Class456) this).aClass518_5453 != null & ((Class456) this).aClass518_5453.anIntArray5922 != null) {
			method7586(((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, 294675199);
			method7588((byte) 118);
		}
	}

	public final boolean method7634(int i) {
		if (null == ((Class456) this).aClass518_5453 || 0 == i)
			return false;
		if (((Class456) this).anInt5458 * 1072784051 > 0) {
			if (((Class456) this).anInt5458 * 1072784051 >= i) {
				((Class456) this).anInt5458 -= i * 333323387;
				return false;
			}
			i -= 1072784051 * ((Class456) this).anInt5458;
			((Class456) this).anInt5458 = 0;
			method7586(((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, -434067382);
		}
		i += ((Class456) this).anInt5457 * -706748429;
		boolean bool = ((Class456) this).aClass518_5453.aBool5924 | Class518.aBool5925;
		if (i > 100 && -542281047 * ((Class456) this).aClass518_5453.anInt5914 > 0) {
			int i_49_;
			for (i_49_ = (((Class456) this).aClass518_5453.anIntArray5922.length - (((Class456) this).aClass518_5453.anInt5914 * -542281047)); (643220577 * ((Class456) this).anInt5460 < i_49_ && i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])); ((Class456) this).anInt5460 += 723443617)
				i -= (((Class456) this).aClass518_5453.anIntArray5912[643220577 * ((Class456) this).anInt5460]);
			if (((Class456) this).anInt5460 * 643220577 >= i_49_) {
				int i_50_ = 0;
				for (int i_51_ = i_49_; i_51_ < (((Class456) this).aClass518_5453.anIntArray5922).length; i_51_++)
					i_50_ += (((Class456) this).aClass518_5453.anIntArray5912[i_51_]);
				if (((Class456) this).anInt5461 * 619010179 == 0)
					((Class456) this).anInt5459 += i / i_50_ * -1013682765;
				i %= i_50_;
			}
			((Class456) this).anInt5466 = ((Class456) this).anInt5460 * 212721317 + 1017168069;
			if (1844755981 * ((Class456) this).anInt5466 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if (-1 == (((Class456) this).aClass518_5453.anInt5914 * -542281047) && ((Class456) this).aBool5456)
					((Class456) this).anInt5466 = 0;
				else
					((Class456) this).anInt5466 -= (2081946637 * ((Class456) this).aClass518_5453.anInt5914);
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
			}
			bool = true;
		}
		while (i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])) {
			bool = true;
			i -= (((Class456) this).aClass518_5453.anIntArray5912[((((Class456) this).anInt5460 += 723443617) * 643220577 - 1)]);
			if (643220577 * ((Class456) this).anInt5460 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if (-1 != (-542281047 * ((Class456) this).aClass518_5453.anInt5914) && ((Class456) this).anInt5461 * 619010179 != 2) {
					((Class456) this).anInt5460 -= (((Class456) this).aClass518_5453.anInt5914 * -1782295735);
					if (0 == ((Class456) this).anInt5461 * 619010179)
						((Class456) this).anInt5459 += -1013682765;
				}
				if ((((Class456) this).anInt5459 * 1323043195 >= ((Class456) this).aClass518_5453.anInt5929 * 820353795) || ((Class456) this).anInt5460 * 643220577 < 0 || (((Class456) this).anInt5460 * 643220577 >= (((Class456) this).aClass518_5453.anIntArray5922).length)) {
					((Class456) this).aBool5462 = true;
					break;
				}
			}
			method7586(((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, 812847685);
			((Class456) this).anInt5466 = 1017168069 + 212721317 * ((Class456) this).anInt5460;
			if (1844755981 * ((Class456) this).anInt5466 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if ((((Class456) this).aClass518_5453.anInt5914 * -542281047 == -1) && ((Class456) this).aBool5456)
					((Class456) this).anInt5466 = 0;
				else
					((Class456) this).anInt5466 -= (((Class456) this).aClass518_5453.anInt5914 * 2081946637);
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
			}
		}
		((Class456) this).anInt5457 = i * 1755763003;
		if (bool)
			method7588((byte) 23);
		return bool;
	}

	public final void method7635(int i) {
		((Class456) this).anInt5460 = 0;
		((Class456) this).anInt5466 = (1017168069 * (((Class456) this).aClass518_5453.anIntArray5922.length > 1 ? 1 : -1));
		((Class456) this).anInt5457 = 0;
		((Class456) this).aBool5462 = false;
		((Class456) this).anInt5458 = 333323387 * i;
		((Class456) this).anInt5459 = 0;
		if (((Class456) this).aClass518_5453 != null & ((Class456) this).aClass518_5453.anIntArray5922 != null) {
			method7586(((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, -1358729402);
			method7588((byte) 92);
		}
	}

	public final boolean method7636(int i) {
		if (null == ((Class456) this).aClass518_5453 || 0 == i)
			return false;
		if (((Class456) this).anInt5458 * 1072784051 > 0) {
			if (((Class456) this).anInt5458 * 1072784051 >= i) {
				((Class456) this).anInt5458 -= i * 333323387;
				return false;
			}
			i -= 1072784051 * ((Class456) this).anInt5458;
			((Class456) this).anInt5458 = 0;
			method7586(((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 699561857);
		}
		i += ((Class456) this).anInt5457 * -706748429;
		boolean bool = ((Class456) this).aClass518_5453.aBool5924 | Class518.aBool5925;
		if (i > 100 && -542281047 * ((Class456) this).aClass518_5453.anInt5914 > 0) {
			int i_52_;
			for (i_52_ = (((Class456) this).aClass518_5453.anIntArray5922.length - (((Class456) this).aClass518_5453.anInt5914 * -542281047)); (643220577 * ((Class456) this).anInt5460 < i_52_ && i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])); ((Class456) this).anInt5460 += 723443617)
				i -= (((Class456) this).aClass518_5453.anIntArray5912[643220577 * ((Class456) this).anInt5460]);
			if (((Class456) this).anInt5460 * 643220577 >= i_52_) {
				int i_53_ = 0;
				for (int i_54_ = i_52_; i_54_ < (((Class456) this).aClass518_5453.anIntArray5922).length; i_54_++)
					i_53_ += (((Class456) this).aClass518_5453.anIntArray5912[i_54_]);
				if (((Class456) this).anInt5461 * 619010179 == 0)
					((Class456) this).anInt5459 += i / i_53_ * -1013682765;
				i %= i_53_;
			}
			((Class456) this).anInt5466 = ((Class456) this).anInt5460 * 212721317 + 1017168069;
			if (1844755981 * ((Class456) this).anInt5466 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if (-1 == (((Class456) this).aClass518_5453.anInt5914 * -542281047) && ((Class456) this).aBool5456)
					((Class456) this).anInt5466 = 0;
				else
					((Class456) this).anInt5466 -= (2081946637 * ((Class456) this).aClass518_5453.anInt5914);
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
			}
			bool = true;
		}
		while (i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])) {
			bool = true;
			i -= (((Class456) this).aClass518_5453.anIntArray5912[((((Class456) this).anInt5460 += 723443617) * 643220577 - 1)]);
			if (643220577 * ((Class456) this).anInt5460 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if (-1 != (-542281047 * ((Class456) this).aClass518_5453.anInt5914) && ((Class456) this).anInt5461 * 619010179 != 2) {
					((Class456) this).anInt5460 -= (((Class456) this).aClass518_5453.anInt5914 * -1782295735);
					if (0 == ((Class456) this).anInt5461 * 619010179)
						((Class456) this).anInt5459 += -1013682765;
				}
				if ((((Class456) this).anInt5459 * 1323043195 >= ((Class456) this).aClass518_5453.anInt5929 * 820353795) || ((Class456) this).anInt5460 * 643220577 < 0 || (((Class456) this).anInt5460 * 643220577 >= (((Class456) this).aClass518_5453.anIntArray5922).length)) {
					((Class456) this).aBool5462 = true;
					break;
				}
			}
			method7586(((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, 2025741731);
			((Class456) this).anInt5466 = 1017168069 + 212721317 * ((Class456) this).anInt5460;
			if (1844755981 * ((Class456) this).anInt5466 >= ((Class456) this).aClass518_5453.anIntArray5922.length) {
				if ((((Class456) this).aClass518_5453.anInt5914 * -542281047 == -1) && ((Class456) this).aBool5456)
					((Class456) this).anInt5466 = 0;
				else
					((Class456) this).anInt5466 -= (((Class456) this).aClass518_5453.anInt5914 * 2081946637);
				if (((Class456) this).anInt5466 * 1844755981 < 0 || (((Class456) this).anInt5466 * 1844755981 >= (((Class456) this).aClass518_5453.anIntArray5922).length))
					((Class456) this).anInt5466 = -1017168069;
			}
		}
		((Class456) this).anInt5457 = i * 1755763003;
		if (bool)
			method7588((byte) 126);
		return bool;
	}

	public static void method7637(Class523 class523) {
		Class40.aClass523_411 = class523;
	}

	public final boolean method7638(int i) {
		if (((Class456) this).aClass518_5453 == null | (i -= ((Class456) this).anInt5458 * 1072784051) <= 0)
			return false;
		return (((Class456) this).aClass518_5453.aBool5924 | (((Class456) this).anInt5457 * -706748429 + i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])));
	}

	public final boolean method7639(int i) {
		if (((Class456) this).aClass518_5453 == null | (i -= ((Class456) this).anInt5458 * 1072784051) <= 0)
			return false;
		return (((Class456) this).aClass518_5453.aBool5924 | (((Class456) this).anInt5457 * -706748429 + i > (((Class456) this).aClass518_5453.anIntArray5912[((Class456) this).anInt5460 * 643220577])));
	}

	public final int method7640(int i) {
		if (method7562(1825517174)) {
			int i_55_ = 0;
			if (method7562(1608975787)) {
				i_55_ |= -1154737479 * ((Class462) (((Class456) this).aClass462_5464)).anInt5543;
				if (((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null)
					i_55_ |= (((Class462) ((Class456) this).aClass462_5467).anInt5543) * -1154737479;
			}
			return i_55_;
		}
		return 0;
	}

	final boolean method7641() {
		if (((Class456) this).aClass518_5453 != null) {
			boolean bool = (((Class456) this).aClass462_5464.method7707(Class40.aClass523_411, ((Class456) this).aClass518_5453, ((Class456) this).anInt5460 * 643220577, 1844755981 * ((Class456) this).anInt5466, ((Class456) this).aClass518_5453.anIntArray5922, 126016061));
			if (bool && ((Class456) this).aBool5463 && ((Class456) this).aClass518_5453.anIntArray5911 != null)
				((Class456) this).aClass462_5467.method7707(Class40.aClass523_411, ((Class456) this).aClass518_5453, 643220577 * ((Class456) this).anInt5460, ((Class456) this).anInt5466 * 1844755981, ((Class456) this).aClass518_5453.anIntArray5911, -1726308342);
			return bool;
		}
		return false;
	}

	final void method7642() {
		((Class456) this).aClass462_5464.method7708(13720241);
		if (((Class456) this).aBool5463)
			((Class456) this).aClass462_5467.method7708(13720241);
	}

	static final void method7643(Class527 class527, byte i) {
		int i_56_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_56_, (byte) 98);
		int i_57_ = -1;
		int i_58_ = -1;
		Class119 class119 = class118.method2046(Class316.aClass505_3680, 1740359651);
		if (null != class119) {
			if (i >= -1)
				throw new IllegalStateException();
			i_57_ = -1125753931 * class119.anInt1458;
			i_58_ = class119.anInt1454 * 2069222845;
		}
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_57_;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_58_;
	}

	static Class149_Sub1 method7644(RsByteBuffer class282_sub35, int i) {
		return new Class149_Sub1(class282_sub35.method13081(2106402099), class282_sub35.method13081(1584223282), class282_sub35.method13081(1636690940), class282_sub35.method13081(1585273759), class282_sub35.method13082((short) 9349), class282_sub35.method13082((short) 13270), class282_sub35.readUnsignedByte());
	}
}
