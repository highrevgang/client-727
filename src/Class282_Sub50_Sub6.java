/* Class282_Sub50_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub50_Sub6 extends Class282_Sub50 {
	public int anInt9532;
	public String aString9533;
	public String aString9534;
	public int anInt9535;
	public int anInt9536;
	Class482 aClass482_9537;
	int anInt9538 = -1732618221;
	public int anInt9539;
	public int anInt9540;
	public int anInt9541;
	public int anInt9542 = 1226471855;
	boolean aBool9543;

	public boolean method14775(int i, int i_0_, int[] is, int i_1_) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 104); class282_sub28 != null; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(756655030)) {
			if (class282_sub28.method12409(i, i_0_, (byte) 15)) {
				class282_sub28.method12410(i, i_0_, is, -161585074);
				return true;
			}
		}
		return false;
	}

	void method14776() {
		anInt9539 = -481251840;
		anInt9540 = 0;
		anInt9541 = -1308904960;
		anInt9535 = 0;
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 108); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(1164766753)) {
			if (((Class282_Sub28) class282_sub28).anInt7701 * 313968361 < anInt9539 * -742910705)
				anInt9539 = ((Class282_Sub28) class282_sub28).anInt7701 * -300313465;
			if (((Class282_Sub28) class282_sub28).anInt7704 * -734163897 > 235539227 * anInt9540)
				anInt9540 = (-1855681979 * ((Class282_Sub28) class282_sub28).anInt7704);
			if (-355220531 * ((Class282_Sub28) class282_sub28).anInt7703 < anInt9541 * 458255425)
				anInt9541 = ((Class282_Sub28) class282_sub28).anInt7703 * 412034189;
			if (-1734431313 * ((Class282_Sub28) class282_sub28).anInt7705 > anInt9535 * 1097246003)
				anInt9535 = 1890540949 * ((Class282_Sub28) class282_sub28).anInt7705;
		}
	}

	Class282_Sub50_Sub6(int i, String string, String string_2_, int i_3_, int i_4_, boolean bool, int i_5_, int i_6_) {
		anInt9539 = -481251840;
		anInt9540 = 0;
		anInt9541 = -1308904960;
		anInt9535 = 0;
		((Class282_Sub50_Sub6) this).aBool9543 = true;
		anInt9536 = -1198805455 * i;
		aString9533 = string;
		aString9534 = string_2_;
		anInt9532 = i_3_ * 1082750961;
		((Class282_Sub50_Sub6) this).anInt9538 = 1732618221 * i_4_;
		((Class282_Sub50_Sub6) this).aBool9543 = bool;
		anInt9542 = -1226471855 * i_5_;
		if (255 == -1337359695 * anInt9542)
			anInt9542 = 0;
		Class480.method8046(i_6_, (byte) 69);
		((Class282_Sub50_Sub6) this).aClass482_9537 = new Class482();
	}

	public boolean method14777(int i, int i_7_, int[] is, int i_8_) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 72); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(1143191300)) {
			if (class282_sub28.method12415(i, i_7_, 2103169135)) {
				class282_sub28.method12414(i, i_7_, is, -1628136570);
				return true;
			}
		}
		return false;
	}

	public boolean method14778(int i, int i_9_, int i_10_, int[] is, int i_11_) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 96); class282_sub28 != null; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(-376153003)) {
			if (class282_sub28.method12408(i, i_9_, i_10_, -752144716)) {
				class282_sub28.method12414(i_9_, i_10_, is, 698012248);
				return true;
			}
		}
		return false;
	}

	void method14779(byte i) {
		anInt9539 = -481251840;
		anInt9540 = 0;
		anInt9541 = -1308904960;
		anInt9535 = 0;
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 91); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(361173732)) {
			if (((Class282_Sub28) class282_sub28).anInt7701 * 313968361 < anInt9539 * -742910705)
				anInt9539 = ((Class282_Sub28) class282_sub28).anInt7701 * -300313465;
			if (((Class282_Sub28) class282_sub28).anInt7704 * -734163897 > 235539227 * anInt9540)
				anInt9540 = (-1855681979 * ((Class282_Sub28) class282_sub28).anInt7704);
			if (-355220531 * ((Class282_Sub28) class282_sub28).anInt7703 < anInt9541 * 458255425)
				anInt9541 = ((Class282_Sub28) class282_sub28).anInt7703 * 412034189;
			if (-1734431313 * ((Class282_Sub28) class282_sub28).anInt7705 > anInt9535 * 1097246003)
				anInt9535 = 1890540949 * ((Class282_Sub28) class282_sub28).anInt7705;
		}
	}

	boolean method14780(int i, int i_12_) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 57); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(1431283189)) {
			if (class282_sub28.method12415(i, i_12_, -35215805))
				return true;
		}
		return false;
	}

	void method14781() {
		anInt9539 = -481251840;
		anInt9540 = 0;
		anInt9541 = -1308904960;
		anInt9535 = 0;
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 64); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(1479491741)) {
			if (((Class282_Sub28) class282_sub28).anInt7701 * 313968361 < anInt9539 * -742910705)
				anInt9539 = ((Class282_Sub28) class282_sub28).anInt7701 * -300313465;
			if (((Class282_Sub28) class282_sub28).anInt7704 * -734163897 > 235539227 * anInt9540)
				anInt9540 = (-1855681979 * ((Class282_Sub28) class282_sub28).anInt7704);
			if (-355220531 * ((Class282_Sub28) class282_sub28).anInt7703 < anInt9541 * 458255425)
				anInt9541 = ((Class282_Sub28) class282_sub28).anInt7703 * 412034189;
			if (-1734431313 * ((Class282_Sub28) class282_sub28).anInt7705 > anInt9535 * 1097246003)
				anInt9535 = 1890540949 * ((Class282_Sub28) class282_sub28).anInt7705;
		}
	}

	public boolean method14782(int i, int i_13_, int[] is) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 70); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(657256049)) {
			if (class282_sub28.method12415(i, i_13_, 1900005912)) {
				class282_sub28.method12414(i, i_13_, is, 926978718);
				return true;
			}
		}
		return false;
	}

	public boolean method14783(int i, int i_14_, int[] is) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 94); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(-605266790)) {
			if (class282_sub28.method12415(i, i_14_, 1092797169)) {
				class282_sub28.method12414(i, i_14_, is, -97575652);
				return true;
			}
		}
		return false;
	}

	boolean method14784(int i, int i_15_, int i_16_) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 23); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(1597035848)) {
			if (class282_sub28.method12415(i, i_15_, 2088759506))
				return true;
		}
		return false;
	}

	public boolean method14785(int i, int i_17_, int[] is) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 118); class282_sub28 != null; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(-562017105)) {
			if (class282_sub28.method12409(i, i_17_, (byte) 33)) {
				class282_sub28.method12410(i, i_17_, is, 78420916);
				return true;
			}
		}
		return false;
	}

	public boolean method14786(int i, int i_18_, int[] is) {
		for (Class282_Sub28 class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8097((byte) 92); null != class282_sub28; class282_sub28 = (Class282_Sub28) ((Class282_Sub50_Sub6) this).aClass482_9537.method8067(-909167131)) {
			if (class282_sub28.method12415(i, i_18_, 469285759)) {
				class282_sub28.method12414(i, i_18_, is, -388607933);
				return true;
			}
		}
		return false;
	}

	static Class282_Sub50_Sub6 method14787(Class317 class317, int i, int i_19_) {
		RsByteBuffer class282_sub35 = new RsByteBuffer(class317.method5607(i, i_19_, -1290013407));
		Class282_Sub50_Sub6 class282_sub50_sub6 = new Class282_Sub50_Sub6(i_19_, class282_sub35.readString(418112910), class282_sub35.readString(629146262), class282_sub35.readIntLE(), class282_sub35.readIntLE(), (class282_sub35.readUnsignedByte() == 1), class282_sub35.readUnsignedByte(), class282_sub35.readUnsignedByte());
		int i_20_ = class282_sub35.readUnsignedByte();
		for (int i_21_ = 0; i_21_ < i_20_; i_21_++)
			((Class282_Sub50_Sub6) class282_sub50_sub6).aClass482_9537.method8059(new Class282_Sub28(class282_sub35.readUnsignedByte(), class282_sub35.readUnsignedShort(), class282_sub35.readUnsignedShort(), class282_sub35.readUnsignedShort(), class282_sub35.readUnsignedShort(), class282_sub35.readUnsignedShort(), class282_sub35.readUnsignedShort(), class282_sub35.readUnsignedShort(), class282_sub35.readUnsignedShort()), 745692094);
		class282_sub50_sub6.method14779((byte) 64);
		return class282_sub50_sub6;
	}

	public static void method14788(String string, boolean bool, boolean bool_22_, boolean bool_23_, boolean bool_24_, byte i) {
		Class508.method8736(string, bool, bool_22_, "openjs", bool_23_, bool_24_, (byte) 60);
	}
}
