/* Class521_Sub1_Sub3_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class521_Sub1_Sub3_Sub1 extends Class521_Sub1_Sub3 implements Interface12 {
	Class282_Sub50_Sub17 aClass282_Sub50_Sub17_10491;
	Class474 aClass474_10492;
	boolean aBool10493;
	Class200 aClass200_10494;
	int anInt10495;
	byte aByte10496;
	boolean aBool10497;
	boolean aBool10498;
	Class528 aClass528_10499;
	boolean aBool10500;

	public void method98(Class505 class505) {
		Object object = null;
		Class282_Sub50_Sub17 class282_sub50_sub17;
		if ((null == ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491) && ((Class521_Sub1_Sub3_Sub1) this).aBool10500) {
			Class452 class452 = method16074(class505, 262144, true, 750971439);
			class282_sub50_sub17 = (Class282_Sub50_Sub17) (null != class452 ? class452.anObject5444 : null);
		} else {
			class282_sub50_sub17 = ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491;
			((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491 = null;
		}
		Class385 class385 = method11166().aClass385_3595;
		if (null != class282_sub50_sub17)
			aClass206_7970.method3427(class282_sub50_sub17, aByte7968, (int) class385.aFloat4671, (int) class385.aFloat4673, null, (byte) 53);
	}

	public void method96() {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11259();
	}

	public int method13003() {
		return (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null ? ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.YA() : 0);
	}

	public int method12995(int i) {
		return (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null ? ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.YA() : 0);
	}

	Class285 method12990(Class505 class505, int i) {
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 == null)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub3_Sub1) this).aBool10498, (byte) -59);
		((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11282(class294, aClass275_Sub5Array7965[0], 0);
		return class285;
	}

	Class452 method16074(Class505 class505, int i, boolean bool, int i_0_) {
		Class478 class478 = (((Class521_Sub1_Sub3_Sub1) this).aClass474_10492.method7891(((Class521_Sub1_Sub3_Sub1) this).anInt10495 * -1419362635, 65280));
		Class390 class390;
		Class390 class390_1_;
		if (((Class521_Sub1_Sub3_Sub1) this).aBool10493) {
			class390 = aClass206_7970.aClass390Array2614[aByte7968];
			class390_1_ = aClass206_7970.aClass390Array2607[0];
		} else {
			class390 = aClass206_7970.aClass390Array2607[aByte7968];
			if (aByte7968 < 3)
				class390_1_ = aClass206_7970.aClass390Array2607[1 + aByte7968];
			else
				class390_1_ = null;
		}
		Class385 class385 = method11166().aClass385_3595;
		return class478.method8010(class505, i, (1109376893 * Class458.aClass458_5480.anInt5481), ((Class521_Sub1_Sub3_Sub1) this).aByte10496, class390, class390_1_, (int) class385.aFloat4671, (int) class385.aFloat4672, (int) class385.aFloat4673, bool, null, 219855525);
	}

	public Class200 method12992(Class505 class505, byte i) {
		Class385 class385 = method11166().aClass385_3595;
		if (((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 == null)
			((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 = Class275_Sub2.method12505((int) class385.aFloat4671, (int) class385.aFloat4672, (int) class385.aFloat4673, method16075(class505, 0, -857380966), (byte) -72);
		return ((Class521_Sub1_Sub3_Sub1) this).aClass200_10494;
	}

	public int method91() {
		return ((Class521_Sub1_Sub3_Sub1) this).aByte10496;
	}

	void method12991(Class505 class505, int i) {
		/* empty */
	}

	public void method88(Class505 class505, int i) {
		Object object = null;
		Class282_Sub50_Sub17 class282_sub50_sub17;
		if ((null == ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491) && ((Class521_Sub1_Sub3_Sub1) this).aBool10500) {
			Class452 class452 = method16074(class505, 262144, true, 750971439);
			class282_sub50_sub17 = (Class282_Sub50_Sub17) (null != class452 ? class452.anObject5444 : null);
		} else {
			class282_sub50_sub17 = ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491;
			((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491 = null;
		}
		Class385 class385 = method11166().aClass385_3595;
		if (null != class282_sub50_sub17)
			aClass206_7970.method3427(class282_sub50_sub17, aByte7968, (int) class385.aFloat4671, (int) class385.aFloat4673, null, (byte) -76);
	}

	Class285 method13010(Class505 class505) {
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 == null)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub3_Sub1) this).aBool10498, (byte) -56);
		((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11282(class294, aClass275_Sub5Array7965[0], 0);
		return class285;
	}

	void method13013(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_2_, int i_3_, boolean bool, int i_4_) {
		if (class521_sub1 instanceof Class521_Sub1_Sub3_Sub1) {
			Class521_Sub1_Sub3_Sub1 class521_sub1_sub3_sub1_5_ = (Class521_Sub1_Sub3_Sub1) class521_sub1;
			if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 && null != ((Class521_Sub1_Sub3_Sub1) class521_sub1_sub3_sub1_5_).aClass528_10499)
				((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11269((((Class521_Sub1_Sub3_Sub1) class521_sub1_sub3_sub1_5_).aClass528_10499), i, i_2_, i_3_, bool);
		}
	}

	void method12984(int i) {
		((Class521_Sub1_Sub3_Sub1) this).aBool10497 = false;
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null)
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.KA(((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.m() & ~0x10000);
	}

	public int method84(int i) {
		return -1419362635 * ((Class521_Sub1_Sub3_Sub1) this).anInt10495;
	}

	Class528 method16075(Class505 class505, int i, int i_6_) {
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null && class505.method8452(((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.m(), i) == 0)
			return ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499;
		Class452 class452 = method16074(class505, i, false, 750971439);
		if (class452 != null)
			return (Class528) class452.anObject5443;
		return null;
	}

	public int method92(int i) {
		return ((Class521_Sub1_Sub3_Sub1) this).aByte10496;
	}

	boolean method12986(int i) {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			return !((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.u();
		return true;
	}

	boolean method13011() {
		return ((Class521_Sub1_Sub3_Sub1) this).aBool10497;
	}

	Class528 method16076(Class505 class505, int i) {
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null && class505.method8452(((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.m(), i) == 0)
			return ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499;
		Class452 class452 = method16074(class505, i, false, 750971439);
		if (class452 != null)
			return (Class528) class452.anObject5443;
		return null;
	}

	public void method93(Class505 class505) {
		Object object = null;
		Class282_Sub50_Sub17 class282_sub50_sub17;
		if ((null == ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491) && ((Class521_Sub1_Sub3_Sub1) this).aBool10500) {
			Class452 class452 = method16074(class505, 262144, true, 750971439);
			class282_sub50_sub17 = (Class282_Sub50_Sub17) (null != class452 ? class452.anObject5444 : null);
		} else {
			class282_sub50_sub17 = ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491;
			((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491 = null;
		}
		Class385 class385 = method11166().aClass385_3595;
		if (class282_sub50_sub17 != null)
			aClass206_7970.method3426(class282_sub50_sub17, aByte7968, (int) class385.aFloat4671, (int) class385.aFloat4673, null, (byte) -43);
	}

	boolean method12985(int i) {
		return ((Class521_Sub1_Sub3_Sub1) this).aBool10497;
	}

	public int method39() {
		return -1419362635 * ((Class521_Sub1_Sub3_Sub1) this).anInt10495;
	}

	public int method73() {
		return Class458.aClass458_5480.anInt5481 * 1109376893;
	}

	public int method77() {
		return Class458.aClass458_5480.anInt5481 * 1109376893;
	}

	public boolean method86(int i) {
		return ((Class521_Sub1_Sub3_Sub1) this).aBool10500;
	}

	public void method87(Class505 class505, int i) {
		Object object = null;
		Class282_Sub50_Sub17 class282_sub50_sub17;
		if ((null == ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491) && ((Class521_Sub1_Sub3_Sub1) this).aBool10500) {
			Class452 class452 = method16074(class505, 262144, true, 750971439);
			class282_sub50_sub17 = (Class282_Sub50_Sub17) (null != class452 ? class452.anObject5444 : null);
		} else {
			class282_sub50_sub17 = ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491;
			((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491 = null;
		}
		Class385 class385 = method11166().aClass385_3595;
		if (class282_sub50_sub17 != null)
			aClass206_7970.method3426(class282_sub50_sub17, aByte7968, (int) class385.aFloat4671, (int) class385.aFloat4673, null, (byte) -99);
	}

	void method13023(Class505 class505) {
		/* empty */
	}

	public void method90() {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11259();
	}

	public boolean method94() {
		return ((Class521_Sub1_Sub3_Sub1) this).aBool10500;
	}

	public boolean method95() {
		return ((Class521_Sub1_Sub3_Sub1) this).aBool10500;
	}

	public void method85(byte i) {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11259();
	}

	public void method83(Class505 class505) {
		Object object = null;
		Class282_Sub50_Sub17 class282_sub50_sub17;
		if ((null == ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491) && ((Class521_Sub1_Sub3_Sub1) this).aBool10500) {
			Class452 class452 = method16074(class505, 262144, true, 750971439);
			class282_sub50_sub17 = (Class282_Sub50_Sub17) (null != class452 ? class452.anObject5444 : null);
		} else {
			class282_sub50_sub17 = ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491;
			((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491 = null;
		}
		Class385 class385 = method11166().aClass385_3595;
		if (class282_sub50_sub17 != null)
			aClass206_7970.method3426(class282_sub50_sub17, aByte7968, (int) class385.aFloat4671, (int) class385.aFloat4673, null, (byte) -62);
	}

	public void method97(Class505 class505) {
		Object object = null;
		Class282_Sub50_Sub17 class282_sub50_sub17;
		if ((null == ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491) && ((Class521_Sub1_Sub3_Sub1) this).aBool10500) {
			Class452 class452 = method16074(class505, 262144, true, 750971439);
			class282_sub50_sub17 = (Class282_Sub50_Sub17) (null != class452 ? class452.anObject5444 : null);
		} else {
			class282_sub50_sub17 = ((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491;
			((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491 = null;
		}
		Class385 class385 = method11166().aClass385_3595;
		if (null != class282_sub50_sub17)
			aClass206_7970.method3427(class282_sub50_sub17, aByte7968, (int) class385.aFloat4671, (int) class385.aFloat4673, null, (byte) 71);
	}

	public int method76() {
		return -1419362635 * ((Class521_Sub1_Sub3_Sub1) this).anInt10495;
	}

	boolean method12999() {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			return !((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.u();
		return true;
	}

	void method13016(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_7_, int i_8_, boolean bool) {
		if (class521_sub1 instanceof Class521_Sub1_Sub3_Sub1) {
			Class521_Sub1_Sub3_Sub1 class521_sub1_sub3_sub1_9_ = (Class521_Sub1_Sub3_Sub1) class521_sub1;
			if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 && null != ((Class521_Sub1_Sub3_Sub1) class521_sub1_sub3_sub1_9_).aClass528_10499)
				((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11269((((Class521_Sub1_Sub3_Sub1) class521_sub1_sub3_sub1_9_).aClass528_10499), i, i_7_, i_8_, bool);
		}
	}

	boolean method13001() {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			return ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.i();
		return false;
	}

	boolean method13002() {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			return ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.i();
		return false;
	}

	boolean method13000() {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			return ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.i();
		return false;
	}

	public int method13017() {
		return (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null ? ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.YA() : 0);
	}

	public int method13005() {
		return (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null ? ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.YA() : 0);
	}

	public int method13006() {
		return (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null ? ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.YA() : 0);
	}

	Class285 method13009(Class505 class505) {
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 == null)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub3_Sub1) this).aBool10498, (byte) -109);
		((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11282(class294, aClass275_Sub5Array7965[0], 0);
		return class285;
	}

	boolean method12987(int i) {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			return ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.i();
		return false;
	}

	Class285 method12989(Class505 class505) {
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 == null)
			return null;
		Class294 class294 = method11168();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub3_Sub1) this).aBool10498, (byte) -30);
		((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11282(class294, aClass275_Sub5Array7965[0], 0);
		return class285;
	}

	void method13012(Class505 class505) {
		/* empty */
	}

	public Class521_Sub1_Sub3_Sub1(Class206 class206, Class505 class505, Class474 class474, Class478 class478, int i, int i_10_, int i_11_, int i_12_, int i_13_, boolean bool, int i_14_, boolean bool_15_) {
		super(class206, i_11_, i_12_, i_13_, i, i_10_, class478.anInt5704 * 1338534295);
		((Class521_Sub1_Sub3_Sub1) this).aClass474_10492 = class474;
		((Class521_Sub1_Sub3_Sub1) this).anInt10495 = class478.anInt5633 * 386955635;
		((Class521_Sub1_Sub3_Sub1) this).aBool10493 = bool;
		((Class521_Sub1_Sub3_Sub1) this).aByte10496 = (byte) i_14_;
		((Class521_Sub1_Sub3_Sub1) this).aBool10498 = class478.anInt5652 * -348507379 != 0 && !bool;
		((Class521_Sub1_Sub3_Sub1) this).aBool10497 = bool_15_;
		((Class521_Sub1_Sub3_Sub1) this).aBool10500 = (class505.method8402() && class478.aBool5703 && !((Class521_Sub1_Sub3_Sub1) this).aBool10493 && Class393.aClass282_Sub54_4783.aClass468_Sub2_8205.method12624((byte) -122) != 0);
		int i_16_ = 2048;
		if (((Class521_Sub1_Sub3_Sub1) this).aBool10497)
			i_16_ |= 0x10000;
		if (class478.aBool5711)
			i_16_ |= 0x80000;
		Class452 class452 = method16074(class505, i_16_, ((Class521_Sub1_Sub3_Sub1) this).aBool10500, 750971439);
		if (null != class452) {
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 = (Class528) class452.anObject5443;
			((Class521_Sub1_Sub3_Sub1) this).aClass282_Sub50_Sub17_10491 = (Class282_Sub50_Sub17) class452.anObject5444;
			if (((Class521_Sub1_Sub3_Sub1) this).aBool10497 || class478.aBool5711) {
				((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 = ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11289((byte) 0, i_16_, false);
				if (class478.aBool5711) {
					Class341 class341 = client.aClass257_7353.method4426(1159043919);
					((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.PA(class341.anInt3992 * 1367691291, 1646020803 * class341.anInt3993, class341.anInt3994 * 945117807, 2100466695 * class341.anInt3995);
				}
			}
		}
		method13008(1, (byte) -30);
	}

	boolean method13026() {
		return ((Class521_Sub1_Sub3_Sub1) this).aBool10497;
	}

	boolean method13020(Class505 class505, int i, int i_17_) {
		Class528 class528 = method16075(class505, 131072, -338274423);
		if (null != class528) {
			Class294 class294 = method11168();
			return class528.method11270(i, i_17_, class294, false, 0);
		}
		return false;
	}

	public void method38() {
		if (null != ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499)
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.method11259();
	}

	public Class200 method13019(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		if (((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 == null)
			((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 = Class275_Sub2.method12505((int) class385.aFloat4671, (int) class385.aFloat4672, (int) class385.aFloat4673, method16075(class505, 0, 2111828058), (byte) 15);
		return ((Class521_Sub1_Sub3_Sub1) this).aClass200_10494;
	}

	public Class200 method13018(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		if (((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 == null)
			((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 = Class275_Sub2.method12505((int) class385.aFloat4671, (int) class385.aFloat4672, (int) class385.aFloat4673, method16075(class505, 0, -1243491544), (byte) 55);
		return ((Class521_Sub1_Sub3_Sub1) this).aClass200_10494;
	}

	public Class200 method12993(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		if (((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 == null)
			((Class521_Sub1_Sub3_Sub1) this).aClass200_10494 = Class275_Sub2.method12505((int) class385.aFloat4671, (int) class385.aFloat4672, (int) class385.aFloat4673, method16075(class505, 0, 1861667283), (byte) 4);
		return ((Class521_Sub1_Sub3_Sub1) this).aClass200_10494;
	}

	public int method89(int i) {
		return Class458.aClass458_5480.anInt5481 * 1109376893;
	}

	void method13021() {
		((Class521_Sub1_Sub3_Sub1) this).aBool10497 = false;
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null)
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.KA(((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.m() & ~0x10000);
	}

	void method13015() {
		((Class521_Sub1_Sub3_Sub1) this).aBool10497 = false;
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null)
			((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.KA(((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.m() & ~0x10000);
	}

	boolean method12983(Class505 class505, int i, int i_18_, int i_19_) {
		Class528 class528 = method16075(class505, 131072, 1304391023);
		if (null != class528) {
			Class294 class294 = method11168();
			return class528.method11270(i, i_18_, class294, false, 0);
		}
		return false;
	}

	Class528 method16077(Class505 class505, int i) {
		if (((Class521_Sub1_Sub3_Sub1) this).aClass528_10499 != null && class505.method8452(((Class521_Sub1_Sub3_Sub1) this).aClass528_10499.m(), i) == 0)
			return ((Class521_Sub1_Sub3_Sub1) this).aClass528_10499;
		Class452 class452 = method16074(class505, i, false, 750971439);
		if (class452 != null)
			return (Class528) class452.anObject5443;
		return null;
	}

	public static final void method16078(Class528 class528, Class456 class456, Class456 class456_20_, int i) {
		if (class456.method7562(1986884672) && class456_20_.method7562(1815488454)) {
			Class518 class518 = ((Class456) class456).aClass518_5453;
			Class518 class518_21_ = ((Class456) class456_20_).aClass518_5453;
			class528.method11264((((Class462) ((Class456) class456).aClass462_5464).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) ((Class456) class456).aClass462_5464).anInt5542, (((Class462) ((Class456) class456).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) class456).aClass462_5464).anInt5547 * -775231561), ((Class456) class456).anInt5457 * -706748429, (class518.anIntArray5912[643220577 * ((Class456) class456).anInt5460]), (((Class462) ((Class456) class456_20_).aClass462_5464).aClass282_Sub50_Sub13_5545), -104209121 * ((Class462) (((Class456) class456_20_).aClass462_5464)).anInt5542, (((Class462) ((Class456) class456_20_).aClass462_5464).aClass282_Sub50_Sub13_5546), (((Class462) ((Class456) class456_20_).aClass462_5464).anInt5547) * -775231561, ((Class456) class456_20_).anInt5457 * -706748429, (class518_21_.anIntArray5912[643220577 * ((Class456) class456_20_).anInt5460]), class518.aBoolArray5915, class518.aBool5923 | class518_21_.aBool5923);
		}
	}

	static final void method16079(Class527 class527, short i) {
		((Class527) class527).anInt7012 -= 283782002;
		Class12.method486((((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]), (((Class527) class527).anIntArray6999[1 + ((Class527) class527).anInt7012 * 1942118537]), 0, 596260679);
	}

	static Class282_Sub53_Sub2 method16080(int i, int i_22_, int i_23_, long l, int i_24_, int i_25_) {
		synchronized (Class282_Sub53_Sub2.aClass282_Sub53_Sub2Array9633) {
			Class282_Sub53_Sub2 class282_sub53_sub2;
			if (0 == 187656911 * Class279.anInt3370)
				class282_sub53_sub2 = new Class282_Sub53_Sub2();
			else
				class282_sub53_sub2 = (Class282_Sub53_Sub2.aClass282_Sub53_Sub2Array9633[(Class279.anInt3370 -= 1338465327) * 187656911]);
			((Class282_Sub53_Sub2) class282_sub53_sub2).anInt9631 = i * 516361889;
			((Class282_Sub53_Sub2) class282_sub53_sub2).anInt9632 = i_22_ * 1042258953;
			((Class282_Sub53_Sub2) class282_sub53_sub2).anInt9629 = -1656963315 * i_23_;
			((Class282_Sub53_Sub2) class282_sub53_sub2).aLong9634 = l * 9069369255196266531L;
			((Class282_Sub53_Sub2) class282_sub53_sub2).anInt9635 = i_24_ * -824774269;
			Class282_Sub53_Sub2 class282_sub53_sub2_26_ = class282_sub53_sub2;
			return class282_sub53_sub2_26_;
		}
	}

	static boolean method16081(int i) {
		return Class179.aBool2229;
	}
}
