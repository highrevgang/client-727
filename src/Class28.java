/* Class28 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class28 implements Interface2 {
	Class365 aClass365_349;
	Class30 aClass30_350;
	static long aLong351;

	Class28(Class30 class30, Class365 class365) {
		((Class28) this).aClass365_349 = class365;
		((Class28) this).aClass30_350 = class30;
	}

	public boolean method10(int i) {
		return ((Class28) this).aClass30_350.method792(998475180);
	}

	public boolean method17() {
		return ((Class28) this).aClass30_350.method792(-2107971523);
	}

	public void method11(boolean bool) {
		Class105 class105 = ((Class28) this).aClass30_350.method791((((Class28) this).aClass365_349.anInt4229) * 778888219, (byte) -49);
		if (null != class105) {
			int i = ((((Class28) this).aClass365_349.aClass356_4225.method6221(((Class28) this).aClass365_349.anInt4227 * -547232823, 150480619 * client.anInt7439, 1460019555)) + ((Class28) this).aClass365_349.anInt4221 * 1878539677);
			int i_0_ = ((((Class28) this).aClass365_349.aClass353_4226.method6198(1416311209 * ((Class28) this).aClass365_349.anInt4228, 1176039023 * client.anInt7312, 1018239308)) + ((Class28) this).aClass365_349.anInt4223 * -1081607939);
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8430(i, i_0_, -547232823 * ((Class28) this).aClass365_349.anInt4227, ((Class28) this).aClass365_349.anInt4228 * 1416311209, ((Class28) this).aClass365_349.anInt4220 * -1214034363, 0);
			i_0_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1067, i, i_0_, 5, -2023522211) * 12;
			i_0_ += 8;
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8433(i, i_0_, (((Class28) this).aClass365_349.anInt4227 * -547232823 + i - 1), i_0_, -1214034363 * ((Class28) this).aClass365_349.anInt4220, 0);
			i_0_ = ++i_0_ + method770(Class285.aClass8_3394, ((Class105) class105).aString1066, i, i_0_, 5, -2109162483) * 12;
			i_0_ += 5;
			i_0_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1068, i, i_0_, 5, 282496799) * 12;
		}
	}

	public void method20(boolean bool, int i) {
		Class105 class105 = ((Class28) this).aClass30_350.method791((((Class28) this).aClass365_349.anInt4229) * 778888219, (byte) -61);
		if (null != class105) {
			int i_1_ = ((((Class28) this).aClass365_349.aClass356_4225.method6221(((Class28) this).aClass365_349.anInt4227 * -547232823, 150480619 * client.anInt7439, 1591676659)) + ((Class28) this).aClass365_349.anInt4221 * 1878539677);
			int i_2_ = ((((Class28) this).aClass365_349.aClass353_4226.method6198(1416311209 * ((Class28) this).aClass365_349.anInt4228, 1176039023 * client.anInt7312, 1855171415)) + ((Class28) this).aClass365_349.anInt4223 * -1081607939);
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8430(i_1_, i_2_, -547232823 * ((Class28) this).aClass365_349.anInt4227, ((Class28) this).aClass365_349.anInt4228 * 1416311209, ((Class28) this).aClass365_349.anInt4220 * -1214034363, 0);
			i_2_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1067, i_1_, i_2_, 5, -945681217) * 12;
			i_2_ += 8;
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8433(i_1_, i_2_, (((Class28) this).aClass365_349.anInt4227 * -547232823 + i_1_ - 1), i_2_, -1214034363 * ((Class28) this).aClass365_349.anInt4220, 0);
			i_2_ = ++i_2_ + method770(Class285.aClass8_3394, ((Class105) class105).aString1066, i_1_, i_2_, 5, -2057271844) * 12;
			i_2_ += 5;
			i_2_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1068, i_1_, i_2_, 5, 34451052) * 12;
		}
	}

	public void method12(boolean bool) {
		Class105 class105 = ((Class28) this).aClass30_350.method791((((Class28) this).aClass365_349.anInt4229) * 778888219, (byte) -33);
		if (null != class105) {
			int i = ((((Class28) this).aClass365_349.aClass356_4225.method6221(((Class28) this).aClass365_349.anInt4227 * -547232823, 150480619 * client.anInt7439, 1607557191)) + ((Class28) this).aClass365_349.anInt4221 * 1878539677);
			int i_3_ = ((((Class28) this).aClass365_349.aClass353_4226.method6198(1416311209 * ((Class28) this).aClass365_349.anInt4228, 1176039023 * client.anInt7312, 1779448896)) + ((Class28) this).aClass365_349.anInt4223 * -1081607939);
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8430(i, i_3_, -547232823 * ((Class28) this).aClass365_349.anInt4227, ((Class28) this).aClass365_349.anInt4228 * 1416311209, ((Class28) this).aClass365_349.anInt4220 * -1214034363, 0);
			i_3_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1067, i, i_3_, 5, -1904326509) * 12;
			i_3_ += 8;
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8433(i, i_3_, (((Class28) this).aClass365_349.anInt4227 * -547232823 + i - 1), i_3_, -1214034363 * ((Class28) this).aClass365_349.anInt4220, 0);
			i_3_ = ++i_3_ + method770(Class285.aClass8_3394, ((Class105) class105).aString1066, i, i_3_, 5, 141561560) * 12;
			i_3_ += 5;
			i_3_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1068, i, i_3_, 5, -279930969) * 12;
		}
	}

	int method770(Class8 class8, String string, int i, int i_4_, int i_5_, int i_6_) {
		return (class8.method378(string, i_5_ + i, i_4_ + i_5_, (((Class28) this).aClass365_349.anInt4227 * -547232823 - i_5_ * 2), (1416311209 * ((Class28) this).aClass365_349.anInt4228 - 2 * i_5_), ((Class28) this).aClass365_349.anInt4224 * 1984706887, 1850405797 * ((Class28) this).aClass365_349.anInt4222, 0, 0, 0, null, null, null, 0, 0, (byte) 76));
	}

	public void method14(boolean bool) {
		Class105 class105 = ((Class28) this).aClass30_350.method791((((Class28) this).aClass365_349.anInt4229) * 778888219, (byte) -70);
		if (null != class105) {
			int i = ((((Class28) this).aClass365_349.aClass356_4225.method6221(((Class28) this).aClass365_349.anInt4227 * -547232823, 150480619 * client.anInt7439, 1779044306)) + ((Class28) this).aClass365_349.anInt4221 * 1878539677);
			int i_7_ = ((((Class28) this).aClass365_349.aClass353_4226.method6198(1416311209 * ((Class28) this).aClass365_349.anInt4228, 1176039023 * client.anInt7312, 1414644446)) + ((Class28) this).aClass365_349.anInt4223 * -1081607939);
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8430(i, i_7_, -547232823 * ((Class28) this).aClass365_349.anInt4227, ((Class28) this).aClass365_349.anInt4228 * 1416311209, ((Class28) this).aClass365_349.anInt4220 * -1214034363, 0);
			i_7_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1067, i, i_7_, 5, 1283831250) * 12;
			i_7_ += 8;
			if (((Class28) this).aClass365_349.aBool4230)
				Class316.aClass505_3680.method8433(i, i_7_, (((Class28) this).aClass365_349.anInt4227 * -547232823 + i - 1), i_7_, -1214034363 * ((Class28) this).aClass365_349.anInt4220, 0);
			i_7_ = ++i_7_ + method770(Class285.aClass8_3394, ((Class105) class105).aString1066, i, i_7_, 5, -318852891) * 12;
			i_7_ += 5;
			i_7_ += method770(Class285.aClass8_3394, ((Class105) class105).aString1068, i, i_7_, 5, -633571226) * 12;
		}
	}

	public void method22(int i) {
		/* empty */
	}

	public void method16() {
		/* empty */
	}

	public void method23() {
		/* empty */
	}

	public void method18() {
		/* empty */
	}

	public void method19() {
		/* empty */
	}

	public boolean method13() {
		return ((Class28) this).aClass30_350.method792(-362908592);
	}

	public boolean method9() {
		return ((Class28) this).aClass30_350.method792(1107978088);
	}

	public boolean method21() {
		return ((Class28) this).aClass30_350.method792(-1024774196);
	}

	public void method15() {
		/* empty */
	}

	int method771(Class8 class8, String string, int i, int i_8_, int i_9_) {
		return (class8.method378(string, i_9_ + i, i_8_ + i_9_, (((Class28) this).aClass365_349.anInt4227 * -547232823 - i_9_ * 2), (1416311209 * ((Class28) this).aClass365_349.anInt4228 - 2 * i_9_), ((Class28) this).aClass365_349.anInt4224 * 1984706887, 1850405797 * ((Class28) this).aClass365_349.anInt4222, 0, 0, 0, null, null, null, 0, 0, (byte) 18));
	}

	public static void method772(byte i) {
		if (null != Class328.aClass306_3771) {
			if (i <= -1) {
				/* empty */
			}
			Class328.aClass306_3771.method5437(-138348763);
		}
		if (Class377.aThread4520 != null) {
			if (i <= -1) {
				/* empty */
			}
			for (;;) {
				try {
					Class377.aThread4520.join();
					break;
				} catch (InterruptedException interruptedexception) {
					/* empty */
				}
			}
		}
	}

	static final void method773(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_10_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		int i_11_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 + 1]);
		if (null != Class84.myPlayer.aClass238_10558)
			Class84.myPlayer.aClass238_10558.method3995(i_10_, i_11_, Class119.aClass426_1463, (byte) 57);
	}

	static final void method774(Class118 class118, Class98 class98, Class527 class527, int i) {
		class118.aBool1351 = ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1);
		Class109.method1858(class118, (byte) 44);
	}

	static final void method775(Class527 class527, int i) {
		Class438.method7333(false, 2001834911);
	}

	static final void method776(Class118 class118, byte i) {
		if (1422794759 * Class118.anInt1266 == 1449989045 * class118.anInt1290) {
			if (Class84.myPlayer.aString10546 == null) {
				class118.anInt1330 = 0;
				class118.anInt1339 = 0;
			} else {
				class118.anInt1385 = -188173078;
				class118.anInt1336 = ((int) (Math.sin((double) (-1809259861 * client.anInt7174) / 40.0) * 256.0) & 0x7ff) * -140036259;
				class118.anInt1329 = -1346216911;
				class118.anInt1330 = client.anInt7315 * -1183558903;
				class118.anInt1339 = Class272.method4840((Class84.myPlayer.aString10546), (byte) 15) * 207030057;
				Class456_Sub3 class456_sub3 = (Class84.myPlayer.aClass456_Sub3_10337);
				if (class456_sub3 != null) {
					if (null == class118.aClass456_1437)
						class118.aClass456_1437 = new Class456_Sub1();
					class118.anInt1321 = class456_sub3.method7597(-486545120) * 388683695;
					class118.aClass456_1437.method7563(class456_sub3, -887916681);
				} else
					class118.aClass456_1437 = null;
			}
		}
	}

	static void method777(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 2] = (Class409.aClass242_4922.method4156((((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 2]), -1396181317).anIntArray2968[(((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 1])]);
		((Class527) class527).anInt7012 -= 141891001;
	}

	static void method778(int i, int i_12_, int i_13_, int i_14_, byte i_15_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(10, (long) i);
		class282_sub50_sub12.method14995(866600532);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_12_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_13_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = -1932168275 * i_14_;
	}

	static final void method779(Class527 class527, byte i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		int i_16_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		i_16_--;
		if (class118.aStringArray1352 == null || i_16_ >= class118.aStringArray1352.length || class118.aStringArray1352[i_16_] == null)
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = "";
		else
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = class118.aStringArray1352[i_16_];
	}
}
