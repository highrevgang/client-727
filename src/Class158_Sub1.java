/* Class158_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class158_Sub1 extends Class158 {
	protected static final int anInt8506 = 4;
	public static Class3 aClass3_8507;

	public abstract void method13757(Interface8 interface8);

	public abstract void method13758(Interface8 interface8);

	public abstract void method13759(int i, Interface9 interface9);

	Class158_Sub1() {
		/* empty */
	}

	public abstract void method13760(int i, Interface9 interface9);

	public abstract void method13761(int i, Interface9 interface9);

	public abstract void method13762(int i, Interface9 interface9);

	public abstract void method13763(Interface8 interface8);

	public abstract boolean method13764();

	public abstract void method13765(Interface8 interface8);

	public abstract boolean method13766();

	static Class102[] method13767(int i) {
		return (new Class102[] { Class102.aClass102_1055, Class102.aClass102_1036, Class102.aClass102_1025, Class102.aClass102_1034, Class102.aClass102_1035, Class102.aClass102_1024, Class102.aClass102_1027, Class102.aClass102_1041, Class102.aClass102_1017, Class102.aClass102_1052, Class102.aClass102_1050, Class102.aClass102_1019, Class102.aClass102_1018, Class102.aClass102_1044, Class102.aClass102_1038, Class102.aClass102_1042, Class102.aClass102_1048, Class102.aClass102_1047, Class102.aClass102_1046, Class102.aClass102_1032, Class102.aClass102_1045, Class102.aClass102_1031, Class102.aClass102_1026, Class102.aClass102_1028, Class102.aClass102_1037, Class102.aClass102_1023, Class102.aClass102_1033, Class102.aClass102_1040, Class102.aClass102_1030, Class102.aClass102_1020, Class102.aClass102_1029, Class102.aClass102_1022, Class102.aClass102_1043, Class102.aClass102_1053, Class102.aClass102_1021, Class102.aClass102_1051, Class102.aClass102_1049 });
	}

	public static String method13768(Class282_Sub50_Sub7 class282_sub50_sub7, int i) {
		if (Class20.aBool161 || null == class282_sub50_sub7)
			return "";
		return ((Class282_Sub50_Sub7) class282_sub50_sub7).aString9576;
	}
}
