/* Class47 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class47 {
	protected Class505_Sub2 aClass505_Sub2_439;
	public Class384 aClass384_440;
	public Interface6 anInterface6_441;
	public Class384 aClass384_442 = new Class384();
	public Interface6 anInterface6_443;
	public Class384 aClass384_444;
	public Interface4 anInterface4_445;
	public int anInt446;
	public Class70 aClass70_447;

	Class47(Class505_Sub2 class505_sub2) {
		aClass384_440 = new Class384();
		aClass384_444 = new Class384();
		aClass505_Sub2_439 = class505_sub2;
	}

	public abstract void method936();

	public abstract void method937(int i, int i_0_);

	public abstract void method938();

	public abstract void method939(int i, int i_1_);

	public abstract void method940(int i, int i_2_);

	public abstract void method941();

	public abstract void method942();
}
