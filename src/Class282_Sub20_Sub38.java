/* Class282_Sub20_Sub38 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub20_Sub38 extends Class282_Sub20 {
	int method15425(int i, int i_0_) {
		int i_1_ = i + i_0_ * 57;
		i_1_ = i_1_ << 1 ^ i_1_;
		return 4096 - (1376312589 + (789221 + 15731 * (i_1_ * i_1_)) * i_1_ & 0x7fffffff) / 262144;
	}

	int[] method12319(int i, int i_2_) {
		int[] is = aClass320_7667.method5721(i, -1471197134);
		if (aClass320_7667.aBool3722) {
			int i_3_ = Class316.anIntArray3668[i];
			for (int i_4_ = 0; i_4_ < -402153223 * Class316.anInt3670; i_4_++)
				is[i_4_] = method15428(Class316.anIntArray3672[i_4_], i_3_, 847752759) % 4096;
		}
		return is;
	}

	int[] method12325(int i) {
		int[] is = aClass320_7667.method5721(i, -1484908126);
		if (aClass320_7667.aBool3722) {
			int i_5_ = Class316.anIntArray3668[i];
			for (int i_6_ = 0; i_6_ < -402153223 * Class316.anInt3670; i_6_++)
				is[i_6_] = method15428(Class316.anIntArray3672[i_6_], i_5_, -238121916) % 4096;
		}
		return is;
	}

	int method15426(int i, int i_7_) {
		int i_8_ = i + i_7_ * 57;
		i_8_ = i_8_ << 1 ^ i_8_;
		return 4096 - (1376312589 + (789221 + 15731 * (i_8_ * i_8_)) * i_8_ & 0x7fffffff) / 262144;
	}

	int[] method12336(int i) {
		int[] is = aClass320_7667.method5721(i, -687516705);
		if (aClass320_7667.aBool3722) {
			int i_9_ = Class316.anIntArray3668[i];
			for (int i_10_ = 0; i_10_ < -402153223 * Class316.anInt3670; i_10_++)
				is[i_10_] = method15428(Class316.anIntArray3672[i_10_], i_9_, 1887628433) % 4096;
		}
		return is;
	}

	int[] method12327(int i) {
		int[] is = aClass320_7667.method5721(i, 543574588);
		if (aClass320_7667.aBool3722) {
			int i_11_ = Class316.anIntArray3668[i];
			for (int i_12_ = 0; i_12_ < -402153223 * Class316.anInt3670; i_12_++)
				is[i_12_] = method15428(Class316.anIntArray3672[i_12_], i_11_, 1602249240) % 4096;
		}
		return is;
	}

	int method15427(int i, int i_13_) {
		int i_14_ = i + i_13_ * 57;
		i_14_ = i_14_ << 1 ^ i_14_;
		return 4096 - (1376312589 + (789221 + 15731 * (i_14_ * i_14_)) * i_14_ & 0x7fffffff) / 262144;
	}

	int method15428(int i, int i_15_, int i_16_) {
		int i_17_ = i + i_15_ * 57;
		i_17_ = i_17_ << 1 ^ i_17_;
		return 4096 - (1376312589 + (789221 + 15731 * (i_17_ * i_17_)) * i_17_ & 0x7fffffff) / 262144;
	}

	public Class282_Sub20_Sub38() {
		super(0, true);
	}

	int method15429(int i, int i_18_) {
		int i_19_ = i + i_18_ * 57;
		i_19_ = i_19_ << 1 ^ i_19_;
		return 4096 - (1376312589 + (789221 + 15731 * (i_19_ * i_19_)) * i_19_ & 0x7fffffff) / 262144;
	}

	public static void method15430(int i, int i_20_) {
		if (Class388.method6693(1937106134)) {
			if (1051306693 * Class9.anInt76 != i)
				Class9.aLong77 = -2087908126908168589L;
			Class9.anInt76 = i * 1481524237;
			client.aClass184_7218.method3051((byte) -50);
			Class365.method6298(14, 1309842268);
		}
	}
}
