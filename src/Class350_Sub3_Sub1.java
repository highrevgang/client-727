/* Class350_Sub3_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class350_Sub3_Sub1 extends Class350_Sub3 {
	public int anInt10160;

	Class350_Sub3_Sub1(Class356 class356, Class353 class353, int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_) {
		super(class356, class353, i, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_, i_7_, i_8_, i_9_, i_10_, i_11_);
		anInt10160 = i_12_ * -1886107255;
	}

	public Class60 method24(int i) {
		return Class60.aClass60_610;
	}

	public Class60 method25() {
		return Class60.aClass60_610;
	}

	public static Class350 method15556(RsByteBuffer class282_sub35) {
		Class350_Sub3 class350_sub3 = (Class350_Sub3) Class163.method2843(class282_sub35, 1202400093);
		int i = class282_sub35.method13081(2132148202);
		return new Class350_Sub3_Sub1(class350_sub3.aClass356_4094, class350_sub3.aClass353_4087, -1967081549 * class350_sub3.anInt4090, -1196256967 * class350_sub3.anInt4089, class350_sub3.anInt4093 * 329542577, 323608093 * class350_sub3.anInt4088, class350_sub3.anInt4092 * -1921815535, 985690519 * class350_sub3.anInt4086, -771513131 * class350_sub3.anInt4091, -1642545265 * class350_sub3.anInt7844, class350_sub3.anInt7840 * -733628077, -821287133 * class350_sub3.anInt7842, class350_sub3.anInt7843 * -2050274227, class350_sub3.anInt7841 * -485291629, class350_sub3.anInt7845 * 657783501, i);
	}

	public static Class350 method15557(RsByteBuffer class282_sub35) {
		Class350_Sub3 class350_sub3 = (Class350_Sub3) Class163.method2843(class282_sub35, 1986075333);
		int i = class282_sub35.method13081(1694865347);
		return new Class350_Sub3_Sub1(class350_sub3.aClass356_4094, class350_sub3.aClass353_4087, -1967081549 * class350_sub3.anInt4090, -1196256967 * class350_sub3.anInt4089, class350_sub3.anInt4093 * 329542577, 323608093 * class350_sub3.anInt4088, class350_sub3.anInt4092 * -1921815535, 985690519 * class350_sub3.anInt4086, -771513131 * class350_sub3.anInt4091, -1642545265 * class350_sub3.anInt7844, class350_sub3.anInt7840 * -733628077, -821287133 * class350_sub3.anInt7842, class350_sub3.anInt7843 * -2050274227, class350_sub3.anInt7841 * -485291629, class350_sub3.anInt7845 * 657783501, i);
	}

	public static Class356[] method15558(int i) {
		return (new Class356[] { Class356.aClass356_4117, Class356.aClass356_4118, Class356.aClass356_4119 });
	}

	static final void method15559(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = class118.anInt1277 * 442725395;
	}

	static final void method15560(long l) {
		try {
			Thread.sleep(l);
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
	}
}
