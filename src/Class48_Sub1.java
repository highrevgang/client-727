/* Class48_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class48_Sub1 extends Class48 {
	void method14514() {
		aClass505_Sub2_449.method14035(anInterface6_452);
		aClass505_Sub2_449.method13973().method6562(aClass384_454);
		aClass505_Sub2_449.method13974(Class37.aClass37_388);
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method957(Class384 class384) {
		aClass505_Sub2_449.method13892(class384, aClass505_Sub2_449.aClass384_8740, aClass505_Sub2_449.aClass384_8727);
	}

	public void method946(boolean bool) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		method14515();
	}

	void method14515() {
		aClass505_Sub2_449.method14035(anInterface6_452);
		aClass505_Sub2_449.method13973().method6562(aClass384_454);
		aClass505_Sub2_449.method13974(Class37.aClass37_388);
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method954(boolean bool) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		method14515();
	}

	public void method950() {
		method965(0);
	}

	public void method949(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_687);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_678);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method951(Class384 class384) {
		aClass505_Sub2_449.method13892(class384, aClass505_Sub2_449.aClass384_8740, aClass505_Sub2_449.aClass384_8727);
	}

	public void method952(Class384 class384) {
		aClass505_Sub2_449.method13892(class384, aClass505_Sub2_449.aClass384_8740, aClass505_Sub2_449.aClass384_8727);
	}

	public void method948(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_687);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_678);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method953(Class384 class384) {
		aClass505_Sub2_449.method13892(class384, aClass505_Sub2_449.aClass384_8740, aClass505_Sub2_449.aClass384_8727);
	}

	void method14516() {
		aClass505_Sub2_449.method14035(anInterface6_452);
		aClass505_Sub2_449.method13973().method6562(aClass384_454);
		aClass505_Sub2_449.method13974(Class37.aClass37_388);
		aClass505_Sub2_449.method14002(Class352.aClass352_4098, anInt467, anInt468, anInt469, anInt470);
	}

	public void method965(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public Class48_Sub1(Class505_Sub2 class505_sub2) {
		super(class505_sub2);
	}

	public void method956(boolean bool) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		method14515();
	}

	public void method963(Class384 class384) {
		aClass505_Sub2_449.method13892(class384, aClass505_Sub2_449.aClass384_8740, aClass505_Sub2_449.aClass384_8727);
	}

	public void method960(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method955(boolean bool) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		method14515();
	}

	public void method945(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method959(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_683);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method967(Class384 class384) {
		aClass505_Sub2_449.method13892(class384, aClass505_Sub2_449.aClass384_8740, aClass505_Sub2_449.aClass384_8727);
	}

	public void method961(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_687);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_678);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method962(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_687);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_678);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method947(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_687);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_678);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method964(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_687);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_678);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method958(int i) {
		aClass505_Sub2_449.method14163(0);
		aClass505_Sub2_449.method13966(Class68.aClass68_683, Class68.aClass68_687);
		aClass505_Sub2_449.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_449.method13969(0, Class67.aClass67_678);
		aClass505_Sub2_449.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_449.method13969(1, Class67.aClass67_678);
		method14515();
	}

	public void method966() {
		method965(0);
	}

	public void method943() {
		method965(0);
	}
}
