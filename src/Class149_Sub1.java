/* Class149_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class149_Sub1 extends Class149 {
	int anInt9273;
	int anInt9274;
	int anInt9275;
	int anInt9276;
	static final int anInt9277 = 3;

	void method2559(int i, int i_0_) {
		/* empty */
	}

	void method2561(int i, int i_1_, int i_2_) {
		/* empty */
	}

	void method2556(int i, int i_3_, int i_4_) {
		int i_5_ = ((Class149_Sub1) this).anInt9274 * 1801773735 * i >> 12;
		int i_6_ = ((Class149_Sub1) this).anInt9276 * 731213113 * i >> 12;
		int i_7_ = i_3_ * (-1269720619 * ((Class149_Sub1) this).anInt9273) >> 12;
		int i_8_ = i_3_ * (1736404795 * ((Class149_Sub1) this).anInt9275) >> 12;
		method14584(i_5_, i_7_, i_6_, i_8_, ((Class149_Sub1) this).anInt1743 * 1340859839, (short) -27335);
	}

	void method2560(int i, int i_9_) {
		int i_10_ = ((Class149_Sub1) this).anInt9274 * 1801773735 * i >> 12;
		int i_11_ = ((Class149_Sub1) this).anInt9276 * 731213113 * i >> 12;
		int i_12_ = i_9_ * (-1269720619 * ((Class149_Sub1) this).anInt9273) >> 12;
		int i_13_ = i_9_ * (1736404795 * ((Class149_Sub1) this).anInt9275) >> 12;
		method14584(i_10_, i_12_, i_11_, i_13_, ((Class149_Sub1) this).anInt1743 * 1340859839, (short) -5794);
	}

	void method2555(int i, int i_14_) {
		/* empty */
	}

	void method2557(int i, int i_15_, byte i_16_) {
		int i_17_ = ((Class149_Sub1) this).anInt9274 * 1801773735 * i >> 12;
		int i_18_ = ((Class149_Sub1) this).anInt9276 * 731213113 * i >> 12;
		int i_19_ = -1269720619 * ((Class149_Sub1) this).anInt9273 * i_15_ >> 12;
		int i_20_ = i_15_ * (1736404795 * ((Class149_Sub1) this).anInt9275) >> 12;
		Class299.method5316(i_17_, i_19_, i_18_, i_20_, ((Class149_Sub1) this).anInt1743 * 1340859839, -1525176857 * ((Class149_Sub1) this).anInt1741, -1125689331 * ((Class149_Sub1) this).anInt1742, (short) 15863);
	}

	void method2558(int i, int i_21_) {
		int i_22_ = ((Class149_Sub1) this).anInt9274 * 1801773735 * i >> 12;
		int i_23_ = ((Class149_Sub1) this).anInt9276 * 731213113 * i >> 12;
		int i_24_ = i_21_ * (-1269720619 * ((Class149_Sub1) this).anInt9273) >> 12;
		int i_25_ = i_21_ * (1736404795 * ((Class149_Sub1) this).anInt9275) >> 12;
		method14584(i_22_, i_24_, i_23_, i_25_, ((Class149_Sub1) this).anInt1743 * 1340859839, (short) -3087);
	}

	Class149_Sub1(int i, int i_26_, int i_27_, int i_28_, int i_29_, int i_30_, int i_31_) {
		super(i_29_, i_30_, i_31_);
		((Class149_Sub1) this).anInt9274 = i * 177644823;
		((Class149_Sub1) this).anInt9273 = 1360975741 * i_26_;
		((Class149_Sub1) this).anInt9276 = 265384201 * i_27_;
		((Class149_Sub1) this).anInt9275 = i_28_ * -1651797517;
	}

	static Class149_Sub1 method14580(RsByteBuffer class282_sub35) {
		return new Class149_Sub1(class282_sub35.method13081(1786463170), class282_sub35.method13081(2044491815), class282_sub35.method13081(2103255888), class282_sub35.method13081(1961668029), class282_sub35.method13082((short) 31731), class282_sub35.method13082((short) 7481), class282_sub35.readUnsignedByte());
	}

	void method2562(int i, int i_32_) {
		int i_33_ = ((Class149_Sub1) this).anInt9274 * 1801773735 * i >> 12;
		int i_34_ = ((Class149_Sub1) this).anInt9276 * 731213113 * i >> 12;
		int i_35_ = -1269720619 * ((Class149_Sub1) this).anInt9273 * i_32_ >> 12;
		int i_36_ = i_32_ * (1736404795 * ((Class149_Sub1) this).anInt9275) >> 12;
		Class299.method5316(i_33_, i_35_, i_34_, i_36_, ((Class149_Sub1) this).anInt1743 * 1340859839, -1525176857 * ((Class149_Sub1) this).anInt1741, -1125689331 * ((Class149_Sub1) this).anInt1742, (short) 16743);
	}

	static Class149_Sub1 method14581(RsByteBuffer class282_sub35) {
		return new Class149_Sub1(class282_sub35.method13081(1783648323), class282_sub35.method13081(1758019693), class282_sub35.method13081(1770239692), class282_sub35.method13081(1845399409), class282_sub35.method13082((short) 7331), class282_sub35.method13082((short) 24830), class282_sub35.readUnsignedByte());
	}

	public static void method14582(int i, int i_37_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(5, (long) i);
		class282_sub50_sub12.method14965((byte) -51);
	}

	static void method14583(int i, int i_38_, int i_39_, int i_40_, int i_41_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(8, (long) i);
		class282_sub50_sub12.method14995(1672803485);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_38_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = i_39_ * 717659479;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = -1932168275 * i_40_;
	}

	public static void method14584(int i, int i_42_, int i_43_, int i_44_, int i_45_, short i_46_) {
		if (i_43_ == i_44_)
			Class364.method6292(i, i_42_, i_43_, i_45_, -1428254942);
		else if (i - i_43_ >= Class532_Sub3_Sub1.anInt7071 * -612590951 && i + i_43_ <= -1345107225 * Class532_Sub3_Sub1.anInt7069 && i_42_ - i_44_ >= Class532_Sub3_Sub1.anInt7070 * 324226563 && i_42_ + i_44_ <= Class532_Sub3_Sub1.anInt7068 * -348932735)
			Class221.method3751(i, i_42_, i_43_, i_44_, i_45_, (byte) 61);
		else
			Class282_Sub17_Sub3.method15405(i, i_42_, i_43_, i_44_, i_45_, (byte) 0);
	}
}
