/* Class513 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class513 {
	Class98 aClass98_5885;
	Class118 aClass118_5886;

	boolean method8764(int i, int i_0_, int i_1_) {
		Class118 class118 = Class317.method5694(i, i_0_, -1661012661);
		if (null != class118) {
			((Class513) this).aClass98_5885 = Class468_Sub8.aClass98Array7889[i >> 16];
			((Class513) this).aClass118_5886 = class118;
			return true;
		}
		method8768(525234186);
		return false;
	}

	void method8765() {
		((Class513) this).aClass98_5885 = null;
		((Class513) this).aClass118_5886 = null;
	}

	boolean method8766(Class98 class98, int i, int i_2_, byte i_3_) {
		if (class98 != null) {
			Class118 class118 = class98.method1618(i, (byte) 12);
			if (class118 != null) {
				((Class513) this).aClass98_5885 = class98;
				((Class513) this).aClass118_5886 = class118;
				return true;
			}
		}
		method8768(-2008016727);
		return false;
	}

	Class118 method8767() {
		return ((Class513) this).aClass98_5885.method1618((-1952846363 * (((Class513) this).aClass118_5886.anInt1287)), (byte) 12);
	}

	void method8768(int i) {
		((Class513) this).aClass98_5885 = null;
		((Class513) this).aClass118_5886 = null;
	}

	boolean method8769(int i, int i_4_) {
		Class118 class118 = Class317.method5694(i, i_4_, -2096788619);
		if (null != class118) {
			((Class513) this).aClass98_5885 = Class468_Sub8.aClass98Array7889[i >> 16];
			((Class513) this).aClass118_5886 = class118;
			return true;
		}
		method8768(-1650622853);
		return false;
	}

	boolean method8770(int i, int i_5_) {
		Class118 class118 = Class317.method5694(i, i_5_, 34459327);
		if (null != class118) {
			((Class513) this).aClass98_5885 = Class468_Sub8.aClass98Array7889[i >> 16];
			((Class513) this).aClass118_5886 = class118;
			return true;
		}
		method8768(674252676);
		return false;
	}

	boolean method8771(int i, int i_6_) {
		Class118 class118 = Class317.method5694(i, i_6_, -1507662436);
		if (null != class118) {
			((Class513) this).aClass98_5885 = Class468_Sub8.aClass98Array7889[i >> 16];
			((Class513) this).aClass118_5886 = class118;
			return true;
		}
		method8768(1128998173);
		return false;
	}

	Class513() {
		/* empty */
	}

	Class118 method8772(int i) {
		return ((Class513) this).aClass98_5885.method1618((-1952846363 * (((Class513) this).aClass118_5886.anInt1287)), (byte) 12);
	}

	boolean method8773(Class98 class98, int i, int i_7_) {
		if (class98 != null) {
			Class118 class118 = class98.method1618(i, (byte) 12);
			if (class118 != null) {
				((Class513) this).aClass98_5885 = class98;
				((Class513) this).aClass118_5886 = class118;
				return true;
			}
		}
		method8768(1529356125);
		return false;
	}

	boolean method8774(Class98 class98, int i, int i_8_) {
		if (class98 != null) {
			Class118 class118 = class98.method1618(i, (byte) 12);
			if (class118 != null) {
				((Class513) this).aClass98_5885 = class98;
				((Class513) this).aClass118_5886 = class118;
				return true;
			}
		}
		method8768(-946767130);
		return false;
	}

	boolean method8775(Class98 class98, int i, int i_9_) {
		if (class98 != null) {
			Class118 class118 = class98.method1618(i, (byte) 12);
			if (class118 != null) {
				((Class513) this).aClass98_5885 = class98;
				((Class513) this).aClass118_5886 = class118;
				return true;
			}
		}
		method8768(-1447248681);
		return false;
	}

	Class118 method8776() {
		return ((Class513) this).aClass98_5885.method1618((-1952846363 * (((Class513) this).aClass118_5886.anInt1287)), (byte) 12);
	}

	static final void method8777(int i, byte i_10_) {
		if (!Class456_Sub3.method12682(i, null, -1514339264)) {
			if (i_10_ >= 5) {
				/* empty */
			}
		} else
			Class464.method7743((Class468_Sub8.aClass98Array7889[i].aClass118Array998), -1, (byte) 10);
	}
}
