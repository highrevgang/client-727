/* Class187 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class187 {
	static Class482 aClass482_2350;
	static int anInt2351;
	static boolean aBool2352;
	static int anInt2353;
	static int[] anIntArray2354;
	static int[] anIntArray2355;
	static boolean aBool2356;
	static int[] anIntArray2357;
	static boolean aBool2358 = false;
	public static int anInt2359;
	static boolean aBool2360;
	public static int anInt2361;
	static final int anInt2362 = 48;
	static int anInt2363;

	static boolean method3086(Interface12 interface12) {
		Class478 class478 = client.aClass257_7353.method4436(-2118381265).method7891(interface12.method84(-326509168), 65280);
		if (-272332433 * class478.anInt5689 == -1)
			return true;
		Class418 class418 = Class97.aClass427_995.method7172(-272332433 * class478.anInt5689, -1014703371);
		if (-1 == -1053123675 * class418.anInt4995)
			return true;
		return class418.method7015(65280);
	}

	static {
		aBool2352 = false;
		anInt2351 = -1481335827;
		anInt2353 = 0;
		anIntArray2354 = new int[1014];
		anIntArray2355 = new int[1001];
		anIntArray2357 = new int[1010];
		aClass482_2350 = new Class482();
		anInt2361 = 134656021;
		anInt2359 = 818291313;
		aBool2360 = true;
		aBool2356 = false;
		anInt2363 = 0;
	}

	static void method3087(Class505 class505, int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		Class206 class206 = client.aClass257_7353.method4430(-1948087701);
		Interface12 interface12 = (Interface12) class206.method3381(i, i_0_, i_1_, (byte) -75);
		if (null != interface12) {
			Class478 class478 = client.aClass257_7353.method4436(-2007893886).method7891(interface12.method84(626678104), 65280);
			int i_6_ = interface12.method92(912502279) & 0x3;
			int i_7_ = interface12.method89(664192342);
			if (-272332433 * class478.anInt5689 != -1)
				Class415.method6997(class505, class478, i_6_, i_2_, i_3_, -681045520);
			else {
				int i_8_ = i_4_;
				if (-348507379 * class478.anInt5652 > 0)
					i_8_ = i_5_;
				if (i_7_ == 1109376893 * Class458.aClass458_5470.anInt5481 || (1109376893 * Class458.aClass458_5472.anInt5481 == i_7_)) {
					if (0 == i_6_)
						class505.method8428(i_2_, i_3_, 4, i_8_, -1796764807);
					else if (i_6_ == 1)
						class505.method8659(i_2_, i_3_, 4, i_8_, -920937700);
					else if (2 == i_6_)
						class505.method8428(3 + i_2_, i_3_, 4, i_8_, -1796764807);
					else if (3 == i_6_)
						class505.method8659(i_2_, i_3_ + 3, 4, i_8_, 315682433);
				}
				if (i_7_ == Class458.aClass458_5473.anInt5481 * 1109376893) {
					if (i_6_ == 0)
						class505.method8425(i_2_, i_3_, 1, 1, i_8_, (byte) -83);
					else if (i_6_ == 1)
						class505.method8425(i_2_ + 3, i_3_, 1, 1, i_8_, (byte) -127);
					else if (i_6_ == 2)
						class505.method8425(i_2_ + 3, 3 + i_3_, 1, 1, i_8_, (byte) -36);
					else if (i_6_ == 3)
						class505.method8425(i_2_, 3 + i_3_, 1, 1, i_8_, (byte) -43);
				}
				if (i_7_ == 1109376893 * Class458.aClass458_5472.anInt5481) {
					if (i_6_ == 0)
						class505.method8659(i_2_, i_3_, 4, i_8_, -1137139860);
					else if (i_6_ == 1)
						class505.method8428(i_2_ + 3, i_3_, 4, i_8_, -1796764807);
					else if (i_6_ == 2)
						class505.method8659(i_2_, i_3_ + 3, 4, i_8_, 263194071);
					else if (3 == i_6_)
						class505.method8428(i_2_, i_3_, 4, i_8_, -1796764807);
				}
			}
		}
		interface12 = (Interface12) class206.method3413(i, i_0_, i_1_, client.anInterface25_7446, -233664382);
		if (interface12 != null) {
			Class478 class478 = client.aClass257_7353.method4436(-1549694881).method7891(interface12.method84(2116756521), 65280);
			int i_9_ = interface12.method92(-1683670314) & 0x3;
			int i_10_ = interface12.method89(1283419054);
			if (-272332433 * class478.anInt5689 != -1)
				Class415.method6997(class505, class478, i_9_, i_2_, i_3_, -1234568697);
			else if (i_10_ == Class458.aClass458_5474.anInt5481 * 1109376893) {
				int i_11_ = -1118482;
				if (class478.anInt5652 * -348507379 > 0)
					i_11_ = -1179648;
				if (i_9_ == 0 || i_9_ == 2)
					class505.method8429(i_2_, 3 + i_3_, 3 + i_2_, i_3_, i_11_, (byte) 33);
				else
					class505.method8429(i_2_, i_3_, i_2_ + 3, 3 + i_3_, i_11_, (byte) 10);
			}
		}
		interface12 = (Interface12) class206.method3415(i, i_0_, i_1_, -387297653);
		if (interface12 != null) {
			Class478 class478 = client.aClass257_7353.method4436(-2072849518).method7891(interface12.method84(511365810), 65280);
			int i_12_ = interface12.method92(-1269197044) & 0x3;
			if (class478.anInt5689 * -272332433 != -1)
				Class415.method6997(class505, class478, i_12_, i_2_, i_3_, -530545051);
		}
	}

	static void method3088(Class118 class118, Class455 class455, int i, int i_13_, int i_14_, int i_15_, int i_16_, String string, Class8 class8, Class414 class414, int i_17_) {
		int i_18_;
		if (4 == -672443707 * Class262.anInt3240)
			i_18_ = (int) client.aFloat7365 & 0x3fff;
		else
			i_18_ = (client.anInt7255 * -1790074477 + (int) client.aFloat7365 & 0x3fff);
		int i_19_ = (Math.max(class118.anInt1301 * 1506818197 / 2, class118.anInt1429 * -492594917 / 2) + 10);
		int i_20_ = i_15_ * i_15_ + i_14_ * i_14_;
		if (i_20_ <= i_19_ * i_19_) {
			int i_21_ = Class382.anIntArray4657[i_18_];
			int i_22_ = Class382.anIntArray4661[i_18_];
			if (4 != -672443707 * Class262.anInt3240) {
				i_21_ = i_21_ * 256 / (256 + client.anInt7203 * -1864403271);
				i_22_ = i_22_ * 256 / (256 + client.anInt7203 * -1864403271);
			}
			int i_23_ = i_21_ * i_15_ + i_14_ * i_22_ >> 14;
			int i_24_ = i_22_ * i_15_ - i_21_ * i_14_ >> 14;
			int i_25_ = class414.method6951(string, 100, null, (byte) 113);
			int i_26_ = class414.method6972(string, 100, 0, null, 830346304);
			i_23_ -= i_25_ / 2;
			if (i_23_ >= -(class118.anInt1301 * 1506818197) && i_23_ <= class118.anInt1301 * 1506818197 && i_24_ >= -(-492594917 * class118.anInt1429) && i_24_ <= class118.anInt1429 * -492594917)
				class8.method378(string, 1506818197 * class118.anInt1301 / 2 + (i_23_ + i), (-492594917 * class118.anInt1429 / 2 + i_13_ - i_24_ - i_16_ - i_26_), i_25_, 50, i_17_, 0, 1, 0, 0, null, null, class455, i, i_13_, (byte) 52);
		}
	}

	static void method3089() {
		anInt2363 = 0;
		anInt2351 = -1481335827;
		anInt2361 = 134656021;
		anInt2359 = 818291313;
	}

	static void method3090(Class505 class505) {
		if ((335283739 * anInt2351 != Class84.myPlayer.aByte7967) && client.aClass257_7353.method4430(-983685168) != null) {
			Class169.method2869(1978389713);
			if (Class289.method5097(class505, (Class84.myPlayer.aByte7967), -544839726))
				anInt2351 = (1481335827 * Class84.myPlayer.aByte7967);
		}
	}

	static void method3091() {
		anInt2363 = 0;
		anInt2361 = 134656021;
		anInt2359 = 818291313;
	}

	public static void method3092() {
		Class419.aClass160_5004 = null;
		anInt2351 = -1481335827;
	}

	public static void method3093() {
		Class419.aClass160_5004 = null;
		anInt2351 = -1481335827;
	}

	static void method3094(Class505 class505, int i, int i_27_, int i_28_, int i_29_, int i_30_, int i_31_, int i_32_) {
		Class206 class206 = client.aClass257_7353.method4430(-1600984556);
		Interface12 interface12 = (Interface12) class206.method3381(i, i_27_, i_28_, (byte) -10);
		if (null != interface12) {
			Class478 class478 = client.aClass257_7353.method4436(-1483622872).method7891(interface12.method84(-1419359144), 65280);
			int i_33_ = interface12.method92(-1613278351) & 0x3;
			int i_34_ = interface12.method89(326355339);
			if (-272332433 * class478.anInt5689 != -1)
				Class415.method6997(class505, class478, i_33_, i_29_, i_30_, 865251554);
			else {
				int i_35_ = i_31_;
				if (-348507379 * class478.anInt5652 > 0)
					i_35_ = i_32_;
				if (i_34_ == 1109376893 * Class458.aClass458_5470.anInt5481 || (1109376893 * Class458.aClass458_5472.anInt5481 == i_34_)) {
					if (0 == i_33_)
						class505.method8428(i_29_, i_30_, 4, i_35_, -1796764807);
					else if (i_33_ == 1)
						class505.method8659(i_29_, i_30_, 4, i_35_, -674023653);
					else if (2 == i_33_)
						class505.method8428(3 + i_29_, i_30_, 4, i_35_, -1796764807);
					else if (3 == i_33_)
						class505.method8659(i_29_, i_30_ + 3, 4, i_35_, -2116750618);
				}
				if (i_34_ == Class458.aClass458_5473.anInt5481 * 1109376893) {
					if (i_33_ == 0)
						class505.method8425(i_29_, i_30_, 1, 1, i_35_, (byte) -122);
					else if (i_33_ == 1)
						class505.method8425(i_29_ + 3, i_30_, 1, 1, i_35_, (byte) -128);
					else if (i_33_ == 2)
						class505.method8425(i_29_ + 3, 3 + i_30_, 1, 1, i_35_, (byte) -95);
					else if (i_33_ == 3)
						class505.method8425(i_29_, 3 + i_30_, 1, 1, i_35_, (byte) -45);
				}
				if (i_34_ == 1109376893 * Class458.aClass458_5472.anInt5481) {
					if (i_33_ == 0)
						class505.method8659(i_29_, i_30_, 4, i_35_, 828979178);
					else if (i_33_ == 1)
						class505.method8428(i_29_ + 3, i_30_, 4, i_35_, -1796764807);
					else if (i_33_ == 2)
						class505.method8659(i_29_, i_30_ + 3, 4, i_35_, 1172969548);
					else if (3 == i_33_)
						class505.method8428(i_29_, i_30_, 4, i_35_, -1796764807);
				}
			}
		}
		interface12 = (Interface12) class206.method3413(i, i_27_, i_28_, client.anInterface25_7446, -233664382);
		if (interface12 != null) {
			Class478 class478 = client.aClass257_7353.method4436(-1380489516).method7891(interface12.method84(-628715439), 65280);
			int i_36_ = interface12.method92(525401084) & 0x3;
			int i_37_ = interface12.method89(1377100294);
			if (-272332433 * class478.anInt5689 != -1)
				Class415.method6997(class505, class478, i_36_, i_29_, i_30_, -2049238608);
			else if (i_37_ == Class458.aClass458_5474.anInt5481 * 1109376893) {
				int i_38_ = -1118482;
				if (class478.anInt5652 * -348507379 > 0)
					i_38_ = -1179648;
				if (i_36_ == 0 || i_36_ == 2)
					class505.method8429(i_29_, 3 + i_30_, 3 + i_29_, i_30_, i_38_, (byte) 92);
				else
					class505.method8429(i_29_, i_30_, i_29_ + 3, 3 + i_30_, i_38_, (byte) -23);
			}
		}
		interface12 = (Interface12) class206.method3415(i, i_27_, i_28_, -387297653);
		if (interface12 != null) {
			Class478 class478 = client.aClass257_7353.method4436(-1903879360).method7891(interface12.method84(-341179320), 65280);
			int i_39_ = interface12.method92(790063002) & 0x3;
			if (class478.anInt5689 * -272332433 != -1)
				Class415.method6997(class505, class478, i_39_, i_29_, i_30_, -1548145816);
		}
	}

	static boolean method3095(Class505 class505, int i) {
		Class169.method2869(2069163243);
		if (!class505.method8455(-430422880))
			return false;
		int i_40_ = client.aClass257_7353.method4424(1802161148);
		int i_41_ = client.aClass257_7353.method4451(-2072443514);
		Class311 class311 = client.aClass257_7353.method4433(33386298);
		Class206 class206 = client.aClass257_7353.method4430(-1462611319);
		int i_42_ = i_40_ / 2;
		int i_43_ = 0;
		int i_44_ = 0;
		boolean bool = true;
		for (int i_45_ = i_43_; i_45_ < i_40_ + i_43_; i_45_++) {
			for (int i_46_ = i_44_; i_46_ < i_41_ + i_44_; i_46_++) {
				for (int i_47_ = i; i_47_ <= 3; i_47_++) {
					if (class311.method5498(i, i_47_, i_45_, i_46_, (short) -11679)) {
						int i_48_ = i_47_;
						if (class311.method5497(i_45_, i_46_, 1893024760))
							i_48_--;
						if (i_48_ >= 0)
							bool &= Class282_Sub50_Sub9.method14921(i_48_, i_45_, i_46_, (byte) 15);
					}
				}
			}
		}
		if (!bool)
			return false;
		int i_49_ = 48 + 4 * i_40_ + 48;
		int[] is = new int[i_49_ * i_49_];
		for (int i_50_ = 0; i_50_ < is.length; i_50_++)
			is[i_50_] = -16777216;
		Class158_Sub1 class158_sub1 = null;
		int i_51_ = 0;
		int i_52_ = 0;
		if (aBool2352) {
			Class419.aClass160_5004 = class505.method8654(i_49_, i_49_, false, true);
			class158_sub1 = class505.method8418();
			class158_sub1.method13759(0, Class419.aClass160_5004.method2808());
			Interface8 interface8 = class505.method8419(i_49_, i_49_);
			class158_sub1.method13765(interface8);
			class505.method8637(class158_sub1, -731268950);
			i_42_ = i_40_;
			i_51_ = 48;
			i_52_ = 48;
			class505.ba(1, 0);
		} else
			Class419.aClass160_5004 = class505.method8549(is, 0, i_49_, i_49_, i_49_, 265003650);
		client.aClass257_7353.method4435((byte) 1).method4052((byte) 1);
		int i_53_ = ~0xffffff | ((238 + (int) (Math.random() * 20.0) - 10 << 16) + (238 + (int) (Math.random() * 20.0) - 10 << 8) + (238 + (int) (Math.random() * 20.0) - 10));
		int i_54_ = ~0xffffff | 238 + (int) (Math.random() * 20.0) - 10 << 16;
		int i_55_ = ((int) (Math.random() * 8.0) << 16 | (int) (Math.random() * 8.0) << 8 | (int) (Math.random() * 8.0));
		boolean[][] bools = new boolean[1 + i_42_ + 2][i_42_ + 1 + 2];
		for (int i_56_ = i_43_; i_56_ < i_43_ + i_40_; i_56_ += i_42_) {
			for (int i_57_ = i_44_; i_57_ < i_41_ + i_44_; i_57_ += i_42_) {
				int i_58_ = i_51_;
				int i_59_ = i_52_;
				int i_60_ = i_56_;
				if (i_60_ > 0) {
					i_60_--;
					i_58_ += 4;
				}
				int i_61_ = i_57_;
				if (i_61_ > 0)
					i_61_--;
				int i_62_ = i_42_ + i_56_;
				if (i_62_ < i_40_)
					i_62_++;
				int i_63_ = i_42_ + i_57_;
				if (i_63_ < i_41_) {
					i_63_++;
					i_59_ += 4;
				}
				if (aBool2358)
					class505.L();
				else
					class505.r(0, 0, 4 * i_42_ + i_58_, i_42_ * 4 + i_59_);
				class505.ba(3, -16777216);
				int i_64_ = i_42_;
				if (i_64_ > i_40_ - 1)
					i_64_ = i_40_ - 1;
				for (int i_65_ = i; i_65_ <= 3; i_65_++) {
					for (int i_66_ = 0; i_66_ <= i_64_; i_66_++) {
						for (int i_67_ = 0; i_67_ <= i_64_; i_67_++)
							bools[i_66_][i_67_] = class311.method5498(i, i_65_, i_60_ + i_66_, i_67_ + i_61_, (short) -14860);
					}
					class206.aClass390Array2607[i_65_].method6715(i_51_, i_52_, 1024, i_60_, i_61_, i_62_, i_63_, bools);
					if (!aBool2356) {
						for (int i_68_ = -4; i_68_ < i_42_; i_68_++) {
							for (int i_69_ = -4; i_69_ < i_42_; i_69_++) {
								int i_70_ = i_56_ + i_68_;
								int i_71_ = i_57_ + i_69_;
								if (i_70_ >= i_43_ && i_71_ >= i_44_ && class311.method5498(i, i_65_, i_70_, i_71_, (short) 3426)) {
									int i_72_ = i_65_;
									if (class311.method5497(i_70_, i_71_, 1976584401))
										i_72_--;
									if (i_72_ >= 0)
										Class225_Sub5.method13042(class505, i_72_, i_70_, i_71_, i_58_ + 4 * i_68_, i_59_ + 4 * (i_42_ - i_69_) - 4, i_53_, i_54_, 621081934);
								}
							}
						}
					}
				}
				if (aBool2356) {
					Class336 class336 = client.aClass257_7353.method4552(i, 1801793645);
					for (int i_73_ = 0; i_73_ < i_42_; i_73_++) {
						for (int i_74_ = 0; i_74_ < i_42_; i_74_++) {
							int i_75_ = i_56_ + i_73_;
							int i_76_ = i_57_ + i_74_;
							int i_77_ = (class336.anIntArrayArray3922[i_75_ - -1969357273 * class336.anInt3931][i_76_ - 1503444365 * class336.anInt3964]);
							if (0 != (i_77_ & 0x40240000))
								class505.method8425(4 * i_73_ + i_58_, i_59_ + 4 * (i_42_ - i_74_) - 4, 4, 4, -1713569622, (byte) -94);
							else if ((i_77_ & 0x800000) != 0)
								class505.method8659(i_58_ + 4 * i_73_, ((i_42_ - i_74_) * 4 + i_59_ - 4), 4, -1713569622, -2141362285);
							else if ((i_77_ & 0x2000000) != 0)
								class505.method8428(i_58_ + i_73_ * 4 + 3, (4 * (i_42_ - i_74_) + i_59_ - 4), 4, -1713569622, -1796764807);
							else if (0 != (i_77_ & 0x8000000))
								class505.method8659(i_58_ + i_73_ * 4, 3 + ((i_42_ - i_74_) * 4 + i_59_ - 4), 4, -1713569622, -1379624461);
							else if ((i_77_ & 0x20000000) != 0)
								class505.method8428(i_73_ * 4 + i_58_, (i_59_ + (i_42_ - i_74_) * 4 - 4), 4, -1713569622, -1796764807);
						}
					}
				}
				class505.B(i_58_, i_59_, i_42_ * 4, i_42_ * 4, i_55_, 2);
				if (!aBool2352) {
					Class419.aClass160_5004.method2750(4 * (i_56_ - i_43_) + 48, i_41_ * 4 + 48 - 4 * (i_57_ - i_44_) - i_42_ * 4, 4 * i_42_, 4 * i_42_, i_58_, i_59_);
					if (aBool2358) {
						Class419.aClass160_5004.method2752(256, 0);
						try {
							class505.method8393((short) 7229);
							Class89.method1504(2000L);
						} catch (Exception exception) {
							/* empty */
						}
					}
				}
			}
		}
		if (aBool2352) {
			class505.method8416(class158_sub1, (byte) -118);
			if (aBool2358) {
				Class419.aClass160_5004.method2752(256, 0);
				try {
					class505.method8393((short) 11810);
					Class89.method1504(2000L);
				} catch (Exception exception) {
					/* empty */
				}
			}
		}
		class505.L();
		class505.ba(1, 1);
		IncommingPacket.method6378(-1538407760);
		Class474 class474 = client.aClass257_7353.method4436(-1618994596);
		anInt2353 = 0;
		aClass482_2350.method8118(1320582811);
		if (!aBool2356) {
			for (int i_78_ = i_43_; i_78_ < i_43_ + i_40_; i_78_++) {
				for (int i_79_ = i_44_; i_79_ < i_41_ + i_44_; i_79_++) {
					for (int i_80_ = i; i_80_ <= i + 1 && i_80_ <= 3; i_80_++) {
						if (class311.method5498(i, i_80_, i_78_, i_79_, (short) -16360)) {
							Interface12 interface12 = ((Interface12) class206.method3415(i_80_, i_78_, i_79_, -387297653));
							if (null == interface12)
								interface12 = ((Interface12) (class206.method3413(i_80_, i_78_, i_79_, client.anInterface25_7446, -233664382)));
							if (interface12 == null)
								interface12 = ((Interface12) class206.method3381(i_80_, i_78_, i_79_, (byte) -111));
							if (interface12 == null)
								interface12 = ((Interface12) class206.method3511(i_80_, i_78_, i_79_, (byte) -17));
							if (interface12 != null) {
								Class478 class478 = (class474.method7891(interface12.method84(-1469910462), 65280));
								if (!class478.aBool5660 || client.aBool7317) {
									int i_81_ = class478.anInt5669 * -1796959211;
									if (class478.anIntArray5650 != null) {
										for (int i_82_ = 0; i_82_ < (class478.anIntArray5650).length; i_82_++) {
											if (class478.anIntArray5650[i_82_] != -1) {
												Class478 class478_83_ = (class474.method7891((class478.anIntArray5650[i_82_]), 65280));
												if ((class478_83_.anInt5669 * -1796959211) >= 0)
													i_81_ = (-1796959211 * (class478_83_.anInt5669));
											}
										}
									}
									if (i_81_ >= 0) {
										boolean bool_84_ = false;
										if (i_81_ >= 0) {
											Class220 class220 = (Class397.aClass218_4813.method3700(i_81_, 1758326687));
											if (class220 != null && class220.aBool2730)
												bool_84_ = true;
										}
										int i_85_ = i_78_;
										int i_86_ = i_79_;
										if (bool_84_) {
											int[][] is_87_ = (client.aClass257_7353.method4552(i_80_, 1801793645).anIntArrayArray3922);
											int i_88_ = ((client.aClass257_7353.method4552(i_80_, 1801793645).anInt3931) * -1969357273);
											int i_89_ = ((client.aClass257_7353.method4552(i_80_, 1801793645).anInt3964) * 1503444365);
											for (int i_90_ = 0; i_90_ < 10; i_90_++) {
												int i_91_ = (int) (Math.random() * 4.0);
												if (0 == i_91_ && i_85_ > i_43_ && i_85_ > i_78_ - 3 && 0 == ((is_87_[(i_85_ - 1 - i_88_)][i_86_ - i_89_]) & 0x2c0108))
													i_85_--;
												if (i_91_ == 1 && (i_85_ < i_43_ + i_40_ - 1) && i_85_ < 3 + i_78_ && ((is_87_[1 + i_85_ - i_88_][i_86_ - i_89_]) & 0x2c0180) == 0)
													i_85_++;
												if (2 == i_91_ && i_86_ > i_44_ && i_86_ > i_79_ - 3 && 0 == ((is_87_[i_85_ - i_88_][(i_86_ - 1 - i_89_)]) & 0x2c0102))
													i_86_--;
												if (i_91_ == 3 && (i_86_ < i_44_ + i_41_ - 1) && i_86_ < 3 + i_79_ && 0 == ((is_87_[i_85_ - i_88_][(1 + i_86_ - i_89_)]) & 0x2c0120))
													i_86_++;
											}
										}
										anIntArray2357[anInt2353 * 1036045197] = class478.anInt5633 * -2132690865;
										anIntArray2354[1036045197 * anInt2353] = i_85_;
										anIntArray2355[anInt2353 * 1036045197] = i_86_;
										anInt2353 += -928813243;
									}
								}
							}
						}
					}
				}
			}
			Class283 class283 = client.aClass257_7353.method4528((byte) 94);
			if (class283 != null) {
				Class397.aClass218_4813.method3697(1024, 64, 1120242543);
				Class219 class219 = client.aClass257_7353.method4519(1351911825);
				for (int i_92_ = 0; i_92_ < class283.anInt3382 * -361490119; i_92_++) {
					int i_93_ = class283.anIntArray3381[i_92_];
					if (i_93_ >> 28 == (Class84.myPlayer.aByte7967)) {
						int i_94_ = ((i_93_ >> 14 & 0x3fff) - class219.anInt2711 * 1948093437);
						int i_95_ = ((i_93_ & 0x3fff) - class219.anInt2712 * -1002240017);
						if (i_94_ >= 0 && i_94_ < i_40_ && i_95_ >= 0 && i_95_ < i_41_)
							aClass482_2350.method8059(new Class282_Sub38(i_92_), 183772667);
						else {
							Class220 class220 = (Class397.aClass218_4813.method3700(class283.anIntArray3383[i_92_], -198409634));
							if (null != class220.anIntArray2717 && (i_94_ + class220.anInt2731 * -1051190363 >= 0) && (class220.anInt2747 * -195227125 + i_94_ < i_40_) && 1123286327 * class220.anInt2746 + i_95_ >= 0 && (i_95_ + class220.anInt2744 * 1104382109 < i_41_))
								aClass482_2350.method8059(new Class282_Sub38(i_92_), -762553260);
						}
					}
				}
				Class397.aClass218_4813.method3697(128, 64, -1970115902);
			}
		}
		return true;
	}

	static boolean method3096(int i, int i_96_, int i_97_) {
		Class206 class206 = client.aClass257_7353.method4430(-1915412558);
		boolean bool = true;
		Interface12 interface12 = (Interface12) class206.method3381(i, i_96_, i_97_, (byte) -62);
		if (null != interface12)
			bool &= Class93.method1577(interface12, -1693071504);
		interface12 = (Interface12) class206.method3413(i, i_96_, i_97_, client.anInterface25_7446, -233664382);
		if (interface12 != null)
			bool &= Class93.method1577(interface12, -2064079199);
		interface12 = (Interface12) class206.method3415(i, i_96_, i_97_, -387297653);
		if (interface12 != null)
			bool &= Class93.method1577(interface12, -2005264057);
		return bool;
	}

	static boolean method3097(int i, int i_98_, int i_99_) {
		Class206 class206 = client.aClass257_7353.method4430(-1634579275);
		boolean bool = true;
		Interface12 interface12 = (Interface12) class206.method3381(i, i_98_, i_99_, (byte) -100);
		if (null != interface12)
			bool &= Class93.method1577(interface12, -1598191053);
		interface12 = (Interface12) class206.method3413(i, i_98_, i_99_, client.anInterface25_7446, -233664382);
		if (interface12 != null)
			bool &= Class93.method1577(interface12, -1846776019);
		interface12 = (Interface12) class206.method3415(i, i_98_, i_99_, -387297653);
		if (interface12 != null)
			bool &= Class93.method1577(interface12, -2003442416);
		return bool;
	}

	static void method3098(Class118 class118, Class119 class119, int i, int i_100_, int i_101_, int i_102_, int i_103_, long l) {
		int i_104_ = i_101_ * i_101_ + i_102_ * i_102_;
		if ((long) i_104_ <= l) {
			int i_105_;
			if (4 == -672443707 * Class262.anInt3240)
				i_105_ = (int) client.aFloat7365 & 0x3fff;
			else
				i_105_ = ((int) client.aFloat7365 + client.anInt7255 * -1790074477 & 0x3fff);
			int i_106_ = Class382.anIntArray4657[i_105_];
			int i_107_ = Class382.anIntArray4661[i_105_];
			if (-672443707 * Class262.anInt3240 != 4) {
				i_106_ = i_106_ * 256 / (client.anInt7203 * -1864403271 + 256);
				i_107_ = 256 * i_107_ / (256 + -1864403271 * client.anInt7203);
			}
			int i_108_ = i_102_ * i_106_ + i_107_ * i_101_ >> 14;
			int i_109_ = i_102_ * i_107_ - i_106_ * i_101_ >> 14;
			Class160 class160 = Class282_Sub20_Sub15.aClass160Array9838[i_103_];
			int i_110_ = class160.method2747();
			int i_111_ = class160.method2793();
			int i_112_ = class118.anInt1301 * 1506818197 / 2 + i_108_ - i_110_ / 2;
			int i_113_ = i_110_ + i_112_;
			int i_114_ = class118.anInt1429 * -492594917 / 2 + -i_109_ - i_111_;
			int i_115_ = i_114_ + i_111_;
			if (!class119.method2073(i_112_, i_114_, 874624609) || !class119.method2073(i_113_, i_114_, 1753441566) || !class119.method2073(i_112_, i_115_, 692173179) || !class119.method2073(i_113_, i_115_, -770394254)) {
				double d = Math.atan2((double) i_108_, (double) i_109_);
				int i_116_ = Math.min(class118.anInt1301 * 1506818197 / 2, -492594917 * class118.anInt1429 / 2);
				i_116_ -= 6;
				int i_117_ = (int) (Math.sin(d) * (double) i_116_);
				int i_118_ = (int) (Math.cos(d) * (double) i_116_);
				Class245.aClass160Array3027[i_103_].method2758(((float) i + (float) (class118.anInt1301 * 1506818197) / 2.0F + (float) i_117_), ((float) i_100_ + (float) (-492594917 * class118.anInt1429) / 2.0F - (float) i_118_), 4096, (int) (65535.0 * (-d / 6.283185307179586)));
			} else
				class160.method2773(i + i_112_, i_100_ + i_114_, class119.aClass455_1456, i, i_100_);
		}
	}

	static void method3099(Class118 class118, Class119 class119, int i, int i_119_, int i_120_, int i_121_, int i_122_, long l) {
		int i_123_ = i_120_ * i_120_ + i_121_ * i_121_;
		if ((long) i_123_ <= l) {
			int i_124_;
			if (4 == -672443707 * Class262.anInt3240)
				i_124_ = (int) client.aFloat7365 & 0x3fff;
			else
				i_124_ = ((int) client.aFloat7365 + client.anInt7255 * -1790074477 & 0x3fff);
			int i_125_ = Class382.anIntArray4657[i_124_];
			int i_126_ = Class382.anIntArray4661[i_124_];
			if (-672443707 * Class262.anInt3240 != 4) {
				i_125_ = i_125_ * 256 / (client.anInt7203 * -1864403271 + 256);
				i_126_ = 256 * i_126_ / (256 + -1864403271 * client.anInt7203);
			}
			int i_127_ = i_121_ * i_125_ + i_126_ * i_120_ >> 14;
			int i_128_ = i_121_ * i_126_ - i_125_ * i_120_ >> 14;
			Class160 class160 = Class282_Sub20_Sub15.aClass160Array9838[i_122_];
			int i_129_ = class160.method2747();
			int i_130_ = class160.method2793();
			int i_131_ = class118.anInt1301 * 1506818197 / 2 + i_127_ - i_129_ / 2;
			int i_132_ = i_129_ + i_131_;
			int i_133_ = class118.anInt1429 * -492594917 / 2 + -i_128_ - i_130_;
			int i_134_ = i_133_ + i_130_;
			if (!class119.method2073(i_131_, i_133_, -326785836) || !class119.method2073(i_132_, i_133_, 380712187) || !class119.method2073(i_131_, i_134_, -1299028238) || !class119.method2073(i_132_, i_134_, -2125231683)) {
				double d = Math.atan2((double) i_127_, (double) i_128_);
				int i_135_ = Math.min(class118.anInt1301 * 1506818197 / 2, -492594917 * class118.anInt1429 / 2);
				i_135_ -= 6;
				int i_136_ = (int) (Math.sin(d) * (double) i_135_);
				int i_137_ = (int) (Math.cos(d) * (double) i_135_);
				Class245.aClass160Array3027[i_122_].method2758(((float) i + (float) (class118.anInt1301 * 1506818197) / 2.0F + (float) i_136_), ((float) i_119_ + (float) (-492594917 * class118.anInt1429) / 2.0F - (float) i_137_), 4096, (int) (65535.0 * (-d / 6.283185307179586)));
			} else
				class160.method2773(i + i_131_, i_119_ + i_133_, class119.aClass455_1456, i, i_119_);
		}
	}

	static boolean method3100(Interface12 interface12) {
		Class478 class478 = client.aClass257_7353.method4436(-2052724610).method7891(interface12.method84(-139834775), 65280);
		if (-272332433 * class478.anInt5689 == -1)
			return true;
		Class418 class418 = Class97.aClass427_995.method7172(-272332433 * class478.anInt5689, -1014703371);
		if (-1 == -1053123675 * class418.anInt4995)
			return true;
		return class418.method7015(65280);
	}

	static void method3101() {
		anInt2363 = 0;
		anInt2351 = -1481335827;
		anInt2361 = 134656021;
		anInt2359 = 818291313;
	}

	static void method3102(Class505 class505, Class455 class455, Class118 class118, int i, int i_138_, int i_139_, int i_140_, int i_141_) {
		Class220 class220 = Class397.aClass218_4813.method3700(i_141_, 1313237634);
		if (class220 != null && class220.aBool2729 && class220.method3719(Class158_Sub1.aClass3_8507, 1584685808)) {
			if (null != class220.anIntArray2717) {
				int[] is = new int[class220.anIntArray2717.length];
				for (int i_142_ = 0; i_142_ < is.length / 2; i_142_++) {
					int i_143_;
					if (-672443707 * Class262.anInt3240 == 4)
						i_143_ = (int) client.aFloat7365 & 0x3fff;
					else
						i_143_ = (client.anInt7255 * -1790074477 + (int) client.aFloat7365) & 0x3fff;
					int i_144_ = Class382.anIntArray4657[i_143_];
					int i_145_ = Class382.anIntArray4661[i_143_];
					if (4 != -672443707 * Class262.anInt3240) {
						i_144_ = i_144_ * 256 / (client.anInt7203 * -1864403271 + 256);
						i_145_ = 256 * i_145_ / (-1864403271 * client.anInt7203 + 256);
					}
					is[i_142_ * 2] = ((i_144_ * (i_140_ + 4 * (class220.anIntArray2717[2 * i_142_ + 1])) + ((i_139_ + 4 * class220.anIntArray2717[i_142_ * 2]) * i_145_)) >> 14) + (1506818197 * class118.anInt1301 / 2 + i);
					is[i_142_ * 2 + 1] = (-492594917 * class118.anInt1429 / 2 + i_138_ - ((((i_140_ + 4 * class220.anIntArray2717[1 + i_142_ * 2]) * i_145_) - ((i_139_ + 4 * class220.anIntArray2717[i_142_ * 2]) * i_144_)) >> 14));
				}
				Class119 class119 = class118.method2046(class505, 1990569068);
				if (null != class119)
					Class147.method2505(class505, is, class220.anInt2715 * 152819427, class119.anIntArray1457, class119.anIntArray1455);
				if (-1216326857 * class220.anInt2748 > 0) {
					for (int i_146_ = 0; i_146_ < is.length / 2 - 1; i_146_++) {
						int i_147_ = is[i_146_ * 2];
						int i_148_ = is[i_146_ * 2 + 1];
						int i_149_ = is[2 * (i_146_ + 1)];
						int i_150_ = is[1 + 2 * (i_146_ + 1)];
						if (i_149_ < i_147_) {
							int i_151_ = i_147_;
							int i_152_ = i_148_;
							i_147_ = i_149_;
							i_148_ = i_150_;
							i_149_ = i_151_;
							i_150_ = i_152_;
						} else if (i_147_ == i_149_ && i_150_ < i_148_) {
							int i_153_ = i_148_;
							i_148_ = i_150_;
							i_150_ = i_153_;
						}
						class505.method8563(i_147_, i_148_, i_149_, i_150_, (class220.anIntArray2738[(class220.aByteArray2754[i_146_] & 0xff)]), 1, class455, i, i_138_, -1216326857 * class220.anInt2748, 1940337227 * class220.anInt2749, -155138445 * class220.anInt2756);
					}
					int i_154_ = is[is.length - 2];
					int i_155_ = is[is.length - 1];
					int i_156_ = is[0];
					int i_157_ = is[1];
					if (i_156_ < i_154_) {
						int i_158_ = i_154_;
						int i_159_ = i_155_;
						i_154_ = i_156_;
						i_155_ = i_157_;
						i_156_ = i_158_;
						i_157_ = i_159_;
					} else if (i_156_ == i_154_ && i_157_ < i_155_) {
						int i_160_ = i_155_;
						i_155_ = i_157_;
						i_157_ = i_160_;
					}
					class505.method8563(i_154_, i_155_, i_156_, i_157_, (class220.anIntArray2738[(class220.aByteArray2754[(class220.aByteArray2754.length - 1)]) & 0xff]), 1, class455, i, i_138_, -1216326857 * class220.anInt2748, 1940337227 * class220.anInt2749, class220.anInt2756 * -155138445);
				} else {
					for (int i_161_ = 0; i_161_ < is.length / 2 - 1; i_161_++)
						class505.method8669(is[2 * i_161_], is[1 + i_161_ * 2], is[(i_161_ + 1) * 2], is[1 + (i_161_ + 1) * 2], (class220.anIntArray2738[(class220.aByteArray2754[i_161_] & 0xff)]), 1, class455, i, i_138_);
					class505.method8669(is[is.length - 2], is[is.length - 1], is[0], is[1], (class220.anIntArray2738[(class220.aByteArray2754[(class220.aByteArray2754.length - 1)]) & 0xff]), 1, class455, i, i_138_);
				}
			}
			Class160 class160 = null;
			if (-1 != class220.anInt2719 * -1248709255) {
				class160 = class220.method3735(class505, false, (byte) 8);
				if (class160 != null)
					Class190.method3149(class118, class455, i, i_138_, i_139_, i_140_, class160, -743879431);
			}
			if (null != class220.aString2751) {
				int i_162_ = 0;
				if (class160 != null)
					i_162_ = class160.method2793();
				Class8 class8 = Class540.aClass8_7138;
				Class414 class414 = Class282_Sub17_Sub2.aClass414_9933;
				if (1 == class220.anInt2722 * 1172439539) {
					class8 = Class16.aClass8_144;
					class414 = Class16.aClass414_139;
				}
				if (class220.anInt2722 * 1172439539 == 2) {
					class8 = Class285.aClass8_3394;
					class414 = Class288.aClass414_3438;
				}
				Class241.method4152(class118, class455, i, i_138_, i_139_, i_140_, i_162_, class220.aString2751, class8, class414, class220.anInt2720 * -2116785903, -1728351676);
			}
		}
	}

	static void method3103() {
		anInt2363 = 0;
		anInt2351 = -1481335827;
		anInt2361 = 134656021;
		anInt2359 = 818291313;
	}

	static void method3104(Class505 class505, Class118 class118, int i, int i_163_) {
		Class119 class119 = class118.method2046(class505, 1134133395);
		if (class119 != null) {
			Class455 class455 = class119.aClass455_1456;
			class505.r(i, i_163_, i + class118.anInt1301 * 1506818197, class118.anInt1429 * -492594917 + i_163_);
			if ((1506818197 * class118.anInt1301 != -1125753931 * class119.anInt1458) || (-492594917 * class118.anInt1429 != 2069222845 * class119.anInt1454))
				throw new IllegalStateException("");
			if (2 != -1221526793 * anInt2363 && anInt2363 * -1221526793 != 5 && Class419.aClass160_5004 != null) {
				Class219 class219 = client.aClass257_7353.method4519(1153919557);
				int i_164_;
				int i_165_;
				int i_166_;
				int i_167_;
				if (-672443707 * Class262.anInt3240 == 4) {
					i_164_ = client.anInt7262 * 61805441;
					i_165_ = client.anInt7376 * -1032332761;
					i_166_ = (int) -client.aFloat7365 & 0x3fff;
					i_167_ = 4096;
				} else {
					Class385 class385 = (Class84.myPlayer.method11166().aClass385_3595);
					i_164_ = (int) class385.aFloat4671;
					i_165_ = (int) class385.aFloat4673;
					i_166_ = ((int) -client.aFloat7365 + -1790074477 * client.anInt7255) & 0x3fff;
					i_167_ = 4096 - 234318736 * client.anInt7203;
				}
				int i_168_ = 48 + i_164_ / 128;
				int i_169_ = (48 + client.aClass257_7353.method4451(-1318065497) * 4 - i_165_ / 128);
				Class419.aClass160_5004.method2762(((float) i + (float) (class118.anInt1301 * 1506818197) / 2.0F), ((float) i_163_ + (float) (-492594917 * class118.anInt1429) / 2.0F), (float) i_168_, (float) i_169_, i_167_, i_166_ << 2, class455, i, i_163_);
				Class283 class283 = client.aClass257_7353.method4528((byte) 63);
				for (Class282_Sub38 class282_sub38 = ((Class282_Sub38) aClass482_2350.method8097((byte) 84)); null != class282_sub38; class282_sub38 = ((Class282_Sub38) aClass482_2350.method8067(-1076678458))) {
					int i_170_ = class282_sub38.anInt8002 * -570797415;
					int i_171_ = ((class283.anIntArray3381[i_170_] >> 14 & 0x3fff) - class219.anInt2711 * 1948093437);
					int i_172_ = ((class283.anIntArray3381[i_170_] & 0x3fff) - class219.anInt2712 * -1002240017);
					int i_173_ = 4 * i_171_ + 2 - i_164_ / 128;
					int i_174_ = 2 + i_172_ * 4 - i_165_ / 128;
					Class158.method2731(class505, class455, class118, i, i_163_, i_173_, i_174_, class283.anIntArray3383[i_170_], -1053153336);
				}
				for (int i_175_ = 0; i_175_ < anInt2353 * 1036045197; i_175_++) {
					int i_176_ = 4 * anIntArray2354[i_175_] + 2 - i_164_ / 128;
					int i_177_ = anIntArray2355[i_175_] * 4 + 2 - i_165_ / 128;
					Class478 class478 = client.aClass257_7353.method4436(-1507516071).method7891(anIntArray2357[i_175_], 65280);
					if (null != class478.anIntArray5650) {
						class478 = class478.method8013(Class158_Sub1.aClass3_8507, (byte) -86);
						if (class478 == null || -1796959211 * class478.anInt5669 == -1)
							continue;
					}
					Class158.method2731(class505, class455, class118, i, i_163_, i_176_, i_177_, -1796959211 * class478.anInt5669, -1286828798);
				}
				for (Class282_Sub29 class282_sub29 = ((Class282_Sub29) client.aClass465_7414.method7750(1358409500)); class282_sub29 != null; class282_sub29 = ((Class282_Sub29) client.aClass465_7414.method7751((byte) 120))) {
					int i_178_ = (int) ((class282_sub29.aLong3379 * -3442165056282524525L) >> 28 & 0x3L);
					if (335283739 * anInt2351 == i_178_) {
						int i_179_ = ((int) ((-3442165056282524525L * class282_sub29.aLong3379) & 0x3fffL) - class219.anInt2711 * 1948093437);
						int i_180_ = ((int) ((class282_sub29.aLong3379 * -3442165056282524525L) >> 14 & 0x3fffL) - class219.anInt2712 * -1002240017);
						int i_181_ = 4 * i_179_ + 2 - i_164_ / 128;
						int i_182_ = 2 + 4 * i_180_ - i_165_ / 128;
						Class190.method3149(class118, class455, i, i_163_, i_181_, i_182_, Class250.aClass160Array3092[0], 269316589);
					}
				}
				Class469.method7805(class505, i_164_, i_165_, class118, class455, i, i_163_, 2085955785);
				Class82.method1457(i_164_, i_165_, class118, class455, i, i_163_, 1942118537);
				Class190.method3151(i_164_, i_165_, class118, class119, i, i_163_, 848202629);
				if (Class262.anInt3240 * -672443707 != 4) {
					if (895508675 * anInt2361 != 0) {
						int i_183_ = (-712932596 * anInt2361 + 2 - i_164_ / 128 + (Class84.myPlayer.method15805(828768449) - 1) * 2);
						int i_184_ = (2 + 20612540 * anInt2359 - i_165_ / 128 + (Class84.myPlayer.method15805(828768449) - 1) * 2);
						Class190.method3149(class118, class455, i, i_163_, i_183_, i_184_, (Class16.aClass160Array145[aBool2360 ? 1 : 0]), 35856752);
					}
					if (!Class84.myPlayer.aBool10548)
						class505.method8425(i + 1506818197 * class118.anInt1301 / 2 - 1, i_163_ + -492594917 * class118.anInt1429 / 2 - 1, 3, 3, -1, (byte) -34);
				}
			} else
				class505.DA(-16777216, class455, i, i_163_);
		}
	}

	static void method3105(Class505 class505, Class118 class118, int i, int i_185_) {
		Class119 class119 = class118.method2046(class505, -708316004);
		if (class119 != null) {
			Class455 class455 = class119.aClass455_1456;
			class505.r(i, i_185_, i + class118.anInt1301 * 1506818197, class118.anInt1429 * -492594917 + i_185_);
			if ((1506818197 * class118.anInt1301 != -1125753931 * class119.anInt1458) || (-492594917 * class118.anInt1429 != 2069222845 * class119.anInt1454))
				throw new IllegalStateException("");
			if (2 != -1221526793 * anInt2363 && anInt2363 * -1221526793 != 5 && Class419.aClass160_5004 != null) {
				Class219 class219 = client.aClass257_7353.method4519(183918460);
				int i_186_;
				int i_187_;
				int i_188_;
				int i_189_;
				if (-672443707 * Class262.anInt3240 == 4) {
					i_186_ = client.anInt7262 * 61805441;
					i_187_ = client.anInt7376 * -1032332761;
					i_188_ = (int) -client.aFloat7365 & 0x3fff;
					i_189_ = 4096;
				} else {
					Class385 class385 = (Class84.myPlayer.method11166().aClass385_3595);
					i_186_ = (int) class385.aFloat4671;
					i_187_ = (int) class385.aFloat4673;
					i_188_ = ((int) -client.aFloat7365 + -1790074477 * client.anInt7255) & 0x3fff;
					i_189_ = 4096 - 234318736 * client.anInt7203;
				}
				int i_190_ = 48 + i_186_ / 128;
				int i_191_ = (48 + client.aClass257_7353.method4451(-793700854) * 4 - i_187_ / 128);
				Class419.aClass160_5004.method2762(((float) i + (float) (class118.anInt1301 * 1506818197) / 2.0F), ((float) i_185_ + (float) (-492594917 * class118.anInt1429) / 2.0F), (float) i_190_, (float) i_191_, i_189_, i_188_ << 2, class455, i, i_185_);
				Class283 class283 = client.aClass257_7353.method4528((byte) 20);
				for (Class282_Sub38 class282_sub38 = ((Class282_Sub38) aClass482_2350.method8097((byte) 73)); null != class282_sub38; class282_sub38 = ((Class282_Sub38) aClass482_2350.method8067(1404797203))) {
					int i_192_ = class282_sub38.anInt8002 * -570797415;
					int i_193_ = ((class283.anIntArray3381[i_192_] >> 14 & 0x3fff) - class219.anInt2711 * 1948093437);
					int i_194_ = ((class283.anIntArray3381[i_192_] & 0x3fff) - class219.anInt2712 * -1002240017);
					int i_195_ = 4 * i_193_ + 2 - i_186_ / 128;
					int i_196_ = 2 + i_194_ * 4 - i_187_ / 128;
					Class158.method2731(class505, class455, class118, i, i_185_, i_195_, i_196_, class283.anIntArray3383[i_192_], 516590075);
				}
				for (int i_197_ = 0; i_197_ < anInt2353 * 1036045197; i_197_++) {
					int i_198_ = 4 * anIntArray2354[i_197_] + 2 - i_186_ / 128;
					int i_199_ = anIntArray2355[i_197_] * 4 + 2 - i_187_ / 128;
					Class478 class478 = client.aClass257_7353.method4436(-1458347711).method7891(anIntArray2357[i_197_], 65280);
					if (null != class478.anIntArray5650) {
						class478 = class478.method8013(Class158_Sub1.aClass3_8507, (byte) 27);
						if (class478 == null || -1796959211 * class478.anInt5669 == -1)
							continue;
					}
					Class158.method2731(class505, class455, class118, i, i_185_, i_198_, i_199_, -1796959211 * class478.anInt5669, 1111518542);
				}
				for (Class282_Sub29 class282_sub29 = ((Class282_Sub29) client.aClass465_7414.method7750(-1890306229)); class282_sub29 != null; class282_sub29 = ((Class282_Sub29) client.aClass465_7414.method7751((byte) 36))) {
					int i_200_ = (int) ((class282_sub29.aLong3379 * -3442165056282524525L) >> 28 & 0x3L);
					if (335283739 * anInt2351 == i_200_) {
						int i_201_ = ((int) ((-3442165056282524525L * class282_sub29.aLong3379) & 0x3fffL) - class219.anInt2711 * 1948093437);
						int i_202_ = ((int) ((class282_sub29.aLong3379 * -3442165056282524525L) >> 14 & 0x3fffL) - class219.anInt2712 * -1002240017);
						int i_203_ = 4 * i_201_ + 2 - i_186_ / 128;
						int i_204_ = 2 + 4 * i_202_ - i_187_ / 128;
						Class190.method3149(class118, class455, i, i_185_, i_203_, i_204_, Class250.aClass160Array3092[0], -992627499);
					}
				}
				Class469.method7805(class505, i_186_, i_187_, class118, class455, i, i_185_, 2132463299);
				Class82.method1457(i_186_, i_187_, class118, class455, i, i_185_, 1942118537);
				Class190.method3151(i_186_, i_187_, class118, class119, i, i_185_, 848202629);
				if (Class262.anInt3240 * -672443707 != 4) {
					if (895508675 * anInt2361 != 0) {
						int i_205_ = (-712932596 * anInt2361 + 2 - i_186_ / 128 + (Class84.myPlayer.method15805(828768449) - 1) * 2);
						int i_206_ = (2 + 20612540 * anInt2359 - i_187_ / 128 + (Class84.myPlayer.method15805(828768449) - 1) * 2);
						Class190.method3149(class118, class455, i, i_185_, i_205_, i_206_, (Class16.aClass160Array145[aBool2360 ? 1 : 0]), -470882151);
					}
					if (!Class84.myPlayer.aBool10548)
						class505.method8425(i + 1506818197 * class118.anInt1301 / 2 - 1, i_185_ + -492594917 * class118.anInt1429 / 2 - 1, 3, 3, -1, (byte) -89);
				}
			} else
				class505.DA(-16777216, class455, i, i_185_);
		}
	}

	public static void method3106() {
		Class419.aClass160_5004 = null;
		anInt2351 = -1481335827;
	}

	static void method3107(Class505 class505, int i, int i_207_, Class118 class118, Class455 class455, int i_208_, int i_209_) {
		for (int i_210_ = 0; i_210_ < -685729279 * client.anInt7211; i_210_++) {
			Class282_Sub47 class282_sub47 = ((Class282_Sub47) client.aClass465_7208.method7754((long) client.anIntArray7212[i_210_]));
			if (class282_sub47 != null) {
				Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2 = ((Class521_Sub1_Sub1_Sub2_Sub2) class282_sub47.anObject8068);
				if (class521_sub1_sub1_sub2_sub2.method16160(-521331999) && (class521_sub1_sub1_sub2_sub2.aByte7967 == (Class84.myPlayer.aByte7967))) {
					Class409 class409 = class521_sub1_sub1_sub2_sub2.aClass409_10580;
					if (null != class409 && null != class409.anIntArray4886)
						class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
					if (class409 != null && class409.aBool4864 && class409.aBool4893) {
						Class385 class385 = (class521_sub1_sub1_sub2_sub2.method11166().aClass385_3595);
						int i_211_ = (int) class385.aFloat4671 / 128 - i / 128;
						int i_212_ = (int) class385.aFloat4673 / 128 - i_207_ / 128;
						if (-1230504941 * class409.anInt4914 != -1)
							Class158.method2731(class505, class455, class118, i_208_, i_209_, i_211_, i_212_, (-1230504941 * class409.anInt4914), 70094831);
						else
							Class190.method3149(class118, class455, i_208_, i_209_, i_211_, i_212_, Class250.aClass160Array3092[1], 1694391109);
					}
				}
			}
		}
	}

	static void method3108(Class505 class505, int i, int i_213_, Class118 class118, Class455 class455, int i_214_, int i_215_) {
		for (int i_216_ = 0; i_216_ < -685729279 * client.anInt7211; i_216_++) {
			Class282_Sub47 class282_sub47 = ((Class282_Sub47) client.aClass465_7208.method7754((long) client.anIntArray7212[i_216_]));
			if (class282_sub47 != null) {
				Class521_Sub1_Sub1_Sub2_Sub2 class521_sub1_sub1_sub2_sub2 = ((Class521_Sub1_Sub1_Sub2_Sub2) class282_sub47.anObject8068);
				if (class521_sub1_sub1_sub2_sub2.method16160(-1939000509) && (class521_sub1_sub1_sub2_sub2.aByte7967 == (Class84.myPlayer.aByte7967))) {
					Class409 class409 = class521_sub1_sub1_sub2_sub2.aClass409_10580;
					if (null != class409 && null != class409.anIntArray4886)
						class409 = class409.method6884(Class158_Sub1.aClass3_8507, 265881693);
					if (class409 != null && class409.aBool4864 && class409.aBool4893) {
						Class385 class385 = (class521_sub1_sub1_sub2_sub2.method11166().aClass385_3595);
						int i_217_ = (int) class385.aFloat4671 / 128 - i / 128;
						int i_218_ = (int) class385.aFloat4673 / 128 - i_213_ / 128;
						if (-1230504941 * class409.anInt4914 != -1)
							Class158.method2731(class505, class455, class118, i_214_, i_215_, i_217_, i_218_, (-1230504941 * class409.anInt4914), -1757218088);
						else
							Class190.method3149(class118, class455, i_214_, i_215_, i_217_, i_218_, Class250.aClass160Array3092[1], 79902556);
					}
				}
			}
		}
	}

	static void method3109(Class505 class505, Class455 class455, Class118 class118, int i, int i_219_, int i_220_, int i_221_, int i_222_) {
		Class220 class220 = Class397.aClass218_4813.method3700(i_222_, -573403);
		if (class220 != null && class220.aBool2729 && class220.method3719(Class158_Sub1.aClass3_8507, 984524935)) {
			if (null != class220.anIntArray2717) {
				int[] is = new int[class220.anIntArray2717.length];
				for (int i_223_ = 0; i_223_ < is.length / 2; i_223_++) {
					int i_224_;
					if (-672443707 * Class262.anInt3240 == 4)
						i_224_ = (int) client.aFloat7365 & 0x3fff;
					else
						i_224_ = (client.anInt7255 * -1790074477 + (int) client.aFloat7365) & 0x3fff;
					int i_225_ = Class382.anIntArray4657[i_224_];
					int i_226_ = Class382.anIntArray4661[i_224_];
					if (4 != -672443707 * Class262.anInt3240) {
						i_225_ = i_225_ * 256 / (client.anInt7203 * -1864403271 + 256);
						i_226_ = 256 * i_226_ / (-1864403271 * client.anInt7203 + 256);
					}
					is[i_223_ * 2] = ((i_225_ * (i_221_ + 4 * (class220.anIntArray2717[2 * i_223_ + 1])) + ((i_220_ + 4 * class220.anIntArray2717[i_223_ * 2]) * i_226_)) >> 14) + (1506818197 * class118.anInt1301 / 2 + i);
					is[i_223_ * 2 + 1] = (-492594917 * class118.anInt1429 / 2 + i_219_ - ((((i_221_ + 4 * class220.anIntArray2717[1 + i_223_ * 2]) * i_226_) - ((i_220_ + 4 * class220.anIntArray2717[i_223_ * 2]) * i_225_)) >> 14));
				}
				Class119 class119 = class118.method2046(class505, 103905667);
				if (null != class119)
					Class147.method2505(class505, is, class220.anInt2715 * 152819427, class119.anIntArray1457, class119.anIntArray1455);
				if (-1216326857 * class220.anInt2748 > 0) {
					for (int i_227_ = 0; i_227_ < is.length / 2 - 1; i_227_++) {
						int i_228_ = is[i_227_ * 2];
						int i_229_ = is[i_227_ * 2 + 1];
						int i_230_ = is[2 * (i_227_ + 1)];
						int i_231_ = is[1 + 2 * (i_227_ + 1)];
						if (i_230_ < i_228_) {
							int i_232_ = i_228_;
							int i_233_ = i_229_;
							i_228_ = i_230_;
							i_229_ = i_231_;
							i_230_ = i_232_;
							i_231_ = i_233_;
						} else if (i_228_ == i_230_ && i_231_ < i_229_) {
							int i_234_ = i_229_;
							i_229_ = i_231_;
							i_231_ = i_234_;
						}
						class505.method8563(i_228_, i_229_, i_230_, i_231_, (class220.anIntArray2738[(class220.aByteArray2754[i_227_] & 0xff)]), 1, class455, i, i_219_, -1216326857 * class220.anInt2748, 1940337227 * class220.anInt2749, -155138445 * class220.anInt2756);
					}
					int i_235_ = is[is.length - 2];
					int i_236_ = is[is.length - 1];
					int i_237_ = is[0];
					int i_238_ = is[1];
					if (i_237_ < i_235_) {
						int i_239_ = i_235_;
						int i_240_ = i_236_;
						i_235_ = i_237_;
						i_236_ = i_238_;
						i_237_ = i_239_;
						i_238_ = i_240_;
					} else if (i_237_ == i_235_ && i_238_ < i_236_) {
						int i_241_ = i_236_;
						i_236_ = i_238_;
						i_238_ = i_241_;
					}
					class505.method8563(i_235_, i_236_, i_237_, i_238_, (class220.anIntArray2738[(class220.aByteArray2754[(class220.aByteArray2754.length - 1)]) & 0xff]), 1, class455, i, i_219_, -1216326857 * class220.anInt2748, 1940337227 * class220.anInt2749, class220.anInt2756 * -155138445);
				} else {
					for (int i_242_ = 0; i_242_ < is.length / 2 - 1; i_242_++)
						class505.method8669(is[2 * i_242_], is[1 + i_242_ * 2], is[(i_242_ + 1) * 2], is[1 + (i_242_ + 1) * 2], (class220.anIntArray2738[(class220.aByteArray2754[i_242_] & 0xff)]), 1, class455, i, i_219_);
					class505.method8669(is[is.length - 2], is[is.length - 1], is[0], is[1], (class220.anIntArray2738[(class220.aByteArray2754[(class220.aByteArray2754.length - 1)]) & 0xff]), 1, class455, i, i_219_);
				}
			}
			Class160 class160 = null;
			if (-1 != class220.anInt2719 * -1248709255) {
				class160 = class220.method3735(class505, false, (byte) 8);
				if (class160 != null)
					Class190.method3149(class118, class455, i, i_219_, i_220_, i_221_, class160, -1305268935);
			}
			if (null != class220.aString2751) {
				int i_243_ = 0;
				if (class160 != null)
					i_243_ = class160.method2793();
				Class8 class8 = Class540.aClass8_7138;
				Class414 class414 = Class282_Sub17_Sub2.aClass414_9933;
				if (1 == class220.anInt2722 * 1172439539) {
					class8 = Class16.aClass8_144;
					class414 = Class16.aClass414_139;
				}
				if (class220.anInt2722 * 1172439539 == 2) {
					class8 = Class285.aClass8_3394;
					class414 = Class288.aClass414_3438;
				}
				Class241.method4152(class118, class455, i, i_219_, i_220_, i_221_, i_243_, class220.aString2751, class8, class414, class220.anInt2720 * -2116785903, -2085198310);
			}
		}
	}

	Class187() throws Throwable {
		throw new Error();
	}

	static void method3110(Class505 class505, Class118 class118, int i, int i_244_) {
		Class119 class119 = class118.method2046(class505, 522701617);
		if (class119 != null) {
			Class455 class455 = class119.aClass455_1456;
			class505.r(i, i_244_, i + class118.anInt1301 * 1506818197, class118.anInt1429 * -492594917 + i_244_);
			if ((1506818197 * class118.anInt1301 != -1125753931 * class119.anInt1458) || (-492594917 * class118.anInt1429 != 2069222845 * class119.anInt1454))
				throw new IllegalStateException("");
			if (2 != -1221526793 * anInt2363 && anInt2363 * -1221526793 != 5 && Class419.aClass160_5004 != null) {
				Class219 class219 = client.aClass257_7353.method4519(1659374082);
				int i_245_;
				int i_246_;
				int i_247_;
				int i_248_;
				if (-672443707 * Class262.anInt3240 == 4) {
					i_245_ = client.anInt7262 * 61805441;
					i_246_ = client.anInt7376 * -1032332761;
					i_247_ = (int) -client.aFloat7365 & 0x3fff;
					i_248_ = 4096;
				} else {
					Class385 class385 = (Class84.myPlayer.method11166().aClass385_3595);
					i_245_ = (int) class385.aFloat4671;
					i_246_ = (int) class385.aFloat4673;
					i_247_ = ((int) -client.aFloat7365 + -1790074477 * client.anInt7255) & 0x3fff;
					i_248_ = 4096 - 234318736 * client.anInt7203;
				}
				int i_249_ = 48 + i_245_ / 128;
				int i_250_ = (48 + client.aClass257_7353.method4451(-1780488761) * 4 - i_246_ / 128);
				Class419.aClass160_5004.method2762(((float) i + (float) (class118.anInt1301 * 1506818197) / 2.0F), ((float) i_244_ + (float) (-492594917 * class118.anInt1429) / 2.0F), (float) i_249_, (float) i_250_, i_248_, i_247_ << 2, class455, i, i_244_);
				Class283 class283 = client.aClass257_7353.method4528((byte) 4);
				for (Class282_Sub38 class282_sub38 = ((Class282_Sub38) aClass482_2350.method8097((byte) 92)); null != class282_sub38; class282_sub38 = ((Class282_Sub38) aClass482_2350.method8067(490916566))) {
					int i_251_ = class282_sub38.anInt8002 * -570797415;
					int i_252_ = ((class283.anIntArray3381[i_251_] >> 14 & 0x3fff) - class219.anInt2711 * 1948093437);
					int i_253_ = ((class283.anIntArray3381[i_251_] & 0x3fff) - class219.anInt2712 * -1002240017);
					int i_254_ = 4 * i_252_ + 2 - i_245_ / 128;
					int i_255_ = 2 + i_253_ * 4 - i_246_ / 128;
					Class158.method2731(class505, class455, class118, i, i_244_, i_254_, i_255_, class283.anIntArray3383[i_251_], -1325406400);
				}
				for (int i_256_ = 0; i_256_ < anInt2353 * 1036045197; i_256_++) {
					int i_257_ = 4 * anIntArray2354[i_256_] + 2 - i_245_ / 128;
					int i_258_ = anIntArray2355[i_256_] * 4 + 2 - i_246_ / 128;
					Class478 class478 = client.aClass257_7353.method4436(-1945527275).method7891(anIntArray2357[i_256_], 65280);
					if (null != class478.anIntArray5650) {
						class478 = class478.method8013(Class158_Sub1.aClass3_8507, (byte) -16);
						if (class478 == null || -1796959211 * class478.anInt5669 == -1)
							continue;
					}
					Class158.method2731(class505, class455, class118, i, i_244_, i_257_, i_258_, -1796959211 * class478.anInt5669, -1400770453);
				}
				for (Class282_Sub29 class282_sub29 = ((Class282_Sub29) client.aClass465_7414.method7750(-1332887526)); class282_sub29 != null; class282_sub29 = ((Class282_Sub29) client.aClass465_7414.method7751((byte) 76))) {
					int i_259_ = (int) ((class282_sub29.aLong3379 * -3442165056282524525L) >> 28 & 0x3L);
					if (335283739 * anInt2351 == i_259_) {
						int i_260_ = ((int) ((-3442165056282524525L * class282_sub29.aLong3379) & 0x3fffL) - class219.anInt2711 * 1948093437);
						int i_261_ = ((int) ((class282_sub29.aLong3379 * -3442165056282524525L) >> 14 & 0x3fffL) - class219.anInt2712 * -1002240017);
						int i_262_ = 4 * i_260_ + 2 - i_245_ / 128;
						int i_263_ = 2 + 4 * i_261_ - i_246_ / 128;
						Class190.method3149(class118, class455, i, i_244_, i_262_, i_263_, Class250.aClass160Array3092[0], -1651658493);
					}
				}
				Class469.method7805(class505, i_245_, i_246_, class118, class455, i, i_244_, 1965954165);
				Class82.method1457(i_245_, i_246_, class118, class455, i, i_244_, 1942118537);
				Class190.method3151(i_245_, i_246_, class118, class119, i, i_244_, 848202629);
				if (Class262.anInt3240 * -672443707 != 4) {
					if (895508675 * anInt2361 != 0) {
						int i_264_ = (-712932596 * anInt2361 + 2 - i_245_ / 128 + (Class84.myPlayer.method15805(828768449) - 1) * 2);
						int i_265_ = (2 + 20612540 * anInt2359 - i_246_ / 128 + (Class84.myPlayer.method15805(828768449) - 1) * 2);
						Class190.method3149(class118, class455, i, i_244_, i_264_, i_265_, (Class16.aClass160Array145[aBool2360 ? 1 : 0]), -1476867877);
					}
					if (!Class84.myPlayer.aBool10548)
						class505.method8425(i + 1506818197 * class118.anInt1301 / 2 - 1, i_244_ + -492594917 * class118.anInt1429 / 2 - 1, 3, 3, -1, (byte) -102);
				}
			} else
				class505.DA(-16777216, class455, i, i_244_);
		}
	}

	static void method3111(Class118 class118, Class119 class119, int i, int i_266_, int i_267_, int i_268_, int i_269_, long l) {
		int i_270_ = i_267_ * i_267_ + i_268_ * i_268_;
		if ((long) i_270_ <= l) {
			int i_271_;
			if (4 == -672443707 * Class262.anInt3240)
				i_271_ = (int) client.aFloat7365 & 0x3fff;
			else
				i_271_ = ((int) client.aFloat7365 + client.anInt7255 * -1790074477 & 0x3fff);
			int i_272_ = Class382.anIntArray4657[i_271_];
			int i_273_ = Class382.anIntArray4661[i_271_];
			if (-672443707 * Class262.anInt3240 != 4) {
				i_272_ = i_272_ * 256 / (client.anInt7203 * -1864403271 + 256);
				i_273_ = 256 * i_273_ / (256 + -1864403271 * client.anInt7203);
			}
			int i_274_ = i_268_ * i_272_ + i_273_ * i_267_ >> 14;
			int i_275_ = i_268_ * i_273_ - i_272_ * i_267_ >> 14;
			Class160 class160 = Class282_Sub20_Sub15.aClass160Array9838[i_269_];
			int i_276_ = class160.method2747();
			int i_277_ = class160.method2793();
			int i_278_ = class118.anInt1301 * 1506818197 / 2 + i_274_ - i_276_ / 2;
			int i_279_ = i_276_ + i_278_;
			int i_280_ = class118.anInt1429 * -492594917 / 2 + -i_275_ - i_277_;
			int i_281_ = i_280_ + i_277_;
			if (!class119.method2073(i_278_, i_280_, 1391853647) || !class119.method2073(i_279_, i_280_, 113739014) || !class119.method2073(i_278_, i_281_, -875689973) || !class119.method2073(i_279_, i_281_, 1592088217)) {
				double d = Math.atan2((double) i_274_, (double) i_275_);
				int i_282_ = Math.min(class118.anInt1301 * 1506818197 / 2, -492594917 * class118.anInt1429 / 2);
				i_282_ -= 6;
				int i_283_ = (int) (Math.sin(d) * (double) i_282_);
				int i_284_ = (int) (Math.cos(d) * (double) i_282_);
				Class245.aClass160Array3027[i_269_].method2758(((float) i + (float) (class118.anInt1301 * 1506818197) / 2.0F + (float) i_283_), ((float) i_266_ + (float) (-492594917 * class118.anInt1429) / 2.0F - (float) i_284_), 4096, (int) (65535.0 * (-d / 6.283185307179586)));
			} else
				class160.method2773(i + i_278_, i_266_ + i_280_, class119.aClass455_1456, i, i_266_);
		}
	}

	static void method3112(Class505 class505, int i, int i_285_, int i_286_, int i_287_, int i_288_, int i_289_, int i_290_) {
		Class206 class206 = client.aClass257_7353.method4430(-1379295368);
		Interface12 interface12 = ((Interface12) class206.method3381(i, i_285_, i_286_, (byte) -101));
		if (null != interface12) {
			Class478 class478 = client.aClass257_7353.method4436(-1910755686).method7891(interface12.method84(135973245), 65280);
			int i_291_ = interface12.method92(1000596537) & 0x3;
			int i_292_ = interface12.method89(1741104025);
			if (-272332433 * class478.anInt5689 != -1)
				Class415.method6997(class505, class478, i_291_, i_287_, i_288_, 1099820530);
			else {
				int i_293_ = i_289_;
				if (-348507379 * class478.anInt5652 > 0)
					i_293_ = i_290_;
				if (i_292_ == 1109376893 * Class458.aClass458_5470.anInt5481 || (1109376893 * Class458.aClass458_5472.anInt5481 == i_292_)) {
					if (0 == i_291_)
						class505.method8428(i_287_, i_288_, 4, i_293_, -1796764807);
					else if (i_291_ == 1)
						class505.method8659(i_287_, i_288_, 4, i_293_, 501856706);
					else if (2 == i_291_)
						class505.method8428(3 + i_287_, i_288_, 4, i_293_, -1796764807);
					else if (3 == i_291_)
						class505.method8659(i_287_, i_288_ + 3, 4, i_293_, -603602264);
				}
				if (i_292_ == Class458.aClass458_5473.anInt5481 * 1109376893) {
					if (i_291_ == 0)
						class505.method8425(i_287_, i_288_, 1, 1, i_293_, (byte) -2);
					else if (i_291_ == 1)
						class505.method8425(i_287_ + 3, i_288_, 1, 1, i_293_, (byte) -62);
					else if (i_291_ == 2)
						class505.method8425(i_287_ + 3, 3 + i_288_, 1, 1, i_293_, (byte) -78);
					else if (i_291_ == 3)
						class505.method8425(i_287_, 3 + i_288_, 1, 1, i_293_, (byte) -122);
				}
				if (i_292_ == 1109376893 * Class458.aClass458_5472.anInt5481) {
					if (i_291_ == 0)
						class505.method8659(i_287_, i_288_, 4, i_293_, 1564989894);
					else if (i_291_ == 1)
						class505.method8428(i_287_ + 3, i_288_, 4, i_293_, -1796764807);
					else if (i_291_ == 2)
						class505.method8659(i_287_, i_288_ + 3, 4, i_293_, 1725892278);
					else if (3 == i_291_)
						class505.method8428(i_287_, i_288_, 4, i_293_, -1796764807);
				}
			}
		}
		interface12 = (Interface12) class206.method3413(i, i_285_, i_286_, client.anInterface25_7446, -233664382);
		if (interface12 != null) {
			Class478 class478 = client.aClass257_7353.method4436(-1459660401).method7891(interface12.method84(1944939974), 65280);
			int i_294_ = interface12.method92(732185325) & 0x3;
			int i_295_ = interface12.method89(1320349645);
			if (-272332433 * class478.anInt5689 != -1)
				Class415.method6997(class505, class478, i_294_, i_287_, i_288_, 635081289);
			else if (i_295_ == Class458.aClass458_5474.anInt5481 * 1109376893) {
				int i_296_ = -1118482;
				if (class478.anInt5652 * -348507379 > 0)
					i_296_ = -1179648;
				if (i_294_ == 0 || i_294_ == 2)
					class505.method8429(i_287_, 3 + i_288_, 3 + i_287_, i_288_, i_296_, (byte) 86);
				else
					class505.method8429(i_287_, i_288_, i_287_ + 3, 3 + i_288_, i_296_, (byte) -59);
			}
		}
		interface12 = (Interface12) class206.method3415(i, i_285_, i_286_, -387297653);
		if (interface12 != null) {
			Class478 class478 = client.aClass257_7353.method4436(-1379292929).method7891(interface12.method84(2113103116), 65280);
			int i_297_ = interface12.method92(-1780779575) & 0x3;
			if (class478.anInt5689 * -272332433 != -1)
				Class415.method6997(class505, class478, i_297_, i_287_, i_288_, -1939916173);
		}
	}

	static void method3113(Class118 class118, Class455 class455, int i, int i_298_, int i_299_, int i_300_, Class160 class160) {
		if (null != class160) {
			int i_301_;
			if (4 == Class262.anInt3240 * -672443707)
				i_301_ = (int) client.aFloat7365 & 0x3fff;
			else
				i_301_ = ((int) client.aFloat7365 + client.anInt7255 * -1790074477 & 0x3fff);
			int i_302_ = (Math.max(1506818197 * class118.anInt1301 / 2, -492594917 * class118.anInt1429 / 2) + 10);
			int i_303_ = i_300_ * i_300_ + i_299_ * i_299_;
			if (i_303_ <= i_302_ * i_302_) {
				int i_304_ = Class382.anIntArray4657[i_301_];
				int i_305_ = Class382.anIntArray4661[i_301_];
				if (4 != -672443707 * Class262.anInt3240) {
					i_304_ = 256 * i_304_ / (-1864403271 * client.anInt7203 + 256);
					i_305_ = 256 * i_305_ / (client.anInt7203 * -1864403271 + 256);
				}
				int i_306_ = i_300_ * i_304_ + i_305_ * i_299_ >> 14;
				int i_307_ = i_300_ * i_305_ - i_304_ * i_299_ >> 14;
				class160.method2773((1506818197 * class118.anInt1301 / 2 + i + i_306_ - class160.method228() / 2), (class118.anInt1429 * -492594917 / 2 + i_298_ - i_307_ - class160.method2748() / 2), class455, i, i_298_);
			}
		}
	}

	static void method3114(Class118 class118, Class455 class455, int i, int i_308_, int i_309_, int i_310_, int i_311_, String string, Class8 class8, Class414 class414, int i_312_) {
		int i_313_;
		if (4 == -672443707 * Class262.anInt3240)
			i_313_ = (int) client.aFloat7365 & 0x3fff;
		else
			i_313_ = (client.anInt7255 * -1790074477 + (int) client.aFloat7365 & 0x3fff);
		int i_314_ = (Math.max(class118.anInt1301 * 1506818197 / 2, class118.anInt1429 * -492594917 / 2) + 10);
		int i_315_ = i_310_ * i_310_ + i_309_ * i_309_;
		if (i_315_ <= i_314_ * i_314_) {
			int i_316_ = Class382.anIntArray4657[i_313_];
			int i_317_ = Class382.anIntArray4661[i_313_];
			if (4 != -672443707 * Class262.anInt3240) {
				i_316_ = i_316_ * 256 / (256 + client.anInt7203 * -1864403271);
				i_317_ = i_317_ * 256 / (256 + client.anInt7203 * -1864403271);
			}
			int i_318_ = i_316_ * i_310_ + i_309_ * i_317_ >> 14;
			int i_319_ = i_317_ * i_310_ - i_316_ * i_309_ >> 14;
			int i_320_ = class414.method6951(string, 100, null, (byte) 125);
			int i_321_ = class414.method6972(string, 100, 0, null, 2093859064);
			i_318_ -= i_320_ / 2;
			if (i_318_ >= -(class118.anInt1301 * 1506818197) && i_318_ <= class118.anInt1301 * 1506818197 && i_319_ >= -(-492594917 * class118.anInt1429) && i_319_ <= class118.anInt1429 * -492594917)
				class8.method378(string, 1506818197 * class118.anInt1301 / 2 + (i_318_ + i), (-492594917 * class118.anInt1429 / 2 + i_308_ - i_319_ - i_311_ - i_321_), i_320_, 50, i_312_, 0, 1, 0, 0, null, null, class455, i, i_308_, (byte) 56);
		}
	}

	static void method3115(Class118 class118, Class455 class455, int i, int i_322_, int i_323_, int i_324_, int i_325_, String string, Class8 class8, Class414 class414, int i_326_) {
		int i_327_;
		if (4 == -672443707 * Class262.anInt3240)
			i_327_ = (int) client.aFloat7365 & 0x3fff;
		else
			i_327_ = (client.anInt7255 * -1790074477 + (int) client.aFloat7365 & 0x3fff);
		int i_328_ = (Math.max(class118.anInt1301 * 1506818197 / 2, class118.anInt1429 * -492594917 / 2) + 10);
		int i_329_ = i_324_ * i_324_ + i_323_ * i_323_;
		if (i_329_ <= i_328_ * i_328_) {
			int i_330_ = Class382.anIntArray4657[i_327_];
			int i_331_ = Class382.anIntArray4661[i_327_];
			if (4 != -672443707 * Class262.anInt3240) {
				i_330_ = i_330_ * 256 / (256 + client.anInt7203 * -1864403271);
				i_331_ = i_331_ * 256 / (256 + client.anInt7203 * -1864403271);
			}
			int i_332_ = i_330_ * i_324_ + i_323_ * i_331_ >> 14;
			int i_333_ = i_331_ * i_324_ - i_330_ * i_323_ >> 14;
			int i_334_ = class414.method6951(string, 100, null, (byte) 77);
			int i_335_ = class414.method6972(string, 100, 0, null, 1290121798);
			i_332_ -= i_334_ / 2;
			if (i_332_ >= -(class118.anInt1301 * 1506818197) && i_332_ <= class118.anInt1301 * 1506818197 && i_333_ >= -(-492594917 * class118.anInt1429) && i_333_ <= class118.anInt1429 * -492594917)
				class8.method378(string, 1506818197 * class118.anInt1301 / 2 + (i_332_ + i), (-492594917 * class118.anInt1429 / 2 + i_322_ - i_333_ - i_325_ - i_335_), i_334_, 50, i_326_, 0, 1, 0, 0, null, null, class455, i, i_322_, (byte) 105);
		}
	}

	static boolean method3116(Interface12 interface12) {
		Class478 class478 = client.aClass257_7353.method4436(-1628169360).method7891(interface12.method84(669412535), 65280);
		if (-272332433 * class478.anInt5689 == -1)
			return true;
		Class418 class418 = Class97.aClass427_995.method7172(-272332433 * class478.anInt5689, -1014703371);
		if (-1 == -1053123675 * class418.anInt4995)
			return true;
		return class418.method7015(65280);
	}

	static final void method3117(Class527 class527, byte i) {
		int i_336_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_336_, (byte) 83);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_336_ >> 16];
		Class282_Sub25.method12401(class118, class98, class527, 2018146774);
	}

	public static boolean method3118(byte i) {
		return Class404.anInterface36_4830 != null;
	}

	static final void method3119(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class9.anInt107 * -1951489731;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = -79406355 * Class9.anInt109;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = -532543837 * Class9.anInt108;
		Class9.anInt107 = 264209366;
		Class9.anInt109 = -915280613;
		Class9.anInt108 = 691675893;
	}

	static final void method3120(Class527 class527, byte i) {
		Class86.method1478(-2031012113);
	}

	public static void method3121(int i, short i_337_) {
		Class148.aClass282_Sub15_Sub2_1735.method15094(i >> 8, 1679991202);
	}
}
