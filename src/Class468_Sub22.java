/* Class468_Sub22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class468_Sub22 extends Class468 {
	public static final int anInt7926 = 0;
	public static final int anInt7927 = 1;

	public int method12870() {
		return anInt5578 * -859024475;
	}

	int method7787() {
		return 1;
	}

	public void method12871(byte i) {
		if (aClass282_Sub54_5581.method13514((byte) 28) != Class486.aClass486_5744)
			anInt5578 = -754033619;
		if (0 != -859024475 * anInt5578 && 1 != anInt5578 * -859024475)
			anInt5578 = method7781(2131611866) * -754033619;
	}

	int method7781(int i) {
		return 1;
	}

	public boolean method12872(int i) {
		if (aClass282_Sub54_5581.method13514((byte) 119) == Class486.aClass486_5744)
			return true;
		return false;
	}

	public int method7785(int i, int i_0_) {
		if (aClass282_Sub54_5581.method13514((byte) 66) == Class486.aClass486_5744) {
			if (i == 0 || aClass282_Sub54_5581.aClass468_Sub17_8200.method12762(288309414) == 1)
				return 1;
			return 2;
		}
		return 3;
	}

	public int method12873(int i) {
		return anInt5578 * -859024475;
	}

	int method7786() {
		return 1;
	}

	public int method7784(int i) {
		if (aClass282_Sub54_5581.method13514((byte) 72) == Class486.aClass486_5744) {
			if (i == 0 || aClass282_Sub54_5581.aClass468_Sub17_8200.method12762(-925093981) == 1)
				return 1;
			return 2;
		}
		return 3;
	}

	void method7780(int i) {
		anInt5578 = -754033619 * i;
	}

	public Class468_Sub22(Class282_Sub54 class282_sub54) {
		super(class282_sub54);
	}

	void method7783(int i, int i_1_) {
		anInt5578 = -754033619 * i;
	}

	public void method12874() {
		if (aClass282_Sub54_5581.method13514((byte) 68) != Class486.aClass486_5744)
			anInt5578 = -754033619;
		if (0 != -859024475 * anInt5578 && 1 != anInt5578 * -859024475)
			anInt5578 = method7781(1923068196) * -754033619;
	}

	public boolean method12875() {
		if (aClass282_Sub54_5581.method13514((byte) 114) == Class486.aClass486_5744)
			return true;
		return false;
	}

	public boolean method12876() {
		if (aClass282_Sub54_5581.method13514((byte) 60) == Class486.aClass486_5744)
			return true;
		return false;
	}

	public boolean method12877() {
		if (aClass282_Sub54_5581.method13514((byte) 63) == Class486.aClass486_5744)
			return true;
		return false;
	}

	public boolean method12878() {
		if (aClass282_Sub54_5581.method13514((byte) 27) == Class486.aClass486_5744)
			return true;
		return false;
	}

	public int method12879() {
		return anInt5578 * -859024475;
	}

	public Class468_Sub22(int i, Class282_Sub54 class282_sub54) {
		super(i, class282_sub54);
	}

	static Class118 method12880(Class118 class118, byte i) {
		Class118 class118_2_ = client.method11634(class118);
		if (class118_2_ == null)
			class118_2_ = class118.aClass118_1379;
		return class118_2_;
	}
}
