/* Class521_Sub1_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class521_Sub1_Sub3 extends Class521_Sub1 {
	public short aShort9561;

	int method13036(Class282_Sub24[] class282_sub24s, int i) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), class282_sub24s, 868144561);
	}

	boolean method13030(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3271(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592)));
	}

	boolean method13037(Class505 class505, int i) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3271(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592)));
	}

	boolean method13029(byte i) {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - -527863643 * ((Class206) aClass206_7970).anInt2628 + ((Class206) aClass206_7970).anInt2652 * 1459994833)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}

	int method13024(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), class282_sub24s, 868144561);
	}

	int method13025(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), class282_sub24s, 868144561);
	}

	Class521_Sub1_Sub3(Class206 class206, int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		super(class206);
		aByte7967 = (byte) i_2_;
		aByte7968 = (byte) i_3_;
		aShort9561 = (short) i_4_;
		method11171(new Class385((float) i, (float) i_0_, (float) i_1_));
	}

	int method12982(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), class282_sub24s, 868144561);
	}

	boolean method13022(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3271(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592)));
	}

	boolean method13033() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - -527863643 * ((Class206) aClass206_7970).anInt2628 + ((Class206) aClass206_7970).anInt2652 * 1459994833)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}

	int method13031(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293), class282_sub24s, 868144561);
	}

	boolean method12998(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3271(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592)));
	}

	boolean method13032() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - -527863643 * ((Class206) aClass206_7970).anInt2628 + ((Class206) aClass206_7970).anInt2652 * 1459994833)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}

	boolean method12988(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		return (aClass206_7970.aClass201_2600.method3271(aByte7968, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592)));
	}

	boolean method13034() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - -527863643 * ((Class206) aClass206_7970).anInt2628 + ((Class206) aClass206_7970).anInt2652 * 1459994833)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - 1580412859 * ((Class206) aClass206_7970).anInt2629))]);
	}
}
