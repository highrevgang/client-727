/* Class282_Sub15_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class282_Sub15_Sub5 extends Class282_Sub15 {
	int anInt9844;
	int anInt9845;
	int anInt9846;
	int anInt9847;
	int anInt9848;
	int anInt9849;
	int anInt9850;
	int anInt9851;
	int anInt9852;
	int anInt9853;
	int anInt9854;
	int anInt9855;
	int anInt9856;
	boolean aBool9857;
	int anInt9858;
	static int anInt9859;

	public final synchronized void method15308(int i, short i_0_) {
		if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0)
			((Class282_Sub15_Sub5) this).anInt9855 = -i * 1927994969;
		else
			((Class282_Sub15_Sub5) this).anInt9855 = i * 1927994969;
	}

	public final synchronized void method15309(int i) {
		method15364(method15315(-1647613913), i, 16711935);
	}

	public final synchronized void method15310(int i, int i_1_, int i_2_) {
		if (i == 0)
			method15364(i_1_, i_2_, 16711935);
		else {
			int i_3_ = Class12.method488(i_1_, i_2_, -1821383421);
			int i_4_ = Class282_Sub20_Sub24.method15390(i_1_, i_2_, 1305108722);
			if (i_3_ == ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 && 2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 == i_4_)
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
			else {
				int i_5_ = (i_1_ - -217803069 * ((Class282_Sub15_Sub5) this).anInt9848);
				if (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_1_ > i_5_)
					i_5_ = (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_1_);
				if (i_3_ - 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_5_)
					i_5_ = i_3_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117);
				if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_3_ > i_5_)
					i_5_ = (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_3_);
				if (i_4_ - ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 > i_5_)
					i_5_ = i_4_ - (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173);
				if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 - i_4_ > i_5_)
					i_5_ = (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 - i_4_);
				if (i > i_5_)
					i = i_5_;
				((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * i;
				((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i_1_;
				((Class282_Sub15_Sub5) this).anInt9847 = i_2_ * 375820671;
				((Class282_Sub15_Sub5) this).anInt9849 = (-1756138925 * ((i_1_ - (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848)) / i));
				((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * ((i_3_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117)) / i);
				((Class282_Sub15_Sub5) this).anInt9851 = (-1589776061 * ((i_4_ - (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850)) / i));
			}
		}
	}

	public final synchronized boolean method15311(byte i) {
		return 0 != ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447;
	}

	public abstract void method12241(int[] is, int i, int i_6_);

	public final synchronized void method15312(int i, byte i_7_) {
		method15364(i << 6, method15316(1349583884), 16711935);
	}

	public final synchronized void method15313(boolean bool, int i) {
		((Class282_Sub15_Sub5) this).anInt9855 = (1927994969 * ((-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 ^ ((Class282_Sub15_Sub5) this).anInt9855 * -177305111 >> 31) + (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 >>> 31)));
		if (bool)
			((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
	}

	static final int method15314(int i, int i_8_) {
		return (i_8_ < 0 ? -i : (int) ((double) i * Math.sqrt(1.220703125E-4 * (double) i_8_) + 0.5));
	}

	public final synchronized void method12234(int i) {
		if (1008956447 * ((Class282_Sub15_Sub5) this).anInt9858 > 0) {
			if (i >= 1008956447 * ((Class282_Sub15_Sub5) this).anInt9858) {
				if (-2147483648 == ((Class282_Sub15_Sub5) this).anInt9846 * -1302038045) {
					((Class282_Sub15_Sub5) this).anInt9846 = 0;
					((Class282_Sub15_Sub5) this).anInt9850 = 0;
					((Class282_Sub15_Sub5) this).anInt9845 = 0;
					((Class282_Sub15_Sub5) this).anInt9848 = 0;
					method4991(-371378792);
					i = ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447;
				}
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
				method15332((byte) -7);
			} else {
				((Class282_Sub15_Sub5) this).anInt9848 += (809781739 * (i * (1670226395 * ((Class282_Sub15_Sub5) this).anInt9849)));
				((Class282_Sub15_Sub5) this).anInt9845 += 1672655349 * (((Class282_Sub15_Sub5) this).anInt9852 * 1923898617 * i);
				((Class282_Sub15_Sub5) this).anInt9850 += 1339372733 * (((Class282_Sub15_Sub5) this).anInt9851 * 241181547 * i);
				((Class282_Sub15_Sub5) this).anInt9858 -= i * 1156515807;
			}
		}
		int i_9_ = ((Class282_Sub15_Sub5) this).anInt9844 * -99794975 << 8;
		int i_10_ = ((Class282_Sub15_Sub5) this).anInt9856 * -1048610041 << 8;
		int i_11_ = (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8);
		int i_12_ = i_10_ - i_9_;
		if (i_12_ <= 0)
			((Class282_Sub15_Sub5) this).anInt9854 = 0;
		if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 < 0) {
			if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 > 0)
				((Class282_Sub15_Sub5) this).anInt9853 = 0;
			else {
				method15354(-859024475);
				method4991(-371378792);
				return;
			}
		}
		if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_11_) {
			if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0)
				((Class282_Sub15_Sub5) this).anInt9853 = (i_11_ - 1) * -834443097;
			else {
				method15354(-859024475);
				method4991(-371378792);
				return;
			}
		}
		((Class282_Sub15_Sub5) this).anInt9853 += -834443097 * (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 * i);
		if (1507503163 * ((Class282_Sub15_Sub5) this).anInt9854 < 0) {
			if (((Class282_Sub15_Sub5) this).aBool9857) {
				if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 < 0) {
					if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_9_)
						return;
					((Class282_Sub15_Sub5) this).anInt9853 = (i_9_ + i_9_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) * -834443097;
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
				}
				while (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 >= i_10_) {
					((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * (i_10_ + i_10_ - 1 - -354697449 * ((Class282_Sub15_Sub5) this).anInt9853);
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
					if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_9_)
						break;
					((Class282_Sub15_Sub5) this).anInt9853 = (i_9_ + i_9_ - 1 - -354697449 * (((Class282_Sub15_Sub5) this).anInt9853)) * -834443097;
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
				}
			} else if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 < 0) {
				if (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 < i_9_)
					((Class282_Sub15_Sub5) this).anInt9853 = (-834443097 * (i_10_ - 1 - (i_10_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) % i_12_));
			} else if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_10_)
				((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * (i_9_ + (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 - i_9_) % i_12_);
		} else {
			do {
				if (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163 > 0) {
					if (((Class282_Sub15_Sub5) this).aBool9857) {
						if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
							if ((((Class282_Sub15_Sub5) this).anInt9853 * -354697449) >= i_9_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = (i_9_ + i_9_ - 1 - -354697449 * (((Class282_Sub15_Sub5) this).anInt9853)) * -834443097;
							((Class282_Sub15_Sub5) this).anInt9855 = -(((Class282_Sub15_Sub5) this).anInt9855 * 1);
							if ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 == 0)
								break;
						}
						do {
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) < i_10_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = (i_10_ + i_10_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) * -834443097;
							((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
							if ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 == 0)
								break;
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) >= i_9_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * (i_9_ + i_9_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853) * -354697449);
							((Class282_Sub15_Sub5) this).anInt9855 = -(((Class282_Sub15_Sub5) this).anInt9855 * 1);
						} while ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 != 0);
					} else {
						if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) < i_9_) {
								int i_13_ = ((i_10_ - 1 - -354697449 * ((Class282_Sub15_Sub5) this).anInt9853) / i_12_);
								if (i_13_ >= (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163)) {
									((Class282_Sub15_Sub5) this).anInt9853 += (-834443097 * (1507503163 * (((Class282_Sub15_Sub5) this).anInt9854) * i_12_));
									((Class282_Sub15_Sub5) this).anInt9854 = 0;
									break;
								}
								((Class282_Sub15_Sub5) this).anInt9853 += -834443097 * (i_12_ * i_13_);
								((Class282_Sub15_Sub5) this).anInt9854 -= i_13_ * -485012237;
							}
						} else if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) >= i_10_) {
							int i_14_ = (((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) - i_9_) / i_12_);
							if (i_14_ >= 1507503163 * (((Class282_Sub15_Sub5) this).anInt9854)) {
								((Class282_Sub15_Sub5) this).anInt9853 -= -834443097 * (i_12_ * (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163));
								((Class282_Sub15_Sub5) this).anInt9854 = 0;
								break;
							}
							((Class282_Sub15_Sub5) this).anInt9853 -= -834443097 * (i_12_ * i_14_);
							((Class282_Sub15_Sub5) this).anInt9854 -= i_14_ * -485012237;
						}
						return;
					}
				}
			} while (false);
			if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
				if (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 < 0) {
					((Class282_Sub15_Sub5) this).anInt9853 = 834443097;
					method15354(-859024475);
					method4991(-371378792);
				}
			} else if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_11_) {
				((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * i_11_;
				method15354(-859024475);
				method4991(-371378792);
			}
		}
	}

	public final synchronized int method15315(int i) {
		return ((-2147483648 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846) ? 0 : -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846);
	}

	public final synchronized int method15316(int i) {
		return (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847 < 0 ? -1 : ((Class282_Sub15_Sub5) this).anInt9847 * -980996481);
	}

	public final synchronized void method15317(int i, int i_15_) {
		int i_16_ = (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8);
		if (i < -1)
			i = -1;
		if (i > i_16_)
			i = i_16_;
		((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * i;
	}

	final void method15318() {
		((Class282_Sub15_Sub5) this).anInt9848 = ((Class282_Sub15_Sub5) this).anInt9846 * 1497996897;
		((Class282_Sub15_Sub5) this).anInt9845 = Class12.method488((-1302038045 * ((Class282_Sub15_Sub5) this).anInt9846), (((Class282_Sub15_Sub5) this).anInt9847 * -980996481), -2111177742) * 1672655349;
		((Class282_Sub15_Sub5) this).anInt9850 = (Class282_Sub20_Sub24.method15390(-1302038045 * ((Class282_Sub15_Sub5) this).anInt9846, -980996481 * ((Class282_Sub15_Sub5) this).anInt9847, 707617134)) * 1339372733;
	}

	public final synchronized boolean method15319(byte i) {
		return (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 < 0 || (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= ((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8));
	}

	final Class282_Sub15 method12235() {
		return null;
	}

	public final synchronized void method15320(int i, int i_17_, int i_18_) {
		method15321(i, i_17_, method15316(-879511518), 43881265);
	}

	public final synchronized void method15321(int i, int i_19_, int i_20_, int i_21_) {
		if (i == 0)
			method15364(i_19_, i_20_, 16711935);
		else {
			int i_22_ = Class12.method488(i_19_, i_20_, -1863158316);
			int i_23_ = Class282_Sub20_Sub24.method15390(i_19_, i_20_, 768902996);
			if (i_22_ == ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 && (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 == i_23_))
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
			else {
				int i_24_ = (i_19_ - -217803069 * ((Class282_Sub15_Sub5) this).anInt9848);
				if (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_19_ > i_24_)
					i_24_ = (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_19_);
				if (i_22_ - 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_24_)
					i_24_ = i_22_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117);
				if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_22_ > i_24_)
					i_24_ = (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_22_);
				if (i_23_ - ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 > i_24_)
					i_24_ = i_23_ - (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173);
				if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 - i_23_ > i_24_)
					i_24_ = (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 - i_23_);
				if (i > i_24_)
					i = i_24_;
				((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * i;
				((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i_19_;
				((Class282_Sub15_Sub5) this).anInt9847 = i_20_ * 375820671;
				((Class282_Sub15_Sub5) this).anInt9849 = (-1756138925 * ((i_19_ - (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848)) / i));
				((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * ((i_22_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117)) / i);
				((Class282_Sub15_Sub5) this).anInt9851 = (-1589776061 * ((i_23_ - (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850)) / i));
			}
		}
	}

	public final synchronized void method15322(int i, byte i_25_) {
		if (0 == i) {
			method15326(0, (byte) -36);
			method4991(-371378792);
		} else if (0 == 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 && 0 == (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173)) {
			((Class282_Sub15_Sub5) this).anInt9858 = 0;
			((Class282_Sub15_Sub5) this).anInt9846 = 0;
			((Class282_Sub15_Sub5) this).anInt9848 = 0;
			method4991(-371378792);
		} else {
			int i_26_ = -(((Class282_Sub15_Sub5) this).anInt9848 * -217803069);
			if (((Class282_Sub15_Sub5) this).anInt9848 * -217803069 > i_26_)
				i_26_ = ((Class282_Sub15_Sub5) this).anInt9848 * -217803069;
			if (-(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845) > i_26_)
				i_26_ = -(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845);
			if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_26_)
				i_26_ = ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117;
			if (-(((Class282_Sub15_Sub5) this).anInt9850 * 2085185173) > i_26_)
				i_26_ = -(2085185173 * ((Class282_Sub15_Sub5) this).anInt9850);
			if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 > i_26_)
				i_26_ = ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173;
			if (i > i_26_)
				i = i_26_;
			((Class282_Sub15_Sub5) this).anInt9858 = i * 1156515807;
			((Class282_Sub15_Sub5) this).anInt9846 = -2147483648;
			((Class282_Sub15_Sub5) this).anInt9849 = (-(((Class282_Sub15_Sub5) this).anInt9848 * -217803069) / i * -1756138925);
			((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * (-(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845) / i);
			((Class282_Sub15_Sub5) this).anInt9851 = (-(2085185173 * ((Class282_Sub15_Sub5) this).anInt9850) / i * -1589776061);
		}
	}

	final int method12238() {
		if (0 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 && ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0)
			return 0;
		return 1;
	}

	public final synchronized int method15323(int i) {
		return (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0 ? -(-177305111 * ((Class282_Sub15_Sub5) this).anInt9855) : -177305111 * ((Class282_Sub15_Sub5) this).anInt9855);
	}

	final int method12228() {
		if (0 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 && ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0)
			return 0;
		return 1;
	}

	final boolean method15324(byte i) {
		int i_27_ = -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846;
		int i_28_;
		int i_29_;
		if (i_27_ == -2147483648) {
			i_29_ = 0;
			i_28_ = 0;
			i_27_ = 0;
		} else {
			i_28_ = Class12.method488(i_27_, (((Class282_Sub15_Sub5) this).anInt9847 * -980996481), -1833618815);
			i_29_ = Class282_Sub20_Sub24.method15390(i_27_, (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847), 1570261179);
		}
		if (i_27_ != ((Class282_Sub15_Sub5) this).anInt9848 * -217803069 || i_28_ != ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 || i_29_ != ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173) {
			if (((Class282_Sub15_Sub5) this).anInt9848 * -217803069 < i_27_) {
				((Class282_Sub15_Sub5) this).anInt9849 = -1756138925;
				((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * (i_27_ - (((Class282_Sub15_Sub5) this).anInt9848 * -217803069));
			} else if (((Class282_Sub15_Sub5) this).anInt9848 * -217803069 > i_27_) {
				((Class282_Sub15_Sub5) this).anInt9849 = 1756138925;
				((Class282_Sub15_Sub5) this).anInt9858 = (1156515807 * (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_27_));
			} else
				((Class282_Sub15_Sub5) this).anInt9849 = 0;
			if (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 < i_28_) {
				((Class282_Sub15_Sub5) this).anInt9852 = -661281463;
				if (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0 || (1008956447 * ((Class282_Sub15_Sub5) this).anInt9858 > i_28_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117)))
					((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * (i_28_ - 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845);
			} else if (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 > i_28_) {
				((Class282_Sub15_Sub5) this).anInt9852 = 661281463;
				if (0 == 1008956447 * ((Class282_Sub15_Sub5) this).anInt9858 || (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 > (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_28_)))
					((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * ((((Class282_Sub15_Sub5) this).anInt9845 * 1784001117) - i_28_);
			} else
				((Class282_Sub15_Sub5) this).anInt9852 = 0;
			if (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 < i_29_) {
				((Class282_Sub15_Sub5) this).anInt9851 = -1589776061;
				if (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0 || (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 > i_29_ - (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173)))
					((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * (i_29_ - (((Class282_Sub15_Sub5) this).anInt9850) * 2085185173);
			} else if (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 > i_29_) {
				((Class282_Sub15_Sub5) this).anInt9851 = 1589776061;
				if (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0 || (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 > (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 - i_29_)))
					((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * ((((Class282_Sub15_Sub5) this).anInt9850 * 2085185173) - i_29_);
			} else
				((Class282_Sub15_Sub5) this).anInt9851 = 0;
			return false;
		}
		if (-2147483648 == ((Class282_Sub15_Sub5) this).anInt9846 * -1302038045) {
			((Class282_Sub15_Sub5) this).anInt9846 = 0;
			((Class282_Sub15_Sub5) this).anInt9850 = 0;
			((Class282_Sub15_Sub5) this).anInt9845 = 0;
			((Class282_Sub15_Sub5) this).anInt9848 = 0;
			method4991(-371378792);
			return true;
		}
		method15332((byte) -51);
		return false;
	}

	public final synchronized void method12231(int i) {
		if (1008956447 * ((Class282_Sub15_Sub5) this).anInt9858 > 0) {
			if (i >= 1008956447 * ((Class282_Sub15_Sub5) this).anInt9858) {
				if (-2147483648 == ((Class282_Sub15_Sub5) this).anInt9846 * -1302038045) {
					((Class282_Sub15_Sub5) this).anInt9846 = 0;
					((Class282_Sub15_Sub5) this).anInt9850 = 0;
					((Class282_Sub15_Sub5) this).anInt9845 = 0;
					((Class282_Sub15_Sub5) this).anInt9848 = 0;
					method4991(-371378792);
					i = ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447;
				}
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
				method15332((byte) -60);
			} else {
				((Class282_Sub15_Sub5) this).anInt9848 += (809781739 * (i * (1670226395 * ((Class282_Sub15_Sub5) this).anInt9849)));
				((Class282_Sub15_Sub5) this).anInt9845 += 1672655349 * (((Class282_Sub15_Sub5) this).anInt9852 * 1923898617 * i);
				((Class282_Sub15_Sub5) this).anInt9850 += 1339372733 * (((Class282_Sub15_Sub5) this).anInt9851 * 241181547 * i);
				((Class282_Sub15_Sub5) this).anInt9858 -= i * 1156515807;
			}
		}
		int i_30_ = ((Class282_Sub15_Sub5) this).anInt9844 * -99794975 << 8;
		int i_31_ = ((Class282_Sub15_Sub5) this).anInt9856 * -1048610041 << 8;
		int i_32_ = (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8);
		int i_33_ = i_31_ - i_30_;
		if (i_33_ <= 0)
			((Class282_Sub15_Sub5) this).anInt9854 = 0;
		if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 < 0) {
			if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 > 0)
				((Class282_Sub15_Sub5) this).anInt9853 = 0;
			else {
				method15354(-859024475);
				method4991(-371378792);
				return;
			}
		}
		if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_32_) {
			if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0)
				((Class282_Sub15_Sub5) this).anInt9853 = (i_32_ - 1) * -834443097;
			else {
				method15354(-859024475);
				method4991(-371378792);
				return;
			}
		}
		((Class282_Sub15_Sub5) this).anInt9853 += -834443097 * (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 * i);
		if (1507503163 * ((Class282_Sub15_Sub5) this).anInt9854 < 0) {
			if (((Class282_Sub15_Sub5) this).aBool9857) {
				if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 < 0) {
					if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_30_)
						return;
					((Class282_Sub15_Sub5) this).anInt9853 = (i_30_ + i_30_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) * -834443097;
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
				}
				while (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 >= i_31_) {
					((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * (i_31_ + i_31_ - 1 - -354697449 * ((Class282_Sub15_Sub5) this).anInt9853);
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
					if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_30_)
						break;
					((Class282_Sub15_Sub5) this).anInt9853 = (i_30_ + i_30_ - 1 - -354697449 * (((Class282_Sub15_Sub5) this).anInt9853)) * -834443097;
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
				}
			} else if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 < 0) {
				if (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 < i_30_)
					((Class282_Sub15_Sub5) this).anInt9853 = (-834443097 * (i_31_ - 1 - (i_31_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) % i_33_));
			} else if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_31_)
				((Class282_Sub15_Sub5) this).anInt9853 = (-834443097 * (i_30_ + ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) - i_30_) % i_33_));
		} else {
			do {
				if (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163 > 0) {
					if (((Class282_Sub15_Sub5) this).aBool9857) {
						if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
							if ((((Class282_Sub15_Sub5) this).anInt9853 * -354697449) >= i_30_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = (i_30_ + i_30_ - 1 - -354697449 * (((Class282_Sub15_Sub5) this).anInt9853)) * -834443097;
							((Class282_Sub15_Sub5) this).anInt9855 = -(((Class282_Sub15_Sub5) this).anInt9855 * 1);
							if ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 == 0)
								break;
						}
						do {
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) < i_31_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = (i_31_ + i_31_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) * -834443097;
							((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
							if ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 == 0)
								break;
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) >= i_30_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * (i_30_ + i_30_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853) * -354697449);
							((Class282_Sub15_Sub5) this).anInt9855 = -(((Class282_Sub15_Sub5) this).anInt9855 * 1);
						} while ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 != 0);
					} else {
						if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) < i_30_) {
								int i_34_ = ((i_31_ - 1 - -354697449 * ((Class282_Sub15_Sub5) this).anInt9853) / i_33_);
								if (i_34_ >= (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163)) {
									((Class282_Sub15_Sub5) this).anInt9853 += (-834443097 * (1507503163 * (((Class282_Sub15_Sub5) this).anInt9854) * i_33_));
									((Class282_Sub15_Sub5) this).anInt9854 = 0;
									break;
								}
								((Class282_Sub15_Sub5) this).anInt9853 += -834443097 * (i_33_ * i_34_);
								((Class282_Sub15_Sub5) this).anInt9854 -= i_34_ * -485012237;
							}
						} else if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) >= i_31_) {
							int i_35_ = (((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) - i_30_) / i_33_);
							if (i_35_ >= 1507503163 * (((Class282_Sub15_Sub5) this).anInt9854)) {
								((Class282_Sub15_Sub5) this).anInt9853 -= -834443097 * (i_33_ * (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163));
								((Class282_Sub15_Sub5) this).anInt9854 = 0;
								break;
							}
							((Class282_Sub15_Sub5) this).anInt9853 -= -834443097 * (i_33_ * i_35_);
							((Class282_Sub15_Sub5) this).anInt9854 -= i_35_ * -485012237;
						}
						return;
					}
				}
			} while (false);
			if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
				if (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 < 0) {
					((Class282_Sub15_Sub5) this).anInt9853 = 834443097;
					method15354(-859024475);
					method4991(-371378792);
				}
			} else if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_32_) {
				((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * i_32_;
				method15354(-859024475);
				method4991(-371378792);
			}
		}
	}

	final Class282_Sub15 method12226() {
		return null;
	}

	public final synchronized void method15325(int i, byte i_36_) {
		((Class282_Sub15_Sub5) this).anInt9854 = -485012237 * i;
	}

	final synchronized void method15326(int i, byte i_37_) {
		method15364(i, method15316(-934939985), 16711935);
	}

	final Class282_Sub15 method12232() {
		return null;
	}

	public final synchronized int method15327() {
		return (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0 ? -(-177305111 * ((Class282_Sub15_Sub5) this).anInt9855) : -177305111 * ((Class282_Sub15_Sub5) this).anInt9855);
	}

	public final synchronized void method12243(int i) {
		if (1008956447 * ((Class282_Sub15_Sub5) this).anInt9858 > 0) {
			if (i >= 1008956447 * ((Class282_Sub15_Sub5) this).anInt9858) {
				if (-2147483648 == ((Class282_Sub15_Sub5) this).anInt9846 * -1302038045) {
					((Class282_Sub15_Sub5) this).anInt9846 = 0;
					((Class282_Sub15_Sub5) this).anInt9850 = 0;
					((Class282_Sub15_Sub5) this).anInt9845 = 0;
					((Class282_Sub15_Sub5) this).anInt9848 = 0;
					method4991(-371378792);
					i = ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447;
				}
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
				method15332((byte) -8);
			} else {
				((Class282_Sub15_Sub5) this).anInt9848 += (809781739 * (i * (1670226395 * ((Class282_Sub15_Sub5) this).anInt9849)));
				((Class282_Sub15_Sub5) this).anInt9845 += 1672655349 * (((Class282_Sub15_Sub5) this).anInt9852 * 1923898617 * i);
				((Class282_Sub15_Sub5) this).anInt9850 += 1339372733 * (((Class282_Sub15_Sub5) this).anInt9851 * 241181547 * i);
				((Class282_Sub15_Sub5) this).anInt9858 -= i * 1156515807;
			}
		}
		int i_38_ = ((Class282_Sub15_Sub5) this).anInt9844 * -99794975 << 8;
		int i_39_ = ((Class282_Sub15_Sub5) this).anInt9856 * -1048610041 << 8;
		int i_40_ = (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8);
		int i_41_ = i_39_ - i_38_;
		if (i_41_ <= 0)
			((Class282_Sub15_Sub5) this).anInt9854 = 0;
		if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 < 0) {
			if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 > 0)
				((Class282_Sub15_Sub5) this).anInt9853 = 0;
			else {
				method15354(-859024475);
				method4991(-371378792);
				return;
			}
		}
		if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_40_) {
			if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0)
				((Class282_Sub15_Sub5) this).anInt9853 = (i_40_ - 1) * -834443097;
			else {
				method15354(-859024475);
				method4991(-371378792);
				return;
			}
		}
		((Class282_Sub15_Sub5) this).anInt9853 += -834443097 * (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 * i);
		if (1507503163 * ((Class282_Sub15_Sub5) this).anInt9854 < 0) {
			if (((Class282_Sub15_Sub5) this).aBool9857) {
				if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 < 0) {
					if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_38_)
						return;
					((Class282_Sub15_Sub5) this).anInt9853 = (i_38_ + i_38_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) * -834443097;
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
				}
				while (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 >= i_39_) {
					((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * (i_39_ + i_39_ - 1 - -354697449 * ((Class282_Sub15_Sub5) this).anInt9853);
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
					if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_38_)
						break;
					((Class282_Sub15_Sub5) this).anInt9853 = (i_38_ + i_38_ - 1 - -354697449 * (((Class282_Sub15_Sub5) this).anInt9853)) * -834443097;
					((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
				}
			} else if (((Class282_Sub15_Sub5) this).anInt9855 * -177305111 < 0) {
				if (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 < i_38_)
					((Class282_Sub15_Sub5) this).anInt9853 = (-834443097 * (i_39_ - 1 - (i_39_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) % i_41_));
			} else if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_39_)
				((Class282_Sub15_Sub5) this).anInt9853 = (-834443097 * (i_38_ + ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) - i_38_) % i_41_));
		} else {
			do {
				if (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163 > 0) {
					if (((Class282_Sub15_Sub5) this).aBool9857) {
						if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
							if ((((Class282_Sub15_Sub5) this).anInt9853 * -354697449) >= i_38_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = (i_38_ + i_38_ - 1 - -354697449 * (((Class282_Sub15_Sub5) this).anInt9853)) * -834443097;
							((Class282_Sub15_Sub5) this).anInt9855 = -(((Class282_Sub15_Sub5) this).anInt9855 * 1);
							if ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 == 0)
								break;
						}
						do {
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) < i_39_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = (i_39_ + i_39_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853 * -354697449)) * -834443097;
							((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
							if ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 == 0)
								break;
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) >= i_38_)
								return;
							((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * (i_38_ + i_38_ - 1 - (((Class282_Sub15_Sub5) this).anInt9853) * -354697449);
							((Class282_Sub15_Sub5) this).anInt9855 = -(((Class282_Sub15_Sub5) this).anInt9855 * 1);
						} while ((((Class282_Sub15_Sub5) this).anInt9854 -= -485012237) * 1507503163 != 0);
					} else {
						if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
							if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) < i_38_) {
								int i_42_ = ((i_39_ - 1 - -354697449 * ((Class282_Sub15_Sub5) this).anInt9853) / i_41_);
								if (i_42_ >= (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163)) {
									((Class282_Sub15_Sub5) this).anInt9853 += (-834443097 * (1507503163 * (((Class282_Sub15_Sub5) this).anInt9854) * i_41_));
									((Class282_Sub15_Sub5) this).anInt9854 = 0;
									break;
								}
								((Class282_Sub15_Sub5) this).anInt9853 += -834443097 * (i_41_ * i_42_);
								((Class282_Sub15_Sub5) this).anInt9854 -= i_42_ * -485012237;
							}
						} else if ((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) >= i_39_) {
							int i_43_ = (((-354697449 * ((Class282_Sub15_Sub5) this).anInt9853) - i_38_) / i_41_);
							if (i_43_ >= 1507503163 * (((Class282_Sub15_Sub5) this).anInt9854)) {
								((Class282_Sub15_Sub5) this).anInt9853 -= -834443097 * (i_41_ * (((Class282_Sub15_Sub5) this).anInt9854 * 1507503163));
								((Class282_Sub15_Sub5) this).anInt9854 = 0;
								break;
							}
							((Class282_Sub15_Sub5) this).anInt9853 -= -834443097 * (i_41_ * i_43_);
							((Class282_Sub15_Sub5) this).anInt9854 -= i_43_ * -485012237;
						}
						return;
					}
				}
			} while (false);
			if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0) {
				if (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 < 0) {
					((Class282_Sub15_Sub5) this).anInt9853 = 834443097;
					method15354(-859024475);
					method4991(-371378792);
				}
			} else if (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= i_40_) {
				((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * i_40_;
				method15354(-859024475);
				method4991(-371378792);
			}
		}
	}

	public final synchronized void method15328(int i) {
		method15364(method15315(-913313750), i, 16711935);
	}

	public final synchronized int method15329() {
		return ((-2147483648 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846) ? 0 : -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846);
	}

	final Class282_Sub15 method12229() {
		return null;
	}

	final int method12224() {
		if (0 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 && ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0)
			return 0;
		return 1;
	}

	public final synchronized void method15330(int i) {
		int i_44_ = (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8);
		if (i < -1)
			i = -1;
		if (i > i_44_)
			i = i_44_;
		((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * i;
	}

	final int method12244() {
		if (0 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 && ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0)
			return 0;
		return 1;
	}

	public abstract void method12240(int[] is, int i, int i_45_);

	public final synchronized void method15331(int i) {
		int i_46_ = (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8);
		if (i < -1)
			i = -1;
		if (i > i_46_)
			i = i_46_;
		((Class282_Sub15_Sub5) this).anInt9853 = -834443097 * i;
	}

	public abstract void method12242(int[] is, int i, int i_47_);

	final void method15332(byte i) {
		((Class282_Sub15_Sub5) this).anInt9848 = ((Class282_Sub15_Sub5) this).anInt9846 * 1497996897;
		((Class282_Sub15_Sub5) this).anInt9845 = Class12.method488((-1302038045 * ((Class282_Sub15_Sub5) this).anInt9846), (((Class282_Sub15_Sub5) this).anInt9847 * -980996481), -1884211393) * 1672655349;
		((Class282_Sub15_Sub5) this).anInt9850 = (Class282_Sub20_Sub24.method15390(-1302038045 * ((Class282_Sub15_Sub5) this).anInt9846, -980996481 * ((Class282_Sub15_Sub5) this).anInt9847, 1768966833)) * 1339372733;
	}

	final Class282_Sub15 method12233() {
		return null;
	}

	final int method12245() {
		int i = ((Class282_Sub15_Sub5) this).anInt9848 * -653409207 >> 6;
		i = (i ^ i >> 31) + (i >>> 31);
		if (0 == ((Class282_Sub15_Sub5) this).anInt9854 * 1507503163)
			i -= (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 * i / (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8));
		else if (1507503163 * ((Class282_Sub15_Sub5) this).anInt9854 >= 0)
			i -= (i * (-99794975 * ((Class282_Sub15_Sub5) this).anInt9844) / ((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223());
		return i > 255 ? 255 : i;
	}

	final int method12246() {
		int i = ((Class282_Sub15_Sub5) this).anInt9848 * -653409207 >> 6;
		i = (i ^ i >> 31) + (i >>> 31);
		if (0 == ((Class282_Sub15_Sub5) this).anInt9854 * 1507503163)
			i -= (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 * i / (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8));
		else if (1507503163 * ((Class282_Sub15_Sub5) this).anInt9854 >= 0)
			i -= (i * (-99794975 * ((Class282_Sub15_Sub5) this).anInt9844) / ((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223());
		return i > 255 ? 255 : i;
	}

	final int method12247() {
		int i = ((Class282_Sub15_Sub5) this).anInt9848 * -653409207 >> 6;
		i = (i ^ i >> 31) + (i >>> 31);
		if (0 == ((Class282_Sub15_Sub5) this).anInt9854 * 1507503163)
			i -= (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 * i / (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8));
		else if (1507503163 * ((Class282_Sub15_Sub5) this).anInt9854 >= 0)
			i -= (i * (-99794975 * ((Class282_Sub15_Sub5) this).anInt9844) / ((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223());
		return i > 255 ? 255 : i;
	}

	static final int method15333(int i, int i_48_) {
		return (i_48_ < 0 ? i : (int) ((double) i * Math.sqrt(1.220703125E-4 * (double) (16384 - i_48_)) + 0.5));
	}

	static final int method15334(int i, int i_49_) {
		return i_49_ < 0 ? -i : (int) ((double) i * Math.sqrt(1.220703125E-4 * (double) i_49_) + 0.5);
	}

	final int method12225() {
		int i = ((Class282_Sub15_Sub5) this).anInt9848 * -653409207 >> 6;
		i = (i ^ i >> 31) + (i >>> 31);
		if (0 == ((Class282_Sub15_Sub5) this).anInt9854 * 1507503163)
			i -= (-354697449 * ((Class282_Sub15_Sub5) this).anInt9853 * i / (((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8));
		else if (1507503163 * ((Class282_Sub15_Sub5) this).anInt9854 >= 0)
			i -= (i * (-99794975 * ((Class282_Sub15_Sub5) this).anInt9844) / ((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223());
		return i > 255 ? 255 : i;
	}

	final Class282_Sub15 method12236() {
		return null;
	}

	public final synchronized void method15335(int i) {
		method15364(i << 6, method15316(1822924875), 16711935);
	}

	public final synchronized boolean method15336() {
		return 0 != ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447;
	}

	public final synchronized void method15337(int i) {
		((Class282_Sub15_Sub5) this).anInt9854 = -485012237 * i;
	}

	public final synchronized void method15338(int i) {
		method15364(i << 6, method15316(-24625161), 16711935);
	}

	public final synchronized void method15339(int i) {
		method15364(i << 6, method15316(-1317527472), 16711935);
	}

	public final synchronized void method15340(int i, int i_50_) {
		method15364(method15315(-1209647357), i, 16711935);
	}

	public final synchronized void method15341(int i) {
		method15364(i << 6, method15316(1226016025), 16711935);
	}

	final int method12227() {
		if (0 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 && ((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 == 0)
			return 0;
		return 1;
	}

	public abstract void method12230(int[] is, int i, int i_51_);

	final synchronized void method15342(int i, int i_52_) {
		((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i;
		((Class282_Sub15_Sub5) this).anInt9847 = 375820671 * i_52_;
		((Class282_Sub15_Sub5) this).anInt9858 = 0;
		method15332((byte) -33);
	}

	final synchronized void method15343(int i, int i_53_) {
		((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i;
		((Class282_Sub15_Sub5) this).anInt9847 = 375820671 * i_53_;
		((Class282_Sub15_Sub5) this).anInt9858 = 0;
		method15332((byte) 0);
	}

	final synchronized void method15344(int i, int i_54_) {
		((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i;
		((Class282_Sub15_Sub5) this).anInt9847 = 375820671 * i_54_;
		((Class282_Sub15_Sub5) this).anInt9858 = 0;
		method15332((byte) 3);
	}

	final Class282_Sub15 method12239() {
		return null;
	}

	public final synchronized int method15345() {
		return ((-2147483648 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846) ? 0 : -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846);
	}

	public final synchronized int method15346() {
		return ((-2147483648 == -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846) ? 0 : -1302038045 * ((Class282_Sub15_Sub5) this).anInt9846);
	}

	public final synchronized int method15347() {
		return (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847 < 0 ? -1 : ((Class282_Sub15_Sub5) this).anInt9847 * -980996481);
	}

	public final synchronized int method15348() {
		return (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847 < 0 ? -1 : ((Class282_Sub15_Sub5) this).anInt9847 * -980996481);
	}

	public final synchronized int method15349() {
		return (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847 < 0 ? -1 : ((Class282_Sub15_Sub5) this).anInt9847 * -980996481);
	}

	public final synchronized int method15350() {
		return (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847 < 0 ? -1 : ((Class282_Sub15_Sub5) this).anInt9847 * -980996481);
	}

	public final synchronized int method15351() {
		return (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847 < 0 ? -1 : ((Class282_Sub15_Sub5) this).anInt9847 * -980996481);
	}

	public final synchronized int method15352() {
		return (-980996481 * ((Class282_Sub15_Sub5) this).anInt9847 < 0 ? -1 : ((Class282_Sub15_Sub5) this).anInt9847 * -980996481);
	}

	public final synchronized void method15353(int i) {
		((Class282_Sub15_Sub5) this).anInt9854 = -485012237 * i;
	}

	final void method15354(int i) {
		if (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 != 0) {
			if (-1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 == -2147483648)
				((Class282_Sub15_Sub5) this).anInt9846 = 0;
			((Class282_Sub15_Sub5) this).anInt9858 = 0;
			method15332((byte) 11);
		}
	}

	public final synchronized void method15355(boolean bool) {
		((Class282_Sub15_Sub5) this).anInt9855 = (1927994969 * ((-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 ^ ((Class282_Sub15_Sub5) this).anInt9855 * -177305111 >> 31) + (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 >>> 31)));
		if (bool)
			((Class282_Sub15_Sub5) this).anInt9855 = -(1 * ((Class282_Sub15_Sub5) this).anInt9855);
	}

	public final synchronized boolean method15356() {
		return (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 < 0 || (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= ((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8));
	}

	public final synchronized boolean method15357() {
		return (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 < 0 || (((Class282_Sub15_Sub5) this).anInt9853 * -354697449 >= ((Class282_Sub26_Sub1) ((Class282_Sub15_Sub5) this).aClass282_Sub26_7601).method15223() << 8));
	}

	final void method15358() {
		if (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 != 0) {
			if (-1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 == -2147483648)
				((Class282_Sub15_Sub5) this).anInt9846 = 0;
			((Class282_Sub15_Sub5) this).anInt9858 = 0;
			method15332((byte) -122);
		}
	}

	final void method15359() {
		if (((Class282_Sub15_Sub5) this).anInt9858 * 1008956447 != 0) {
			if (-1302038045 * ((Class282_Sub15_Sub5) this).anInt9846 == -2147483648)
				((Class282_Sub15_Sub5) this).anInt9846 = 0;
			((Class282_Sub15_Sub5) this).anInt9858 = 0;
			method15332((byte) -109);
		}
	}

	public final synchronized void method15360(int i, int i_55_) {
		method15321(i, i_55_, method15316(-967122969), 473432342);
	}

	public final synchronized void method15361(int i, int i_56_, int i_57_) {
		if (i == 0)
			method15364(i_56_, i_57_, 16711935);
		else {
			int i_58_ = Class12.method488(i_56_, i_57_, -2114527043);
			int i_59_ = Class282_Sub20_Sub24.method15390(i_56_, i_57_, 1054094133);
			if (i_58_ == ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 && (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 == i_59_))
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
			else {
				int i_60_ = (i_56_ - -217803069 * ((Class282_Sub15_Sub5) this).anInt9848);
				if (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_56_ > i_60_)
					i_60_ = (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_56_);
				if (i_58_ - 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_60_)
					i_60_ = i_58_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117);
				if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_58_ > i_60_)
					i_60_ = (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_58_);
				if (i_59_ - ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 > i_60_)
					i_60_ = i_59_ - (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173);
				if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 - i_59_ > i_60_)
					i_60_ = (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 - i_59_);
				if (i > i_60_)
					i = i_60_;
				((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * i;
				((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i_56_;
				((Class282_Sub15_Sub5) this).anInt9847 = i_57_ * 375820671;
				((Class282_Sub15_Sub5) this).anInt9849 = (-1756138925 * ((i_56_ - (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848)) / i));
				((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * ((i_58_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117)) / i);
				((Class282_Sub15_Sub5) this).anInt9851 = (-1589776061 * ((i_59_ - (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850)) / i));
			}
		}
	}

	public final synchronized void method15362(int i, int i_61_, int i_62_) {
		if (i == 0)
			method15364(i_61_, i_62_, 16711935);
		else {
			int i_63_ = Class12.method488(i_61_, i_62_, -2019021703);
			int i_64_ = Class282_Sub20_Sub24.method15390(i_61_, i_62_, 1983768421);
			if (i_63_ == ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 && (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 == i_64_))
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
			else {
				int i_65_ = (i_61_ - -217803069 * ((Class282_Sub15_Sub5) this).anInt9848);
				if (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_61_ > i_65_)
					i_65_ = (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_61_);
				if (i_63_ - 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_65_)
					i_65_ = i_63_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117);
				if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_63_ > i_65_)
					i_65_ = (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_63_);
				if (i_64_ - ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 > i_65_)
					i_65_ = i_64_ - (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173);
				if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 - i_64_ > i_65_)
					i_65_ = (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 - i_64_);
				if (i > i_65_)
					i = i_65_;
				((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * i;
				((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i_61_;
				((Class282_Sub15_Sub5) this).anInt9847 = i_62_ * 375820671;
				((Class282_Sub15_Sub5) this).anInt9849 = (-1756138925 * ((i_61_ - (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848)) / i));
				((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * ((i_63_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117)) / i);
				((Class282_Sub15_Sub5) this).anInt9851 = (-1589776061 * ((i_64_ - (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850)) / i));
			}
		}
	}

	public final synchronized void method15363(int i, int i_66_, int i_67_) {
		if (i == 0)
			method15364(i_66_, i_67_, 16711935);
		else {
			int i_68_ = Class12.method488(i_66_, i_67_, -2119751902);
			int i_69_ = Class282_Sub20_Sub24.method15390(i_66_, i_67_, 1961722868);
			if (i_68_ == ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117 && (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 == i_69_))
				((Class282_Sub15_Sub5) this).anInt9858 = 0;
			else {
				int i_70_ = (i_66_ - -217803069 * ((Class282_Sub15_Sub5) this).anInt9848);
				if (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_66_ > i_70_)
					i_70_ = (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848 - i_66_);
				if (i_68_ - 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_70_)
					i_70_ = i_68_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117);
				if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_68_ > i_70_)
					i_70_ = (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 - i_68_);
				if (i_69_ - ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 > i_70_)
					i_70_ = i_69_ - (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173);
				if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 - i_69_ > i_70_)
					i_70_ = (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173 - i_69_);
				if (i > i_70_)
					i = i_70_;
				((Class282_Sub15_Sub5) this).anInt9858 = 1156515807 * i;
				((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i_66_;
				((Class282_Sub15_Sub5) this).anInt9847 = i_67_ * 375820671;
				((Class282_Sub15_Sub5) this).anInt9849 = (-1756138925 * ((i_66_ - (-217803069 * ((Class282_Sub15_Sub5) this).anInt9848)) / i));
				((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * ((i_68_ - (((Class282_Sub15_Sub5) this).anInt9845 * 1784001117)) / i);
				((Class282_Sub15_Sub5) this).anInt9851 = (-1589776061 * ((i_69_ - (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850)) / i));
			}
		}
	}

	final synchronized void method15364(int i, int i_71_, int i_72_) {
		((Class282_Sub15_Sub5) this).anInt9846 = -298675253 * i;
		((Class282_Sub15_Sub5) this).anInt9847 = 375820671 * i_71_;
		((Class282_Sub15_Sub5) this).anInt9858 = 0;
		method15332((byte) -91);
	}

	public final synchronized void method15365(int i) {
		if (0 == i) {
			method15326(0, (byte) -24);
			method4991(-371378792);
		} else if (0 == 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 && 0 == (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173)) {
			((Class282_Sub15_Sub5) this).anInt9858 = 0;
			((Class282_Sub15_Sub5) this).anInt9846 = 0;
			((Class282_Sub15_Sub5) this).anInt9848 = 0;
			method4991(-371378792);
		} else {
			int i_73_ = -(((Class282_Sub15_Sub5) this).anInt9848 * -217803069);
			if (((Class282_Sub15_Sub5) this).anInt9848 * -217803069 > i_73_)
				i_73_ = ((Class282_Sub15_Sub5) this).anInt9848 * -217803069;
			if (-(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845) > i_73_)
				i_73_ = -(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845);
			if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_73_)
				i_73_ = ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117;
			if (-(((Class282_Sub15_Sub5) this).anInt9850 * 2085185173) > i_73_)
				i_73_ = -(2085185173 * ((Class282_Sub15_Sub5) this).anInt9850);
			if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 > i_73_)
				i_73_ = ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173;
			if (i > i_73_)
				i = i_73_;
			((Class282_Sub15_Sub5) this).anInt9858 = i * 1156515807;
			((Class282_Sub15_Sub5) this).anInt9846 = -2147483648;
			((Class282_Sub15_Sub5) this).anInt9849 = (-(((Class282_Sub15_Sub5) this).anInt9848 * -217803069) / i * -1756138925);
			((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * (-(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845) / i);
			((Class282_Sub15_Sub5) this).anInt9851 = (-(2085185173 * ((Class282_Sub15_Sub5) this).anInt9850) / i * -1589776061);
		}
	}

	public final synchronized void method15366(int i) {
		if (0 == i) {
			method15326(0, (byte) 30);
			method4991(-371378792);
		} else if (0 == 1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 && 0 == (((Class282_Sub15_Sub5) this).anInt9850 * 2085185173)) {
			((Class282_Sub15_Sub5) this).anInt9858 = 0;
			((Class282_Sub15_Sub5) this).anInt9846 = 0;
			((Class282_Sub15_Sub5) this).anInt9848 = 0;
			method4991(-371378792);
		} else {
			int i_74_ = -(((Class282_Sub15_Sub5) this).anInt9848 * -217803069);
			if (((Class282_Sub15_Sub5) this).anInt9848 * -217803069 > i_74_)
				i_74_ = ((Class282_Sub15_Sub5) this).anInt9848 * -217803069;
			if (-(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845) > i_74_)
				i_74_ = -(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845);
			if (1784001117 * ((Class282_Sub15_Sub5) this).anInt9845 > i_74_)
				i_74_ = ((Class282_Sub15_Sub5) this).anInt9845 * 1784001117;
			if (-(((Class282_Sub15_Sub5) this).anInt9850 * 2085185173) > i_74_)
				i_74_ = -(2085185173 * ((Class282_Sub15_Sub5) this).anInt9850);
			if (2085185173 * ((Class282_Sub15_Sub5) this).anInt9850 > i_74_)
				i_74_ = ((Class282_Sub15_Sub5) this).anInt9850 * 2085185173;
			if (i > i_74_)
				i = i_74_;
			((Class282_Sub15_Sub5) this).anInt9858 = i * 1156515807;
			((Class282_Sub15_Sub5) this).anInt9846 = -2147483648;
			((Class282_Sub15_Sub5) this).anInt9849 = (-(((Class282_Sub15_Sub5) this).anInt9848 * -217803069) / i * -1756138925);
			((Class282_Sub15_Sub5) this).anInt9852 = -661281463 * (-(1784001117 * ((Class282_Sub15_Sub5) this).anInt9845) / i);
			((Class282_Sub15_Sub5) this).anInt9851 = (-(2085185173 * ((Class282_Sub15_Sub5) this).anInt9850) / i * -1589776061);
		}
	}

	public final synchronized void method15367(int i) {
		if (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0)
			((Class282_Sub15_Sub5) this).anInt9855 = -i * 1927994969;
		else
			((Class282_Sub15_Sub5) this).anInt9855 = i * 1927994969;
	}

	Class282_Sub15_Sub5() {
		/* empty */
	}

	public final synchronized int method15368() {
		return (-177305111 * ((Class282_Sub15_Sub5) this).anInt9855 < 0 ? -(-177305111 * ((Class282_Sub15_Sub5) this).anInt9855) : -177305111 * ((Class282_Sub15_Sub5) this).anInt9855);
	}

	static final void decodeMask(RsBitsBuffer buffer, int playerIndex, Class521_Sub1_Sub1_Sub2_Sub1 player, int mask, int i_76_) {
		byte movementType = Class249.aClass249_3083.aByte3085;
		if ((mask & 0x20000) != 0) {// 1
			player.aByte10371 = buffer.method13236((short) -13092);
			player.aByte10327 = buffer.readByteC(-1984309750);
			player.aByte10364 = buffer.read128Byte((short) -22365);
			player.aByte10352 = (byte) buffer.readUnsigned128Byte();
			player.anInt10347 = (-1809259861 * client.anInt7174 + buffer.readUnsignedShort128()) * -245378127;
			player.anInt10348 = (client.anInt7174 * -1809259861 + buffer.readUnsignedShort()) * 485671023;
		}
		if ((mask & 0x400000) != 0) {// 2
			int i_78_ = ((buffer.buffer[((buffer.index += -1115476867) * -1990677291) - 1]) & 0xff);
			for (int i_79_ = 0; i_79_ < i_78_; i_79_++) {
				int i_80_ = buffer.readUnsignedShortLE((byte) -120);
				int i_81_ = buffer.readIntLE();
				player.aClass155_10561.method2625(i_80_, i_81_, (byte) 28);
			}
		}
		if ((mask & 0x200) != 0) {// 3 gfx 2
			int i_82_ = buffer.readUnsignedShort();
			int i_83_ = buffer.readUnsignedIntV2(-534504239);
			if (i_82_ == 65535)
				i_82_ = -1;
			int i_84_ = buffer.readUnsigned128Byte();
			int i_85_ = i_84_ & 0x7;
			int i_86_ = i_84_ >> 3 & 0xf;
			if (i_86_ == 15)
				i_86_ = -1;
			boolean bool = (i_84_ >> 7 & 0x1) == 1;
			player.sendGraphics(i_82_, i_83_, i_85_, i_86_, bool, 1, 1574977032);
		}
		if ((mask & 0x20) != 0) {// 4 face direction
			player.anInt10557 = buffer.readUnsignedShort128() * 1435090479;
			if (1871221471 * player.anInt10355 == 0) {
				player.method15863(327043279 * player.anInt10557, 1784401370);
				player.anInt10557 = -1435090479;
			}
		}
		if (0 != (mask & 0x800000)) {// 5
			player.aClass155_10561.method2624(-1258310842);
			int i_87_ = ((buffer.buffer[((buffer.index += -1115476867) * -1990677291) - 1]) & 0xff);
			for (int i_88_ = 0; i_88_ < i_87_; i_88_++) {
				int i_89_ = buffer.readUnsignedShortLE((byte) -4);
				int i_90_ = buffer.readIntLE();
				player.aClass155_10561.method2625(i_89_, i_90_, (byte) 28);
			}
		}
		if ((mask & 0x10000) != 0)// 6
			player.aBool10550 = buffer.readUnsigned128Byte() == 1;
		if (0 != (mask & 0x100000)) {// 7
			int i_91_ = buffer.readUnsigned128Byte();
			int[] is = new int[i_91_];
			int[] is_92_ = new int[i_91_];
			for (int i_93_ = 0; i_93_ < i_91_; i_93_++) {
				int i_94_ = buffer.readUnsignedShort128();
				if ((i_94_ & 0xc000) == 49152) {
					int i_95_ = buffer.readUnsignedShort128();
					is[i_93_] = i_94_ << 16 | i_95_;
				} else
					is[i_93_] = i_94_;
				is_92_[i_93_] = buffer.readUnsignedShort();
			}
			player.method15797(is, is_92_, -1858199952);
		}
		if (0 != (mask & 0x4000)) {// 8 force talk
			String string = buffer.readString(-406005188);
			if (player == Class84.myPlayer)
				Class191.method3167(2, 0, player.method16127(true, 2097729093), player.method16128(false, 1912893547), player.aString10563, string, 1185055161);
			player.method16134(string, 0, 0, (byte) -79);
		}
		if ((mask & 0x80000) != 0) {// 9
			String string = buffer.readString(-526945645);
			int i_96_ = buffer.readUnsignedByte128(-1590045136);
			if (0 != (i_96_ & 0x1))
				Class191.method3167(2, i_96_, player.method16127(true, 1944808899), player.method16128(false, 1912893547), player.aString10563, string, 1566432010);
			player.method16134(string, 0, 0, (byte) -55);
		}
		if ((mask & 0x40000) != 0) {// 10 gfx 3
			int i_97_ = buffer.readUnsignedShortLE128(602501853);
			int i_98_ = buffer.readUnsignedIntV2(-1265526717);
			if (i_97_ == 65535)
				i_97_ = -1;
			int i_99_ = buffer.readUnsignedByte128(-1571232040);
			int i_100_ = i_99_ & 0x7;
			int i_101_ = i_99_ >> 3 & 0xf;
			if (15 == i_101_)
				i_101_ = -1;
			boolean bool = (i_99_ >> 7 & 0x1) == 1;
			player.sendGraphics(i_97_, i_98_, i_100_, i_101_, bool, 2, -986175675);
		}
		if ((mask & 0x8000) != 0)// 11
			player.aBool10571 = buffer.readUnsigned128Byte() == 1;
		if ((mask & 0x40) != 0) {// 12 hits
			int count = buffer.readUnsigned128Byte();
			if (count > 0) {
				for (int index = 0; index < count; index++) {
					int i_104_ = -1;
					int i_105_ = -1;
					int i_106_ = -1;
					int i_107_ = buffer.readUnsignedSmart(1634249310);
					if (i_107_ == 32767) {
						i_107_ = buffer.readUnsignedSmart(1927514283);
						i_105_ = buffer.readUnsignedSmart(1968115481);
						i_104_ = buffer.readUnsignedSmart(1841145813);
						i_106_ = buffer.readUnsignedSmart(2133803964);
					} else if (i_107_ != 32766)
						i_105_ = buffer.readUnsignedSmart(2072861558);
					else {
						i_107_ = -1;
						i_105_ = buffer.readUnsignedByteC(-1911985407);
					}
					int i_108_ = buffer.readUnsignedSmart(2132556063);
					player.method15802(i_107_, i_105_, i_104_, i_106_, (client.anInt7174 * -1809259861), i_108_, 775732782);
				}
			}
			int i_109_ = buffer.readUnsignedByte();
			if (i_109_ > 0) {
				for (int i_110_ = 0; i_110_ < i_109_; i_110_++) {
					int barType = buffer.readUnsignedSmart(1729379803);
					int full = buffer.readUnsignedSmart(1506698812);
					if (32767 != full) {
						int i_113_ = buffer.readUnsignedSmart(1806128568);
						int i_114_ = buffer.readUnsigned128Byte();
						int i_115_ = (full > 0 ? buffer.readUnsignedByte128(-1733572994) : i_114_);
						player.method15803(barType, -1809259861 * client.anInt7174, full, i_113_, i_114_, i_115_, (byte) 0);
					} else
						player.method15857(barType, 1885735347);
				}
			}
		}
		if ((mask & 0x1) != 0) {// 13 appearance
			int i_116_ = buffer.readUnsignedByteC(850654507);
			byte[] is = new byte[i_116_];
			RsByteBuffer class282_sub35 = new RsByteBuffer(is);
			buffer.readBytes(is, 0, i_116_, 1586054229);
			Class197.aClass282_Sub35Array2428[playerIndex] = class282_sub35;
			player.decodeAppearance(class282_sub35, 1925099603);
		}
		if ((mask & 0x10) != 0) {// 14 emotes
			int[] is = new int[Class8_Sub3.method14339(1672148913).length];
			for (int i_117_ = 0; i_117_ < Class8_Sub3.method14339(1868309585).length; i_117_++)
				is[i_117_] = buffer.readBigSmart(2101955119);
			int i_118_ = buffer.readUnsignedByte();
			Class20.method746(player, is, i_118_, false, (byte) -32);
		}
		if (0 != (mask & 0x2)) {// 15 face entity
			int i_119_ = buffer.readUnsignedShort128();
			if (i_119_ == 65535)
				i_119_ = -1;
			player.anInt10373 = i_119_ * -2059452093;
		}
		if (0 != (mask & 0x1000))// 16 temporary movement type
			movementType = buffer.readByteC(-430958654);
		if (0 != (mask & 0x2000)) {// 17
			int i_120_ = buffer.readUnsigned128Byte();
			int[] is = new int[i_120_];
			int[] is_121_ = new int[i_120_];
			int[] is_122_ = new int[i_120_];
			for (int i_123_ = 0; i_123_ < i_120_; i_123_++) {
				is[i_123_] = buffer.readBigSmart(2140788143);
				is_121_[i_123_] = buffer.readUnsignedByteC(145947443);
				is_122_[i_123_] = buffer.readUnsignedShortLE128(602501853);
			}
			Class331.method5923(player, is, is_121_, is_122_, 263851655);
		}
		if (0 != (mask & 0x4))// 18 movement type
			Class197.playerMovementTypes[playerIndex] = buffer.read128Byte((short) -5982);
		if (0 != (mask & 0x200000)) {// 19 gfx 4
			int i_124_ = buffer.readUnsignedShortLE((byte) -115);
			int i_125_ = buffer.readUnsignedIntLE(771784879);
			if (65535 == i_124_)
				i_124_ = -1;
			int i_126_ = buffer.readUnsigned128Byte();
			int i_127_ = i_126_ & 0x7;
			int i_128_ = i_126_ >> 3 & 0xf;
			if (i_128_ == 15)
				i_128_ = -1;
			boolean bool = (i_126_ >> 7 & 0x1) == 1;
			player.sendGraphics(i_124_, i_125_, i_127_, i_128_, bool, 3, -795624313);
		}
		if (0 != (mask & 0x800)) {// 20 force movement
			player.anInt10326 = buffer.readByteC(1259464740) * 830034083;
			player.anInt10328 = buffer.read128Byte((short) -6627) * -2091025609;
			player.anInt10341 = buffer.readByte128(1577194121) * 1110107435;
			player.anInt10343 = buffer.readByteC(343474194) * -1352948627;
			player.anInt10342 = (buffer.readUnsignedShortLE128(602501853) + -1809259861 * client.anInt7174) * -506987231;
			player.anInt10345 = (buffer.readUnsignedShortLE((byte) -80) + -1809259861 * client.anInt7174) * -38144783;
			player.anInt10346 = buffer.readUnsignedShort128() * -19018465;
			if (player.aBool10568) {
				player.anInt10326 += -358216913 * player.anInt10569;
				player.anInt10328 += player.anInt10570 * 1391269727;
				player.anInt10341 += 505622999 * player.anInt10569;
				player.anInt10343 += -858307675 * player.anInt10570;
				player.anInt10355 = 0;
			} else {
				player.anInt10326 += (830034083 * player.anIntArray10356[0]);
				player.anInt10328 += (player.anIntArray10336[0] * -2091025609);
				player.anInt10341 += (1110107435 * player.anIntArray10356[0]);
				player.anInt10343 += (-1352948627 * player.anIntArray10336[0]);
				player.anInt10355 = -2086688481;
			}
			player.anInt10367 = 0;
		}
		if (0 != (mask & 0x80)) {// 21 gfx 1
			int i_129_ = buffer.readUnsignedShortLE((byte) -74);
			int i_130_ = buffer.readIntLE();
			if (i_129_ == 65535)
				i_129_ = -1;
			int i_131_ = buffer.readUnsignedByte128(-1063420458);
			int i_132_ = i_131_ & 0x7;
			int i_133_ = i_131_ >> 3 & 0xf;
			if (15 == i_133_)
				i_133_ = -1;
			boolean bool = (i_131_ >> 7 & 0x1) == 1;
			player.sendGraphics(i_129_, i_130_, i_132_, i_133_, bool, 0, 114463464);
		}
		if (player.aBool10568) {
			if (movementType == 127)
				player.method16130(997861381 * player.anInt10569, 487713049 * player.anInt10570, -166177247);
			else {
				byte i_134_;
				if (Class249.aClass249_3083.aByte3085 != movementType)
					i_134_ = movementType;
				else
					i_134_ = Class197.playerMovementTypes[playerIndex];
				Class236.method3985(player, i_134_, (byte) -106);
				player.method16129(player.anInt10569 * 997861381, 487713049 * player.anInt10570, i_134_, 2145500163);
			}
		}
	}
}
