/* Class521_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class521_Sub1_Sub2 extends Class521_Sub1 {
	static int anInt9461;

	boolean method13030(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		Class208 class208 = aClass206_7970.method3507(aByte7967, ((int) class385.aFloat4671 >> (aClass206_7970.anInt2592 * -1928575293)), ((int) class385.aFloat4673 >> (aClass206_7970.anInt2592 * -1928575293)), (byte) -22);
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			return (aClass206_7970.aClass201_2600.method3273(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), (class208.aClass521_Sub1_Sub1_2659.method12995(660913959) + method12995(-1468272747))));
		return (aClass206_7970.aClass201_2600.method3271(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293)));
	}

	final void method13016(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_0_, int i_1_, boolean bool) {
		throw new IllegalStateException();
	}

	boolean method13033() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643 + 1459994833 * ((Class206) aClass206_7970).anInt2652)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - ((Class206) aClass206_7970).anInt2629 * 1580412859))]);
	}

	boolean method13029(byte i) {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643 + 1459994833 * ((Class206) aClass206_7970).anInt2652)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - ((Class206) aClass206_7970).anInt2629 * 1580412859))]);
	}

	final boolean method12985(int i) {
		return false;
	}

	final void method13013(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_2_, int i_3_, boolean bool, int i_4_) {
		throw new IllegalStateException();
	}

	final void method12984(int i) {
		throw new IllegalStateException();
	}

	final boolean method13026() {
		return false;
	}

	final boolean method13011() {
		return false;
	}

	boolean method12988(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		Class208 class208 = aClass206_7970.method3507(aByte7967, ((int) class385.aFloat4671 >> (aClass206_7970.anInt2592 * -1928575293)), ((int) class385.aFloat4673 >> (aClass206_7970.anInt2592 * -1928575293)), (byte) -12);
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			return (aClass206_7970.aClass201_2600.method3273(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), (class208.aClass521_Sub1_Sub1_2659.method12995(-300040140) + method12995(-669380437))));
		return (aClass206_7970.aClass201_2600.method3271(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293)));
	}

	final void method13015() {
		throw new IllegalStateException();
	}

	boolean method13034() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643 + 1459994833 * ((Class206) aClass206_7970).anInt2652)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - ((Class206) aClass206_7970).anInt2629 * 1580412859))]);
	}

	int method13024(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> aClass206_7970.anInt2592 * -1928575293), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	int method13025(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> aClass206_7970.anInt2592 * -1928575293), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	int method13031(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> aClass206_7970.anInt2592 * -1928575293), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	int method12982(Class282_Sub24[] class282_sub24s) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> aClass206_7970.anInt2592 * -1928575293), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	Class521_Sub1_Sub2(Class206 class206, int i, int i_5_, int i_6_, int i_7_, int i_8_) {
		super(class206);
		aByte7967 = (byte) i_7_;
		aByte7968 = (byte) i_8_;
		method11171(new Class385((float) i, (float) i_5_, (float) i_6_));
	}

	final void method13021() {
		throw new IllegalStateException();
	}

	int method13036(Class282_Sub24[] class282_sub24s, int i) {
		Class385 class385 = method11166().aClass385_3595;
		return method13004(((int) class385.aFloat4671 >> aClass206_7970.anInt2592 * -1928575293), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class282_sub24s, 868144561);
	}

	boolean method13022(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		Class208 class208 = aClass206_7970.method3507(aByte7967, ((int) class385.aFloat4671 >> (aClass206_7970.anInt2592 * -1928575293)), ((int) class385.aFloat4673 >> (aClass206_7970.anInt2592 * -1928575293)), (byte) -29);
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			return (aClass206_7970.aClass201_2600.method3273(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class208.aClass521_Sub1_Sub1_2659.method12995(-1585085886) + method12995(-849675272)));
		return (aClass206_7970.aClass201_2600.method3271(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293)));
	}

	boolean method13032() {
		Class385 class385 = method11166().aClass385_3595;
		return (((Class206) aClass206_7970).aBoolArrayArray2651[(((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592) - ((Class206) aClass206_7970).anInt2628 * -527863643 + 1459994833 * ((Class206) aClass206_7970).anInt2652)][(1459994833 * ((Class206) aClass206_7970).anInt2652 + (((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293) - ((Class206) aClass206_7970).anInt2629 * 1580412859))]);
	}

	boolean method13037(Class505 class505, int i) {
		Class385 class385 = method11166().aClass385_3595;
		Class208 class208 = aClass206_7970.method3507(aByte7967, ((int) class385.aFloat4671 >> (aClass206_7970.anInt2592 * -1928575293)), ((int) class385.aFloat4673 >> (aClass206_7970.anInt2592 * -1928575293)), (byte) 43);
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			return (aClass206_7970.aClass201_2600.method3273(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), (class208.aClass521_Sub1_Sub1_2659.method12995(626314496) + method12995(-413929199))));
		return (aClass206_7970.aClass201_2600.method3271(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293)));
	}

	boolean method12998(Class505 class505) {
		Class385 class385 = method11166().aClass385_3595;
		Class208 class208 = aClass206_7970.method3507(aByte7967, ((int) class385.aFloat4671 >> (aClass206_7970.anInt2592 * -1928575293)), ((int) class385.aFloat4673 >> (aClass206_7970.anInt2592 * -1928575293)), (byte) 22);
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			return (aClass206_7970.aClass201_2600.method3273(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> -1928575293 * aClass206_7970.anInt2592), class208.aClass521_Sub1_Sub1_2659.method12995(-1436578253) + method12995(-206928837)));
		return (aClass206_7970.aClass201_2600.method3271(aByte7967, ((int) class385.aFloat4671 >> -1928575293 * aClass206_7970.anInt2592), ((int) class385.aFloat4673 >> aClass206_7970.anInt2592 * -1928575293)));
	}
}
