/* Class225 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class225 {
	public int anInt2779;
	public int anInt2780;
	public int anInt2781;
	public int anInt2782;

	public abstract boolean method3785(int i, int i_0_, int i_1_, Class336 class336);

	public abstract boolean method3786(int i, int i_2_, int i_3_, Class336 class336);

	public abstract boolean method3787(int i, int i_4_, int i_5_, Class336 class336, byte i_6_);

	public abstract boolean method3788(int i, int i_7_, int i_8_, Class336 class336);

	Class225() {
		/* empty */
	}

	public abstract boolean method3789(int i, int i_9_, int i_10_, Class336 class336);

	static final void method3790(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		if (((Class513) class513).aClass118_5886.anInt1288 * 1924549737 == -1) {
			if (((Class527) class527).aBool7022)
				throw new RuntimeException("");
			throw new RuntimeException("");
		}
		Class118 class118 = class513.method8772(42076446);
		class118.aClass118Array1438[1924549737 * (((Class513) class513).aClass118_5886.anInt1288)] = null;
		Class109.method1858(class118, (byte) -39);
	}

	static final void method3791(Class527 class527, byte i) {
		int i_11_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_11_, (byte) 77);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_11_ >> 16];
		Class524.method11223(class118, class98, class527, 334837898);
	}

	static final void method3792(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class163.aClass209_2031.method3569(2029806925);
	}

	static final void method3793(Class527 class527, byte i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1] = string.toLowerCase();
	}

	public static final void method3794(int i, int i_12_, int i_13_, int i_14_, int i_15_, boolean bool, byte i_16_) {
		Class296.anInt3534 = 39297289 * i;
		Class282_Sub44.anInt8064 = -1587752955 * i_12_;
		Class525.anInt6985 = i_13_ * -1905375393;
		Class454.anInt5451 = i_14_ * 1048053695;
		Class115.anInt1249 = 1032825985 * i_15_;
		if (bool) {
			if (i_16_ != -1) {
				/* empty */
			}
			if (Class115.anInt1249 * 613192577 >= 100) {
				if (i_16_ != -1) {
					/* empty */
				}
				Class31.anInt361 = Class296.anInt3534 * -899014144 + 898537728;
				Class246.anInt3029 = 26398976 + Class282_Sub44.anInt8064 * 2107292160;
				Class109_Sub1.anInt9384 = (Class504.method8389(Class31.anInt361 * -360258135, 413271601 * Class246.anInt3029, 675588453 * Class4.anInt35, (byte) 10) - 368694431 * Class525.anInt6985) * -126779709;
			}
		}
		Class262.anInt3240 = 1926220865;
		Class86.anInt833 = -1509271845;
		Class508.anInt5864 = 987778595;
	}
}
