/* Class27 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class27 {
	public static final int anInt332 = 0;
	public static final int anInt333 = 21;
	public static final int anInt334 = 22;
	public static final int anInt335 = 23;
	public static final int anInt336 = 28;
	public static final int anInt337 = 50;
	public static final int anInt338 = 51;
	public static final int anInt339 = 77;
	public static final int anInt340 = 80;
	public static final int anInt341 = 101;
	public static final int anInt342 = 102;
	public static final int anInt343 = 113;
	public static final int anInt344 = 116;
	public static int anInt345 = method755('D', 'X', 'T', '1') * -532386627;
	public static int anInt346 = method755('D', 'X', 'T', '5') * -450646721;

	private Class27() throws Throwable {
		throw new Error();
	}

	private static int method755(char c, char c_0_, char c_1_, char c_2_) {
		return ((c & 0xff) << 0 | (c_0_ & 0xff) << 8 | (c_1_ & 0xff) << 16 | (c_2_ & 0xff) << 24);
	}

	private static int method756(char c, char c_3_, char c_4_, char c_5_) {
		return ((c & 0xff) << 0 | (c_3_ & 0xff) << 8 | (c_4_ & 0xff) << 16 | (c_5_ & 0xff) << 24);
	}

	private static int method757(char c, char c_6_, char c_7_, char c_8_) {
		return ((c & 0xff) << 0 | (c_6_ & 0xff) << 8 | (c_7_ & 0xff) << 16 | (c_8_ & 0xff) << 24);
	}
}
