/* Class468_Sub30 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class468_Sub30 extends Class468 {
	public static final int anInt8070 = 1;
	public static final int anInt8071 = 0;

	public int method13414() {
		return -859024475 * anInt5578;
	}

	public Class468_Sub30(int i, Class282_Sub54 class282_sub54) {
		super(i, class282_sub54);
	}

	public void method13415(byte i) {
		if (anInt5578 * -859024475 != 0 && aClass282_Sub54_5581.aClass468_Sub17_8200.method12762(1488967501) != 1)
			anInt5578 = 0;
		if (0 != -859024475 * anInt5578 && aClass282_Sub54_5581.aClass468_Sub29_8201.method13050(519629067) == 2)
			anInt5578 = 0;
		if (-859024475 * anInt5578 < 0 || anInt5578 * -859024475 > 1)
			anInt5578 = method7781(1923702795) * -754033619;
	}

	int method7781(int i) {
		return 1;
	}

	public Class468_Sub30(Class282_Sub54 class282_sub54) {
		super(class282_sub54);
	}

	public int method7785(int i, int i_0_) {
		if (i != 0 && aClass282_Sub54_5581.aClass468_Sub29_8201.method13050(519629067) == 2)
			return 3;
		if (0 == i || aClass282_Sub54_5581.aClass468_Sub17_8200.method12762(410905125) == 1)
			return 1;
		return 2;
	}

	public int method13416() {
		return -859024475 * anInt5578;
	}

	public int method13417(int i) {
		return -859024475 * anInt5578;
	}

	public int method7784(int i) {
		if (i != 0 && aClass282_Sub54_5581.aClass468_Sub29_8201.method13050(519629067) == 2)
			return 3;
		if (0 == i || aClass282_Sub54_5581.aClass468_Sub17_8200.method12762(-1758430035) == 1)
			return 1;
		return 2;
	}

	void method7780(int i) {
		anInt5578 = i * -754033619;
	}

	int method7786() {
		return 1;
	}

	int method7787() {
		return 1;
	}

	public void method13418() {
		if (anInt5578 * -859024475 != 0 && aClass282_Sub54_5581.aClass468_Sub17_8200.method12762(1635898231) != 1)
			anInt5578 = 0;
		if (0 != -859024475 * anInt5578 && aClass282_Sub54_5581.aClass468_Sub29_8201.method13050(519629067) == 2)
			anInt5578 = 0;
		if (-859024475 * anInt5578 < 0 || anInt5578 * -859024475 > 1)
			anInt5578 = method7781(2120824666) * -754033619;
	}

	public void method13419() {
		if (anInt5578 * -859024475 != 0 && aClass282_Sub54_5581.aClass468_Sub17_8200.method12762(1392403259) != 1)
			anInt5578 = 0;
		if (0 != -859024475 * anInt5578 && aClass282_Sub54_5581.aClass468_Sub29_8201.method13050(519629067) == 2)
			anInt5578 = 0;
		if (-859024475 * anInt5578 < 0 || anInt5578 * -859024475 > 1)
			anInt5578 = method7781(1934221889) * -754033619;
	}

	public boolean method13420() {
		return true;
	}

	public boolean method13421(int i) {
		return true;
	}

	void method7783(int i, int i_1_) {
		anInt5578 = i * -754033619;
	}

	static int method13422(char c, Class495 class495, int i) {
		int i_2_ = c << 4;
		if (Character.isUpperCase(c) || Character.isTitleCase(c)) {
			int i_3_ = Character.toLowerCase(c);
			i_2_ = (i_3_ << 4) + 1;
		}
		return i_2_;
	}
}
