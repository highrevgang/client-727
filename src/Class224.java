/* Class224 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class224 {
	static Class225_Sub3 aClass225_Sub3_2773;
	static Class225_Sub6 aClass225_Sub6_2774;
	static Class225_Sub5 aClass225_Sub5_2775;
	static Class225_Sub1 aClass225_Sub1_2776 = new Class225_Sub1();
	static Class225_Sub4 aClass225_Sub4_2777;
	static Class225_Sub2 aClass225_Sub2_2778;

	public static Class225 method3771(int i, int i_0_, int i_1_, int i_2_, Class458 class458, int i_3_) {
		aClass225_Sub4_2777.anInt2779 = -726719813 * i;
		aClass225_Sub4_2777.anInt2780 = i_0_ * 1767388707;
		aClass225_Sub4_2777.anInt2781 = i_1_ * -12808295;
		aClass225_Sub4_2777.anInt2782 = i_2_ * 1709796035;
		((Class225_Sub4) aClass225_Sub4_2777).aClass458_7934 = class458;
		((Class225_Sub4) aClass225_Sub4_2777).anInt7933 = i_3_ * 657015239;
		return aClass225_Sub4_2777;
	}

	static {
		aClass225_Sub6_2774 = new Class225_Sub6();
		aClass225_Sub3_2773 = new Class225_Sub3();
		aClass225_Sub2_2778 = new Class225_Sub2();
		aClass225_Sub4_2777 = new Class225_Sub4();
		aClass225_Sub5_2775 = new Class225_Sub5();
	}

	Class224() throws Throwable {
		throw new Error();
	}

	public static Class225 method3772(int i, int i_4_, int i_5_, int i_6_) {
		aClass225_Sub6_2774.anInt2779 = i * -726719813;
		aClass225_Sub6_2774.anInt2780 = 1767388707 * i_4_;
		aClass225_Sub6_2774.anInt2781 = i_5_ * -12808295;
		aClass225_Sub6_2774.anInt2782 = i_6_ * 1709796035;
		return aClass225_Sub6_2774;
	}

	public static Class225 method3773(int i, int i_7_, int i_8_, int i_9_, int i_10_) {
		aClass225_Sub3_2773.anInt2779 = i * -726719813;
		aClass225_Sub3_2773.anInt2780 = 1767388707 * i_7_;
		aClass225_Sub3_2773.anInt2781 = -12808295 * i_8_;
		aClass225_Sub3_2773.anInt2782 = 1709796035 * i_9_;
		((Class225_Sub3) aClass225_Sub3_2773).anInt7929 = i_10_ * 1507410871;
		return aClass225_Sub3_2773;
	}

	public static Class225 method3774(int i, int i_11_, int i_12_, int i_13_, Class458 class458, int i_14_) {
		aClass225_Sub4_2777.anInt2779 = -726719813 * i;
		aClass225_Sub4_2777.anInt2780 = i_11_ * 1767388707;
		aClass225_Sub4_2777.anInt2781 = i_12_ * -12808295;
		aClass225_Sub4_2777.anInt2782 = i_13_ * 1709796035;
		((Class225_Sub4) aClass225_Sub4_2777).aClass458_7934 = class458;
		((Class225_Sub4) aClass225_Sub4_2777).anInt7933 = i_14_ * 657015239;
		return aClass225_Sub4_2777;
	}

	public static Class225 method3775(int i, int i_15_) {
		aClass225_Sub1_2776.anInt2779 = -726719813 * i;
		aClass225_Sub1_2776.anInt2780 = 1767388707 * i_15_;
		aClass225_Sub1_2776.anInt2781 = -12808295;
		aClass225_Sub1_2776.anInt2782 = 1709796035;
		return aClass225_Sub1_2776;
	}

	public static Class225 method3776(int i, int i_16_, int i_17_, int i_18_, int i_19_) {
		aClass225_Sub2_2778.anInt2779 = -726719813 * i;
		aClass225_Sub2_2778.anInt2780 = i_16_ * 1767388707;
		aClass225_Sub2_2778.anInt2781 = -12808295 * i_17_;
		aClass225_Sub2_2778.anInt2782 = i_18_ * 1709796035;
		((Class225_Sub2) aClass225_Sub2_2778).anInt7928 = i_19_ * -1398297429;
		return aClass225_Sub2_2778;
	}

	public static Class225 method3777(int i, int i_20_, int i_21_, int i_22_, Class458 class458, int i_23_) {
		aClass225_Sub4_2777.anInt2779 = -726719813 * i;
		aClass225_Sub4_2777.anInt2780 = i_20_ * 1767388707;
		aClass225_Sub4_2777.anInt2781 = i_21_ * -12808295;
		aClass225_Sub4_2777.anInt2782 = i_22_ * 1709796035;
		((Class225_Sub4) aClass225_Sub4_2777).aClass458_7934 = class458;
		((Class225_Sub4) aClass225_Sub4_2777).anInt7933 = i_23_ * 657015239;
		return aClass225_Sub4_2777;
	}

	public static Class225 method3778(int i, int i_24_, int i_25_, int i_26_, Class458 class458, int i_27_) {
		aClass225_Sub5_2775.anInt2779 = -726719813 * i;
		aClass225_Sub5_2775.anInt2780 = i_24_ * 1767388707;
		aClass225_Sub5_2775.anInt2781 = -12808295 * i_25_;
		aClass225_Sub5_2775.anInt2782 = i_26_ * 1709796035;
		((Class225_Sub5) aClass225_Sub5_2775).aClass458_7972 = class458;
		((Class225_Sub5) aClass225_Sub5_2775).anInt7971 = -845112339 * i_27_;
		return aClass225_Sub5_2775;
	}

	static final void method3779(Class527 class527, int i) {
		int i_28_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1] = ((Class527) class527).aClass61_7010.aStringArray617[i_28_];
	}

	static final void method3780(Class527 class527, byte i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_29_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_30_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 + 1]);
		Class282_Sub50_Sub14 class282_sub50_sub14 = Class296.aClass331_3535.method5918(i_29_, -2049190154);
		int i_31_ = class282_sub50_sub14.anIntArray9746[i_30_];
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_31_;
	}

	static final void method3781(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class393.aClass282_Sub54_4783.aClass468_Sub18_8214.method12774((byte) -14) ? 1 : 0;
	}

	public static void method3782(Class282_Sub50 class282_sub50, Class282_Sub50 class282_sub50_32_, int i) {
		if (null != class282_sub50.aClass282_Sub50_8118)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = class282_sub50_32_;
		class282_sub50.aClass282_Sub50_8119 = class282_sub50_32_.aClass282_Sub50_8119;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	static final void method3783(Class527 class527, int i) {
		int i_33_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class108 class108 = Class180.method3032(i_33_, (byte) -1);
		int i_34_ = -1;
		if (class108 != null)
			i_34_ = class108.anInt1091 * -1987818893;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_34_;
	}

	public static Class366 method3784(RsByteBuffer class282_sub35, int i) {
		int i_35_ = class282_sub35.readIntLE();
		return new Class366(i_35_);
	}
}
