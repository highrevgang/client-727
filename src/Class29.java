/* Class29 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class29 {
	Class317 aClass317_352;
	Class317 aClass317_353;
	Class30 aClass30_354;

	Class30 method780() {
		if (null == ((Class29) this).aClass30_354)
			((Class29) this).aClass30_354 = new Class30();
		return ((Class29) this).aClass30_354;
	}

	public Interface2 method781(Interface3 interface3, int i) {
		if (null == interface3)
			return null;
		Class60 class60 = interface3.method24(-2039586573);
		if (class60 == Class60.aClass60_609)
			return new Class46((Class366) interface3);
		if (class60 == Class60.aClass60_605)
			return new Class28(method782(-472205430), (Class365) interface3);
		if (Class60.aClass60_606 == class60)
			return new Class42(((Class29) this).aClass317_352, (Class357) interface3);
		if (Class60.aClass60_607 == class60)
			return new Class42_Sub1(((Class29) this).aClass317_352, (Class357_Sub1) interface3);
		if (class60 == Class60.aClass60_602)
			return new Class52_Sub3(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub2) interface3);
		if (Class60.aClass60_603 == class60)
			return new Class52_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub1) interface3);
		if (class60 == Class60.aClass60_608)
			return new Class52_Sub2(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3) interface3);
		if (Class60.aClass60_604 == class60)
			return new Class51(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class62) interface3);
		if (Class60.aClass60_601 == class60)
			return new Class43(((Class29) this).aClass317_352, (Class351) interface3);
		if (Class60.aClass60_610 == class60)
			return new Class52_Sub2_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3_Sub1) interface3);
		return null;
	}

	Class30 method782(int i) {
		if (null == ((Class29) this).aClass30_354)
			((Class29) this).aClass30_354 = new Class30();
		return ((Class29) this).aClass30_354;
	}

	public Class29(Class317 class317, Class317 class317_0_) {
		((Class29) this).aClass317_352 = class317;
		((Class29) this).aClass317_353 = class317_0_;
	}

	public Interface2 method783(Interface3 interface3) {
		if (null == interface3)
			return null;
		Class60 class60 = interface3.method24(-2039586573);
		if (class60 == Class60.aClass60_609)
			return new Class46((Class366) interface3);
		if (class60 == Class60.aClass60_605)
			return new Class28(method782(1435380564), (Class365) interface3);
		if (Class60.aClass60_606 == class60)
			return new Class42(((Class29) this).aClass317_352, (Class357) interface3);
		if (Class60.aClass60_607 == class60)
			return new Class42_Sub1(((Class29) this).aClass317_352, (Class357_Sub1) interface3);
		if (class60 == Class60.aClass60_602)
			return new Class52_Sub3(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub2) interface3);
		if (Class60.aClass60_603 == class60)
			return new Class52_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub1) interface3);
		if (class60 == Class60.aClass60_608)
			return new Class52_Sub2(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3) interface3);
		if (Class60.aClass60_604 == class60)
			return new Class51(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class62) interface3);
		if (Class60.aClass60_601 == class60)
			return new Class43(((Class29) this).aClass317_352, (Class351) interface3);
		if (Class60.aClass60_610 == class60)
			return new Class52_Sub2_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3_Sub1) interface3);
		return null;
	}

	public Interface2 method784(Interface3 interface3) {
		if (null == interface3)
			return null;
		Class60 class60 = interface3.method24(-2039586573);
		if (class60 == Class60.aClass60_609)
			return new Class46((Class366) interface3);
		if (class60 == Class60.aClass60_605)
			return new Class28(method782(-1296341907), (Class365) interface3);
		if (Class60.aClass60_606 == class60)
			return new Class42(((Class29) this).aClass317_352, (Class357) interface3);
		if (Class60.aClass60_607 == class60)
			return new Class42_Sub1(((Class29) this).aClass317_352, (Class357_Sub1) interface3);
		if (class60 == Class60.aClass60_602)
			return new Class52_Sub3(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub2) interface3);
		if (Class60.aClass60_603 == class60)
			return new Class52_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub1) interface3);
		if (class60 == Class60.aClass60_608)
			return new Class52_Sub2(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3) interface3);
		if (Class60.aClass60_604 == class60)
			return new Class51(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class62) interface3);
		if (Class60.aClass60_601 == class60)
			return new Class43(((Class29) this).aClass317_352, (Class351) interface3);
		if (Class60.aClass60_610 == class60)
			return new Class52_Sub2_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3_Sub1) interface3);
		return null;
	}

	public Interface2 method785(Interface3 interface3) {
		if (null == interface3)
			return null;
		Class60 class60 = interface3.method24(-2039586573);
		if (class60 == Class60.aClass60_609)
			return new Class46((Class366) interface3);
		if (class60 == Class60.aClass60_605)
			return new Class28(method782(-1024272750), (Class365) interface3);
		if (Class60.aClass60_606 == class60)
			return new Class42(((Class29) this).aClass317_352, (Class357) interface3);
		if (Class60.aClass60_607 == class60)
			return new Class42_Sub1(((Class29) this).aClass317_352, (Class357_Sub1) interface3);
		if (class60 == Class60.aClass60_602)
			return new Class52_Sub3(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub2) interface3);
		if (Class60.aClass60_603 == class60)
			return new Class52_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub1) interface3);
		if (class60 == Class60.aClass60_608)
			return new Class52_Sub2(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3) interface3);
		if (Class60.aClass60_604 == class60)
			return new Class51(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class62) interface3);
		if (Class60.aClass60_601 == class60)
			return new Class43(((Class29) this).aClass317_352, (Class351) interface3);
		if (Class60.aClass60_610 == class60)
			return new Class52_Sub2_Sub1(((Class29) this).aClass317_352, ((Class29) this).aClass317_353, (Class350_Sub3_Sub1) interface3);
		return null;
	}

	static final void method786(Class118 class118, Class98 class98, Class527 class527, int i) {
		boolean bool = ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1);
		if (bool != class118.aBool1306) {
			class118.aBool1306 = bool;
			Class109.method1858(class118, (byte) -3);
		}
		if (class118.anInt1288 * 1924549737 == -1 && !class98.aBool999)
			Class78.method1389(class118.anInt1287 * -1952846363, -626903266);
	}

	static final void method787(Class527 class527, byte i) {
		Class282_Sub36 class282_sub36 = Class540.method11595(-1576243923);
		if (class282_sub36 == null) {
			if (i == 65) {
				((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537) - 1] = -1;
				((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537) - 1] = -1;
			}
		} else {
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = -1798678621 * class282_sub36.anInt7991;
			int i_1_ = (-1967986419 * class282_sub36.anInt7988 << 28 | (Class291.anInt3472 + class282_sub36.anInt7987 * -1306535747) << 14 | (Class291.anInt3473 + class282_sub36.anInt7993 * 1012301095));
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = i_1_;
		}
	}

	static void method788(int i, boolean bool, int i_2_) {
		Class282_Sub30 class282_sub30 = Class517.method11127(i, bool, 1384603359);
		if (class282_sub30 != null)
			class282_sub30.method4991(-371378792);
	}

	static final void method789(Class118 class118, Class98 class98, Class527 class527, int i) {
		if (5 == -2131393857 * class118.anInt1268)
			Class306.method5459(class118, class98, class527, -1486072931);
	}
}
