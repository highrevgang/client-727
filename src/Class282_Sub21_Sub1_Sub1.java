/* Class282_Sub21_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class282_Sub21_Sub1_Sub1 extends Class282_Sub21_Sub1 {
	Class101_Sub2 aClass101_Sub2_10305;
	int[] anIntArray10306 = null;
	int[] anIntArray10307 = null;

	public final int method15472(int i) {
		int i_0_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[i];
		int i_1_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[i];
		if (i_0_ != -1)
			return i_0_;
		if (i_1_ != -1)
			return i_1_ | 0x10000;
		return -1;
	}

	public boolean method15471(int i) {
		return true;
	}

	final int method15782() {
		return (((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[((Class282_Sub21_Sub1_Sub1) this).aClass101_Sub2_10305.method1714(-2032886092)]);
	}

	final int method15783() {
		return (((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[((Class282_Sub21_Sub1_Sub1) this).aClass101_Sub2_10305.method1714(-1901630483)]);
	}

	public final int method15460(int i) {
		int i_2_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[i];
		int i_3_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[i];
		if (i_2_ != -1)
			return i_2_;
		if (i_3_ != -1)
			return i_3_ | 0x10000;
		return -1;
	}

	public boolean method15463(int i) {
		return true;
	}

	public boolean method15461(int i) {
		return true;
	}

	public final int method15455(int i) {
		int i_4_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[i];
		int i_5_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[i];
		if (i_4_ != -1)
			return i_4_;
		if (i_5_ != -1)
			return i_5_ | 0x10000;
		return -1;
	}

	public final int method15473(int i) {
		int i_6_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[i];
		int i_7_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[i];
		if (i_6_ != -1)
			return i_6_;
		if (i_7_ != -1)
			return i_7_ | 0x10000;
		return -1;
	}

	public final int method15453(int i) {
		int i_8_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[i];
		int i_9_ = ((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[i];
		if (i_8_ != -1)
			return i_8_;
		if (i_9_ != -1)
			return i_9_ | 0x10000;
		return -1;
	}

	public boolean method15470(int i) {
		return true;
	}

	public boolean method15462(int i) {
		return true;
	}

	Class282_Sub21_Sub1_Sub1(Class101_Sub2 class101_sub2, Class122 class122) {
		super(class122);
		((Class282_Sub21_Sub1_Sub1) this).aClass101_Sub2_10305 = class101_sub2;
		((Class282_Sub21_Sub1_Sub1) this).anIntArray10306 = class122.anIntArray1531;
		((Class282_Sub21_Sub1_Sub1) this).anIntArray10307 = class122.anIntArray1534;
	}

	final int method15784() {
		return (((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[((Class282_Sub21_Sub1_Sub1) this).aClass101_Sub2_10305.method1714(-2070704637)]);
	}

	final int method15785() {
		return (((Class282_Sub21_Sub1_Sub1) this).anIntArray10306[((Class282_Sub21_Sub1_Sub1) this).aClass101_Sub2_10305.method1714(-1910890568)]);
	}

	final int method15786() {
		return (((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[((Class282_Sub21_Sub1_Sub1) this).aClass101_Sub2_10305.method1714(-2042726954)]);
	}

	final int method15787() {
		return (((Class282_Sub21_Sub1_Sub1) this).anIntArray10307[((Class282_Sub21_Sub1_Sub1) this).aClass101_Sub2_10305.method1714(-2126765927)]);
	}
}
