/* Class96_Sub19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class96_Sub19 extends Class96 {
	int anInt9399;
	int anInt9400;
	int anInt9401;
	int anInt9402;
	int anInt9403;
	int anInt9404;
	int anInt9405;
	static final int anInt9406 = 1;
	int anInt9407;
	int anInt9408;
	int anInt9409;
	int anInt9410;
	static final int anInt9411 = 0;
	int anInt9412;
	int anInt9413;

	public void method1592(int i) {
		int i_0_;
		int i_1_;
		int i_2_;
		if (((Class96_Sub19) this).anInt9403 * 1708709241 >= 0) {
			i_0_ = -1314196992 * ((Class96_Sub19) this).anInt9403 + 256;
			i_1_ = 256 + 1197836800 * ((Class96_Sub19) this).anInt9404;
			i_2_ = 1713842129 * ((Class96_Sub19) this).anInt9401;
		} else {
			Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2 = Class82.aClass75Array804[-772738915 * ((Class96_Sub19) this).anInt9402].method1342((byte) 70);
			Class385 class385 = class521_sub1_sub1_sub2.method11166().aClass385_3595;
			i_0_ = (int) class385.aFloat4671;
			i_1_ = (int) class385.aFloat4673;
			i_2_ = class521_sub1_sub1_sub2.aByte7967;
		}
		int i_3_;
		int i_4_;
		if (((Class96_Sub19) this).anInt9404 * -316427579 >= 0) {
			i_3_ = ((Class96_Sub19) this).anInt9407 * -875894272 + 256;
			i_4_ = ((Class96_Sub19) this).anInt9408 * -2078663168 + 256;
		} else {
			Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2 = Class82.aClass75Array804[((Class96_Sub19) this).anInt9400 * 1060287315].method1342((byte) 21);
			Class385 class385 = class521_sub1_sub1_sub2.method11166().aClass385_3595;
			i_3_ = (int) class385.aFloat4671;
			i_4_ = (int) class385.aFloat4673;
			if (i_2_ < 0)
				i_2_ = class521_sub1_sub1_sub2.aByte7967;
		}
		int i_5_ = -716833811 * ((Class96_Sub19) this).anInt9399 << 2;
		Class521_Sub1_Sub1_Sub3 class521_sub1_sub1_sub3 = (new Class521_Sub1_Sub1_Sub3(client.aClass257_7353.method4430(-1436448746), ((Class96_Sub19) this).anInt9410 * 899631601, i_2_, i_2_, i_0_, i_1_, ((Class96_Sub19) this).anInt9405 * -1118504215 << 2, client.anInt7174 * -1809259861, (-1809259861 * client.anInt7174 + ((Class96_Sub19) this).anInt9409 * -511484631), ((Class96_Sub19) this).anInt9412 * 1829419495, i_5_, 1 + ((Class96_Sub19) this).anInt9402 * -772738915, ((Class96_Sub19) this).anInt9400 * 1060287315 + 1, -209777139 * ((Class96_Sub19) this).anInt9413 << 2, false, 0));
		class521_sub1_sub1_sub3.method15904(i_3_, i_4_, -209777139 * ((Class96_Sub19) this).anInt9413 << 2, (-511484631 * ((Class96_Sub19) this).anInt9409 + client.anInt7174 * -1809259861), -209443166);
		client.aClass482_7333.method8059(new Class282_Sub50_Sub16(class521_sub1_sub1_sub3), 1701018076);
	}

	public void method1601() {
		int i;
		int i_6_;
		int i_7_;
		if (((Class96_Sub19) this).anInt9403 * 1708709241 >= 0) {
			i = -1314196992 * ((Class96_Sub19) this).anInt9403 + 256;
			i_6_ = 256 + 1197836800 * ((Class96_Sub19) this).anInt9404;
			i_7_ = 1713842129 * ((Class96_Sub19) this).anInt9401;
		} else {
			Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2 = Class82.aClass75Array804[-772738915 * ((Class96_Sub19) this).anInt9402].method1342((byte) 45);
			Class385 class385 = class521_sub1_sub1_sub2.method11166().aClass385_3595;
			i = (int) class385.aFloat4671;
			i_6_ = (int) class385.aFloat4673;
			i_7_ = class521_sub1_sub1_sub2.aByte7967;
		}
		int i_8_;
		int i_9_;
		if (((Class96_Sub19) this).anInt9404 * -316427579 >= 0) {
			i_8_ = ((Class96_Sub19) this).anInt9407 * -875894272 + 256;
			i_9_ = ((Class96_Sub19) this).anInt9408 * -2078663168 + 256;
		} else {
			Class521_Sub1_Sub1_Sub2 class521_sub1_sub1_sub2 = Class82.aClass75Array804[((Class96_Sub19) this).anInt9400 * 1060287315].method1342((byte) 42);
			Class385 class385 = class521_sub1_sub1_sub2.method11166().aClass385_3595;
			i_8_ = (int) class385.aFloat4671;
			i_9_ = (int) class385.aFloat4673;
			if (i_7_ < 0)
				i_7_ = class521_sub1_sub1_sub2.aByte7967;
		}
		int i_10_ = -716833811 * ((Class96_Sub19) this).anInt9399 << 2;
		Class521_Sub1_Sub1_Sub3 class521_sub1_sub1_sub3 = (new Class521_Sub1_Sub1_Sub3(client.aClass257_7353.method4430(-2009786094), ((Class96_Sub19) this).anInt9410 * 899631601, i_7_, i_7_, i, i_6_, ((Class96_Sub19) this).anInt9405 * -1118504215 << 2, client.anInt7174 * -1809259861, (-1809259861 * client.anInt7174 + ((Class96_Sub19) this).anInt9409 * -511484631), ((Class96_Sub19) this).anInt9412 * 1829419495, i_10_, 1 + ((Class96_Sub19) this).anInt9402 * -772738915, ((Class96_Sub19) this).anInt9400 * 1060287315 + 1, -209777139 * ((Class96_Sub19) this).anInt9413 << 2, false, 0));
		class521_sub1_sub1_sub3.method15904(i_8_, i_9_, -209777139 * ((Class96_Sub19) this).anInt9413 << 2, (-511484631 * ((Class96_Sub19) this).anInt9409 + client.anInt7174 * -1809259861), -209443166);
		client.aClass482_7333.method8059(new Class282_Sub50_Sub16(class521_sub1_sub1_sub3), -1862724150);
	}

	boolean method1599(int i) {
		Class525 class525 = Class96_Sub20.aClass515_9416.method8845((((Class96_Sub19) this).anInt9410) * 899631601, (byte) 46);
		boolean bool = class525.method11230(-583697397);
		Class518 class518 = Class330.aClass523_3868.method11205(class525.anInt6977 * -364555849, (byte) 7);
		bool &= class518.method11132(1967469985);
		return bool;
	}

	Class96_Sub19(RsByteBuffer class282_sub35, int i, int i_11_) {
		super(class282_sub35);
		if (0 == i) {
			int i_12_ = class282_sub35.readIntLE();
			((Class96_Sub19) this).anInt9403 = -895580983 * (i_12_ >>> 16);
			((Class96_Sub19) this).anInt9404 = (i_12_ & 0xffff) * -943784947;
			((Class96_Sub19) this).anInt9402 = 37939787;
		} else {
			((Class96_Sub19) this).anInt9403 = 895580983;
			((Class96_Sub19) this).anInt9404 = 943784947;
			((Class96_Sub19) this).anInt9402 = class282_sub35.readUnsignedShort() * -37939787;
		}
		if (i_11_ == 0) {
			int i_13_ = class282_sub35.readIntLE();
			((Class96_Sub19) this).anInt9407 = (i_13_ >>> 16) * -1660550435;
			((Class96_Sub19) this).anInt9408 = 11223535 * (i_13_ & 0xffff);
			((Class96_Sub19) this).anInt9400 = -857868507;
		} else {
			((Class96_Sub19) this).anInt9407 = 1660550435;
			((Class96_Sub19) this).anInt9408 = -11223535;
			((Class96_Sub19) this).anInt9400 = class282_sub35.readUnsignedShort() * 857868507;
		}
		if (0 == i && 0 == i_11_)
			((Class96_Sub19) this).anInt9401 = class282_sub35.readUnsignedByte() * 1723109681;
		else
			((Class96_Sub19) this).anInt9401 = -1723109681;
		((Class96_Sub19) this).anInt9410 = class282_sub35.readUnsignedShort() * 2035189521;
		((Class96_Sub19) this).anInt9405 = class282_sub35.readUnsignedByte() * 2127134041;
		((Class96_Sub19) this).anInt9413 = class282_sub35.readUnsignedByte() * 885452997;
		((Class96_Sub19) this).anInt9409 = class282_sub35.method13082((short) 21871) * -229470439;
		((Class96_Sub19) this).anInt9412 = class282_sub35.readUnsignedShort() * -1446164009;
		((Class96_Sub19) this).anInt9399 = class282_sub35.readUnsignedByte() * 2139724261;
	}

	boolean method1596() {
		Class525 class525 = Class96_Sub20.aClass515_9416.method8845((((Class96_Sub19) this).anInt9410) * 899631601, (byte) -24);
		boolean bool = class525.method11230(-524786959);
		Class518 class518 = Class330.aClass523_3868.method11205(class525.anInt6977 * -364555849, (byte) 12);
		bool &= class518.method11132(2022792810);
		return bool;
	}

	boolean method1591() {
		Class525 class525 = Class96_Sub20.aClass515_9416.method8845((((Class96_Sub19) this).anInt9410) * 899631601, (byte) -73);
		boolean bool = class525.method11230(74376582);
		Class518 class518 = Class330.aClass523_3868.method11205(class525.anInt6977 * -364555849, (byte) 58);
		bool &= class518.method11132(2116421018);
		return bool;
	}

	public static final void method14665(String string, int i) {
		if (string != null) {
			String string_14_ = Class383.method6515(string, 1942118537);
			if (null != string_14_) {
				for (int i_15_ = 0; i_15_ < -1754449153 * client.anInt7373; i_15_++) {
					Class10 class10 = client.aClass10Array7456[i_15_];
					String string_16_ = class10.aString115;
					String string_17_ = Class383.method6515(string_16_, 1942118537);
					if (Class159.method2734(string, string_14_, string_16_, string_17_, (byte) -110)) {
						client.anInt7373 -= -637942529;
						for (int i_18_ = i_15_; i_18_ < -1754449153 * client.anInt7373; i_18_++)
							client.aClass10Array7456[i_18_] = client.aClass10Array7456[1 + i_18_];
						client.anInt7386 = client.anInt7347 * 23579151;
						Class184 class184 = Class468_Sub20.method12807(-1089718324);
						Class282_Sub23 class282_sub23 = Class271.method4828(OutgoingPacket.REMOVE_IGNORE_PACKET, class184.aClass432_2283, 1422272717);
						class282_sub23.buffer.writeByte(Class108.method1846(string, -984668828));
						class282_sub23.buffer.writeString(string);
						class184.method3049(class282_sub23, 1742869197);
						break;
					}
				}
			}
		}
	}

	public static void method14666(String string, boolean bool, int i, String string_19_, int i_20_) {
		Class215.method3673(string, bool, i, -1, string_19_, true, -857703512);
	}

	static final void method14667(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = client.anInt7300 * 550395357;
	}
}
