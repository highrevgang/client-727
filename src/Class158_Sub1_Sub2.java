
/* Class158_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Class158_Sub1_Sub2 extends Class158_Sub1 {
	Class192 aClass192_10126;
	Class125 aClass125_10127;
	int anInt10128;
	int anInt10129 = 0;
	Class505_Sub3 aClass505_Sub3_10130;
	static int anInt10131;

	public int method2719() {
		return 1324847185 * ((Class158_Sub1_Sub2) this).anInt10129;
	}

	public int method2714() {
		return 1324847185 * ((Class158_Sub1_Sub2) this).anInt10129;
	}

	public int method2720() {
		return 1324847185 * ((Class158_Sub1_Sub2) this).anInt10129;
	}

	public void method13759(int i, Interface9 interface9) {
		if (0 != i)
			throw new RuntimeException();
		Class192 class192 = (Class192) interface9;
		if (null != ((Class158_Sub1_Sub2) this).aClass125_10127 && class192 != null && ((240028587 * ((Class192) class192).anInt2388 != -2020594833 * ((Class125) (((Class158_Sub1_Sub2) this).aClass125_10127)).anInt1568) || ((((Class125) ((Class158_Sub1_Sub2) this).aClass125_10127).anInt1569) * 946359657 != ((Class192) class192).anInt2389 * 248625969)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass192_10126 = class192;
		if (null != class192) {
			((Class158_Sub1_Sub2) this).anInt10129 = -1897236165 * ((Class192) class192).anInt2388;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class192) class192).anInt2389 * 1364316587;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass125_10127) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 123))
			method213();
	}

	public void method13765(Interface8 interface8) {
		Class125 class125 = (Class125) interface8;
		if (null != ((Class158_Sub1_Sub2) this).aClass192_10126 && class125 != null && (((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2388) * 240028587 != -2020594833 * ((Class125) class125).anInt1568) || ((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2389) * 248625969 != 946359657 * ((Class125) class125).anInt1569)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass125_10127 = class125;
		if (null != class125) {
			((Class158_Sub1_Sub2) this).anInt10129 = 202035135 * ((Class125) class125).anInt1568;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class125) class125).anInt1569 * 1876541843;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass192_10126) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 122))
			method213();
	}

	public boolean method13764() {
		return true;
	}

	public int method2716() {
		return 1050215059 * ((Class158_Sub1_Sub2) this).anInt10128;
	}

	boolean method2717() {
		return true;
	}

	boolean method2718() {
		return true;
	}

	public int method2726() {
		return 1324847185 * ((Class158_Sub1_Sub2) this).anInt10129;
	}

	boolean method213() {
		((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method14376(1324847185 * ((Class158_Sub1_Sub2) this).anInt10129, 1050215059 * ((Class158_Sub1_Sub2) this).anInt10128, (((Class158_Sub1_Sub2) this).aClass192_10126 == null ? null : (((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anIntArray2390)), (((Class158_Sub1_Sub2) this).aClass125_10127 == null ? null : (((Class125) ((Class158_Sub1_Sub2) this).aClass125_10127).aFloatArray1570)));
		return true;
	}

	public void method212() {
		/* empty */
	}

	public void method13760(int i, Interface9 interface9) {
		if (0 != i)
			throw new RuntimeException();
		Class192 class192 = (Class192) interface9;
		if (null != ((Class158_Sub1_Sub2) this).aClass125_10127 && class192 != null && ((240028587 * ((Class192) class192).anInt2388 != -2020594833 * ((Class125) (((Class158_Sub1_Sub2) this).aClass125_10127)).anInt1568) || ((((Class125) ((Class158_Sub1_Sub2) this).aClass125_10127).anInt1569) * 946359657 != ((Class192) class192).anInt2389 * 248625969)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass192_10126 = class192;
		if (null != class192) {
			((Class158_Sub1_Sub2) this).anInt10129 = -1897236165 * ((Class192) class192).anInt2388;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class192) class192).anInt2389 * 1364316587;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass125_10127) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 113))
			method213();
	}

	public void method186() {
		/* empty */
	}

	Class158_Sub1_Sub2(Class505_Sub3 class505_sub3) {
		((Class158_Sub1_Sub2) this).anInt10128 = 0;
		((Class158_Sub1_Sub2) this).aClass505_Sub3_10130 = class505_sub3;
	}

	public void method13763(Interface8 interface8) {
		Class125 class125 = (Class125) interface8;
		if (null != ((Class158_Sub1_Sub2) this).aClass192_10126 && class125 != null && (((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2388) * 240028587 != -2020594833 * ((Class125) class125).anInt1568) || ((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2389) * 248625969 != 946359657 * ((Class125) class125).anInt1569)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass125_10127 = class125;
		if (null != class125) {
			((Class158_Sub1_Sub2) this).anInt10129 = 202035135 * ((Class125) class125).anInt1568;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class125) class125).anInt1569 * 1876541843;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass192_10126) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 115))
			method213();
	}

	public void method13757(Interface8 interface8) {
		Class125 class125 = (Class125) interface8;
		if (null != ((Class158_Sub1_Sub2) this).aClass192_10126 && class125 != null && (((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2388) * 240028587 != -2020594833 * ((Class125) class125).anInt1568) || ((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2389) * 248625969 != 946359657 * ((Class125) class125).anInt1569)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass125_10127 = class125;
		if (null != class125) {
			((Class158_Sub1_Sub2) this).anInt10129 = 202035135 * ((Class125) class125).anInt1568;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class125) class125).anInt1569 * 1876541843;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass192_10126) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 112))
			method213();
	}

	boolean method2725() {
		return true;
	}

	public boolean method13766() {
		return true;
	}

	boolean method211() {
		((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method14376(1324847185 * ((Class158_Sub1_Sub2) this).anInt10129, 1050215059 * ((Class158_Sub1_Sub2) this).anInt10128, (((Class158_Sub1_Sub2) this).aClass192_10126 == null ? null : (((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anIntArray2390)), (((Class158_Sub1_Sub2) this).aClass125_10127 == null ? null : (((Class125) ((Class158_Sub1_Sub2) this).aClass125_10127).aFloatArray1570)));
		return true;
	}

	boolean method54() {
		((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method14376(1324847185 * ((Class158_Sub1_Sub2) this).anInt10129, 1050215059 * ((Class158_Sub1_Sub2) this).anInt10128, (((Class158_Sub1_Sub2) this).aClass192_10126 == null ? null : (((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anIntArray2390)), (((Class158_Sub1_Sub2) this).aClass125_10127 == null ? null : (((Class125) ((Class158_Sub1_Sub2) this).aClass125_10127).aFloatArray1570)));
		return true;
	}

	boolean method2723() {
		return true;
	}

	boolean method2715() {
		return true;
	}

	public void method13761(int i, Interface9 interface9) {
		if (0 != i)
			throw new RuntimeException();
		Class192 class192 = (Class192) interface9;
		if (null != ((Class158_Sub1_Sub2) this).aClass125_10127 && class192 != null && ((240028587 * ((Class192) class192).anInt2388 != -2020594833 * ((Class125) (((Class158_Sub1_Sub2) this).aClass125_10127)).anInt1568) || ((((Class125) ((Class158_Sub1_Sub2) this).aClass125_10127).anInt1569) * 946359657 != ((Class192) class192).anInt2389 * 248625969)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass192_10126 = class192;
		if (null != class192) {
			((Class158_Sub1_Sub2) this).anInt10129 = -1897236165 * ((Class192) class192).anInt2388;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class192) class192).anInt2389 * 1364316587;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass125_10127) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 111))
			method213();
	}

	boolean method2724() {
		return true;
	}

	public void method13762(int i, Interface9 interface9) {
		if (0 != i)
			throw new RuntimeException();
		Class192 class192 = (Class192) interface9;
		if (null != ((Class158_Sub1_Sub2) this).aClass125_10127 && class192 != null && ((240028587 * ((Class192) class192).anInt2388 != -2020594833 * ((Class125) (((Class158_Sub1_Sub2) this).aClass125_10127)).anInt1568) || ((((Class125) ((Class158_Sub1_Sub2) this).aClass125_10127).anInt1569) * 946359657 != ((Class192) class192).anInt2389 * 248625969)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass192_10126 = class192;
		if (null != class192) {
			((Class158_Sub1_Sub2) this).anInt10129 = -1897236165 * ((Class192) class192).anInt2388;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class192) class192).anInt2389 * 1364316587;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass125_10127) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 118))
			method213();
	}

	boolean method2722() {
		return true;
	}

	public int method2721() {
		return 1050215059 * ((Class158_Sub1_Sub2) this).anInt10128;
	}

	public int method2727() {
		return 1050215059 * ((Class158_Sub1_Sub2) this).anInt10128;
	}

	public void method13758(Interface8 interface8) {
		Class125 class125 = (Class125) interface8;
		if (null != ((Class158_Sub1_Sub2) this).aClass192_10126 && class125 != null && (((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2388) * 240028587 != -2020594833 * ((Class125) class125).anInt1568) || ((((Class192) ((Class158_Sub1_Sub2) this).aClass192_10126).anInt2389) * 248625969 != 946359657 * ((Class125) class125).anInt1569)))
			throw new RuntimeException();
		((Class158_Sub1_Sub2) this).aClass125_10127 = class125;
		if (null != class125) {
			((Class158_Sub1_Sub2) this).anInt10129 = 202035135 * ((Class125) class125).anInt1568;
			((Class158_Sub1_Sub2) this).anInt10128 = ((Class125) class125).anInt1569 * 1876541843;
		} else if (null == ((Class158_Sub1_Sub2) this).aClass192_10126) {
			((Class158_Sub1_Sub2) this).anInt10129 = 0;
			((Class158_Sub1_Sub2) this).anInt10128 = 0;
		}
		if (this == ((Class158_Sub1_Sub2) this).aClass505_Sub3_10130.method8523((byte) 124))
			method213();
	}

	public static final void method15535(int i, String string, Color color, Color color_0_, Color color_1_, int i_2_) {
		try {
			Graphics graphics = Class351.aCanvas4096.getGraphics();
			if (null == Class515.aFont5893)
				Class515.aFont5893 = new Font("Helvetica", 1, 13);
			if (color == null)
				color = new Color(140, 17, 17);
			if (color_0_ == null)
				color_0_ = new Color(140, 17, 17);
			if (null == color_1_)
				color_1_ = new Color(255, 255, 255);
			try {
				if (OutputStream_Sub1.anImage7953 == null)
					OutputStream_Sub1.anImage7953 = (Class351.aCanvas4096.createImage(-418109423 * Class349.anInt4083, -969250379 * Class263.anInt3243));
				Graphics graphics_3_ = OutputStream_Sub1.anImage7953.getGraphics();
				graphics_3_.setColor(Color.black);
				graphics_3_.fillRect(0, 0, Class349.anInt4083 * -418109423, -969250379 * Class263.anInt3243);
				int i_4_ = Class349.anInt4083 * -418109423 / 2 - 152;
				int i_5_ = -969250379 * Class263.anInt3243 / 2 - 18;
				graphics_3_.setColor(color_0_);
				graphics_3_.drawRect(i_4_, i_5_, 303, 33);
				graphics_3_.setColor(color);
				graphics_3_.fillRect(2 + i_4_, i_5_ + 2, i * 3, 30);
				graphics_3_.setColor(Color.black);
				graphics_3_.drawRect(i_4_ + 1, i_5_ + 1, 301, 31);
				graphics_3_.fillRect(i * 3 + (i_4_ + 2), i_5_ + 2, 300 - 3 * i, 30);
				graphics_3_.setFont(Class515.aFont5893);
				graphics_3_.setColor(color_1_);
				graphics_3_.drawString(string, i_4_ + (304 - string.length() * 6) / 2, 22 + i_5_);
				if (Class263.aString3252 != null) {
					graphics_3_.setFont(Class515.aFont5893);
					graphics_3_.setColor(color_1_);
					graphics_3_.drawString(Class263.aString3252, (Class349.anInt4083 * -418109423 / 2 - (Class263.aString3252.length() * 6 / 2)), (Class263.anInt3243 * -969250379 / 2 - 26));
				}
				graphics.drawImage(OutputStream_Sub1.anImage7953, 0, 0, null);
			} catch (Exception exception) {
				graphics.setColor(Color.black);
				graphics.fillRect(0, 0, -418109423 * Class349.anInt4083, -969250379 * Class263.anInt3243);
				int i_6_ = Class349.anInt4083 * -418109423 / 2 - 152;
				int i_7_ = -969250379 * Class263.anInt3243 / 2 - 18;
				graphics.setColor(color_0_);
				graphics.drawRect(i_6_, i_7_, 303, 33);
				graphics.setColor(color);
				graphics.fillRect(2 + i_6_, 2 + i_7_, 3 * i, 30);
				graphics.setColor(Color.black);
				graphics.drawRect(i_6_ + 1, i_7_ + 1, 301, 31);
				graphics.fillRect(3 * i + (i_6_ + 2), i_7_ + 2, 300 - 3 * i, 30);
				graphics.setFont(Class515.aFont5893);
				graphics.setColor(color_1_);
				if (Class263.aString3252 != null) {
					graphics.setFont(Class515.aFont5893);
					graphics.setColor(color_1_);
					graphics.drawString(Class263.aString3252, (-418109423 * Class349.anInt4083 / 2 - (Class263.aString3252.length() * 6 / 2)), (-969250379 * Class263.anInt3243 / 2 - 26));
				}
				graphics.drawString(string, i_6_ + (304 - string.length() * 6) / 2, 22 + i_7_);
			}
		} catch (Exception exception) {
			Class351.aCanvas4096.repaint();
		}
	}

	public static void method15536(int i) {
		for (Class282_Sub37 class282_sub37 = ((Class282_Sub37) Class492.aClass465_5774.method7750(1144665389)); class282_sub37 != null; class282_sub37 = (Class282_Sub37) Class492.aClass465_5774.method7751((byte) 17)) {
			if (((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4921((byte) -124))
				Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), 1917715893);
			else {
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method13447(-947293525);
				try {
					((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4932(-1422369242);
				} catch (Exception exception) {
					Class151.method2594(new StringBuilder().append("").append((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683).toString(), exception, (byte) -82);
					Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683, 1917715893);
				}
				if (!((Class282_Sub37) class282_sub37).aBool8000 && !((Class282_Sub37) class282_sub37).aBool7995) {
					Class282_Sub41_Sub4 class282_sub41_sub4 = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4914(-1341767433);
					if (class282_sub41_sub4 != null) {
						Class282_Sub15_Sub1 class282_sub15_sub1 = class282_sub41_sub4.method14845(2048602056);
						if (class282_sub15_sub1 != null) {
							class282_sub15_sub1.method14820(961890651 * (((Class282_Sub37) class282_sub37).anInt7997), 1903554942);
							Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub1);
							((Class282_Sub37) class282_sub37).aBool8000 = true;
						}
					}
				}
			}
		}
	}
}
