/* Class413 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class413 implements Interface43 {
	static Class413 aClass413_4964;
	static Class413 aClass413_4965;
	static Class413 aClass413_4966;
	static Class413 aClass413_4967;
	static Class413 aClass413_4968;
	static Class413 aClass413_4969;
	static Class413 aClass413_4970;
	static Class413 aClass413_4971 = new Class413(1, 0, "", "");
	int anInt4972;
	int anInt4973;
	String aString4974;

	public String method6936() {
		return ((Class413) this).aString4974;
	}

	public String method6937() {
		return ((Class413) this).aString4974;
	}

	public int method243(byte i) {
		return ((Class413) this).anInt4973 * -1048592359;
	}

	public String toString() {
		return ((Class413) this).aString4974;
	}

	static {
		aClass413_4965 = new Class413(6, 1, "", "");
		aClass413_4968 = new Class413(7, 2, "", "");
		aClass413_4967 = new Class413(3, 3, "", "");
		aClass413_4970 = new Class413(4, 4, "", "");
		aClass413_4969 = new Class413(5, 5, "", "");
		aClass413_4964 = new Class413(0, 6, "", "");
		aClass413_4966 = new Class413(2, -1, "", "", true, new Class413[] { aClass413_4971, aClass413_4965, aClass413_4968, aClass413_4970, aClass413_4967 });
	}

	Class413(int i, int i_0_, String string, String string_1_) {
		((Class413) this).anInt4972 = i * 177674421;
		((Class413) this).anInt4973 = i_0_ * -1733059543;
		((Class413) this).aString4974 = string_1_;
	}

	Class413(int i, int i_2_, String string, String string_3_, boolean bool, Class413[] class413s) {
		((Class413) this).anInt4972 = i * 177674421;
		((Class413) this).anInt4973 = i_2_ * -1733059543;
		((Class413) this).aString4974 = string_3_;
	}

	public int method75() {
		return ((Class413) this).anInt4973 * -1048592359;
	}

	public int method76() {
		return ((Class413) this).anInt4973 * -1048592359;
	}

	static final void method6938(Class527 class527, byte i) {
		((Class527) class527).anInt7012 -= 425673003;
		byte[] is = { (byte) ((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537] };
		byte[] is_4_ = { (byte) ((Class527) class527).anIntArray6999[1 + 1942118537 * ((Class527) class527).anInt7012] };
		int i_5_ = (((Class527) class527).anIntArray6999[2 + 1942118537 * ((Class527) class527).anInt7012]);
		Class118 class118 = Class117.method1981(i_5_, (byte) 48);
		Class107.method1838(class118, is, is_4_, class527, 1844755981);
	}

	static void method6939(byte i) {
		Class282_Sub30.aClass229_7712.method3859(1403175614);
	}

	static final void method6940(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 425673003;
		int i_6_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_7_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 + 1]);
		int i_8_ = (((Class527) class527).anIntArray6999[2 + ((Class527) class527).anInt7012 * 1942118537]);
		Class96_Sub10.method14603(6, i_6_ << 16 | i_7_, i_8_, "", (byte) 43);
	}

	static final void method6941(Class527 class527, int i) {
		int i_9_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_9_, (byte) 102);
		Class282.method5001(class118, class527, -11442386);
	}

	static final void method6942(Class118 class118, Class527 class527, int i) {
		if (client.method11633(class118).method12182(1317632638) && client.aClass118_7352 == null) {
			Class158_Sub2.method14355(class118.anInt1287 * -1952846363, 1924549737 * class118.anInt1288, -946546687);
			client.aClass118_7352 = Class317.method5694(-1952846363 * class118.anInt1287, 1924549737 * class118.anInt1288, -1963750401);
			Class109.method1858(client.aClass118_7352, (byte) -38);
		}
	}
}
