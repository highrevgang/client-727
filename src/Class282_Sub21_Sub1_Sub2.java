
/* Class282_Sub21_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jaggl.OpenGL;

public class Class282_Sub21_Sub1_Sub2 extends Class282_Sub21_Sub1 {
	int[] anIntArray10308 = null;
	Class101_Sub1 aClass101_Sub1_10309;

	public final int method15460(int i) {
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i];
	}

	final int method15788() {
		return (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1714(-2105255554)]);
	}

	public boolean method15471(int i) {
		if (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 == null)
			((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 = new int[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1650(1785758904)];
		Class115_Sub2 class115_sub2 = ((Class115_Sub2) ((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1652(i, (byte) 117));
		((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] = OpenGL.glGetUniformLocation((((Class115_Sub2) class115_sub2).anInt9423), method15454(363103679));
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] != -1;
	}

	public final int method15455(int i) {
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i];
	}

	public final int method15472(int i) {
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i];
	}

	public boolean method15461(int i) {
		if (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 == null)
			((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 = new int[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1650(1813156673)];
		Class115_Sub2 class115_sub2 = ((Class115_Sub2) ((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1652(i, (byte) 36));
		((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] = OpenGL.glGetUniformLocation((((Class115_Sub2) class115_sub2).anInt9423), method15454(363103679));
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] != -1;
	}

	public boolean method15462(int i) {
		if (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 == null)
			((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 = new int[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1650(1812751188)];
		Class115_Sub2 class115_sub2 = ((Class115_Sub2) ((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1652(i, (byte) 86));
		((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] = OpenGL.glGetUniformLocation((((Class115_Sub2) class115_sub2).anInt9423), method15454(363103679));
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] != -1;
	}

	public boolean method15463(int i) {
		if (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 == null)
			((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 = new int[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1650(749594059)];
		Class115_Sub2 class115_sub2 = ((Class115_Sub2) ((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1652(i, (byte) 105));
		((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] = OpenGL.glGetUniformLocation((((Class115_Sub2) class115_sub2).anInt9423), method15454(363103679));
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] != -1;
	}

	public final int method15453(int i) {
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i];
	}

	public final int method15473(int i) {
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i];
	}

	final int method15789() {
		return (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1714(-2030777244)]);
	}

	Class282_Sub21_Sub1_Sub2(Class101_Sub1 class101_sub1, Class122 class122) {
		super(class122);
		((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309 = class101_sub1;
	}

	public boolean method15470(int i) {
		if (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 == null)
			((Class282_Sub21_Sub1_Sub2) this).anIntArray10308 = new int[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1650(1694397767)];
		Class115_Sub2 class115_sub2 = ((Class115_Sub2) ((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1652(i, (byte) 46));
		((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] = OpenGL.glGetUniformLocation((((Class115_Sub2) class115_sub2).anInt9423), method15454(363103679));
		return ((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[i] != -1;
	}

	final int method15790() {
		return (((Class282_Sub21_Sub1_Sub2) this).anIntArray10308[((Class282_Sub21_Sub1_Sub2) this).aClass101_Sub1_10309.method1714(-2061788489)]);
	}
}
