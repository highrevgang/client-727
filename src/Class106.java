/* Class106 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class106 {
	static Class106 aClass106_1070;
	public int anInt1071;
	static Class106 aClass106_1072;
	static Class106 aClass106_1073;
	static Class106 aClass106_1074;
	public static Class106 aClass106_1075 = new Class106(0, 104);
	public int anInt1076;
	static String[] aStringArray1077;

	public static Class106 method1808(int i) {
		Class106[] class106s = Class530.method11352(-2129730258);
		for (int i_0_ = 0; i_0_ < class106s.length; i_0_++) {
			Class106 class106 = class106s[i_0_];
			if (-530599889 * class106.anInt1071 == i)
				return class106;
		}
		return null;
	}

	static {
		aClass106_1074 = new Class106(1, 120);
		aClass106_1072 = new Class106(2, 136);
		aClass106_1073 = new Class106(3, 168);
		aClass106_1070 = new Class106(4, 72);
	}

	static Class106[] method1809() {
		return new Class106[] { aClass106_1072, aClass106_1073, aClass106_1070, aClass106_1074, aClass106_1075 };
	}

	public static Class106 method1810(int i) {
		Class106[] class106s = Class530.method11352(-1988603435);
		for (int i_1_ = 0; i_1_ < class106s.length; i_1_++) {
			Class106 class106 = class106s[i_1_];
			if (-530599889 * class106.anInt1071 == i)
				return class106;
		}
		return null;
	}

	static Class106[] method1811() {
		return new Class106[] { aClass106_1072, aClass106_1073, aClass106_1070, aClass106_1074, aClass106_1075 };
	}

	Class106(int i, int i_2_) {
		anInt1071 = i * -827615537;
		anInt1076 = i_2_ * 416091785;
	}

	static Class106[] method1812() {
		return new Class106[] { aClass106_1072, aClass106_1073, aClass106_1070, aClass106_1074, aClass106_1075 };
	}

	static final void method1813(Class527 class527, byte i) {
		int i_3_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_3_, (byte) 126);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_3_ >> 16];
		Class524.method11221(class118, class98, class527, -2007218506);
	}

	static final void method1814(Class527 class527, int i) {
		int i_4_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class108 class108 = Class180.method3032(i_4_, (byte) -1);
		int i_5_ = 0;
		if (null != class108)
			i_5_ = 905843927 * class108.anInt1086;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_5_;
	}

	static final void method1815(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		client.aShort7394 = (short) (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		if (client.aShort7394 <= 0)
			client.aShort7394 = (short) 256;
		client.aShort7324 = (short) (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]);
		if (client.aShort7324 <= 0)
			client.aShort7324 = (short) 320;
	}

	static final void method1816(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class354.anInt4112 * -1400898651;
	}

	static final void method1817(Class527 class527, byte i) {
		int i_6_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1] = Class59.method1163(i_6_, 619010179);
	}

	public static void method1818(int i, int i_7_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(8, (long) i);
		class282_sub50_sub12.method14965((byte) 5);
	}
}
