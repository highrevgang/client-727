/* Class521_Sub1_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class521_Sub1_Sub2_Sub1 extends Class521_Sub1_Sub2 {
	public int anInt10411 = 1841858001;
	public int anInt10412;
	int anInt10413;
	public int anInt10414;
	public int anInt10415 = -100349465;
	public int anInt10416;
	int anInt10417;
	int anInt10418;
	boolean aBool10419;
	int anInt10420;
	int anInt10421;
	public int anInt10422 = 0;
	public int anInt10423;

	public int method12997(int i) {
		return 1327971947 * anInt10422 - method12995(-2133263456);
	}

	void method15947(int i) {
		((Class521_Sub1_Sub2_Sub1) this).anInt10420 = (32 + (int) (Math.random() * 4.0)) * -450257987;
		((Class521_Sub1_Sub2_Sub1) this).anInt10421 = (3 + (int) (Math.random() * 2.0)) * -146187205;
		((Class521_Sub1_Sub2_Sub1) this).anInt10413 = (16 + (int) (Math.random() * 3.0)) * 1440872383;
		if (Class393.aClass282_Sub54_4783.aClass468_Sub22_8213.method12873(1014790347) == 1)
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 10.0) * 1371726999;
		else
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 20.0) * 1371726999;
	}

	boolean method12999() {
		return false;
	}

	boolean method12987(int i) {
		return ((Class521_Sub1_Sub2_Sub1) this).aBool10419;
	}

	public Class200 method12992(Class505 class505, byte i) {
		return null;
	}

	public int method12995(int i) {
		return -10;
	}

	boolean method13001() {
		return ((Class521_Sub1_Sub2_Sub1) this).aBool10419;
	}

	void method12991(Class505 class505, int i) {
		/* empty */
	}

	public Class200 method13019(Class505 class505) {
		return null;
	}

	boolean method12986(int i) {
		return false;
	}

	boolean method12983(Class505 class505, int i, int i_0_, int i_1_) {
		Class294 class294 = class505.method8450();
		class294.method5210(method11166());
		class294.method5219(0.0F, -10.0F, 0.0F);
		Class528 class528 = (Class119.aClass426_1463.method7145(anInt10423 * -876219087, 1970157841).method7084(class505, 131072, anInt10416 * 513267953, null, null, 0, 0, 0, 0, (byte) 0));
		if (null != class528 && class528.method11270(i, i_0_, class294, true, 0))
			return true;
		if (anInt10411 * 313920719 != -1) {
			class528 = (Class119.aClass426_1463.method7145(313920719 * anInt10411, 1550672616).method7084(class505, 131072, 1089437631 * anInt10414, null, null, 0, 0, 0, 0, (byte) 0));
			if (null != class528 && class528.method11270(i, i_0_, class294, true, 0))
				return true;
		}
		if (-1 != anInt10415 * -503986647) {
			class528 = (Class119.aClass426_1463.method7145(anInt10415 * -503986647, 2022809096).method7084(class505, 131072, anInt10412 * -931815553, null, null, 0, 0, 0, 0, (byte) 0));
			if (class528 != null && class528.method11270(i, i_0_, class294, true, 0))
				return true;
		}
		return false;
	}

	public int method13003() {
		return -10;
	}

	public int method13006() {
		return -10;
	}

	public int method13028() {
		return 1327971947 * anInt10422 - method12995(-1120755049);
	}

	boolean method13002() {
		return ((Class521_Sub1_Sub2_Sub1) this).aBool10419;
	}

	Class285 method13009(Class505 class505) {
		Class385 class385 = Class385.method6623(method11166().aClass385_3595);
		Class208 class208 = aClass206_7970.method3507(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, (byte) -112);
		Class521_Sub1_Sub3 class521_sub1_sub3 = aClass206_7970.method3415(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, -387297653);
		int i = 0;
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			i = class208.aClass521_Sub1_Sub1_2659.method12995(21913800);
		if (null != class521_sub1_sub3 && class521_sub1_sub3.aShort9561 > -i)
			i = -class521_sub1_sub3.aShort9561;
		if (i != 1327971947 * anInt10422) {
			class385.aFloat4672 += (float) (i - 1327971947 * anInt10422);
			method11171(class385);
			anInt10422 = -460947901 * i;
		}
		Class294 class294 = class505.method8450();
		class294.method5212();
		if (0 == anInt10422 * 1327971947) {
			boolean bool = false;
			boolean bool_2_ = false;
			boolean bool_3_ = false;
			Class390 class390 = aClass206_7970.aClass390Array2591[aByte7968];
			int i_4_ = 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418 << 1;
			int i_5_ = i_4_;
			int i_6_ = -i_4_ / 2;
			int i_7_ = -i_5_ / 2;
			int i_8_ = class390.method6709((int) class385.aFloat4671 + i_6_, (int) class385.aFloat4673 + i_7_, 2105992060);
			int i_9_ = i_4_ / 2;
			int i_10_ = -i_5_ / 2;
			int i_11_ = class390.method6709((int) class385.aFloat4671 + i_9_, i_10_ + (int) class385.aFloat4673, 1157857956);
			int i_12_ = -i_4_ / 2;
			int i_13_ = i_5_ / 2;
			int i_14_ = class390.method6709((int) class385.aFloat4671 + i_12_, i_13_ + (int) class385.aFloat4673, -102791110);
			int i_15_ = i_4_ / 2;
			int i_16_ = i_5_ / 2;
			int i_17_ = class390.method6709((int) class385.aFloat4671 + i_15_, (int) class385.aFloat4673 + i_16_, 63414596);
			int i_18_ = i_8_ < i_11_ ? i_8_ : i_11_;
			int i_19_ = i_14_ < i_17_ ? i_14_ : i_17_;
			int i_20_ = i_11_ < i_17_ ? i_11_ : i_17_;
			int i_21_ = i_8_ < i_14_ ? i_8_ : i_14_;
			if (0 != i_5_) {
				int i_22_ = ((int) (Math.atan2((double) (i_18_ - i_19_), (double) i_5_) * 2607.5945876176133) & 0x3fff);
				if (0 != i_22_)
					class294.method5220(1.0F, 0.0F, 0.0F, Class382.method6508(i_22_));
			}
			if (i_4_ != 0) {
				int i_23_ = ((int) (Math.atan2((double) (i_21_ - i_20_), (double) i_4_) * 2607.5945876176133) & 0x3fff);
				if (i_23_ != 0)
					class294.method5220(0.0F, 0.0F, 1.0F, Class382.method6508(-i_23_));
			}
			int i_24_ = i_8_ + i_17_;
			if (i_11_ + i_14_ < i_24_)
				i_24_ = i_11_ + i_14_;
			i_24_ = (i_24_ >> 1) - (int) class385.aFloat4672;
			if (i_24_ != 0)
				class294.method5219(0.0F, (float) i_24_, 0.0F);
		}
		class385.method6624();
		Class385 class385_25_ = method11166().aClass385_3595;
		class294.method5219(class385_25_.aFloat4671, class385_25_.aFloat4672 - 10.0F, class385_25_.aFloat4673);
		Class285 class285 = Class470.method7824(true, (byte) 2);
		((Class521_Sub1_Sub2_Sub1) this).aBool10419 = false;
		((Class521_Sub1_Sub2_Sub1) this).anInt10418 = 0;
		if (-503986647 * anInt10415 != -1) {
			Class528 class528 = (Class119.aClass426_1463.method7145(-503986647 * anInt10415, 1566081376).method7084(class505, 526336, anInt10412 * -931815553, null, null, ((Class521_Sub1_Sub2_Sub1) this).anInt10420 * 225048469, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[2], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		if (-1 != anInt10411 * 313920719) {
			Class528 class528 = (Class119.aClass426_1463.method7145(anInt10411 * 313920719, 627296952).method7084(class505, 526336, 1089437631 * anInt10414, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, -731570957 * ((Class521_Sub1_Sub2_Sub1) this).anInt10421, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[1], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				if (class528.n() > ((Class521_Sub1_Sub2_Sub1) this).anInt10418 * 599728753)
					((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		Class528 class528 = (Class119.aClass426_1463.method7145(-876219087 * anInt10423, 1397021511).method7084(class505, 526336, anInt10416 * 513267953, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
		if (null != class528) {
			class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
			((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
			if (class528.n() > 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418)
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
		}
		return class285;
	}

	public int method13017() {
		return -10;
	}

	public int method13005() {
		return -10;
	}

	int method12996(int i) {
		Class425 class425 = Class119.aClass426_1463.method7145(-876219087 * anInt10423, 688788238);
		int i_26_ = class425.anInt5098 * 1420665221;
		if (anInt10411 * 313920719 != -1) {
			Class425 class425_27_ = Class119.aClass426_1463.method7145(313920719 * anInt10411, 448963833);
			if (1420665221 * class425_27_.anInt5098 > i_26_)
				i_26_ = 1420665221 * class425_27_.anInt5098;
		}
		if (-1 != -503986647 * anInt10415) {
			Class425 class425_28_ = Class119.aClass426_1463.method7145(-503986647 * anInt10415, 2069769929);
			if (class425_28_.anInt5098 * 1420665221 > i_26_)
				i_26_ = 1420665221 * class425_28_.anInt5098;
		}
		return i_26_;
	}

	public int method13007() {
		return 1327971947 * anInt10422 - method12995(-1757983208);
	}

	public Class521_Sub1_Sub2_Sub1(Class206 class206, int i, int i_29_, int i_30_, int i_31_, int i_32_) {
		super(class206, i, i_29_, i_30_, i_31_, i_32_);
		((Class521_Sub1_Sub2_Sub1) this).anInt10418 = 0;
		((Class521_Sub1_Sub2_Sub1) this).aBool10419 = false;
		method13008(3, (byte) -104);
		method15947(1890677127);
	}

	boolean method13000() {
		return ((Class521_Sub1_Sub2_Sub1) this).aBool10419;
	}

	Class285 method13010(Class505 class505) {
		Class385 class385 = Class385.method6623(method11166().aClass385_3595);
		Class208 class208 = aClass206_7970.method3507(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, (byte) 62);
		Class521_Sub1_Sub3 class521_sub1_sub3 = aClass206_7970.method3415(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, -387297653);
		int i = 0;
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			i = class208.aClass521_Sub1_Sub1_2659.method12995(-2077263748);
		if (null != class521_sub1_sub3 && class521_sub1_sub3.aShort9561 > -i)
			i = -class521_sub1_sub3.aShort9561;
		if (i != 1327971947 * anInt10422) {
			class385.aFloat4672 += (float) (i - 1327971947 * anInt10422);
			method11171(class385);
			anInt10422 = -460947901 * i;
		}
		Class294 class294 = class505.method8450();
		class294.method5212();
		if (0 == anInt10422 * 1327971947) {
			boolean bool = false;
			boolean bool_33_ = false;
			boolean bool_34_ = false;
			Class390 class390 = aClass206_7970.aClass390Array2591[aByte7968];
			int i_35_ = 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418 << 1;
			int i_36_ = i_35_;
			int i_37_ = -i_35_ / 2;
			int i_38_ = -i_36_ / 2;
			int i_39_ = class390.method6709((int) class385.aFloat4671 + i_37_, (int) class385.aFloat4673 + i_38_, 1641785657);
			int i_40_ = i_35_ / 2;
			int i_41_ = -i_36_ / 2;
			int i_42_ = class390.method6709((int) class385.aFloat4671 + i_40_, i_41_ + (int) class385.aFloat4673, -450358171);
			int i_43_ = -i_35_ / 2;
			int i_44_ = i_36_ / 2;
			int i_45_ = class390.method6709((int) class385.aFloat4671 + i_43_, i_44_ + (int) class385.aFloat4673, -1463134594);
			int i_46_ = i_35_ / 2;
			int i_47_ = i_36_ / 2;
			int i_48_ = class390.method6709((int) class385.aFloat4671 + i_46_, (int) class385.aFloat4673 + i_47_, -1500462449);
			int i_49_ = i_39_ < i_42_ ? i_39_ : i_42_;
			int i_50_ = i_45_ < i_48_ ? i_45_ : i_48_;
			int i_51_ = i_42_ < i_48_ ? i_42_ : i_48_;
			int i_52_ = i_39_ < i_45_ ? i_39_ : i_45_;
			if (0 != i_36_) {
				int i_53_ = ((int) (Math.atan2((double) (i_49_ - i_50_), (double) i_36_) * 2607.5945876176133) & 0x3fff);
				if (0 != i_53_)
					class294.method5220(1.0F, 0.0F, 0.0F, Class382.method6508(i_53_));
			}
			if (i_35_ != 0) {
				int i_54_ = ((int) (Math.atan2((double) (i_52_ - i_51_), (double) i_35_) * 2607.5945876176133) & 0x3fff);
				if (i_54_ != 0)
					class294.method5220(0.0F, 0.0F, 1.0F, Class382.method6508(-i_54_));
			}
			int i_55_ = i_39_ + i_48_;
			if (i_42_ + i_45_ < i_55_)
				i_55_ = i_42_ + i_45_;
			i_55_ = (i_55_ >> 1) - (int) class385.aFloat4672;
			if (i_55_ != 0)
				class294.method5219(0.0F, (float) i_55_, 0.0F);
		}
		class385.method6624();
		Class385 class385_56_ = method11166().aClass385_3595;
		class294.method5219(class385_56_.aFloat4671, class385_56_.aFloat4672 - 10.0F, class385_56_.aFloat4673);
		Class285 class285 = Class470.method7824(true, (byte) -32);
		((Class521_Sub1_Sub2_Sub1) this).aBool10419 = false;
		((Class521_Sub1_Sub2_Sub1) this).anInt10418 = 0;
		if (-503986647 * anInt10415 != -1) {
			Class528 class528 = (Class119.aClass426_1463.method7145(-503986647 * anInt10415, 1810214014).method7084(class505, 526336, anInt10412 * -931815553, null, null, ((Class521_Sub1_Sub2_Sub1) this).anInt10420 * 225048469, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[2], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		if (-1 != anInt10411 * 313920719) {
			Class528 class528 = (Class119.aClass426_1463.method7145(anInt10411 * 313920719, 208623712).method7084(class505, 526336, 1089437631 * anInt10414, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, -731570957 * ((Class521_Sub1_Sub2_Sub1) this).anInt10421, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[1], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				if (class528.n() > ((Class521_Sub1_Sub2_Sub1) this).anInt10418 * 599728753)
					((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		Class528 class528 = (Class119.aClass426_1463.method7145(-876219087 * anInt10423, 1591407655).method7084(class505, 526336, anInt10416 * 513267953, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
		if (null != class528) {
			class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
			((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
			if (class528.n() > 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418)
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
		}
		return class285;
	}

	Class285 method12989(Class505 class505) {
		Class385 class385 = Class385.method6623(method11166().aClass385_3595);
		Class208 class208 = aClass206_7970.method3507(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, (byte) 88);
		Class521_Sub1_Sub3 class521_sub1_sub3 = aClass206_7970.method3415(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, -387297653);
		int i = 0;
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			i = class208.aClass521_Sub1_Sub1_2659.method12995(-1830209037);
		if (null != class521_sub1_sub3 && class521_sub1_sub3.aShort9561 > -i)
			i = -class521_sub1_sub3.aShort9561;
		if (i != 1327971947 * anInt10422) {
			class385.aFloat4672 += (float) (i - 1327971947 * anInt10422);
			method11171(class385);
			anInt10422 = -460947901 * i;
		}
		Class294 class294 = class505.method8450();
		class294.method5212();
		if (0 == anInt10422 * 1327971947) {
			boolean bool = false;
			boolean bool_57_ = false;
			boolean bool_58_ = false;
			Class390 class390 = aClass206_7970.aClass390Array2591[aByte7968];
			int i_59_ = 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418 << 1;
			int i_60_ = i_59_;
			int i_61_ = -i_59_ / 2;
			int i_62_ = -i_60_ / 2;
			int i_63_ = class390.method6709((int) class385.aFloat4671 + i_61_, (int) class385.aFloat4673 + i_62_, 1551660618);
			int i_64_ = i_59_ / 2;
			int i_65_ = -i_60_ / 2;
			int i_66_ = class390.method6709((int) class385.aFloat4671 + i_64_, i_65_ + (int) class385.aFloat4673, 250380296);
			int i_67_ = -i_59_ / 2;
			int i_68_ = i_60_ / 2;
			int i_69_ = class390.method6709((int) class385.aFloat4671 + i_67_, i_68_ + (int) class385.aFloat4673, -268709026);
			int i_70_ = i_59_ / 2;
			int i_71_ = i_60_ / 2;
			int i_72_ = class390.method6709((int) class385.aFloat4671 + i_70_, (int) class385.aFloat4673 + i_71_, 1481182627);
			int i_73_ = i_63_ < i_66_ ? i_63_ : i_66_;
			int i_74_ = i_69_ < i_72_ ? i_69_ : i_72_;
			int i_75_ = i_66_ < i_72_ ? i_66_ : i_72_;
			int i_76_ = i_63_ < i_69_ ? i_63_ : i_69_;
			if (0 != i_60_) {
				int i_77_ = ((int) (Math.atan2((double) (i_73_ - i_74_), (double) i_60_) * 2607.5945876176133) & 0x3fff);
				if (0 != i_77_)
					class294.method5220(1.0F, 0.0F, 0.0F, Class382.method6508(i_77_));
			}
			if (i_59_ != 0) {
				int i_78_ = ((int) (Math.atan2((double) (i_76_ - i_75_), (double) i_59_) * 2607.5945876176133) & 0x3fff);
				if (i_78_ != 0)
					class294.method5220(0.0F, 0.0F, 1.0F, Class382.method6508(-i_78_));
			}
			int i_79_ = i_63_ + i_72_;
			if (i_66_ + i_69_ < i_79_)
				i_79_ = i_66_ + i_69_;
			i_79_ = (i_79_ >> 1) - (int) class385.aFloat4672;
			if (i_79_ != 0)
				class294.method5219(0.0F, (float) i_79_, 0.0F);
		}
		class385.method6624();
		Class385 class385_80_ = method11166().aClass385_3595;
		class294.method5219(class385_80_.aFloat4671, class385_80_.aFloat4672 - 10.0F, class385_80_.aFloat4673);
		Class285 class285 = Class470.method7824(true, (byte) -61);
		((Class521_Sub1_Sub2_Sub1) this).aBool10419 = false;
		((Class521_Sub1_Sub2_Sub1) this).anInt10418 = 0;
		if (-503986647 * anInt10415 != -1) {
			Class528 class528 = (Class119.aClass426_1463.method7145(-503986647 * anInt10415, 1568687750).method7084(class505, 526336, anInt10412 * -931815553, null, null, ((Class521_Sub1_Sub2_Sub1) this).anInt10420 * 225048469, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[2], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		if (-1 != anInt10411 * 313920719) {
			Class528 class528 = (Class119.aClass426_1463.method7145(anInt10411 * 313920719, 849121032).method7084(class505, 526336, 1089437631 * anInt10414, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, -731570957 * ((Class521_Sub1_Sub2_Sub1) this).anInt10421, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[1], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				if (class528.n() > ((Class521_Sub1_Sub2_Sub1) this).anInt10418 * 599728753)
					((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		Class528 class528 = (Class119.aClass426_1463.method7145(-876219087 * anInt10423, 1958359567).method7084(class505, 526336, anInt10416 * 513267953, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
		if (null != class528) {
			class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
			((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
			if (class528.n() > 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418)
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
		}
		return class285;
	}

	void method13012(Class505 class505) {
		/* empty */
	}

	void method13023(Class505 class505) {
		/* empty */
	}

	boolean method13020(Class505 class505, int i, int i_81_) {
		Class294 class294 = class505.method8450();
		class294.method5210(method11166());
		class294.method5219(0.0F, -10.0F, 0.0F);
		Class528 class528 = (Class119.aClass426_1463.method7145(anInt10423 * -876219087, 1772986804).method7084(class505, 131072, anInt10416 * 513267953, null, null, 0, 0, 0, 0, (byte) 0));
		if (null != class528 && class528.method11270(i, i_81_, class294, true, 0))
			return true;
		if (anInt10411 * 313920719 != -1) {
			class528 = (Class119.aClass426_1463.method7145(313920719 * anInt10411, 505864410).method7084(class505, 131072, 1089437631 * anInt10414, null, null, 0, 0, 0, 0, (byte) 0));
			if (null != class528 && class528.method11270(i, i_81_, class294, true, 0))
				return true;
		}
		if (-1 != anInt10415 * -503986647) {
			class528 = (Class119.aClass426_1463.method7145(anInt10415 * -503986647, 1589292457).method7084(class505, 131072, anInt10412 * -931815553, null, null, 0, 0, 0, 0, (byte) 0));
			if (class528 != null && class528.method11270(i, i_81_, class294, true, 0))
				return true;
		}
		return false;
	}

	public Class200 method13018(Class505 class505) {
		return null;
	}

	public Class200 method12993(Class505 class505) {
		return null;
	}

	Class285 method12990(Class505 class505, int i) {
		Class385 class385 = Class385.method6623(method11166().aClass385_3595);
		Class208 class208 = aClass206_7970.method3507(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, (byte) -92);
		Class521_Sub1_Sub3 class521_sub1_sub3 = aClass206_7970.method3415(aByte7967, (int) class385.aFloat4671 >> 9, (int) class385.aFloat4673 >> 9, -387297653);
		int i_82_ = 0;
		if (class208 != null && class208.aClass521_Sub1_Sub1_2659.aBool9459)
			i_82_ = class208.aClass521_Sub1_Sub1_2659.method12995(-1954899292);
		if (null != class521_sub1_sub3 && class521_sub1_sub3.aShort9561 > -i_82_)
			i_82_ = -class521_sub1_sub3.aShort9561;
		if (i_82_ != 1327971947 * anInt10422) {
			class385.aFloat4672 += (float) (i_82_ - 1327971947 * anInt10422);
			method11171(class385);
			anInt10422 = -460947901 * i_82_;
		}
		Class294 class294 = class505.method8450();
		class294.method5212();
		if (0 == anInt10422 * 1327971947) {
			boolean bool = false;
			boolean bool_83_ = false;
			boolean bool_84_ = false;
			Class390 class390 = aClass206_7970.aClass390Array2591[aByte7968];
			int i_85_ = 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418 << 1;
			int i_86_ = i_85_;
			int i_87_ = -i_85_ / 2;
			int i_88_ = -i_86_ / 2;
			int i_89_ = class390.method6709((int) class385.aFloat4671 + i_87_, (int) class385.aFloat4673 + i_88_, 924924615);
			int i_90_ = i_85_ / 2;
			int i_91_ = -i_86_ / 2;
			int i_92_ = class390.method6709((int) class385.aFloat4671 + i_90_, i_91_ + (int) class385.aFloat4673, 533438354);
			int i_93_ = -i_85_ / 2;
			int i_94_ = i_86_ / 2;
			int i_95_ = class390.method6709((int) class385.aFloat4671 + i_93_, i_94_ + (int) class385.aFloat4673, 656324571);
			int i_96_ = i_85_ / 2;
			int i_97_ = i_86_ / 2;
			int i_98_ = class390.method6709((int) class385.aFloat4671 + i_96_, (int) class385.aFloat4673 + i_97_, -1831727640);
			int i_99_ = i_89_ < i_92_ ? i_89_ : i_92_;
			int i_100_ = i_95_ < i_98_ ? i_95_ : i_98_;
			int i_101_ = i_92_ < i_98_ ? i_92_ : i_98_;
			int i_102_ = i_89_ < i_95_ ? i_89_ : i_95_;
			if (0 != i_86_) {
				int i_103_ = ((int) (Math.atan2((double) (i_99_ - i_100_), (double) i_86_) * 2607.5945876176133) & 0x3fff);
				if (0 != i_103_)
					class294.method5220(1.0F, 0.0F, 0.0F, Class382.method6508(i_103_));
			}
			if (i_85_ != 0) {
				int i_104_ = ((int) (Math.atan2((double) (i_102_ - i_101_), (double) i_85_) * 2607.5945876176133) & 0x3fff);
				if (i_104_ != 0)
					class294.method5220(0.0F, 0.0F, 1.0F, Class382.method6508(-i_104_));
			}
			int i_105_ = i_89_ + i_98_;
			if (i_92_ + i_95_ < i_105_)
				i_105_ = i_92_ + i_95_;
			i_105_ = (i_105_ >> 1) - (int) class385.aFloat4672;
			if (i_105_ != 0)
				class294.method5219(0.0F, (float) i_105_, 0.0F);
		}
		class385.method6624();
		Class385 class385_106_ = method11166().aClass385_3595;
		class294.method5219(class385_106_.aFloat4671, class385_106_.aFloat4672 - 10.0F, class385_106_.aFloat4673);
		Class285 class285 = Class470.method7824(true, (byte) -5);
		((Class521_Sub1_Sub2_Sub1) this).aBool10419 = false;
		((Class521_Sub1_Sub2_Sub1) this).anInt10418 = 0;
		if (-503986647 * anInt10415 != -1) {
			Class528 class528 = (Class119.aClass426_1463.method7145(-503986647 * anInt10415, 2123092777).method7084(class505, 526336, anInt10412 * -931815553, null, null, ((Class521_Sub1_Sub2_Sub1) this).anInt10420 * 225048469, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[2], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		if (-1 != anInt10411 * 313920719) {
			Class528 class528 = (Class119.aClass426_1463.method7145(anInt10411 * 313920719, 687184901).method7084(class505, 526336, 1089437631 * anInt10414, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, -731570957 * ((Class521_Sub1_Sub2_Sub1) this).anInt10421, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
			if (class528 != null) {
				class528.method11282(class294, aClass275_Sub5Array7965[1], 0);
				((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
				if (class528.n() > ((Class521_Sub1_Sub2_Sub1) this).anInt10418 * 599728753)
					((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
			}
		}
		Class528 class528 = (Class119.aClass426_1463.method7145(-876219087 * anInt10423, 994725585).method7084(class505, 526336, anInt10416 * 513267953, null, null, 225048469 * ((Class521_Sub1_Sub2_Sub1) this).anInt10420, ((Class521_Sub1_Sub2_Sub1) this).anInt10421 * -731570957, 1264449599 * ((Class521_Sub1_Sub2_Sub1) this).anInt10413, -359736537 * ((Class521_Sub1_Sub2_Sub1) this).anInt10417, (byte) 0));
		if (null != class528) {
			class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
			((Class521_Sub1_Sub2_Sub1) this).aBool10419 |= class528.i();
			if (class528.n() > 599728753 * ((Class521_Sub1_Sub2_Sub1) this).anInt10418)
				((Class521_Sub1_Sub2_Sub1) this).anInt10418 = class528.n() * -1885839727;
		}
		return class285;
	}

	void method15948() {
		((Class521_Sub1_Sub2_Sub1) this).anInt10420 = (32 + (int) (Math.random() * 4.0)) * -450257987;
		((Class521_Sub1_Sub2_Sub1) this).anInt10421 = (3 + (int) (Math.random() * 2.0)) * -146187205;
		((Class521_Sub1_Sub2_Sub1) this).anInt10413 = (16 + (int) (Math.random() * 3.0)) * 1440872383;
		if (Class393.aClass282_Sub54_4783.aClass468_Sub22_8213.method12873(1643820283) == 1)
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 10.0) * 1371726999;
		else
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 20.0) * 1371726999;
	}

	void method15949() {
		((Class521_Sub1_Sub2_Sub1) this).anInt10420 = (32 + (int) (Math.random() * 4.0)) * -450257987;
		((Class521_Sub1_Sub2_Sub1) this).anInt10421 = (3 + (int) (Math.random() * 2.0)) * -146187205;
		((Class521_Sub1_Sub2_Sub1) this).anInt10413 = (16 + (int) (Math.random() * 3.0)) * 1440872383;
		if (Class393.aClass282_Sub54_4783.aClass468_Sub22_8213.method12873(614376406) == 1)
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 10.0) * 1371726999;
		else
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 20.0) * 1371726999;
	}

	void method15950() {
		((Class521_Sub1_Sub2_Sub1) this).anInt10420 = (32 + (int) (Math.random() * 4.0)) * -450257987;
		((Class521_Sub1_Sub2_Sub1) this).anInt10421 = (3 + (int) (Math.random() * 2.0)) * -146187205;
		((Class521_Sub1_Sub2_Sub1) this).anInt10413 = (16 + (int) (Math.random() * 3.0)) * 1440872383;
		if (Class393.aClass282_Sub54_4783.aClass468_Sub22_8213.method12873(-448401282) == 1)
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 10.0) * 1371726999;
		else
			((Class521_Sub1_Sub2_Sub1) this).anInt10417 = (int) (Math.random() * 20.0) * 1371726999;
	}

	int method13027() {
		Class425 class425 = Class119.aClass426_1463.method7145(-876219087 * anInt10423, 1217538762);
		int i = class425.anInt5098 * 1420665221;
		if (anInt10411 * 313920719 != -1) {
			Class425 class425_107_ = Class119.aClass426_1463.method7145(313920719 * anInt10411, 463771101);
			if (1420665221 * class425_107_.anInt5098 > i)
				i = 1420665221 * class425_107_.anInt5098;
		}
		if (-1 != -503986647 * anInt10415) {
			Class425 class425_108_ = Class119.aClass426_1463.method7145(-503986647 * anInt10415, 1280633151);
			if (class425_108_.anInt5098 * 1420665221 > i)
				i = 1420665221 * class425_108_.anInt5098;
		}
		return i;
	}
}
