/* Class228 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class228 {
	public int anInt2832;
	public int anInt2833;
	public static final int anInt2834 = 0;
	public static final int anInt2835 = 1;
	static final int anInt2836 = 16777215;
	static final int anInt2837 = 70;
	public boolean aBool2838 = false;
	public int anInt2839;
	String aString2840;
	public int anInt2841;
	int anInt2842;
	int anInt2843;
	public int anInt2844;
	int anInt2845;
	public int anInt2846;
	public int anInt2847;
	public static final int anInt2848 = -1;
	public int anInt2849 = 1924818623;
	Class210 aClass210_2850;
	int anInt2851;

	public Class160 method3831(Class505 class505) {
		if (((Class228) this).anInt2843 * 1560346643 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2843 * 1560346643)));
		if (class160 == null) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2843 * 1560346643)));
		}
		return class160;
	}

	public Class160 method3832(Class505 class505, int i) {
		if (1879760921 * ((Class228) this).anInt2851 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921)));
		if (null == class160) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921)));
		}
		return class160;
	}

	void method3833(RsByteBuffer class282_sub35, int i, int i_0_) {
		if (1 == i)
			anInt2849 = class282_sub35.readBigSmart(2145503846) * -1924818623;
		else if (i == 2) {
			anInt2844 = class282_sub35.method13082((short) 23294) * -12518251;
			aBool2838 = true;
		} else if (3 == i)
			((Class228) this).anInt2842 = class282_sub35.readBigSmart(2046192391) * -1198566823;
		else if (4 == i)
			((Class228) this).anInt2851 = class282_sub35.readBigSmart(1982250090) * 1982233641;
		else if (5 == i)
			((Class228) this).anInt2843 = class282_sub35.readBigSmart(1996501063) * -1265448421;
		else if (i == 6)
			((Class228) this).anInt2845 = class282_sub35.readBigSmart(1975161659) * 1744105937;
		else if (7 == i)
			anInt2846 = class282_sub35.method13081(2005060480) * -2072485699;
		else if (i == 8)
			((Class228) this).aString2840 = class282_sub35.method13091(-474041225);
		else if (9 == i)
			anInt2841 = class282_sub35.readUnsignedShort() * -1360322159;
		else if (i == 10)
			anInt2833 = class282_sub35.method13081(1971568647) * -2048527837;
		else if (i == 11)
			anInt2847 = 0;
		else if (i == 12)
			anInt2839 = class282_sub35.readUnsignedByte() * -813850459;
		else if (13 == i)
			anInt2832 = class282_sub35.method13081(1645619084) * -740432191;
		else if (14 == i)
			anInt2847 = class282_sub35.readUnsignedShort() * -1299082475;
	}

	void method3834(RsByteBuffer class282_sub35, int i) {
		if (1 == i)
			anInt2849 = class282_sub35.readBigSmart(2141801902) * -1924818623;
		else if (i == 2) {
			anInt2844 = class282_sub35.method13082((short) 20684) * -12518251;
			aBool2838 = true;
		} else if (3 == i)
			((Class228) this).anInt2842 = class282_sub35.readBigSmart(1987448126) * -1198566823;
		else if (4 == i)
			((Class228) this).anInt2851 = class282_sub35.readBigSmart(1993767181) * 1982233641;
		else if (5 == i)
			((Class228) this).anInt2843 = class282_sub35.readBigSmart(1951207567) * -1265448421;
		else if (i == 6)
			((Class228) this).anInt2845 = class282_sub35.readBigSmart(2113972631) * 1744105937;
		else if (7 == i)
			anInt2846 = class282_sub35.method13081(1844357187) * -2072485699;
		else if (i == 8)
			((Class228) this).aString2840 = class282_sub35.method13091(614069463);
		else if (9 == i)
			anInt2841 = class282_sub35.readUnsignedShort() * -1360322159;
		else if (i == 10)
			anInt2833 = class282_sub35.method13081(2059375932) * -2048527837;
		else if (i == 11)
			anInt2847 = 0;
		else if (i == 12)
			anInt2839 = class282_sub35.readUnsignedByte() * -813850459;
		else if (13 == i)
			anInt2832 = class282_sub35.method13081(1890334047) * -740432191;
		else if (14 == i)
			anInt2847 = class282_sub35.readUnsignedShort() * -1299082475;
	}

	public Class160 method3835(Class505 class505, int i) {
		if (((Class228) this).anInt2842 * -1860512279 < 0)
			return null;
		Class160 class160 = ((Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2842 * -1860512279))));
		if (class160 == null) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2842 * -1860512279)));
		}
		return class160;
	}

	public Class160 method3836(Class505 class505, int i) {
		if (((Class228) this).anInt2843 * 1560346643 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2843 * 1560346643)));
		if (class160 == null) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2843 * 1560346643)));
		}
		return class160;
	}

	void method3837(RsByteBuffer class282_sub35, int i) {
		for (;;) {
			int i_1_ = class282_sub35.readUnsignedByte();
			if (0 == i_1_)
				break;
			method3833(class282_sub35, i_1_, 240586301);
		}
	}

	public Class160 method3838(Class505 class505, byte i) {
		if (((Class228) this).anInt2845 * 1454442289 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2845 * 1454442289)));
		if (null == class160) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (1454442289 * ((Class228) this).anInt2845)));
		}
		return class160;
	}

	void method3839(Class505 class505, int i) {
		Class317 class317 = ((Class210) ((Class228) this).aClass210_2850).aClass317_2666;
		if (-1860512279 * ((Class228) this).anInt2842 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2842 * -1860512279))) == null && class317.method5661(-1860512279 * ((Class228) this).anInt2842, -1191844183)) {
			Class91 class91 = Class91.method1515(class317, -1860512279 * (((Class228) this).anInt2842));
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (-1860512279 * ((Class228) this).anInt2842));
		}
		if (1560346643 * ((Class228) this).anInt2843 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (1560346643 * ((Class228) this).anInt2843))) == null && class317.method5661(1560346643 * ((Class228) this).anInt2843, -794594441)) {
			Class91 class91 = Class91.method1515(class317, ((Class228) this).anInt2843 * 1560346643);
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (1560346643 * ((Class228) this).anInt2843));
		}
		if (((Class228) this).anInt2851 * 1879760921 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921))) == null && class317.method5661(((Class228) this).anInt2851 * 1879760921, 1719697328)) {
			Class91 class91 = Class91.method1515(class317, 1879760921 * ((Class228) this).anInt2851);
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (1879760921 * ((Class228) this).anInt2851));
		}
		if (((Class228) this).anInt2845 * 1454442289 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2845 * 1454442289))) == null && class317.method5661(((Class228) this).anInt2845 * 1454442289, 1508436716)) {
			Class91 class91 = Class91.method1515(class317, ((Class228) this).anInt2845 * 1454442289);
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (((Class228) this).anInt2845 * 1454442289));
		}
	}

	public Class160 method3840(Class505 class505) {
		if (((Class228) this).anInt2845 * 1454442289 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2845 * 1454442289)));
		if (null == class160) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (1454442289 * ((Class228) this).anInt2845)));
		}
		return class160;
	}

	void method3841(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (0 == i)
				break;
			method3833(class282_sub35, i, 818399020);
		}
	}

	void method3842(RsByteBuffer class282_sub35) {
		for (;;) {
			int i = class282_sub35.readUnsignedByte();
			if (0 == i)
				break;
			method3833(class282_sub35, i, -480058268);
		}
	}

	public Class160 method3843(Class505 class505) {
		if (((Class228) this).anInt2842 * -1860512279 < 0)
			return null;
		Class160 class160 = ((Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2842 * -1860512279))));
		if (class160 == null) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2842 * -1860512279)));
		}
		return class160;
	}

	public String method3844(int i, byte i_2_) {
		String string = ((Class228) this).aString2840;
		for (;;) {
			int i_3_ = string.indexOf("%1");
			if (i_3_ < 0)
				break;
			string = new StringBuilder().append(string.substring(0, i_3_)).append(Class290.method5120(i, false, 147527358)).append(string.substring(2 + i_3_)).toString();
		}
		return string;
	}

	public Class160 method3845(Class505 class505) {
		if (1879760921 * ((Class228) this).anInt2851 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921)));
		if (null == class160) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921)));
		}
		return class160;
	}

	public Class160 method3846(Class505 class505) {
		if (1879760921 * ((Class228) this).anInt2851 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921)));
		if (null == class160) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921)));
		}
		return class160;
	}

	public Class160 method3847(Class505 class505) {
		if (((Class228) this).anInt2845 * 1454442289 < 0)
			return null;
		Class160 class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2845 * 1454442289)));
		if (null == class160) {
			method3839(class505, -238041462);
			class160 = (Class160) (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (1454442289 * ((Class228) this).anInt2845)));
		}
		return class160;
	}

	Class228() {
		anInt2844 = -1782643861;
		anInt2841 = -733270618;
		((Class228) this).anInt2842 = 1198566823;
		((Class228) this).anInt2843 = 1265448421;
		((Class228) this).anInt2851 = -1982233641;
		((Class228) this).anInt2845 = -1744105937;
		anInt2846 = 0;
		anInt2833 = 0;
		anInt2847 = 1299082475;
		((Class228) this).aString2840 = "";
		anInt2839 = 813850459;
		anInt2832 = 0;
	}

	void method3848(Class505 class505) {
		Class317 class317 = ((Class210) ((Class228) this).aClass210_2850).aClass317_2666;
		if (-1860512279 * ((Class228) this).anInt2842 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2842 * -1860512279))) == null && class317.method5661(-1860512279 * ((Class228) this).anInt2842, 719597086)) {
			Class91 class91 = Class91.method1515(class317, -1860512279 * (((Class228) this).anInt2842));
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (-1860512279 * ((Class228) this).anInt2842));
		}
		if (1560346643 * ((Class228) this).anInt2843 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (1560346643 * ((Class228) this).anInt2843))) == null && class317.method5661(1560346643 * ((Class228) this).anInt2843, 856102236)) {
			Class91 class91 = Class91.method1515(class317, ((Class228) this).anInt2843 * 1560346643);
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (1560346643 * ((Class228) this).anInt2843));
		}
		if (((Class228) this).anInt2851 * 1879760921 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2851 * 1879760921))) == null && class317.method5661(((Class228) this).anInt2851 * 1879760921, 1106964928)) {
			Class91 class91 = Class91.method1515(class317, 1879760921 * ((Class228) this).anInt2851);
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (1879760921 * ((Class228) this).anInt2851));
		}
		if (((Class228) this).anInt2845 * 1454442289 >= 0 && (((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3865((long) (((Class228) this).anInt2845 * 1454442289))) == null && class317.method5661(((Class228) this).anInt2845 * 1454442289, -1491872897)) {
			Class91 class91 = Class91.method1515(class317, ((Class228) this).anInt2845 * 1454442289);
			((Class210) ((Class228) this).aClass210_2850).aClass229_2664.method3856(class505.method8444(class91, true), (long) (((Class228) this).anInt2845 * 1454442289));
		}
	}

	public static boolean method3849(int i, int i_4_) {
		return (i >= Class458.aClass458_5471.anInt5481 * 1109376893 && i <= Class458.aClass458_5489.anInt5481 * 1109376893);
	}

	static void method3850(Class384 class384, boolean bool, float f, float f_5_, float f_6_, float f_7_, int i, int i_8_, byte i_9_) {
		int i_10_ = client.aClass257_7353.method4522(-2126255936);
		int i_11_ = client.aClass257_7353.method4544(820314682);
		class384.method6531(f, f_5_, f_6_, f_7_, (float) i_11_, (float) i_10_, (float) i, (float) i_8_);
	}

	static void method3851(int i) {
		for (Class282_Sub50_Sub15 class282_sub50_sub15 = ((Class282_Sub50_Sub15) Class20.aClass477_182.method7941((byte) 4)); class282_sub50_sub15 != null; class282_sub50_sub15 = ((Class282_Sub50_Sub15) Class20.aClass477_182.method7955(-395453439))) {
			if ((((Class282_Sub50_Sub15) class282_sub50_sub15).anInt9769 * 2026887253) > 1) {
				((Class282_Sub50_Sub15) class282_sub50_sub15).anInt9769 = 0;
				Class20.aClass229_164.method3856(class282_sub50_sub15, ((((Class282_Sub50_Sub7) (Class282_Sub50_Sub7) (((Class282_Sub50_Sub15) class282_sub50_sub15).aClass477_9770.aClass282_Sub50_5629.aClass282_Sub50_8119)).aLong9580) * 820033947929891191L));
				((Class282_Sub50_Sub15) class282_sub50_sub15).aClass477_9770.method7935((byte) 44);
			}
		}
		Class20.anInt170 = 0;
		Class20.anInt169 = 0;
		Class20.aClass482_171.method8118(-1612336586);
		Class20.aClass465_172.method7749(-2124634600);
		Class20.aClass477_182.method7935((byte) 16);
		Class361.method6269(Class20.aClass282_Sub50_Sub7_157, -1498808978);
	}
}
