/* Class282_Sub53 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class282_Sub53 extends Class282 {
	public static final int anInt8175 = 6;
	public static final int anInt8176 = 0;
	static final int anInt8177 = 1;
	static final int anInt8178 = 3;
	public static final int anInt8179 = -1;
	public static final int anInt8180 = 2;
	static final int anInt8181 = 5;
	static final int anInt8182 = 4;

	public abstract int method13469(int i);

	public abstract int method13470(int i);

	public abstract long method13471(int i);

	public abstract int method13472();

	public abstract int method13473(byte i);

	public static boolean method13474(int i) {
		return i == 0 || i == 1 || i == 2;
	}

	public abstract void method13475(int i);

	public abstract int method13476();

	public abstract int method13477();

	public abstract int method13478();

	public abstract long method13479();

	public abstract long method13480();

	public abstract int method13481(int i);

	public abstract long method13482();

	public abstract int method13483();

	public abstract int method13484();

	public abstract void method13485();

	public abstract void method13486();

	public abstract void method13487();

	public abstract int method13488();

	public abstract int method13489();

	public static boolean method13490(int i) {
		return i == 0 || i == 1 || i == 2;
	}

	Class282_Sub53() {
		/* empty */
	}

	static final void method13491(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 2110532063 * class118.anInt1305;
	}
}
