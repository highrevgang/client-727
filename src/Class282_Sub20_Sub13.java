/* Class282_Sub20_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub20_Sub13 extends Class282_Sub20 {
	static final int anInt9815 = 6;
	static final int anInt9816 = 5;
	static final int anInt9817 = 2;
	static final int anInt9818 = 3;
	static final int anInt9819 = 0;
	static final int anInt9820 = 4;
	static final int anInt9821 = 1;
	int[][] anIntArrayArray9822;
	int[] anIntArray9823 = new int[257];

	void method15267(int i) {
		if (0 != i) {
			switch (i) {
			case 2:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[8][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 2650;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 2602;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 2361;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 2313;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 1799;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1558;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 2618;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 2296;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1220;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 2072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 963;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 722;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 2730;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 2152;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 3891;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 2232;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1060;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 915;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1140;
				break;
			case 4:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[6][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 1843;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1493;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 2457;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 2939;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2781;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1124;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 3565;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 546;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 3084;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 4031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				break;
			case 6:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[4][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				break;
			case 3:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[7][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 663;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 1363;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 2727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3411;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 4096;
				break;
			default:
				throw new RuntimeException();
			case 1:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[2][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				break;
			case 5:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[16][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 80;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 192;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 155;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 449;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 562;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 389;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 578;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 690;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 803;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 671;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 995;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 1140;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 897;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 1285;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 1397;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 1509;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 1175;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 1429;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 1368;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1461;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 1333;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 1507;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1702;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][0] = 1736;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][1] = 1108;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][2] = 1590;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][3] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][0] = 2088;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][1] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][2] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][3] = 2666;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][0] = 2355;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][1] = 2409;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][2] = 2586;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][3] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][0] = 2691;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][0] = 3031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][1] = 3806;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][2] = 3710;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][3] = 3196;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][0] = 3522;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][1] = 3437;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][2] = 3421;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][3] = 3019;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][0] = 3727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][1] = 2377;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][2] = 2505;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][3] = 2746;
			}
		}
	}

	int[][] method12320(int i, int i_0_) {
		int[][] is = aClass308_7670.method5463(i, 1432274995);
		if (aClass308_7670.aBool3619) {
			int[] is_1_ = method12317(0, i, 1992371549);
			int[] is_2_ = is[0];
			int[] is_3_ = is[1];
			int[] is_4_ = is[2];
			for (int i_5_ = 0; i_5_ < Class316.anInt3670 * -402153223; i_5_++) {
				int i_6_ = is_1_[i_5_] >> 4;
				if (i_6_ < 0)
					i_6_ = 0;
				if (i_6_ > 256)
					i_6_ = 256;
				i_6_ = ((Class282_Sub20_Sub13) this).anIntArray9823[i_6_];
				is_2_[i_5_] = (i_6_ & 0xff0000) >> 12;
				is_3_[i_5_] = (i_6_ & 0xff00) >> 4;
				is_4_[i_5_] = (i_6_ & 0xff) << 4;
			}
		}
		return is;
	}

	void method12332(int i, RsByteBuffer class282_sub35) {
		if (0 == i) {
			int i_7_ = class282_sub35.readUnsignedByte();
			if (0 == i_7_) {
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[class282_sub35.readUnsignedByte()][4];
				for (int i_8_ = 0; i_8_ < (((Class282_Sub20_Sub13) this).anIntArrayArray9822).length; i_8_++) {
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_8_][0] = class282_sub35.readUnsignedShort();
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_8_][1] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_8_][2] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_8_][3] = class282_sub35.readUnsignedByte() << 4;
				}
			} else
				method15269(i_7_, -1972652144);
		}
	}

	void method12318() {
		if (null == ((Class282_Sub20_Sub13) this).anIntArrayArray9822)
			method15269(1, -1930649384);
		method15268(242844298);
	}

	void method15268(int i) {
		int i_9_ = ((Class282_Sub20_Sub13) this).anIntArrayArray9822.length;
		if (i_9_ > 0) {
			for (int i_10_ = 0; i_10_ < 257; i_10_++) {
				int i_11_ = 0;
				int i_12_ = i_10_ << 4;
				for (int i_13_ = 0; i_13_ < i_9_ && i_12_ >= (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_13_][0]); i_13_++)
					i_11_++;
				int i_14_;
				int i_15_;
				int i_16_;
				if (i_11_ < i_9_) {
					int[] is = (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_11_]);
					if (i_11_ > 0) {
						int[] is_17_ = (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_11_ - 1]);
						int i_18_ = (i_12_ - is_17_[0] << 12) / (is[0] - is_17_[0]);
						int i_19_ = 4096 - i_18_;
						i_14_ = i_18_ * is[1] + i_19_ * is_17_[1] >> 12;
						i_15_ = is_17_[2] * i_19_ + i_18_ * is[2] >> 12;
						i_16_ = i_18_ * is[3] + is_17_[3] * i_19_ >> 12;
					} else {
						i_14_ = is[1];
						i_15_ = is[2];
						i_16_ = is[3];
					}
				} else {
					int[] is = (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_9_ - 1]);
					i_14_ = is[1];
					i_15_ = is[2];
					i_16_ = is[3];
				}
				i_14_ >>= 4;
				i_15_ >>= 4;
				i_16_ >>= 4;
				if (i_14_ < 0)
					i_14_ = 0;
				else if (i_14_ > 255)
					i_14_ = 255;
				if (i_15_ < 0)
					i_15_ = 0;
				else if (i_15_ > 255)
					i_15_ = 255;
				if (i_16_ < 0)
					i_16_ = 0;
				else if (i_16_ > 255)
					i_16_ = 255;
				((Class282_Sub20_Sub13) this).anIntArray9823[i_10_] = i_14_ << 16 | i_15_ << 8 | i_16_;
			}
		}
	}

	void method15269(int i, int i_20_) {
		if (0 != i) {
			switch (i) {
			case 2:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[8][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 2650;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 2602;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 2361;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 2313;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 1799;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1558;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 2618;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 2296;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1220;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 2072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 963;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 722;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 2730;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 2152;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 3891;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 2232;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1060;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 915;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1140;
				break;
			case 4:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[6][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 1843;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1493;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 2457;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 2939;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2781;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1124;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 3565;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 546;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 3084;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 4031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				break;
			case 6:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[4][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				break;
			case 3:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[7][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 663;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 1363;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 2727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3411;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 4096;
				break;
			default:
				throw new RuntimeException();
			case 1:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[2][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				break;
			case 5:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[16][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 80;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 192;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 155;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 449;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 562;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 389;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 578;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 690;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 803;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 671;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 995;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 1140;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 897;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 1285;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 1397;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 1509;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 1175;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 1429;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 1368;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1461;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 1333;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 1507;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1702;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][0] = 1736;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][1] = 1108;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][2] = 1590;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][3] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][0] = 2088;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][1] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][2] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][3] = 2666;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][0] = 2355;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][1] = 2409;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][2] = 2586;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][3] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][0] = 2691;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][0] = 3031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][1] = 3806;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][2] = 3710;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][3] = 3196;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][0] = 3522;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][1] = 3437;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][2] = 3421;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][3] = 3019;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][0] = 3727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][1] = 2377;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][2] = 2505;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][3] = 2746;
			}
		}
	}

	void method12321(int i) {
		if (null == ((Class282_Sub20_Sub13) this).anIntArrayArray9822)
			method15269(1, -1434032930);
		method15268(2048648154);
	}

	void method12329() {
		if (null == ((Class282_Sub20_Sub13) this).anIntArrayArray9822)
			method15269(1, -1640346696);
		method15268(1936925061);
	}

	public Class282_Sub20_Sub13() {
		super(1, false);
	}

	void method12350() {
		if (null == ((Class282_Sub20_Sub13) this).anIntArrayArray9822)
			method15269(1, -2141021596);
		method15268(1292803011);
	}

	void method15270() {
		int i = ((Class282_Sub20_Sub13) this).anIntArrayArray9822.length;
		if (i > 0) {
			for (int i_21_ = 0; i_21_ < 257; i_21_++) {
				int i_22_ = 0;
				int i_23_ = i_21_ << 4;
				for (int i_24_ = 0; i_24_ < i && i_23_ >= (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_24_][0]); i_24_++)
					i_22_++;
				int i_25_;
				int i_26_;
				int i_27_;
				if (i_22_ < i) {
					int[] is = (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_22_]);
					if (i_22_ > 0) {
						int[] is_28_ = (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_22_ - 1]);
						int i_29_ = (i_23_ - is_28_[0] << 12) / (is[0] - is_28_[0]);
						int i_30_ = 4096 - i_29_;
						i_25_ = i_29_ * is[1] + i_30_ * is_28_[1] >> 12;
						i_26_ = is_28_[2] * i_30_ + i_29_ * is[2] >> 12;
						i_27_ = i_29_ * is[3] + is_28_[3] * i_30_ >> 12;
					} else {
						i_25_ = is[1];
						i_26_ = is[2];
						i_27_ = is[3];
					}
				} else {
					int[] is = (((Class282_Sub20_Sub13) this).anIntArrayArray9822[i - 1]);
					i_25_ = is[1];
					i_26_ = is[2];
					i_27_ = is[3];
				}
				i_25_ >>= 4;
				i_26_ >>= 4;
				i_27_ >>= 4;
				if (i_25_ < 0)
					i_25_ = 0;
				else if (i_25_ > 255)
					i_25_ = 255;
				if (i_26_ < 0)
					i_26_ = 0;
				else if (i_26_ > 255)
					i_26_ = 255;
				if (i_27_ < 0)
					i_27_ = 0;
				else if (i_27_ > 255)
					i_27_ = 255;
				((Class282_Sub20_Sub13) this).anIntArray9823[i_21_] = i_25_ << 16 | i_26_ << 8 | i_27_;
			}
		}
	}

	void method12335(int i, RsByteBuffer class282_sub35) {
		if (0 == i) {
			int i_31_ = class282_sub35.readUnsignedByte();
			if (0 == i_31_) {
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[class282_sub35.readUnsignedByte()][4];
				for (int i_32_ = 0; i_32_ < (((Class282_Sub20_Sub13) this).anIntArrayArray9822).length; i_32_++) {
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_32_][0] = class282_sub35.readUnsignedShort();
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_32_][1] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_32_][2] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_32_][3] = class282_sub35.readUnsignedByte() << 4;
				}
			} else
				method15269(i_31_, -1445469925);
		}
	}

	void method12334(int i, RsByteBuffer class282_sub35) {
		if (0 == i) {
			int i_33_ = class282_sub35.readUnsignedByte();
			if (0 == i_33_) {
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[class282_sub35.readUnsignedByte()][4];
				for (int i_34_ = 0; i_34_ < (((Class282_Sub20_Sub13) this).anIntArrayArray9822).length; i_34_++) {
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_34_][0] = class282_sub35.readUnsignedShort();
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_34_][1] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_34_][2] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_34_][3] = class282_sub35.readUnsignedByte() << 4;
				}
			} else
				method15269(i_33_, -1643382890);
		}
	}

	int[][] method12339(int i) {
		int[][] is = aClass308_7670.method5463(i, 1638183275);
		if (aClass308_7670.aBool3619) {
			int[] is_35_ = method12317(0, i, 2089907684);
			int[] is_36_ = is[0];
			int[] is_37_ = is[1];
			int[] is_38_ = is[2];
			for (int i_39_ = 0; i_39_ < Class316.anInt3670 * -402153223; i_39_++) {
				int i_40_ = is_35_[i_39_] >> 4;
				if (i_40_ < 0)
					i_40_ = 0;
				if (i_40_ > 256)
					i_40_ = 256;
				i_40_ = ((Class282_Sub20_Sub13) this).anIntArray9823[i_40_];
				is_36_[i_39_] = (i_40_ & 0xff0000) >> 12;
				is_37_[i_39_] = (i_40_ & 0xff00) >> 4;
				is_38_[i_39_] = (i_40_ & 0xff) << 4;
			}
		}
		return is;
	}

	void method12328() {
		if (null == ((Class282_Sub20_Sub13) this).anIntArrayArray9822)
			method15269(1, -1674137569);
		method15268(1011629163);
	}

	void method15271(int i) {
		if (0 != i) {
			switch (i) {
			case 2:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[8][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 2650;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 2602;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 2361;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 2313;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 1799;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1558;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 2618;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 2296;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1220;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 2072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 963;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 722;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 2730;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 2152;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 3891;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 2232;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1060;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 915;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1140;
				break;
			case 4:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[6][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 1843;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1493;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 2457;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 2939;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2781;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1124;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 3565;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 546;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 3084;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 4031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				break;
			case 6:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[4][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				break;
			case 3:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[7][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 663;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 1363;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 2727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3411;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 4096;
				break;
			default:
				throw new RuntimeException();
			case 1:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[2][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				break;
			case 5:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[16][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 80;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 192;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 155;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 449;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 562;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 389;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 578;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 690;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 803;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 671;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 995;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 1140;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 897;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 1285;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 1397;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 1509;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 1175;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 1429;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 1368;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1461;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 1333;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 1507;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1702;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][0] = 1736;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][1] = 1108;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][2] = 1590;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][3] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][0] = 2088;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][1] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][2] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][3] = 2666;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][0] = 2355;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][1] = 2409;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][2] = 2586;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][3] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][0] = 2691;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][0] = 3031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][1] = 3806;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][2] = 3710;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][3] = 3196;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][0] = 3522;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][1] = 3437;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][2] = 3421;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][3] = 3019;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][0] = 3727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][1] = 2377;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][2] = 2505;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][3] = 2746;
			}
		}
	}

	void method15272(int i) {
		if (0 != i) {
			switch (i) {
			case 2:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[8][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 2650;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 2602;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 2361;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 2313;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 1799;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1558;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 2618;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 2296;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1220;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 2072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 963;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 722;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 2730;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 2152;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 3891;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 2232;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1060;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 915;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1140;
				break;
			case 4:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[6][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 1843;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1493;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 2457;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 2939;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2781;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1124;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 3565;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 546;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 3084;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 4031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				break;
			case 6:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[4][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				break;
			case 3:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[7][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 663;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 1363;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 2727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3411;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 4096;
				break;
			default:
				throw new RuntimeException();
			case 1:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[2][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				break;
			case 5:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[16][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 80;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 192;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 155;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 449;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 562;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 389;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 578;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 690;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 803;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 671;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 995;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 1140;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 897;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 1285;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 1397;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 1509;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 1175;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 1429;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 1368;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1461;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 1333;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 1507;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1702;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][0] = 1736;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][1] = 1108;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][2] = 1590;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][3] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][0] = 2088;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][1] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][2] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][3] = 2666;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][0] = 2355;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][1] = 2409;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][2] = 2586;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][3] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][0] = 2691;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][0] = 3031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][1] = 3806;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][2] = 3710;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][3] = 3196;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][0] = 3522;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][1] = 3437;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][2] = 3421;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][3] = 3019;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][0] = 3727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][1] = 2377;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][2] = 2505;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][3] = 2746;
			}
		}
	}

	void method12322(int i, RsByteBuffer class282_sub35, int i_41_) {
		if (0 == i) {
			int i_42_ = class282_sub35.readUnsignedByte();
			if (0 == i_42_) {
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[class282_sub35.readUnsignedByte()][4];
				for (int i_43_ = 0; i_43_ < (((Class282_Sub20_Sub13) this).anIntArrayArray9822).length; i_43_++) {
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_43_][0] = class282_sub35.readUnsignedShort();
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_43_][1] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_43_][2] = class282_sub35.readUnsignedByte() << 4;
					((Class282_Sub20_Sub13) this).anIntArrayArray9822[i_43_][3] = class282_sub35.readUnsignedByte() << 4;
				}
			} else
				method15269(i_42_, -1511805010);
		}
	}

	void method15273(int i) {
		if (0 != i) {
			switch (i) {
			case 2:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[8][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 2650;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 2602;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 2361;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 2313;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 1799;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1558;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 2618;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 2296;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1220;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 2072;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 963;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 722;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 2730;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 2152;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 3891;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 2232;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1060;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 915;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1686;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1140;
				break;
			case 4:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[6][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 1843;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 1493;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 2457;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 2939;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2781;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 1124;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 3565;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 3481;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 546;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 3084;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 4031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				break;
			case 6:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[4][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 2867;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				break;
			case 3:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[7][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 663;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 1363;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 2048;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 2727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 3411;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 4096;
				break;
			default:
				throw new RuntimeException();
			case 1:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[2][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 4096;
				break;
			case 5:
				((Class282_Sub20_Sub13) this).anIntArrayArray9822 = new int[16][4];
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][0] = 0;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][1] = 80;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][2] = 192;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[0][3] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][0] = 155;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][1] = 321;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][2] = 449;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[1][3] = 562;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][0] = 389;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][1] = 578;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][2] = 690;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[2][3] = 803;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][0] = 671;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][1] = 947;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][2] = 995;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[3][3] = 1140;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][0] = 897;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][1] = 1285;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][2] = 1397;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[4][3] = 1509;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][0] = 1175;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][1] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][2] = 1429;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[5][3] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][0] = 1368;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][1] = 1734;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][2] = 1461;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[6][3] = 1333;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][0] = 1507;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][1] = 1413;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][2] = 1525;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[7][3] = 1702;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][0] = 1736;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][1] = 1108;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][2] = 1590;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[8][3] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][0] = 2088;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][1] = 1766;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][2] = 2056;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[9][3] = 2666;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][0] = 2355;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][1] = 2409;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][2] = 2586;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[10][3] = 3276;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][0] = 2691;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[11][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][0] = 3031;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][1] = 3806;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][2] = 3710;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[12][3] = 3196;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][0] = 3522;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][1] = 3437;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][2] = 3421;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[13][3] = 3019;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][0] = 3727;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][1] = 3116;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][2] = 3148;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[14][3] = 3228;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][0] = 4096;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][1] = 2377;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][2] = 2505;
				((Class282_Sub20_Sub13) this).anIntArrayArray9822[15][3] = 2746;
			}
		}
	}
}
