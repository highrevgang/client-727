/* Class96 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class96 {
	public int anInt992;

	boolean method1591() {
		return true;
	}

	public abstract void method1592(int i);

	void method1593(byte i) {
		/* empty */
	}

	static Class96 method1594(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class411 class411 = Class346.method6156(i, 2143698808);
		Class96 class96 = null;
		switch (class411.anInt4956 * -385427245) {
		case 13:
			class96 = new Class96_Sub3(class282_sub35);
			break;
		case 25:
			class96 = new Class96_Sub10_Sub1(class282_sub35);
			break;
		case 15:
			class96 = new Class96_Sub19(class282_sub35, 0, 0);
			break;
		case 5:
			class96 = new Class96_Sub21(class282_sub35);
			break;
		case 11:
			class96 = new Class96_Sub11(class282_sub35);
			break;
		case 14:
			class96 = new Class96_Sub10_Sub2(class282_sub35);
			break;
		case 16:
			class96 = new Class96_Sub1(class282_sub35);
			break;
		case 29:
			class96 = new Class96_Sub2(class282_sub35);
			break;
		default:
			break;
		case 2:
			class96 = new Class96_Sub14(class282_sub35);
			break;
		case 8:
			class96 = new Class96_Sub5(class282_sub35);
			break;
		case 19:
			class96 = new Class96_Sub19(class282_sub35, 1, 0);
			break;
		case 28:
			class96 = new Class96_Sub19(class282_sub35, 1, 1);
			break;
		case 20:
			class96 = new Class96_Sub6(class282_sub35);
			break;
		case 24:
			class96 = new Class96_Sub19(class282_sub35, 0, 1);
			break;
		case 9:
			class96 = new Class96_Sub22(class282_sub35);
			break;
		case 4:
			class96 = new Class96_Sub4(class282_sub35);
			break;
		case 26:
			class96 = new Class96_Sub18(class282_sub35);
			break;
		case 22:
			class96 = new Class96_Sub12(class282_sub35);
			break;
		case 23:
			class96 = new Class96_Sub13(class282_sub35);
			break;
		case 0:
			class96 = new Class96_Sub15(class282_sub35);
			break;
		case 27:
			class96 = new Class96_Sub16(class282_sub35, true);
			break;
		case 10:
			class96 = new Class96_Sub16(class282_sub35, false);
			break;
		case 17:
			class96 = new Class96_Sub17(class282_sub35);
			break;
		case 6:
			class96 = new Class96_Sub8(class282_sub35);
			break;
		case 12:
			class96 = new Class96_Sub7(class282_sub35);
			break;
		case 30:
			class96 = new Class96_Sub23(class282_sub35);
			break;
		case 18:
			class96 = new Class96_Sub9(class282_sub35);
			break;
		case 1:
			class96 = new Class96_Sub20(class282_sub35);
		}
		return class96;
	}

	void method1595() {
		/* empty */
	}

	boolean method1596() {
		return true;
	}

	Class96(RsByteBuffer class282_sub35) {
		anInt992 = class282_sub35.readUnsignedShort() * -1730002309;
	}

	void method1597() {
		/* empty */
	}

	void method1598() {
		/* empty */
	}

	boolean method1599(int i) {
		return true;
	}

	static Class96 method1600(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class411 class411 = Class346.method6156(i, 1952750012);
		Class96 class96 = null;
		switch (class411.anInt4956 * -385427245) {
		case 13:
			class96 = new Class96_Sub3(class282_sub35);
			break;
		case 25:
			class96 = new Class96_Sub10_Sub1(class282_sub35);
			break;
		case 15:
			class96 = new Class96_Sub19(class282_sub35, 0, 0);
			break;
		case 5:
			class96 = new Class96_Sub21(class282_sub35);
			break;
		case 11:
			class96 = new Class96_Sub11(class282_sub35);
			break;
		case 14:
			class96 = new Class96_Sub10_Sub2(class282_sub35);
			break;
		case 16:
			class96 = new Class96_Sub1(class282_sub35);
			break;
		case 29:
			class96 = new Class96_Sub2(class282_sub35);
			break;
		default:
			break;
		case 2:
			class96 = new Class96_Sub14(class282_sub35);
			break;
		case 8:
			class96 = new Class96_Sub5(class282_sub35);
			break;
		case 19:
			class96 = new Class96_Sub19(class282_sub35, 1, 0);
			break;
		case 28:
			class96 = new Class96_Sub19(class282_sub35, 1, 1);
			break;
		case 20:
			class96 = new Class96_Sub6(class282_sub35);
			break;
		case 24:
			class96 = new Class96_Sub19(class282_sub35, 0, 1);
			break;
		case 9:
			class96 = new Class96_Sub22(class282_sub35);
			break;
		case 4:
			class96 = new Class96_Sub4(class282_sub35);
			break;
		case 26:
			class96 = new Class96_Sub18(class282_sub35);
			break;
		case 22:
			class96 = new Class96_Sub12(class282_sub35);
			break;
		case 23:
			class96 = new Class96_Sub13(class282_sub35);
			break;
		case 0:
			class96 = new Class96_Sub15(class282_sub35);
			break;
		case 27:
			class96 = new Class96_Sub16(class282_sub35, true);
			break;
		case 10:
			class96 = new Class96_Sub16(class282_sub35, false);
			break;
		case 17:
			class96 = new Class96_Sub17(class282_sub35);
			break;
		case 6:
			class96 = new Class96_Sub8(class282_sub35);
			break;
		case 12:
			class96 = new Class96_Sub7(class282_sub35);
			break;
		case 30:
			class96 = new Class96_Sub23(class282_sub35);
			break;
		case 18:
			class96 = new Class96_Sub9(class282_sub35);
			break;
		case 1:
			class96 = new Class96_Sub20(class282_sub35);
		}
		return class96;
	}

	public abstract void method1601();

	static Class96 method1602(RsByteBuffer class282_sub35) {
		int i = class282_sub35.readUnsignedByte();
		Class411 class411 = Class346.method6156(i, 2027155433);
		Class96 class96 = null;
		switch (class411.anInt4956 * -385427245) {
		case 13:
			class96 = new Class96_Sub3(class282_sub35);
			break;
		case 25:
			class96 = new Class96_Sub10_Sub1(class282_sub35);
			break;
		case 15:
			class96 = new Class96_Sub19(class282_sub35, 0, 0);
			break;
		case 5:
			class96 = new Class96_Sub21(class282_sub35);
			break;
		case 11:
			class96 = new Class96_Sub11(class282_sub35);
			break;
		case 14:
			class96 = new Class96_Sub10_Sub2(class282_sub35);
			break;
		case 16:
			class96 = new Class96_Sub1(class282_sub35);
			break;
		case 29:
			class96 = new Class96_Sub2(class282_sub35);
			break;
		default:
			break;
		case 2:
			class96 = new Class96_Sub14(class282_sub35);
			break;
		case 8:
			class96 = new Class96_Sub5(class282_sub35);
			break;
		case 19:
			class96 = new Class96_Sub19(class282_sub35, 1, 0);
			break;
		case 28:
			class96 = new Class96_Sub19(class282_sub35, 1, 1);
			break;
		case 20:
			class96 = new Class96_Sub6(class282_sub35);
			break;
		case 24:
			class96 = new Class96_Sub19(class282_sub35, 0, 1);
			break;
		case 9:
			class96 = new Class96_Sub22(class282_sub35);
			break;
		case 4:
			class96 = new Class96_Sub4(class282_sub35);
			break;
		case 26:
			class96 = new Class96_Sub18(class282_sub35);
			break;
		case 22:
			class96 = new Class96_Sub12(class282_sub35);
			break;
		case 23:
			class96 = new Class96_Sub13(class282_sub35);
			break;
		case 0:
			class96 = new Class96_Sub15(class282_sub35);
			break;
		case 27:
			class96 = new Class96_Sub16(class282_sub35, true);
			break;
		case 10:
			class96 = new Class96_Sub16(class282_sub35, false);
			break;
		case 17:
			class96 = new Class96_Sub17(class282_sub35);
			break;
		case 6:
			class96 = new Class96_Sub8(class282_sub35);
			break;
		case 12:
			class96 = new Class96_Sub7(class282_sub35);
			break;
		case 30:
			class96 = new Class96_Sub23(class282_sub35);
			break;
		case 18:
			class96 = new Class96_Sub9(class282_sub35);
			break;
		case 1:
			class96 = new Class96_Sub20(class282_sub35);
		}
		return class96;
	}

	static final void method1603(Class527 class527, int i) {
		Class184 class184 = Class468_Sub20.method12807(-846822027);
		Class282_Sub23 class282_sub23 = Class271.method4828(OutgoingPacket.aClass379_4591, class184.aClass432_2283, -312926267);
		class282_sub23.buffer.writeByte(0);
		int i_0_ = class282_sub23.buffer.index * -1990677291;
		class282_sub23.buffer.writeByte(3);
		class282_sub23.buffer.writeShort(-624100047 * ((Class527) class527).aClass346_7009.anInt4048, 1417031095);
		((Class527) class527).aClass346_7009.aClass282_Sub50_Sub9_4047.method14896(class282_sub23.buffer, ((Class527) class527).aClass346_7009.anIntArray4046, -1204920325);
		class282_sub23.buffer.method13061((-1990677291 * class282_sub23.buffer.index - i_0_), -2062129936);
		class184.method3049(class282_sub23, 2118885707);
	}

	static final void method1604(Class527 class527, byte i) {
		System.out.println(((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 -= 1476624725) * 1806726141)]);
	}

	public static Class98 method1605(int i, byte i_1_) {
		return Class468_Sub8.aClass98Array7889[i >> 16];
	}

	static final void method1606(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class282_Sub17_Sub4.method15409(class118, class98, class527, -674550702);
	}

	public static void method1607(byte i) {
		Class61.aClass160_647 = null;
		Class60.aClass160_612 = null;
		Class467.aClass160_5576 = null;
		Class437.aClass160_5339 = null;
		Class290.aClass160_3452 = null;
		Class354.aClass160_4110 = null;
		Class125.aClass160_1571 = null;
		Class149_Sub2.aClass160_9315 = null;
		Class186.aClass8_2348 = null;
		Class176.aClass414_2200 = null;
	}

	static final void method1608(Class527 class527, byte i) {
		int i_2_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		if (null != Class467.aClass173Array5575 && i_2_ < Class459.anInt5534 * -1772444859 && (Class467.aClass173Array5575[i_2_].aString2127.equalsIgnoreCase(Class84.myPlayer.aString10546)))
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 1;
		else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = 0;
	}

	static final String method1609(String string, String string_3_, String string_4_, int i) {
		for (int i_5_ = string.indexOf(string_3_); i_5_ != -1; i_5_ = string.indexOf(string_3_, i_5_ + string_4_.length()))
			string = new StringBuilder().append(string.substring(0, i_5_)).append(string_4_).append(string.substring(i_5_ + string_3_.length())).toString();
		return string;
	}
}
