/* Interface6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public interface Interface6 extends Interface30 {
	public float method45(float f);

	public boolean method46();

	public float method47(float f);

	public void method48(int i, int i_0_, int i_1_, int i_2_, int[] is, int i_3_, int i_4_);

	public boolean method49();

	public void method50(boolean bool, boolean bool_5_);

	public float method51(float f);

	public void method52(int i, int i_6_, int i_7_, int i_8_, byte[] is, Class150 class150, int i_9_, int i_10_);

	public void method53(int i, int i_11_, int i_12_, int i_13_, int[] is, int i_14_);

	public boolean method54();

	public int method55();

	public float method56(float f);

	public int method57();

	public float method58(float f);

	public int method1();

	public int method59();

	public int method36();

	public float method60(float f);

	public void method61(int i, int i_15_, int i_16_, int i_17_, int[] is, int i_18_, int i_19_);

	public float method62(float f);

	public boolean method63();

	public void method64(boolean bool, boolean bool_20_);

	public float method65(float f);

	public void method66(int i, int i_21_, int i_22_, int i_23_, int[] is, int i_24_, int i_25_);

	public void method67(int i, int i_26_, int i_27_, int i_28_, int[] is, int i_29_, int i_30_);

	public void method68(int i, int i_31_, int i_32_, int i_33_, byte[] is, Class150 class150, int i_34_, int i_35_);

	public void method69(int i, int i_36_, int i_37_, int i_38_, int[] is, int i_39_);

	public int method70();

	public int method71();

	public int method72();
}
