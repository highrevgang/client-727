/* Class521_Sub1_Sub5_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class521_Sub1_Sub5_Sub2 extends Class521_Sub1_Sub5 implements Interface12 {
	boolean aBool10528 = true;
	boolean aBool10529;
	Class200 aClass200_10530;
	static int[] anIntArray10531 = { 1, 2, 4, 8 };
	public Class123 aClass123_10532;
	static int[] anIntArray10533 = { 16, 32, 64, 128 };

	Class285 method12990(Class505 class505, int i) {
		Class528 class528 = aClass123_10532.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub5_Sub2) this).aBool10529, (byte) -46);
		int i_0_ = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_1_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10532.method2119(class505, class528, class294, i_0_, i_0_, i_1_, i_1_, true, 319778577);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10532).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10532).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub5_Sub2) this).aBool10528 = (class528.i() || ((Class123) aClass123_10532).aClass539_1538 != null);
		if (((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 == null)
			((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) -4);
		else
			Class388.method6694((((Class521_Sub1_Sub5_Sub2) this).aClass200_10530), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -1850766643);
		return class285;
	}

	boolean method12986(int i) {
		return false;
	}

	boolean method12987(int i) {
		return ((Class521_Sub1_Sub5_Sub2) this).aBool10528;
	}

	public Class200 method12992(Class505 class505, byte i) {
		return ((Class521_Sub1_Sub5_Sub2) this).aClass200_10530;
	}

	void method13023(Class505 class505) {
		Class528 class528 = aClass123_10532.method2132(class505, 262144, true, true, -1384937123);
		if (class528 != null) {
			Class294 class294 = method11168();
			Class305 class305 = method11166();
			int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
			int i_2_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
			aClass123_10532.method2119(class505, class528, class294, i, i, i_2_, i_2_, false, -66384219);
		}
	}

	boolean method13001() {
		return ((Class521_Sub1_Sub5_Sub2) this).aBool10528;
	}

	public void method16108(Class476 class476, int i) {
		aClass123_10532.method2116(class476, 424577870);
	}

	void method12991(Class505 class505, int i) {
		Class528 class528 = aClass123_10532.method2132(class505, 262144, true, true, -1384937123);
		if (class528 != null) {
			Class294 class294 = method11168();
			Class305 class305 = method11166();
			int i_3_ = (int) class305.aClass385_3595.aFloat4671 >> 9;
			int i_4_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
			aClass123_10532.method2119(class505, class528, class294, i_3_, i_3_, i_4_, i_4_, false, 657441893);
		}
	}

	public int method77() {
		return ((Class123) aClass123_10532).anInt1555 * 845010167;
	}

	final boolean method12985(int i) {
		return false;
	}

	public boolean method94() {
		return aClass123_10532.method2117(249607349);
	}

	final void method12984(int i) {
		throw new IllegalStateException();
	}

	public int method84(int i) {
		return ((Class123) aClass123_10532).anInt1540 * 1535779425;
	}

	public int method89(int i) {
		return ((Class123) aClass123_10532).anInt1555 * 845010167;
	}

	public int method92(int i) {
		return ((Class123) aClass123_10532).anInt1542 * -515017769;
	}

	public void method85(byte i) {
		/* empty */
	}

	public boolean method86(int i) {
		return aClass123_10532.method2117(-1306330739);
	}

	public void method16109(Class476 class476) {
		aClass123_10532.method2116(class476, -1673723332);
	}

	public void method88(Class505 class505, int i) {
		aClass123_10532.method2118(class505, 1838435469);
	}

	public void method90() {
		/* empty */
	}

	public int method12997(int i) {
		return aClass123_10532.method2121((byte) 1);
	}

	public void method87(Class505 class505, int i) {
		aClass123_10532.method2136(class505, (byte) -126);
	}

	final void method13015() {
		throw new IllegalStateException();
	}

	public void method96() {
		/* empty */
	}

	public int method91() {
		return ((Class123) aClass123_10532).anInt1542 * -515017769;
	}

	public void method38() {
		/* empty */
	}

	boolean method13002() {
		return ((Class521_Sub1_Sub5_Sub2) this).aBool10528;
	}

	Class285 method13010(Class505 class505) {
		Class528 class528 = aClass123_10532.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub5_Sub2) this).aBool10529, (byte) -125);
		int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_5_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10532.method2119(class505, class528, class294, i, i, i_5_, i_5_, true, -337672509);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10532).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10532).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub5_Sub2) this).aBool10528 = (class528.i() || ((Class123) aClass123_10532).aClass539_1538 != null);
		if (((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 == null)
			((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) 5);
		else
			Class388.method6694((((Class521_Sub1_Sub5_Sub2) this).aClass200_10530), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -550552279);
		return class285;
	}

	public int method73() {
		return ((Class123) aClass123_10532).anInt1555 * 845010167;
	}

	public Class521_Sub1_Sub5_Sub2(Class206 class206, Class505 class505, Class474 class474, Class478 class478, int i, int i_6_, int i_7_, int i_8_, int i_9_, boolean bool, int i_10_, int i_11_, int i_12_) {
		super(class206, i_7_, i_8_, i_9_, i, i_6_, Class263.method4777(i_10_, i_11_, (byte) -17));
		aClass123_10532 = new Class123(class505, class474, class478, i_10_, i_11_, aByte7967, i_6_, this, bool, i_12_);
		((Class521_Sub1_Sub5_Sub2) this).aBool10529 = 0 != class478.anInt5652 * -348507379 && !bool;
		method13008(1, (byte) -77);
	}

	public void method93(Class505 class505) {
		aClass123_10532.method2136(class505, (byte) -44);
	}

	public void method83(Class505 class505) {
		aClass123_10532.method2136(class505, (byte) -59);
	}

	public void method97(Class505 class505) {
		aClass123_10532.method2118(class505, 2032514144);
	}

	public void method98(Class505 class505) {
		aClass123_10532.method2118(class505, 2115861985);
	}

	public int method13005() {
		return aClass123_10532.method2115(156104600);
	}

	boolean method13000() {
		return ((Class521_Sub1_Sub5_Sub2) this).aBool10528;
	}

	boolean method12999() {
		return false;
	}

	public int method76() {
		return ((Class123) aClass123_10532).anInt1540 * 1535779425;
	}

	public int method13003() {
		return aClass123_10532.method2115(-1195004848);
	}

	public int method13017() {
		return aClass123_10532.method2115(1873128711);
	}

	public int method39() {
		return ((Class123) aClass123_10532).anInt1540 * 1535779425;
	}

	public int method13006() {
		return aClass123_10532.method2115(-924448901);
	}

	public boolean method95() {
		return aClass123_10532.method2117(1065429909);
	}

	public int method13028() {
		return aClass123_10532.method2121((byte) 1);
	}

	final void method13016(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_13_, int i_14_, boolean bool) {
		throw new IllegalStateException();
	}

	public int method12995(int i) {
		return aClass123_10532.method2115(1807526638);
	}

	Class285 method12989(Class505 class505) {
		Class528 class528 = aClass123_10532.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub5_Sub2) this).aBool10529, (byte) -77);
		int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_15_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10532.method2119(class505, class528, class294, i, i, i_15_, i_15_, true, -204709260);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10532).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10532).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub5_Sub2) this).aBool10528 = (class528.i() || ((Class123) aClass123_10532).aClass539_1538 != null);
		if (((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 == null)
			((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) 8);
		else
			Class388.method6694((((Class521_Sub1_Sub5_Sub2) this).aClass200_10530), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -237678782);
		return class285;
	}

	void method13012(Class505 class505) {
		Class528 class528 = aClass123_10532.method2132(class505, 262144, true, true, -1384937123);
		if (class528 != null) {
			Class294 class294 = method11168();
			Class305 class305 = method11166();
			int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
			int i_16_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
			aClass123_10532.method2119(class505, class528, class294, i, i, i_16_, i_16_, false, 975685007);
		}
	}

	boolean method12983(Class505 class505, int i, int i_17_, int i_18_) {
		Class528 class528 = aClass123_10532.method2132(class505, 131072, false, false, -1384937123);
		if (null == class528)
			return false;
		return class528.method11270(i, i_17_, method11168(), false, 0);
	}

	final boolean method13026() {
		return false;
	}

	final boolean method13011() {
		return false;
	}

	Class285 method13009(Class505 class505) {
		Class528 class528 = aClass123_10532.method2132(class505, 2048, false, true, -1384937123);
		if (null == class528)
			return null;
		Class294 class294 = method11168();
		Class305 class305 = method11166();
		Class285 class285 = Class470.method7824(((Class521_Sub1_Sub5_Sub2) this).aBool10529, (byte) -18);
		int i = (int) class305.aClass385_3595.aFloat4671 >> 9;
		int i_19_ = (int) class305.aClass385_3595.aFloat4673 >> 9;
		aClass123_10532.method2119(class505, class528, class294, i, i, i_19_, i_19_, true, 1290726085);
		class528.method11282(class294, aClass275_Sub5Array7965[0], 0);
		if (((Class123) aClass123_10532).aClass539_1538 != null) {
			Class151 class151 = ((Class123) aClass123_10532).aClass539_1538.method11517();
			class505.method8456(class151);
		}
		((Class521_Sub1_Sub5_Sub2) this).aBool10528 = (class528.i() || ((Class123) aClass123_10532).aClass539_1538 != null);
		if (((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 == null)
			((Class521_Sub1_Sub5_Sub2) this).aClass200_10530 = Class275_Sub2.method12505((int) (class305.aClass385_3595.aFloat4671), (int) (class305.aClass385_3595.aFloat4672), (int) (class305.aClass385_3595.aFloat4673), class528, (byte) -25);
		else
			Class388.method6694((((Class521_Sub1_Sub5_Sub2) this).aClass200_10530), (int) class305.aClass385_3595.aFloat4671, (int) class305.aClass385_3595.aFloat4672, (int) class305.aClass385_3595.aFloat4673, class528, -678461281);
		return class285;
	}

	public Class200 method13019(Class505 class505) {
		return ((Class521_Sub1_Sub5_Sub2) this).aClass200_10530;
	}

	public Class200 method13018(Class505 class505) {
		return ((Class521_Sub1_Sub5_Sub2) this).aClass200_10530;
	}

	public Class200 method12993(Class505 class505) {
		return ((Class521_Sub1_Sub5_Sub2) this).aClass200_10530;
	}

	boolean method13020(Class505 class505, int i, int i_20_) {
		Class528 class528 = aClass123_10532.method2132(class505, 131072, false, false, -1384937123);
		if (null == class528)
			return false;
		return class528.method11270(i, i_20_, method11168(), false, 0);
	}

	final void method13021() {
		throw new IllegalStateException();
	}

	final void method13013(Class505 class505, Class521_Sub1 class521_sub1, int i, int i_21_, int i_22_, boolean bool, int i_23_) {
		throw new IllegalStateException();
	}

	static int method16110(int i, int i_24_) {
		if (Class458.aClass458_5493.anInt5481 * 1109376893 == i || i == Class458.aClass458_5473.anInt5481 * 1109376893)
			return anIntArray10533[i_24_ & 0x3];
		return anIntArray10531[i_24_ & 0x3];
	}

	static int method16111(int i, int i_25_) {
		if (Class458.aClass458_5493.anInt5481 * 1109376893 == i || i == Class458.aClass458_5473.anInt5481 * 1109376893)
			return anIntArray10533[i_25_ & 0x3];
		return anIntArray10531[i_25_ & 0x3];
	}

	static int method16112(int i, int i_26_) {
		if (Class458.aClass458_5493.anInt5481 * 1109376893 == i || i == Class458.aClass458_5473.anInt5481 * 1109376893)
			return anIntArray10533[i_26_ & 0x3];
		return anIntArray10531[i_26_ & 0x3];
	}

	public int method13007() {
		return aClass123_10532.method2121((byte) 1);
	}

	static final void method16113(int i) {
		int i_27_ = 1551528448 * Class296.anInt3534 + 256;
		int i_28_ = 256 + Class282_Sub44.anInt8064 * 1593416192;
		int i_29_ = (Class504.method8389(i_27_, i_28_, Class4.anInt35 * 675588453, (byte) 102) - Class525.anInt6985 * 368694431);
		if (Class115.anInt1249 * 613192577 >= 100) {
			Class31.anInt361 = Class296.anInt3534 * -899014144 + 898537728;
			Class246.anInt3029 = 2107292160 * Class282_Sub44.anInt8064 + 26398976;
			Class109_Sub1.anInt9384 = (Class504.method8389(-360258135 * Class31.anInt361, Class246.anInt3029 * 413271601, 675588453 * Class4.anInt35, (byte) 84) - Class525.anInt6985 * 368694431) * -126779709;
		} else {
			if (Class31.anInt361 * -360258135 < i_27_) {
				Class31.anInt361 += ((-355473345 * Class454.anInt5451 + (Class115.anInt1249 * 613192577 * (i_27_ - -360258135 * Class31.anInt361) / 1000)) * -2043310439);
				if (Class31.anInt361 * -360258135 > i_27_)
					Class31.anInt361 = -2043310439 * i_27_;
			}
			if (Class31.anInt361 * -360258135 > i_27_) {
				Class31.anInt361 -= -2043310439 * (((-360258135 * Class31.anInt361 - i_27_) * (613192577 * Class115.anInt1249) / 1000) + -355473345 * Class454.anInt5451);
				if (Class31.anInt361 * -360258135 < i_27_)
					Class31.anInt361 = i_27_ * -2043310439;
			}
			if (Class109_Sub1.anInt9384 * 1929945579 < i_29_) {
				Class109_Sub1.anInt9384 += (-126779709 * ((613192577 * Class115.anInt1249 * (i_29_ - 1929945579 * Class109_Sub1.anInt9384) / 1000) + -355473345 * Class454.anInt5451));
				if (1929945579 * Class109_Sub1.anInt9384 > i_29_)
					Class109_Sub1.anInt9384 = i_29_ * -126779709;
			}
			if (1929945579 * Class109_Sub1.anInt9384 > i_29_) {
				Class109_Sub1.anInt9384 -= (-126779709 * (-355473345 * Class454.anInt5451 + ((Class109_Sub1.anInt9384 * 1929945579 - i_29_) * (Class115.anInt1249 * 613192577) / 1000)));
				if (1929945579 * Class109_Sub1.anInt9384 < i_29_)
					Class109_Sub1.anInt9384 = i_29_ * -126779709;
			}
			if (413271601 * Class246.anInt3029 < i_28_) {
				Class246.anInt3029 += 1442943697 * (((i_28_ - 413271601 * Class246.anInt3029) * (Class115.anInt1249 * 613192577) / 1000) + -355473345 * Class454.anInt5451);
				if (413271601 * Class246.anInt3029 > i_28_)
					Class246.anInt3029 = i_28_ * 1442943697;
			}
			if (413271601 * Class246.anInt3029 > i_28_) {
				Class246.anInt3029 -= (1442943697 * ((613192577 * Class115.anInt1249 * (413271601 * Class246.anInt3029 - i_28_) / 1000) + -355473345 * Class454.anInt5451));
				if (413271601 * Class246.anInt3029 < i_28_)
					Class246.anInt3029 = i_28_ * 1442943697;
			}
		}
		i_27_ = 75271680 * Class96_Sub13.anInt9368 + 256;
		i_28_ = Class369.anInt4280 * -51121664 + 256;
		i_29_ = Class504.method8389(i_27_, i_28_, Class4.anInt35 * 675588453, (byte) 54) - 654473753 * Class121.anInt1527;
		int i_30_ = i_27_ - -360258135 * Class31.anInt361;
		int i_31_ = i_29_ - 1929945579 * Class109_Sub1.anInt9384;
		int i_32_ = i_28_ - 413271601 * Class246.anInt3029;
		int i_33_ = (int) Math.sqrt((double) (i_32_ * i_32_ + i_30_ * i_30_));
		int i_34_ = ((int) (Math.atan2((double) i_31_, (double) i_33_) * 2607.5945876176133) & 0x3fff);
		int i_35_ = ((int) (Math.atan2((double) i_30_, (double) i_32_) * -2607.5945876176133) & 0x3fff);
		if (i_34_ < 1024)
			i_34_ = 1024;
		if (i_34_ > 3072)
			i_34_ = 3072;
		if (Class293.anInt3512 * 726126721 < i_34_) {
			Class293.anInt3512 += ((692693311 * Class473.anInt5606 + ((i_34_ - Class293.anInt3512 * 726126721 >> 3) * (Class501.anInt5828 * 1123046983) / 1000)) << 3) * -647467135;
			if (726126721 * Class293.anInt3512 > i_34_)
				Class293.anInt3512 = i_34_ * -647467135;
		}
		if (Class293.anInt3512 * 726126721 > i_34_) {
			Class293.anInt3512 -= (-647467135 * ((692693311 * Class473.anInt5606 + ((726126721 * Class293.anInt3512 - i_34_ >> 3) * (Class501.anInt5828 * 1123046983) / 1000)) << 3));
			if (726126721 * Class293.anInt3512 < i_34_)
				Class293.anInt3512 = i_34_ * -647467135;
		}
		int i_36_ = i_35_ - 1236051449 * Class518.anInt5930;
		if (i_36_ > 8192)
			i_36_ -= 16384;
		if (i_36_ < -8192)
			i_36_ += 16384;
		i_36_ >>= 3;
		if (i_36_ > 0) {
			Class518.anInt5930 += (1898253385 * ((692693311 * Class473.anInt5606 + 1123046983 * Class501.anInt5828 * i_36_ / 1000) << 3));
			Class518.anInt5930 = (1236051449 * Class518.anInt5930 & 0x3fff) * 1898253385;
		}
		if (i_36_ < 0) {
			Class518.anInt5930 -= (1898253385 * ((692693311 * Class473.anInt5606 + Class501.anInt5828 * 1123046983 * -i_36_ / 1000) << 3));
			Class518.anInt5930 = (1236051449 * Class518.anInt5930 & 0x3fff) * 1898253385;
		}
		int i_37_ = i_35_ - 1236051449 * Class518.anInt5930;
		if (i_37_ > 8192)
			i_37_ -= 16384;
		if (i_37_ < -8192)
			i_37_ += 16384;
		if (i_37_ < 0 && i_36_ > 0 || i_37_ > 0 && i_36_ < 0)
			Class518.anInt5930 = 1898253385 * i_35_;
		Class121.anInt1525 = 0;
	}

	public static final void method16114(int i) {
		if (!client.aBool7331) {
			client.aFloat7284 += (-12.0F - client.aFloat7284) / 2.0F;
			client.aBool7371 = true;
			client.aBool7331 = true;
		}
	}
}
