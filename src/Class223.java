/* Class223 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class223 {
	Class477 aClass477_2767;
	int anInt2768;
	int anInt2769;
	Class282_Sub50 aClass282_Sub50_2770 = new Class282_Sub50();
	Class465 aClass465_2771;
	public static Class495 aClass495_2772;

	public void method3757() {
		((Class223) this).aClass477_2767.method7935((byte) 40);
		((Class223) this).aClass465_2771.method7749(-1765235443);
		((Class223) this).aClass282_Sub50_2770 = new Class282_Sub50();
		((Class223) this).anInt2769 = ((Class223) this).anInt2768 * -2090494407;
	}

	public Class282_Sub50 method3758(long l) {
		Class282_Sub50 class282_sub50 = (Class282_Sub50) ((Class223) this).aClass465_2771.method7754(l);
		if (null != class282_sub50)
			((Class223) this).aClass477_2767.method7936(class282_sub50, -1738910950);
		return class282_sub50;
	}

	public void method3759(Class282_Sub50 class282_sub50, long l) {
		if (0 == ((Class223) this).anInt2769 * -367780537) {
			Class282_Sub50 class282_sub50_0_ = ((Class223) this).aClass477_2767.method7937(600411374);
			class282_sub50_0_.method4991(-371378792);
			class282_sub50_0_.method13452((byte) -5);
			if (((Class223) this).aClass282_Sub50_2770 == class282_sub50_0_) {
				class282_sub50_0_ = ((Class223) this).aClass477_2767.method7937(1037693707);
				class282_sub50_0_.method4991(-371378792);
				class282_sub50_0_.method13452((byte) -5);
			}
		} else
			((Class223) this).anInt2769 -= 1439202423;
		((Class223) this).aClass465_2771.method7765(class282_sub50, l);
		((Class223) this).aClass477_2767.method7936(class282_sub50, -1738910950);
	}

	public Class223(int i) {
		((Class223) this).aClass477_2767 = new Class477();
		((Class223) this).anInt2768 = i * 1139877935;
		((Class223) this).anInt2769 = i * 1439202423;
		int i_1_;
		for (i_1_ = 1; i_1_ + i_1_ < i; i_1_ += i_1_) {
			/* empty */
		}
		((Class223) this).aClass465_2771 = new Class465(i_1_);
	}

	public void method3760(int i) {
		((Class223) this).aClass477_2767.method7935((byte) -14);
		((Class223) this).aClass465_2771.method7749(-1319395120);
		((Class223) this).aClass282_Sub50_2770 = new Class282_Sub50();
		((Class223) this).anInt2769 = ((Class223) this).anInt2768 * -2090494407;
	}

	public Class282_Sub50 method3761(long l) {
		Class282_Sub50 class282_sub50 = (Class282_Sub50) ((Class223) this).aClass465_2771.method7754(l);
		if (null != class282_sub50)
			((Class223) this).aClass477_2767.method7936(class282_sub50, -1738910950);
		return class282_sub50;
	}

	public Class282_Sub50 method3762(long l) {
		Class282_Sub50 class282_sub50 = (Class282_Sub50) ((Class223) this).aClass465_2771.method7754(l);
		if (null != class282_sub50)
			((Class223) this).aClass477_2767.method7936(class282_sub50, -1738910950);
		return class282_sub50;
	}

	public void method3763(Class282_Sub50 class282_sub50, long l) {
		if (0 == ((Class223) this).anInt2769 * -367780537) {
			Class282_Sub50 class282_sub50_2_ = ((Class223) this).aClass477_2767.method7937(-150780366);
			class282_sub50_2_.method4991(-371378792);
			class282_sub50_2_.method13452((byte) -5);
			if (((Class223) this).aClass282_Sub50_2770 == class282_sub50_2_) {
				class282_sub50_2_ = ((Class223) this).aClass477_2767.method7937(1353399879);
				class282_sub50_2_.method4991(-371378792);
				class282_sub50_2_.method13452((byte) -5);
			}
		} else
			((Class223) this).anInt2769 -= 1439202423;
		((Class223) this).aClass465_2771.method7765(class282_sub50, l);
		((Class223) this).aClass477_2767.method7936(class282_sub50, -1738910950);
	}

	public void method3764() {
		((Class223) this).aClass477_2767.method7935((byte) 11);
		((Class223) this).aClass465_2771.method7749(-1952522546);
		((Class223) this).aClass282_Sub50_2770 = new Class282_Sub50();
		((Class223) this).anInt2769 = ((Class223) this).anInt2768 * -2090494407;
	}

	public void method3765() {
		((Class223) this).aClass477_2767.method7935((byte) -32);
		((Class223) this).aClass465_2771.method7749(1512917282);
		((Class223) this).aClass282_Sub50_2770 = new Class282_Sub50();
		((Class223) this).anInt2769 = ((Class223) this).anInt2768 * -2090494407;
	}

	public void method3766(Class282_Sub50 class282_sub50, long l) {
		if (0 == ((Class223) this).anInt2769 * -367780537) {
			Class282_Sub50 class282_sub50_3_ = ((Class223) this).aClass477_2767.method7937(1704845716);
			class282_sub50_3_.method4991(-371378792);
			class282_sub50_3_.method13452((byte) -5);
			if (((Class223) this).aClass282_Sub50_2770 == class282_sub50_3_) {
				class282_sub50_3_ = ((Class223) this).aClass477_2767.method7937(-1222944703);
				class282_sub50_3_.method4991(-371378792);
				class282_sub50_3_.method13452((byte) -5);
			}
		} else
			((Class223) this).anInt2769 -= 1439202423;
		((Class223) this).aClass465_2771.method7765(class282_sub50, l);
		((Class223) this).aClass477_2767.method7936(class282_sub50, -1738910950);
	}

	public Class282_Sub50 method3767(long l) {
		Class282_Sub50 class282_sub50 = (Class282_Sub50) ((Class223) this).aClass465_2771.method7754(l);
		if (null != class282_sub50)
			((Class223) this).aClass477_2767.method7936(class282_sub50, -1738910950);
		return class282_sub50;
	}

	static final void method3768(Class527 class527, int i) {
		int i_4_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class8_Sub2.method14263(i_4_ >> 14 & 0x3fff, i_4_ & 0x3fff, (byte) 0);
	}

	static final void method3769(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = ((Class527) class527).aClass521_Sub1_Sub2_Sub1_7014.method12997(-218366287);
	}

	public static void method3770(byte i) {
		if (Class93.method1576(285718929))
			Class169.method2877(new Class237(), (byte) 125);
	}
}
