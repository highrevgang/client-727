/* fa - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class fa extends Class160 implements Interface40, Interface9 {
	long nativeid;
	boolean aBool7477 = false;

	public int method229() {
		return WA(((fa) this).nativeid);
	}

	fa(ja var_ja, int[] is, int i, int i_0_, int i_1_, int i_2_, boolean bool) {
		RA(var_ja, is, i, i_0_, i_1_, i_2_, bool);
	}

	public int method2788() {
		return GA(((fa) this).nativeid);
	}

	fa(ja var_ja, int i, int i_3_, int i_4_, int i_5_, boolean bool) {
		K(var_ja, i, i_3_, i_4_, i_5_, bool);
	}

	native void e(long l, float f, float f_6_, float f_7_, float f_8_, float f_9_, float f_10_, int i, int i_11_, int i_12_, int i_13_);

	public void method2750(int i, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_) {
		ea(((fa) this).nativeid, i, i_14_, i_15_, i_16_, i_17_, i_18_);
	}

	public int method75() {
		return da(((fa) this).nativeid);
	}

	native void K(ja var_ja, int i, int i_19_, int i_20_, int i_21_, boolean bool);

	public void finalize() {
		if (((fa) this).nativeid != 0L)
			Class156.method2642(this, (byte) 95);
	}

	public void ma(boolean bool) {
		M(((fa) this).nativeid, bool);
	}

	native int native_do(long l);

	public void method2743(int i, int i_22_, int i_23_, int i_24_) {
		U(((fa) this).nativeid, i, i_22_, i_23_, i_24_);
	}

	public void method2782(int[] is) {
		F(((fa) this).nativeid, is);
	}

	native void dr(long l, int i, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_);

	native void RA(ja var_ja, int[] is, int i, int i_30_, int i_31_, int i_32_, boolean bool);

	public int method1() {
		return da(((fa) this).nativeid);
	}

	public int method2747() {
		return da(((fa) this).nativeid);
	}

	native int da(long l);

	public int method228() {
		return WA(((fa) this).nativeid);
	}

	native int WA(long l);

	native void ee(long l, int i, int i_33_, int i_34_, int i_35_, int i_36_);

	public void method2784(int i, int i_37_, Class455 class455, int i_38_, int i_39_) {
		o(((fa) this).nativeid, i, i_37_, ((wa) (wa) class455).nativeid, i_38_, i_39_);
	}

	native int GA(long l);

	native void dx(ja var_ja, int[] is, byte[] is_40_, byte[] is_41_, int i, int i_42_, int i_43_, int i_44_);

	native int ba(long l);

	public Interface9 method2808() {
		return ((fa) this).aBool7477 ? this : null;
	}

	public void method2777(int[] is) {
		F(((fa) this).nativeid, is);
	}

	native void ea(long l, int i, int i_45_, int i_46_, int i_47_, int i_48_, int i_49_);

	public void method2751(int i, int i_50_, int i_51_) {
		j(((fa) this).nativeid, i, i_50_, i_51_);
	}

	native void j(long l, int i, int i_52_, int i_53_);

	public void method2742(int i, int i_54_, int i_55_, int i_56_, int i_57_) {
		Y(((fa) this).nativeid, i, i_54_, i_55_, i_56_, i_57_);
	}

	native void Y(long l, int i, int i_58_, int i_59_, int i_60_, int i_61_);

	public void method2773(int i, int i_62_, Class455 class455, int i_63_, int i_64_) {
		o(((fa) this).nativeid, i, i_62_, ((wa) (wa) class455).nativeid, i_63_, i_64_);
	}

	native void o(long l, int i, int i_65_, long l_66_, int i_67_, int i_68_);

	void method2755(int i, int i_69_, int i_70_, int i_71_, int i_72_, int i_73_, int i_74_, int i_75_) {
		ha(((fa) this).nativeid, i, i_69_, i_70_, i_71_, i_72_, i_73_, i_74_, i_75_);
	}

	native void eo(long l, int i, int i_76_, int i_77_);

	public void method2772(int i, int i_78_, int i_79_, int i_80_, int i_81_, int i_82_, int i_83_) {
		aa(((fa) this).nativeid, i, i_78_, i_79_, i_80_, i_81_, i_82_, i_83_);
	}

	native void aa(long l, int i, int i_84_, int i_85_, int i_86_, int i_87_, int i_88_, int i_89_);

	native void ex(long l, float f, float f_90_, float f_91_, float f_92_, float f_93_, float f_94_, int i, int i_95_, int i_96_, int i_97_);

	native void dp(long l, int i, int i_98_, int i_99_, int i_100_);

	void method2766(float f, float f_101_, float f_102_, float f_103_, float f_104_, float f_105_, int i, Class455 class455, int i_106_, int i_107_) {
		FA(((fa) this).nativeid, f, f_101_, f_102_, f_103_, f_104_, f_105_, i, ((wa) (wa) class455).nativeid, i_106_, i_107_, ((fa) this).aBool7477);
	}

	native void FA(long l, float f, float f_108_, float f_109_, float f_110_, float f_111_, float f_112_, int i, long l_113_, int i_114_, int i_115_, boolean bool);

	public void method26() {
		/* empty */
	}

	native void dm(long l, int i, int i_116_, int i_117_, int i_118_);

	public void method2768(int i, int i_119_, int i_120_, int i_121_, int i_122_, int i_123_) {
		ea(((fa) this).nativeid, i, i_119_, i_120_, i_121_, i_122_, i_123_);
	}

	public void method2769(int i, int i_124_, int i_125_, int i_126_, int i_127_, int i_128_) {
		ea(((fa) this).nativeid, i, i_124_, i_125_, i_126_, i_127_, i_128_);
	}

	public void method2771(int i, int i_129_, int i_130_) {
		j(((fa) this).nativeid, i, i_129_, i_130_);
	}

	native int ds(long l);

	public Interface9 method2801() {
		return ((fa) this).aBool7477 ? this : null;
	}

	public int method2794() {
		return da(((fa) this).nativeid);
	}

	public void method2774(int i, int i_131_, Class455 class455, int i_132_, int i_133_) {
		o(((fa) this).nativeid, i, i_131_, ((wa) (wa) class455).nativeid, i_132_, i_133_);
	}

	void method2775(int i, int i_134_, int i_135_, int i_136_, int i_137_, int i_138_, int i_139_, int i_140_) {
		ha(((fa) this).nativeid, i, i_134_, i_135_, i_136_, i_137_, i_138_, i_139_, i_140_);
	}

	void method2787(int i, int i_141_, int i_142_, int i_143_, int i_144_, int i_145_, int i_146_, int i_147_) {
		ha(((fa) this).nativeid, i, i_141_, i_142_, i_143_, i_144_, i_145_, i_146_, i_147_);
	}

	public void method2770(int i, int i_148_, int i_149_) {
		j(((fa) this).nativeid, i, i_148_, i_149_);
	}

	public int method76() {
		return da(((fa) this).nativeid);
	}

	public int method39() {
		return GA(((fa) this).nativeid);
	}

	public int method73() {
		return GA(((fa) this).nativeid);
	}

	native void ey(long l, int i, int i_150_, long l_151_, int i_152_, int i_153_);

	public void method2767(int i, int i_154_, int i_155_, int i_156_, int i_157_, int i_158_) {
		ea(((fa) this).nativeid, i, i_154_, i_155_, i_156_, i_157_, i_158_);
	}

	native void M(long l, boolean bool);

	public void method2783(int i, int i_159_, int i_160_, int i_161_) {
		U(((fa) this).nativeid, i, i_159_, i_160_, i_161_);
	}

	public void method2781(int i, int i_162_, int i_163_, int i_164_) {
		U(((fa) this).nativeid, i, i_162_, i_163_, i_164_);
	}

	public void method2780(int i, int i_165_, int i_166_, int i_167_) {
		U(((fa) this).nativeid, i, i_165_, i_166_, i_167_);
	}

	fa(ja var_ja, int[] is, byte[] is_168_, byte[] is_169_, int i, int i_170_, int i_171_, int i_172_) {
		EA(var_ja, is, is_168_, is_169_, i, i_170_, i_171_, i_172_);
	}

	public int method2793() {
		return GA(((fa) this).nativeid);
	}

	public void method2804(int[] is) {
		F(((fa) this).nativeid, is);
	}

	fa(ja var_ja, int i, int i_173_, boolean bool) {
		((fa) this).aBool7477 = bool;
		UA(var_ja, i, i_173_);
	}

	public void method2785(int[] is) {
		F(((fa) this).nativeid, is);
	}

	public int method2786() {
		return da(((fa) this).nativeid);
	}

	native int di(long l);

	native void dl(ja var_ja, int i, int i_174_, int i_175_, int i_176_, boolean bool);

	public int method2765() {
		return GA(((fa) this).nativeid);
	}

	public int method225() {
		return WA(((fa) this).nativeid);
	}

	public int method227() {
		return WA(((fa) this).nativeid);
	}

	public int method226() {
		return WA(((fa) this).nativeid);
	}

	public void method2778(int i, int i_177_, int i_178_, int i_179_, int i_180_) {
		Y(((fa) this).nativeid, i, i_177_, i_178_, i_179_, i_180_);
	}

	native void dq(long l, int i, int i_181_, int i_182_);

	public int method2757() {
		return ba(((fa) this).nativeid);
	}

	public void method2749(int i, int i_183_, int i_184_, int i_185_, int i_186_) {
		Y(((fa) this).nativeid, i, i_183_, i_184_, i_185_, i_186_);
	}

	public void method2799(int i, int i_187_, int i_188_, int i_189_, int i_190_, int i_191_, int i_192_) {
		aa(((fa) this).nativeid, i, i_187_, i_188_, i_189_, i_190_, i_191_, i_192_);
	}

	public void method2796(int i, int i_193_, int i_194_, int i_195_, int i_196_, int i_197_, int i_198_) {
		aa(((fa) this).nativeid, i, i_193_, i_194_, i_195_, i_196_, i_197_, i_198_);
	}

	void method2791(float f, float f_199_, float f_200_, float f_201_, float f_202_, float f_203_, int i, int i_204_, int i_205_, int i_206_) {
		e(((fa) this).nativeid, f, f_199_, f_200_, f_201_, f_202_, f_203_, i, i_204_, i_205_, i_206_);
	}

	void method2798(float f, float f_207_, float f_208_, float f_209_, float f_210_, float f_211_, int i, int i_212_, int i_213_, int i_214_) {
		e(((fa) this).nativeid, f, f_207_, f_208_, f_209_, f_210_, f_211_, i, i_212_, i_213_, i_214_);
	}

	void method2812(float f, float f_215_, float f_216_, float f_217_, float f_218_, float f_219_, int i, int i_220_, int i_221_, int i_222_) {
		e(((fa) this).nativeid, f, f_215_, f_216_, f_217_, f_218_, f_219_, i, i_220_, i_221_, i_222_);
	}

	void method2800(float f, float f_223_, float f_224_, float f_225_, float f_226_, float f_227_, int i, Class455 class455, int i_228_, int i_229_) {
		FA(((fa) this).nativeid, f, f_223_, f_224_, f_225_, f_226_, f_227_, i, ((wa) (wa) class455).nativeid, i_228_, i_229_, ((fa) this).aBool7477);
	}

	void method2811(float f, float f_230_, float f_231_, float f_232_, float f_233_, float f_234_, int i, Class455 class455, int i_235_, int i_236_) {
		FA(((fa) this).nativeid, f, f_230_, f_231_, f_232_, f_233_, f_234_, i, ((wa) (wa) class455).nativeid, i_235_, i_236_, ((fa) this).aBool7477);
	}

	void method12076() {
		if (((fa) this).nativeid != 0L)
			Class156.method2642(this, (byte) 107);
	}

	void method12077() {
		if (((fa) this).nativeid != 0L)
			Class156.method2642(this, (byte) 67);
	}

	public void y(boolean bool) {
		M(((fa) this).nativeid, bool);
	}

	native void dh(ja var_ja, int i, int i_237_, int i_238_, int i_239_, boolean bool);

	public void x(boolean bool) {
		M(((fa) this).nativeid, bool);
	}

	native void cs(ja var_ja, int[] is, int i, int i_240_, int i_241_, int i_242_, boolean bool);

	native void dj(ja var_ja, int[] is, int i, int i_243_, int i_244_, int i_245_, boolean bool);

	native void de(ja var_ja, int[] is, int i, int i_246_, int i_247_, int i_248_, boolean bool);

	native void ek(long l, int i, int i_249_, long l_250_, int i_251_, int i_252_);

	native void dc(ja var_ja, int[] is, byte[] is_253_, byte[] is_254_, int i, int i_255_, int i_256_, int i_257_);

	public int method2753() {
		return ba(((fa) this).nativeid);
	}

	native void F(long l, int[] is);

	native void dk(long l, boolean bool);

	native void dy(long l, boolean bool);

	void method2764(float f, float f_258_, float f_259_, float f_260_, float f_261_, float f_262_, int i, int i_263_, int i_264_, int i_265_) {
		e(((fa) this).nativeid, f, f_258_, f_259_, f_260_, f_261_, f_262_, i, i_263_, i_264_, i_265_);
	}

	native void EA(ja var_ja, int[] is, byte[] is_266_, byte[] is_267_, int i, int i_268_, int i_269_, int i_270_);

	public int method2792() {
		return ba(((fa) this).nativeid);
	}

	native void dg(long l, int[] is);

	native void dd(long l, int[] is);

	native void U(long l, int i, int i_271_, int i_272_, int i_273_);

	public int method77() {
		return GA(((fa) this).nativeid);
	}

	native int dv(long l);

	native void ha(long l, int i, int i_274_, int i_275_, int i_276_, int i_277_, int i_278_, int i_279_, int i_280_);

	native int dz(long l);

	native int dt(long l);

	public int method74() {
		return GA(((fa) this).nativeid);
	}

	native int dn(long l);

	native void dw(long l, int i, int i_281_, int i_282_, int i_283_, int i_284_, int i_285_);

	public int method2748() {
		return ba(((fa) this).nativeid);
	}

	native int df(long l);

	native void db(long l, int i, int i_286_, int i_287_);

	native void ef(long l, int i, int i_288_, int i_289_);

	native void cy(ja var_ja, int i, int i_290_);

	native void eb(long l, int i, int i_291_, int i_292_, int i_293_, int i_294_);

	native void UA(ja var_ja, int i, int i_295_);

	public void method32() {
		/* empty */
	}

	native void es(long l, int i, int i_296_, long l_297_, int i_298_, int i_299_);

	public void method2744(int[] is) {
		F(((fa) this).nativeid, is);
	}

	native void et(long l, int i, int i_300_, int i_301_, int i_302_, int i_303_, int i_304_, int i_305_, int i_306_);

	native void ew(long l, int i, int i_307_, int i_308_, int i_309_, int i_310_, int i_311_, int i_312_, int i_313_);

	native void em(long l, int i, int i_314_, int i_315_, int i_316_, int i_317_, int i_318_, int i_319_, int i_320_);

	native void ed(long l, int i, int i_321_, int i_322_, int i_323_, int i_324_, int i_325_, int i_326_);

	native void du(long l, int[] is);

	native void ep(long l, float f, float f_327_, float f_328_, float f_329_, float f_330_, float f_331_, int i, int i_332_, int i_333_, int i_334_);
}
