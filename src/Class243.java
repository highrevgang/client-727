/* Class243 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class243 {
	static Class243 aClass243_2983;
	public static Class243 aClass243_2984;
	static Class243 aClass243_2985;
	static Class243 aClass243_2986;
	static Class243 aClass243_2987;
	static Class243 aClass243_2988;
	static Class243 aClass243_2989;
	static Class243 aClass243_2990;
	public static Class243 aClass243_2991;
	public static Class243 aClass243_2992;
	public static Class243 aClass243_2993 = new Class243(0, 2, 2, 1);
	static Class243 aClass243_2994;
	public static Class243 aClass243_2995 = new Class243(1, 2, 2, 0);
	public static Class243 aClass243_2996;
	public int anInt2997;
	public int anInt2998;
	public int anInt2999;
	public int anInt3000;
	public static Class498 aClass498_3001;

	public static Class243 method4169(int i) {
		Class243[] class243s = Class92.method1561((short) -1058);
		for (int i_0_ = 0; i_0_ < class243s.length; i_0_++) {
			if (i == class243s[i_0_].anInt2997 * -1869685303)
				return class243s[i_0_];
		}
		return null;
	}

	static {
		aClass243_2985 = new Class243(2, 4, 4, 0);
		aClass243_2986 = new Class243(4, 1, 1, 1);
		aClass243_2984 = new Class243(6, 0, 4, 2);
		aClass243_2996 = new Class243(7, 0, 1, 1);
		aClass243_2989 = new Class243(8, 0, 4, 1);
		aClass243_2990 = new Class243(9, 0, 4, 1);
		aClass243_2991 = new Class243(10, 2, 2, 0);
		aClass243_2992 = new Class243(11, 0, 1, 2);
		aClass243_2987 = new Class243(12, 0, 1, 0);
		aClass243_2983 = new Class243(13, 0, 1, 0);
		aClass243_2988 = new Class243(14, 0, 4, 1);
		aClass243_2994 = new Class243(15, 0, 1, 0);
	}

	Class243(int i, int i_1_, int i_2_, int i_3_) {
		anInt2997 = i * -1336554375;
		anInt2998 = -1079524855 * i_1_;
		anInt2999 = i_2_ * 538118299;
		anInt3000 = 968865815 * i_3_;
	}

	static Class243[] method4170() {
		return new Class243[] { aClass243_2984, aClass243_2987, aClass243_2986, aClass243_2994, aClass243_2991, aClass243_2996, aClass243_2985, aClass243_2990, aClass243_2989, aClass243_2993, aClass243_2983, aClass243_2988, aClass243_2995, aClass243_2992 };
	}

	public static Class243 method4171(int i) {
		Class243[] class243s = Class92.method1561((short) 23630);
		for (int i_4_ = 0; i_4_ < class243s.length; i_4_++) {
			if (i == class243s[i_4_].anInt2997 * -1869685303)
				return class243s[i_4_];
		}
		return null;
	}

	static Class243[] method4172() {
		return new Class243[] { aClass243_2984, aClass243_2987, aClass243_2986, aClass243_2994, aClass243_2991, aClass243_2996, aClass243_2985, aClass243_2990, aClass243_2989, aClass243_2993, aClass243_2983, aClass243_2988, aClass243_2995, aClass243_2992 };
	}

	static final void method4173(Class527 class527, int i) {
		int i_5_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_5_, (byte) 117);
		Class413.method6942(class118, class527, -839720836);
	}

	static final void method4174(Class527 class527, int i) {
		int i_6_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_6_, (byte) 10);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_6_ >> 16];
		Class457.method7667(class118, class98, class527, 668340664);
	}

	static final void method4175(Class527 class527, short i) {
		int i_7_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_7_, (byte) 17);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_7_ >> 16];
		Class125.method2170(class118, class98, class527, 809860903);
	}

	static final void method4176(Class527 class527, int i) {
		int i_8_ = ((client.anIntArrayArrayArray7302[(((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 -= 141891001) * 1942118537)])]).length >> 1);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_8_;
	}

	static final void method4177(Class527 class527, byte i) {
		((Class527) class527).anInt7012 -= 567564004;
		int i_9_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		int i_10_ = (((Class527) class527).anIntArray6999[1 + 1942118537 * ((Class527) class527).anInt7012]);
		int i_11_ = (((Class527) class527).anIntArray6999[2 + ((Class527) class527).anInt7012 * 1942118537]);
		int i_12_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 + 3]);
		Class219 class219 = client.aClass257_7353.method4519(1537474699);
		Class225.method3794(((i_9_ >> 14 & 0x3fff) - class219.anInt2711 * 1948093437), (i_9_ & 0x3fff) - -1002240017 * class219.anInt2712, i_10_ << 2, i_11_, i_12_, false, (byte) -1);
	}

	static final void method4178(Class527 class527, byte i) {
		((Class527) class527).anInt7012 -= 425673003;
		int i_13_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_14_ = (((Class527) class527).anIntArray6999[1 + ((Class527) class527).anInt7012 * 1942118537]);
		int i_15_ = (((Class527) class527).anIntArray6999[2 + 1942118537 * ((Class527) class527).anInt7012]);
		long l = Class42.method891(0, 0, 12, i_13_, i_14_, i_15_, 1796735712);
		int i_16_ = Class474.method7914(l);
		if (i_15_ < 1970) {
			if (i <= 0)
				return;
			i_16_--;
		}
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_16_;
	}

	static final void method4179(int i, int i_17_, int i_18_, int i_19_, Class476 class476, byte i_20_) {
		if (i_18_ >= 1 && i_19_ >= 1 && i_18_ <= client.aClass257_7353.method4424(1811223828) - 2 && i_19_ <= client.aClass257_7353.method4451(-1564374011) - 2) {
			if (client.aClass257_7353.method4430(-1940602014) != null) {
				Interface12 interface12 = client.aClass257_7353.method4441(1508379413).method12467(i, i_17_, i_18_, i_19_, 2009974413);
				if (interface12 != null) {
					if (interface12 instanceof Class521_Sub1_Sub1_Sub5)
						((Class521_Sub1_Sub1_Sub5) interface12).method16096(class476, -59212941);
					else if (interface12 instanceof Class521_Sub1_Sub3_Sub2)
						((Class521_Sub1_Sub3_Sub2) interface12).method16091(class476, -737324181);
					else if (interface12 instanceof Class521_Sub1_Sub5_Sub2)
						((Class521_Sub1_Sub5_Sub2) interface12).method16108(class476, -2063213607);
					else if (interface12 instanceof Class521_Sub1_Sub4_Sub2)
						((Class521_Sub1_Sub4_Sub2) interface12).method16082(class476, -449891505);
				}
			}
		}
	}

	static boolean method4180(int i, byte i_21_) {
		return i == 7 || i == 6;
	}

	static final void method4181(Class527 class527, byte i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		Class96_Sub19.method14665(string, 1220033429);
	}
}
