/* Class84 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class84 {
	public int anInt809;
	public int anInt810;
	public int anInt811;
	public int anInt812;
	public static Class521_Sub1_Sub1_Sub2_Sub1 myPlayer;

	Class84 method1458(int i) {
		return new Class84(anInt812 * -221586257, i, anInt811 * 1852505231, anInt810 * 1823219025);
	}

	Class84 method1459(int i, int i_0_) {
		return new Class84(anInt812 * -221586257, i, anInt811 * 1852505231, anInt810 * 1823219025);
	}

	Class84 method1460(int i) {
		return new Class84(anInt812 * -221586257, i, anInt811 * 1852505231, anInt810 * 1823219025);
	}

	Class84(int i, int i_1_, int i_2_, int i_3_) {
		anInt812 = i * 755890767;
		anInt809 = 1792660695 * i_1_;
		anInt811 = -1869176209 * i_2_;
		anInt810 = i_3_ * 2073643953;
	}

	public static Class273 method1461(int i) {
		Class273_Sub1 class273_sub1;
		try {
			class273_sub1 = new Class273_Sub1();
		} catch (Throwable throwable) {
			return new Class273_Sub2();
		}
		return class273_sub1;
	}

	static final void method1462(Class527 class527, int i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		Class16.method567(string, true, (byte) -67);
	}

	static final void method1463(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class393.aClass282_Sub54_4783.aClass468_Sub7_8210.method12666(2085478304);
	}

	static final void method1464(int i) {
		for (int i_4_ = Class158_Sub1.aClass3_8507.method264(true, (byte) 112); -1 != i_4_; i_4_ = Class158_Sub1.aClass3_8507.method264(false, (byte) 48)) {
			Class499.method8333(i_4_, 1768744786);
			client.anIntArray7379[(client.anInt7453 += 1195975743) * 1129368511 - 1 & 0x1f] = i_4_;
		}
		for (Class282_Sub50_Sub12 class282_sub50_sub12 = Class478.method8022(-54255165); class282_sub50_sub12 != null; class282_sub50_sub12 = Class478.method8022(153366181)) {
			int i_5_ = class282_sub50_sub12.method14953(701484568);
			long l = class282_sub50_sub12.method14967(1056683905);
			if (1 == i_5_) {
				Class320.anIntArray3724[(int) l] = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * 492007);
				client.aBool7400 |= Class282_Sub17_Sub2.aBoolArray9934[(int) l];
				client.anIntArray7150[(client.anInt7368 += -723070537) * -29238265 - 1 & 0x1f] = (int) l;
			} else if (2 == i_5_) {
				Class462.aStringArray5548[(int) l] = (((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640);
				client.anIntArray7387[(client.anInt7388 += -490814007) * 721577081 - 1 & 0x1f] = (int) l;
			} else if (3 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 54);
				if (!((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640.equals(class118.aString1391)) {
					class118.aString1391 = (((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640);
					Class109.method1858(class118, (byte) -73);
				}
			} else if (i_5_ == 22) {
				Class118 class118 = Class117.method1981((int) l, (byte) 57);
				if ((492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) == 1) != class118.aBool1363) {
					class118.aBool1363 = 1 == (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) * 492007;
					Class109.method1858(class118, (byte) -26);
				}
			} else if (4 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 70);
				int i_6_ = 492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668);
				int i_7_ = (989699687 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641));
				int i_8_ = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 * -1412929499);
				if (i_6_ != 2131324949 * class118.anInt1329 || -402732635 * class118.anInt1330 != i_7_ || i_8_ != 1871217945 * class118.anInt1339) {
					class118.anInt1329 = 589750077 * i_6_;
					class118.anInt1330 = -636815827 * i_7_;
					class118.anInt1339 = 207030057 * i_8_;
					class118.aClass417_1308 = null;
					Class109.method1858(class118, (byte) 2);
				}
			} else if (5 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 59);
				if ((((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * 492007) != class118.anInt1321 * 1241177935) {
					if (492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) != -1) {
						if (null == class118.aClass456_1437)
							class118.aClass456_1437 = new Class456_Sub1();
						class118.aClass456_1437.method7567((((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) * 492007, (short) 8960);
					} else
						class118.aClass456_1437 = null;
					class118.anInt1321 = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) * 1679871465;
					Class109.method1858(class118, (byte) -35);
				}
			} else if (6 == i_5_) {
				int i_9_ = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * 492007);
				int i_10_ = i_9_ >> 10 & 0x1f;
				int i_11_ = i_9_ >> 5 & 0x1f;
				int i_12_ = i_9_ & 0x1f;
				int i_13_ = (i_12_ << 3) + ((i_10_ << 19) + (i_11_ << 11));
				Class118 class118 = Class117.method1981((int) l, (byte) 106);
				if (class118.anInt1264 * -795991475 != i_13_) {
					class118.anInt1264 = i_13_ * -1774983547;
					Class109.method1858(class118, (byte) 85);
				}
			} else if (i_5_ == 7) {
				Class118 class118 = Class117.method1981((int) l, (byte) 66);
				boolean bool = ((((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * 492007) == 1);
				if (bool != class118.aBool1306) {
					class118.aBool1306 = bool;
					Class109.method1858(class118, (byte) -2);
				}
			} else if (i_5_ == 8) {
				Class118 class118 = Class117.method1981((int) l, (byte) 73);
				if (492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) != class118.anInt1385 * -1627383873 || (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 * 989699687) != 682349813 * class118.anInt1336 || (-1823193031 * class118.anInt1343 != -1412929499 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642)) {
					class118.anInt1385 = 1287446489 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668;
					class118.anInt1336 = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641) * -1587827605;
					class118.anInt1343 = -69327027 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642;
					if (-1 != class118.anInt1426 * -56249735) {
						if (class118.anInt1417 * -1326245411 > 0)
							class118.anInt1343 = (class118.anInt1343 * 1787365152 / (class118.anInt1417 * -1326245411) * -807852535);
						else if (1352091441 * class118.anInt1297 > 0)
							class118.anInt1343 = (class118.anInt1343 * 1787365152 / (1352091441 * class118.anInt1297) * -807852535);
					}
					Class109.method1858(class118, (byte) -89);
				}
			} else if (i_5_ == 9) {
				Class118 class118 = Class117.method1981((int) l, (byte) 121);
				if ((-56249735 * class118.anInt1426 != 492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668)) || (989699687 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 != class118.anInt1427 * 6040081)) {
					class118.anInt1426 = 686997343 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668;
					class118.anInt1427 = 943229175 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641;
					Class109.method1858(class118, (byte) -62);
				}
			} else if (10 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 4);
				if (492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) != 1552292309 * class118.anInt1293 || (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 * 989699687) != 539377845 * class118.anInt1334 || (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 * -1412929499) != -1009302201 * class118.anInt1337) {
					class118.anInt1293 = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) * -1065750837;
					class118.anInt1334 = 780517419 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641;
					class118.anInt1337 = -2145972941 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642;
					Class109.method1858(class118, (byte) -28);
				}
			} else if (i_5_ == 11) {
				Class118 class118 = Class117.method1981((int) l, (byte) 61);
				class118.aByte1333 = (byte) 0;
				class118.anInt1299 = (class118.anInt1295 = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) * 631081225) * 1054420063;
				class118.aByte1355 = (byte) 0;
				class118.anInt1428 = (class118.anInt1296 = 1759731013 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641)) * 822620217;
				Class109.method1858(class118, (byte) 30);
			} else if (12 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 13);
				int i_14_ = 492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668);
				if (class118 != null && class118.anInt1268 * -2131393857 == 0) {
					if (i_14_ > (-37350919 * class118.anInt1314 - class118.anInt1429 * -492594917))
						i_14_ = (class118.anInt1314 * -37350919 - -492594917 * class118.anInt1429);
					if (i_14_ < 0)
						i_14_ = 0;
					if (i_14_ != 682782159 * class118.anInt1312) {
						class118.anInt1312 = 771324207 * i_14_;
						Class109.method1858(class118, (byte) -20);
					}
				}
			} else if (i_5_ == 14) {
				Class118 class118 = Class117.method1981((int) l, (byte) 39);
				class118.anInt1320 = -105570421 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668;
			} else if (i_5_ == 15) {
				Class187.aBool2360 = true;
				Class187.anInt2361 = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * -1834383347);
				Class187.anInt2359 = 1935238793 * ((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641;
			} else if (16 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 73);
				class118.anInt1277 = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * -1318769315);
			} else if (20 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 127);
				class118.aBool1356 = 1 == (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) * 492007;
			} else if (21 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 114);
				class118.aBool1328 = 1 == (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668) * 492007;
			} else if (i_5_ == 17) {
				Class118 class118 = Class117.method1981((int) l, (byte) 82);
				class118.anInt1435 = (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * -1713155233);
			} else if (18 == i_5_) {
				Class118 class118 = Class117.method1981((int) l, (byte) 125);
				int i_15_ = (int) (l >> 32);
				class118.method2007(i_15_, (short) (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 * 492007), (short) (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 * 989699687), -328591291);
			} else if (i_5_ == 19) {
				Class118 class118 = Class117.method1981((int) l, (byte) 16);
				int i_16_ = (int) (l >> 32);
				class118.method2013(i_16_, (short) (492007 * (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668)), (short) (((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 * 989699687), (byte) 69);
			}
		}
	}

	static boolean method1465(int i) {
		return Class86.method1481(Class149_Sub2.aClass511_9314.aClass232_5873, (byte) -3);
	}
}
