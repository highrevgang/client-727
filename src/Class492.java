/* Class492 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class492 {
	static Class465 aClass465_5774 = new Class465(4);
	static final int anInt5775 = 4096;
	static Class502 aClass502_5776 = new Class502();
	public static Class327_Sub1[] aClass327_Sub1Array5777;

	public static void method8247(int i) {
		Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7754((long) i);
		if (null != class282_sub37) {
			((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4947((byte) 19);
			Class62.method1260((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), ((Class282_Sub37) class282_sub37).aBool7998, -93907801);
			class282_sub37.method4991(-371378792);
		}
	}

	public static void method8248(int i, int i_0_, boolean bool) {
		if (aClass465_5774.method7754((long) i) == null) {
			if (!client.aBool7393)
				Class62.method1260(i, bool, -1854855925);
			else {
				Class282_Sub37 class282_sub37 = new Class282_Sub37(i, new Class278_Sub1(4096, (Class209.aClass317_2663), i), i_0_, bool);
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4915(Class223.aClass495_2772.method8276(1458414646), 443995743);
				aClass465_5774.method7765(class282_sub37, (long) i);
			}
		}
	}

	Class492() throws Throwable {
		throw new Error();
	}

	public static void method8249(int i, int i_1_, boolean bool) {
		if (aClass465_5774.method7754((long) i) == null) {
			if (!client.aBool7393)
				Class62.method1260(i, bool, 953272758);
			else {
				Class282_Sub37 class282_sub37 = new Class282_Sub37(i, new Class278_Sub1(4096, (Class209.aClass317_2663), i), i_1_, bool);
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4915(Class223.aClass495_2772.method8276(1771428192), 1317318998);
				aClass465_5774.method7765(class282_sub37, (long) i);
			}
		}
	}

	public static void method8250(int i, int i_2_, boolean bool) {
		if (aClass465_5774.method7754((long) i) == null) {
			if (!client.aBool7393)
				Class62.method1260(i, bool, -1400708496);
			else {
				Class282_Sub37 class282_sub37 = new Class282_Sub37(i, new Class278_Sub1(4096, (Class209.aClass317_2663), i), i_2_, bool);
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4915(Class223.aClass495_2772.method8276(1799702773), 1441339523);
				aClass465_5774.method7765(class282_sub37, (long) i);
			}
		}
	}

	public static void method8251(int i) {
		Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7754((long) i);
		if (class282_sub37 != null) {
			((Class282_Sub37) class282_sub37).aBool7995 = !((Class282_Sub37) class282_sub37).aBool7995;
			((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4924(((Class282_Sub37) class282_sub37).aBool7995, -1401371611);
		}
	}

	public static void method8252(int i) {
		Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7754((long) i);
		if (null != class282_sub37) {
			((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4947((byte) 121);
			Class62.method1260((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), ((Class282_Sub37) class282_sub37).aBool7998, -402888978);
			class282_sub37.method4991(-371378792);
		}
	}

	public static void method8253(int i, int i_3_, boolean bool) {
		if (aClass465_5774.method7754((long) i) == null) {
			if (!client.aBool7393)
				Class62.method1260(i, bool, 450332339);
			else {
				Class282_Sub37 class282_sub37 = new Class282_Sub37(i, new Class278_Sub1(4096, (Class209.aClass317_2663), i), i_3_, bool);
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4915(Class223.aClass495_2772.method8276(1583994375), 1371307436);
				aClass465_5774.method7765(class282_sub37, (long) i);
			}
		}
	}

	public static String method8254(int i) {
		Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7754((long) i);
		if (class282_sub37 != null) {
			Class282_Sub41_Sub2 class282_sub41_sub2 = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4935(-1982457178);
			if (class282_sub41_sub2 != null) {
				double d = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4923(-230388295);
				if ((double) class282_sub41_sub2.method14702(1630390140) <= d && ((double) class282_sub41_sub2.method14705((byte) 122) >= d))
					return class282_sub41_sub2.method14704(-1293719528);
			}
		}
		return null;
	}

	public static void method8255() {
		for (Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7750(1190415393); class282_sub37 != null; class282_sub37 = (Class282_Sub37) aClass465_5774.method7751((byte) 74)) {
			if (!((Class282_Sub37) class282_sub37).aBool7996)
				Class92.method1563(1729403683 * ((Class282_Sub37) class282_sub37).anInt7999, 1917715893);
			else
				((Class282_Sub37) class282_sub37).aBool7996 = false;
		}
	}

	public static void method8256() {
		for (Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7750(-462944139); class282_sub37 != null; class282_sub37 = (Class282_Sub37) aClass465_5774.method7751((byte) 23)) {
			if (((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4921((byte) -80))
				Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), 1917715893);
			else {
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method13447(-2111012999);
				try {
					((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4932(-650334703);
				} catch (Exception exception) {
					Class151.method2594(new StringBuilder().append("").append((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683).toString(), exception, (byte) -80);
					Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683, 1917715893);
				}
				if (!((Class282_Sub37) class282_sub37).aBool8000 && !((Class282_Sub37) class282_sub37).aBool7995) {
					Class282_Sub41_Sub4 class282_sub41_sub4 = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4914(-1647104916);
					if (class282_sub41_sub4 != null) {
						Class282_Sub15_Sub1 class282_sub15_sub1 = class282_sub41_sub4.method14845(1064626633);
						if (class282_sub15_sub1 != null) {
							class282_sub15_sub1.method14820(961890651 * (((Class282_Sub37) class282_sub37).anInt7997), -1172755495);
							Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub1);
							((Class282_Sub37) class282_sub37).aBool8000 = true;
						}
					}
				}
			}
		}
	}

	public static void method8257() {
		for (Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7750(1402049792); class282_sub37 != null; class282_sub37 = (Class282_Sub37) aClass465_5774.method7751((byte) 16)) {
			if (!((Class282_Sub37) class282_sub37).aBool7996)
				Class92.method1563(1729403683 * ((Class282_Sub37) class282_sub37).anInt7999, 1917715893);
			else
				((Class282_Sub37) class282_sub37).aBool7996 = false;
		}
	}

	public static void method8258() {
		for (Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7750(610554889); class282_sub37 != null; class282_sub37 = (Class282_Sub37) aClass465_5774.method7751((byte) 46)) {
			if (((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4921((byte) -73))
				Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), 1917715893);
			else {
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method13447(-1900656073);
				try {
					((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4932(-595351251);
				} catch (Exception exception) {
					Class151.method2594(new StringBuilder().append("").append((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683).toString(), exception, (byte) -2);
					Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683, 1917715893);
				}
				if (!((Class282_Sub37) class282_sub37).aBool8000 && !((Class282_Sub37) class282_sub37).aBool7995) {
					Class282_Sub41_Sub4 class282_sub41_sub4 = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4914(-1861342608);
					if (class282_sub41_sub4 != null) {
						Class282_Sub15_Sub1 class282_sub15_sub1 = class282_sub41_sub4.method14845(327258904);
						if (class282_sub15_sub1 != null) {
							class282_sub15_sub1.method14820(961890651 * (((Class282_Sub37) class282_sub37).anInt7997), -2147336308);
							Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub1);
							((Class282_Sub37) class282_sub37).aBool8000 = true;
						}
					}
				}
			}
		}
	}

	public static String method8259(int i) {
		Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7754((long) i);
		if (class282_sub37 != null) {
			Class282_Sub41_Sub2 class282_sub41_sub2 = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4935(-2042931243);
			if (class282_sub41_sub2 != null) {
				double d = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4923(1650965135);
				if ((double) class282_sub41_sub2.method14702(1537059033) <= d && ((double) class282_sub41_sub2.method14705((byte) 112) >= d))
					return class282_sub41_sub2.method14704(-1160864690);
			}
		}
		return null;
	}

	public static void method8260() {
		for (Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7750(1029246865); class282_sub37 != null; class282_sub37 = (Class282_Sub37) aClass465_5774.method7751((byte) 32)) {
			if (((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4921((byte) -88))
				Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), 1917715893);
			else {
				((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method13447(451182661);
				try {
					((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4932(-1155140665);
				} catch (Exception exception) {
					Class151.method2594(new StringBuilder().append("").append((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683).toString(), exception, (byte) -5);
					Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999) * 1729403683, 1917715893);
				}
				if (!((Class282_Sub37) class282_sub37).aBool8000 && !((Class282_Sub37) class282_sub37).aBool7995) {
					Class282_Sub41_Sub4 class282_sub41_sub4 = ((Class282_Sub37) class282_sub37).aClass278_Sub1_8001.method4914(-1328114873);
					if (class282_sub41_sub4 != null) {
						Class282_Sub15_Sub1 class282_sub15_sub1 = class282_sub41_sub4.method14845(1105386485);
						if (class282_sub15_sub1 != null) {
							class282_sub15_sub1.method14820(961890651 * (((Class282_Sub37) class282_sub37).anInt7997), 619564202);
							Class79.aClass282_Sub15_Sub4_783.method15275(class282_sub15_sub1);
							((Class282_Sub37) class282_sub37).aBool8000 = true;
						}
					}
				}
			}
		}
	}

	public static void method8261() {
		for (Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7750(1865869152); null != class282_sub37; class282_sub37 = (Class282_Sub37) aClass465_5774.method7751((byte) 99))
			Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), 1917715893);
	}

	public static void method8262() {
		for (Class282_Sub37 class282_sub37 = (Class282_Sub37) aClass465_5774.method7750(-1138316643); null != class282_sub37; class282_sub37 = (Class282_Sub37) aClass465_5774.method7751((byte) 19))
			Class92.method1563((((Class282_Sub37) class282_sub37).anInt7999 * 1729403683), 1917715893);
	}

	static final void method8263(Class527 class527, int i) {
		if (Class475.aBool5623 && null != Class263.aFrame3260)
			Class440.method7373(Class393.aClass282_Sub54_4783.aClass468_Sub9_8226.method12687(577867050), -1, -1, false, (byte) 35);
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		boolean bool = ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1);
		String string_4_ = new StringBuilder().append(Class220.method3741((byte) -1)).append(string).toString();
		Class282_Sub50_Sub6.method14788(string_4_, bool, Class393.aClass282_Sub54_4783.aClass468_Sub18_8230.method12776(-1739196165) == 5, client.aBool7158, client.aBool7159, (byte) -52);
	}

	static final boolean method8264(Class474 class474, int i, int i_5_, int i_6_) {
		Class478 class478 = class474.method7891(i, 65280);
		if (i_5_ == 11)
			i_5_ = 10;
		if (i_5_ >= 5 && i_5_ <= 8)
			i_5_ = 4;
		return class478.method7987(i_5_, 312725191);
	}

	public static final void method8265(int i, int i_7_, int i_8_, int i_9_, boolean bool, byte i_10_) {
		if (i_8_ < 1)
			i_8_ = 1;
		if (i_9_ < 1)
			i_9_ = 1;
		int i_11_ = i_9_ - 334;
		if (i_11_ < 0)
			i_11_ = 0;
		else if (i_11_ > 100)
			i_11_ = 100;
		int i_12_ = ((client.aShort7437 - client.aShort7436) * i_11_ / 100 + client.aShort7436);
		int i_13_ = i_12_ * i_9_ * 512 / (i_8_ * 334);
		if (i_13_ < client.aShort7214) {
			i_13_ = client.aShort7214;
			i_12_ = i_13_ * i_8_ * 334 / (512 * i_9_);
			if (i_12_ > client.aShort7441) {
				i_12_ = client.aShort7441;
				int i_14_ = 512 * (i_12_ * i_9_) / (i_13_ * 334);
				int i_15_ = (i_8_ - i_14_) / 2;
				if (bool) {
					Class316.aClass505_3680.L();
					Class316.aClass505_3680.method8425(i, i_7_, i_15_, i_9_, -16777216, (byte) -9);
					Class316.aClass505_3680.method8425(i_8_ + i - i_15_, i_7_, i_15_, i_9_, -16777216, (byte) -9);
				}
				i += i_15_;
				i_8_ -= i_15_ * 2;
			}
		} else if (i_13_ > client.aShort7474) {
			i_13_ = client.aShort7474;
			i_12_ = i_13_ * i_8_ * 334 / (512 * i_9_);
			if (i_12_ < client.aShort7276) {
				i_12_ = client.aShort7276;
				int i_16_ = i_13_ * i_8_ * 334 / (i_12_ * 512);
				int i_17_ = (i_9_ - i_16_) / 2;
				if (bool) {
					Class316.aClass505_3680.L();
					Class316.aClass505_3680.method8425(i, i_7_, i_8_, i_17_, -16777216, (byte) -109);
					Class316.aClass505_3680.method8425(i, i_9_ + i_7_ - i_17_, i_8_, i_17_, -16777216, (byte) -47);
				}
				i_7_ += i_17_;
				i_9_ -= 2 * i_17_;
			}
		}
		client.anInt7451 = i_12_ * i_9_ / 334 * 1945336671;
		client.anInt7444 = i * 635578953;
		client.anInt7445 = i_7_ * 1957537953;
		client.anInt7188 = -1685337199 * (short) i_8_;
		client.anInt7440 = 1946323855 * (short) i_9_;
	}

	public static int[] method8266(int i, int i_18_, int i_19_, int i_20_, int i_21_, float f, boolean bool, byte i_22_) {
		int[] is = new int[i];
		Class282_Sub20_Sub4 class282_sub20_sub4 = new Class282_Sub20_Sub4();
		((Class282_Sub20_Sub4) class282_sub20_sub4).anInt9733 = i_18_ * -1343016681;
		((Class282_Sub20_Sub4) class282_sub20_sub4).aBool9726 = bool;
		((Class282_Sub20_Sub4) class282_sub20_sub4).anInt9725 = i_19_ * 542765127;
		((Class282_Sub20_Sub4) class282_sub20_sub4).anInt9735 = i_20_ * 554163529;
		((Class282_Sub20_Sub4) class282_sub20_sub4).anInt9736 = 1844348207 * i_21_;
		((Class282_Sub20_Sub4) class282_sub20_sub4).anInt9737 = -1659901129 * (int) (4096.0F * f);
		class282_sub20_sub4.method12321(1061037797);
		Class316.method5593(i, 1, -383407250);
		class282_sub20_sub4.method15197(0, is, 915556346);
		return is;
	}
}
