/* Class103_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class103_Sub2 extends Class103 {
	public void method1792() {
		aClass505_Sub2_1057.method13971(anInt1056);
		aClass505_Sub2_1057.method13967(1, Class67.aClass67_677);
		aClass505_Sub2_1057.method13969(1, Class67.aClass67_677);
		method14513();
		aClass505_Sub2_1057.method14150();
	}

	public void method1788(Class384 class384) {
		aClass505_Sub2_1057.method13892(class384, aClass505_Sub2_1057.aClass384_8740, aClass505_Sub2_1057.aClass384_8727);
	}

	void method14511() {
		aClass505_Sub2_1057.method14163(0);
		aClass505_Sub2_1057.method14035(anInterface6_1059);
		aClass505_Sub2_1057.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_1057.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_1057.method13966(Class68.aClass68_683, Class68.aClass68_683);
		aClass505_Sub2_1057.method13973().method6562(aClass384_1058);
		aClass505_Sub2_1057.method13974(Class37.aClass37_388);
	}

	public Class103_Sub2(Class505_Sub2 class505_sub2) {
		super(class505_sub2);
	}

	public void method1790() {
		aClass505_Sub2_1057.method13971(anInt1056);
		aClass505_Sub2_1057.method13967(1, Class67.aClass67_677);
		aClass505_Sub2_1057.method13969(1, Class67.aClass67_677);
		method14513();
		aClass505_Sub2_1057.method14150();
	}

	public void method1789(Class384 class384) {
		aClass505_Sub2_1057.method13892(class384, aClass505_Sub2_1057.aClass384_8740, aClass505_Sub2_1057.aClass384_8727);
	}

	public void method1786(Class384 class384) {
		aClass505_Sub2_1057.method13892(class384, aClass505_Sub2_1057.aClass384_8740, aClass505_Sub2_1057.aClass384_8727);
	}

	public void method1785(int i) {
		aClass505_Sub2_1057.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_1057.method13969(1, Class67.aClass67_678);
		method14513();
		aClass505_Sub2_1057.method14002(Class352.aClass352_4098, 0, i * 4, 0, i * 2);
	}

	public void method1791() {
		aClass505_Sub2_1057.method13971(anInt1056);
		aClass505_Sub2_1057.method13967(1, Class67.aClass67_677);
		aClass505_Sub2_1057.method13969(1, Class67.aClass67_677);
		method14513();
		aClass505_Sub2_1057.method14150();
	}

	public void method1787(int i) {
		aClass505_Sub2_1057.method13967(1, Class67.aClass67_678);
		aClass505_Sub2_1057.method13969(1, Class67.aClass67_678);
		method14513();
		aClass505_Sub2_1057.method14002(Class352.aClass352_4098, 0, i * 4, 0, i * 2);
	}

	void method14512() {
		aClass505_Sub2_1057.method14163(0);
		aClass505_Sub2_1057.method14035(anInterface6_1059);
		aClass505_Sub2_1057.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_1057.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_1057.method13966(Class68.aClass68_683, Class68.aClass68_683);
		aClass505_Sub2_1057.method13973().method6562(aClass384_1058);
		aClass505_Sub2_1057.method13974(Class37.aClass37_388);
	}

	void method14513() {
		aClass505_Sub2_1057.method14163(0);
		aClass505_Sub2_1057.method14035(anInterface6_1059);
		aClass505_Sub2_1057.method13967(0, Class67.aClass67_680);
		aClass505_Sub2_1057.method13969(0, Class67.aClass67_680);
		aClass505_Sub2_1057.method13966(Class68.aClass68_683, Class68.aClass68_683);
		aClass505_Sub2_1057.method13973().method6562(aClass384_1058);
		aClass505_Sub2_1057.method13974(Class37.aClass37_388);
	}
}
