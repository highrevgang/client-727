/* Class282_Sub31 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class282_Sub31 extends Class282 {
	public int anInt7762;
	public int anInt7763;
	int anInt7764;
	int anInt7765;
	int anInt7766;
	int anInt7767;
	Class476 aClass476_7768;
	int anInt7769;
	int anInt7770;
	int anInt7771;
	int anInt7772;
	boolean aBool7773 = true;
	boolean aBool7774 = false;
	public static Class482 aClass482_7775 = new Class482();
	public static Class482 aClass482_7776 = new Class482();
	static long aLong7777 = 8357660357265826175L;

	public static final void method12520() {
		for (Class282_Sub31 class282_sub31 = (Class282_Sub31) aClass482_7775.method8097((byte) 12); null != class282_sub31; class282_sub31 = (Class282_Sub31) aClass482_7775.method8067(-810304149)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774) {
				((Class282_Sub31) class282_sub31).aBool7773 = true;
				if (37618455 * class282_sub31.anInt7762 >= 0 && -322610393 * class282_sub31.anInt7763 >= 0 && (class282_sub31.anInt7762 * 37618455 < client.aClass257_7353.method4424(1005042570)) && (class282_sub31.anInt7763 * -322610393 < client.aClass257_7353.method4451(-137780717)))
					Class275_Sub4.method12585(class282_sub31, -1043984054);
			} else
				class282_sub31.method4991(-371378792);
		}
		for (Class282_Sub31 class282_sub31 = (Class282_Sub31) aClass482_7776.method8097((byte) 31); null != class282_sub31; class282_sub31 = (Class282_Sub31) aClass482_7776.method8067(-685003687)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774)
				((Class282_Sub31) class282_sub31).aBool7773 = true;
			else
				class282_sub31.method4991(-371378792);
		}
	}

	Class282_Sub31() {
		/* empty */
	}

	public static void method12521(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		Class282_Sub31 class282_sub31 = null;
		for (Class282_Sub31 class282_sub31_6_ = (Class282_Sub31) aClass482_7775.method8097((byte) 79); null != class282_sub31_6_; class282_sub31_6_ = (Class282_Sub31) aClass482_7775.method8067(440856881)) {
			if ((1291499461 * ((Class282_Sub31) class282_sub31_6_).anInt7764 == i) && class282_sub31_6_.anInt7762 * 37618455 == i_0_ && i_1_ == -322610393 * class282_sub31_6_.anInt7763 && i_2_ == -497894501 * (((Class282_Sub31) class282_sub31_6_).anInt7766)) {
				class282_sub31 = class282_sub31_6_;
				break;
			}
		}
		if (null == class282_sub31) {
			class282_sub31 = new Class282_Sub31();
			((Class282_Sub31) class282_sub31).anInt7764 = -362696947 * i;
			((Class282_Sub31) class282_sub31).anInt7766 = 1143878291 * i_2_;
			class282_sub31.anInt7762 = 1690395815 * i_0_;
			class282_sub31.anInt7763 = i_1_ * 765748375;
			if (i_0_ >= 0 && i_1_ >= 0 && i_0_ < client.aClass257_7353.method4424(1828258754) && i_1_ < client.aClass257_7353.method4451(-1867433891))
				Class275_Sub4.method12585(class282_sub31, -1190113326);
			aClass482_7775.method8059(class282_sub31, 1310466844);
		}
		((Class282_Sub31) class282_sub31).anInt7769 = -567871853 * i_3_;
		((Class282_Sub31) class282_sub31).anInt7771 = i_4_ * -763092445;
		((Class282_Sub31) class282_sub31).anInt7772 = i_5_ * 111963359;
		((Class282_Sub31) class282_sub31).aBool7773 = true;
		((Class282_Sub31) class282_sub31).aBool7774 = false;
	}

	public static void method12522(int i, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_) {
		Class282_Sub31 class282_sub31 = null;
		for (Class282_Sub31 class282_sub31_13_ = (Class282_Sub31) aClass482_7775.method8097((byte) 123); null != class282_sub31_13_; class282_sub31_13_ = (Class282_Sub31) aClass482_7775.method8067(672126888)) {
			if ((1291499461 * ((Class282_Sub31) class282_sub31_13_).anInt7764 == i) && class282_sub31_13_.anInt7762 * 37618455 == i_7_ && i_8_ == -322610393 * class282_sub31_13_.anInt7763 && i_9_ == -497894501 * (((Class282_Sub31) class282_sub31_13_).anInt7766)) {
				class282_sub31 = class282_sub31_13_;
				break;
			}
		}
		if (null == class282_sub31) {
			class282_sub31 = new Class282_Sub31();
			((Class282_Sub31) class282_sub31).anInt7764 = -362696947 * i;
			((Class282_Sub31) class282_sub31).anInt7766 = 1143878291 * i_9_;
			class282_sub31.anInt7762 = 1690395815 * i_7_;
			class282_sub31.anInt7763 = i_8_ * 765748375;
			if (i_7_ >= 0 && i_8_ >= 0 && i_7_ < client.aClass257_7353.method4424(-1446225799) && i_8_ < client.aClass257_7353.method4451(-1456094608))
				Class275_Sub4.method12585(class282_sub31, -1061670092);
			aClass482_7775.method8059(class282_sub31, -339641785);
		}
		((Class282_Sub31) class282_sub31).anInt7769 = -567871853 * i_10_;
		((Class282_Sub31) class282_sub31).anInt7771 = i_11_ * -763092445;
		((Class282_Sub31) class282_sub31).anInt7772 = i_12_ * 111963359;
		((Class282_Sub31) class282_sub31).aBool7773 = true;
		((Class282_Sub31) class282_sub31).aBool7774 = false;
	}

	public static void method12523(int i, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_) {
		Class282_Sub31 class282_sub31 = null;
		for (Class282_Sub31 class282_sub31_20_ = (Class282_Sub31) aClass482_7775.method8097((byte) 14); null != class282_sub31_20_; class282_sub31_20_ = (Class282_Sub31) aClass482_7775.method8067(1087518182)) {
			if ((1291499461 * ((Class282_Sub31) class282_sub31_20_).anInt7764 == i) && class282_sub31_20_.anInt7762 * 37618455 == i_14_ && i_15_ == -322610393 * class282_sub31_20_.anInt7763 && i_16_ == -497894501 * (((Class282_Sub31) class282_sub31_20_).anInt7766)) {
				class282_sub31 = class282_sub31_20_;
				break;
			}
		}
		if (null == class282_sub31) {
			class282_sub31 = new Class282_Sub31();
			((Class282_Sub31) class282_sub31).anInt7764 = -362696947 * i;
			((Class282_Sub31) class282_sub31).anInt7766 = 1143878291 * i_16_;
			class282_sub31.anInt7762 = 1690395815 * i_14_;
			class282_sub31.anInt7763 = i_15_ * 765748375;
			if (i_14_ >= 0 && i_15_ >= 0 && i_14_ < client.aClass257_7353.method4424(-114175837) && i_15_ < client.aClass257_7353.method4451(-649751223))
				Class275_Sub4.method12585(class282_sub31, -903230745);
			aClass482_7775.method8059(class282_sub31, 887991261);
		}
		((Class282_Sub31) class282_sub31).anInt7769 = -567871853 * i_17_;
		((Class282_Sub31) class282_sub31).anInt7771 = i_18_ * -763092445;
		((Class282_Sub31) class282_sub31).anInt7772 = i_19_ * 111963359;
		((Class282_Sub31) class282_sub31).aBool7773 = true;
		((Class282_Sub31) class282_sub31).aBool7774 = false;
	}

	static void method12524(int i, int i_21_, int i_22_, int i_23_, int i_24_, int i_25_, Class476 class476) {
		Class282_Sub31 class282_sub31 = null;
		for (Class282_Sub31 class282_sub31_26_ = (Class282_Sub31) aClass482_7776.method8097((byte) 121); null != class282_sub31_26_; class282_sub31_26_ = (Class282_Sub31) aClass482_7776.method8067(1699525032)) {
			if (i == (((Class282_Sub31) class282_sub31_26_).anInt7764 * 1291499461) && i_21_ == 37618455 * class282_sub31_26_.anInt7762 && i_22_ == -322610393 * class282_sub31_26_.anInt7763 && (((Class282_Sub31) class282_sub31_26_).anInt7766 * -497894501) == i_23_) {
				class282_sub31 = class282_sub31_26_;
				break;
			}
		}
		if (class282_sub31 == null) {
			class282_sub31 = new Class282_Sub31();
			((Class282_Sub31) class282_sub31).anInt7764 = -362696947 * i;
			((Class282_Sub31) class282_sub31).anInt7766 = i_23_ * 1143878291;
			class282_sub31.anInt7762 = i_21_ * 1690395815;
			class282_sub31.anInt7763 = 765748375 * i_22_;
			aClass482_7776.method8059(class282_sub31, -1245675620);
		}
		((Class282_Sub31) class282_sub31).anInt7769 = i_24_ * -567871853;
		((Class282_Sub31) class282_sub31).anInt7771 = -763092445 * i_25_;
		((Class282_Sub31) class282_sub31).aClass476_7768 = class476;
		((Class282_Sub31) class282_sub31).aBool7773 = true;
		((Class282_Sub31) class282_sub31).aBool7774 = false;
	}

	static void method12525(int i, int i_27_, int i_28_, int i_29_, int i_30_, int i_31_, Class476 class476) {
		Class282_Sub31 class282_sub31 = null;
		for (Class282_Sub31 class282_sub31_32_ = (Class282_Sub31) aClass482_7776.method8097((byte) 34); null != class282_sub31_32_; class282_sub31_32_ = (Class282_Sub31) aClass482_7776.method8067(704377211)) {
			if (i == (((Class282_Sub31) class282_sub31_32_).anInt7764 * 1291499461) && i_27_ == 37618455 * class282_sub31_32_.anInt7762 && i_28_ == -322610393 * class282_sub31_32_.anInt7763 && (((Class282_Sub31) class282_sub31_32_).anInt7766 * -497894501) == i_29_) {
				class282_sub31 = class282_sub31_32_;
				break;
			}
		}
		if (class282_sub31 == null) {
			class282_sub31 = new Class282_Sub31();
			((Class282_Sub31) class282_sub31).anInt7764 = -362696947 * i;
			((Class282_Sub31) class282_sub31).anInt7766 = i_29_ * 1143878291;
			class282_sub31.anInt7762 = i_27_ * 1690395815;
			class282_sub31.anInt7763 = 765748375 * i_28_;
			aClass482_7776.method8059(class282_sub31, 1300759998);
		}
		((Class282_Sub31) class282_sub31).anInt7769 = i_30_ * -567871853;
		((Class282_Sub31) class282_sub31).anInt7771 = -763092445 * i_31_;
		((Class282_Sub31) class282_sub31).aClass476_7768 = class476;
		((Class282_Sub31) class282_sub31).aBool7773 = true;
		((Class282_Sub31) class282_sub31).aBool7774 = false;
	}

	public static final void method12526() {
		for (Class282_Sub31 class282_sub31 = (Class282_Sub31) aClass482_7775.method8097((byte) 15); null != class282_sub31; class282_sub31 = (Class282_Sub31) aClass482_7775.method8067(-355015383)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774) {
				((Class282_Sub31) class282_sub31).aBool7773 = true;
				if (37618455 * class282_sub31.anInt7762 >= 0 && -322610393 * class282_sub31.anInt7763 >= 0 && (class282_sub31.anInt7762 * 37618455 < client.aClass257_7353.method4424(-198966758)) && (class282_sub31.anInt7763 * -322610393 < client.aClass257_7353.method4451(-1980996368)))
					Class275_Sub4.method12585(class282_sub31, -693686036);
			} else
				class282_sub31.method4991(-371378792);
		}
		for (Class282_Sub31 class282_sub31 = (Class282_Sub31) aClass482_7776.method8097((byte) 20); null != class282_sub31; class282_sub31 = (Class282_Sub31) aClass482_7776.method8067(-1011887008)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774)
				((Class282_Sub31) class282_sub31).aBool7773 = true;
			else
				class282_sub31.method4991(-371378792);
		}
	}

	public static final void method12527() {
		for (Class282_Sub31 class282_sub31 = (Class282_Sub31) aClass482_7775.method8097((byte) 68); null != class282_sub31; class282_sub31 = (Class282_Sub31) aClass482_7775.method8067(-777265098)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774) {
				((Class282_Sub31) class282_sub31).aBool7773 = true;
				if (37618455 * class282_sub31.anInt7762 >= 0 && -322610393 * class282_sub31.anInt7763 >= 0 && (class282_sub31.anInt7762 * 37618455 < client.aClass257_7353.method4424(470361949)) && (class282_sub31.anInt7763 * -322610393 < client.aClass257_7353.method4451(-1450957350)))
					Class275_Sub4.method12585(class282_sub31, -613066628);
			} else
				class282_sub31.method4991(-371378792);
		}
		for (Class282_Sub31 class282_sub31 = (Class282_Sub31) aClass482_7776.method8097((byte) 60); null != class282_sub31; class282_sub31 = (Class282_Sub31) aClass482_7776.method8067(-128254190)) {
			if (!((Class282_Sub31) class282_sub31).aBool7774)
				((Class282_Sub31) class282_sub31).aBool7773 = true;
			else
				class282_sub31.method4991(-371378792);
		}
	}

	static final void method12528(Class282_Sub31 class282_sub31) {
		Class206 class206 = client.aClass257_7353.method4430(-1107390720);
		if (class206 != null) {
			Interface12 interface12 = null;
			if (0 == ((Class282_Sub31) class282_sub31).anInt7766 * -497894501)
				interface12 = ((Interface12) class206.method3381(1291499461 * (((Class282_Sub31) class282_sub31).anInt7764), 37618455 * class282_sub31.anInt7762, (-322610393 * class282_sub31.anInt7763), (byte) -118));
			if (-497894501 * ((Class282_Sub31) class282_sub31).anInt7766 == 1)
				interface12 = ((Interface12) class206.method3511(1291499461 * (((Class282_Sub31) class282_sub31).anInt7764), 37618455 * class282_sub31.anInt7762, (class282_sub31.anInt7763 * -322610393), (byte) -40));
			if (2 == ((Class282_Sub31) class282_sub31).anInt7766 * -497894501)
				interface12 = ((Interface12) class206.method3413(1291499461 * (((Class282_Sub31) class282_sub31).anInt7764), 37618455 * class282_sub31.anInt7762, (-322610393 * class282_sub31.anInt7763), client.anInterface25_7446, -233664382));
			if (-497894501 * ((Class282_Sub31) class282_sub31).anInt7766 == 3)
				interface12 = ((Interface12) class206.method3415((((Class282_Sub31) class282_sub31).anInt7764) * 1291499461, class282_sub31.anInt7762 * 37618455, (class282_sub31.anInt7763 * -322610393), -387297653));
			if (null != interface12) {
				((Class282_Sub31) class282_sub31).anInt7765 = interface12.method84(-1963234883) * -233992759;
				((Class282_Sub31) class282_sub31).anInt7770 = interface12.method89(1540297611) * 1474006371;
				((Class282_Sub31) class282_sub31).anInt7767 = interface12.method92(-1237984275) * 566178073;
			} else {
				((Class282_Sub31) class282_sub31).anInt7765 = 233992759;
				((Class282_Sub31) class282_sub31).anInt7770 = 0;
				((Class282_Sub31) class282_sub31).anInt7767 = 0;
			}
		}
	}

	static final void method12529(Class282_Sub31 class282_sub31, boolean bool) {
		if (((Class282_Sub31) class282_sub31).aBool7774) {
			if (-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765 < 0 || (Class492.method8264(client.aClass257_7353.method4436(-1476027262), ((Class282_Sub31) class282_sub31).anInt7765 * -1369039751, -1307943861 * ((Class282_Sub31) class282_sub31).anInt7770, 1942118537))) {
				if (!bool)
					Class174.method2956((((Class282_Sub31) class282_sub31).anInt7764 * 1291499461), (((Class282_Sub31) class282_sub31).anInt7766 * -497894501), 37618455 * class282_sub31.anInt7762, class282_sub31.anInt7763 * -322610393, (((Class282_Sub31) class282_sub31).anInt7765 * -1369039751), (1421843241 * ((Class282_Sub31) class282_sub31).anInt7767), (-1307943861 * ((Class282_Sub31) class282_sub31).anInt7770), -1, -1249577252);
				else
					Class243.method4179((1291499461 * (((Class282_Sub31) class282_sub31).anInt7764)), (((Class282_Sub31) class282_sub31).anInt7766) * -497894501, class282_sub31.anInt7762 * 37618455, class282_sub31.anInt7763 * -322610393, null, (byte) 100);
				class282_sub31.method4991(-371378792);
			}
		} else if (((Class282_Sub31) class282_sub31).aBool7773 && 37618455 * class282_sub31.anInt7762 >= 1 && class282_sub31.anInt7763 * -322610393 >= 1 && (class282_sub31.anInt7762 * 37618455 <= client.aClass257_7353.method4424(314344797) - 2) && (-322610393 * class282_sub31.anInt7763 <= client.aClass257_7353.method4451(-941267185) - 2) && ((-1809279077 * ((Class282_Sub31) class282_sub31).anInt7769) < 0 || (Class492.method8264(client.aClass257_7353.method4436(-1593117564), (-1809279077 * ((Class282_Sub31) class282_sub31).anInt7769), (((Class282_Sub31) class282_sub31).anInt7771 * 965123467), 1942118537)))) {
			if (!bool)
				Class174.method2956(1291499461 * ((Class282_Sub31) class282_sub31).anInt7764, ((Class282_Sub31) class282_sub31).anInt7766 * -497894501, 37618455 * class282_sub31.anInt7762, class282_sub31.anInt7763 * -322610393, -1809279077 * ((Class282_Sub31) class282_sub31).anInt7769, ((Class282_Sub31) class282_sub31).anInt7772 * -818262241, 965123467 * ((Class282_Sub31) class282_sub31).anInt7771, -1, -2122588193);
			else
				Class243.method4179(1291499461 * ((Class282_Sub31) class282_sub31).anInt7764, ((Class282_Sub31) class282_sub31).anInt7766 * -497894501, class282_sub31.anInt7762 * 37618455, -322610393 * class282_sub31.anInt7763, ((Class282_Sub31) class282_sub31).aClass476_7768, (byte) 103);
			((Class282_Sub31) class282_sub31).aBool7773 = false;
			if (!bool && (-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765 == (((Class282_Sub31) class282_sub31).anInt7769 * -1809279077)) && (-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765 == -1))
				class282_sub31.method4991(-371378792);
			else if (!bool && ((-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765) == (((Class282_Sub31) class282_sub31).anInt7769 * -1809279077)) && ((1421843241 * ((Class282_Sub31) class282_sub31).anInt7767) == (-818262241 * ((Class282_Sub31) class282_sub31).anInt7772)) && ((((Class282_Sub31) class282_sub31).anInt7771 * 965123467) == (((Class282_Sub31) class282_sub31).anInt7770 * -1307943861)))
				class282_sub31.method4991(-371378792);
		}
	}

	static final void method12530(Class282_Sub31 class282_sub31, boolean bool) {
		if (((Class282_Sub31) class282_sub31).aBool7774) {
			if (-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765 < 0 || (Class492.method8264(client.aClass257_7353.method4436(-2118413398), ((Class282_Sub31) class282_sub31).anInt7765 * -1369039751, -1307943861 * ((Class282_Sub31) class282_sub31).anInt7770, 1942118537))) {
				if (!bool)
					Class174.method2956((((Class282_Sub31) class282_sub31).anInt7764 * 1291499461), (((Class282_Sub31) class282_sub31).anInt7766 * -497894501), 37618455 * class282_sub31.anInt7762, class282_sub31.anInt7763 * -322610393, (((Class282_Sub31) class282_sub31).anInt7765 * -1369039751), (1421843241 * ((Class282_Sub31) class282_sub31).anInt7767), (-1307943861 * ((Class282_Sub31) class282_sub31).anInt7770), -1, -1753179848);
				else
					Class243.method4179((1291499461 * (((Class282_Sub31) class282_sub31).anInt7764)), (((Class282_Sub31) class282_sub31).anInt7766) * -497894501, class282_sub31.anInt7762 * 37618455, class282_sub31.anInt7763 * -322610393, null, (byte) 94);
				class282_sub31.method4991(-371378792);
			}
		} else if (((Class282_Sub31) class282_sub31).aBool7773 && 37618455 * class282_sub31.anInt7762 >= 1 && class282_sub31.anInt7763 * -322610393 >= 1 && (class282_sub31.anInt7762 * 37618455 <= client.aClass257_7353.method4424(-814040013) - 2) && (-322610393 * class282_sub31.anInt7763 <= client.aClass257_7353.method4451(-1523005769) - 2) && ((-1809279077 * ((Class282_Sub31) class282_sub31).anInt7769) < 0 || (Class492.method8264(client.aClass257_7353.method4436(-1685506857), (-1809279077 * ((Class282_Sub31) class282_sub31).anInt7769), (((Class282_Sub31) class282_sub31).anInt7771 * 965123467), 1942118537)))) {
			if (!bool)
				Class174.method2956(1291499461 * ((Class282_Sub31) class282_sub31).anInt7764, ((Class282_Sub31) class282_sub31).anInt7766 * -497894501, 37618455 * class282_sub31.anInt7762, class282_sub31.anInt7763 * -322610393, -1809279077 * ((Class282_Sub31) class282_sub31).anInt7769, ((Class282_Sub31) class282_sub31).anInt7772 * -818262241, 965123467 * ((Class282_Sub31) class282_sub31).anInt7771, -1, -1481187144);
			else
				Class243.method4179(1291499461 * ((Class282_Sub31) class282_sub31).anInt7764, ((Class282_Sub31) class282_sub31).anInt7766 * -497894501, class282_sub31.anInt7762 * 37618455, -322610393 * class282_sub31.anInt7763, ((Class282_Sub31) class282_sub31).aClass476_7768, (byte) 62);
			((Class282_Sub31) class282_sub31).aBool7773 = false;
			if (!bool && (-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765 == (((Class282_Sub31) class282_sub31).anInt7769 * -1809279077)) && (-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765 == -1))
				class282_sub31.method4991(-371378792);
			else if (!bool && ((-1369039751 * ((Class282_Sub31) class282_sub31).anInt7765) == (((Class282_Sub31) class282_sub31).anInt7769 * -1809279077)) && ((1421843241 * ((Class282_Sub31) class282_sub31).anInt7767) == (-818262241 * ((Class282_Sub31) class282_sub31).anInt7772)) && ((((Class282_Sub31) class282_sub31).anInt7771 * 965123467) == (((Class282_Sub31) class282_sub31).anInt7770 * -1307943861)))
				class282_sub31.method4991(-371378792);
		}
	}

	static final void method12531(int i, int i_33_, int i_34_, int i_35_, int i_36_, int i_37_, int i_38_, int i_39_) {
		if (i_34_ >= 1 && i_35_ >= 1 && i_34_ <= client.aClass257_7353.method4424(102217935) - 2 && i_35_ <= client.aClass257_7353.method4451(-425131356) - 2) {
			int i_40_ = i;
			if (i_40_ < 3 && client.aClass257_7353.method4433(33386298).method5497(i_34_, i_35_, 1618412511))
				i_40_++;
			if (client.aClass257_7353.method4430(-1311100659) != null) {
				client.aClass257_7353.method4441(1508379413).method12475(Class316.aClass505_3680, i, i_33_, i_34_, i_35_, client.aClass257_7353.method4552(i, 1801793645), -1304522104);
				if (i_36_ >= 0) {
					int i_41_ = Class393.aClass282_Sub54_4783.aClass468_Sub23_8202.method12897((byte) 119);
					Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub23_8202, 1, -533519204);
					client.aClass257_7353.method4441(1508379413).method12459(Class316.aClass505_3680, i_40_, i, i_34_, i_35_, i_36_, i_37_, i_38_, client.aClass257_7353.method4552(i, 1801793645), i_39_, 432184213);
					Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub23_8202, i_41_, -930280166);
				}
			}
		}
	}

	static final void method12532(int i, int i_42_, int i_43_, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_) {
		if (i_43_ >= 1 && i_44_ >= 1 && i_43_ <= client.aClass257_7353.method4424(-178663700) - 2 && i_44_ <= client.aClass257_7353.method4451(-778221763) - 2) {
			int i_49_ = i;
			if (i_49_ < 3 && client.aClass257_7353.method4433(33386298).method5497(i_43_, i_44_, 1424305959))
				i_49_++;
			if (client.aClass257_7353.method4430(-891625715) != null) {
				client.aClass257_7353.method4441(1508379413).method12475(Class316.aClass505_3680, i, i_42_, i_43_, i_44_, client.aClass257_7353.method4552(i, 1801793645), -1755317099);
				if (i_45_ >= 0) {
					int i_50_ = Class393.aClass282_Sub54_4783.aClass468_Sub23_8202.method12897((byte) 110);
					Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub23_8202, 1, -1133166102);
					client.aClass257_7353.method4441(1508379413).method12459(Class316.aClass505_3680, i_49_, i, i_43_, i_44_, i_45_, i_46_, i_47_, client.aClass257_7353.method4552(i, 1801793645), i_48_, 1321227047);
					Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub23_8202, i_50_, -1262675421);
				}
			}
		}
	}

	static final void method12533(int i, int i_51_, int i_52_, int i_53_, Class476 class476) {
		if (i_52_ >= 1 && i_53_ >= 1 && i_52_ <= client.aClass257_7353.method4424(252007102) - 2 && i_53_ <= client.aClass257_7353.method4451(-1924152420) - 2) {
			if (client.aClass257_7353.method4430(-1548066871) != null) {
				Interface12 interface12 = client.aClass257_7353.method4441(1508379413).method12467(i, i_51_, i_52_, i_53_, 1729337904);
				if (interface12 != null) {
					if (interface12 instanceof Class521_Sub1_Sub1_Sub5)
						((Class521_Sub1_Sub1_Sub5) interface12).method16096(class476, -994686865);
					else if (interface12 instanceof Class521_Sub1_Sub3_Sub2)
						((Class521_Sub1_Sub3_Sub2) interface12).method16091(class476, -737324181);
					else if (interface12 instanceof Class521_Sub1_Sub5_Sub2)
						((Class521_Sub1_Sub5_Sub2) interface12).method16108(class476, -1961546751);
					else if (interface12 instanceof Class521_Sub1_Sub4_Sub2)
						((Class521_Sub1_Sub4_Sub2) interface12).method16082(class476, -449891505);
				}
			}
		}
	}

	static final void method12534(int i, int i_54_, int i_55_, int i_56_, Class476 class476) {
		if (i_55_ >= 1 && i_56_ >= 1 && i_55_ <= client.aClass257_7353.method4424(-430984955) - 2 && i_56_ <= client.aClass257_7353.method4451(-1449205692) - 2) {
			if (client.aClass257_7353.method4430(-823902762) != null) {
				Interface12 interface12 = client.aClass257_7353.method4441(1508379413).method12467(i, i_54_, i_55_, i_56_, 1638366681);
				if (interface12 != null) {
					if (interface12 instanceof Class521_Sub1_Sub1_Sub5)
						((Class521_Sub1_Sub1_Sub5) interface12).method16096(class476, -532936846);
					else if (interface12 instanceof Class521_Sub1_Sub3_Sub2)
						((Class521_Sub1_Sub3_Sub2) interface12).method16091(class476, -737324181);
					else if (interface12 instanceof Class521_Sub1_Sub5_Sub2)
						((Class521_Sub1_Sub5_Sub2) interface12).method16108(class476, -2032145490);
					else if (interface12 instanceof Class521_Sub1_Sub4_Sub2)
						((Class521_Sub1_Sub4_Sub2) interface12).method16082(class476, -449891505);
				}
			}
		}
	}

	static final void method12535(Class527 class527, int i) {
		((Class527) class527).anObjectArray7017[(((Class527) class527).anIntArray7018[301123709 * ((Class527) class527).anInt7020])] = (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
	}
}
