
/* Class153 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;

public final class Class153 {
	Class465 aClass465_1963;
	int anInt1964;
	static int anInt1965;

	public int method2606(int i) {
		Class489 class489 = Class243.aClass498_3001.method8322(i, (short) 3220);
		int i_0_ = class489.anInt5764 * 507063071;
		int i_1_ = (31 == -967775471 * class489.anInt5762 ? -1 : (1 << 1 + -967775471 * class489.anInt5762) - 1);
		return ((method2609(i_0_, (byte) 42) & i_1_) >>> -1410455259 * class489.anInt5763);
	}

	public void method2607(byte i) {
		if (null != ((Class153) this).aClass465_1963)
			((Class153) this).aClass465_1963.method7749(1241970145);
	}

	public int method2608(int i) {
		Class489 class489 = Class243.aClass498_3001.method8322(i, (short) 13759);
		int i_2_ = class489.anInt5764 * 507063071;
		int i_3_ = (31 == -967775471 * class489.anInt5762 ? -1 : (1 << 1 + -967775471 * class489.anInt5762) - 1);
		return ((method2609(i_2_, (byte) 50) & i_3_) >>> -1410455259 * class489.anInt5763);
	}

	public int method2609(int i, byte i_4_) {
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class153) this).aClass465_1963.method7754((long) i));
		if (class282_sub38 != null)
			return class282_sub38.anInt8002 * -570797415;
		Class449 class449 = Class45.aClass447_435.method7450(i, (byte) -118);
		if (class449.aChar5431 != 'i')
			return -1;
		return 0;
	}

	public int method2610(int i, short i_5_) {
		Class489 class489 = Class243.aClass498_3001.method8322(i, (short) 4986);
		int i_6_ = class489.anInt5764 * 507063071;
		int i_7_ = (31 == -967775471 * class489.anInt5762 ? -1 : (1 << 1 + -967775471 * class489.anInt5762) - 1);
		return ((method2609(i_6_, (byte) 117) & i_7_) >>> -1410455259 * class489.anInt5763);
	}

	Class153(int i) {
		((Class153) this).anInt1964 = i * -352612607;
	}

	public void method2611() {
		if (null != ((Class153) this).aClass465_1963)
			((Class153) this).aClass465_1963.method7749(-1017795827);
	}

	public void method2612(int i, int i_8_) {
		if (null == ((Class153) this).aClass465_1963)
			((Class153) this).aClass465_1963 = new Class465(((Class153) this).anInt1964 * 535130369);
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class153) this).aClass465_1963.method7754((long) i));
		if (null != class282_sub38)
			class282_sub38.anInt8002 = 1270866345 * i_8_;
		else {
			class282_sub38 = new Class282_Sub38(i_8_);
			((Class153) this).aClass465_1963.method7765(class282_sub38, (long) i);
		}
	}

	public int method2613(int i) {
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class153) this).aClass465_1963.method7754((long) i));
		if (class282_sub38 != null)
			return class282_sub38.anInt8002 * -570797415;
		Class449 class449 = Class45.aClass447_435.method7450(i, (byte) -28);
		if (class449.aChar5431 != 'i')
			return -1;
		return 0;
	}

	public void method2614(int i, int i_9_, short i_10_) {
		if (null == ((Class153) this).aClass465_1963) {
			if (i_10_ != 768)
				return;
			((Class153) this).aClass465_1963 = new Class465(((Class153) this).anInt1964 * 535130369);
		}
		Class282_Sub38 class282_sub38 = ((Class282_Sub38) ((Class153) this).aClass465_1963.method7754((long) i));
		if (null != class282_sub38) {
			if (i_10_ == 768)
				class282_sub38.anInt8002 = 1270866345 * i_9_;
		} else {
			class282_sub38 = new Class282_Sub38(i_9_);
			((Class153) this).aClass465_1963.method7765(class282_sub38, (long) i);
		}
	}

	public int method2615(int i) {
		Class489 class489 = Class243.aClass498_3001.method8322(i, (short) 10335);
		int i_11_ = class489.anInt5764 * 507063071;
		int i_12_ = (31 == -967775471 * class489.anInt5762 ? -1 : (1 << 1 + -967775471 * class489.anInt5762) - 1);
		return ((method2609(i_11_, (byte) 77) & i_12_) >>> -1410455259 * class489.anInt5763);
	}

	public void method2616() {
		if (null != ((Class153) this).aClass465_1963)
			((Class153) this).aClass465_1963.method7749(-1479031717);
	}

	static final int method2617(Class505 class505, Class531 class531, byte i) {
		if (-848345857 * class531.anInt7058 != -1)
			return -848345857 * class531.anInt7058;
		if (-1 != class531.anInt7066 * 1717409107) {
			Class169 class169 = class505.anInterface22_5834.method144(1717409107 * class531.anInt7066, -2032882535);
			if (!class169.aBool2056)
				return class169.aShort2073;
		}
		return 1419498143 * class531.anInt7054;
	}

	public static Class268 method2618(int i, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_) {
		if (Class393.aClass282_Sub54_4783.aClass468_Sub13_8225.method12714(1292855903) != 0 && 0 != i_13_ && -458827259 * Class260.anInt3219 < 50 && i != -1) {
			Class268 class268 = new Class268((byte) 1, i, i_13_, i_14_, i_15_, 0, i_16_, null);
			Class260.aClass268Array3232[(Class260.anInt3219 += -1221598515) * -458827259 - 1] = class268;
			return class268;
		}
		return null;
	}

	public static Class358[] method2619(int i) {
		return (new Class358[] { Class358.aClass358_4157, Class358.aClass358_4153, Class358.aClass358_4155, Class358.aClass358_4141, Class358.aClass358_4127, Class358.aClass358_4140, Class358.aClass358_4128, Class358.aClass358_4137, Class358.aClass358_4152, Class358.aClass358_4151, Class358.aClass358_4148, Class358.aClass358_4133, Class358.aClass358_4126, Class358.aClass358_4156, Class358.aClass358_4131, Class358.aClass358_4135, Class358.aClass358_4136, Class358.aClass358_4129, Class358.aClass358_4147, Class358.aClass358_4146, Class358.aClass358_4138, Class358.aClass358_4139, Class358.aClass358_4158, Class358.aClass358_4149, Class358.aClass358_4145, Class358.aClass358_4132, Class358.aClass358_4134, Class358.aClass358_4144, Class358.aClass358_4142, Class358.aClass358_4154, Class358.aClass358_4130, Class358.aClass358_4125, Class358.aClass358_4143, Class358.aClass358_4150 });
	}

	static final void method2620(Class527 class527, int i) {
		int i_18_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class513 class513;
		if (((Class527) class527).aBool7022)
			class513 = ((Class527) class527).aClass513_6994;
		else
			class513 = ((Class527) class527).aClass513_7007;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = class513.method8764(i_18_, -1, -1290054035) ? 1 : 0;
	}

	public static byte[] method2621(File file, int i) {
		return Class478.method8023(file, (int) file.length(), (byte) 0);
	}

	static Class208 method2622(Class521_Sub1_Sub1 class521_sub1_sub1, int i) {
		Class208 class208;
		if (null == Class507.aClass208_5860)
			class208 = new Class208();
		else {
			class208 = Class507.aClass208_5860;
			Class507.aClass208_5860 = Class507.aClass208_5860.aClass208_2660;
			class208.aClass208_2660 = null;
			Class208.anInt2661 -= -257363711;
		}
		class208.aClass521_Sub1_Sub1_2659 = class521_sub1_sub1;
		return class208;
	}
}
