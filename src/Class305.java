/* Class305 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class305 {
	public Class381 aClass381_3594;
	public Class385 aClass385_3595;

	public void method5411(Class305 class305_0_) {
		aClass381_3594.method6490(class305_0_.aClass381_3594);
		aClass385_3595.method6627(class305_0_.aClass385_3595);
	}

	public String method5412() {
		return new StringBuilder().append("[").append(aClass381_3594.toString()).append("|").append(aClass385_3595.toString()).append("]").toString();
	}

	public Class305() {
		aClass381_3594 = new Class381();
		aClass385_3595 = new Class385();
	}

	public final void method5413() {
		aClass381_3594.method6464();
		aClass385_3595.method6628();
		aClass385_3595.method6634(aClass381_3594);
	}

	public final void method5414(Class305 class305_1_) {
		aClass381_3594.method6466(class305_1_.aClass381_3594);
		aClass385_3595.method6634(class305_1_.aClass381_3594);
		aClass385_3595.method6636(class305_1_.aClass385_3595);
	}

	public Class305(Class305 class305_2_) {
		aClass381_3594 = new Class381();
		aClass385_3595 = new Class385();
		method5411(class305_2_);
	}

	public String toString() {
		return new StringBuilder().append("[").append(aClass381_3594.toString()).append("|").append(aClass385_3595.toString()).append("]").toString();
	}

	public String method5415() {
		return new StringBuilder().append("[").append(aClass381_3594.toString()).append("|").append(aClass385_3595.toString()).append("]").toString();
	}

	public void method5416(Class305 class305_3_) {
		aClass381_3594.method6490(class305_3_.aClass381_3594);
		aClass385_3595.method6627(class305_3_.aClass385_3595);
	}
}
