/* Class349 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class349 {
	Class482 aClass482_4076;
	long aLong4077 = -5721187323234951311L;
	static final int anInt4078 = 3;
	static final int anInt4079 = 1;
	static final int anInt4080 = 2;
	long aLong4081;
	static final int anInt4082 = 4;
	public static int anInt4083;

	public void method6177(Class282_Sub4 class282_sub4) {
		if ((-3442165056282524525L * class282_sub4.aLong3379 != 665091121757577281L * ((Class349) this).aLong4081) || ((((Class282_Sub4) class282_sub4).aLong7502 * 7838563466565878939L) != -343257454049886609L * ((Class349) this).aLong4077))
			throw new RuntimeException("");
		for (Class282_Sub11 class282_sub11 = ((Class282_Sub11) ((Class349) this).aClass482_4076.method8097((byte) 55)); null != class282_sub11; class282_sub11 = (Class282_Sub11) ((Class349) this).aClass482_4076.method8067(1337471144))
			class282_sub11.method12204(class282_sub4, 151325962);
		((Class282_Sub4) class282_sub4).aLong7502 += 8833176109314392467L;
	}

	void method6178(RsByteBuffer class282_sub35, int i) {
		((Class349) this).aLong4081 = class282_sub35.method13087(-33819059) * -7799595159946285119L;
		((Class349) this).aLong4077 = class282_sub35.method13087(377148964) * 5721187323234951311L;
		for (int i_0_ = class282_sub35.readUnsignedByte(); 0 != i_0_; i_0_ = class282_sub35.readUnsignedByte()) {
			Class282_Sub11 class282_sub11;
			if (i_0_ == 1)
				class282_sub11 = new Class282_Sub11_Sub4(this);
			else if (i_0_ == 4)
				class282_sub11 = new Class282_Sub11_Sub1(this);
			else if (3 == i_0_)
				class282_sub11 = new Class282_Sub11_Sub2(this);
			else if (2 == i_0_)
				class282_sub11 = new Class282_Sub11_Sub3(this);
			else
				throw new RuntimeException("");
			class282_sub11.method12203(class282_sub35, -2147137010);
			((Class349) this).aClass482_4076.method8059(class282_sub11, 1721397847);
		}
	}

	public void method6179(Class282_Sub4 class282_sub4, short i) {
		if ((-3442165056282524525L * class282_sub4.aLong3379 != 665091121757577281L * ((Class349) this).aLong4081) || ((((Class282_Sub4) class282_sub4).aLong7502 * 7838563466565878939L) != -343257454049886609L * ((Class349) this).aLong4077))
			throw new RuntimeException("");
		for (Class282_Sub11 class282_sub11 = ((Class282_Sub11) ((Class349) this).aClass482_4076.method8097((byte) 71)); null != class282_sub11; class282_sub11 = (Class282_Sub11) ((Class349) this).aClass482_4076.method8067(2116443073))
			class282_sub11.method12204(class282_sub4, 43407056);
		((Class282_Sub4) class282_sub4).aLong7502 += 8833176109314392467L;
	}

	public void method6180(Class282_Sub4 class282_sub4) {
		if ((-3442165056282524525L * class282_sub4.aLong3379 != 665091121757577281L * ((Class349) this).aLong4081) || ((((Class282_Sub4) class282_sub4).aLong7502 * 7838563466565878939L) != -343257454049886609L * ((Class349) this).aLong4077))
			throw new RuntimeException("");
		for (Class282_Sub11 class282_sub11 = ((Class282_Sub11) ((Class349) this).aClass482_4076.method8097((byte) 116)); null != class282_sub11; class282_sub11 = (Class282_Sub11) ((Class349) this).aClass482_4076.method8067(-1115490511))
			class282_sub11.method12204(class282_sub4, 600579157);
		((Class282_Sub4) class282_sub4).aLong7502 += 8833176109314392467L;
	}

	void method6181(RsByteBuffer class282_sub35) {
		((Class349) this).aLong4081 = class282_sub35.method13087(1517077826) * -7799595159946285119L;
		((Class349) this).aLong4077 = class282_sub35.method13087(1546583950) * 5721187323234951311L;
		for (int i = class282_sub35.readUnsignedByte(); 0 != i; i = class282_sub35.readUnsignedByte()) {
			Class282_Sub11 class282_sub11;
			if (i == 1)
				class282_sub11 = new Class282_Sub11_Sub4(this);
			else if (i == 4)
				class282_sub11 = new Class282_Sub11_Sub1(this);
			else if (3 == i)
				class282_sub11 = new Class282_Sub11_Sub2(this);
			else if (2 == i)
				class282_sub11 = new Class282_Sub11_Sub3(this);
			else
				throw new RuntimeException("");
			class282_sub11.method12203(class282_sub35, 957900088);
			((Class349) this).aClass482_4076.method8059(class282_sub11, 834052677);
		}
	}

	public Class349(RsByteBuffer class282_sub35) {
		((Class349) this).aClass482_4076 = new Class482();
		method6178(class282_sub35, 1987332123);
	}

	void method6182(RsByteBuffer class282_sub35) {
		((Class349) this).aLong4081 = class282_sub35.method13087(967958284) * -7799595159946285119L;
		((Class349) this).aLong4077 = class282_sub35.method13087(80248785) * 5721187323234951311L;
		for (int i = class282_sub35.readUnsignedByte(); 0 != i; i = class282_sub35.readUnsignedByte()) {
			Class282_Sub11 class282_sub11;
			if (i == 1)
				class282_sub11 = new Class282_Sub11_Sub4(this);
			else if (i == 4)
				class282_sub11 = new Class282_Sub11_Sub1(this);
			else if (3 == i)
				class282_sub11 = new Class282_Sub11_Sub2(this);
			else if (2 == i)
				class282_sub11 = new Class282_Sub11_Sub3(this);
			else
				throw new RuntimeException("");
			class282_sub11.method12203(class282_sub35, -1840577157);
			((Class349) this).aClass482_4076.method8059(class282_sub11, 1022888383);
		}
	}

	void method6183(RsByteBuffer class282_sub35) {
		((Class349) this).aLong4081 = class282_sub35.method13087(1612258645) * -7799595159946285119L;
		((Class349) this).aLong4077 = class282_sub35.method13087(5510848) * 5721187323234951311L;
		for (int i = class282_sub35.readUnsignedByte(); 0 != i; i = class282_sub35.readUnsignedByte()) {
			Class282_Sub11 class282_sub11;
			if (i == 1)
				class282_sub11 = new Class282_Sub11_Sub4(this);
			else if (i == 4)
				class282_sub11 = new Class282_Sub11_Sub1(this);
			else if (3 == i)
				class282_sub11 = new Class282_Sub11_Sub2(this);
			else if (2 == i)
				class282_sub11 = new Class282_Sub11_Sub3(this);
			else
				throw new RuntimeException("");
			class282_sub11.method12203(class282_sub35, 1950697613);
			((Class349) this).aClass482_4076.method8059(class282_sub11, -968547209);
		}
	}

	public void method6184(Class282_Sub4 class282_sub4) {
		if ((-3442165056282524525L * class282_sub4.aLong3379 != 665091121757577281L * ((Class349) this).aLong4081) || ((((Class282_Sub4) class282_sub4).aLong7502 * 7838563466565878939L) != -343257454049886609L * ((Class349) this).aLong4077))
			throw new RuntimeException("");
		for (Class282_Sub11 class282_sub11 = ((Class282_Sub11) ((Class349) this).aClass482_4076.method8097((byte) 52)); null != class282_sub11; class282_sub11 = (Class282_Sub11) ((Class349) this).aClass482_4076.method8067(51104557))
			class282_sub11.method12204(class282_sub4, 1780825834);
		((Class282_Sub4) class282_sub4).aLong7502 += 8833176109314392467L;
	}

	void method6185(RsByteBuffer class282_sub35) {
		((Class349) this).aLong4081 = class282_sub35.method13087(142236091) * -7799595159946285119L;
		((Class349) this).aLong4077 = class282_sub35.method13087(1402159063) * 5721187323234951311L;
		for (int i = class282_sub35.readUnsignedByte(); 0 != i; i = class282_sub35.readUnsignedByte()) {
			Class282_Sub11 class282_sub11;
			if (i == 1)
				class282_sub11 = new Class282_Sub11_Sub4(this);
			else if (i == 4)
				class282_sub11 = new Class282_Sub11_Sub1(this);
			else if (3 == i)
				class282_sub11 = new Class282_Sub11_Sub2(this);
			else if (2 == i)
				class282_sub11 = new Class282_Sub11_Sub3(this);
			else
				throw new RuntimeException("");
			class282_sub11.method12203(class282_sub35, 2002219110);
			((Class349) this).aClass482_4076.method8059(class282_sub11, -88632740);
		}
	}

	static final void method6186(Class118 class118, Class98 class98, Class527 class527, int i) {
		if (4 == -2131393857 * class118.anInt1268)
			Class306.method5459(class118, class98, class527, -2108209213);
	}
}
