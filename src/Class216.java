/* Class216 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class216 {
	public static final int anInt2687 = 8;
	public static final int anInt2688 = 9;
	public static final int anInt2689 = 512;
	public static final int anInt2690 = 511;
	public static final int anInt2691 = 1024;
	public static final int anInt2692 = 2;
	public static final int anInt2693 = 256;
	public static final int anInt2694 = 128;
	public static final int anInt2695 = 7;

	Class216() throws Throwable {
		throw new Error();
	}

	static {
		Math.sqrt(131072.0);
	}

	static final void method3674(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_0_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012]);
		int i_1_ = (((Class527) class527).anIntArray6999[1 + ((Class527) class527).anInt7012 * 1942118537]);
		Class437 class437 = Class125.aClass424_1573.method7069(i_1_, (byte) 0);
		if (class437.method7319(1764887280))
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = Class330.aClass523_3868.method11205(i_0_, (byte) -22).method11129(i_1_, class437.aString5335, 803635154);
		else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = (Class330.aClass523_3868.method11205(i_0_, (byte) -46).method11133(i_1_, -1741480635 * class437.anInt5337, 1720724957));
	}

	static void method3675(int i) {
		if (Class260.aClass116_3229 != null && !Class260.aClass116_3229.method1956(1097733979) && Class260.aClass116_3229.method1963(1418683997)) {
			Class282_Sub23 class282_sub23 = Class271.method4828(OutgoingPacket.aClass379_4531, client.aClass184_7475.aClass432_2283, 920221723);
			class282_sub23.buffer.writeInt(773403033 * Class260.aClass116_3229.anInt1254);
			client.aClass184_7475.method3049(class282_sub23, 17076661);
		}
	}

	static final void method3676(Class527 class527, byte i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class473.method7889(class118, class98, class527, 1072784051);
	}

	static boolean method3677(int i) {
		return Class20.anInt169 * 2144330291 > 0;
	}

	static final void method3678(Class527 class527, int i) {
		((Class527) class527).anInt7001 -= 1918006146;
		if ((((Class527) class527).aLongArray7003[1820448321 * ((Class527) class527).anInt7001]) > (((Class527) class527).aLongArray7003[1 + ((Class527) class527).anInt7001 * 1820448321]))
			((Class527) class527).anInt7020 += ((((Class527) class527).anIntArray7018[301123709 * ((Class527) class527).anInt7020]) * -1051529003);
	}
}
