/* Class282_Sub20_Sub23 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub20_Sub23 extends Class282_Sub20 {
	boolean aBool9875;
	static final boolean aBool9876 = true;
	static final boolean aBool9877 = true;
	boolean aBool9878 = true;
	static final boolean aBool9879 = false;

	void method12335(int i, RsByteBuffer class282_sub35) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub23) this).aBool9878 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 2:
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 1:
			((Class282_Sub20_Sub23) this).aBool9875 = class282_sub35.readUnsignedByte() == 1;
			break;
		}
	}

	int[] method12319(int i, int i_0_) {
		int[] is = aClass320_7667.method5721(i, -1293135852);
		if (aClass320_7667.aBool3722) {
			int[] is_1_ = method12317(0, (((Class282_Sub20_Sub23) this).aBool9875 ? 543225399 * Class316.anInt3673 - i : i), 1953683424);
			if (((Class282_Sub20_Sub23) this).aBool9878) {
				for (int i_2_ = 0; i_2_ < -402153223 * Class316.anInt3670; i_2_++)
					is[i_2_] = is_1_[Class316.anInt3669 * 1201532175 - i_2_];
			} else
				Class503.method8362(is_1_, 0, is, 0, Class316.anInt3670 * -402153223);
		}
		return is;
	}

	int[][] method12320(int i, int i_3_) {
		int[][] is = aClass308_7670.method5463(i, 2077005299);
		if (aClass308_7670.aBool3619) {
			int[][] is_4_ = method12333(0, (((Class282_Sub20_Sub23) this).aBool9875 ? Class316.anInt3673 * 543225399 - i : i), (byte) 3);
			int[] is_5_ = is_4_[0];
			int[] is_6_ = is_4_[1];
			int[] is_7_ = is_4_[2];
			int[] is_8_ = is[0];
			int[] is_9_ = is[1];
			int[] is_10_ = is[2];
			if (((Class282_Sub20_Sub23) this).aBool9878) {
				for (int i_11_ = 0; i_11_ < -402153223 * Class316.anInt3670; i_11_++) {
					is_8_[i_11_] = is_5_[1201532175 * Class316.anInt3669 - i_11_];
					is_9_[i_11_] = is_6_[Class316.anInt3669 * 1201532175 - i_11_];
					is_10_[i_11_] = is_7_[1201532175 * Class316.anInt3669 - i_11_];
				}
			} else {
				for (int i_12_ = 0; i_12_ < -402153223 * Class316.anInt3670; i_12_++) {
					is_8_[i_12_] = is_5_[i_12_];
					is_9_[i_12_] = is_6_[i_12_];
					is_10_[i_12_] = is_7_[i_12_];
				}
			}
		}
		return is;
	}

	void method12322(int i, RsByteBuffer class282_sub35, int i_13_) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub23) this).aBool9878 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 2:
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 1:
			((Class282_Sub20_Sub23) this).aBool9875 = class282_sub35.readUnsignedByte() == 1;
			break;
		}
	}

	int[] method12325(int i) {
		int[] is = aClass320_7667.method5721(i, -1710591269);
		if (aClass320_7667.aBool3722) {
			int[] is_14_ = method12317(0, (((Class282_Sub20_Sub23) this).aBool9875 ? 543225399 * Class316.anInt3673 - i : i), 1990874436);
			if (((Class282_Sub20_Sub23) this).aBool9878) {
				for (int i_15_ = 0; i_15_ < -402153223 * Class316.anInt3670; i_15_++)
					is[i_15_] = is_14_[Class316.anInt3669 * 1201532175 - i_15_];
			} else
				Class503.method8362(is_14_, 0, is, 0, Class316.anInt3670 * -402153223);
		}
		return is;
	}

	int[] method12336(int i) {
		int[] is = aClass320_7667.method5721(i, -2130493439);
		if (aClass320_7667.aBool3722) {
			int[] is_16_ = method12317(0, (((Class282_Sub20_Sub23) this).aBool9875 ? 543225399 * Class316.anInt3673 - i : i), 2095222759);
			if (((Class282_Sub20_Sub23) this).aBool9878) {
				for (int i_17_ = 0; i_17_ < -402153223 * Class316.anInt3670; i_17_++)
					is[i_17_] = is_16_[Class316.anInt3669 * 1201532175 - i_17_];
			} else
				Class503.method8362(is_16_, 0, is, 0, Class316.anInt3670 * -402153223);
		}
		return is;
	}

	int[] method12327(int i) {
		int[] is = aClass320_7667.method5721(i, -1307112686);
		if (aClass320_7667.aBool3722) {
			int[] is_18_ = method12317(0, (((Class282_Sub20_Sub23) this).aBool9875 ? 543225399 * Class316.anInt3673 - i : i), 2016203775);
			if (((Class282_Sub20_Sub23) this).aBool9878) {
				for (int i_19_ = 0; i_19_ < -402153223 * Class316.anInt3670; i_19_++)
					is[i_19_] = is_18_[Class316.anInt3669 * 1201532175 - i_19_];
			} else
				Class503.method8362(is_18_, 0, is, 0, Class316.anInt3670 * -402153223);
		}
		return is;
	}

	void method12332(int i, RsByteBuffer class282_sub35) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub23) this).aBool9878 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 2:
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 1:
			((Class282_Sub20_Sub23) this).aBool9875 = class282_sub35.readUnsignedByte() == 1;
			break;
		}
	}

	void method12334(int i, RsByteBuffer class282_sub35) {
		switch (i) {
		case 0:
			((Class282_Sub20_Sub23) this).aBool9878 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 2:
			aBool7669 = class282_sub35.readUnsignedByte() == 1;
			break;
		case 1:
			((Class282_Sub20_Sub23) this).aBool9875 = class282_sub35.readUnsignedByte() == 1;
			break;
		}
	}

	int[][] method12339(int i) {
		int[][] is = aClass308_7670.method5463(i, 1128597303);
		if (aClass308_7670.aBool3619) {
			int[][] is_20_ = method12333(0, (((Class282_Sub20_Sub23) this).aBool9875 ? Class316.anInt3673 * 543225399 - i : i), (byte) 3);
			int[] is_21_ = is_20_[0];
			int[] is_22_ = is_20_[1];
			int[] is_23_ = is_20_[2];
			int[] is_24_ = is[0];
			int[] is_25_ = is[1];
			int[] is_26_ = is[2];
			if (((Class282_Sub20_Sub23) this).aBool9878) {
				for (int i_27_ = 0; i_27_ < -402153223 * Class316.anInt3670; i_27_++) {
					is_24_[i_27_] = is_21_[1201532175 * Class316.anInt3669 - i_27_];
					is_25_[i_27_] = is_22_[Class316.anInt3669 * 1201532175 - i_27_];
					is_26_[i_27_] = is_23_[1201532175 * Class316.anInt3669 - i_27_];
				}
			} else {
				for (int i_28_ = 0; i_28_ < -402153223 * Class316.anInt3670; i_28_++) {
					is_24_[i_28_] = is_21_[i_28_];
					is_25_[i_28_] = is_22_[i_28_];
					is_26_[i_28_] = is_23_[i_28_];
				}
			}
		}
		return is;
	}

	public Class282_Sub20_Sub23() {
		super(1, false);
		((Class282_Sub20_Sub23) this).aBool9875 = true;
	}

	static final void method15389(Class527 class527, byte i) {
		((Class527) class527).anInt7012 -= 283782002;
		int i_29_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_30_ = (((Class527) class527).anIntArray6999[1 + ((Class527) class527).anInt7012 * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class117.method1980(i_29_, i_30_, false, 1448220048);
	}
}
