/* Class176 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class176 {
	int anInt2189;
	float aFloat2190 = 1.0F;
	float aFloat2191 = 1.0F;
	int anInt2192;
	float aFloat2193;
	int anInt2194;
	int anInt2195;
	int anInt2196;
	int anInt2197;
	int anInt2198;
	int anInt2199;
	static Class414 aClass414_2200;
	static Class472 aClass472_2201;

	void method2967(Class176 class176_0_) {
		((Class176) this).aFloat2190 = ((Class176) class176_0_).aFloat2190;
		((Class176) this).aFloat2191 = ((Class176) class176_0_).aFloat2191;
		((Class176) this).anInt2192 = ((Class176) class176_0_).anInt2192 * 1;
		((Class176) this).anInt2189 = ((Class176) class176_0_).anInt2189 * 1;
		((Class176) this).anInt2199 = 1 * ((Class176) class176_0_).anInt2199;
		((Class176) this).anInt2194 = ((Class176) class176_0_).anInt2194 * 1;
	}

	void method2968(Class176 class176_1_, int i) {
		((Class176) this).aFloat2190 = ((Class176) class176_1_).aFloat2190;
		((Class176) this).aFloat2191 = ((Class176) class176_1_).aFloat2191;
		((Class176) this).anInt2192 = ((Class176) class176_1_).anInt2192 * 1;
		((Class176) this).anInt2189 = ((Class176) class176_1_).anInt2189 * 1;
		((Class176) this).anInt2199 = 1 * ((Class176) class176_1_).anInt2199;
		((Class176) this).anInt2194 = ((Class176) class176_1_).anInt2194 * 1;
	}

	Class176(int i, float f, float f_2_, int i_3_, int i_4_, int i_5_) {
		((Class176) this).anInt2199 = -1022818925 * i;
		((Class176) this).aFloat2190 = f;
		((Class176) this).aFloat2191 = f_2_;
		((Class176) this).anInt2192 = -943685543 * i_3_;
		((Class176) this).anInt2189 = 1414070385 * i_4_;
		((Class176) this).anInt2194 = i_5_ * 2076699445;
	}

	Class176(int i) {
		((Class176) this).anInt2199 = -1022818925 * i;
	}

	Class176 method2969() {
		return new Class176(-1303125861 * ((Class176) this).anInt2199, ((Class176) this).aFloat2190, ((Class176) this).aFloat2191, -310982679 * ((Class176) this).anInt2192, 1349697681 * ((Class176) this).anInt2189, 156323613 * ((Class176) this).anInt2194);
	}

	Class176 method2970() {
		return new Class176(-1303125861 * ((Class176) this).anInt2199, ((Class176) this).aFloat2190, ((Class176) this).aFloat2191, -310982679 * ((Class176) this).anInt2192, 1349697681 * ((Class176) this).anInt2189, 156323613 * ((Class176) this).anInt2194);
	}

	void method2971(Class176 class176_6_) {
		((Class176) this).aFloat2190 = ((Class176) class176_6_).aFloat2190;
		((Class176) this).aFloat2191 = ((Class176) class176_6_).aFloat2191;
		((Class176) this).anInt2192 = ((Class176) class176_6_).anInt2192 * 1;
		((Class176) this).anInt2189 = ((Class176) class176_6_).anInt2189 * 1;
		((Class176) this).anInt2199 = 1 * ((Class176) class176_6_).anInt2199;
		((Class176) this).anInt2194 = ((Class176) class176_6_).anInt2194 * 1;
	}

	Class176 method2972(int i) {
		return new Class176(-1303125861 * ((Class176) this).anInt2199, ((Class176) this).aFloat2190, ((Class176) this).aFloat2191, -310982679 * ((Class176) this).anInt2192, 1349697681 * ((Class176) this).anInt2189, 156323613 * ((Class176) this).anInt2194);
	}

	void method2973(Class176 class176_7_) {
		((Class176) this).aFloat2190 = ((Class176) class176_7_).aFloat2190;
		((Class176) this).aFloat2191 = ((Class176) class176_7_).aFloat2191;
		((Class176) this).anInt2192 = ((Class176) class176_7_).anInt2192 * 1;
		((Class176) this).anInt2189 = ((Class176) class176_7_).anInt2189 * 1;
		((Class176) this).anInt2199 = 1 * ((Class176) class176_7_).anInt2199;
		((Class176) this).anInt2194 = ((Class176) class176_7_).anInt2194 * 1;
	}

	void method2974(Class176 class176_8_) {
		((Class176) this).aFloat2190 = ((Class176) class176_8_).aFloat2190;
		((Class176) this).aFloat2191 = ((Class176) class176_8_).aFloat2191;
		((Class176) this).anInt2192 = ((Class176) class176_8_).anInt2192 * 1;
		((Class176) this).anInt2189 = ((Class176) class176_8_).anInt2189 * 1;
		((Class176) this).anInt2199 = 1 * ((Class176) class176_8_).anInt2199;
		((Class176) this).anInt2194 = ((Class176) class176_8_).anInt2194 * 1;
	}

	void method2975(Class176 class176_9_) {
		((Class176) this).aFloat2190 = ((Class176) class176_9_).aFloat2190;
		((Class176) this).aFloat2191 = ((Class176) class176_9_).aFloat2191;
		((Class176) this).anInt2192 = ((Class176) class176_9_).anInt2192 * 1;
		((Class176) this).anInt2189 = ((Class176) class176_9_).anInt2189 * 1;
		((Class176) this).anInt2199 = 1 * ((Class176) class176_9_).anInt2199;
		((Class176) this).anInt2194 = ((Class176) class176_9_).anInt2194 * 1;
	}

	static void method2976(RsByteBuffer class282_sub35, int i) {
		for (;;) {
			int i_10_ = class282_sub35.readUnsignedByte();
			switch (i_10_) {
			case 0:
				Class86.anInt825 = class282_sub35.readUnsignedShort() * -251071231;
				Class86.anInt824 = class282_sub35.readUnsignedShort() * -11206567;
				break;
			case 255:
				return;
			}
		}
	}

	public static void method2977(int i) {
		Class96_Sub5.method14243((short) 31899);
	}

	static final void method2978(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class393.aClass282_Sub54_4783.aClass468_Sub4_8187.method12641(-255008598);
	}

	static long method2979(int i) {
		return Class263.aClass273_3244.method4852(1483747562);
	}

	static boolean method2980(int i, int i_11_) {
		return 5 == i || i == 0 || i == 7 || 13 == i;
	}
}
