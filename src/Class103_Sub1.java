
/* Class103_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;

public class Class103_Sub1 extends Class103 {
	Class282_Sub21_Sub1 aClass282_Sub21_Sub1_9144;
	Class282_Sub21_Sub1 aClass282_Sub21_Sub1_9145;
	Class282_Sub21_Sub1 aClass282_Sub21_Sub1_9146;
	Class384 aClass384_9147 = new Class384();
	Class101 aClass101_9148;
	Class282_Sub21_Sub1 aClass282_Sub21_Sub1_9149;

	boolean method14483(int i) throws Exception_Sub2 {
		((Class103_Sub1) this).aClass101_9148 = aClass505_Sub2_1057.method13890("Particle");
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9144 = ((Class103_Sub1) this).aClass101_9148.method1691("WVPMatrix", (short) 22417);
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9145 = ((Class103_Sub1) this).aClass101_9148.method1691("DiffuseSampler", (short) 26058);
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9149 = ((Class103_Sub1) this).aClass101_9148.method1691("TexCoordMatrix", (short) 32643);
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9146 = ((Class103_Sub1) this).aClass101_9148.method1691("DiffuseColour", (short) 29027);
		((Class103_Sub1) this).aClass101_9148.method1655(((Class103_Sub1) this).aClass101_9148.method1654(-1983174373));
		return true;
	}

	void method14484() {
		((Class103_Sub1) this).aClass101_9148.method1655(((Class103_Sub1) this).aClass101_9148.method1654(-41047680));
		((Class103_Sub1) this).aClass101_9148.method1646();
		((Class103_Sub1) this).aClass101_9148.method1671(((Class103_Sub1) this).aClass282_Sub21_Sub1_9145, 0, anInterface6_1059, (byte) 0);
		((Class103_Sub1) this).aClass101_9148.method1668(((Class103_Sub1) this).aClass282_Sub21_Sub1_9144, ((Class103_Sub1) this).aClass384_9147, (byte) -15);
		((Class103_Sub1) this).aClass101_9148.method1667(((Class103_Sub1) this).aClass282_Sub21_Sub1_9149, aClass384_1058, -98002445);
		((Class103_Sub1) this).aClass101_9148.method1696(((Class103_Sub1) this).aClass282_Sub21_Sub1_9146, anInt1056, 2086125175);
	}

	public void method1788(Class384 class384) {
		((Class103_Sub1) this).aClass384_9147.method6562(class384);
		((Class103_Sub1) this).aClass384_9147.method6523(aClass505_Sub2_1057.aClass384_8729);
	}

	public void method1787(int i) {
		method14486(2119747101);
		aClass505_Sub2_1057.method14002(Class352.aClass352_4098, 0, 4 * i, 0, i * 2);
	}

	public void method1790() {
		method14486(2135879735);
		aClass505_Sub2_1057.method14150();
	}

	void method14485() {
		((Class103_Sub1) this).aClass101_9148.method1655(((Class103_Sub1) this).aClass101_9148.method1654(-438879357));
		((Class103_Sub1) this).aClass101_9148.method1646();
		((Class103_Sub1) this).aClass101_9148.method1671(((Class103_Sub1) this).aClass282_Sub21_Sub1_9145, 0, anInterface6_1059, (byte) 0);
		((Class103_Sub1) this).aClass101_9148.method1668(((Class103_Sub1) this).aClass282_Sub21_Sub1_9144, ((Class103_Sub1) this).aClass384_9147, (byte) -18);
		((Class103_Sub1) this).aClass101_9148.method1667(((Class103_Sub1) this).aClass282_Sub21_Sub1_9149, aClass384_1058, 1083432562);
		((Class103_Sub1) this).aClass101_9148.method1696(((Class103_Sub1) this).aClass282_Sub21_Sub1_9146, anInt1056, 1874762749);
	}

	public void method1789(Class384 class384) {
		((Class103_Sub1) this).aClass384_9147.method6562(class384);
		((Class103_Sub1) this).aClass384_9147.method6523(aClass505_Sub2_1057.aClass384_8729);
	}

	public void method1786(Class384 class384) {
		((Class103_Sub1) this).aClass384_9147.method6562(class384);
		((Class103_Sub1) this).aClass384_9147.method6523(aClass505_Sub2_1057.aClass384_8729);
	}

	public void method1785(int i) {
		method14486(2127063617);
		aClass505_Sub2_1057.method14002(Class352.aClass352_4098, 0, 4 * i, 0, i * 2);
	}

	public void method1791() {
		method14486(2126455360);
		aClass505_Sub2_1057.method14150();
	}

	public void method1792() {
		method14486(2147226519);
		aClass505_Sub2_1057.method14150();
	}

	void method14486(int i) {
		((Class103_Sub1) this).aClass101_9148.method1655(((Class103_Sub1) this).aClass101_9148.method1654(96252371));
		((Class103_Sub1) this).aClass101_9148.method1646();
		((Class103_Sub1) this).aClass101_9148.method1671(((Class103_Sub1) this).aClass282_Sub21_Sub1_9145, 0, anInterface6_1059, (byte) 0);
		((Class103_Sub1) this).aClass101_9148.method1668(((Class103_Sub1) this).aClass282_Sub21_Sub1_9144, ((Class103_Sub1) this).aClass384_9147, (byte) -104);
		((Class103_Sub1) this).aClass101_9148.method1667(((Class103_Sub1) this).aClass282_Sub21_Sub1_9149, aClass384_1058, -262849390);
		((Class103_Sub1) this).aClass101_9148.method1696(((Class103_Sub1) this).aClass282_Sub21_Sub1_9146, anInt1056, 1511000822);
	}

	void method14487() {
		((Class103_Sub1) this).aClass101_9148.method1655(((Class103_Sub1) this).aClass101_9148.method1654(1864945832));
		((Class103_Sub1) this).aClass101_9148.method1646();
		((Class103_Sub1) this).aClass101_9148.method1671(((Class103_Sub1) this).aClass282_Sub21_Sub1_9145, 0, anInterface6_1059, (byte) 0);
		((Class103_Sub1) this).aClass101_9148.method1668(((Class103_Sub1) this).aClass282_Sub21_Sub1_9144, ((Class103_Sub1) this).aClass384_9147, (byte) -86);
		((Class103_Sub1) this).aClass101_9148.method1667(((Class103_Sub1) this).aClass282_Sub21_Sub1_9149, aClass384_1058, 1909482988);
		((Class103_Sub1) this).aClass101_9148.method1696(((Class103_Sub1) this).aClass282_Sub21_Sub1_9146, anInt1056, 1801938600);
	}

	void method14488() {
		((Class103_Sub1) this).aClass101_9148.method1655(((Class103_Sub1) this).aClass101_9148.method1654(-2133459004));
		((Class103_Sub1) this).aClass101_9148.method1646();
		((Class103_Sub1) this).aClass101_9148.method1671(((Class103_Sub1) this).aClass282_Sub21_Sub1_9145, 0, anInterface6_1059, (byte) 0);
		((Class103_Sub1) this).aClass101_9148.method1668(((Class103_Sub1) this).aClass282_Sub21_Sub1_9144, ((Class103_Sub1) this).aClass384_9147, (byte) -14);
		((Class103_Sub1) this).aClass101_9148.method1667(((Class103_Sub1) this).aClass282_Sub21_Sub1_9149, aClass384_1058, 1642986683);
		((Class103_Sub1) this).aClass101_9148.method1696(((Class103_Sub1) this).aClass282_Sub21_Sub1_9146, anInt1056, 1790793974);
	}

	boolean method14489() throws Exception_Sub2 {
		((Class103_Sub1) this).aClass101_9148 = aClass505_Sub2_1057.method13890("Particle");
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9144 = ((Class103_Sub1) this).aClass101_9148.method1691("WVPMatrix", (short) 8116);
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9145 = ((Class103_Sub1) this).aClass101_9148.method1691("DiffuseSampler", (short) 5936);
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9149 = ((Class103_Sub1) this).aClass101_9148.method1691("TexCoordMatrix", (short) 26376);
		((Class103_Sub1) this).aClass282_Sub21_Sub1_9146 = ((Class103_Sub1) this).aClass101_9148.method1691("DiffuseColour", (short) 29720);
		((Class103_Sub1) this).aClass101_9148.method1655(((Class103_Sub1) this).aClass101_9148.method1654(-473489690));
		return true;
	}

	public Class103_Sub1(Class505_Sub2 class505_sub2) throws Exception_Sub2 {
		super(class505_sub2);
		method14483(1614437421);
	}

	static Class160 method14490(byte[] is, byte i) {
		if (is == null)
			throw new RuntimeException("");
		Class160 class160;
		for (;;) {
			try {
				Image image = Toolkit.getDefaultToolkit().createImage(is);
				MediaTracker mediatracker = new MediaTracker(Class282_Sub44.anApplet8065);
				mediatracker.addImage(image, 0);
				mediatracker.waitForAll();
				int i_0_ = image.getWidth(Class282_Sub44.anApplet8065);
				int i_1_ = image.getHeight(Class282_Sub44.anApplet8065);
				if (mediatracker.isErrorAny() || i_0_ < 0 || i_1_ < 0)
					throw new RuntimeException("");
				int[] is_2_ = new int[i_0_ * i_1_];
				PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, i_0_, i_1_, is_2_, 0, i_0_);
				pixelgrabber.grabPixels();
				class160 = Class316.aClass505_3680.method8549(is_2_, 0, i_0_, i_0_, i_1_, 194479026);
				break;
			} catch (InterruptedException interruptedexception) {
				/* empty */
			}
		}
		return class160;
	}
}
