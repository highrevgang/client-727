/* Class411 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class411 {
	static Class411 aClass411_4925;
	static Class411 aClass411_4926;
	static Class411 aClass411_4927;
	static Class411 aClass411_4928;
	static Class411 aClass411_4929;
	static Class411 aClass411_4930;
	static Class411 aClass411_4931;
	static Class411 aClass411_4932;
	static Class411 aClass411_4933;
	static Class411 aClass411_4934;
	static Class411 aClass411_4935;
	static Class411 aClass411_4936;
	static Class411 aClass411_4937;
	static Class411 aClass411_4938;
	static Class411 aClass411_4939;
	static Class411 aClass411_4940;
	static Class411 aClass411_4941;
	static Class411 aClass411_4942;
	static Class411 aClass411_4943;
	static Class411 aClass411_4944;
	static Class411 aClass411_4945;
	static Class411 aClass411_4946;
	static Class411 aClass411_4947;
	static Class411 aClass411_4948;
	static Class411 aClass411_4949;
	static Class411 aClass411_4950;
	static Class411 aClass411_4951 = new Class411(8, 0);
	static Class411 aClass411_4952;
	static Class411 aClass411_4953;
	static Class411 aClass411_4954;
	static Class411 aClass411_4955;
	public int anInt4956;
	int anInt4957;

	static {
		aClass411_4926 = new Class411(11, 1);
		aClass411_4927 = new Class411(3, 2);
		aClass411_4928 = new Class411(7, 3);
		aClass411_4929 = new Class411(4, 10);
		aClass411_4930 = new Class411(1, 11);
		aClass411_4942 = new Class411(9, 12);
		aClass411_4944 = new Class411(12, 13);
		aClass411_4933 = new Class411(26, 14);
		aClass411_4934 = new Class411(2, 15);
		aClass411_4935 = new Class411(29, 16);
		aClass411_4936 = new Class411(25, 17);
		aClass411_4937 = new Class411(20, 20);
		aClass411_4953 = new Class411(0, 21);
		aClass411_4939 = new Class411(13, 22);
		aClass411_4940 = new Class411(18, 30);
		aClass411_4941 = new Class411(23, 31);
		aClass411_4950 = new Class411(5, 32);
		aClass411_4943 = new Class411(6, 33);
		aClass411_4954 = new Class411(30, 40);
		aClass411_4947 = new Class411(17, 41);
		aClass411_4946 = new Class411(14, 42);
		aClass411_4938 = new Class411(21, 43);
		aClass411_4948 = new Class411(28, 50);
		aClass411_4949 = new Class411(24, 51);
		aClass411_4945 = new Class411(15, 52);
		aClass411_4932 = new Class411(19, 53);
		aClass411_4952 = new Class411(10, 60);
		aClass411_4925 = new Class411(27, 61);
		aClass411_4931 = new Class411(22, 70);
		aClass411_4955 = new Class411(16, 255);
	}

	static Class411[] method6910() {
		return new Class411[] { aClass411_4928, aClass411_4952, aClass411_4939, aClass411_4954, aClass411_4942, aClass411_4948, aClass411_4941, aClass411_4953, aClass411_4934, aClass411_4926, aClass411_4951, aClass411_4930, aClass411_4935, aClass411_4946, aClass411_4927, aClass411_4944, aClass411_4943, aClass411_4931, aClass411_4949, aClass411_4940, aClass411_4936, aClass411_4947, aClass411_4925, aClass411_4929, aClass411_4955, aClass411_4950, aClass411_4937, aClass411_4932, aClass411_4933, aClass411_4938, aClass411_4945 };
	}

	Class411(int i, int i_0_) {
		anInt4956 = -690536613 * i;
		((Class411) this).anInt4957 = i_0_ * -2073257567;
	}

	public static Class411 method6911(int i) {
		Class411[] class411s = Class451.method7519((short) 5460);
		Class411[] class411s_1_ = class411s;
		for (int i_2_ = 0; i_2_ < class411s_1_.length; i_2_++) {
			Class411 class411 = class411s_1_[i_2_];
			if (((Class411) class411).anInt4957 * 627673697 == i)
				return class411;
		}
		return null;
	}

	static void method6912(RsByteBuffer class282_sub35, int i) {
		int i_3_ = class282_sub35.readUnsignedSmart(1492689351);
		Class354.aClass213Array4111 = new Class213[i_3_];
		for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
			Class354.aClass213Array4111[i_4_] = new Class213();
			Class354.aClass213Array4111[i_4_].anInt2680 = class282_sub35.readUnsignedSmart(1678784961) * 2012866263;
			Class354.aClass213Array4111[i_4_].aString2679 = class282_sub35.method13091(-25640017);
		}
		Class485.anInt5740 = class282_sub35.readUnsignedSmart(1537539293) * 1385884931;
		Class244.anInt3003 = class282_sub35.readUnsignedSmart(1477928184) * -1292032567;
		Class4.anInt34 = class282_sub35.readUnsignedSmart(2033196684) * -275211251;
		Class244.aClass217_Sub1Array3006 = new Class217_Sub1[(Class244.anInt3003 * -860748679 - -377428565 * Class485.anInt5740 + 1)];
		for (int i_5_ = 0; i_5_ < Class4.anInt34 * -2043473211; i_5_++) {
			int i_6_ = class282_sub35.readUnsignedSmart(1627362556);
			Class217_Sub1 class217_sub1 = Class244.aClass217_Sub1Array3006[i_6_] = new Class217_Sub1();
			((Class217_Sub1) class217_sub1).anInt2696 = class282_sub35.readUnsignedByte() * -2047707083;
			class217_sub1.anInt2700 = class282_sub35.readIntLE() * 285408817;
			class217_sub1.anInt7975 = (i_6_ + -377428565 * Class485.anInt5740) * 726222743;
			class217_sub1.aString7977 = class282_sub35.method13091(-1607231726);
			class217_sub1.aString7976 = class282_sub35.method13091(-157018561);
		}
		Class418.anInt4999 = class282_sub35.readIntLE() * 661556035;
		Class244.aBool3007 = true;
	}

	static final void method6913(Class527 class527, int i) {
		((Class527) class527).anInt7000 -= -1341717846;
		String string = (String) (((Class527) class527).anObjectArray7019[1806726141 * ((Class527) class527).anInt7000]);
		String string_7_ = (String) (((Class527) class527).anObjectArray7019[((Class527) class527).anInt7000 * 1806726141 + 1]);
		if (client.anInt7318 * -644057819 != 0 || (!client.aBool7224 || client.aBool7244) && !client.aBool7325) {
			Class184 class184 = Class468_Sub20.method12807(65699692);
			Class282_Sub23 packet = Class271.method4828(OutgoingPacket.SEND_FRIEND_MESSAGE_PACKET, class184.aClass432_2283, 1733219960);
			packet.buffer.writeShort(0, 1417031095);
			int i_8_ = (-1990677291 * packet.buffer.index);
			packet.buffer.writeString(string);
			Class427.method7189(packet.buffer, string_7_, -1045149130);
			packet.buffer.method13281((-1990677291 * packet.buffer.index) - i_8_, 1201423895);
			class184.method3049(packet, 850371200);
		}
	}

	public static void method6914(int i, short i_9_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(9, (long) i);
		class282_sub50_sub12.method14965((byte) -16);
	}

	public static Class350 method6915(RsByteBuffer class282_sub35, int i) {
		Class350 class350 = Class383.method6512(class282_sub35, 88062096);
		int i_10_ = class282_sub35.readIntLE();
		int i_11_ = class282_sub35.readIntLE();
		return new Class350_Sub2(class350.aClass356_4094, class350.aClass353_4087, class350.anInt4090 * -1967081549, class350.anInt4089 * -1196256967, class350.anInt4093 * 329542577, 323608093 * class350.anInt4088, -1921815535 * class350.anInt4092, 985690519 * class350.anInt4086, class350.anInt4091 * -771513131, i_10_, i_11_);
	}

	static final void method6916(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = (Class84.myPlayer.anInt10565 * -1880473919);
	}
}
