/* Class282_Sub40 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub40 extends Class282 {
	int anInt8006;
	Class345 aClass345_8007;
	int anInt8008;
	Class172 aClass172_8009;
	int anInt8010;
	float aFloat8011;
	float aFloat8012;
	static float[] aFloatArray8013 = new float[3];

	void method13298(byte i) {
		((Class282_Sub40) this).anInt8010 = -1814601513 * ((Class282_Sub40) this).aClass172_8009.anInt2113;
		((Class282_Sub40) this).anInt8006 = 1277796829 * ((Class282_Sub40) this).aClass172_8009.anInt2117;
		((Class282_Sub40) this).anInt8008 = -1663966421 * ((Class282_Sub40) this).aClass172_8009.anInt2118;
		if (((Class282_Sub40) this).aClass172_8009.aClass384_2116 != null)
			((Class282_Sub40) this).aClass172_8009.aClass384_2116.method6614((float) (-161425851 * ((Class282_Sub40) this).aClass345_8007.anInt4027), (float) (-365865779 * ((Class282_Sub40) this).aClass345_8007.anInt4036), (float) (1487656629 * ((Class282_Sub40) this).aClass345_8007.anInt4037), aFloatArray8013);
		((Class282_Sub40) this).aFloat8011 = aFloatArray8013[0];
		((Class282_Sub40) this).aFloat8012 = aFloatArray8013[2];
	}

	void method13299() {
		((Class282_Sub40) this).anInt8010 = -1814601513 * ((Class282_Sub40) this).aClass172_8009.anInt2113;
		((Class282_Sub40) this).anInt8006 = 1277796829 * ((Class282_Sub40) this).aClass172_8009.anInt2117;
		((Class282_Sub40) this).anInt8008 = -1663966421 * ((Class282_Sub40) this).aClass172_8009.anInt2118;
		if (((Class282_Sub40) this).aClass172_8009.aClass384_2116 != null)
			((Class282_Sub40) this).aClass172_8009.aClass384_2116.method6614((float) (-161425851 * ((Class282_Sub40) this).aClass345_8007.anInt4027), (float) (-365865779 * ((Class282_Sub40) this).aClass345_8007.anInt4036), (float) (1487656629 * ((Class282_Sub40) this).aClass345_8007.anInt4037), aFloatArray8013);
		((Class282_Sub40) this).aFloat8011 = aFloatArray8013[0];
		((Class282_Sub40) this).aFloat8012 = aFloatArray8013[2];
	}

	Class282_Sub40(Class172 class172, Class539 class539) {
		((Class282_Sub40) this).aClass172_8009 = class172;
		((Class282_Sub40) this).aClass345_8007 = ((Class282_Sub40) this).aClass172_8009.method2913(-1334854713);
		method13298((byte) -35);
	}

	public static void method13300(int i) {
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub27_8208), 2, 1787860729);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub27_8209), 2, -262111418);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub23_8202), 1, 805134423);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub17_8200), 1, 901728448);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub15_8203), 1, 1356970908);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub26_8224), 1, -2135009160);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub28_8212), 1, -1797604906);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub22_8213), 1, -1288035070);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub2_8205), 1, -1070811454);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub19_8204), 1, 1324855689);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub24_8216), 0, -1873038629);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub30_8194), 1, 161881697);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub4_8187), 0, -160088461);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub4_8223), 0, -27348328);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub20_8207), 1, 989070776);
		Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub1_8197, Class106.aClass106_1075.anInt1071 * -530599889, 259124869);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub12_8195), 0, 1402342862);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub14_8211), 1, -1616300201);
		Class323.method5777(1459972295);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub7_8210), 1, 100643539);
		Class393.aClass282_Sub54_4783.method13511((Class393.aClass282_Sub54_4783.aClass468_Sub6_8192), 3, -1425167451);
		Class94.method1589((short) 255);
		client.aClass257_7353.method4435((byte) 1).method4048(2045600476);
		client.aBool7185 = true;
	}

	static final void method13301(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class393.aClass282_Sub54_4783.aClass468_Sub15_8203.method12739(-118386335);
	}
}
