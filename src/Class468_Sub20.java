/* Class468_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class468_Sub20 extends Class468 {
	public static final int anInt7917 = 1;
	public static final int anInt7918 = 2;
	public static final int anInt7919 = 0;
	static int[] anIntArray7920;

	int method7787() {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(1185241423) < 245)
			return 0;
		return 2;
	}

	public Class468_Sub20(int i, Class282_Sub54 class282_sub54) {
		super(i, class282_sub54);
		Class15.method540(anInt5578 * -859024475, (byte) 115);
	}

	public void method12793(int i) {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(-1348501274) < 245)
			anInt5578 = 0;
		if (anInt5578 * -859024475 < 0 || anInt5578 * -859024475 > 2)
			anInt5578 = method7781(2034586631) * -754033619;
	}

	int method7781(int i) {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(-1989714120) < 245)
			return 0;
		return 2;
	}

	public int method12794(int i) {
		return -859024475 * anInt5578;
	}

	public int method7785(int i, int i_0_) {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(1512170398) < 245)
			return 3;
		return 1;
	}

	void method7783(int i, int i_1_) {
		anInt5578 = -754033619 * i;
		Class15.method540(anInt5578 * -859024475, (byte) 23);
	}

	public void method12795() {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(-1757099344) < 245)
			anInt5578 = 0;
		if (anInt5578 * -859024475 < 0 || anInt5578 * -859024475 > 2)
			anInt5578 = method7781(1718984350) * -754033619;
	}

	public int method12796() {
		return -859024475 * anInt5578;
	}

	void method7780(int i) {
		anInt5578 = -754033619 * i;
		Class15.method540(anInt5578 * -859024475, (byte) 31);
	}

	int method7786() {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(1353747708) < 245)
			return 0;
		return 2;
	}

	public boolean method12797(byte i) {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(335245917) < 245)
			return false;
		return true;
	}

	public int method7784(int i) {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(-672863929) < 245)
			return 3;
		return 1;
	}

	public void method12798() {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(1215473891) < 245)
			anInt5578 = 0;
		if (anInt5578 * -859024475 < 0 || anInt5578 * -859024475 > 2)
			anInt5578 = method7781(2114975887) * -754033619;
	}

	public boolean method12799() {
		if (aClass282_Sub54_5581.method13504((byte) 1).method7721(-1891678777) < 245)
			return false;
		return true;
	}

	public int method12800() {
		return -859024475 * anInt5578;
	}

	public int method12801() {
		return -859024475 * anInt5578;
	}

	public Class468_Sub20(Class282_Sub54 class282_sub54) {
		super(class282_sub54);
		Class15.method540(anInt5578 * -859024475, (byte) 39);
	}

	static final void method12802(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class262.method4651(class118, class98, class527, (byte) 56);
	}

	static final void method12803(Class527 class527, int i) {
		Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub30_8194, ((((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]) == 1) ? 1 : 0, -204242656);
		client.aClass257_7353.method4547((byte) -96);
		Class190.method3148((byte) 37);
		client.aBool7175 = false;
	}

	static final void method12804(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = (((Class527) class527).anIntArray7018[((Class527) class527).anInt7020 * 301123709]);
	}

	static short[][] method12805(float[][] fs, short[][] is, int i) {
		for (int i_2_ = 0; i_2_ < fs.length; i_2_++) {
			for (int i_3_ = 0; i_3_ < is[i_2_].length; i_3_++)
				is[i_2_][i_3_] = (short) (int) (16383.0F * fs[i_2_][i_3_]);
		}
		return is;
	}

	public static void method12806(Class317 class317, byte i) {
		Class165.anInt2035 = class317.method5610("headicons_pk", 1525500729) * -821889283;
		Class475.anInt5622 = class317.method5610("headicons_prayer", -270805148) * -2055869675;
		Class16.anInt135 = class317.method5610("hint_headicons", 1408398760) * -1041729095;
		Class16.anInt142 = class317.method5610("hint_mapmarkers", -351887994) * 850992921;
		Class16.anInt137 = class317.method5610("mapflag", 1460718602) * 1055165697;
		Class468_Sub12.anInt7899 = class317.method5610("cross", -1941091457) * 395125199;
		Class13.anInt130 = class317.method5610("mapdots", 7471751) * 453785251;
		Class16.anInt143 = class317.method5610("name_icons", -134716230) * -715854365;
		Class400.anInt4821 = class317.method5610("floorshadows", -1977448379) * 237532063;
		Class16.anInt141 = class317.method5610("compass", -1914692488) * 1354470119;
		Class165.anInt2037 = class317.method5610("otherlevel", -2135564190) * -1341930679;
		Class271.anInt3330 = class317.method5610("hint_mapedge", -1418586709) * 1834682789;
	}

	public static Class184 method12807(int i) {
		if (Class282_Sub17.method12259(-1741204137 * client.anInt7166, -1194987033))
			return client.aClass184_7218;
		return client.aClass184_7475;
	}

	public static void method12808(int i) {
		if (null == Class302.aClass387Array3557) {
			Class302.aClass387Array3557 = Class387.method6676(926734878);
			Class474.aClass387_5621 = Class302.aClass387Array3557[0];
			Class302.aLong3562 = Class169.method2869(1526371561) * -5491689582512610839L;
		}
		if (Class328.aClass306_3771 == null)
			Class165.method2852(-536830752);
		Class387 class387 = Class474.aClass387_5621;
		int i_4_ = Class76.method1359((byte) 82);
		if (Class474.aClass387_5621 == class387) {
			Class380.aString4636 = ((Class387) Class474.aClass387_5621).aClass433_4711.method7273(Class223.aClass495_2772, -1559474593);
			if (((Class387) Class474.aClass387_5621).aBool4718)
				Class302.anInt3563 = (-1332863859 * (((586831783 * ((Class387) Class474.aClass387_5621).anInt4703) - (((Class387) Class474.aClass387_5621).anInt4715 * 2033966327)) * i_4_ / 100 + 2033966327 * (((Class387) Class474.aClass387_5621).anInt4715)));
			if (((Class387) Class474.aClass387_5621).aBool4694)
				Class380.aString4636 = new StringBuilder().append(Class380.aString4636).append(-723319227 * Class302.anInt3563).append("%").toString();
		} else if (Class474.aClass387_5621 == Class387.aClass387_4701) {
			Class328.aClass306_3771 = null;
			Class365.method6298(5, 2138325476);
		} else {
			Class380.aString4636 = ((Class387) class387).aClass433_4714.method7273(Class223.aClass495_2772, -2022596623);
			if (((Class387) Class474.aClass387_5621).aBool4694)
				Class380.aString4636 = new StringBuilder().append(Class380.aString4636).append(((Class387) class387).anInt4703 * 586831783).append("%").toString();
			Class302.anInt3563 = ((Class387) class387).anInt4703 * 337283835;
			if (((Class387) Class474.aClass387_5621).aBool4718 || ((Class387) class387).aBool4718)
				Class302.aLong3562 = Class169.method2869(1526854933) * -5491689582512610839L;
		}
		if (null != Class328.aClass306_3771) {
			Class328.aClass306_3771.method5422((5158573110282126937L * Class302.aLong3562), Class380.aString4636, -723319227 * Class302.anInt3563, Class474.aClass387_5621, -361143334);
			if (null != Class302.anInterface27Array3559) {
				for (int i_5_ = 1 + 1461682935 * Class302.anInt3560; i_5_ < Class302.anInterface27Array3559.length; i_5_++) {
					if (Class302.anInterface27Array3559[i_5_].method191(1666965000) >= 100 && Class302.anInt3560 * 1461682935 == i_5_ - 1 && 4 != -1741204137 * client.anInt7166 && Class328.aClass306_3771.method5450(1945339550)) {
						try {
							Class302.anInterface27Array3559[i_5_].method161(-1145558933);
						} catch (Exception exception) {
							Class302.anInterface27Array3559 = null;
							break;
						}
						Class328.aClass306_3771.method5445(Class302.anInterface27Array3559[i_5_], (byte) 64);
						Class302.anInt3560 += 432660167;
						if ((Class302.anInt3560 * 1461682935 >= Class302.anInterface27Array3559.length - 1) && Class302.anInterface27Array3559.length > 1)
							Class302.anInt3560 = (Class275_Sub7.aClass400_7862.method6790(512238595) ? 0 : -1) * 432660167;
					}
				}
			}
		}
	}
}
