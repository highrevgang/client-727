/* Class453 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class453 {
	int anInt5445;
	Class282[] aClass282Array5446;
	long aLong5447;
	Class282 aClass282_5448;
	static int[] anIntArray5449;

	public Class282 method7529() {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_0_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_0_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public Class282 method7530(long l) {
		((Class453) this).aLong5447 = -3885405380164673631L * l;
		Class282 class282 = (((Class453) this).aClass282Array5446[(int) (l & (long) (948624889 * ((Class453) this).anInt5445 - 1))]);
		for (((Class453) this).aClass282_5448 = class282.aClass282_3378; ((Class453) this).aClass282_5448 != class282; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if (l == (((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L)) {
				Class282 class282_1_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_1_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public Class282 method7531() {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_2_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_2_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public int method7532(Class282[] class282s, byte i) {
		int i_3_ = 0;
		for (int i_4_ = 0; i_4_ < ((Class453) this).anInt5445 * 948624889; i_4_++) {
			Class282 class282 = ((Class453) this).aClass282Array5446[i_4_];
			for (Class282 class282_5_ = class282.aClass282_3378; class282 != class282_5_; class282_5_ = class282_5_.aClass282_3378)
				class282s[i_3_++] = class282_5_;
		}
		return i_3_;
	}

	public Class282 method7533() {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_6_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_6_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public void method7534(Class282 class282, long l) {
		if (null != class282.aClass282_3380)
			class282.method4991(-371378792);
		Class282 class282_7_ = (((Class453) this).aClass282Array5446[(int) (l & (long) (948624889 * ((Class453) this).anInt5445 - 1))]);
		class282.aClass282_3380 = class282_7_.aClass282_3380;
		class282.aClass282_3378 = class282_7_;
		class282.aClass282_3380.aClass282_3378 = class282;
		class282.aClass282_3378.aClass282_3380 = class282;
		class282.aLong3379 = l * -1253863389874800229L;
	}

	public Class282 method7535(long l) {
		((Class453) this).aLong5447 = -3885405380164673631L * l;
		Class282 class282 = (((Class453) this).aClass282Array5446[(int) (l & (long) (948624889 * ((Class453) this).anInt5445 - 1))]);
		for (((Class453) this).aClass282_5448 = class282.aClass282_3378; ((Class453) this).aClass282_5448 != class282; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if (l == (((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L)) {
				Class282 class282_8_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_8_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public Class282 method7536(long l) {
		((Class453) this).aLong5447 = -3885405380164673631L * l;
		Class282 class282 = (((Class453) this).aClass282Array5446[(int) (l & (long) (948624889 * ((Class453) this).anInt5445 - 1))]);
		for (((Class453) this).aClass282_5448 = class282.aClass282_3378; ((Class453) this).aClass282_5448 != class282; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if (l == (((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L)) {
				Class282 class282_9_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_9_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public Class282 method7537() {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_10_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_10_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public Class282 method7538() {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_11_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_11_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public Class282 method7539() {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_12_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_12_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public int method7540(int i) {
		int i_13_ = 0;
		for (int i_14_ = 0; i_14_ < ((Class453) this).anInt5445 * 948624889; i_14_++) {
			Class282 class282 = ((Class453) this).aClass282Array5446[i_14_];
			for (Class282 class282_15_ = class282.aClass282_3378; class282 != class282_15_; class282_15_ = class282_15_.aClass282_3378)
				i_13_++;
		}
		return i_13_;
	}

	public Class453(int i) {
		((Class453) this).anInt5445 = 1146704969 * i;
		((Class453) this).aClass282Array5446 = new Class282[i];
		for (int i_16_ = 0; i_16_ < i; i_16_++) {
			Class282 class282 = ((Class453) this).aClass282Array5446[i_16_] = new Class282();
			class282.aClass282_3378 = class282;
			class282.aClass282_3380 = class282;
		}
	}

	public Class282 method7541() {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_17_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_17_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public int method7542(Class282[] class282s) {
		int i = 0;
		for (int i_18_ = 0; i_18_ < ((Class453) this).anInt5445 * 948624889; i_18_++) {
			Class282 class282 = ((Class453) this).aClass282Array5446[i_18_];
			for (Class282 class282_19_ = class282.aClass282_3378; class282 != class282_19_; class282_19_ = class282_19_.aClass282_3378)
				class282s[i++] = class282_19_;
		}
		return i;
	}

	public int method7543(Class282[] class282s) {
		int i = 0;
		for (int i_20_ = 0; i_20_ < ((Class453) this).anInt5445 * 948624889; i_20_++) {
			Class282 class282 = ((Class453) this).aClass282Array5446[i_20_];
			for (Class282 class282_21_ = class282.aClass282_3378; class282 != class282_21_; class282_21_ = class282_21_.aClass282_3378)
				class282s[i++] = class282_21_;
		}
		return i;
	}

	public Class282 method7544(int i) {
		if (null == ((Class453) this).aClass282_5448)
			return null;
		for (Class282 class282 = (((Class453) this).aClass282Array5446[(int) (((Class453) this).aLong5447 * -1090233634234760095L & (long) (948624889 * ((Class453) this).anInt5445 - 1))]); class282 != ((Class453) this).aClass282_5448; ((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378) {
			if ((((Class453) this).aClass282_5448.aLong3379 * -3442165056282524525L) == ((Class453) this).aLong5447 * -1090233634234760095L) {
				Class282 class282_22_ = ((Class453) this).aClass282_5448;
				((Class453) this).aClass282_5448 = ((Class453) this).aClass282_5448.aClass282_3378;
				return class282_22_;
			}
		}
		((Class453) this).aClass282_5448 = null;
		return null;
	}

	public int method7545() {
		int i = 0;
		for (int i_23_ = 0; i_23_ < ((Class453) this).anInt5445 * 948624889; i_23_++) {
			Class282 class282 = ((Class453) this).aClass282Array5446[i_23_];
			for (Class282 class282_24_ = class282.aClass282_3378; class282 != class282_24_; class282_24_ = class282_24_.aClass282_3378)
				i++;
		}
		return i;
	}

	public int method7546() {
		int i = 0;
		for (int i_25_ = 0; i_25_ < ((Class453) this).anInt5445 * 948624889; i_25_++) {
			Class282 class282 = ((Class453) this).aClass282Array5446[i_25_];
			for (Class282 class282_26_ = class282.aClass282_3378; class282 != class282_26_; class282_26_ = class282_26_.aClass282_3378)
				i++;
		}
		return i;
	}

	static final void method7547(Class118 class118, Class98 class98, Class527 class527, int i) {
		class118.aString1348 = (String) (((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 -= 1476624725) * 1806726141)]);
	}
}
