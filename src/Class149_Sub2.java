/* Class149_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class149_Sub2 extends Class149 {
	int anInt9305;
	int anInt9306;
	int anInt9307;
	int anInt9308;
	int anInt9309;
	static final int anInt9310 = 1;
	int anInt9311;
	int anInt9312;
	int anInt9313;
	public static Class511 aClass511_9314;
	public static Class160 aClass160_9315;
	public static Class461 aClass461_9316;

	void method2560(int i, int i_0_) {
		/* empty */
	}

	static Class149_Sub2 method14607(RsByteBuffer class282_sub35) {
		return new Class149_Sub2(class282_sub35.method13081(2003141552), class282_sub35.method13081(2055658374), class282_sub35.method13081(1667236096), class282_sub35.method13081(2139929831), class282_sub35.method13081(1862763469), class282_sub35.method13081(1819463410), class282_sub35.method13081(1592654655), class282_sub35.method13081(1671590886), class282_sub35.method13082((short) 24536), class282_sub35.readUnsignedByte());
	}

	void method2556(int i, int i_1_, int i_2_) {
		/* empty */
	}

	void method2557(int i, int i_3_, byte i_4_) {
		/* empty */
	}

	Class149_Sub2(int i, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_) {
		super(-1, i_12_, i_13_);
		((Class149_Sub2) this).anInt9306 = -1663850125 * i;
		((Class149_Sub2) this).anInt9307 = i_5_ * 489790669;
		((Class149_Sub2) this).anInt9305 = i_6_ * 1559657947;
		((Class149_Sub2) this).anInt9309 = -1469754047 * i_7_;
		((Class149_Sub2) this).anInt9308 = i_8_ * -590934233;
		((Class149_Sub2) this).anInt9311 = i_9_ * -1674872431;
		((Class149_Sub2) this).anInt9312 = i_10_ * -1310894617;
		((Class149_Sub2) this).anInt9313 = 745027993 * i_11_;
	}

	void method2561(int i, int i_14_, int i_15_) {
		int i_16_ = ((Class149_Sub2) this).anInt9306 * -574250053 * i >> 12;
		int i_17_ = ((Class149_Sub2) this).anInt9307 * 724021253 * i_14_ >> 12;
		int i_18_ = i * (-1184156077 * ((Class149_Sub2) this).anInt9305) >> 12;
		int i_19_ = i_14_ * (((Class149_Sub2) this).anInt9309 * -1849017663) >> 12;
		int i_20_ = ((Class149_Sub2) this).anInt9308 * -108826473 * i >> 12;
		int i_21_ = i_14_ * (1636198257 * ((Class149_Sub2) this).anInt9311) >> 12;
		int i_22_ = i * (((Class149_Sub2) this).anInt9312 * 905637335) >> 12;
		int i_23_ = ((Class149_Sub2) this).anInt9313 * 264374953 * i_14_ >> 12;
		Class282_Sub24.method12399(i_16_, i_17_, i_18_, i_19_, i_20_, i_21_, i_22_, i_23_, (-1525176857 * ((Class149_Sub2) this).anInt1741), -2112758099);
	}

	void method2558(int i, int i_24_) {
		/* empty */
	}

	static Class149_Sub2 method14608(RsByteBuffer class282_sub35) {
		return new Class149_Sub2(class282_sub35.method13081(1998911843), class282_sub35.method13081(1983145739), class282_sub35.method13081(2049498865), class282_sub35.method13081(1897994004), class282_sub35.method13081(1836323433), class282_sub35.method13081(1953394053), class282_sub35.method13081(1576305204), class282_sub35.method13081(1947146012), class282_sub35.method13082((short) 28889), class282_sub35.readUnsignedByte());
	}

	void method2562(int i, int i_25_) {
		/* empty */
	}

	void method2555(int i, int i_26_) {
		int i_27_ = ((Class149_Sub2) this).anInt9306 * -574250053 * i >> 12;
		int i_28_ = ((Class149_Sub2) this).anInt9307 * 724021253 * i_26_ >> 12;
		int i_29_ = i * (-1184156077 * ((Class149_Sub2) this).anInt9305) >> 12;
		int i_30_ = i_26_ * (((Class149_Sub2) this).anInt9309 * -1849017663) >> 12;
		int i_31_ = ((Class149_Sub2) this).anInt9308 * -108826473 * i >> 12;
		int i_32_ = i_26_ * (1636198257 * ((Class149_Sub2) this).anInt9311) >> 12;
		int i_33_ = i * (((Class149_Sub2) this).anInt9312 * 905637335) >> 12;
		int i_34_ = ((Class149_Sub2) this).anInt9313 * 264374953 * i_26_ >> 12;
		Class282_Sub24.method12399(i_27_, i_28_, i_29_, i_30_, i_31_, i_32_, i_33_, i_34_, (-1525176857 * ((Class149_Sub2) this).anInt1741), -2120176712);
	}

	void method2559(int i, int i_35_) {
		int i_36_ = ((Class149_Sub2) this).anInt9306 * -574250053 * i >> 12;
		int i_37_ = ((Class149_Sub2) this).anInt9307 * 724021253 * i_35_ >> 12;
		int i_38_ = i * (-1184156077 * ((Class149_Sub2) this).anInt9305) >> 12;
		int i_39_ = i_35_ * (((Class149_Sub2) this).anInt9309 * -1849017663) >> 12;
		int i_40_ = ((Class149_Sub2) this).anInt9308 * -108826473 * i >> 12;
		int i_41_ = i_35_ * (1636198257 * ((Class149_Sub2) this).anInt9311) >> 12;
		int i_42_ = i * (((Class149_Sub2) this).anInt9312 * 905637335) >> 12;
		int i_43_ = ((Class149_Sub2) this).anInt9313 * 264374953 * i_35_ >> 12;
		Class282_Sub24.method12399(i_36_, i_37_, i_38_, i_39_, i_40_, i_41_, i_42_, i_43_, (-1525176857 * ((Class149_Sub2) this).anInt1741), -2131700490);
	}

	public static void method14609(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1, int i) {
		Class282_Sub48 class282_sub48 = ((Class282_Sub48) (Class282_Sub48.aClass465_8075.method7754((long) (class521_sub1_sub1_sub2_sub1.anInt10314 * -1691508299))));
		if (class282_sub48 != null) {
			if (((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 != null) {
				Class79.aClass282_Sub15_Sub4_783.method15276(((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099);
				((Class282_Sub48) class282_sub48).aClass282_Sub15_Sub5_8099 = null;
			}
			class282_sub48.method4991(-371378792);
		}
	}

	static int method14610(Class521_Sub1_Sub1_Sub2_Sub1 class521_sub1_sub1_sub2_sub1, int i) {
		int i_44_ = -1104078683 * class521_sub1_sub1_sub2_sub1.anInt10572;
		Class227 class227 = class521_sub1_sub1_sub2_sub1.method15855((byte) -17);
		int i_45_ = class521_sub1_sub1_sub2_sub1.aClass456_Sub3_10337.method7597(-1192629729);
		if (-1 == i_45_ || class521_sub1_sub1_sub2_sub1.aClass456_Sub3_10337.aBool7891)
			i_44_ = 921552681 * class521_sub1_sub1_sub2_sub1.anInt10560;
		else if (i_45_ == class227.anInt2797 * -474675041 || i_45_ == 486149589 * class227.anInt2817 || i_45_ == class227.anInt2800 * -833477781 || 1642803439 * class227.anInt2799 == i_45_)
			i_44_ = class521_sub1_sub1_sub2_sub1.anInt10574 * -1965967729;
		else if (i_45_ == 2055956425 * class227.anInt2801 || i_45_ == 1053306035 * class227.anInt2828 || class227.anInt2812 * 1489597113 == i_45_ || 1879075923 * class227.anInt2803 == i_45_)
			i_44_ = -1465525213 * class521_sub1_sub1_sub2_sub1.anInt10547;
		return i_44_;
	}
}
