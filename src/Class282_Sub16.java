/* Class282_Sub16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub16 extends Class282 {
	int anInt7602;
	int anInt7603;

	Class282_Sub16(int i, int i_0_) {
		((Class282_Sub16) this).anInt7603 = i * -746120135;
		((Class282_Sub16) this).anInt7602 = i_0_ * 1466310595;
	}

	static boolean method12248(Class220 class220, int i) {
		if (null == class220)
			return false;
		if (!class220.aBool2728)
			return false;
		if (!class220.method3719(Class291_Sub1.anInterface42_3458, 1626898528))
			return false;
		if (Class291_Sub1.aClass465_8025.method7754((long) (class220.anInt2753 * -813142717)) != null)
			return false;
		if (Class291_Sub1.aClass465_8029.method7754((long) (781329827 * class220.anInt2718)) != null)
			return false;
		return true;
	}
}
