/* Class446 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class446 {
	public static Class446 aClass446_5385;
	public static Class446 aClass446_5386 = new Class446(Class445.aClass445_5381);
	public static Class446 aClass446_5387;
	public static Class446 aClass446_5388;
	public static Class446 aClass446_5389;
	public static Class446 aClass446_5390;
	public static Class446 aClass446_5391;
	public static Class446 aClass446_5392;
	public static Class446 aClass446_5393;
	public static Class446 aClass446_5394;
	public static Class446 aClass446_5395;
	public static int anInt5396;
	public static Class446 aClass446_5397;
	public static Class446 aClass446_5398;
	public static Class446 aClass446_5399;
	public static Class446 aClass446_5400;
	public static Class446 aClass446_5401;
	public static Class446 aClass446_5402;
	public static Class446 aClass446_5403;
	public static Class446 aClass446_5404;
	public static Class446 aClass446_5405;
	Class445 aClass445_5406;
	public static Class446 aClass446_5407;
	int anInt5408;
	public static Class446 aClass446_5409;
	public static Class446 aClass446_5410;
	public static Class446 aClass446_5411;
	public static Class446 aClass446_5412 = new Class446(Class445.aClass445_5383);
	public static Class446 aClass446_5413;
	Interface41 anInterface41_5414;
	public static Class446 aClass446_5415;
	static Class194 aClass194_5416;

	public static void method7430(Class282 class282, Class282 class282_0_, int i) {
		if (class282.aClass282_3380 != null)
			class282.method4991(-371378792);
		class282.aClass282_3380 = class282_0_.aClass282_3380;
		class282.aClass282_3378 = class282_0_;
		class282.aClass282_3380.aClass282_3378 = class282;
		class282.aClass282_3378.aClass282_3380 = class282;
	}

	public void method7431(int i) {
		((Class446) this).anInt5408 = i * -1530265891;
	}

	public int method7432(byte i) {
		return ((Class446) this).anInt5408 * -1137718923;
	}

	public static Class446[] method7433() {
		return (new Class446[] { aClass446_5386, aClass446_5412, aClass446_5387, aClass446_5388, aClass446_5389, aClass446_5415, aClass446_5385, aClass446_5392, aClass446_5390, aClass446_5394, aClass446_5395, aClass446_5397, aClass446_5393, aClass446_5413, aClass446_5399, aClass446_5400, aClass446_5401, aClass446_5402, aClass446_5403, aClass446_5404, aClass446_5405, aClass446_5410, aClass446_5407, aClass446_5391, aClass446_5409, aClass446_5398, aClass446_5411 });
	}

	static void method7434(Class527 class527, short i) {
		((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 1] = (Class409.aClass242_4922.method4156((((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 - 1]), -1396181317).anInt2960) * -1031162511;
	}

	public void method7435(Interface41 interface41, byte i) {
		if (interface41.method232((byte) -115) != ((Class446) this).aClass445_5406)
			throw new IllegalArgumentException();
		((Class446) this).anInterface41_5414 = interface41;
	}

	static {
		aClass446_5387 = new Class446(Class445.aClass445_5383);
		aClass446_5388 = new Class446(Class445.aClass445_5383);
		aClass446_5389 = new Class446(Class445.aClass445_5383);
		aClass446_5415 = new Class446(Class445.aClass445_5383);
		aClass446_5385 = new Class446(Class445.aClass445_5383);
		aClass446_5392 = new Class446(Class445.aClass445_5381);
		aClass446_5390 = new Class446(Class445.aClass445_5381);
		aClass446_5394 = new Class446(Class445.aClass445_5381);
		aClass446_5395 = new Class446(Class445.aClass445_5381);
		aClass446_5397 = new Class446(Class445.aClass445_5381);
		aClass446_5393 = new Class446(Class445.aClass445_5381);
		aClass446_5413 = new Class446(Class445.aClass445_5381);
		aClass446_5399 = new Class446(Class445.aClass445_5381);
		aClass446_5400 = new Class446(Class445.aClass445_5381);
		aClass446_5401 = new Class446(Class445.aClass445_5381);
		aClass446_5402 = new Class446(Class445.aClass445_5381);
		aClass446_5403 = new Class446(Class445.aClass445_5381);
		aClass446_5404 = new Class446(Class445.aClass445_5381);
		aClass446_5405 = new Class446(Class445.aClass445_5381);
		aClass446_5410 = new Class446(Class445.aClass445_5381);
		aClass446_5407 = new Class446(Class445.aClass445_5380);
		aClass446_5391 = new Class446(Class445.aClass445_5381);
		aClass446_5409 = new Class446(Class445.aClass445_5381);
		aClass446_5398 = new Class446(Class445.aClass445_5381);
		aClass446_5411 = new Class446(Class445.aClass445_5382);
	}

	public static Class446[] method7436(byte i) {
		return (new Class446[] { aClass446_5386, aClass446_5412, aClass446_5387, aClass446_5388, aClass446_5389, aClass446_5415, aClass446_5385, aClass446_5392, aClass446_5390, aClass446_5394, aClass446_5395, aClass446_5397, aClass446_5393, aClass446_5413, aClass446_5399, aClass446_5400, aClass446_5401, aClass446_5402, aClass446_5403, aClass446_5404, aClass446_5405, aClass446_5410, aClass446_5407, aClass446_5391, aClass446_5409, aClass446_5398, aClass446_5411 });
	}

	public static Class446[] method7437() {
		return (new Class446[] { aClass446_5386, aClass446_5412, aClass446_5387, aClass446_5388, aClass446_5389, aClass446_5415, aClass446_5385, aClass446_5392, aClass446_5390, aClass446_5394, aClass446_5395, aClass446_5397, aClass446_5393, aClass446_5413, aClass446_5399, aClass446_5400, aClass446_5401, aClass446_5402, aClass446_5403, aClass446_5404, aClass446_5405, aClass446_5410, aClass446_5407, aClass446_5391, aClass446_5409, aClass446_5398, aClass446_5411 });
	}

	public int method7438() {
		return ((Class446) this).anInt5408 * -1137718923;
	}

	public void method7439(int i, short i_1_) {
		((Class446) this).anInt5408 = i * -1530265891;
	}

	public Interface41 method7440() {
		return ((Class446) this).anInterface41_5414;
	}

	public void method7441(int i) {
		((Class446) this).anInt5408 = i * -1530265891;
	}

	Class446(Class445 class445) {
		((Class446) this).aClass445_5406 = class445;
		((Class446) this).anInt5408 = -1530265891;
	}

	public void method7442(Interface41 interface41) {
		if (interface41.method232((byte) -54) != ((Class446) this).aClass445_5406)
			throw new IllegalArgumentException();
		((Class446) this).anInterface41_5414 = interface41;
	}

	public Interface41 method7443(int i) {
		return ((Class446) this).anInterface41_5414;
	}

	static final void method7444(Class527 class527, int i) {
		int i_2_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = client.anIntArray7338[i_2_];
	}

	static final void method7445(Class527 class527, int i) {
		int i_3_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = (Class119.aClass426_1463.method7145(i_3_, 1828860210).anInt5047 * 1718037007);
	}

	public void method7446(int i) {
		((Class446) this).anInt5408 = i * -1530265891;
	}

	public static void method7447(String string, boolean bool, Class505 class505, Class8 class8, Class414 class414, byte i) {
		boolean bool_4_ = !Class174.aBool2135 || Class174.method2954(1454743484);
		if (bool_4_) {
			if (Class174.aBool2135 && bool_4_) {
				class414 = Class197.aClass414_2436;
				class8 = class505.method8448(class414, Class174.aClass91Array2132, true);
				int i_5_ = class414.method6951(string, 250, null, (byte) 127);
				int i_6_ = class414.method6972(string, 250, 1200654985 * class414.anInt4975, null, 369575167);
				int i_7_ = Class174.aClass91_2138.anInt957;
				int i_8_ = 4 + i_7_;
				i_5_ += i_8_ * 2;
				i_6_ += i_8_ * 2;
				if (i_5_ < -1424630769 * Class174.anInt2136)
					i_5_ = -1424630769 * Class174.anInt2136;
				if (i_6_ < Class208.anInt2662 * 15328729)
					i_6_ = Class208.anInt2662 * 15328729;
				int i_9_ = (Class174.aClass356_2139.method6221(i_5_, (client.anInt7439 * 150480619), 1961089623) + Class174.anInt2134 * 1169073525);
				int i_10_ = (Class174.aClass353_2140.method6198(i_6_, (client.anInt7312 * 1176039023), 1070310069) + -347742909 * Class174.anInt2133);
				class505.method8444(Class242.aClass91_2981, false).method2772(Class174.aClass91_2141.anInt957 + i_9_, Class174.aClass91_2141.anInt954 + i_10_, i_5_ - Class174.aClass91_2141.anInt957 * 2, i_6_ - 2 * Class174.aClass91_2141.anInt954, 1, 0, 0);
				class505.method8444(Class174.aClass91_2141, true).method2752(i_9_, i_10_);
				Class174.aClass91_2141.method1525();
				class505.method8444(Class174.aClass91_2141, true).method2752(i_5_ + i_9_ - i_7_, i_10_);
				Class174.aClass91_2141.method1526();
				class505.method8444(Class174.aClass91_2141, true).method2752(i_5_ + i_9_ - i_7_, i_6_ + i_10_ - i_7_);
				Class174.aClass91_2141.method1525();
				class505.method8444(Class174.aClass91_2141, true).method2752(i_9_, i_6_ + i_10_ - i_7_);
				Class174.aClass91_2141.method1526();
				class505.method8444(Class174.aClass91_2138, true).method2756(i_9_, Class174.aClass91_2141.anInt954 + i_10_, i_7_, i_6_ - Class174.aClass91_2141.anInt954 * 2);
				Class174.aClass91_2138.method1527();
				class505.method8444(Class174.aClass91_2138, true).method2756(Class174.aClass91_2141.anInt957 + i_9_, i_10_, i_5_ - Class174.aClass91_2141.anInt957 * 2, i_7_);
				Class174.aClass91_2138.method1527();
				class505.method8444(Class174.aClass91_2138, true).method2756(i_9_ + i_5_ - i_7_, i_10_ + Class174.aClass91_2141.anInt954, i_7_, i_6_ - 2 * Class174.aClass91_2141.anInt954);
				Class174.aClass91_2138.method1527();
				class505.method8444(Class174.aClass91_2138, true).method2756(Class174.aClass91_2141.anInt957 + i_9_, i_6_ + i_10_ - i_7_, i_5_ - 2 * Class174.aClass91_2141.anInt957, i_7_);
				Class174.aClass91_2138.method1527();
				class8.method378(string, i_9_ + i_8_, i_10_ + i_8_, i_5_ - i_8_ * 2, i_6_ - 2 * i_8_, Class434.anInt5329 * 1549061485 | ~0xffffff, -1, 1, 1, 0, null, null, null, 0, 0, (byte) 36);
				Class292.method5201(i_9_, i_10_, i_5_, i_6_, (byte) 12);
			} else {
				int i_11_ = class414.method6951(string, 250, null, (byte) 107);
				int i_12_ = class414.method6949(string, 250, null, 437013959) * 13;
				int i_13_ = 4;
				int i_14_ = i_13_ + 6;
				int i_15_ = 6 + i_13_;
				class505.B(i_14_ - i_13_, i_15_ - i_13_, i_13_ + i_11_ + i_13_, i_12_ + i_13_ + i_13_, -16777216, 0);
				class505.method8430(i_14_ - i_13_, i_15_ - i_13_, i_13_ + (i_11_ + i_13_), i_13_ + i_12_ + i_13_, -1, 0);
				class8.method378(string, i_14_, i_15_, i_11_, i_12_, -1, -1, 1, 1, 0, null, null, null, 0, 0, (byte) 14);
				Class292.method5201(i_14_ - i_13_, i_15_ - i_13_, i_13_ + i_11_ + i_13_, i_13_ + i_12_ + i_13_, (byte) 12);
			}
			if (bool) {
				try {
					class505.method8393((short) 2182);
				} catch (Exception_Sub3 exception_sub3) {
					/* empty */
				}
			}
		}
	}

	static final void method7448(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class393.aClass282_Sub54_4783.aClass468_Sub5_8221.method12651(-1091175329);
	}
}
