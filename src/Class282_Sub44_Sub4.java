/* Class282_Sub44_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub44_Sub4 extends Class282_Sub44 {
	Class530 aClass530_9564;

	public boolean method13402() {
		Interface12 interface12 = ((Class282_Sub44_Sub4) this).aClass530_9564.method11344((byte) 1);
		if (interface12 != null) {
			Class15.method549(Class397.aClass397_4808, 587626901 * anInt8063, -1, interface12, -479586155);
			return true;
		}
		return false;
	}

	public boolean method13401(byte i) {
		Interface12 interface12 = ((Class282_Sub44_Sub4) this).aClass530_9564.method11344((byte) 1);
		if (interface12 != null) {
			if (i >= -1)
				throw new IllegalStateException();
			Class15.method549(Class397.aClass397_4808, 587626901 * anInt8063, -1, interface12, -479586155);
			return true;
		}
		return false;
	}

	public Class282_Sub44_Sub4(int i, int i_0_, Class530 class530) {
		super(i, i_0_);
		((Class282_Sub44_Sub4) this).aClass530_9564 = class530;
	}

	public boolean method13403() {
		Interface12 interface12 = ((Class282_Sub44_Sub4) this).aClass530_9564.method11344((byte) 1);
		if (interface12 != null) {
			Class15.method549(Class397.aClass397_4808, 587626901 * anInt8063, -1, interface12, -479586155);
			return true;
		}
		return false;
	}
}
