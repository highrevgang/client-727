
/* Class380 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Class380 implements Iterator {
	AbstractQueue_Sub1 anAbstractQueue_Sub1_4633;
	int anInt4634 = 0;
	int anInt4635 = ((((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10073) * 259966515);
	static String aString4636;

	public Object next() {
		if ((((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10073) * 1751455793 != ((Class380) this).anInt4635 * 396827659)
			throw new ConcurrentModificationException();
		if (((Class380) this).anInt4634 * -626994753 < 1767048053 * (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070)) {
			Object object = (((Class354) (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).aClass354Array10072[-626994753 * ((Class380) this).anInt4634])).anObject4109);
			((Class380) this).anInt4634 += 1814568511;
			return object;
		}
		throw new NoSuchElementException();
	}

	public boolean hasNext() {
		return (-626994753 * ((Class380) this).anInt4634 < (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070 * 1767048053));
	}

	public boolean method6441() {
		return (-626994753 * ((Class380) this).anInt4634 < (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070 * 1767048053));
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}

	public void method6442() {
		throw new UnsupportedOperationException();
	}

	public boolean method6443() {
		return (-626994753 * ((Class380) this).anInt4634 < (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070 * 1767048053));
	}

	public boolean method6444() {
		return (-626994753 * ((Class380) this).anInt4634 < (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070 * 1767048053));
	}

	public Object method6445() {
		if ((((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10073) * 1751455793 != ((Class380) this).anInt4635 * 396827659)
			throw new ConcurrentModificationException();
		if (((Class380) this).anInt4634 * -626994753 < 1767048053 * (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070)) {
			Object object = (((Class354) (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).aClass354Array10072[-626994753 * ((Class380) this).anInt4634])).anObject4109);
			((Class380) this).anInt4634 += 1814568511;
			return object;
		}
		throw new NoSuchElementException();
	}

	public Object method6446() {
		if ((((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10073) * 1751455793 != ((Class380) this).anInt4635 * 396827659)
			throw new ConcurrentModificationException();
		if (((Class380) this).anInt4634 * -626994753 < 1767048053 * (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070)) {
			Object object = (((Class354) (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).aClass354Array10072[-626994753 * ((Class380) this).anInt4634])).anObject4109);
			((Class380) this).anInt4634 += 1814568511;
			return object;
		}
		throw new NoSuchElementException();
	}

	Class380(AbstractQueue_Sub1 abstractqueue_sub1) {
		((Class380) this).anAbstractQueue_Sub1_4633 = abstractqueue_sub1;
	}

	public Object method6447() {
		if ((((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10073) * 1751455793 != ((Class380) this).anInt4635 * 396827659)
			throw new ConcurrentModificationException();
		if (((Class380) this).anInt4634 * -626994753 < 1767048053 * (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).anInt10070)) {
			Object object = (((Class354) (((AbstractQueue_Sub1) ((Class380) this).anAbstractQueue_Sub1_4633).aClass354Array10072[-626994753 * ((Class380) this).anInt4634])).anObject4109);
			((Class380) this).anInt4634 += 1814568511;
			return object;
		}
		throw new NoSuchElementException();
	}

	public void method6448() {
		throw new UnsupportedOperationException();
	}

	static final void method6449(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = (1 == class118.anInt1329 * 2131324949 ? class118.anInt1330 * -402732635 : -1);
	}

	public static boolean method6450(char c, int i) {
		return (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z');
	}

	static void method6451(int i, int i_0_, int i_1_) {
		Class20.anInt180 = i * 797571487;
		Class20.anInt181 = -2139368457 * i_0_;
	}

	static final void method6452(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class295.method5291(class118, class98, class527, 989451427);
	}

	static final void method6453(Class118 class118, Class98 class98, Class527 class527, int i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		int[] is = Class96_Sub14.method14642(string, class527, 1022957959);
		if (is != null)
			string = string.substring(0, string.length() - 1);
		class118.anObjectArray1342 = Class351.method6193(string, class527, 720778855);
		class118.anIntArray1398 = is;
		class118.aBool1384 = true;
	}
}
