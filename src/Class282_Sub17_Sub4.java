/* Class282_Sub17_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub17_Sub4 extends Class282_Sub17 {
	int anInt9936;
	int anInt9937;
	int anInt9938;
	int anInt9939;
	Class348 this$0;
	public static int anInt9940;

	void method12255(Class61 class61) {
		class61.method1212(-31291083 * ((Class282_Sub17_Sub4) this).anInt9937, (-1425966289 * ((Class282_Sub17_Sub4) this).anInt9938), ((Class282_Sub17_Sub4) this).anInt9939 * -53470607, ((Class282_Sub17_Sub4) this).anInt9936 * 2126963875, (byte) 86);
	}

	void method12250(RsByteBuffer class282_sub35, int i) {
		((Class282_Sub17_Sub4) this).anInt9937 = class282_sub35.readIntLE() * 339755293;
		((Class282_Sub17_Sub4) this).anInt9938 = class282_sub35.readIntLE() * 1183558607;
		((Class282_Sub17_Sub4) this).anInt9939 = class282_sub35.readUnsignedByte() * 2031642257;
		((Class282_Sub17_Sub4) this).anInt9936 = class282_sub35.readUnsignedByte() * -1979650293;
	}

	void method12251(Class61 class61, int i) {
		class61.method1212(-31291083 * ((Class282_Sub17_Sub4) this).anInt9937, (-1425966289 * ((Class282_Sub17_Sub4) this).anInt9938), ((Class282_Sub17_Sub4) this).anInt9939 * -53470607, ((Class282_Sub17_Sub4) this).anInt9936 * 2126963875, (byte) 62);
	}

	void method12254(Class61 class61) {
		class61.method1212(-31291083 * ((Class282_Sub17_Sub4) this).anInt9937, (-1425966289 * ((Class282_Sub17_Sub4) this).anInt9938), ((Class282_Sub17_Sub4) this).anInt9939 * -53470607, ((Class282_Sub17_Sub4) this).anInt9936 * 2126963875, (byte) 67);
	}

	void method12257(RsByteBuffer class282_sub35) {
		((Class282_Sub17_Sub4) this).anInt9937 = class282_sub35.readIntLE() * 339755293;
		((Class282_Sub17_Sub4) this).anInt9938 = class282_sub35.readIntLE() * 1183558607;
		((Class282_Sub17_Sub4) this).anInt9939 = class282_sub35.readUnsignedByte() * 2031642257;
		((Class282_Sub17_Sub4) this).anInt9936 = class282_sub35.readUnsignedByte() * -1979650293;
	}

	void method12249(RsByteBuffer class282_sub35) {
		((Class282_Sub17_Sub4) this).anInt9937 = class282_sub35.readIntLE() * 339755293;
		((Class282_Sub17_Sub4) this).anInt9938 = class282_sub35.readIntLE() * 1183558607;
		((Class282_Sub17_Sub4) this).anInt9939 = class282_sub35.readUnsignedByte() * 2031642257;
		((Class282_Sub17_Sub4) this).anInt9936 = class282_sub35.readUnsignedByte() * -1979650293;
	}

	Class282_Sub17_Sub4(Class348 class348) {
		((Class282_Sub17_Sub4) this).this$0 = class348;
	}

	void method12258(Class61 class61) {
		class61.method1212(-31291083 * ((Class282_Sub17_Sub4) this).anInt9937, (-1425966289 * ((Class282_Sub17_Sub4) this).anInt9938), ((Class282_Sub17_Sub4) this).anInt9939 * -53470607, ((Class282_Sub17_Sub4) this).anInt9936 * 2126963875, (byte) 20);
	}

	void method12256(Class61 class61) {
		class61.method1212(-31291083 * ((Class282_Sub17_Sub4) this).anInt9937, (-1425966289 * ((Class282_Sub17_Sub4) this).anInt9938), ((Class282_Sub17_Sub4) this).anInt9939 * -53470607, ((Class282_Sub17_Sub4) this).anInt9936 * 2126963875, (byte) 62);
	}

	void method12252(Class61 class61) {
		class61.method1212(-31291083 * ((Class282_Sub17_Sub4) this).anInt9937, (-1425966289 * ((Class282_Sub17_Sub4) this).anInt9938), ((Class282_Sub17_Sub4) this).anInt9939 * -53470607, ((Class282_Sub17_Sub4) this).anInt9936 * 2126963875, (byte) 64);
	}

	void method12253(RsByteBuffer class282_sub35) {
		((Class282_Sub17_Sub4) this).anInt9937 = class282_sub35.readIntLE() * 339755293;
		((Class282_Sub17_Sub4) this).anInt9938 = class282_sub35.readIntLE() * 1183558607;
		((Class282_Sub17_Sub4) this).anInt9939 = class282_sub35.readUnsignedByte() * 2031642257;
		((Class282_Sub17_Sub4) this).anInt9936 = class282_sub35.readUnsignedByte() * -1979650293;
	}

	static final void method15406(Class527 class527, int i) {
		boolean bool = true;
		if (client.aBool7310) {
			try {
				Object object = Class361.aClass361_4169.method6255((short) 7681);
				if (object != null)
					bool = ((Boolean) object).booleanValue();
			} catch (Throwable throwable) {
				/* empty */
			}
		}
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = bool ? 1 : 0;
	}

	static final void method15407(Class527 class527, int i) {
		int i_0_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = Class393.aClass282_Sub54_4783.aClass468_Sub30_8194.method7785(i_0_, -723558345);
	}

	static final void method15408(Class527 class527, int i) {
		int i_1_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class227 class227 = Class96_Sub16.aClass211_9383.method3616(i_1_, (byte) 12);
		if (null != class227.anIntArray2814 && class227.anIntArray2814.length > 0) {
			int i_2_ = 0;
			int i_3_ = class227.anIntArray2789[0];
			for (int i_4_ = 1; i_4_ < class227.anIntArray2814.length; i_4_++) {
				if (class227.anIntArray2789[i_4_] > i_3_) {
					i_2_ = i_4_;
					i_3_ = class227.anIntArray2789[i_4_];
				}
			}
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = class227.anIntArray2814[i_2_];
		} else
			((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1)] = -243982531 * class227.anInt2819;
	}

	static final void method15409(Class118 class118, Class98 class98, Class527 class527, int i) {
		String string = (String) (((Class527) class527).anObjectArray7019[(((Class527) class527).anInt7000 -= 1476624725) * 1806726141]);
		if (Class96_Sub14.method14642(string, class527, 1686354780) != null)
			string = string.substring(0, string.length() - 1);
		class118.anObjectArray1386 = Class351.method6193(string, class527, 1189927775);
		class118.aBool1384 = true;
	}
}
