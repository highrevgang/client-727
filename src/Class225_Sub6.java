/* Class225_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class225_Sub6 extends Class225 {
	public static String aString8069;

	public boolean method3785(int i, int i_0_, int i_1_, Class336 class336) {
		return Class325.method5790(i_0_, i_1_, i, i, -1440558477 * anInt2779, 1732585867 * anInt2780, anInt2781 * -1384164183, 361960939 * anInt2782, -1278426193);
	}

	public boolean method3787(int i, int i_2_, int i_3_, Class336 class336, byte i_4_) {
		return Class325.method5790(i_2_, i_3_, i, i, -1440558477 * anInt2779, 1732585867 * anInt2780, anInt2781 * -1384164183, 361960939 * anInt2782, -1229597907);
	}

	public boolean method3786(int i, int i_5_, int i_6_, Class336 class336) {
		return Class325.method5790(i_5_, i_6_, i, i, -1440558477 * anInt2779, 1732585867 * anInt2780, anInt2781 * -1384164183, 361960939 * anInt2782, -1069558416);
	}

	public boolean method3789(int i, int i_7_, int i_8_, Class336 class336) {
		return Class325.method5790(i_7_, i_8_, i, i, -1440558477 * anInt2779, 1732585867 * anInt2780, anInt2781 * -1384164183, 361960939 * anInt2782, -1327717790);
	}

	public boolean method3788(int i, int i_9_, int i_10_, Class336 class336) {
		return Class325.method5790(i_9_, i_10_, i, i, -1440558477 * anInt2779, 1732585867 * anInt2780, anInt2781 * -1384164183, 361960939 * anInt2782, -232258627);
	}

	Class225_Sub6() {
		/* empty */
	}

	static final void method13411(int i, int i_11_, int i_12_, int i_13_, byte i_14_) {
		if (i_11_ > i_12_) {
			for (int i_15_ = i_12_; i_15_ < i_11_; i_15_++)
				Class532_Sub2.anIntArrayArray7072[i_15_][i] = i_13_;
		} else {
			for (int i_16_ = i_11_; i_16_ < i_12_; i_16_++)
				Class532_Sub2.anIntArrayArray7072[i_16_][i] = i_13_;
		}
	}

	static final void method13412(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class282_Sub41_Sub1.method14700(class118, class98, class527, 1949391932);
	}

	static void method13413(int i) {
		Class540.aClass8_7138 = null;
		Class16.aClass8_144 = null;
		Class285.aClass8_3394 = null;
		Class282_Sub21.aClass160Array7673 = null;
		Class16.aClass160Array138 = null;
		Class391.aClass160Array4778 = null;
		Class282_Sub20_Sub15.aClass160Array9838 = null;
		Class16.aClass160Array145 = null;
		Class282_Sub36.aClass160Array7994 = null;
		Class250.aClass160Array3092 = null;
		Class182.aClass160Array2261 = null;
		Class16.aClass160_146 = null;
		Exception_Sub1.aClass160_10075 = null;
		Class245.aClass160Array3027 = null;
	}
}
