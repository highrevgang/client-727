/* Class50 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class50 {
	static final boolean method977(int i, int i_0_) {
		return (i & 0x37) != 0;
	}

	static final boolean method978(int i, int i_1_) {
		return (i & 0x37) != 0;
	}

	static final boolean method979(int i, int i_2_) {
		return (i & 0x22) != 0;
	}

	static final boolean method980(int i, int i_3_) {
		return (i & 0x34) != 0;
	}

	static final boolean method981(int i, int i_4_) {
		return (i & 0x37) != 0;
	}

	static final boolean method982(int i, int i_5_) {
		return (i & 0x220) == 544 | (i & 0x18) != 0;
	}

	static final boolean method983(int i, int i_6_) {
		return (i & 0x800) != 0 && (i_6_ & 0x37) != 0;
	}

	static final boolean method984(int i, int i_7_) {
		return (i & 0x100) != 0;
	}

	static final boolean method985(int i, int i_8_) {
		return false;
	}

	static final boolean method986(int i, int i_9_) {
		return (i & 0x10) != 0;
	}

	static final boolean method987(int i, int i_10_) {
		return method1022(i, i_10_) || method1051(i, i_10_);
	}

	static final boolean method988(int i, int i_11_) {
		return (i & 0xc580) != 0;
	}

	static final boolean method989(int i, int i_12_) {
		return (i & 0x10000) != 0;
	}

	static final boolean method990(int i, int i_13_) {
		return (i & 0x800) != 0;
	}

	static final boolean method991(int i, int i_14_) {
		return ((i & 0x40000) != 0 | method984(i, i_14_) || method1051(i, i_14_));
	}

	static final boolean method992(int i, int i_15_) {
		return (i & 0x8000) != 0;
	}

	static final boolean method993(int i, int i_16_) {
		return (i & 0x800) != 0;
	}

	static final boolean method994(int i, int i_17_) {
		return (i & 0x21) != 0;
	}

	static final boolean method995(int i, int i_18_) {
		return ((method994(i, i_18_) | method979(i, i_18_) | method980(i, i_18_)) & method990(i, i_18_));
	}

	static final boolean method996(int i, int i_19_) {
		boolean bool = ((i_19_ & 0x37) != 0 ? method1020(i, i_19_) : method1051(i, i_19_));
		return (i & 0x10000) != 0 | method982(i, i_19_) | bool;
	}

	static final boolean method997(int i, int i_20_) {
		return (i & 0x10000) != 0;
	}

	static final boolean method998(int i, int i_21_) {
		return method1022(i, i_21_) || method1051(i, i_21_);
	}

	static final boolean method999(int i, int i_22_) {
		return method986(i, i_22_) & method1043(i, i_22_);
	}

	static final boolean method1000(int i, int i_23_) {
		return ((i & 0x70000) != 0 || method994(i, i_23_) || method995(i, i_23_));
	}

	static final boolean method1001(int i, int i_24_) {
		return ((i & 0x70000) != 0 || method979(i, i_24_) || method995(i, i_24_));
	}

	static final boolean method1002(int i, int i_25_) {
		return ((i & 0x70000) != 0 || method980(i, i_25_) || method995(i, i_25_));
	}

	static final boolean method1003(int i, int i_26_) {
		return (i & 0x34) != 0;
	}

	Class50() throws Throwable {
		throw new Error();
	}

	static final boolean method1004(int i, int i_27_) {
		return ((i & 0x40000) != 0 | method984(i, i_27_) || method1051(i, i_27_));
	}

	static final boolean method1005(int i, int i_28_) {
		return method985(i, i_28_) || method1056(i, i_28_);
	}

	static final boolean method1006(int i, int i_29_) {
		return (i & 0x180) != 0;
	}

	static final boolean method1007(int i, int i_30_) {
		return (i & 0x20) != 0;
	}

	static final boolean method1008(int i, int i_31_) {
		return (i & 0x400) != 0;
	}

	static final boolean method1009(int i, int i_32_) {
		return (i & 0x60000) != 0 | method986(i, i_32_) || method999(i, i_32_);
	}

	static final boolean method1010(int i, int i_33_) {
		return (i & 0x800) != 0 | method992(i, i_33_) || method1051(i, i_33_);
	}

	static final boolean method1011(int i, int i_34_) {
		return (i & 0x21) != 0;
	}

	static final boolean method1012(int i, int i_35_) {
		return (i & 0x800) != 0 && (i_35_ & 0x37) != 0;
	}

	static final boolean method1013(int i, int i_36_) {
		return (i & 0x34) != 0;
	}

	static final boolean method1014(int i, int i_37_) {
		return (i & 0x37) != 0;
	}

	static final boolean method1015(int i, int i_38_) {
		return method985(i, i_38_) & method993(i, i_38_);
	}

	static final boolean method1016(int i, int i_39_) {
		return (i & 0x37) != 0;
	}

	static final boolean method1017(int i, int i_40_) {
		return ((i & 0x40000) != 0 | method984(i, i_40_) || method1051(i, i_40_));
	}

	static final boolean method1018(int i, int i_41_) {
		return (i & 0x84080) != 0;
	}

	static final boolean method1019(int i, int i_42_) {
		return (i & 0x100) != 0;
	}

	static final boolean method1020(int i, int i_43_) {
		return method983(i, i_43_) & ((i & 0x2000) != 0 | method982(i, i_43_) | method989(i, i_43_));
	}

	static final boolean method1021(int i, int i_44_) {
		return ((i & 0x70000) != 0 || method979(i, i_44_) || method995(i, i_44_));
	}

	static final boolean method1022(int i, int i_45_) {
		return (i & 0x84080) != 0;
	}

	static final boolean method1023(int i, int i_46_) {
		return (i & 0x10) != 0;
	}

	static final boolean method1024(int i, int i_47_) {
		return (i & 0x8000) != 0;
	}

	static final boolean method1025(int i, int i_48_) {
		return (i & 0x800) != 0;
	}

	static final boolean method1026(int i, int i_49_) {
		return (i & 0xc580) != 0;
	}

	static final boolean method1027(int i, int i_50_) {
		return (i & 0xc580) != 0;
	}

	static final boolean method1028(int i, int i_51_) {
		return method983(i, i_51_) & ((i & 0x2000) != 0 | method982(i, i_51_) | method989(i, i_51_));
	}

	static final boolean method1029(int i, int i_52_) {
		return (i & 0x10000) != 0;
	}

	static final boolean method1030(int i, int i_53_) {
		return (i & 0x800) != 0;
	}

	static final boolean method1031(int i, int i_54_) {
		return method985(i, i_54_) & method993(i, i_54_);
	}

	static final boolean method1032(int i, int i_55_) {
		return (i & 0x800) != 0 && (i_55_ & 0x37) != 0;
	}

	static final boolean method1033(int i, int i_56_) {
		return (i & 0x800) != 0 && (i_56_ & 0x37) != 0;
	}

	static final boolean method1034(int i, int i_57_) {
		return (i & 0xc580) != 0;
	}

	static final boolean method1035(int i, int i_58_) {
		return false;
	}

	static final boolean method1036(int i, int i_59_) {
		return (i & 0x800) != 0;
	}

	static final boolean method1037(int i, int i_60_) {
		return (i & 0x800) != 0;
	}

	static final boolean method1038(int i, int i_61_) {
		return ((method994(i, i_61_) | method979(i, i_61_) | method980(i, i_61_)) & method990(i, i_61_));
	}

	static final boolean method1039(int i, int i_62_) {
		return (i & 0xc580) != 0;
	}

	static final boolean method1040(int i, int i_63_) {
		return method983(i, i_63_) & ((i & 0x2000) != 0 | method982(i, i_63_) | method989(i, i_63_));
	}

	static final boolean method1041(int i, int i_64_) {
		if (!method1059(i, i_64_))
			return false;
		if ((i & 0x9000) != 0 | method1022(i, i_64_) | method984(i, i_64_))
			return true;
		return (i_64_ & 0x37) == 0 & ((i & 0x2000) != 0 | method982(i, i_64_) | method989(i, i_64_));
	}

	static final boolean method1042(int i, int i_65_) {
		return false;
	}

	static final boolean method1043(int i, int i_66_) {
		return (i & 0x800) != 0;
	}

	static final boolean method1044(int i, int i_67_) {
		return method985(i, i_67_) & method993(i, i_67_);
	}

	static final boolean method1045(int i, int i_68_) {
		return method985(i, i_68_) & method993(i, i_68_);
	}

	static final boolean method1046(int i, int i_69_) {
		return method985(i, i_69_) || method1056(i, i_69_);
	}

	static final boolean method1047(int i, int i_70_) {
		boolean bool = ((i_70_ & 0x37) != 0 ? method1020(i, i_70_) : method1051(i, i_70_));
		return (i & 0x10000) != 0 | method982(i, i_70_) | bool;
	}

	static final boolean method1048(int i, int i_71_) {
		return ((i & 0x70000) != 0 || method994(i, i_71_) || method995(i, i_71_));
	}

	static final boolean method1049(int i, int i_72_) {
		return ((i & 0x70000) != 0 || method994(i, i_72_) || method995(i, i_72_));
	}

	static final boolean method1050(int i, int i_73_) {
		return ((i & 0x70000) != 0 || method979(i, i_73_) || method995(i, i_73_));
	}

	static final boolean method1051(int i, int i_74_) {
		if (!method1059(i, i_74_))
			return false;
		if ((i & 0x9000) != 0 | method1022(i, i_74_) | method984(i, i_74_))
			return true;
		return (i_74_ & 0x37) == 0 & ((i & 0x2000) != 0 | method982(i, i_74_) | method989(i, i_74_));
	}

	static final boolean method1052(int i, int i_75_) {
		return ((i & 0x70000) != 0 || method980(i, i_75_) || method995(i, i_75_));
	}

	static final boolean method1053(int i, int i_76_) {
		boolean bool = ((i_76_ & 0x37) != 0 ? method1020(i, i_76_) : method1051(i, i_76_));
		return (i & 0x10000) != 0 | method982(i, i_76_) | bool;
	}

	static final boolean method1054(int i, int i_77_) {
		return method1022(i, i_77_) || method1051(i, i_77_);
	}

	static final boolean method1055(int i, int i_78_) {
		return false;
	}

	static final boolean method1056(int i, int i_79_) {
		return method985(i, i_79_) & method993(i, i_79_);
	}

	static final boolean method1057(int i, int i_80_) {
		return ((i & 0x40000) != 0 | method984(i, i_80_) || method1051(i, i_80_));
	}

	static final boolean method1058(int i, int i_81_) {
		return method986(i, i_81_) & method1043(i, i_81_);
	}

	static final boolean method1059(int i, int i_82_) {
		return (i & 0x800) != 0;
	}

	static final boolean method1060(int i, int i_83_) {
		return ((i & 0x70000) != 0 || method994(i, i_83_) || method995(i, i_83_));
	}

	static final boolean method1061(int i, int i_84_) {
		return (i & 0x180) != 0;
	}

	static final boolean method1062(int i, int i_85_) {
		return (i & 0x400) != 0;
	}

	static final boolean method1063(int i, int i_86_) {
		return (i & 0x60000) != 0 | method986(i, i_86_) || method999(i, i_86_);
	}

	static final boolean method1064(int i, int i_87_) {
		return (i & 0x60000) != 0 | method986(i, i_87_) || method999(i, i_87_);
	}

	static final boolean method1065(int i, int i_88_) {
		return (i & 0x800) != 0 | method992(i, i_88_) || method1051(i, i_88_);
	}

	static final boolean method1066(int i, int i_89_) {
		return (i & 0x800) != 0 | method992(i, i_89_) || method1051(i, i_89_);
	}
}
