/* Class91 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class91 {
	public int anInt953;
	public int anInt954;
	public int[] anIntArray955;
	public int anInt956;
	public int anInt957;
	public int anInt958;
	public int anInt959;
	public byte[] aByteArray960;
	public byte[] aByteArray961;

	public int method1513() {
		return anInt957 + anInt956 + anInt958;
	}

	public static Class91[] method1514(Class317 class317, int i, int i_0_) {
		byte[] is = class317.method5607(i, i_0_, -1211833322);
		if (is == null)
			return null;
		return method1517(is);
	}

	public static Class91 method1515(Class317 class317, int i) {
		byte[] is = class317.method5615(i, -560964409);
		if (is == null)
			return null;
		return method1517(is)[0];
	}

	public void method1516(int i) {
		int i_1_ = -1;
		if (anIntArray955.length < 255) {
			for (int i_2_ = 0; i_2_ < anIntArray955.length; i_2_++) {
				if (anIntArray955[i_2_] == i) {
					i_1_ = i_2_;
					break;
				}
			}
			if (i_1_ == -1) {
				i_1_ = anIntArray955.length;
				int[] is = new int[anIntArray955.length + 1];
				Class503.method8362(anIntArray955, 0, is, 0, anIntArray955.length);
				anIntArray955 = is;
				is[i_1_] = i;
			}
		} else {
			int i_3_ = 2147483647;
			int i_4_ = i >> 16 & 0xff;
			int i_5_ = i >> 8 & 0xff;
			int i_6_ = i & 0xff;
			for (int i_7_ = 0; i_7_ < anIntArray955.length; i_7_++) {
				int i_8_ = anIntArray955[i_7_];
				int i_9_ = i_8_ >> 16 & 0xff;
				int i_10_ = i_8_ >> 8 & 0xff;
				int i_11_ = i_8_ & 0xff;
				int i_12_ = i_4_ - i_9_;
				if (i_12_ < 0)
					i_12_ = -i_12_;
				int i_13_ = i_5_ - i_10_;
				if (i_13_ < 0)
					i_13_ = -i_13_;
				int i_14_ = i_6_ - i_11_;
				if (i_14_ < 0)
					i_14_ = -i_14_;
				int i_15_ = i_12_ + i_13_ + i_14_;
				if (i_15_ < i_3_) {
					i_3_ = i_15_;
					i_1_ = i_7_;
				}
			}
		}
		for (int i_16_ = anInt954 - 1; i_16_ > 0; i_16_--) {
			int i_17_ = i_16_ * anInt957;
			for (int i_18_ = anInt957 - 1; i_18_ > 0; i_18_--) {
				if (anIntArray955[aByteArray960[i_18_ + i_17_] & 0xff] == 0 && ((anIntArray955[aByteArray960[i_18_ + i_17_ - 1 - anInt957] & 0xff]) != 0))
					aByteArray960[i_18_ + i_17_] = (byte) i_1_;
			}
		}
	}

	public static Class91[] method1517(byte[] is) {
		RsByteBuffer class282_sub35 = new RsByteBuffer(is);
		class282_sub35.index = (is.length - 2) * -1115476867;
		int i = class282_sub35.readUnsignedShort();
		Class91[] class91s = new Class91[i];
		for (int i_19_ = 0; i_19_ < i; i_19_++)
			class91s[i_19_] = new Class91();
		class282_sub35.index = (is.length - 7 - i * 8) * -1115476867;
		int i_20_ = class282_sub35.readUnsignedShort();
		int i_21_ = class282_sub35.readUnsignedShort();
		int i_22_ = (class282_sub35.readUnsignedByte() & 0xff) + 1;
		for (int i_23_ = 0; i_23_ < i; i_23_++)
			class91s[i_23_].anInt956 = class282_sub35.readUnsignedShort();
		for (int i_24_ = 0; i_24_ < i; i_24_++)
			class91s[i_24_].anInt959 = class282_sub35.readUnsignedShort();
		for (int i_25_ = 0; i_25_ < i; i_25_++)
			class91s[i_25_].anInt957 = class282_sub35.readUnsignedShort();
		for (int i_26_ = 0; i_26_ < i; i_26_++)
			class91s[i_26_].anInt954 = class282_sub35.readUnsignedShort();
		for (int i_27_ = 0; i_27_ < i; i_27_++) {
			Class91 class91 = class91s[i_27_];
			class91.anInt958 = i_20_ - class91.anInt957 - class91.anInt956;
			class91.anInt953 = i_21_ - class91.anInt954 - class91.anInt959;
		}
		class282_sub35.index = (is.length - 7 - i * 8 - (i_22_ - 1) * 3) * -1115476867;
		int[] is_28_ = new int[i_22_];
		for (int i_29_ = 1; i_29_ < i_22_; i_29_++) {
			is_28_[i_29_] = class282_sub35.method13082((short) 31603);
			if (is_28_[i_29_] == 0)
				is_28_[i_29_] = 1;
		}
		for (int i_30_ = 0; i_30_ < i; i_30_++)
			class91s[i_30_].anIntArray955 = is_28_;
		class282_sub35.index = 0;
		for (int i_31_ = 0; i_31_ < i; i_31_++) {
			Class91 class91 = class91s[i_31_];
			int i_32_ = class91.anInt957 * class91.anInt954;
			class91.aByteArray960 = new byte[i_32_];
			int i_33_ = class282_sub35.readUnsignedByte();
			if ((i_33_ & 0x2) == 0) {
				if ((i_33_ & 0x1) == 0) {
					for (int i_34_ = 0; i_34_ < i_32_; i_34_++)
						class91.aByteArray960[i_34_] = class282_sub35.method13236((short) -19464);
				} else {
					for (int i_35_ = 0; i_35_ < class91.anInt957; i_35_++) {
						for (int i_36_ = 0; i_36_ < class91.anInt954; i_36_++)
							class91.aByteArray960[(i_35_ + i_36_ * class91.anInt957)] = class282_sub35.method13236((short) -9979);
					}
				}
			} else {
				boolean bool = false;
				class91.aByteArray961 = new byte[i_32_];
				if ((i_33_ & 0x1) == 0) {
					for (int i_37_ = 0; i_37_ < i_32_; i_37_++)
						class91.aByteArray960[i_37_] = class282_sub35.method13236((short) -11258);
					for (int i_38_ = 0; i_38_ < i_32_; i_38_++) {
						byte i_39_ = (class91.aByteArray961[i_38_] = class282_sub35.method13236((short) -29337));
						bool = bool | i_39_ != -1;
					}
				} else {
					for (int i_40_ = 0; i_40_ < class91.anInt957; i_40_++) {
						for (int i_41_ = 0; i_41_ < class91.anInt954; i_41_++)
							class91.aByteArray960[(i_40_ + i_41_ * class91.anInt957)] = class282_sub35.method13236((short) -5480);
					}
					for (int i_42_ = 0; i_42_ < class91.anInt957; i_42_++) {
						for (int i_43_ = 0; i_43_ < class91.anInt954; i_43_++) {
							byte i_44_ = (class91.aByteArray961[i_42_ + i_43_ * class91.anInt957] = class282_sub35.method13236((short) -2518));
							bool = bool | i_44_ != -1;
						}
					}
				}
				if (!bool)
					class91.aByteArray961 = null;
			}
		}
		return class91s;
	}

	public void method1518() {
		byte[] is = aByteArray960;
		if (aByteArray961 == null) {
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_45_ = i * anInt957;
				int i_46_ = (anInt954 - i - 1) * anInt957;
				for (int i_47_ = -anInt957; i_47_ < 0; i_47_++) {
					byte i_48_ = is[i_45_];
					is[i_45_] = is[i_46_];
					is[i_46_] = i_48_;
					i_45_++;
					i_46_++;
				}
			}
		} else {
			byte[] is_49_ = aByteArray961;
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_50_ = i * anInt957;
				int i_51_ = (anInt954 - i - 1) * anInt957;
				for (int i_52_ = -anInt957; i_52_ < 0; i_52_++) {
					byte i_53_ = is[i_50_];
					is[i_50_] = is[i_51_];
					is[i_51_] = i_53_;
					i_53_ = is_49_[i_50_];
					is_49_[i_50_] = is_49_[i_51_];
					is_49_[i_51_] = i_53_;
					i_50_++;
					i_51_++;
				}
			}
		}
		int i = anInt959;
		anInt959 = anInt953;
		anInt953 = i;
	}

	public int method1519() {
		return anInt957 + anInt956 + anInt958;
	}

	public int method1520() {
		return anInt954 + anInt959 + anInt953;
	}

	public void method1521() {
		byte[] is = new byte[anInt957 * anInt954];
		int i = 0;
		if (aByteArray961 == null) {
			for (int i_54_ = 0; i_54_ < anInt957; i_54_++) {
				for (int i_55_ = anInt954 - 1; i_55_ >= 0; i_55_--)
					is[i++] = aByteArray960[i_54_ + i_55_ * anInt957];
			}
			aByteArray960 = is;
		} else {
			byte[] is_56_ = new byte[anInt957 * anInt954];
			for (int i_57_ = 0; i_57_ < anInt957; i_57_++) {
				for (int i_58_ = anInt954 - 1; i_58_ >= 0; i_58_--) {
					is[i] = aByteArray960[i_57_ + i_58_ * anInt957];
					is_56_[i++] = aByteArray961[i_57_ + i_58_ * anInt957];
				}
			}
			aByteArray960 = is;
			aByteArray961 = is_56_;
		}
		int i_59_ = anInt959;
		anInt959 = anInt956;
		anInt956 = anInt953;
		anInt953 = anInt958;
		anInt958 = anInt959;
		i_59_ = anInt954;
		anInt954 = anInt957;
		anInt957 = i_59_;
	}

	public static Class91 method1522(Class317 class317, int i, int i_60_) {
		byte[] is = class317.method5607(i, i_60_, -2041689556);
		if (is == null)
			return null;
		return method1517(is)[0];
	}

	public void method1523() {
		int i = method1519();
		int i_61_ = method1520();
		if (anInt957 != i || anInt954 != i_61_) {
			byte[] is = new byte[i * i_61_];
			if (aByteArray961 != null) {
				byte[] is_62_ = new byte[i * i_61_];
				for (int i_63_ = 0; i_63_ < anInt954; i_63_++) {
					int i_64_ = i_63_ * anInt957;
					int i_65_ = (i_63_ + anInt959) * i + anInt956;
					for (int i_66_ = 0; i_66_ < anInt957; i_66_++) {
						is[i_65_] = aByteArray960[i_64_];
						is_62_[i_65_++] = aByteArray961[i_64_++];
					}
				}
				aByteArray961 = is_62_;
			} else {
				for (int i_67_ = 0; i_67_ < anInt954; i_67_++) {
					int i_68_ = i_67_ * anInt957;
					int i_69_ = (i_67_ + anInt959) * i + anInt956;
					for (int i_70_ = 0; i_70_ < anInt957; i_70_++)
						is[i_69_++] = aByteArray960[i_68_++];
				}
			}
			anInt953 = 0;
			anInt959 = 0;
			anInt958 = 0;
			anInt956 = 0;
			anInt957 = i;
			anInt954 = i_61_;
			aByteArray960 = is;
		}
	}

	public void method1524(int i) {
		int i_71_ = -1;
		if (anIntArray955.length < 255) {
			for (int i_72_ = 0; i_72_ < anIntArray955.length; i_72_++) {
				if (anIntArray955[i_72_] == i) {
					i_71_ = i_72_;
					break;
				}
			}
			if (i_71_ == -1) {
				i_71_ = anIntArray955.length;
				int[] is = new int[anIntArray955.length + 1];
				Class503.method8362(anIntArray955, 0, is, 0, anIntArray955.length);
				anIntArray955 = is;
				is[i_71_] = i;
			}
		} else {
			int i_73_ = 2147483647;
			int i_74_ = i >> 16 & 0xff;
			int i_75_ = i >> 8 & 0xff;
			int i_76_ = i & 0xff;
			for (int i_77_ = 0; i_77_ < anIntArray955.length; i_77_++) {
				int i_78_ = anIntArray955[i_77_];
				int i_79_ = i_78_ >> 16 & 0xff;
				int i_80_ = i_78_ >> 8 & 0xff;
				int i_81_ = i_78_ & 0xff;
				int i_82_ = i_74_ - i_79_;
				if (i_82_ < 0)
					i_82_ = -i_82_;
				int i_83_ = i_75_ - i_80_;
				if (i_83_ < 0)
					i_83_ = -i_83_;
				int i_84_ = i_76_ - i_81_;
				if (i_84_ < 0)
					i_84_ = -i_84_;
				int i_85_ = i_82_ + i_83_ + i_84_;
				if (i_85_ < i_73_) {
					i_73_ = i_85_;
					i_71_ = i_77_;
				}
			}
		}
		for (int i_86_ = anInt954 - 1; i_86_ > 0; i_86_--) {
			int i_87_ = i_86_ * anInt957;
			for (int i_88_ = anInt957 - 1; i_88_ > 0; i_88_--) {
				if (anIntArray955[aByteArray960[i_88_ + i_87_] & 0xff] == 0 && ((anIntArray955[aByteArray960[i_88_ + i_87_ - 1 - anInt957] & 0xff]) != 0))
					aByteArray960[i_88_ + i_87_] = (byte) i_71_;
			}
		}
	}

	public void method1525() {
		byte[] is = aByteArray960;
		if (aByteArray961 == null) {
			for (int i = anInt954 - 1; i >= 0; i--) {
				int i_89_ = i * anInt957;
				for (int i_90_ = (i + 1) * anInt957; i_89_ < i_90_; i_89_++) {
					i_90_--;
					byte i_91_ = is[i_89_];
					is[i_89_] = is[i_90_];
					is[i_90_] = i_91_;
				}
			}
		} else {
			byte[] is_92_ = aByteArray961;
			for (int i = anInt954 - 1; i >= 0; i--) {
				int i_93_ = i * anInt957;
				for (int i_94_ = (i + 1) * anInt957; i_93_ < i_94_; i_93_++) {
					i_94_--;
					byte i_95_ = is[i_93_];
					is[i_93_] = is[i_94_];
					is[i_94_] = i_95_;
					i_95_ = is_92_[i_93_];
					is_92_[i_93_] = is_92_[i_94_];
					is_92_[i_94_] = i_95_;
				}
			}
		}
		int i = anInt956;
		anInt956 = anInt958;
		anInt958 = i;
	}

	public void method1526() {
		byte[] is = aByteArray960;
		if (aByteArray961 == null) {
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_96_ = i * anInt957;
				int i_97_ = (anInt954 - i - 1) * anInt957;
				for (int i_98_ = -anInt957; i_98_ < 0; i_98_++) {
					byte i_99_ = is[i_96_];
					is[i_96_] = is[i_97_];
					is[i_97_] = i_99_;
					i_96_++;
					i_97_++;
				}
			}
		} else {
			byte[] is_100_ = aByteArray961;
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_101_ = i * anInt957;
				int i_102_ = (anInt954 - i - 1) * anInt957;
				for (int i_103_ = -anInt957; i_103_ < 0; i_103_++) {
					byte i_104_ = is[i_101_];
					is[i_101_] = is[i_102_];
					is[i_102_] = i_104_;
					i_104_ = is_100_[i_101_];
					is_100_[i_101_] = is_100_[i_102_];
					is_100_[i_102_] = i_104_;
					i_101_++;
					i_102_++;
				}
			}
		}
		int i = anInt959;
		anInt959 = anInt953;
		anInt953 = i;
	}

	public void method1527() {
		byte[] is = new byte[anInt957 * anInt954];
		int i = 0;
		if (aByteArray961 == null) {
			for (int i_105_ = 0; i_105_ < anInt957; i_105_++) {
				for (int i_106_ = anInt954 - 1; i_106_ >= 0; i_106_--)
					is[i++] = aByteArray960[i_105_ + i_106_ * anInt957];
			}
			aByteArray960 = is;
		} else {
			byte[] is_107_ = new byte[anInt957 * anInt954];
			for (int i_108_ = 0; i_108_ < anInt957; i_108_++) {
				for (int i_109_ = anInt954 - 1; i_109_ >= 0; i_109_--) {
					is[i] = aByteArray960[i_108_ + i_109_ * anInt957];
					is_107_[i++] = aByteArray961[i_108_ + i_109_ * anInt957];
				}
			}
			aByteArray960 = is;
			aByteArray961 = is_107_;
		}
		int i_110_ = anInt959;
		anInt959 = anInt956;
		anInt956 = anInt953;
		anInt953 = anInt958;
		anInt958 = anInt959;
		i_110_ = anInt954;
		anInt954 = anInt957;
		anInt957 = i_110_;
	}

	public int[] method1528() {
		int i = method1519();
		int[] is = new int[i * method1520()];
		if (aByteArray961 != null) {
			for (int i_111_ = 0; i_111_ < anInt954; i_111_++) {
				int i_112_ = i_111_ * anInt957;
				int i_113_ = anInt956 + (i_111_ + anInt959) * i;
				for (int i_114_ = 0; i_114_ < anInt957; i_114_++) {
					is[i_113_++] = (aByteArray961[i_112_] << 24 | anIntArray955[aByteArray960[i_112_] & 0xff]);
					i_112_++;
				}
			}
		} else {
			for (int i_115_ = 0; i_115_ < anInt954; i_115_++) {
				int i_116_ = i_115_ * anInt957;
				int i_117_ = anInt956 + (i_115_ + anInt959) * i;
				for (int i_118_ = 0; i_118_ < anInt957; i_118_++) {
					int i_119_ = anIntArray955[aByteArray960[i_116_++] & 0xff];
					if (i_119_ != 0)
						is[i_117_++] = ~0xffffff | i_119_;
					else
						is[i_117_++] = 0;
				}
			}
		}
		return is;
	}

	public void method1529(int i, int i_120_, int i_121_) {
		for (int i_122_ = 1; i_122_ < anIntArray955.length; i_122_++) {
			if (anIntArray955[i_122_] != 1 && anIntArray955[i_122_] != 16711935) {
				int i_123_ = anIntArray955[i_122_] >> 16 & 0xff;
				i_123_ += i;
				if (i_123_ < 0)
					i_123_ = 0;
				else if (i_123_ > 255)
					i_123_ = 255;
				int i_124_ = anIntArray955[i_122_] >> 8 & 0xff;
				i_124_ += i_120_;
				if (i_124_ < 0)
					i_124_ = 0;
				else if (i_124_ > 255)
					i_124_ = 255;
				int i_125_ = anIntArray955[i_122_] >> 0 & 0xff;
				i_125_ += i_121_;
				if (i_125_ < 0)
					i_125_ = 0;
				else if (i_125_ > 255)
					i_125_ = 255;
				anIntArray955[i_122_] = i_123_ << 16 | i_124_ << 8 | i_125_;
			}
		}
	}

	public void method1530() {
		int i = method1519();
		int i_126_ = method1520();
		if (anInt957 != i || anInt954 != i_126_) {
			byte[] is = new byte[i * i_126_];
			if (aByteArray961 != null) {
				byte[] is_127_ = new byte[i * i_126_];
				for (int i_128_ = 0; i_128_ < anInt954; i_128_++) {
					int i_129_ = i_128_ * anInt957;
					int i_130_ = (i_128_ + anInt959) * i + anInt956;
					for (int i_131_ = 0; i_131_ < anInt957; i_131_++) {
						is[i_130_] = aByteArray960[i_129_];
						is_127_[i_130_++] = aByteArray961[i_129_++];
					}
				}
				aByteArray961 = is_127_;
			} else {
				for (int i_132_ = 0; i_132_ < anInt954; i_132_++) {
					int i_133_ = i_132_ * anInt957;
					int i_134_ = (i_132_ + anInt959) * i + anInt956;
					for (int i_135_ = 0; i_135_ < anInt957; i_135_++)
						is[i_134_++] = aByteArray960[i_133_++];
				}
			}
			anInt953 = 0;
			anInt959 = 0;
			anInt958 = 0;
			anInt956 = 0;
			anInt957 = i;
			anInt954 = i_126_;
			aByteArray960 = is;
		}
	}

	public static Class91[] method1531(Class317 class317, int i, int i_136_) {
		byte[] is = class317.method5607(i, i_136_, -1628886426);
		if (is == null)
			return null;
		return method1517(is);
	}

	public static Class91[] method1532(Class317 class317, int i) {
		byte[] is = class317.method5615(i, -325433248);
		if (is == null)
			return null;
		return method1517(is);
	}

	public static Class91[] method1533(Class317 class317, int i) {
		byte[] is = class317.method5615(i, -850664287);
		if (is == null)
			return null;
		return method1517(is);
	}

	public static Class91[] method1534(Class317 class317, int i) {
		byte[] is = class317.method5615(i, -415130062);
		if (is == null)
			return null;
		return method1517(is);
	}

	public static Class91[] method1535(byte[] is) {
		RsByteBuffer class282_sub35 = new RsByteBuffer(is);
		class282_sub35.index = (is.length - 2) * -1115476867;
		int i = class282_sub35.readUnsignedShort();
		Class91[] class91s = new Class91[i];
		for (int i_137_ = 0; i_137_ < i; i_137_++)
			class91s[i_137_] = new Class91();
		class282_sub35.index = (is.length - 7 - i * 8) * -1115476867;
		int i_138_ = class282_sub35.readUnsignedShort();
		int i_139_ = class282_sub35.readUnsignedShort();
		int i_140_ = (class282_sub35.readUnsignedByte() & 0xff) + 1;
		for (int i_141_ = 0; i_141_ < i; i_141_++)
			class91s[i_141_].anInt956 = class282_sub35.readUnsignedShort();
		for (int i_142_ = 0; i_142_ < i; i_142_++)
			class91s[i_142_].anInt959 = class282_sub35.readUnsignedShort();
		for (int i_143_ = 0; i_143_ < i; i_143_++)
			class91s[i_143_].anInt957 = class282_sub35.readUnsignedShort();
		for (int i_144_ = 0; i_144_ < i; i_144_++)
			class91s[i_144_].anInt954 = class282_sub35.readUnsignedShort();
		for (int i_145_ = 0; i_145_ < i; i_145_++) {
			Class91 class91 = class91s[i_145_];
			class91.anInt958 = i_138_ - class91.anInt957 - class91.anInt956;
			class91.anInt953 = i_139_ - class91.anInt954 - class91.anInt959;
		}
		class282_sub35.index = (is.length - 7 - i * 8 - (i_140_ - 1) * 3) * -1115476867;
		int[] is_146_ = new int[i_140_];
		for (int i_147_ = 1; i_147_ < i_140_; i_147_++) {
			is_146_[i_147_] = class282_sub35.method13082((short) 584);
			if (is_146_[i_147_] == 0)
				is_146_[i_147_] = 1;
		}
		for (int i_148_ = 0; i_148_ < i; i_148_++)
			class91s[i_148_].anIntArray955 = is_146_;
		class282_sub35.index = 0;
		for (int i_149_ = 0; i_149_ < i; i_149_++) {
			Class91 class91 = class91s[i_149_];
			int i_150_ = class91.anInt957 * class91.anInt954;
			class91.aByteArray960 = new byte[i_150_];
			int i_151_ = class282_sub35.readUnsignedByte();
			if ((i_151_ & 0x2) == 0) {
				if ((i_151_ & 0x1) == 0) {
					for (int i_152_ = 0; i_152_ < i_150_; i_152_++)
						class91.aByteArray960[i_152_] = class282_sub35.method13236((short) -598);
				} else {
					for (int i_153_ = 0; i_153_ < class91.anInt957; i_153_++) {
						for (int i_154_ = 0; i_154_ < class91.anInt954; i_154_++)
							class91.aByteArray960[i_153_ + i_154_ * class91.anInt957] = class282_sub35.method13236((short) -21554);
					}
				}
			} else {
				boolean bool = false;
				class91.aByteArray961 = new byte[i_150_];
				if ((i_151_ & 0x1) == 0) {
					for (int i_155_ = 0; i_155_ < i_150_; i_155_++)
						class91.aByteArray960[i_155_] = class282_sub35.method13236((short) -9997);
					for (int i_156_ = 0; i_156_ < i_150_; i_156_++) {
						byte i_157_ = (class91.aByteArray961[i_156_] = class282_sub35.method13236((short) -4906));
						bool = bool | i_157_ != -1;
					}
				} else {
					for (int i_158_ = 0; i_158_ < class91.anInt957; i_158_++) {
						for (int i_159_ = 0; i_159_ < class91.anInt954; i_159_++)
							class91.aByteArray960[i_158_ + i_159_ * class91.anInt957] = class282_sub35.method13236((short) -25180);
					}
					for (int i_160_ = 0; i_160_ < class91.anInt957; i_160_++) {
						for (int i_161_ = 0; i_161_ < class91.anInt954; i_161_++) {
							byte i_162_ = (class91.aByteArray961[i_160_ + i_161_ * class91.anInt957] = class282_sub35.method13236((short) -20857));
							bool = bool | i_162_ != -1;
						}
					}
				}
				if (!bool)
					class91.aByteArray961 = null;
			}
		}
		return class91s;
	}

	public void method1536(int i) {
		int i_163_ = -1;
		if (anIntArray955.length < 255) {
			for (int i_164_ = 0; i_164_ < anIntArray955.length; i_164_++) {
				if (anIntArray955[i_164_] == i) {
					i_163_ = i_164_;
					break;
				}
			}
			if (i_163_ == -1) {
				i_163_ = anIntArray955.length;
				int[] is = new int[anIntArray955.length + 1];
				Class503.method8362(anIntArray955, 0, is, 0, anIntArray955.length);
				anIntArray955 = is;
				is[i_163_] = i;
			}
		} else {
			int i_165_ = 2147483647;
			int i_166_ = i >> 16 & 0xff;
			int i_167_ = i >> 8 & 0xff;
			int i_168_ = i & 0xff;
			for (int i_169_ = 0; i_169_ < anIntArray955.length; i_169_++) {
				int i_170_ = anIntArray955[i_169_];
				int i_171_ = i_170_ >> 16 & 0xff;
				int i_172_ = i_170_ >> 8 & 0xff;
				int i_173_ = i_170_ & 0xff;
				int i_174_ = i_166_ - i_171_;
				if (i_174_ < 0)
					i_174_ = -i_174_;
				int i_175_ = i_167_ - i_172_;
				if (i_175_ < 0)
					i_175_ = -i_175_;
				int i_176_ = i_168_ - i_173_;
				if (i_176_ < 0)
					i_176_ = -i_176_;
				int i_177_ = i_174_ + i_175_ + i_176_;
				if (i_177_ < i_165_) {
					i_165_ = i_177_;
					i_163_ = i_169_;
				}
			}
		}
		int i_178_ = 0;
		byte[] is = new byte[anInt957 * anInt954];
		for (int i_179_ = 0; i_179_ < anInt954; i_179_++) {
			for (int i_180_ = 0; i_180_ < anInt957; i_180_++) {
				int i_181_ = aByteArray960[i_178_] & 0xff;
				if (anIntArray955[i_181_] == 0) {
					if (i_180_ > 0 && (anIntArray955[aByteArray960[i_178_ - 1] & 0xff] != 0))
						i_181_ = i_163_;
					else if (i_179_ > 0 && anIntArray955[(aByteArray960[i_178_ - anInt957] & 0xff)] != 0)
						i_181_ = i_163_;
					else if (i_180_ < anInt957 - 1 && (anIntArray955[aByteArray960[i_178_ + 1] & 0xff]) != 0)
						i_181_ = i_163_;
					else if (i_179_ < anInt954 - 1 && anIntArray955[(aByteArray960[i_178_ + anInt957] & 0xff)] != 0)
						i_181_ = i_163_;
				}
				is[i_178_++] = (byte) i_181_;
			}
		}
		aByteArray960 = is;
	}

	public static Class91[] method1537(byte[] is) {
		RsByteBuffer class282_sub35 = new RsByteBuffer(is);
		class282_sub35.index = (is.length - 2) * -1115476867;
		int i = class282_sub35.readUnsignedShort();
		Class91[] class91s = new Class91[i];
		for (int i_182_ = 0; i_182_ < i; i_182_++)
			class91s[i_182_] = new Class91();
		class282_sub35.index = (is.length - 7 - i * 8) * -1115476867;
		int i_183_ = class282_sub35.readUnsignedShort();
		int i_184_ = class282_sub35.readUnsignedShort();
		int i_185_ = (class282_sub35.readUnsignedByte() & 0xff) + 1;
		for (int i_186_ = 0; i_186_ < i; i_186_++)
			class91s[i_186_].anInt956 = class282_sub35.readUnsignedShort();
		for (int i_187_ = 0; i_187_ < i; i_187_++)
			class91s[i_187_].anInt959 = class282_sub35.readUnsignedShort();
		for (int i_188_ = 0; i_188_ < i; i_188_++)
			class91s[i_188_].anInt957 = class282_sub35.readUnsignedShort();
		for (int i_189_ = 0; i_189_ < i; i_189_++)
			class91s[i_189_].anInt954 = class282_sub35.readUnsignedShort();
		for (int i_190_ = 0; i_190_ < i; i_190_++) {
			Class91 class91 = class91s[i_190_];
			class91.anInt958 = i_183_ - class91.anInt957 - class91.anInt956;
			class91.anInt953 = i_184_ - class91.anInt954 - class91.anInt959;
		}
		class282_sub35.index = (is.length - 7 - i * 8 - (i_185_ - 1) * 3) * -1115476867;
		int[] is_191_ = new int[i_185_];
		for (int i_192_ = 1; i_192_ < i_185_; i_192_++) {
			is_191_[i_192_] = class282_sub35.method13082((short) 25771);
			if (is_191_[i_192_] == 0)
				is_191_[i_192_] = 1;
		}
		for (int i_193_ = 0; i_193_ < i; i_193_++)
			class91s[i_193_].anIntArray955 = is_191_;
		class282_sub35.index = 0;
		for (int i_194_ = 0; i_194_ < i; i_194_++) {
			Class91 class91 = class91s[i_194_];
			int i_195_ = class91.anInt957 * class91.anInt954;
			class91.aByteArray960 = new byte[i_195_];
			int i_196_ = class282_sub35.readUnsignedByte();
			if ((i_196_ & 0x2) == 0) {
				if ((i_196_ & 0x1) == 0) {
					for (int i_197_ = 0; i_197_ < i_195_; i_197_++)
						class91.aByteArray960[i_197_] = class282_sub35.method13236((short) -222);
				} else {
					for (int i_198_ = 0; i_198_ < class91.anInt957; i_198_++) {
						for (int i_199_ = 0; i_199_ < class91.anInt954; i_199_++)
							class91.aByteArray960[i_198_ + i_199_ * class91.anInt957] = class282_sub35.method13236((short) -13722);
					}
				}
			} else {
				boolean bool = false;
				class91.aByteArray961 = new byte[i_195_];
				if ((i_196_ & 0x1) == 0) {
					for (int i_200_ = 0; i_200_ < i_195_; i_200_++)
						class91.aByteArray960[i_200_] = class282_sub35.method13236((short) -11797);
					for (int i_201_ = 0; i_201_ < i_195_; i_201_++) {
						byte i_202_ = (class91.aByteArray961[i_201_] = class282_sub35.method13236((short) -29559));
						bool = bool | i_202_ != -1;
					}
				} else {
					for (int i_203_ = 0; i_203_ < class91.anInt957; i_203_++) {
						for (int i_204_ = 0; i_204_ < class91.anInt954; i_204_++)
							class91.aByteArray960[i_203_ + i_204_ * class91.anInt957] = class282_sub35.method13236((short) -8475);
					}
					for (int i_205_ = 0; i_205_ < class91.anInt957; i_205_++) {
						for (int i_206_ = 0; i_206_ < class91.anInt954; i_206_++) {
							byte i_207_ = (class91.aByteArray961[i_205_ + i_206_ * class91.anInt957] = class282_sub35.method13236((short) -6617));
							bool = bool | i_207_ != -1;
						}
					}
				}
				if (!bool)
					class91.aByteArray961 = null;
			}
		}
		return class91s;
	}

	public int method1538() {
		return anInt957 + anInt956 + anInt958;
	}

	public int method1539() {
		return anInt957 + anInt956 + anInt958;
	}

	public int method1540() {
		return anInt954 + anInt959 + anInt953;
	}

	Class91() {
		/* empty */
	}

	public int method1541() {
		return anInt954 + anInt959 + anInt953;
	}

	public int method1542() {
		return anInt954 + anInt959 + anInt953;
	}

	public void method1543(int i) {
		int i_208_ = method1519();
		int i_209_ = method1520();
		if (anInt957 != i_208_ || anInt954 != i_209_) {
			int i_210_ = i;
			if (i_210_ > anInt956)
				i_210_ = anInt956;
			int i_211_ = i;
			if (i_211_ + anInt956 + anInt957 > i_208_)
				i_211_ = i_208_ - anInt956 - anInt957;
			int i_212_ = i;
			if (i_212_ > anInt959)
				i_212_ = anInt959;
			int i_213_ = i;
			if (i_213_ + anInt959 + anInt954 > i_209_)
				i_213_ = i_209_ - anInt959 - anInt954;
			int i_214_ = anInt957 + i_210_ + i_211_;
			int i_215_ = anInt954 + i_212_ + i_213_;
			byte[] is = new byte[i_214_ * i_215_];
			if (aByteArray961 == null) {
				for (int i_216_ = 0; i_216_ < anInt954; i_216_++) {
					int i_217_ = i_216_ * anInt957;
					int i_218_ = (i_216_ + i_212_) * i_214_ + i_210_;
					for (int i_219_ = 0; i_219_ < anInt957; i_219_++)
						is[i_218_++] = aByteArray960[i_217_++];
				}
			} else {
				byte[] is_220_ = new byte[i_214_ * i_215_];
				for (int i_221_ = 0; i_221_ < anInt954; i_221_++) {
					int i_222_ = i_221_ * anInt957;
					int i_223_ = (i_221_ + i_212_) * i_214_ + i_210_;
					for (int i_224_ = 0; i_224_ < anInt957; i_224_++) {
						is_220_[i_223_] = aByteArray961[i_222_];
						is[i_223_++] = aByteArray960[i_222_++];
					}
				}
				aByteArray961 = is_220_;
			}
			anInt956 -= i_210_;
			anInt959 -= i_212_;
			anInt958 -= i_211_;
			anInt953 -= i_213_;
			anInt957 = i_214_;
			anInt954 = i_215_;
			aByteArray960 = is;
		}
	}

	public void method1544() {
		int i = method1519();
		int i_225_ = method1520();
		if (anInt957 != i || anInt954 != i_225_) {
			byte[] is = new byte[i * i_225_];
			if (aByteArray961 != null) {
				byte[] is_226_ = new byte[i * i_225_];
				for (int i_227_ = 0; i_227_ < anInt954; i_227_++) {
					int i_228_ = i_227_ * anInt957;
					int i_229_ = (i_227_ + anInt959) * i + anInt956;
					for (int i_230_ = 0; i_230_ < anInt957; i_230_++) {
						is[i_229_] = aByteArray960[i_228_];
						is_226_[i_229_++] = aByteArray961[i_228_++];
					}
				}
				aByteArray961 = is_226_;
			} else {
				for (int i_231_ = 0; i_231_ < anInt954; i_231_++) {
					int i_232_ = i_231_ * anInt957;
					int i_233_ = (i_231_ + anInt959) * i + anInt956;
					for (int i_234_ = 0; i_234_ < anInt957; i_234_++)
						is[i_233_++] = aByteArray960[i_232_++];
				}
			}
			anInt953 = 0;
			anInt959 = 0;
			anInt958 = 0;
			anInt956 = 0;
			anInt957 = i;
			anInt954 = i_225_;
			aByteArray960 = is;
		}
	}

	public void method1545() {
		int i = method1519();
		int i_235_ = method1520();
		if (anInt957 != i || anInt954 != i_235_) {
			byte[] is = new byte[i * i_235_];
			if (aByteArray961 != null) {
				byte[] is_236_ = new byte[i * i_235_];
				for (int i_237_ = 0; i_237_ < anInt954; i_237_++) {
					int i_238_ = i_237_ * anInt957;
					int i_239_ = (i_237_ + anInt959) * i + anInt956;
					for (int i_240_ = 0; i_240_ < anInt957; i_240_++) {
						is[i_239_] = aByteArray960[i_238_];
						is_236_[i_239_++] = aByteArray961[i_238_++];
					}
				}
				aByteArray961 = is_236_;
			} else {
				for (int i_241_ = 0; i_241_ < anInt954; i_241_++) {
					int i_242_ = i_241_ * anInt957;
					int i_243_ = (i_241_ + anInt959) * i + anInt956;
					for (int i_244_ = 0; i_244_ < anInt957; i_244_++)
						is[i_243_++] = aByteArray960[i_242_++];
				}
			}
			anInt953 = 0;
			anInt959 = 0;
			anInt958 = 0;
			anInt956 = 0;
			anInt957 = i;
			anInt954 = i_235_;
			aByteArray960 = is;
		}
	}

	public int method1546() {
		return anInt957 + anInt956 + anInt958;
	}

	public void method1547(int i) {
		int i_245_ = method1519();
		int i_246_ = method1520();
		if (anInt957 != i_245_ || anInt954 != i_246_) {
			int i_247_ = i;
			if (i_247_ > anInt956)
				i_247_ = anInt956;
			int i_248_ = i;
			if (i_248_ + anInt956 + anInt957 > i_245_)
				i_248_ = i_245_ - anInt956 - anInt957;
			int i_249_ = i;
			if (i_249_ > anInt959)
				i_249_ = anInt959;
			int i_250_ = i;
			if (i_250_ + anInt959 + anInt954 > i_246_)
				i_250_ = i_246_ - anInt959 - anInt954;
			int i_251_ = anInt957 + i_247_ + i_248_;
			int i_252_ = anInt954 + i_249_ + i_250_;
			byte[] is = new byte[i_251_ * i_252_];
			if (aByteArray961 == null) {
				for (int i_253_ = 0; i_253_ < anInt954; i_253_++) {
					int i_254_ = i_253_ * anInt957;
					int i_255_ = (i_253_ + i_249_) * i_251_ + i_247_;
					for (int i_256_ = 0; i_256_ < anInt957; i_256_++)
						is[i_255_++] = aByteArray960[i_254_++];
				}
			} else {
				byte[] is_257_ = new byte[i_251_ * i_252_];
				for (int i_258_ = 0; i_258_ < anInt954; i_258_++) {
					int i_259_ = i_258_ * anInt957;
					int i_260_ = (i_258_ + i_249_) * i_251_ + i_247_;
					for (int i_261_ = 0; i_261_ < anInt957; i_261_++) {
						is_257_[i_260_] = aByteArray961[i_259_];
						is[i_260_++] = aByteArray960[i_259_++];
					}
				}
				aByteArray961 = is_257_;
			}
			anInt956 -= i_247_;
			anInt959 -= i_249_;
			anInt958 -= i_248_;
			anInt953 -= i_250_;
			anInt957 = i_251_;
			anInt954 = i_252_;
			aByteArray960 = is;
		}
	}

	public void method1548(int i) {
		int i_262_ = method1519();
		int i_263_ = method1520();
		if (anInt957 != i_262_ || anInt954 != i_263_) {
			int i_264_ = i;
			if (i_264_ > anInt956)
				i_264_ = anInt956;
			int i_265_ = i;
			if (i_265_ + anInt956 + anInt957 > i_262_)
				i_265_ = i_262_ - anInt956 - anInt957;
			int i_266_ = i;
			if (i_266_ > anInt959)
				i_266_ = anInt959;
			int i_267_ = i;
			if (i_267_ + anInt959 + anInt954 > i_263_)
				i_267_ = i_263_ - anInt959 - anInt954;
			int i_268_ = anInt957 + i_264_ + i_265_;
			int i_269_ = anInt954 + i_266_ + i_267_;
			byte[] is = new byte[i_268_ * i_269_];
			if (aByteArray961 == null) {
				for (int i_270_ = 0; i_270_ < anInt954; i_270_++) {
					int i_271_ = i_270_ * anInt957;
					int i_272_ = (i_270_ + i_266_) * i_268_ + i_264_;
					for (int i_273_ = 0; i_273_ < anInt957; i_273_++)
						is[i_272_++] = aByteArray960[i_271_++];
				}
			} else {
				byte[] is_274_ = new byte[i_268_ * i_269_];
				for (int i_275_ = 0; i_275_ < anInt954; i_275_++) {
					int i_276_ = i_275_ * anInt957;
					int i_277_ = (i_275_ + i_266_) * i_268_ + i_264_;
					for (int i_278_ = 0; i_278_ < anInt957; i_278_++) {
						is_274_[i_277_] = aByteArray961[i_276_];
						is[i_277_++] = aByteArray960[i_276_++];
					}
				}
				aByteArray961 = is_274_;
			}
			anInt956 -= i_264_;
			anInt959 -= i_266_;
			anInt958 -= i_265_;
			anInt953 -= i_267_;
			anInt957 = i_268_;
			anInt954 = i_269_;
			aByteArray960 = is;
		}
	}

	public static Class91 method1549(Class317 class317, int i) {
		byte[] is = class317.method5615(i, -1640055623);
		if (is == null)
			return null;
		return method1517(is)[0];
	}

	public void method1550() {
		byte[] is = aByteArray960;
		if (aByteArray961 == null) {
			for (int i = anInt954 - 1; i >= 0; i--) {
				int i_279_ = i * anInt957;
				for (int i_280_ = (i + 1) * anInt957; i_279_ < i_280_; i_279_++) {
					i_280_--;
					byte i_281_ = is[i_279_];
					is[i_279_] = is[i_280_];
					is[i_280_] = i_281_;
				}
			}
		} else {
			byte[] is_282_ = aByteArray961;
			for (int i = anInt954 - 1; i >= 0; i--) {
				int i_283_ = i * anInt957;
				for (int i_284_ = (i + 1) * anInt957; i_283_ < i_284_; i_283_++) {
					i_284_--;
					byte i_285_ = is[i_283_];
					is[i_283_] = is[i_284_];
					is[i_284_] = i_285_;
					i_285_ = is_282_[i_283_];
					is_282_[i_283_] = is_282_[i_284_];
					is_282_[i_284_] = i_285_;
				}
			}
		}
		int i = anInt956;
		anInt956 = anInt958;
		anInt958 = i;
	}

	public int method1551() {
		return anInt954 + anInt959 + anInt953;
	}

	public void method1552() {
		byte[] is = aByteArray960;
		if (aByteArray961 == null) {
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_286_ = i * anInt957;
				int i_287_ = (anInt954 - i - 1) * anInt957;
				for (int i_288_ = -anInt957; i_288_ < 0; i_288_++) {
					byte i_289_ = is[i_286_];
					is[i_286_] = is[i_287_];
					is[i_287_] = i_289_;
					i_286_++;
					i_287_++;
				}
			}
		} else {
			byte[] is_290_ = aByteArray961;
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_291_ = i * anInt957;
				int i_292_ = (anInt954 - i - 1) * anInt957;
				for (int i_293_ = -anInt957; i_293_ < 0; i_293_++) {
					byte i_294_ = is[i_291_];
					is[i_291_] = is[i_292_];
					is[i_292_] = i_294_;
					i_294_ = is_290_[i_291_];
					is_290_[i_291_] = is_290_[i_292_];
					is_290_[i_292_] = i_294_;
					i_291_++;
					i_292_++;
				}
			}
		}
		int i = anInt959;
		anInt959 = anInt953;
		anInt953 = i;
	}

	public void method1553() {
		byte[] is = aByteArray960;
		if (aByteArray961 == null) {
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_295_ = i * anInt957;
				int i_296_ = (anInt954 - i - 1) * anInt957;
				for (int i_297_ = -anInt957; i_297_ < 0; i_297_++) {
					byte i_298_ = is[i_295_];
					is[i_295_] = is[i_296_];
					is[i_296_] = i_298_;
					i_295_++;
					i_296_++;
				}
			}
		} else {
			byte[] is_299_ = aByteArray961;
			for (int i = (anInt954 >> 1) - 1; i >= 0; i--) {
				int i_300_ = i * anInt957;
				int i_301_ = (anInt954 - i - 1) * anInt957;
				for (int i_302_ = -anInt957; i_302_ < 0; i_302_++) {
					byte i_303_ = is[i_300_];
					is[i_300_] = is[i_301_];
					is[i_301_] = i_303_;
					i_303_ = is_299_[i_300_];
					is_299_[i_300_] = is_299_[i_301_];
					is_299_[i_301_] = i_303_;
					i_300_++;
					i_301_++;
				}
			}
		}
		int i = anInt959;
		anInt959 = anInt953;
		anInt953 = i;
	}

	public void method1554(int i) {
		int i_304_ = method1519();
		int i_305_ = method1520();
		if (anInt957 != i_304_ || anInt954 != i_305_) {
			int i_306_ = i;
			if (i_306_ > anInt956)
				i_306_ = anInt956;
			int i_307_ = i;
			if (i_307_ + anInt956 + anInt957 > i_304_)
				i_307_ = i_304_ - anInt956 - anInt957;
			int i_308_ = i;
			if (i_308_ > anInt959)
				i_308_ = anInt959;
			int i_309_ = i;
			if (i_309_ + anInt959 + anInt954 > i_305_)
				i_309_ = i_305_ - anInt959 - anInt954;
			int i_310_ = anInt957 + i_306_ + i_307_;
			int i_311_ = anInt954 + i_308_ + i_309_;
			byte[] is = new byte[i_310_ * i_311_];
			if (aByteArray961 == null) {
				for (int i_312_ = 0; i_312_ < anInt954; i_312_++) {
					int i_313_ = i_312_ * anInt957;
					int i_314_ = (i_312_ + i_308_) * i_310_ + i_306_;
					for (int i_315_ = 0; i_315_ < anInt957; i_315_++)
						is[i_314_++] = aByteArray960[i_313_++];
				}
			} else {
				byte[] is_316_ = new byte[i_310_ * i_311_];
				for (int i_317_ = 0; i_317_ < anInt954; i_317_++) {
					int i_318_ = i_317_ * anInt957;
					int i_319_ = (i_317_ + i_308_) * i_310_ + i_306_;
					for (int i_320_ = 0; i_320_ < anInt957; i_320_++) {
						is_316_[i_319_] = aByteArray961[i_318_];
						is[i_319_++] = aByteArray960[i_318_++];
					}
				}
				aByteArray961 = is_316_;
			}
			anInt956 -= i_306_;
			anInt959 -= i_308_;
			anInt958 -= i_307_;
			anInt953 -= i_309_;
			anInt957 = i_310_;
			anInt954 = i_311_;
			aByteArray960 = is;
		}
	}

	public void method1555() {
		byte[] is = new byte[anInt957 * anInt954];
		int i = 0;
		if (aByteArray961 == null) {
			for (int i_321_ = 0; i_321_ < anInt957; i_321_++) {
				for (int i_322_ = anInt954 - 1; i_322_ >= 0; i_322_--)
					is[i++] = aByteArray960[i_321_ + i_322_ * anInt957];
			}
			aByteArray960 = is;
		} else {
			byte[] is_323_ = new byte[anInt957 * anInt954];
			for (int i_324_ = 0; i_324_ < anInt957; i_324_++) {
				for (int i_325_ = anInt954 - 1; i_325_ >= 0; i_325_--) {
					is[i] = aByteArray960[i_324_ + i_325_ * anInt957];
					is_323_[i++] = aByteArray961[i_324_ + i_325_ * anInt957];
				}
			}
			aByteArray960 = is;
			aByteArray961 = is_323_;
		}
		int i_326_ = anInt959;
		anInt959 = anInt956;
		anInt956 = anInt953;
		anInt953 = anInt958;
		anInt958 = anInt959;
		i_326_ = anInt954;
		anInt954 = anInt957;
		anInt957 = i_326_;
	}

	public int[] method1556() {
		int i = method1519();
		int[] is = new int[i * method1520()];
		if (aByteArray961 != null) {
			for (int i_327_ = 0; i_327_ < anInt954; i_327_++) {
				int i_328_ = i_327_ * anInt957;
				int i_329_ = anInt956 + (i_327_ + anInt959) * i;
				for (int i_330_ = 0; i_330_ < anInt957; i_330_++) {
					is[i_329_++] = (aByteArray961[i_328_] << 24 | anIntArray955[aByteArray960[i_328_] & 0xff]);
					i_328_++;
				}
			}
		} else {
			for (int i_331_ = 0; i_331_ < anInt954; i_331_++) {
				int i_332_ = i_331_ * anInt957;
				int i_333_ = anInt956 + (i_331_ + anInt959) * i;
				for (int i_334_ = 0; i_334_ < anInt957; i_334_++) {
					int i_335_ = anIntArray955[aByteArray960[i_332_++] & 0xff];
					if (i_335_ != 0)
						is[i_333_++] = ~0xffffff | i_335_;
					else
						is[i_333_++] = 0;
				}
			}
		}
		return is;
	}
}
