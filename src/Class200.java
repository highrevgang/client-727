/* Class200 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class200 {
	int anInt2531;
	int anInt2532;
	int anInt2533;
	int anInt2534;
	int anInt2535;
	int anInt2536;
	int anInt2537;
	int anInt2538;
	int anInt2539;
	int anInt2540;

	void method3253(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		((Class200) this).anInt2534 = i;
		((Class200) this).anInt2532 = i_0_;
		((Class200) this).anInt2533 = i_1_;
		((Class200) this).anInt2540 = i_2_ * i_2_;
		((Class200) this).anInt2535 = ((Class200) this).anInt2534 + i_3_;
		((Class200) this).anInt2536 = ((Class200) this).anInt2534 + i_4_;
		((Class200) this).anInt2537 = ((Class200) this).anInt2532 + i_5_;
		((Class200) this).anInt2538 = ((Class200) this).anInt2532 + i_6_;
		((Class200) this).anInt2539 = ((Class200) this).anInt2533 + i_7_;
		((Class200) this).anInt2531 = ((Class200) this).anInt2533 + i_8_;
	}

	void method3254(int i, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_) {
		((Class200) this).anInt2534 = i;
		((Class200) this).anInt2532 = i_9_;
		((Class200) this).anInt2533 = i_10_;
		((Class200) this).anInt2540 = i_11_ * i_11_;
		((Class200) this).anInt2535 = ((Class200) this).anInt2534 + i_12_;
		((Class200) this).anInt2536 = ((Class200) this).anInt2534 + i_13_;
		((Class200) this).anInt2537 = ((Class200) this).anInt2532 + i_14_;
		((Class200) this).anInt2538 = ((Class200) this).anInt2532 + i_15_;
		((Class200) this).anInt2539 = ((Class200) this).anInt2533 + i_16_;
		((Class200) this).anInt2531 = ((Class200) this).anInt2533 + i_17_;
	}

	public boolean method3255(int i, int i_18_, int i_19_) {
		if (i < ((Class200) this).anInt2535 || i > ((Class200) this).anInt2536)
			return false;
		if (i_18_ < ((Class200) this).anInt2537 || i_18_ > ((Class200) this).anInt2538)
			return false;
		if (i_19_ < ((Class200) this).anInt2539 || i_19_ > ((Class200) this).anInt2531)
			return false;
		int i_20_ = i - ((Class200) this).anInt2534;
		int i_21_ = i_19_ - ((Class200) this).anInt2533;
		return i_20_ * i_20_ + i_21_ * i_21_ < ((Class200) this).anInt2540;
	}

	void method3256(int i, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_, int i_30_) {
		((Class200) this).anInt2534 = i;
		((Class200) this).anInt2532 = i_22_;
		((Class200) this).anInt2533 = i_23_;
		((Class200) this).anInt2540 = i_24_ * i_24_;
		((Class200) this).anInt2535 = ((Class200) this).anInt2534 + i_25_;
		((Class200) this).anInt2536 = ((Class200) this).anInt2534 + i_26_;
		((Class200) this).anInt2537 = ((Class200) this).anInt2532 + i_27_;
		((Class200) this).anInt2538 = ((Class200) this).anInt2532 + i_28_;
		((Class200) this).anInt2539 = ((Class200) this).anInt2533 + i_29_;
		((Class200) this).anInt2531 = ((Class200) this).anInt2533 + i_30_;
	}

	Class200(int i, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_, int i_37_, int i_38_, int i_39_) {
		((Class200) this).anInt2534 = i;
		((Class200) this).anInt2532 = i_31_;
		((Class200) this).anInt2533 = i_32_;
		((Class200) this).anInt2540 = i_33_ * i_33_;
		((Class200) this).anInt2535 = ((Class200) this).anInt2534 + i_34_;
		((Class200) this).anInt2536 = ((Class200) this).anInt2534 + i_35_;
		((Class200) this).anInt2537 = ((Class200) this).anInt2532 + i_36_;
		((Class200) this).anInt2538 = ((Class200) this).anInt2532 + i_37_;
		((Class200) this).anInt2539 = ((Class200) this).anInt2533 + i_38_;
		((Class200) this).anInt2531 = ((Class200) this).anInt2533 + i_39_;
	}

	void method3257(int i, int i_40_, int i_41_, int i_42_, int i_43_, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_) {
		((Class200) this).anInt2534 = i;
		((Class200) this).anInt2532 = i_40_;
		((Class200) this).anInt2533 = i_41_;
		((Class200) this).anInt2540 = i_42_ * i_42_;
		((Class200) this).anInt2535 = ((Class200) this).anInt2534 + i_43_;
		((Class200) this).anInt2536 = ((Class200) this).anInt2534 + i_44_;
		((Class200) this).anInt2537 = ((Class200) this).anInt2532 + i_45_;
		((Class200) this).anInt2538 = ((Class200) this).anInt2532 + i_46_;
		((Class200) this).anInt2539 = ((Class200) this).anInt2533 + i_47_;
		((Class200) this).anInt2531 = ((Class200) this).anInt2533 + i_48_;
	}

	public boolean method3258(int i, int i_49_, int i_50_) {
		if (i < ((Class200) this).anInt2535 || i > ((Class200) this).anInt2536)
			return false;
		if (i_49_ < ((Class200) this).anInt2537 || i_49_ > ((Class200) this).anInt2538)
			return false;
		if (i_50_ < ((Class200) this).anInt2539 || i_50_ > ((Class200) this).anInt2531)
			return false;
		int i_51_ = i - ((Class200) this).anInt2534;
		int i_52_ = i_50_ - ((Class200) this).anInt2533;
		return i_51_ * i_51_ + i_52_ * i_52_ < ((Class200) this).anInt2540;
	}

	public boolean method3259(int i, int i_53_, int i_54_) {
		if (i < ((Class200) this).anInt2535 || i > ((Class200) this).anInt2536)
			return false;
		if (i_53_ < ((Class200) this).anInt2537 || i_53_ > ((Class200) this).anInt2538)
			return false;
		if (i_54_ < ((Class200) this).anInt2539 || i_54_ > ((Class200) this).anInt2531)
			return false;
		int i_55_ = i - ((Class200) this).anInt2534;
		int i_56_ = i_54_ - ((Class200) this).anInt2533;
		return i_55_ * i_55_ + i_56_ * i_56_ < ((Class200) this).anInt2540;
	}

	public boolean method3260(int i, int i_57_, int i_58_) {
		if (i < ((Class200) this).anInt2535 || i > ((Class200) this).anInt2536)
			return false;
		if (i_57_ < ((Class200) this).anInt2537 || i_57_ > ((Class200) this).anInt2538)
			return false;
		if (i_58_ < ((Class200) this).anInt2539 || i_58_ > ((Class200) this).anInt2531)
			return false;
		int i_59_ = i - ((Class200) this).anInt2534;
		int i_60_ = i_58_ - ((Class200) this).anInt2533;
		return i_59_ * i_59_ + i_60_ * i_60_ < ((Class200) this).anInt2540;
	}
}
