/* Class282_Sub50_Sub12 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub50_Sub12 extends Class282_Sub50 {
	String aString9640;
	int anInt9641;
	int anInt9642;
	static final int anInt9643 = 21;
	static final int anInt9644 = 1;
	static final int anInt9645 = 13;
	static final int anInt9646 = 3;
	static final int anInt9647 = 4;
	static final int anInt9648 = 5;
	static final int anInt9649 = 20;
	static final int anInt9650 = 7;
	static final int anInt9651 = 17;
	static final int anInt9652 = 9;
	static final int anInt9653 = 6;
	static final int anInt9654 = 11;
	static Class477 aClass477_9655;
	static final int anInt9656 = 12;
	static final int anInt9657 = 14;
	static final int anInt9658 = 15;
	static final int anInt9659 = 16;
	static final int anInt9660 = 22;
	static final long aLong9661 = -9223372036854775808L;
	static final int anInt9662 = 19;
	static final int anInt9663 = 2;
	static final int anInt9664 = 18;
	static final int anInt9665 = 8;
	static Class477 aClass477_9666 = new Class477();
	static Class465 aClass465_9667;
	int anInt9668;
	static final long aLong9669 = 9223372036854775807L;
	static final int anInt9670 = 10;
	static final long aLong9671 = 500L;

	public static void method14950(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(11, (long) i);
		class282_sub50_sub12.method14965((byte) -111);
	}

	static void method14951(int i, int i_0_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14995(1267455957);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_0_;
	}

	static void method14952(int i, boolean bool) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(21, (long) i);
		class282_sub50_sub12.method14995(1513810553);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * (bool ? 1 : 0);
	}

	int method14953(int i) {
		return (int) (-3442165056282524525L * aLong3379 >>> 56 & 0xffL);
	}

	void method14954() {
		aLong8120 = ((aLong8120 * -7883876913471066125L | ~0x7fffffffffffffffL) * 5418180015864004923L);
		if (method14955(-208083146) == 0L)
			aClass477_9666.method7936(this, -1738910950);
	}

	long method14955(int i) {
		return aLong8120 * -7883876913471066125L & 0x7fffffffffffffffL;
	}

	static {
		aClass477_9655 = new Class477();
		aClass465_9667 = new Class465(16);
	}

	static Class282_Sub50_Sub12 method14956(int i, long l) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = ((Class282_Sub50_Sub12) aClass465_9667.method7754((long) i << 56 | l));
		if (class282_sub50_sub12 == null) {
			class282_sub50_sub12 = new Class282_Sub50_Sub12(i, l);
			aClass465_9667.method7765(class282_sub50_sub12, (class282_sub50_sub12.aLong3379 * -3442165056282524525L));
		}
		return class282_sub50_sub12;
	}

	static void method14957(int i, int i_1_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(1, (long) i);
		class282_sub50_sub12.method14995(1758835654);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_1_ * -1773141545;
	}

	static void method14958(int i, int i_2_, int i_3_, int i_4_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_2_ << 32 | (long) i);
		class282_sub50_sub12.method14995(1430607382);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_3_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_4_;
	}

	public static void method14959(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(5, (long) i);
		class282_sub50_sub12.method14965((byte) 31);
	}

	static void method14960(int i, int i_5_, int i_6_, int i_7_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_5_ << 32 | (long) i);
		class282_sub50_sub12.method14995(600983034);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_6_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_7_;
	}

	public static void method14961(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(22, (long) i);
		class282_sub50_sub12.method14965((byte) 12);
	}

	public static void method14962(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(1, (long) i);
		class282_sub50_sub12.method14965((byte) 9);
	}

	static Class282_Sub50_Sub12 method14963() {
		Class282_Sub50_Sub12 class282_sub50_sub12 = (Class282_Sub50_Sub12) aClass477_9666.method7941((byte) 4);
		if (null != class282_sub50_sub12) {
			class282_sub50_sub12.method4991(-371378792);
			class282_sub50_sub12.method13452((byte) -5);
			return class282_sub50_sub12;
		}
		for (;;) {
			class282_sub50_sub12 = (Class282_Sub50_Sub12) aClass477_9655.method7941((byte) 4);
			if (class282_sub50_sub12 == null)
				return null;
			if (class282_sub50_sub12.method14955(-1012620390) > Class169.method2869(1744066149))
				return null;
			class282_sub50_sub12.method4991(-371378792);
			class282_sub50_sub12.method13452((byte) -5);
			if ((class282_sub50_sub12.aLong8120 * -7883876913471066125L & ~0x7fffffffffffffffL) != 0L)
				return class282_sub50_sub12;
		}
	}

	static Class282_Sub50_Sub12 method14964() {
		Class282_Sub50_Sub12 class282_sub50_sub12 = (Class282_Sub50_Sub12) aClass477_9666.method7941((byte) 4);
		if (null != class282_sub50_sub12) {
			class282_sub50_sub12.method4991(-371378792);
			class282_sub50_sub12.method13452((byte) -5);
			return class282_sub50_sub12;
		}
		for (;;) {
			class282_sub50_sub12 = (Class282_Sub50_Sub12) aClass477_9655.method7941((byte) 4);
			if (class282_sub50_sub12 == null)
				return null;
			if (class282_sub50_sub12.method14955(-1129507240) > Class169.method2869(1903202524))
				return null;
			class282_sub50_sub12.method4991(-371378792);
			class282_sub50_sub12.method13452((byte) -5);
			if ((class282_sub50_sub12.aLong8120 * -7883876913471066125L & ~0x7fffffffffffffffL) != 0L)
				return class282_sub50_sub12;
		}
	}

	void method14965(byte i) {
		aLong8120 = ((-7883876913471066125L * aLong8120 & ~0x7fffffffffffffffL | Class169.method2869(1651107072) + 500L) * 5418180015864004923L);
		aClass477_9655.method7936(this, -1738910950);
	}

	public static void method14966(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(2, (long) i);
		class282_sub50_sub12.method14965((byte) -21);
	}

	long method14967(int i) {
		return aLong3379 * -3442165056282524525L & 0xffffffffffffffL;
	}

	public static void method14968(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(2, (long) i);
		class282_sub50_sub12.method14965((byte) -58);
	}

	public static void method14969(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(3, (long) i);
		class282_sub50_sub12.method14965((byte) 42);
	}

	public static void method14970(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(3, (long) i);
		class282_sub50_sub12.method14965((byte) 19);
	}

	public static void method14971(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(22, (long) i);
		class282_sub50_sub12.method14965((byte) -60);
	}

	static void method14972(int i, int i_8_, int i_9_, int i_10_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_8_ << 32 | (long) i);
		class282_sub50_sub12.method14995(1486372342);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_9_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_10_;
	}

	public static void method14973(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14965((byte) 22);
	}

	static void method14974(int i, int i_11_, int i_12_, int i_13_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(18, (long) i_11_ << 32 | (long) i);
		class282_sub50_sub12.method14995(691607735);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_12_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_13_;
	}

	public static void method14975(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14965((byte) -18);
	}

	public static void method14976(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(5, (long) i);
		class282_sub50_sub12.method14965((byte) 17);
	}

	public static void method14977(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(2, (long) i);
		class282_sub50_sub12.method14965((byte) 59);
	}

	public static void method14978(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(5, (long) i);
		class282_sub50_sub12.method14965((byte) -40);
	}

	static void method14979(int i, int i_14_, int i_15_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(11, (long) i);
		class282_sub50_sub12.method14995(1255697116);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_14_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_15_;
	}

	public static void method14980(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(6, (long) i);
		class282_sub50_sub12.method14965((byte) -65);
	}

	public static void method14981(int i, int i_16_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(18, (long) i_16_ << 32 | (long) i);
		class282_sub50_sub12.method14965((byte) -28);
	}

	public static void method14982(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14965((byte) -66);
	}

	public static void method14983(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14965((byte) 68);
	}

	long method14984() {
		return aLong8120 * -7883876913471066125L & 0x7fffffffffffffffL;
	}

	public static void method14985(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14965((byte) -66);
	}

	public static void method14986(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(8, (long) i);
		class282_sub50_sub12.method14965((byte) 9);
	}

	public static void method14987(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(9, (long) i);
		class282_sub50_sub12.method14965((byte) 36);
	}

	static void method14988(int i, int i_17_, int i_18_, int i_19_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14995(670022909);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_17_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_18_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = i_19_ * -1932168275;
	}

	public static void method14989(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(12, (long) i);
		class282_sub50_sub12.method14965((byte) -7);
	}

	public static void method14990(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(12, (long) i);
		class282_sub50_sub12.method14965((byte) -29);
	}

	public static void method14991(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(12, (long) i);
		class282_sub50_sub12.method14965((byte) -41);
	}

	static void method14992(int i, int i_20_, int i_21_, int i_22_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_20_ << 32 | (long) i);
		class282_sub50_sub12.method14995(1555473754);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_21_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_22_;
	}

	public static void method14993(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(14, (long) i);
		class282_sub50_sub12.method14965((byte) 12);
	}

	public static void method14994(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(16, (long) i);
		class282_sub50_sub12.method14965((byte) -17);
	}

	void method14995(int i) {
		aLong8120 = ((aLong8120 * -7883876913471066125L | ~0x7fffffffffffffffL) * 5418180015864004923L);
		if (method14955(-1539167546) == 0L)
			aClass477_9666.method7936(this, -1738910950);
	}

	static void method14996(int i, int i_23_, int i_24_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(11, (long) i);
		class282_sub50_sub12.method14995(1894469471);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_23_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_24_;
	}

	public static void method14997(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(20, (long) i);
		class282_sub50_sub12.method14965((byte) -48);
	}

	static void method14998(int i, int i_25_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(17, (long) i);
		class282_sub50_sub12.method14995(1145301693);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_25_ * -1773141545;
	}

	public static void method14999(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14965((byte) -10);
	}

	public static void method15000(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(21, (long) i);
		class282_sub50_sub12.method14965((byte) 91);
	}

	public static void method15001(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(17, (long) i);
		class282_sub50_sub12.method14965((byte) 8);
	}

	static void method15002() {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(15, 0L);
		class282_sub50_sub12.method14965((byte) -109);
	}

	public static void method15003(int i, int i_26_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(18, (long) i_26_ << 32 | (long) i);
		class282_sub50_sub12.method14965((byte) 70);
	}

	public static void method15004(int i, int i_27_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(18, (long) i_27_ << 32 | (long) i);
		class282_sub50_sub12.method14965((byte) -1);
	}

	public static void method15005(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(14, (long) i);
		class282_sub50_sub12.method14965((byte) -81);
	}

	public static void method15006(int i, int i_28_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(18, (long) i_28_ << 32 | (long) i);
		class282_sub50_sub12.method14965((byte) 62);
	}

	public static void method15007(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(16, (long) i);
		class282_sub50_sub12.method14965((byte) -55);
	}

	public static void method15008(int i, int i_29_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_29_ << 32 | (long) i);
		class282_sub50_sub12.method14965((byte) 8);
	}

	static void method15009(int i, int i_30_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(1, (long) i);
		class282_sub50_sub12.method14995(1184639327);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_30_ * -1773141545;
	}

	static Class282_Sub50_Sub12 method15010(int i, long l) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = ((Class282_Sub50_Sub12) aClass465_9667.method7754((long) i << 56 | l));
		if (class282_sub50_sub12 == null) {
			class282_sub50_sub12 = new Class282_Sub50_Sub12(i, l);
			aClass465_9667.method7765(class282_sub50_sub12, (class282_sub50_sub12.aLong3379 * -3442165056282524525L));
		}
		return class282_sub50_sub12;
	}

	public static void method15011(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(16, (long) i);
		class282_sub50_sub12.method14965((byte) -12);
	}

	static void method15012(int i, String string) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(2, (long) i);
		class282_sub50_sub12.method14995(1177974089);
		((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640 = string;
	}

	static void method15013(int i, String string) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(2, (long) i);
		class282_sub50_sub12.method14995(1766542737);
		((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640 = string;
	}

	static void method15014(int i, String string) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(2, (long) i);
		class282_sub50_sub12.method14995(441068116);
		((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640 = string;
	}

	static void method15015(int i, String string) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(3, (long) i);
		class282_sub50_sub12.method14995(1968547146);
		((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640 = string;
	}

	static void method15016(int i, int i_31_, int i_32_, int i_33_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14995(1162567441);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_31_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_32_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = i_33_ * -1932168275;
	}

	static void method15017(int i, int i_34_, int i_35_, int i_36_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14995(931599093);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_34_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_35_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = i_36_ * -1932168275;
	}

	static void method15018(int i, int i_37_, int i_38_, int i_39_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14995(478288131);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_37_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_38_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = i_39_ * -1932168275;
	}

	static void method15019(int i, int i_40_, int i_41_, int i_42_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(4, (long) i);
		class282_sub50_sub12.method14995(1149988690);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_40_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_41_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = i_42_ * -1932168275;
	}

	public static void method15020(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(20, (long) i);
		class282_sub50_sub12.method14965((byte) 76);
	}

	static Class282_Sub50_Sub12 method15021(int i, long l) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = ((Class282_Sub50_Sub12) aClass465_9667.method7754((long) i << 56 | l));
		if (class282_sub50_sub12 == null) {
			class282_sub50_sub12 = new Class282_Sub50_Sub12(i, l);
			aClass465_9667.method7765(class282_sub50_sub12, (class282_sub50_sub12.aLong3379 * -3442165056282524525L));
		}
		return class282_sub50_sub12;
	}

	static void method15022(int i, int i_43_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(5, (long) i);
		class282_sub50_sub12.method14995(593723579);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_43_ * -1773141545;
	}

	static void method15023(int i, int i_44_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(6, (long) i);
		class282_sub50_sub12.method14995(1941845560);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_44_ * -1773141545;
	}

	static void method15024(int i, int i_45_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(17, (long) i);
		class282_sub50_sub12.method14995(2127157752);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_45_ * -1773141545;
	}

	public static void method15025(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14965((byte) 50);
	}

	static void method15026(int i, int i_46_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14995(1362644083);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_46_;
	}

	static void method15027(int i, int i_47_, int i_48_, int i_49_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(8, (long) i);
		class282_sub50_sub12.method14995(1803199251);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_47_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = i_48_ * 717659479;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = -1932168275 * i_49_;
	}

	static void method15028(int i, int i_50_, int i_51_, int i_52_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(8, (long) i);
		class282_sub50_sub12.method14995(687697534);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_50_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = i_51_ * 717659479;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = -1932168275 * i_52_;
	}

	static void method15029(int i, int i_53_, int i_54_, int i_55_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(8, (long) i);
		class282_sub50_sub12.method14995(1304166530);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_53_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = i_54_ * 717659479;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = -1932168275 * i_55_;
	}

	static void method15030(int i, int i_56_, int i_57_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(9, (long) i);
		class282_sub50_sub12.method14995(1128034033);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_56_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_57_;
	}

	static void method15031(int i, int i_58_, int i_59_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(9, (long) i);
		class282_sub50_sub12.method14995(466950409);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_58_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_59_;
	}

	static void method15032(int i, int i_60_, int i_61_, int i_62_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(10, (long) i);
		class282_sub50_sub12.method14995(1878949926);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_60_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_61_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9642 = -1932168275 * i_62_;
	}

	void method15033() {
		aLong8120 = ((aLong8120 * -7883876913471066125L | ~0x7fffffffffffffffL) * 5418180015864004923L);
		if (method14955(-109559590) == 0L)
			aClass477_9666.method7936(this, -1738910950);
	}

	static void method15034(int i, int i_63_, int i_64_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(11, (long) i);
		class282_sub50_sub12.method14995(1051823049);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_63_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_64_;
	}

	static void method15035(int i, int i_65_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(12, (long) i);
		class282_sub50_sub12.method14995(1440152599);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_65_;
	}

	static void method15036(int i, int i_66_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(12, (long) i);
		class282_sub50_sub12.method14995(1732800604);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_66_;
	}

	static void method15037(int i, boolean bool) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(21, (long) i);
		class282_sub50_sub12.method14995(832277493);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * (bool ? 1 : 0);
	}

	static void method15038(int i, int i_67_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(12, (long) i);
		class282_sub50_sub12.method14995(422379978);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_67_;
	}

	static void method15039(int i, int i_68_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(13, (long) i);
		class282_sub50_sub12.method14995(463361379);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_68_ * -1773141545;
	}

	static void method15040(int i, int i_69_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(13, (long) i);
		class282_sub50_sub12.method14995(558143965);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_69_ * -1773141545;
	}

	static void method15041(int i, int i_70_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(13, (long) i);
		class282_sub50_sub12.method14995(1966226808);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_70_ * -1773141545;
	}

	static void method15042(int i, int i_71_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(14, (long) i);
		class282_sub50_sub12.method14995(1966163644);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_71_ * -1773141545;
	}

	static void method15043(int i, int i_72_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(16, (long) i);
		class282_sub50_sub12.method14995(1518942862);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_72_ * -1773141545;
	}

	static void method15044(int i, int i_73_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(16, (long) i);
		class282_sub50_sub12.method14995(476132029);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_73_ * -1773141545;
	}

	static void method15045(int i, String string) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(2, (long) i);
		class282_sub50_sub12.method14995(1545399760);
		((Class282_Sub50_Sub12) class282_sub50_sub12).aString9640 = string;
	}

	static void method15046(int i, int i_74_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(5, (long) i);
		class282_sub50_sub12.method14995(1610414120);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_74_ * -1773141545;
	}

	static void method15047(int i, boolean bool) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(21, (long) i);
		class282_sub50_sub12.method14995(1837311942);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * (bool ? 1 : 0);
	}

	static void method15048(int i, boolean bool) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(21, (long) i);
		class282_sub50_sub12.method14995(2079728834);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * (bool ? 1 : 0);
	}

	long method15049() {
		return aLong8120 * -7883876913471066125L & 0x7fffffffffffffffL;
	}

	static void method15050(int i, int i_75_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(17, (long) i);
		class282_sub50_sub12.method14995(754614043);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_75_ * -1773141545;
	}

	public static void method15051(int i, int i_76_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_76_ << 32 | (long) i);
		class282_sub50_sub12.method14965((byte) -21);
	}

	static Class282_Sub50_Sub12 method15052() {
		Class282_Sub50_Sub12 class282_sub50_sub12 = (Class282_Sub50_Sub12) aClass477_9666.method7941((byte) 4);
		if (null != class282_sub50_sub12) {
			class282_sub50_sub12.method4991(-371378792);
			class282_sub50_sub12.method13452((byte) -5);
			return class282_sub50_sub12;
		}
		for (;;) {
			class282_sub50_sub12 = (Class282_Sub50_Sub12) aClass477_9655.method7941((byte) 4);
			if (class282_sub50_sub12 == null)
				return null;
			if (class282_sub50_sub12.method14955(-255636947) > Class169.method2869(1624259814))
				return null;
			class282_sub50_sub12.method4991(-371378792);
			class282_sub50_sub12.method13452((byte) -5);
			if ((class282_sub50_sub12.aLong8120 * -7883876913471066125L & ~0x7fffffffffffffffL) != 0L)
				return class282_sub50_sub12;
		}
	}

	static Class282_Sub50_Sub12 method15053(int i, long l) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = ((Class282_Sub50_Sub12) aClass465_9667.method7754((long) i << 56 | l));
		if (class282_sub50_sub12 == null) {
			class282_sub50_sub12 = new Class282_Sub50_Sub12(i, l);
			aClass465_9667.method7765(class282_sub50_sub12, (class282_sub50_sub12.aLong3379 * -3442165056282524525L));
		}
		return class282_sub50_sub12;
	}

	static void method15054(int i, int i_77_, int i_78_, int i_79_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(18, (long) i_77_ << 32 | (long) i);
		class282_sub50_sub12.method14995(655588329);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_78_;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_79_;
	}

	static void method15055() {
		aClass465_9667.method7749(2018799166);
		aClass477_9655.method7935((byte) 43);
		aClass477_9666.method7935((byte) 88);
	}

	static void method15056(int i, int i_80_, int i_81_, int i_82_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_80_ << 32 | (long) i);
		class282_sub50_sub12.method14995(1835312439);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_81_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_82_;
	}

	static void method15057(int i, boolean bool) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(21, (long) i);
		class282_sub50_sub12.method14995(1821593988);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * (bool ? 1 : 0);
	}

	static void method15058(int i, int i_83_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(7, (long) i);
		class282_sub50_sub12.method14995(1464749497);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = -1773141545 * i_83_;
	}

	long method15059() {
		return aLong8120 * -7883876913471066125L & 0x7fffffffffffffffL;
	}

	void method15060() {
		aLong8120 = ((-7883876913471066125L * aLong8120 & ~0x7fffffffffffffffL | Class169.method2869(1674245562) + 500L) * 5418180015864004923L);
		aClass477_9655.method7936(this, -1738910950);
	}

	static void method15061(int i, int i_84_, int i_85_, int i_86_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_84_ << 32 | (long) i);
		class282_sub50_sub12.method14995(1415417436);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_85_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_86_;
	}

	static void method15062(int i, int i_87_, int i_88_, int i_89_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(19, (long) i_87_ << 32 | (long) i);
		class282_sub50_sub12.method14995(576798604);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = i_88_ * -1773141545;
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9641 = 717659479 * i_89_;
	}

	void method15063() {
		aLong8120 = ((-7883876913471066125L * aLong8120 & ~0x7fffffffffffffffL | Class169.method2869(2073369920) + 500L) * 5418180015864004923L);
		aClass477_9655.method7936(this, -1738910950);
	}

	void method15064() {
		aLong8120 = ((-7883876913471066125L * aLong8120 & ~0x7fffffffffffffffL | Class169.method2869(1777593068) + 500L) * 5418180015864004923L);
		aClass477_9655.method7936(this, -1738910950);
	}

	public static void method15065(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(20, (long) i);
		class282_sub50_sub12.method14965((byte) -18);
	}

	public static void method15066(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(6, (long) i);
		class282_sub50_sub12.method14965((byte) 42);
	}

	int method15067() {
		return (int) (-3442165056282524525L * aLong3379 >>> 56 & 0xffL);
	}

	int method15068() {
		return (int) (-3442165056282524525L * aLong3379 >>> 56 & 0xffL);
	}

	int method15069() {
		return (int) (-3442165056282524525L * aLong3379 >>> 56 & 0xffL);
	}

	long method15070() {
		return aLong3379 * -3442165056282524525L & 0xffffffffffffffL;
	}

	long method15071() {
		return aLong3379 * -3442165056282524525L & 0xffffffffffffffL;
	}

	long method15072() {
		return aLong3379 * -3442165056282524525L & 0xffffffffffffffL;
	}

	static void method15073() {
		aClass465_9667.method7749(-79758321);
		aClass477_9655.method7935((byte) 83);
		aClass477_9666.method7935((byte) 10);
	}

	Class282_Sub50_Sub12(int i, long l) {
		aLong3379 = -1253863389874800229L * ((long) i << 56 | l);
	}

	public static void method15074(int i) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(6, (long) i);
		class282_sub50_sub12.method14965((byte) 35);
	}

	static final void method15075(Class527 class527, int i) {
		Class404.method6811((((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 -= 141891001) * 1942118537)]), -967098159);
	}
}
