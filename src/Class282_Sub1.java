/* Class282_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class282_Sub1 extends Class282 {
	Class282_Sub1() {
		/* empty */
	}

	static final void method11612(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 425673003;
		Class117.method1978((((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]), (((Class527) class527).anIntArray6999[1 + (1942118537 * ((Class527) class527).anInt7012)]), (((Class527) class527).anIntArray6999[(1942118537 * ((Class527) class527).anInt7012 + 2)]), (byte) 1);
	}

	static final void method11613(Class527 class527, int i) {
		int i_0_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		if (-1 != -1699899559 * client.anInt7349) {
			if (0 == i_0_) {
				((Class527) class527).anIntArray6999[((((Class527) class527).anInt7012 += 141891001) * 1942118537) - 1] = -1699899559 * client.anInt7349;
				return;
			}
			i_0_--;
		}
		Class282_Sub44 class282_sub44 = (Class282_Sub44) client.aClass465_7442.method7750(-1846591050);
		while (i_0_-- > 0)
			class282_sub44 = (Class282_Sub44) client.aClass465_7442.method7751((byte) 5);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = 587626901 * class282_sub44.anInt8063;
	}

	public static void method11614(int i, int i_1_) {
		if (Class388.method6693(2080572135)) {
			if (Class9.anInt76 * 1051306693 != i)
				Class9.aLong77 = -2087908126908168589L;
			Class9.anInt76 = i * 1481524237;
			Class365.method6298(19, 1363938860);
		}
	}

	public static void method11615(int i, Class317 class317, int i_2_, int i_3_, int i_4_, boolean bool, Class109 class109, int i_5_) {
		if (i > 0) {
			Class148.anInt1730 = 800770715;
			Class75.aClass317_746 = class317;
			Class6.anInt46 = i_2_ * -26560111;
			Class148.anInt1738 = i_3_ * 275076647;
			Class282_Sub33.aClass282_Sub15_Sub2_7836 = null;
			Class158_Sub2_Sub3.anInt10243 = i_4_ * 965667275;
			Class152.aBool1962 = bool;
			Class96_Sub22.anInt9440 = (Class148.aClass282_Sub15_Sub2_1735.method15123(-1727708848) / i * -465842921);
			if (Class96_Sub22.anInt9440 * -1553319257 < 1)
				Class96_Sub22.anInt9440 = -465842921;
			Class11.aClass109_121 = class109;
		} else {
			if (null != class109)
				class109.method1849((byte) -109);
			Class282_Sub43.method13400(class317, i_2_, i_3_, i_4_, bool, (byte) -106);
		}
	}
}
