/* Class52_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class52_Sub3 extends Class52 {
	void method1080(boolean bool, int i, int i_0_) {
		int i_1_ = (method1078(-2131946604) * (((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577) / 10000);
		Class316.aClass505_3680.B(i, 2 + i_0_, i_1_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, -2070636517 * ((Class350_Sub2) ((Class52_Sub3) this).aClass350_489).anInt7813, 0);
		Class316.aClass505_3680.B(i + i_1_, i_0_ + 2, ((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577 - i_1_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, 0, 0);
	}

	Class52_Sub3(Class317 class317, Class317 class317_2_, Class350_Sub2 class350_sub2) {
		super(class317, class317_2_, (Class350) class350_sub2);
	}

	void method1077(boolean bool, int i, int i_3_, int i_4_) {
		int i_5_ = (method1078(-2125749676) * (((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577) / 10000);
		Class316.aClass505_3680.B(i, 2 + i_3_, i_5_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, -2070636517 * ((Class350_Sub2) ((Class52_Sub3) this).aClass350_489).anInt7813, 0);
		Class316.aClass505_3680.B(i + i_5_, i_3_ + 2, ((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577 - i_5_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, 0, 0);
	}

	void method1081(boolean bool, int i, int i_6_, int i_7_) {
		Class316.aClass505_3680.method8430(i - 2, i_6_, 4 + ((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 + 2, 1516875301 * ((Class350_Sub2) ((Class52_Sub3) this).aClass350_489).anInt7814, 0);
		Class316.aClass505_3680.method8430(i - 1, 1 + i_6_, 2 + 329542577 * ((Class52_Sub3) this).aClass350_489.anInt4093, 323608093 * ((Class52_Sub3) this).aClass350_489.anInt4088, 0, 0);
	}

	void method1075(boolean bool, int i, int i_8_) {
		int i_9_ = (method1078(-1961921613) * (((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577) / 10000);
		Class316.aClass505_3680.B(i, 2 + i_8_, i_9_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, -2070636517 * ((Class350_Sub2) ((Class52_Sub3) this).aClass350_489).anInt7813, 0);
		Class316.aClass505_3680.B(i + i_9_, i_8_ + 2, ((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577 - i_9_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, 0, 0);
	}

	void method1079(boolean bool, int i, int i_10_) {
		int i_11_ = (method1078(-2075222431) * (((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577) / 10000);
		Class316.aClass505_3680.B(i, 2 + i_10_, i_11_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, -2070636517 * ((Class350_Sub2) ((Class52_Sub3) this).aClass350_489).anInt7813, 0);
		Class316.aClass505_3680.B(i + i_11_, i_10_ + 2, ((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577 - i_11_, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 - 2, 0, 0);
	}

	void method1076(boolean bool, int i, int i_12_) {
		Class316.aClass505_3680.method8430(i - 2, i_12_, 4 + ((Class52_Sub3) this).aClass350_489.anInt4093 * 329542577, ((Class52_Sub3) this).aClass350_489.anInt4088 * 323608093 + 2, 1516875301 * ((Class350_Sub2) ((Class52_Sub3) this).aClass350_489).anInt7814, 0);
		Class316.aClass505_3680.method8430(i - 1, 1 + i_12_, 2 + 329542577 * ((Class52_Sub3) this).aClass350_489.anInt4093, 323608093 * ((Class52_Sub3) this).aClass350_489.anInt4088, 0, 0);
	}

	static final void method14519(short i) {
		for (Class275_Sub2 class275_sub2 = ((Class275_Sub2) client.aClass457_7290.method7648(1998226533)); class275_sub2 != null; class275_sub2 = ((Class275_Sub2) client.aClass457_7290.method7648(1129303155)))
			Class505.method8695(class275_sub2, 131934272);
		int i_13_ = 0;
		int i_14_ = 3;
		if (4 == client.anInt7341 * -891719545) {
			for (int i_15_ = i_13_; i_15_ <= i_14_; i_15_++)
				client.method11629(i_15_);
			client.method11619();
		} else {
			client.method11626();
			for (int i_16_ = i_13_; i_16_ <= i_14_; i_16_++) {
				client.method11627();
				client.method11628(i_16_);
				client.method11629(i_16_);
			}
			client.method11750();
			client.method11619();
		}
	}

	static final void method14520(Class527 class527, int i) {
		((Class527) class527).anInt7012 -= 567564004;
		Class153.method2618((((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]), (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]), (((Class527) class527).anIntArray6999[2 + ((Class527) class527).anInt7012 * 1942118537]), (((Class527) class527).anIntArray6999[3 + 1942118537 * ((Class527) class527).anInt7012]), 256, 1476817598);
	}

	static final void method14521(Class527 class527, byte i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class240.method4136(class118, class98, class527, (byte) 55);
	}

	public static int[] method14522(int i) {
		return (new int[] { Class16.anInt140 * 1500183805, 436671641 * Class16.anInt136, -625646657 * Class395.anInt4788 });
	}
}
