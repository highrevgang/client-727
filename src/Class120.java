/* Class120 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class120 {
	public static Class120 aClass120_1464;
	static Class120 aClass120_1465;
	public static Class120 aClass120_1466;
	public static Class120 aClass120_1467;
	static Class120 aClass120_1468;
	public static Class120 aClass120_1469;
	static Class120 aClass120_1470;
	public static Class120 aClass120_1471;
	static Class120 aClass120_1472;
	public static Class120 aClass120_1473;
	public static Class120 aClass120_1474;
	public static Class120 aClass120_1475;
	public static Class120 aClass120_1476;
	public static Class120 aClass120_1477;
	public static Class120 aClass120_1478;
	public static Class120 aClass120_1479;
	static Class120 aClass120_1480;
	static Class120 aClass120_1481;
	public static Class120 aClass120_1482;
	public static Class120 aClass120_1483;
	static Class120 aClass120_1484;
	static Class120 aClass120_1485;
	static Class120 aClass120_1486;
	public static Class120 aClass120_1487;
	public static Class120 aClass120_1488;
	public static Class120 aClass120_1489;
	static Class120 aClass120_1490;
	static Class120 aClass120_1491;
	public static Class120 aClass120_1492;
	public static Class120 aClass120_1493;
	public static Class120 aClass120_1494;
	int anInt1495;
	public static Class120 aClass120_1496;
	public static Class120 aClass120_1497;
	public static Class120 aClass120_1498;
	static Class120 aClass120_1499;
	static Class120 aClass120_1500;
	static Class120 aClass120_1501;
	static Class120 aClass120_1502;
	static Class120 aClass120_1503;
	public static Class120 aClass120_1504 = new Class120(1);
	static Class120 aClass120_1505;
	static Class120 aClass120_1506;
	static Class120 aClass120_1507;
	static Class120 aClass120_1508;
	public static Class120 aClass120_1509;
	public static Class120 aClass120_1510;
	public static Class120 aClass120_1511;
	public static Class120 aClass120_1512;
	static Class120 aClass120_1513;
	public static Class120 aClass120_1514;
	static Class120 aClass120_1515;
	static Class120 aClass120_1516;
	static Class120 aClass120_1517;
	static Class120 aClass120_1518;
	static Class120 aClass120_1519;
	static Class120 aClass120_1520;
	public int anInt1521;
	public static Class120 aClass120_1522;

	Class120(int i) {
		this(i, 0);
	}

	Class120(int i, int i_0_) {
		anInt1521 = -982036495 * i;
		((Class120) this).anInt1495 = 1490949057 * i_0_;
	}

	public int method2078(int i) {
		return 1 << 854326337 * ((Class120) this).anInt1495;
	}

	public int method2079(int i, int i_1_) {
		return i >>> 854326337 * ((Class120) this).anInt1495;
	}

	public int method2080(int i, int i_2_) {
		return i & (1 << ((Class120) this).anInt1495 * 854326337) - 1;
	}

	public int method2081(int i) {
		return i >>> 854326337 * ((Class120) this).anInt1495;
	}

	public int method2082() {
		return 1 << 854326337 * ((Class120) this).anInt1495;
	}

	public int method2083() {
		return 1 << 854326337 * ((Class120) this).anInt1495;
	}

	public static boolean method2084(Class317 class317, Class317 class317_3_, Class317 class317_4_, Class282_Sub15_Sub2 class282_sub15_sub2, Class253 class253, int i) {
		Class148.aClass317_1737 = class317;
		Class148.aClass317_1731 = class317_3_;
		Class148.aClass317_1732 = class317_4_;
		Class148.aClass282_Sub15_Sub2_1735 = class282_sub15_sub2;
		Class502.aClass253_5830 = class253;
		Class453.anIntArray5449 = new int[16];
		for (int i_5_ = 0; i_5_ < 16; i_5_++)
			Class453.anIntArray5449[i_5_] = 255;
		return true;
	}

	public int method2085(int i) {
		return i >>> 854326337 * ((Class120) this).anInt1495;
	}

	static {
		aClass120_1465 = new Class120(2);
		aClass120_1466 = new Class120(3);
		aClass120_1514 = new Class120(4);
		aClass120_1492 = new Class120(5);
		aClass120_1488 = new Class120(6, 8);
		aClass120_1470 = new Class120(7);
		aClass120_1471 = new Class120(8, 8);
		aClass120_1467 = new Class120(9, 7);
		aClass120_1493 = new Class120(10, 8);
		aClass120_1474 = new Class120(11);
		aClass120_1475 = new Class120(12, 7);
		aClass120_1476 = new Class120(13, 8);
		aClass120_1477 = new Class120(14, 10);
		aClass120_1478 = new Class120(15);
		aClass120_1487 = new Class120(16);
		aClass120_1480 = new Class120(17);
		aClass120_1481 = new Class120(18);
		aClass120_1482 = new Class120(19);
		aClass120_1472 = new Class120(20);
		aClass120_1484 = new Class120(21);
		aClass120_1485 = new Class120(22);
		aClass120_1486 = new Class120(23);
		aClass120_1522 = new Class120(24);
		aClass120_1479 = new Class120(25);
		aClass120_1473 = new Class120(26);
		aClass120_1490 = new Class120(27);
		aClass120_1491 = new Class120(28);
		aClass120_1469 = new Class120(29);
		aClass120_1489 = new Class120(30);
		aClass120_1494 = new Class120(31);
		aClass120_1511 = new Class120(32);
		aClass120_1496 = new Class120(33);
		aClass120_1497 = new Class120(34);
		aClass120_1498 = new Class120(35);
		aClass120_1483 = new Class120(36);
		aClass120_1500 = new Class120(37);
		aClass120_1501 = new Class120(38);
		aClass120_1502 = new Class120(39);
		aClass120_1503 = new Class120(40);
		aClass120_1468 = new Class120(41);
		aClass120_1505 = new Class120(42);
		aClass120_1506 = new Class120(43);
		aClass120_1507 = new Class120(44);
		aClass120_1508 = new Class120(45);
		aClass120_1509 = new Class120(46);
		aClass120_1510 = new Class120(47);
		aClass120_1518 = new Class120(48);
		aClass120_1499 = new Class120(49);
		aClass120_1513 = new Class120(50);
		aClass120_1516 = new Class120(51);
		aClass120_1515 = new Class120(53);
		aClass120_1464 = new Class120(54);
		aClass120_1517 = new Class120(70);
		aClass120_1512 = new Class120(72);
		aClass120_1519 = new Class120(73);
		aClass120_1520 = new Class120(74);
	}

	public int method2086(int i) {
		return i & (1 << ((Class120) this).anInt1495 * 854326337) - 1;
	}

	public int method2087(int i) {
		return i & (1 << ((Class120) this).anInt1495 * 854326337) - 1;
	}

	public int method2088(int i) {
		return i & (1 << ((Class120) this).anInt1495 * 854326337) - 1;
	}

	public int method2089(int i) {
		return i & (1 << ((Class120) this).anInt1495 * 854326337) - 1;
	}

	public int method2090(int i) {
		return i & (1 << ((Class120) this).anInt1495 * 854326337) - 1;
	}

	static final void method2091(Class527 class527, byte i) {
		((Class527) class527).anInt7012 -= 425673003;
		int i_6_ = (((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537]);
		int i_7_ = (((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 + 1]);
		int i_8_ = (((Class527) class527).anIntArray6999[2 + ((Class527) class527).anInt7012 * 1942118537]);
		Class282_Sub50_Sub9 class282_sub50_sub9 = Class368.aClass429_4265.method7214(i_6_, -1670488027);
		if ((class282_sub50_sub9.method14918(i_7_, -1596809459).anInt2997 * -1869685303) != 0) {
			if (i >= 1)
				return;
			throw new RuntimeException("");
		}
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = class282_sub50_sub9.method14901(i_7_, i_8_, 1954102872);
	}

	static void method2092(Class527 class527, byte i) {
		((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 - 2] = (Class409.aClass242_4922.method4156((((Class527) class527).anIntArray6999[((Class527) class527).anInt7012 * 1942118537 - 2]), -1396181317).anIntArrayArray2966[(((Class527) class527).anIntArray6999[1942118537 * ((Class527) class527).anInt7012 - 1])][0]);
		((Class527) class527).anInt7012 -= 141891001;
	}

	static final void method2093(Class527 class527, byte i) {
		int i_9_ = (((Class527) class527).anIntArray7018[((Class527) class527).anInt7020 * 301123709]);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = ((Class521_Sub1_Sub1_Sub2_Sub2) ((Class527) class527).aClass521_Sub1_Sub1_Sub2_7006).aClass153_10579.method2610(i_9_, (short) 25901);
	}

	public static void method2094(int i, byte i_10_) {
		Class291_Sub1.anInt8018 = 428705471;
		Class291_Sub1.anInt8026 = i * 194319891;
		Class361.anInt4185 = 413638675;
		Class96_Sub9.anInt9282 = -1386689828;
	}

	static void method2095(Class282_Sub50_Sub7 class282_sub50_sub7, int i) {
		if (!Class20.aBool161) {
			class282_sub50_sub7.method4991(-371378792);
			Class20.anInt169 -= 1410722043;
			if (!((Class282_Sub50_Sub7) class282_sub50_sub7).aBool9586) {
				long l = (((Class282_Sub50_Sub7) class282_sub50_sub7).aLong9580 * 820033947929891191L);
				Class282_Sub50_Sub15 class282_sub50_sub15;
				for (class282_sub50_sub15 = ((Class282_Sub50_Sub15) Class20.aClass465_172.method7754(l)); (class282_sub50_sub15 != null && !(((Class282_Sub50_Sub15) class282_sub50_sub15).aString9771.equals(((Class282_Sub50_Sub7) class282_sub50_sub7).aString9588))); class282_sub50_sub15 = ((Class282_Sub50_Sub15) Class20.aClass465_172.method7747(-1584347554))) {
					/* empty */
				}
				if (null != class282_sub50_sub15 && class282_sub50_sub15.method15249(class282_sub50_sub7, (byte) 17))
					Class13.method503(class282_sub50_sub15, 1715883578);
			} else {
				for (Class282_Sub50_Sub15 class282_sub50_sub15 = ((Class282_Sub50_Sub15) Class20.aClass477_182.method7941((byte) 4)); class282_sub50_sub15 != null; class282_sub50_sub15 = ((Class282_Sub50_Sub15) Class20.aClass477_182.method7955(194809254))) {
					if (((Class282_Sub50_Sub15) class282_sub50_sub15).aString9771.equals(((Class282_Sub50_Sub7) class282_sub50_sub7).aString9588)) {
						boolean bool = false;
						for (Class282_Sub50_Sub7 class282_sub50_sub7_11_ = ((Class282_Sub50_Sub7) ((Class282_Sub50_Sub15) class282_sub50_sub15).aClass477_9770.method7941((byte) 4)); null != class282_sub50_sub7_11_; class282_sub50_sub7_11_ = ((Class282_Sub50_Sub7) ((Class282_Sub50_Sub15) class282_sub50_sub15).aClass477_9770.method7955(444396099))) {
							if (class282_sub50_sub7_11_ == class282_sub50_sub7) {
								if (class282_sub50_sub15.method15249(class282_sub50_sub7, (byte) 111))
									Class13.method503(class282_sub50_sub15, 1035797942);
								bool = true;
								break;
							}
						}
						if (bool)
							break;
					}
				}
			}
		}
	}

	static final void method2096(Class527 class527, byte i) {
		int i_12_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		if (2 == client.anInt7434 * 1609086245 && i_12_ < client.anInt7449 * 493536965) {
			Class6 class6 = client.aClass6Array7452[i_12_];
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = class6.aString37;
			if (class6.aString43 != null)
				((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141) - 1] = class6.aString43;
			else
				((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141) - 1] = "";
		} else {
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = "";
			((Class527) class527).anObjectArray7019[((((Class527) class527).anInt7000 += 1476624725) * 1806726141 - 1)] = "";
		}
	}
}
