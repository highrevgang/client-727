
/* Class135_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jaclib.memory.Buffer;

public class Class135_Sub2 extends Class135 implements Interface14 {
	int anInt9085;

	public long method6() {
		return 0L;
	}

	Class135_Sub2(Class505_Sub1 class505_sub1, int i, byte[] is, int i_0_, boolean bool) {
		super(class505_sub1, 34962, is, i_0_, bool);
		((Class135_Sub2) this).anInt9085 = i;
	}

	public void method106(int i, byte[] is, int i_1_) {
		method2342(is, i_1_);
		((Class135_Sub2) this).anInt9085 = i;
	}

	public int method59() {
		return ((Class135_Sub2) this).anInt9085;
	}

	public long method109() {
		return 0L;
	}

	void method2341() {
		((Class135_Sub2) this).aClass505_Sub1_1620.method13601(this);
	}

	public int method1() {
		return ((Class135_Sub2) this).anInt1622;
	}

	public int method3() {
		return ((Class135_Sub2) this).anInt1622;
	}

	public int method4() {
		return ((Class135_Sub2) this).anInt1622;
	}

	public int method75() {
		return ((Class135_Sub2) this).anInt9085;
	}

	void method2345() {
		((Class135_Sub2) this).aClass505_Sub1_1620.method13601(this);
	}

	public void method107(int i, byte[] is, int i_2_) {
		method2342(is, i_2_);
		((Class135_Sub2) this).anInt9085 = i;
	}

	public void method108(int i, byte[] is, int i_3_) {
		method2342(is, i_3_);
		((Class135_Sub2) this).anInt9085 = i;
	}

	Class135_Sub2(Class505_Sub1 class505_sub1, int i, Buffer buffer, int i_4_, boolean bool) {
		super(class505_sub1, 34962, buffer, i_4_, bool);
		((Class135_Sub2) this).anInt9085 = i;
	}

	void method2346() {
		((Class135_Sub2) this).aClass505_Sub1_1620.method13601(this);
	}
}
