
/* Class199 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;

public abstract class Class199 {
	static final int anInt2448 = 35;
	static final int anInt2449 = 1;
	public static final int anInt2450 = 96;
	static final int anInt2451 = 3;
	static final int anInt2452 = 4;
	static final int anInt2453 = 34;
	static final int anInt2454 = 6;
	static final int anInt2455 = 7;
	static final int anInt2456 = 22;
	static final int anInt2457 = 9;
	static final int anInt2458 = 27;
	static final int anInt2459 = 11;
	static final int anInt2460 = 12;
	static final int anInt2461 = 16;
	static final int anInt2462 = 5;
	public static final int anInt2463 = 101;
	static final int anInt2464 = 19;
	public static final int anInt2465 = 104;
	static final int anInt2466 = 21;
	static final int anInt2467 = 43;
	static final int anInt2468 = 23;
	static final int anInt2469 = 24;
	static final int anInt2470 = 25;
	public static final int anInt2471 = 81;
	public static final int anInt2472 = 67;
	static final int anInt2473 = 32;
	static final int anInt2474 = 33;
	static final int anInt2475 = 87;
	public static final int anInt2476 = 82;
	static final int anInt2477 = 18;
	static final int anInt2478 = 37;
	static final int anInt2479 = 38;
	static final int anInt2480 = 39;
	static final int anInt2481 = 40;
	static final int anInt2482 = 41;
	static final int anInt2483 = 70;
	static final int anInt2484 = 83;
	static final int anInt2485 = 48;
	static final int anInt2486 = 49;
	static final int anInt2487 = 50;
	static final int anInt2488 = 88;
	static final int anInt2489 = 52;
	static final int anInt2490 = 53;
	static final int anInt2491 = 54;
	static final int anInt2492 = 42;
	static final int anInt2493 = 56;
	static final int anInt2494 = 57;
	static final int anInt2495 = 58;
	static final int anInt2496 = 64;
	static final int anInt2497 = 65;
	public static final int anInt2498 = 66;
	public static final int anInt2499 = 85;
	static final int anInt2500 = 68;
	static final int anInt2501 = 69;
	static final int anInt2502 = 36;
	static final int anInt2503 = 71;
	static final int anInt2504 = 72;
	static final int anInt2505 = 73;
	public static final int anInt2506 = 80;
	static final int anInt2507 = 10;
	static final int anInt2508 = 55;
	public static final int anInt2509 = 86;
	public static final int anInt2510 = 105;
	static final int anInt2511 = 13;
	static final int anInt2512 = 2;
	static final int anInt2513 = 59;
	static final int anInt2514 = 20;
	static final int anInt2515 = 28;
	static final int anInt2516 = 74;
	static final int anInt2517 = 26;
	static final int anInt2518 = 89;
	static final int anInt2519 = 90;
	static final int anInt2520 = 91;
	public static final int anInt2521 = 84;
	public static final int anInt2522 = 97;
	static final int anInt2523 = 98;
	static final int anInt2524 = 99;
	static final int anInt2525 = 100;
	static final int anInt2526 = 8;
	public static final int anInt2527 = 102;
	public static final int anInt2528 = 103;
	static final int anInt2529 = 17;
	static final int anInt2530 = 51;

	public abstract Interface16 method3234();

	public abstract void method3235(byte i);

	public abstract boolean method3236(int i, int i_0_);

	public abstract Interface16 method3237(int i);

	public abstract void method3238(int i);

	public abstract Interface16 method3239();

	public abstract boolean method3240(int i);

	public static Class199 method3241(Component component) {
		return new Class199_Sub1(component);
	}

	public abstract boolean method3242(int i);

	public abstract Interface16 method3243();

	public abstract boolean method3244(int i);

	public abstract void method3245();

	public abstract void method3246();

	public abstract void method3247();

	public abstract void method3248();

	public abstract void method3249();

	Class199() {
		/* empty */
	}

	static final void method3250(Class527 class527, int i) {
		int i_1_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_1_, (byte) 33);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_1_ >> 16];
		Class13.method501(class118, class98, class527, (byte) -2);
	}

	static final void method3251(Class118 class118, Class527 class527, int i) {
		if (-613817135 * ((Class527) class527).anInt7015 >= 10)
			throw new RuntimeException();
		if (class118.anObjectArray1271 != null) {
			Class282_Sub43 class282_sub43 = new Class282_Sub43();
			class282_sub43.aClass118_8053 = class118;
			class282_sub43.anObjectArray8054 = class118.anObjectArray1271;
			((Class282_Sub43) class282_sub43).anInt8061 = 268621789 + 859311981 * ((Class527) class527).anInt7015;
			client.aClass482_7402.method8059(class282_sub43, -2001342586);
		}
	}

	static void method3252(int i, boolean bool, int i_2_) {
		Class282_Sub50_Sub12 class282_sub50_sub12 = Class263.method4778(22, (long) i);
		class282_sub50_sub12.method14995(2035738525);
		((Class282_Sub50_Sub12) class282_sub50_sub12).anInt9668 = (bool ? 1 : 0) * -1773141545;
	}
}
