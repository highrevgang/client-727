/* Class115 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class115 implements Interface7 {
	protected String aString1244;
	protected String aString1245 = null;
	protected String aString1246;
	public static int anInt1247;
	static Class160[] aClass160Array1248;
	static int anInt1249;

	abstract void method1891(Class282_Sub21_Sub1 class282_sub21_sub1, Class384 class384);

	String method1892(byte i) {
		return aString1245;
	}

	abstract void method1893(int i, float f, float f_0_, float f_1_);

	abstract void method1894(Class282_Sub21_Sub1 class282_sub21_sub1, float f);

	abstract void method1895(int i, float f, float f_2_, float f_3_, float f_4_);

	abstract void method1896(Class282_Sub21_Sub1 class282_sub21_sub1, int i, Interface30 interface30);

	abstract void method1897(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_5_, float f_6_, float f_7_);

	public abstract boolean method1898();

	public abstract boolean method1899();

	abstract void method1900(int i, Class384 class384);

	abstract void method1901(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_8_, float f_9_, float f_10_);

	abstract void method1902(int i, Class384 class384);

	abstract void method1903(Class282_Sub21_Sub1 class282_sub21_sub1, float[] fs, int i);

	abstract void method1904(int i, float[] fs, int i_11_);

	abstract void method1905(int i, Class384 class384);

	abstract void method1906(int i, Class384 class384);

	abstract void method1907(int i, float f, float f_12_, float f_13_, float f_14_);

	abstract void method1908(int i, int i_15_, Interface30 interface30);

	String method1909() {
		return aString1245;
	}

	abstract void method1910(int i, Class384 class384);

	public abstract boolean method1911();

	abstract void method1912(Class282_Sub21_Sub1 class282_sub21_sub1, float f);

	abstract void method1913(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_16_);

	abstract void method1914(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_17_, float f_18_);

	abstract void method1915(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_19_, float f_20_);

	abstract void method1916(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_21_, float f_22_);

	abstract void method1917(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_23_, float f_24_, float f_25_);

	abstract void method1918(Class282_Sub21_Sub1 class282_sub21_sub1, float[] fs, int i);

	abstract void method1919(Class282_Sub21_Sub1 class282_sub21_sub1, float[] fs, int i);

	abstract void method1920(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_26_, float f_27_, float f_28_);

	abstract void method1921(Class282_Sub21_Sub1 class282_sub21_sub1, float[] fs, int i);

	abstract void method1922(int i, Class384 class384);

	Class115() {
		/* empty */
	}

	abstract void method1923(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_29_);

	abstract void method1924(Class282_Sub21_Sub1 class282_sub21_sub1, Class384 class384);

	abstract void method1925(Class282_Sub21_Sub1 class282_sub21_sub1, Class384 class384);

	abstract void method1926(Class282_Sub21_Sub1 class282_sub21_sub1, Class384 class384);

	abstract void method1927(Class282_Sub21_Sub1 class282_sub21_sub1, Class384 class384);

	abstract void method1928(Class282_Sub21_Sub1 class282_sub21_sub1, int i, Interface30 interface30);

	abstract void method1929(Class282_Sub21_Sub1 class282_sub21_sub1, int i, Interface30 interface30);

	abstract void method1930(Class282_Sub21_Sub1 class282_sub21_sub1, int i, Interface30 interface30);

	abstract void method1931(int i, float f, float f_30_, float f_31_);

	abstract void method1932(int i, float f, float f_32_, float f_33_, float f_34_);

	abstract void method1933(int i, float f, float f_35_, float f_36_, float f_37_);

	abstract void method1934(int i, float f, float f_38_, float f_39_, float f_40_);

	abstract void method1935(int i, float f, float f_41_, float f_42_, float f_43_);

	abstract void method1936(Class282_Sub21_Sub1 class282_sub21_sub1, Class384 class384);

	abstract void method1937(int i, float[] fs, int i_44_);

	abstract void method1938(int i, float[] fs, int i_45_);

	abstract void method1939(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_46_, float f_47_);

	abstract void method1940(int i, Class384 class384);

	abstract void method1941(Class282_Sub21_Sub1 class282_sub21_sub1, float f, float f_48_, float f_49_, float f_50_);

	abstract void method1942(int i, Class384 class384);

	abstract void method1943(int i, Class384 class384);

	abstract void method1944(Class282_Sub21_Sub1 class282_sub21_sub1, float[] fs, int i);

	abstract void method1945(int i, int i_51_, Interface30 interface30);

	abstract void method1946(int i, int i_52_, Interface30 interface30);

	abstract void method1947(int i, int i_53_, Interface30 interface30);

	abstract void method1948(int i, int i_54_, Interface30 interface30);

	static final void method1949(Class527 class527, int i) {
		Class513 class513 = (((Class527) class527).aBool7022 ? ((Class527) class527).aClass513_6994 : ((Class527) class527).aClass513_7007);
		Class118 class118 = ((Class513) class513).aClass118_5886;
		Class98 class98 = ((Class513) class513).aClass98_5885;
		Class17.method569(class118, class98, class527, 2101137019);
	}

	public static boolean method1950(CharSequence charsequence, int i) {
		return Class272.method4841(charsequence, 10, true, 547705693);
	}

	static final void method1951(Class527 class527, byte i) {
		int i_55_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		int i_56_ = client.aClass330Array7428[i_55_].method5908(-1286970996);
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = i_56_ == 5 ? 1 : 0;
	}

	public static boolean method1952(boolean bool, int i) {
		boolean bool_57_ = Class316.aClass505_3680.method8471();
		if (bool_57_ != bool) {
			if (bool) {
				if (!Class316.aClass505_3680.method8469())
					bool = false;
			} else
				Class316.aClass505_3680.method8490();
			if (bool_57_ != bool) {
				Class393.aClass282_Sub54_4783.method13511(Class393.aClass282_Sub54_4783.aClass468_Sub12_8195, bool ? 1 : 0, -2016706726);
				Class190.method3148((byte) 111);
				return true;
			}
			return false;
		}
		return true;
	}
}
