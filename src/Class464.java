/* Class464 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class Class464 {
	Class275_Sub1 aClass275_Sub1_5553;
	public Class275_Sub1 aClass275_Sub1_5554 = new Class275_Sub1();
	static String aString5555;
	public static boolean aBool5556;

	public Class275_Sub1 method7731(int i) {
		Class275_Sub1 class275_sub1 = aClass275_Sub1_5554.aClass275_Sub1_7706;
		if (aClass275_Sub1_5554 == class275_sub1) {
			((Class464) this).aClass275_Sub1_5553 = null;
			return null;
		}
		((Class464) this).aClass275_Sub1_5553 = class275_sub1.aClass275_Sub1_7706;
		return class275_sub1;
	}

	public void method7732() {
		for (;;) {
			Class275_Sub1 class275_sub1 = aClass275_Sub1_5554.aClass275_Sub1_7706;
			if (class275_sub1 == aClass275_Sub1_5554)
				break;
			class275_sub1.method12423((byte) -20);
		}
		((Class464) this).aClass275_Sub1_5553 = null;
	}

	public void method7733() {
		for (;;) {
			Class275_Sub1 class275_sub1 = aClass275_Sub1_5554.aClass275_Sub1_7706;
			if (class275_sub1 == aClass275_Sub1_5554)
				break;
			class275_sub1.method12423((byte) -81);
		}
		((Class464) this).aClass275_Sub1_5553 = null;
	}

	public Class275_Sub1 method7734(byte i) {
		Class275_Sub1 class275_sub1 = ((Class464) this).aClass275_Sub1_5553;
		if (aClass275_Sub1_5554 == class275_sub1) {
			((Class464) this).aClass275_Sub1_5553 = null;
			return null;
		}
		((Class464) this).aClass275_Sub1_5553 = class275_sub1.aClass275_Sub1_7706;
		return class275_sub1;
	}

	public void method7735(Class275_Sub1 class275_sub1, int i) {
		if (null != class275_sub1.aClass275_Sub1_7707)
			class275_sub1.method12423((byte) -20);
		class275_sub1.aClass275_Sub1_7707 = aClass275_Sub1_5554.aClass275_Sub1_7707;
		class275_sub1.aClass275_Sub1_7706 = aClass275_Sub1_5554;
		class275_sub1.aClass275_Sub1_7707.aClass275_Sub1_7706 = class275_sub1;
		class275_sub1.aClass275_Sub1_7706.aClass275_Sub1_7707 = class275_sub1;
	}

	public int method7736(int i) {
		int i_0_ = 0;
		for (Class275_Sub1 class275_sub1 = aClass275_Sub1_5554.aClass275_Sub1_7706; class275_sub1 != aClass275_Sub1_5554; class275_sub1 = class275_sub1.aClass275_Sub1_7706)
			i_0_++;
		return i_0_;
	}

	public void method7737(Class275_Sub1 class275_sub1) {
		if (null != class275_sub1.aClass275_Sub1_7707)
			class275_sub1.method12423((byte) -55);
		class275_sub1.aClass275_Sub1_7707 = aClass275_Sub1_5554.aClass275_Sub1_7707;
		class275_sub1.aClass275_Sub1_7706 = aClass275_Sub1_5554;
		class275_sub1.aClass275_Sub1_7707.aClass275_Sub1_7706 = class275_sub1;
		class275_sub1.aClass275_Sub1_7706.aClass275_Sub1_7707 = class275_sub1;
	}

	public void method7738(Class275_Sub1 class275_sub1) {
		if (null != class275_sub1.aClass275_Sub1_7707)
			class275_sub1.method12423((byte) -42);
		class275_sub1.aClass275_Sub1_7707 = aClass275_Sub1_5554.aClass275_Sub1_7707;
		class275_sub1.aClass275_Sub1_7706 = aClass275_Sub1_5554;
		class275_sub1.aClass275_Sub1_7707.aClass275_Sub1_7706 = class275_sub1;
		class275_sub1.aClass275_Sub1_7706.aClass275_Sub1_7707 = class275_sub1;
	}

	public Class275_Sub1 method7739() {
		Class275_Sub1 class275_sub1 = aClass275_Sub1_5554.aClass275_Sub1_7706;
		if (aClass275_Sub1_5554 == class275_sub1) {
			((Class464) this).aClass275_Sub1_5553 = null;
			return null;
		}
		((Class464) this).aClass275_Sub1_5553 = class275_sub1.aClass275_Sub1_7706;
		return class275_sub1;
	}

	public void method7740(int i) {
		for (;;) {
			Class275_Sub1 class275_sub1 = aClass275_Sub1_5554.aClass275_Sub1_7706;
			if (class275_sub1 == aClass275_Sub1_5554)
				break;
			class275_sub1.method12423((byte) -118);
		}
		((Class464) this).aClass275_Sub1_5553 = null;
	}

	public Class464() {
		aClass275_Sub1_5554.aClass275_Sub1_7706 = aClass275_Sub1_5554;
		aClass275_Sub1_5554.aClass275_Sub1_7707 = aClass275_Sub1_5554;
	}

	public void method7741() {
		for (;;) {
			Class275_Sub1 class275_sub1 = aClass275_Sub1_5554.aClass275_Sub1_7706;
			if (class275_sub1 == aClass275_Sub1_5554)
				break;
			class275_sub1.method12423((byte) -74);
		}
		((Class464) this).aClass275_Sub1_5553 = null;
	}

	static boolean method7742(int i, byte i_1_) {
		return 11 == i || i == 2 || i == 18 || i == 6 || i == 9;
	}

	static final void method7743(Class118[] class118s, int i, byte i_2_) {
		for (int i_3_ = 0; i_3_ < class118s.length; i_3_++) {
			Class118 class118 = class118s[i_3_];
			if (class118 != null && 2110532063 * class118.anInt1305 == i && !client.method11651(class118)) {
				if (0 == class118.anInt1268 * -2131393857) {
					method7743(class118s, -1952846363 * class118.anInt1287, (byte) 10);
					if (class118.aClass118Array1439 != null)
						method7743(class118.aClass118Array1439, class118.anInt1287 * -1952846363, (byte) 10);
					Class282_Sub44 class282_sub44 = ((Class282_Sub44) (client.aClass465_7442.method7754((long) (-1952846363 * class118.anInt1287))));
					if (class282_sub44 != null)
						Class513.method8777((587626901 * class282_sub44.anInt8063), (byte) -57);
				}
				if (-2131393857 * class118.anInt1268 == 6 && 1241177935 * class118.anInt1321 != -1) {
					if (class118.aClass456_1437 == null) {
						class118.aClass456_1437 = new Class456_Sub1();
						class118.aClass456_1437.method7567((class118.anInt1321 * 1241177935), (short) 8960);
					}
					if (class118.aClass456_1437.method7627((client.anInt7261 * -383924731), 1231441216) && class118.aClass456_1437.method7580(602854407))
						class118.aClass456_1437.method7582((byte) -59);
				}
			}
		}
	}

	static final void method7744(Class527 class527, int i) {
		((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 += 141891001) * 1942118537 - 1] = -704900783 * Class9.anInt94;
	}
}
