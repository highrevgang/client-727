/* Class282_Sub15_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub15_Sub2 extends Class282_Sub15 {
	int anInt9690;
	int[] anIntArray9691;
	int anInt9692 = 183543552;
	long aLong9693;
	Class465 aClass465_9694;
	int anInt9695;
	int anInt9696;
	int[] anIntArray9697;
	int[] anIntArray9698;
	int[] anIntArray9699;
	int[] anIntArray9700;
	int[] anIntArray9701;
	int[] anIntArray9702;
	int[] anIntArray9703;
	int[] anIntArray9704;
	static final int anInt9705 = 2;
	static final int anInt9706 = 4;
	int[] anIntArray9707;
	int[] anIntArray9708;
	int[] anIntArray9709;
	long aLong9710;
	int[] anIntArray9711;
	int[] anIntArray9712;
	Class282_Sub13[][] aClass282_Sub13ArrayArray9713;
	Class282_Sub13[][] aClass282_Sub13ArrayArray9714;
	Class80 aClass80_9715;
	boolean aBool9716;
	int anInt9717;
	static final int anInt9718 = 1;
	int anInt9719;
	int[] anIntArray9720;
	Class282_Sub15_Sub3 aClass282_Sub15_Sub3_9721;
	Class282_Sub7 aClass282_Sub7_9722;
	int[] anIntArray9723;
	boolean aBool9724;

	void method15091(int i, int i_0_, byte i_1_) {
		((Class282_Sub15_Sub2) this).anIntArray9701[i] = i_0_;
	}

	public Class282_Sub15_Sub2(Class282_Sub15_Sub2 class282_sub15_sub2_2_) {
		((Class282_Sub15_Sub2) this).anInt9690 = 2137157376;
		((Class282_Sub15_Sub2) this).anInt9695 = -340992960;
		((Class282_Sub15_Sub2) this).anIntArray9702 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9711 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9691 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9697 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9698 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9699 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9700 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9701 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9720 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9703 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9707 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9708 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9709 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9704 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9723 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9712 = new int[16];
		((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713 = new Class282_Sub13[16][128];
		((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714 = new Class282_Sub13[16][128];
		((Class282_Sub15_Sub2) this).aClass80_9715 = new Class80();
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721 = new Class282_Sub15_Sub3(this);
		((Class282_Sub15_Sub2) this).aClass465_9694 = ((Class282_Sub15_Sub2) class282_sub15_sub2_2_).aClass465_9694;
		method15095(-1, 256, 1388598652);
		method15116(true, -716297982);
	}

	boolean method15092(Class282_Sub13 class282_sub13) {
		if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568 == null) {
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)] = null;
			}
			return true;
		}
		return false;
	}

	synchronized void method15093() {
		method15102(true, -1527277802);
	}

	synchronized void method15094(int i, int i_3_) {
		((Class282_Sub15_Sub2) this).anInt9690 = i * -1954586001;
	}

	synchronized void method15095(int i, int i_4_, int i_5_) {
		if (i < 0) {
			for (int i_6_ = 0; i_6_ < 16; i_6_++)
				((Class282_Sub15_Sub2) this).anIntArray9702[i_6_] = i_4_;
		} else
			((Class282_Sub15_Sub2) this).anIntArray9702[i] = i_4_;
	}

	synchronized void method12230(int[] is, int i, int i_7_) {
		if (((Class282_Sub15_Sub2) this).aClass80_9715.method1409()) {
			int i_8_ = (((Class282_Sub15_Sub2) this).anInt9695 * -33380583 * (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anInt785) / Class253.anInt3129);
			do {
				long l = ((long) i_8_ * (long) i_7_ + (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710));
				if ((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - l >= 0L) {
					((Class282_Sub15_Sub2) this).aLong9710 = l * 8282832003758463051L;
					break;
				}
				int i_9_ = (int) (((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710) + (long) i_8_ - 1L) / (long) i_8_);
				((Class282_Sub15_Sub2) this).aLong9710 += 8282832003758463051L * ((long) i_9_ * (long) i_8_);
				((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_9_);
				i += i_9_;
				i_7_ -= i_9_;
				method15127(2053011749);
			} while (((Class282_Sub15_Sub2) this).aClass80_9715.method1409());
		}
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_7_);
	}

	synchronized void method15096(int i) {
		for (Class282_Sub14 class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7750(708245763); null != class282_sub14; class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7751((byte) 107))
			class282_sub14.method12217((short) -2455);
	}

	synchronized void method15097(short i) {
		for (Class282_Sub14 class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7750(-1836926680); class282_sub14 != null; class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7751((byte) 110)) {
			if (i != 256)
				break;
			class282_sub14.method4991(-371378792);
		}
	}

	synchronized void method15098(Class282_Sub7 class282_sub7, boolean bool, int i) {
		method15131(class282_sub7, bool, true, (byte) -90);
	}

	void method15099(Class282_Sub13 class282_sub13, boolean bool, byte i) {
		int i_10_ = ((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.method15223();
		int i_11_;
		if (bool && (((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.aBool9752)) {
			int i_12_ = (i_10_ + i_10_ - -69474713 * (((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.anInt9749));
			i_11_ = (int) ((long) (((Class282_Sub15_Sub2) this).anIntArray9704[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * (long) i_12_ >> 6);
			i_10_ <<= 8;
			if (i_11_ >= i_10_) {
				i_11_ = i_10_ + i_10_ - 1 - i_11_;
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15313(true, -1914044650);
			}
		} else
			i_11_ = (int) ((long) (((Class282_Sub15_Sub2) this).anIntArray9704[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * (long) i_10_ >> 6);
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15317(i_11_, -730360599);
	}

	synchronized void method15100(Class282_Sub7 class282_sub7, boolean bool, boolean bool_13_, long l) {
		method15131(class282_sub7, bool, bool_13_, (byte) -65);
		method15126((long) ((Class80) (((Class282_Sub15_Sub2) this).aClass80_9715)).anInt785 * l);
	}

	synchronized void method15101(short i) {
		method15102(true, -1762277091);
	}

	synchronized void method15102(boolean bool, int i) {
		((Class282_Sub15_Sub2) this).aClass80_9715.method1395();
		((Class282_Sub15_Sub2) this).aClass282_Sub7_9722 = null;
		method15116(bool, -1895797200);
	}

	synchronized boolean method15103(int i) {
		return ((Class282_Sub15_Sub2) this).aClass80_9715.method1409();
	}

	void method15104(int i, int i_14_, byte i_15_) {
		((Class282_Sub15_Sub2) this).anIntArray9723[i] = i_14_;
		((Class282_Sub15_Sub2) this).anIntArray9712[i] = (int) (2097152.0 * Math.pow(2.0, (double) i_14_ * 5.4931640625E-4) + 0.5);
	}

	void method15105(int i, int i_16_, int i_17_) {
		((Class282_Sub15_Sub2) this).anIntArray9698[i] = i_16_;
		((Class282_Sub15_Sub2) this).anIntArray9700[i] = i_16_ & ~0x7f;
		method15189(i, i_16_, -1424510729);
	}

	void method15106(int i) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2) != 0) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 13)); class282_sub13 != null; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(448447573))) {
				if ((((Class282_Sub13) class282_sub13).anInt7582 * 846406309 == i) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][(((Class282_Sub13) class282_sub13).anInt7569 * 1293000467)]) == null && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 < 0))
					((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	void method15107(int i, int i_18_, int i_19_, int i_20_) {
		method15109(i, i_18_, 64, (byte) -17);
		if (0 != (((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2)) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8065(-2033543523)); null != class282_sub13; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8068((byte) 87))) {
				if ((846406309 * ((Class282_Sub13) class282_sub13).anInt7582 == i) && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 < 0)) {
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][(((Class282_Sub13) class282_sub13).anInt7569 * 1293000467)] = null;
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][i_18_] = class282_sub13;
					int i_21_ = (((-1958822223 * ((Class282_Sub13) class282_sub13).anInt7575 * (-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574)) >> 12) + (((Class282_Sub13) class282_sub13).anInt7573 * 833000445));
					((Class282_Sub13) class282_sub13).anInt7573 += (-1686740651 * (i_18_ - (((Class282_Sub13) class282_sub13).anInt7569) * 1293000467 << 8));
					((Class282_Sub13) class282_sub13).anInt7574 = 1311270507 * (i_21_ - 833000445 * (((Class282_Sub13) class282_sub13).anInt7573));
					((Class282_Sub13) class282_sub13).anInt7575 = -324726784;
					((Class282_Sub13) class282_sub13).anInt7569 = i_18_ * -897776869;
					return;
				}
			}
		}
		Class282_Sub14 class282_sub14 = ((Class282_Sub14) (((Class282_Sub15_Sub2) this).aClass465_9694.method7754((long) ((Class282_Sub15_Sub2) this).anIntArray9699[i])));
		if (class282_sub14 != null) {
			Class282_Sub26_Sub1 class282_sub26_sub1 = (((Class282_Sub14) class282_sub14).aClass282_Sub26_Sub1Array7589[i_18_]);
			if (null != class282_sub26_sub1) {
				Class282_Sub13 class282_sub13 = new Class282_Sub13();
				((Class282_Sub13) class282_sub13).anInt7582 = -817892051 * i;
				((Class282_Sub13) class282_sub13).aClass282_Sub14_7566 = class282_sub14;
				((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565 = class282_sub26_sub1;
				((Class282_Sub13) class282_sub13).aClass110_7586 = (((Class282_Sub14) class282_sub14).aClass110Array7594[i_18_]);
				((Class282_Sub13) class282_sub13).anInt7567 = 1410078841 * (((Class282_Sub14) class282_sub14).aByteArray7595[i_18_]);
				((Class282_Sub13) class282_sub13).anInt7569 = i_18_ * -897776869;
				((Class282_Sub13) class282_sub13).anInt7570 = (-1232501025 * ((i_19_ * i_19_ * (1850100591 * ((Class282_Sub14) class282_sub14).anInt7593) * (((Class282_Sub14) class282_sub14).aByteArray7592[i_18_])) + 1024 >> 11));
				((Class282_Sub13) class282_sub13).anInt7572 = (((Class282_Sub14) class282_sub14).aByteArray7591[i_18_] & 0xff) * 980559453;
				((Class282_Sub13) class282_sub13).anInt7573 = -1686740651 * ((i_18_ << 8) - ((((Class282_Sub14) class282_sub14).aShortArray7590[i_18_]) & 0x7fff));
				((Class282_Sub13) class282_sub13).anInt7576 = 0;
				((Class282_Sub13) class282_sub13).anInt7577 = 0;
				((Class282_Sub13) class282_sub13).anInt7578 = 0;
				((Class282_Sub13) class282_sub13).anInt7579 = -523944391;
				((Class282_Sub13) class282_sub13).anInt7580 = 0;
				if (((Class282_Sub15_Sub2) this).anIntArray9704[i] == 0)
					((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568 = (class282_sub26_sub1.method15225(method15120(class282_sub13, 1301568554), method15108(class282_sub13, -1972302167), method15122(class282_sub13, 428915488)));
				else {
					((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568 = (class282_sub26_sub1.method15225(method15120(class282_sub13, -893425928), 0, method15122(class282_sub13, -1110039207)));
					method15099(class282_sub13, (((Class282_Sub14) class282_sub14).aShortArray7590[i_18_]) < 0, (byte) 32);
				}
				if (((Class282_Sub14) class282_sub14).aShortArray7590[i_18_] < 0)
					((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15325(-1, (byte) 8);
				if (((Class282_Sub13) class282_sub13).anInt7567 * -2127113783 >= 0) {
					Class282_Sub13 class282_sub13_22_ = (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[i][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)]);
					if (class282_sub13_22_ != null && (((Class282_Sub13) class282_sub13_22_).anInt7579 * 761835511) < 0) {
						((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][(((Class282_Sub13) class282_sub13_22_).anInt7569 * 1293000467)] = null;
						((Class282_Sub13) class282_sub13_22_).anInt7579 = 0;
					}
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[i][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)] = class282_sub13;
				}
				((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8059(class282_sub13, 95149405);
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][i_18_] = class282_sub13;
			}
		}
	}

	int method15108(Class282_Sub13 class282_sub13, int i) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9702[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) == 0)
			return 0;
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		int i_23_ = ((((((Class282_Sub15_Sub2) this).anIntArray9697[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]) * (((Class282_Sub15_Sub2) this).anIntArray9711[846406309 * ((Class282_Sub13) class282_sub13).anInt7582])) + 4096) >> 13);
		i_23_ = i_23_ * i_23_ + 16384 >> 15;
		i_23_ = 16384 + (((Class282_Sub13) class282_sub13).anInt7570 * 1470346527 * i_23_) >> 15;
		i_23_ = (1824241431 * ((Class282_Sub15_Sub2) this).anInt9692 * i_23_ + 128) >> 8;
		i_23_ = (i_23_ * (((Class282_Sub15_Sub2) this).anInt9690 * 513424527) >> 8);
		i_23_ = i_23_ * (((Class282_Sub15_Sub2) this).anIntArray9702[846406309 * (((Class282_Sub13) class282_sub13).anInt7582)]) + 128 >> 8;
		if (((Class110) class110).anInt1096 * 1284723511 > 0)
			i_23_ = (int) (((double) i_23_ * Math.pow(0.5, ((double) (69449581 * ((Class282_Sub13) class282_sub13).anInt7576) * 1.953125E-5 * (double) (1284723511 * (((Class110) class110).anInt1096))))) + 0.5);
		if (null != ((Class110) class110).aByteArray1097) {
			int i_24_ = ((Class282_Sub13) class282_sub13).anInt7577 * -474562881;
			int i_25_ = (((Class110) class110).aByteArray1097[1 + (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405)]);
			if (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) {
				int i_26_ = (((((Class110) class110).aByteArray1097[1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8);
				int i_27_ = ((((Class110) class110).aByteArray1097[2 + (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405)]) & 0xff) << 8;
				i_25_ += ((((Class110) class110).aByteArray1097[(((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) + 3]) - i_25_) * (i_24_ - i_26_) / (i_27_ - i_26_);
			}
			i_23_ = i_23_ * i_25_ + 32 >> 6;
		}
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 > 0 && ((Class110) class110).aByteArray1094 != null) {
			int i_28_ = 761835511 * ((Class282_Sub13) class282_sub13).anInt7579;
			int i_29_ = (((Class110) class110).aByteArray1094[1 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]);
			if (-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) {
				int i_30_ = (((((Class110) class110).aByteArray1094[(((Class282_Sub13) class282_sub13).anInt7580 * -9799685)]) & 0xff) << 8);
				int i_31_ = (((((Class110) class110).aByteArray1094[(((Class282_Sub13) class282_sub13).anInt7580 * -9799685) + 2]) & 0xff) << 8);
				i_29_ += ((((Class110) class110).aByteArray1094[3 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) - i_29_) * (i_28_ - i_30_) / (i_31_ - i_30_);
			}
			i_23_ = i_29_ * i_23_ + 32 >> 6;
		}
		return i_23_;
	}

	void method15109(int i, int i_32_, int i_33_, byte i_34_) {
		Class282_Sub13 class282_sub13 = (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][i_32_]);
		if (class282_sub13 != null) {
			((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][i_32_] = null;
			if (0 != (((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2)) {
				for (Class282_Sub13 class282_sub13_35_ = (Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 52); null != class282_sub13_35_; class282_sub13_35_ = (Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(1921958128)) {
					if (((((Class282_Sub13) class282_sub13).anInt7582 * 846406309) == (((Class282_Sub13) class282_sub13_35_).anInt7582 * 846406309)) && 761835511 * (((Class282_Sub13) class282_sub13_35_).anInt7579) < 0 && class282_sub13_35_ != class282_sub13) {
						((Class282_Sub13) class282_sub13).anInt7579 = 0;
						break;
					}
				}
			} else
				((Class282_Sub13) class282_sub13).anInt7579 = 0;
		}
	}

	int method15110(Class282_Sub13 class282_sub13) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9702[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) == 0)
			return 0;
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		int i = ((((((Class282_Sub15_Sub2) this).anIntArray9697[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]) * (((Class282_Sub15_Sub2) this).anIntArray9711[846406309 * ((Class282_Sub13) class282_sub13).anInt7582])) + 4096) >> 13);
		i = i * i + 16384 >> 15;
		i = 16384 + (((Class282_Sub13) class282_sub13).anInt7570 * 1470346527 * i) >> 15;
		i = 1824241431 * ((Class282_Sub15_Sub2) this).anInt9692 * i + 128 >> 8;
		i = i * (((Class282_Sub15_Sub2) this).anInt9690 * 513424527) >> 8;
		i = (i * (((Class282_Sub15_Sub2) this).anIntArray9702[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]) + 128) >> 8;
		if (((Class110) class110).anInt1096 * 1284723511 > 0)
			i = (int) (((double) i * Math.pow(0.5, ((double) (69449581 * ((Class282_Sub13) class282_sub13).anInt7576) * 1.953125E-5 * (double) (1284723511 * (((Class110) class110).anInt1096))))) + 0.5);
		if (null != ((Class110) class110).aByteArray1097) {
			int i_36_ = ((Class282_Sub13) class282_sub13).anInt7577 * -474562881;
			int i_37_ = (((Class110) class110).aByteArray1097[1 + (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405)]);
			if (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) {
				int i_38_ = (((((Class110) class110).aByteArray1097[1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8);
				int i_39_ = ((((Class110) class110).aByteArray1097[2 + (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405)]) & 0xff) << 8;
				i_37_ += ((((Class110) class110).aByteArray1097[(((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) + 3]) - i_37_) * (i_36_ - i_38_) / (i_39_ - i_38_);
			}
			i = i * i_37_ + 32 >> 6;
		}
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 > 0 && ((Class110) class110).aByteArray1094 != null) {
			int i_40_ = 761835511 * ((Class282_Sub13) class282_sub13).anInt7579;
			int i_41_ = (((Class110) class110).aByteArray1094[1 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]);
			if (-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) {
				int i_42_ = (((((Class110) class110).aByteArray1094[(((Class282_Sub13) class282_sub13).anInt7580 * -9799685)]) & 0xff) << 8);
				int i_43_ = (((((Class110) class110).aByteArray1094[(((Class282_Sub13) class282_sub13).anInt7580 * -9799685) + 2]) & 0xff) << 8);
				i_41_ += ((((Class110) class110).aByteArray1094[3 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) - i_41_) * (i_40_ - i_42_) / (i_43_ - i_42_);
			}
			i = i_41_ * i + 32 >> 6;
		}
		return i;
	}

	void method15111(int i, byte i_44_) {
		for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8097((byte) 78)); class282_sub13 != null; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8067(-1020703673))) {
			if (i < 0 || (846406309 * ((Class282_Sub13) class282_sub13).anInt7582 == i)) {
				if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568 != null) {
					((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15322(Class253.anInt3129 / 100, (byte) -106);
					if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15311((byte) 33))
						((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass282_Sub15_Sub4_9755.method15275(((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568);
					class282_sub13.method12213(-2085241547);
				}
				if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 < 0)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7569 * 1293000467)] = null;
				class282_sub13.method4991(-371378792);
			}
		}
	}

	void method15112(int i, int i_45_) {
		/* empty */
	}

	int method15113(Class282_Sub13 class282_sub13) {
		int i = (((-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574 * (-1958822223 * ((Class282_Sub13) class282_sub13).anInt7575)) >> 12) + 833000445 * ((Class282_Sub13) class282_sub13).anInt7573);
		i += ((((((Class282_Sub15_Sub2) this).anIntArray9701[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]) - 8192) * (((Class282_Sub15_Sub2) this).anIntArray9709[846406309 * ((Class282_Sub13) class282_sub13).anInt7582])) >> 12);
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		if (-1304717473 * ((Class110) class110).anInt1101 > 0 && (((Class110) class110).anInt1100 * 1255754511 > 0 || ((((Class282_Sub15_Sub2) this).anIntArray9720[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) > 0))) {
			int i_46_ = ((Class110) class110).anInt1100 * 1255754511 << 2;
			int i_47_ = 1253259197 * ((Class110) class110).anInt1102 << 1;
			if (((Class282_Sub13) class282_sub13).anInt7581 * 346269105 < i_47_)
				i_46_ = i_46_ * (((Class282_Sub13) class282_sub13).anInt7581 * 346269105) / i_47_;
			i_46_ += ((((Class282_Sub15_Sub2) this).anIntArray9720[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) >> 7);
			double d = Math.sin(0.01227184630308513 * (double) ((((Class282_Sub13) class282_sub13).anInt7571) * -70351715 & 0x1ff));
			i += (int) (d * (double) i_46_);
		}
		int i_48_ = (int) (((double) (1034460416 * (((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.anInt9750)) * Math.pow(2.0, 3.255208333333333E-4 * (double) i) / (double) Class253.anInt3129) + 0.5);
		return i_48_ < 1 ? 1 : i_48_;
	}

	void method15114(int i, int i_49_) {
		if (i < 0) {
			for (i = 0; i < 16; i++)
				method15114(i, 51239021);
		} else {
			((Class282_Sub15_Sub2) this).anIntArray9711[i] = 12800;
			((Class282_Sub15_Sub2) this).anIntArray9691[i] = 8192;
			((Class282_Sub15_Sub2) this).anIntArray9697[i] = 16383;
			((Class282_Sub15_Sub2) this).anIntArray9701[i] = 8192;
			((Class282_Sub15_Sub2) this).anIntArray9720[i] = 0;
			((Class282_Sub15_Sub2) this).anIntArray9703[i] = 8192;
			method15117(i, 1241350830);
			method15118(i, (byte) 41);
			((Class282_Sub15_Sub2) this).anIntArray9707[i] = 0;
			((Class282_Sub15_Sub2) this).anIntArray9708[i] = 32767;
			((Class282_Sub15_Sub2) this).anIntArray9709[i] = 256;
			((Class282_Sub15_Sub2) this).anIntArray9704[i] = 0;
			method15104(i, 8192, (byte) -124);
		}
	}

	boolean method15115(Class282_Sub13 class282_sub13, int i) {
		if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568 == null) {
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)] = null;
			}
			return true;
		}
		return false;
	}

	void method15116(boolean bool, int i) {
		if (bool)
			method15111(-1, (byte) 34);
		else
			method15192(-1, 1739757097);
		method15114(-1, 51239021);
		for (int i_50_ = 0; i_50_ < 16; i_50_++)
			((Class282_Sub15_Sub2) this).anIntArray9699[i_50_] = ((Class282_Sub15_Sub2) this).anIntArray9698[i_50_];
		for (int i_51_ = 0; i_51_ < 16; i_51_++)
			((Class282_Sub15_Sub2) this).anIntArray9700[i_51_] = ((Class282_Sub15_Sub2) this).anIntArray9698[i_51_] & ~0x7f;
	}

	void method15117(int i, int i_52_) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2) != 0) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 108)); class282_sub13 != null; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(1357849272))) {
				if ((((Class282_Sub13) class282_sub13).anInt7582 * 846406309 == i) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][(((Class282_Sub13) class282_sub13).anInt7569 * 1293000467)]) == null && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 < 0))
					((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	void method15118(int i, byte i_53_) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x4) != 0) {
			if (i_53_ > 1) {
				for (Class282_Sub13 class282_sub13 = (Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 107); null != class282_sub13; class282_sub13 = (Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(1196631772)) {
					if (i_53_ <= 1)
						break;
					if (846406309 * ((Class282_Sub13) class282_sub13).anInt7582 == i) {
						if (i_53_ <= 1)
							break;
						((Class282_Sub13) class282_sub13).anInt7583 = 0;
					}
				}
			}
		}
	}

	void method15119(int i, int i_54_) {
		int i_55_ = i & 0xf0;
		if (128 == i_55_) {
			int i_56_ = i & 0xf;
			int i_57_ = i >> 8 & 0x7f;
			int i_58_ = i >> 16 & 0x7f;
			method15109(i_56_, i_57_, i_58_, (byte) 2);
		} else if (144 == i_55_) {
			int i_59_ = i & 0xf;
			int i_60_ = i >> 8 & 0x7f;
			int i_61_ = i >> 16 & 0x7f;
			if (i_61_ > 0)
				method15107(i_59_, i_60_, i_61_, 850987057);
			else
				method15109(i_59_, i_60_, 64, (byte) -42);
		} else if (160 == i_55_) {
			int i_62_ = i & 0xf;
			int i_63_ = i >> 8 & 0x7f;
			int i_64_ = i >> 16 & 0x7f;
			method15188(i_62_, i_63_, i_64_, 1251880068);
		} else if (176 == i_55_) {
			int i_65_ = i & 0xf;
			int i_66_ = i >> 8 & 0x7f;
			int i_67_ = i >> 16 & 0x7f;
			if (i_66_ == 0)
				((Class282_Sub15_Sub2) this).anIntArray9700[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9700[i_65_] & ~0x1fc000) + (i_67_ << 14);
			if (32 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9700[i_65_] = ((i_67_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9700[i_65_] & ~0x3f80));
			if (1 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9720[i_65_] = ((i_67_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9720[i_65_] & ~0x3f80));
			if (i_66_ == 33)
				((Class282_Sub15_Sub2) this).anIntArray9720[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9720[i_65_] & ~0x7f) + i_67_;
			if (5 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9703[i_65_] = ((i_67_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9703[i_65_] & ~0x3f80));
			if (i_66_ == 37)
				((Class282_Sub15_Sub2) this).anIntArray9703[i_65_] = (i_67_ + (((Class282_Sub15_Sub2) this).anIntArray9703[i_65_] & ~0x7f));
			if (i_66_ == 7)
				((Class282_Sub15_Sub2) this).anIntArray9711[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9711[i_65_] & ~0x3f80) + (i_67_ << 7);
			if (39 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9711[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9711[i_65_] & ~0x7f) + i_67_;
			if (i_66_ == 10)
				((Class282_Sub15_Sub2) this).anIntArray9691[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9691[i_65_] & ~0x3f80) + (i_67_ << 7);
			if (i_66_ == 42)
				((Class282_Sub15_Sub2) this).anIntArray9691[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9691[i_65_] & ~0x7f) + i_67_;
			if (11 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9697[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9697[i_65_] & ~0x3f80) + (i_67_ << 7);
			if (i_66_ == 43)
				((Class282_Sub15_Sub2) this).anIntArray9697[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9697[i_65_] & ~0x7f) + i_67_;
			if (64 == i_66_) {
				if (i_67_ >= 64)
					((Class282_Sub15_Sub2) this).anIntArray9707[i_65_] |= 0x1;
				else
					((Class282_Sub15_Sub2) this).anIntArray9707[i_65_] &= ~0x1;
			}
			if (i_66_ == 65) {
				if (i_67_ >= 64)
					((Class282_Sub15_Sub2) this).anIntArray9707[i_65_] |= 0x2;
				else {
					method15117(i_65_, 872608445);
					((Class282_Sub15_Sub2) this).anIntArray9707[i_65_] &= ~0x2;
				}
			}
			if (i_66_ == 99)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] = ((i_67_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] & 0x7f));
			if (i_66_ == 98)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] = (i_67_ + (((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] & 0x3f80));
			if (101 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] = (16384 + (((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] & 0x7f) + (i_67_ << 7));
			if (i_66_ == 100)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9708[i_65_] & 0x3f80) + 16384 + i_67_;
			if (i_66_ == 120)
				method15111(i_65_, (byte) 95);
			if (i_66_ == 121)
				method15114(i_65_, 51239021);
			if (123 == i_66_)
				method15192(i_65_, 2058582731);
			if (i_66_ == 6) {
				int i_68_ = ((Class282_Sub15_Sub2) this).anIntArray9708[i_65_];
				if (16384 == i_68_)
					((Class282_Sub15_Sub2) this).anIntArray9709[i_65_] = (i_67_ << 7) + ((((Class282_Sub15_Sub2) this).anIntArray9709[i_65_]) & ~0x3f80);
			}
			if (38 == i_66_) {
				int i_69_ = ((Class282_Sub15_Sub2) this).anIntArray9708[i_65_];
				if (16384 == i_69_)
					((Class282_Sub15_Sub2) this).anIntArray9709[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9709[i_65_] & ~0x7f) + i_67_;
			}
			if (16 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9704[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9704[i_65_] & ~0x3f80) + (i_67_ << 7);
			if (48 == i_66_)
				((Class282_Sub15_Sub2) this).anIntArray9704[i_65_] = (((Class282_Sub15_Sub2) this).anIntArray9704[i_65_] & ~0x7f) + i_67_;
			if (i_66_ == 81) {
				if (i_67_ >= 64)
					((Class282_Sub15_Sub2) this).anIntArray9707[i_65_] |= 0x4;
				else {
					method15118(i_65_, (byte) 97);
					((Class282_Sub15_Sub2) this).anIntArray9707[i_65_] &= ~0x4;
				}
			}
			if (17 == i_66_)
				method15104(i_65_, (((Class282_Sub15_Sub2) this).anIntArray9723[i_65_] & ~0x3f80) + (i_67_ << 7), (byte) -102);
			if (i_66_ == 49)
				method15104(i_65_, i_67_ + ((((Class282_Sub15_Sub2) this).anIntArray9723[i_65_]) & ~0x7f), (byte) -40);
		} else if (192 == i_55_) {
			int i_70_ = i & 0xf;
			int i_71_ = i >> 8 & 0x7f;
			method15189(i_70_, i_71_ + (((Class282_Sub15_Sub2) this).anIntArray9700[i_70_]), -2023630915);
		} else if (208 == i_55_) {
			int i_72_ = i & 0xf;
			int i_73_ = i >> 8 & 0x7f;
			method15121(i_72_, i_73_, (byte) 37);
		} else if (224 == i_55_) {
			int i_74_ = i & 0xf;
			int i_75_ = (i >> 8 & 0x7f) + (i >> 9 & 0x3f80);
			method15091(i_74_, i_75_, (byte) 1);
		} else {
			i_55_ = i & 0xff;
			if (i_55_ == 255)
				method15116(true, -1420505556);
		}
	}

	public Class282_Sub15_Sub2() {
		((Class282_Sub15_Sub2) this).anInt9690 = 2137157376;
		((Class282_Sub15_Sub2) this).anInt9695 = -340992960;
		((Class282_Sub15_Sub2) this).anIntArray9702 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9711 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9691 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9697 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9698 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9699 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9700 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9701 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9720 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9703 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9707 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9708 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9709 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9704 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9723 = new int[16];
		((Class282_Sub15_Sub2) this).anIntArray9712 = new int[16];
		((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713 = new Class282_Sub13[16][128];
		((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714 = new Class282_Sub13[16][128];
		((Class282_Sub15_Sub2) this).aClass80_9715 = new Class80();
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721 = new Class282_Sub15_Sub3(this);
		((Class282_Sub15_Sub2) this).aClass465_9694 = new Class465(128);
		method15095(-1, 256, 1096565083);
		method15116(true, -869042088);
	}

	int method15120(Class282_Sub13 class282_sub13, int i) {
		int i_76_ = (((-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574 * (-1958822223 * ((Class282_Sub13) class282_sub13).anInt7575)) >> 12) + 833000445 * ((Class282_Sub13) class282_sub13).anInt7573);
		i_76_ += ((((((Class282_Sub15_Sub2) this).anIntArray9701[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]) - 8192) * (((Class282_Sub15_Sub2) this).anIntArray9709[846406309 * ((Class282_Sub13) class282_sub13).anInt7582])) >> 12);
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		if (-1304717473 * ((Class110) class110).anInt1101 > 0 && (((Class110) class110).anInt1100 * 1255754511 > 0 || ((((Class282_Sub15_Sub2) this).anIntArray9720[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) > 0))) {
			int i_77_ = ((Class110) class110).anInt1100 * 1255754511 << 2;
			int i_78_ = 1253259197 * ((Class110) class110).anInt1102 << 1;
			if (((Class282_Sub13) class282_sub13).anInt7581 * 346269105 < i_78_)
				i_77_ = i_77_ * (((Class282_Sub13) class282_sub13).anInt7581 * 346269105) / i_78_;
			i_77_ += ((((Class282_Sub15_Sub2) this).anIntArray9720[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) >> 7);
			double d = Math.sin(0.01227184630308513 * (double) ((((Class282_Sub13) class282_sub13).anInt7571) * -70351715 & 0x1ff));
			i_76_ += (int) (d * (double) i_77_);
		}
		int i_79_ = (int) (((double) (1034460416 * (((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.anInt9750)) * Math.pow(2.0, 3.255208333333333E-4 * (double) i_76_) / (double) Class253.anInt3129) + 0.5);
		return i_79_ < 1 ? 1 : i_79_;
	}

	void method15121(int i, int i_80_, byte i_81_) {
		/* empty */
	}

	int method15122(Class282_Sub13 class282_sub13, int i) {
		int i_82_ = (((Class282_Sub15_Sub2) this).anIntArray9691[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]);
		if (i_82_ < 8192)
			return (32 + (((Class282_Sub13) class282_sub13).anInt7572 * -1677730315 * i_82_) >> 6);
		return (16384 - (32 + ((16384 - i_82_) * (128 - (-1677730315 * ((Class282_Sub13) class282_sub13).anInt7572))) >> 6));
	}

	synchronized Class282_Sub15 method12226() {
		return ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721;
	}

	int method15123(int i) {
		return 1824241431 * ((Class282_Sub15_Sub2) this).anInt9692;
	}

	synchronized void method15124(int i, int i_83_) {
		if (i < 0) {
			for (int i_84_ = 0; i_84_ < 16; i_84_++)
				((Class282_Sub15_Sub2) this).anIntArray9702[i_84_] = i_83_;
		} else
			((Class282_Sub15_Sub2) this).anIntArray9702[i] = i_83_;
	}

	int method15125() {
		return 1824241431 * ((Class282_Sub15_Sub2) this).anInt9692;
	}

	synchronized void method12231(int i) {
		if (((Class282_Sub15_Sub2) this).aClass80_9715.method1409()) {
			int i_85_ = (-33380583 * ((Class282_Sub15_Sub2) this).anInt9695 * (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anInt785) / Class253.anInt3129);
			do {
				long l = ((long) i * (long) i_85_ + (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710));
				if ((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - l >= 0L) {
					((Class282_Sub15_Sub2) this).aLong9710 = l * 8282832003758463051L;
					break;
				}
				int i_86_ = (int) (((6737959858605438665L * ((Class282_Sub15_Sub2) this).aLong9693) - (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710) + (long) i_85_ - 1L) / (long) i_85_);
				((Class282_Sub15_Sub2) this).aLong9710 += 8282832003758463051L * ((long) i_85_ * (long) i_86_);
				((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12231(i_86_);
				i -= i_86_;
				method15127(2143736845);
			} while (((Class282_Sub15_Sub2) this).aClass80_9715.method1409());
		}
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12231(i);
	}

	void method15126(long l) {
		long l_87_;
		for (/**/; (6737959858605438665L * ((Class282_Sub15_Sub2) this).aLong9693 <= l); ((Class282_Sub15_Sub2) this).aLong9693 = l_87_ * 7131548477904339833L) {
			int i = -29074701 * ((Class282_Sub15_Sub2) this).anInt9717;
			int i_88_ = -1521239861 * ((Class282_Sub15_Sub2) this).anInt9696;
			l_87_ = (((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L);
			while (-1521239861 * ((Class282_Sub15_Sub2) this).anInt9696 == i_88_) {
				while (i_88_ == ((Class80) (((Class282_Sub15_Sub2) this).aClass80_9715)).anIntArray788[i]) {
					((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i);
					int i_89_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1401(i);
					if (i_89_ == 1) {
						((Class282_Sub15_Sub2) this).aClass80_9715.method1400();
						((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i);
						if (((Class282_Sub15_Sub2) this).aClass80_9715.method1405()) {
							if (((Class282_Sub15_Sub2) this).aBool9716 && i_88_ != 0)
								((Class282_Sub15_Sub2) this).aClass80_9715.method1406(l_87_);
							else {
								method15116(true, -794384351);
								((Class282_Sub15_Sub2) this).aClass80_9715.method1395();
								return;
							}
						}
						break;
					}
					if ((i_89_ & 0x80) != 0 && (i_89_ & 0xf0) != 144)
						method15119(i_89_, -438013028);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i);
				}
				((Class282_Sub15_Sub2) this).aLong9710 = 8282832003758463051L * l_87_;
				i = ((Class282_Sub15_Sub2) this).aClass80_9715.method1393();
				i_88_ = (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[i]);
				l_87_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1403(i_88_);
			}
			((Class282_Sub15_Sub2) this).anInt9717 = i * -2087498181;
			((Class282_Sub15_Sub2) this).anInt9696 = -466031389 * i_88_;
		}
	}

	void method15127(int i) {
		int i_90_ = -29074701 * ((Class282_Sub15_Sub2) this).anInt9717;
		int i_91_ = ((Class282_Sub15_Sub2) this).anInt9696 * -1521239861;
		long l = 6737959858605438665L * ((Class282_Sub15_Sub2) this).aLong9693;
		if (null != ((Class282_Sub15_Sub2) this).aClass282_Sub7_9722 && ((Class282_Sub15_Sub2) this).anInt9719 * 1703527275 == i_91_) {
			method15131(((Class282_Sub15_Sub2) this).aClass282_Sub7_9722, ((Class282_Sub15_Sub2) this).aBool9716, ((Class282_Sub15_Sub2) this).aBool9724, (byte) -124);
			method15127(2133226278);
		} else {
			while (-1521239861 * ((Class282_Sub15_Sub2) this).anInt9696 == i_91_) {
				while ((((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[i_90_]) == i_91_) {
					((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i_90_);
					int i_92_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1401(i_90_);
					if (i_92_ == 1) {
						((Class282_Sub15_Sub2) this).aClass80_9715.method1400();
						((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i_90_);
						if (((Class282_Sub15_Sub2) this).aClass80_9715.method1405()) {
							if ((((Class282_Sub15_Sub2) this).aClass282_Sub7_9722) != null) {
								method15098((((Class282_Sub15_Sub2) this).aClass282_Sub7_9722), (((Class282_Sub15_Sub2) this).aBool9716), 1910975193);
								method15127(1898910658);
								return;
							}
							if (((Class282_Sub15_Sub2) this).aBool9716 && 0 != i_91_)
								((Class282_Sub15_Sub2) this).aClass80_9715.method1406(l);
							else {
								method15116(true, -548728435);
								((Class282_Sub15_Sub2) this).aClass80_9715.method1395();
								return;
							}
						}
						break;
					}
					if (0 != (i_92_ & 0x80))
						method15119(i_92_, 156102430);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i_90_);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i_90_);
				}
				i_90_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1393();
				i_91_ = (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[i_90_]);
				l = ((Class282_Sub15_Sub2) this).aClass80_9715.method1403(i_91_);
			}
			((Class282_Sub15_Sub2) this).anInt9717 = i_90_ * -2087498181;
			((Class282_Sub15_Sub2) this).anInt9696 = -466031389 * i_91_;
			((Class282_Sub15_Sub2) this).aLong9693 = l * 7131548477904339833L;
			if (((Class282_Sub15_Sub2) this).aClass282_Sub7_9722 != null && (1703527275 * ((Class282_Sub15_Sub2) this).anInt9719 < i_91_)) {
				((Class282_Sub15_Sub2) this).anInt9717 = 2087498181;
				((Class282_Sub15_Sub2) this).anInt9696 = ((Class282_Sub15_Sub2) this).anInt9719 * 347532257;
				((Class282_Sub15_Sub2) this).aLong9693 = ((((Class282_Sub15_Sub2) this).aClass80_9715.method1403(-1521239861 * ((Class282_Sub15_Sub2) this).anInt9696)) * 7131548477904339833L);
			}
		}
	}

	synchronized int method12228() {
		return 0;
	}

	boolean method15128(Class282_Sub13 class282_sub13, int[] is, int i, int i_93_, int i_94_) {
		((Class282_Sub13) class282_sub13).anInt7584 = -431461175 * (Class253.anInt3129 / 100);
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0 && (null == (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568) || ((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15319((byte) -36))) {
			class282_sub13.method12213(-2085241547);
			class282_sub13.method4991(-371378792);
			if (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0 && (class282_sub13 == (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)])))
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[((Class282_Sub13) class282_sub13).anInt7582 * 846406309][-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567] = null;
			return true;
		}
		int i_95_ = -1958822223 * ((Class282_Sub13) class282_sub13).anInt7575;
		if (i_95_ > 0) {
			i_95_ -= (int) ((16.0 * Math.pow(2.0, ((double) (((Class282_Sub15_Sub2) this).anIntArray9703[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * 4.921259842519685E-4))) + 0.5);
			if (i_95_ < 0)
				i_95_ = 0;
			((Class282_Sub13) class282_sub13).anInt7575 = i_95_ * 1359923793;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15308(method15120(class282_sub13, -1048331373), (short) 25320);
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		boolean bool = false;
		((Class282_Sub13) class282_sub13).anInt7581 += -903904431;
		((Class282_Sub13) class282_sub13).anInt7571 += ((Class110) class110).anInt1101 * -370064085;
		double d = (5.086263020833333E-6 * (double) (((-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574 * (((Class282_Sub13) class282_sub13).anInt7575 * -1958822223)) >> 12) + (1293000467 * (((Class282_Sub13) class282_sub13).anInt7569) - 60 << 8)));
		if (1284723511 * ((Class110) class110).anInt1096 > 0) {
			if (-1649764267 * ((Class110) class110).anInt1099 > 0)
				((Class282_Sub13) class282_sub13).anInt7576 += (int) ((128.0 * Math.pow(2.0, d * (double) ((((Class110) class110).anInt1099) * -1649764267))) + 0.5) * 1188817509;
			else
				((Class282_Sub13) class282_sub13).anInt7576 += 1844785792;
			if ((1284723511 * ((Class110) class110).anInt1096 * (((Class282_Sub13) class282_sub13).anInt7576 * 69449581)) >= 819200)
				bool = true;
		}
		if (((Class110) class110).aByteArray1097 != null) {
			if (1684458023 * ((Class110) class110).anInt1095 > 0)
				((Class282_Sub13) class282_sub13).anInt7577 += (int) (128.0 * Math.pow(2.0, d * (double) (1684458023 * (((Class110) class110).anInt1095))) + 0.5) * 35369279;
			else
				((Class282_Sub13) class282_sub13).anInt7577 += 232300416;
			for (/**/; ((((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) && (-474562881 * ((Class282_Sub13) class282_sub13).anInt7577 > ((((Class110) class110).aByteArray1097[2 + 1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7578 += -1958397046) {
				/* empty */
			}
			if ((((Class110) class110).aByteArray1097.length - 2 == ((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) && (((Class110) class110).aByteArray1097[(1159387405 * ((Class282_Sub13) class282_sub13).anInt7578 + 1)]) == 0)
				bool = true;
		}
		if (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 >= 0 && null != ((Class110) class110).aByteArray1094 && (0 == ((((Class282_Sub15_Sub2) this).anIntArray9707[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) & 0x1)) && (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 < 0 || (class282_sub13 != (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)])))) {
			if (((Class110) class110).anInt1098 * 1351159779 > 0)
				((Class282_Sub13) class282_sub13).anInt7579 += (int) (128.0 * Math.pow(2.0, d * (double) (1351159779 * (((Class110) class110).anInt1098))) + 0.5) * 523944391;
			else
				((Class282_Sub13) class282_sub13).anInt7579 += -1654594688;
			for (/**/; ((-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 > ((((Class110) class110).aByteArray1094[2 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7580 += -1517554074) {
				/* empty */
			}
			if (((Class110) class110).aByteArray1094.length - 2 == -9799685 * ((Class282_Sub13) class282_sub13).anInt7580)
				bool = true;
		}
		if (bool) {
			((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15322(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, (byte) 66);
			if (null != is)
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12230(is, i, i_93_);
			else
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12231(i_93_);
			if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15311((byte) 33))
				((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass282_Sub15_Sub4_9755.method15275(((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568);
			class282_sub13.method12213(-2085241547);
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((((Class282_Sub13) class282_sub13).anInt7567 * -2127113783 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)] = null;
			}
			return true;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15321(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, method15108(class282_sub13, 1413328073), method15122(class282_sub13, -2057424596), 1544823331);
		return false;
	}

	void method15129(int i, int i_96_) {
		((Class282_Sub15_Sub2) this).anIntArray9701[i] = i_96_;
	}

	synchronized Class282_Sub15 method12233() {
		return ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721;
	}

	synchronized Class282_Sub15 method12229() {
		return null;
	}

	synchronized Class282_Sub15 method12235() {
		return null;
	}

	synchronized Class282_Sub15 method12236() {
		return null;
	}

	synchronized int method12227() {
		return 0;
	}

	synchronized int method12224() {
		return 0;
	}

	synchronized int method12238() {
		return 0;
	}

	synchronized void method15130() {
		method15102(true, -1688082880);
	}

	synchronized void method12240(int[] is, int i, int i_97_) {
		if (((Class282_Sub15_Sub2) this).aClass80_9715.method1409()) {
			int i_98_ = (((Class282_Sub15_Sub2) this).anInt9695 * -33380583 * (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anInt785) / Class253.anInt3129);
			do {
				long l = ((long) i_98_ * (long) i_97_ + (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710));
				if ((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - l >= 0L) {
					((Class282_Sub15_Sub2) this).aLong9710 = l * 8282832003758463051L;
					break;
				}
				int i_99_ = (int) (((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710) + (long) i_98_ - 1L) / (long) i_98_);
				((Class282_Sub15_Sub2) this).aLong9710 += 8282832003758463051L * ((long) i_99_ * (long) i_98_);
				((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_99_);
				i += i_99_;
				i_97_ -= i_99_;
				method15127(2016228354);
			} while (((Class282_Sub15_Sub2) this).aClass80_9715.method1409());
		}
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_97_);
	}

	synchronized void method15131(Class282_Sub7 class282_sub7, boolean bool, boolean bool_100_, byte i) {
		method15102(bool_100_, -1975049959);
		((Class282_Sub15_Sub2) this).aClass80_9715.method1394(((Class282_Sub7) class282_sub7).aByteArray7522);
		((Class282_Sub15_Sub2) this).aBool9716 = bool;
		((Class282_Sub15_Sub2) this).aLong9710 = 0L;
		int i_101_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1397();
		for (int i_102_ = 0; i_102_ < i_101_; i_102_++) {
			((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i_102_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i_102_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i_102_);
		}
		((Class282_Sub15_Sub2) this).anInt9717 = (((Class282_Sub15_Sub2) this).aClass80_9715.method1393() * -2087498181);
		((Class282_Sub15_Sub2) this).anInt9696 = ((((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[((Class282_Sub15_Sub2) this).anInt9717 * -29074701]) * -466031389);
		((Class282_Sub15_Sub2) this).aLong9693 = ((((Class282_Sub15_Sub2) this).aClass80_9715.method1403(((Class282_Sub15_Sub2) this).anInt9696 * -1521239861)) * 7131548477904339833L);
	}

	void method15132(int i, int i_103_) {
		((Class282_Sub15_Sub2) this).anIntArray9701[i] = i_103_;
	}

	synchronized void method12243(int i) {
		if (((Class282_Sub15_Sub2) this).aClass80_9715.method1409()) {
			int i_104_ = (-33380583 * ((Class282_Sub15_Sub2) this).anInt9695 * (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anInt785) / Class253.anInt3129);
			do {
				long l = ((long) i * (long) i_104_ + (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710));
				if ((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - l >= 0L) {
					((Class282_Sub15_Sub2) this).aLong9710 = l * 8282832003758463051L;
					break;
				}
				int i_105_ = (int) (((6737959858605438665L * ((Class282_Sub15_Sub2) this).aLong9693) - (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710) + (long) i_104_ - 1L) / (long) i_104_);
				((Class282_Sub15_Sub2) this).aLong9710 += 8282832003758463051L * ((long) i_104_ * (long) i_105_);
				((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12231(i_105_);
				i -= i_105_;
				method15127(1900223242);
			} while (((Class282_Sub15_Sub2) this).aClass80_9715.method1409());
		}
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12231(i);
	}

	synchronized void method12234(int i) {
		if (((Class282_Sub15_Sub2) this).aClass80_9715.method1409()) {
			int i_106_ = (-33380583 * ((Class282_Sub15_Sub2) this).anInt9695 * (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anInt785) / Class253.anInt3129);
			do {
				long l = ((long) i * (long) i_106_ + (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710));
				if ((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - l >= 0L) {
					((Class282_Sub15_Sub2) this).aLong9710 = l * 8282832003758463051L;
					break;
				}
				int i_107_ = (int) (((6737959858605438665L * ((Class282_Sub15_Sub2) this).aLong9693) - (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710) + (long) i_106_ - 1L) / (long) i_106_);
				((Class282_Sub15_Sub2) this).aLong9710 += 8282832003758463051L * ((long) i_106_ * (long) i_107_);
				((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12231(i_107_);
				i -= i_107_;
				method15127(1971709298);
			} while (((Class282_Sub15_Sub2) this).aClass80_9715.method1409());
		}
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12231(i);
	}

	synchronized void method15133(int i) {
		((Class282_Sub15_Sub2) this).anInt9692 = i * -385159001;
	}

	synchronized void method15134(int i) {
		((Class282_Sub15_Sub2) this).anInt9692 = i * -385159001;
	}

	synchronized void method15135(int i) {
		((Class282_Sub15_Sub2) this).anInt9692 = i * -385159001;
	}

	synchronized void method15136(int i) {
		((Class282_Sub15_Sub2) this).anInt9692 = i * -385159001;
	}

	public synchronized void method15137(int i, int i_108_, int i_109_) {
		method15105(i, i_108_, -101926143);
	}

	int method15138() {
		return 1824241431 * ((Class282_Sub15_Sub2) this).anInt9692;
	}

	void method15139(long l) {
		long l_110_;
		for (/**/; (6737959858605438665L * ((Class282_Sub15_Sub2) this).aLong9693 <= l); ((Class282_Sub15_Sub2) this).aLong9693 = l_110_ * 7131548477904339833L) {
			int i = -29074701 * ((Class282_Sub15_Sub2) this).anInt9717;
			int i_111_ = -1521239861 * ((Class282_Sub15_Sub2) this).anInt9696;
			l_110_ = (((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L);
			while (-1521239861 * ((Class282_Sub15_Sub2) this).anInt9696 == i_111_) {
				while (i_111_ == ((Class80) (((Class282_Sub15_Sub2) this).aClass80_9715)).anIntArray788[i]) {
					((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i);
					int i_112_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1401(i);
					if (i_112_ == 1) {
						((Class282_Sub15_Sub2) this).aClass80_9715.method1400();
						((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i);
						if (((Class282_Sub15_Sub2) this).aClass80_9715.method1405()) {
							if (((Class282_Sub15_Sub2) this).aBool9716 && i_111_ != 0)
								((Class282_Sub15_Sub2) this).aClass80_9715.method1406(l_110_);
							else {
								method15116(true, -726440160);
								((Class282_Sub15_Sub2) this).aClass80_9715.method1395();
								return;
							}
						}
						break;
					}
					if ((i_112_ & 0x80) != 0 && (i_112_ & 0xf0) != 144)
						method15119(i_112_, -886390411);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i);
				}
				((Class282_Sub15_Sub2) this).aLong9710 = 8282832003758463051L * l_110_;
				i = ((Class282_Sub15_Sub2) this).aClass80_9715.method1393();
				i_111_ = (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[i]);
				l_110_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1403(i_111_);
			}
			((Class282_Sub15_Sub2) this).anInt9717 = i * -2087498181;
			((Class282_Sub15_Sub2) this).anInt9696 = -466031389 * i_111_;
		}
	}

	synchronized void method15140(int i) {
		((Class282_Sub15_Sub2) this).anInt9690 = i * -1954586001;
	}

	synchronized void method15141(int i) {
		((Class282_Sub15_Sub2) this).anInt9690 = i * -1954586001;
	}

	synchronized void method15142(int i) {
		((Class282_Sub15_Sub2) this).anInt9690 = i * -1954586001;
	}

	synchronized void method15143(int i) {
		((Class282_Sub15_Sub2) this).anInt9690 = i * -1954586001;
	}

	synchronized void method15144(int i, int i_113_) {
		((Class282_Sub15_Sub2) this).anInt9692 = i * -385159001;
	}

	synchronized boolean method15145(Class282_Sub7 class282_sub7, Class317 class317, Class250 class250, int i) {
		class282_sub7.method12165();
		boolean bool = true;
		int[] is = null;
		if (i > 0)
			is = new int[] { i };
		for (Class282_Sub47 class282_sub47 = (Class282_Sub47) ((Class282_Sub7) class282_sub7).aClass465_7521.method7750(-672312325); class282_sub47 != null; class282_sub47 = (Class282_Sub47) ((Class282_Sub7) class282_sub7).aClass465_7521.method7751((byte) 100)) {
			int i_114_ = (int) (-3442165056282524525L * class282_sub47.aLong3379);
			Class282_Sub14 class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7754((long) i_114_);
			if (class282_sub14 == null) {
				class282_sub14 = Class96_Sub21.method14676(class317, i_114_, 450746801);
				if (class282_sub14 == null) {
					bool = false;
					continue;
				}
				((Class282_Sub15_Sub2) this).aClass465_9694.method7765(class282_sub14, (long) i_114_);
			}
			if (!class282_sub14.method12216(class250, ((byte[]) class282_sub47.anObject8068), is, -397044582))
				bool = false;
		}
		if (bool)
			class282_sub7.method12167();
		return bool;
	}

	synchronized Class282_Sub15 method12239() {
		return null;
	}

	synchronized void method15146(Class282_Sub7 class282_sub7, boolean bool) {
		method15131(class282_sub7, bool, true, (byte) 3);
	}

	synchronized void method15147(Class282_Sub7 class282_sub7, boolean bool) {
		method15131(class282_sub7, bool, true, (byte) 1);
	}

	synchronized void method15148(Class282_Sub7 class282_sub7, boolean bool, boolean bool_115_) {
		method15102(bool_115_, -1927187100);
		((Class282_Sub15_Sub2) this).aClass80_9715.method1394(((Class282_Sub7) class282_sub7).aByteArray7522);
		((Class282_Sub15_Sub2) this).aBool9716 = bool;
		((Class282_Sub15_Sub2) this).aLong9710 = 0L;
		int i = ((Class282_Sub15_Sub2) this).aClass80_9715.method1397();
		for (int i_116_ = 0; i_116_ < i; i_116_++) {
			((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i_116_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i_116_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i_116_);
		}
		((Class282_Sub15_Sub2) this).anInt9717 = (((Class282_Sub15_Sub2) this).aClass80_9715.method1393() * -2087498181);
		((Class282_Sub15_Sub2) this).anInt9696 = ((((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[((Class282_Sub15_Sub2) this).anInt9717 * -29074701]) * -466031389);
		((Class282_Sub15_Sub2) this).aLong9693 = ((((Class282_Sub15_Sub2) this).aClass80_9715.method1403(((Class282_Sub15_Sub2) this).anInt9696 * -1521239861)) * 7131548477904339833L);
	}

	synchronized void method15149(Class282_Sub7 class282_sub7, boolean bool, boolean bool_117_) {
		method15102(bool_117_, -2107909126);
		((Class282_Sub15_Sub2) this).aClass80_9715.method1394(((Class282_Sub7) class282_sub7).aByteArray7522);
		((Class282_Sub15_Sub2) this).aBool9716 = bool;
		((Class282_Sub15_Sub2) this).aLong9710 = 0L;
		int i = ((Class282_Sub15_Sub2) this).aClass80_9715.method1397();
		for (int i_118_ = 0; i_118_ < i; i_118_++) {
			((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i_118_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i_118_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i_118_);
		}
		((Class282_Sub15_Sub2) this).anInt9717 = (((Class282_Sub15_Sub2) this).aClass80_9715.method1393() * -2087498181);
		((Class282_Sub15_Sub2) this).anInt9696 = ((((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[((Class282_Sub15_Sub2) this).anInt9717 * -29074701]) * -466031389);
		((Class282_Sub15_Sub2) this).aLong9693 = ((((Class282_Sub15_Sub2) this).aClass80_9715.method1403(((Class282_Sub15_Sub2) this).anInt9696 * -1521239861)) * 7131548477904339833L);
	}

	synchronized void method15150(Class282_Sub7 class282_sub7, boolean bool, boolean bool_119_) {
		method15102(bool_119_, -1562451504);
		((Class282_Sub15_Sub2) this).aClass80_9715.method1394(((Class282_Sub7) class282_sub7).aByteArray7522);
		((Class282_Sub15_Sub2) this).aBool9716 = bool;
		((Class282_Sub15_Sub2) this).aLong9710 = 0L;
		int i = ((Class282_Sub15_Sub2) this).aClass80_9715.method1397();
		for (int i_120_ = 0; i_120_ < i; i_120_++) {
			((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i_120_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i_120_);
			((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i_120_);
		}
		((Class282_Sub15_Sub2) this).anInt9717 = (((Class282_Sub15_Sub2) this).aClass80_9715.method1393() * -2087498181);
		((Class282_Sub15_Sub2) this).anInt9696 = ((((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[((Class282_Sub15_Sub2) this).anInt9717 * -29074701]) * -466031389);
		((Class282_Sub15_Sub2) this).aLong9693 = ((((Class282_Sub15_Sub2) this).aClass80_9715.method1403(((Class282_Sub15_Sub2) this).anInt9696 * -1521239861)) * 7131548477904339833L);
	}

	synchronized void method15151() {
		for (Class282_Sub14 class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7750(619228521); null != class282_sub14; class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7751((byte) 61))
			class282_sub14.method12217((short) -4118);
	}

	synchronized void method12241(int[] is, int i, int i_121_) {
		if (((Class282_Sub15_Sub2) this).aClass80_9715.method1409()) {
			int i_122_ = (((Class282_Sub15_Sub2) this).anInt9695 * -33380583 * (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anInt785) / Class253.anInt3129);
			do {
				long l = ((long) i_122_ * (long) i_121_ + (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710));
				if ((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - l >= 0L) {
					((Class282_Sub15_Sub2) this).aLong9710 = l * 8282832003758463051L;
					break;
				}
				int i_123_ = (int) (((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710) + (long) i_122_ - 1L) / (long) i_122_);
				((Class282_Sub15_Sub2) this).aLong9710 += 8282832003758463051L * ((long) i_123_ * (long) i_122_);
				((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_123_);
				i += i_123_;
				i_121_ -= i_123_;
				method15127(1931121516);
			} while (((Class282_Sub15_Sub2) this).aClass80_9715.method1409());
		}
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_121_);
	}

	synchronized void method15152() {
		method15102(true, -1783899332);
	}

	void method15153(int i, int i_124_) {
		((Class282_Sub15_Sub2) this).anIntArray9701[i] = i_124_;
	}

	boolean method15154(Class282_Sub13 class282_sub13, int[] is, int i, int i_125_) {
		((Class282_Sub13) class282_sub13).anInt7584 = -431461175 * (Class253.anInt3129 / 100);
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0 && (null == (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568) || ((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15319((byte) -125))) {
			class282_sub13.method12213(-2085241547);
			class282_sub13.method4991(-371378792);
			if (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0 && (class282_sub13 == (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)])))
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[((Class282_Sub13) class282_sub13).anInt7582 * 846406309][-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567] = null;
			return true;
		}
		int i_126_ = -1958822223 * ((Class282_Sub13) class282_sub13).anInt7575;
		if (i_126_ > 0) {
			i_126_ -= (int) ((16.0 * Math.pow(2.0, ((double) (((Class282_Sub15_Sub2) this).anIntArray9703[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * 4.921259842519685E-4))) + 0.5);
			if (i_126_ < 0)
				i_126_ = 0;
			((Class282_Sub13) class282_sub13).anInt7575 = i_126_ * 1359923793;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15308(method15120(class282_sub13, -869149759), (short) 14138);
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		boolean bool = false;
		((Class282_Sub13) class282_sub13).anInt7581 += -903904431;
		((Class282_Sub13) class282_sub13).anInt7571 += ((Class110) class110).anInt1101 * -370064085;
		double d = (5.086263020833333E-6 * (double) (((-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574 * (((Class282_Sub13) class282_sub13).anInt7575 * -1958822223)) >> 12) + (1293000467 * (((Class282_Sub13) class282_sub13).anInt7569) - 60 << 8)));
		if (1284723511 * ((Class110) class110).anInt1096 > 0) {
			if (-1649764267 * ((Class110) class110).anInt1099 > 0)
				((Class282_Sub13) class282_sub13).anInt7576 += (int) ((128.0 * Math.pow(2.0, d * (double) ((((Class110) class110).anInt1099) * -1649764267))) + 0.5) * 1188817509;
			else
				((Class282_Sub13) class282_sub13).anInt7576 += 1844785792;
			if ((1284723511 * ((Class110) class110).anInt1096 * (((Class282_Sub13) class282_sub13).anInt7576 * 69449581)) >= 819200)
				bool = true;
		}
		if (((Class110) class110).aByteArray1097 != null) {
			if (1684458023 * ((Class110) class110).anInt1095 > 0)
				((Class282_Sub13) class282_sub13).anInt7577 += (int) (128.0 * Math.pow(2.0, d * (double) (1684458023 * (((Class110) class110).anInt1095))) + 0.5) * 35369279;
			else
				((Class282_Sub13) class282_sub13).anInt7577 += 232300416;
			for (/**/; ((((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) && (-474562881 * ((Class282_Sub13) class282_sub13).anInt7577 > ((((Class110) class110).aByteArray1097[2 + 1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7578 += -1958397046) {
				/* empty */
			}
			if ((((Class110) class110).aByteArray1097.length - 2 == ((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) && (((Class110) class110).aByteArray1097[(1159387405 * ((Class282_Sub13) class282_sub13).anInt7578 + 1)]) == 0)
				bool = true;
		}
		if (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 >= 0 && null != ((Class110) class110).aByteArray1094 && (0 == ((((Class282_Sub15_Sub2) this).anIntArray9707[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) & 0x1)) && (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 < 0 || (class282_sub13 != (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)])))) {
			if (((Class110) class110).anInt1098 * 1351159779 > 0)
				((Class282_Sub13) class282_sub13).anInt7579 += (int) (128.0 * Math.pow(2.0, d * (double) (1351159779 * (((Class110) class110).anInt1098))) + 0.5) * 523944391;
			else
				((Class282_Sub13) class282_sub13).anInt7579 += -1654594688;
			for (/**/; ((-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 > ((((Class110) class110).aByteArray1094[2 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7580 += -1517554074) {
				/* empty */
			}
			if (((Class110) class110).aByteArray1094.length - 2 == -9799685 * ((Class282_Sub13) class282_sub13).anInt7580)
				bool = true;
		}
		if (bool) {
			((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15322(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, (byte) -68);
			if (null != is)
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12230(is, i, i_125_);
			else
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12231(i_125_);
			if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15311((byte) 33))
				((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass282_Sub15_Sub4_9755.method15275(((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568);
			class282_sub13.method12213(-2085241547);
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((((Class282_Sub13) class282_sub13).anInt7567 * -2127113783 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)] = null;
			}
			return true;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15321(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, method15108(class282_sub13, 1664918838), method15122(class282_sub13, -945179280), -966937305);
		return false;
	}

	synchronized void method15155(boolean bool) {
		((Class282_Sub15_Sub2) this).aClass80_9715.method1395();
		((Class282_Sub15_Sub2) this).aClass282_Sub7_9722 = null;
		method15116(bool, -967923878);
	}

	synchronized boolean method15156() {
		return ((Class282_Sub15_Sub2) this).aClass80_9715.method1409();
	}

	synchronized boolean method15157() {
		return ((Class282_Sub15_Sub2) this).aClass80_9715.method1409();
	}

	synchronized boolean method15158() {
		return ((Class282_Sub15_Sub2) this).aClass80_9715.method1409();
	}

	public synchronized void method15159(int i, int i_127_) {
		method15105(i, i_127_, -274759717);
	}

	public synchronized void method15160(int i, int i_128_) {
		method15105(i, i_128_, 1140550837);
	}

	void method15161(int i, int i_129_) {
		((Class282_Sub15_Sub2) this).anIntArray9698[i] = i_129_;
		((Class282_Sub15_Sub2) this).anIntArray9700[i] = i_129_ & ~0x7f;
		method15189(i, i_129_, -271829685);
	}

	void method15162(int i, int i_130_) {
		if (i_130_ != ((Class282_Sub15_Sub2) this).anIntArray9699[i]) {
			((Class282_Sub15_Sub2) this).anIntArray9699[i] = i_130_;
			for (int i_131_ = 0; i_131_ < 128; i_131_++)
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[i][i_131_] = null;
		}
	}

	void method15163(int i, int i_132_) {
		if (i_132_ != ((Class282_Sub15_Sub2) this).anIntArray9699[i]) {
			((Class282_Sub15_Sub2) this).anIntArray9699[i] = i_132_;
			for (int i_133_ = 0; i_133_ < 128; i_133_++)
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[i][i_133_] = null;
		}
	}

	void method15164(Class282_Sub13 class282_sub13, boolean bool) {
		int i = ((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.method15223();
		int i_134_;
		if (bool && (((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.aBool9752)) {
			int i_135_ = i + i - -69474713 * (((Class282_Sub13) class282_sub13).aClass282_Sub26_Sub1_7565.anInt9749);
			i_134_ = (int) ((long) (((Class282_Sub15_Sub2) this).anIntArray9704[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * (long) i_135_ >> 6);
			i <<= 8;
			if (i_134_ >= i) {
				i_134_ = i + i - 1 - i_134_;
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15313(true, -2024320208);
			}
		} else
			i_134_ = (int) ((long) (((Class282_Sub15_Sub2) this).anIntArray9704[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * (long) i >> 6);
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15317(i_134_, -760928023);
	}

	void method15165(int i, int i_136_, int i_137_) {
		Class282_Sub13 class282_sub13 = (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][i_136_]);
		if (class282_sub13 != null) {
			((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][i_136_] = null;
			if (0 != (((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2)) {
				for (Class282_Sub13 class282_sub13_138_ = (Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 24); null != class282_sub13_138_; class282_sub13_138_ = (Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(-829657576)) {
					if (((((Class282_Sub13) class282_sub13).anInt7582 * 846406309) == (((Class282_Sub13) class282_sub13_138_).anInt7582 * 846406309)) && 761835511 * (((Class282_Sub13) class282_sub13_138_).anInt7579) < 0 && class282_sub13_138_ != class282_sub13) {
						((Class282_Sub13) class282_sub13).anInt7579 = 0;
						break;
					}
				}
			} else
				((Class282_Sub13) class282_sub13).anInt7579 = 0;
		}
	}

	synchronized void method15166() {
		method15102(true, -1200864633);
	}

	void method15167(int i, int i_139_) {
		((Class282_Sub15_Sub2) this).anIntArray9723[i] = i_139_;
		((Class282_Sub15_Sub2) this).anIntArray9712[i] = (int) (2097152.0 * Math.pow(2.0, (double) i_139_ * 5.4931640625E-4) + 0.5);
	}

	synchronized int method12244() {
		return 0;
	}

	synchronized Class282_Sub15 method12232() {
		return ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721;
	}

	void method15168(long l) {
		long l_140_;
		for (/**/; (6737959858605438665L * ((Class282_Sub15_Sub2) this).aLong9693 <= l); ((Class282_Sub15_Sub2) this).aLong9693 = l_140_ * 7131548477904339833L) {
			int i = -29074701 * ((Class282_Sub15_Sub2) this).anInt9717;
			int i_141_ = -1521239861 * ((Class282_Sub15_Sub2) this).anInt9696;
			l_140_ = (((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L);
			while (-1521239861 * ((Class282_Sub15_Sub2) this).anInt9696 == i_141_) {
				while (i_141_ == ((Class80) (((Class282_Sub15_Sub2) this).aClass80_9715)).anIntArray788[i]) {
					((Class282_Sub15_Sub2) this).aClass80_9715.method1398(i);
					int i_142_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1401(i);
					if (i_142_ == 1) {
						((Class282_Sub15_Sub2) this).aClass80_9715.method1400();
						((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i);
						if (((Class282_Sub15_Sub2) this).aClass80_9715.method1405()) {
							if (((Class282_Sub15_Sub2) this).aBool9716 && i_141_ != 0)
								((Class282_Sub15_Sub2) this).aClass80_9715.method1406(l_140_);
							else {
								method15116(true, -916096632);
								((Class282_Sub15_Sub2) this).aClass80_9715.method1395();
								return;
							}
						}
						break;
					}
					if ((i_142_ & 0x80) != 0 && (i_142_ & 0xf0) != 144)
						method15119(i_142_, 421125637);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1392(i);
					((Class282_Sub15_Sub2) this).aClass80_9715.method1404(i);
				}
				((Class282_Sub15_Sub2) this).aLong9710 = 8282832003758463051L * l_140_;
				i = ((Class282_Sub15_Sub2) this).aClass80_9715.method1393();
				i_141_ = (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anIntArray788[i]);
				l_140_ = ((Class282_Sub15_Sub2) this).aClass80_9715.method1403(i_141_);
			}
			((Class282_Sub15_Sub2) this).anInt9717 = i * -2087498181;
			((Class282_Sub15_Sub2) this).anInt9696 = -466031389 * i_141_;
		}
	}

	void method15169(int i, int i_143_) {
		((Class282_Sub15_Sub2) this).anIntArray9701[i] = i_143_;
	}

	void method15170(int i) {
		for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8097((byte) 126)); null != class282_sub13; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8067(2085325162))) {
			if ((i < 0 || i == (846406309 * ((Class282_Sub13) class282_sub13).anInt7582)) && (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 < 0)) {
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[846406309 * ((Class282_Sub13) class282_sub13).anInt7582][((Class282_Sub13) class282_sub13).anInt7569 * 1293000467] = null;
				((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	void method15171(int i) {
		for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8097((byte) 63)); null != class282_sub13; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8067(726246443))) {
			if ((i < 0 || i == (846406309 * ((Class282_Sub13) class282_sub13).anInt7582)) && (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 < 0)) {
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[846406309 * ((Class282_Sub13) class282_sub13).anInt7582][((Class282_Sub13) class282_sub13).anInt7569 * 1293000467] = null;
				((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	void method15172(boolean bool) {
		if (bool)
			method15111(-1, (byte) 114);
		else
			method15192(-1, 1138763560);
		method15114(-1, 51239021);
		for (int i = 0; i < 16; i++)
			((Class282_Sub15_Sub2) this).anIntArray9699[i] = ((Class282_Sub15_Sub2) this).anIntArray9698[i];
		for (int i = 0; i < 16; i++)
			((Class282_Sub15_Sub2) this).anIntArray9700[i] = ((Class282_Sub15_Sub2) this).anIntArray9698[i] & ~0x7f;
	}

	void method15173(boolean bool) {
		if (bool)
			method15111(-1, (byte) 74);
		else
			method15192(-1, 1209368089);
		method15114(-1, 51239021);
		for (int i = 0; i < 16; i++)
			((Class282_Sub15_Sub2) this).anIntArray9699[i] = ((Class282_Sub15_Sub2) this).anIntArray9698[i];
		for (int i = 0; i < 16; i++)
			((Class282_Sub15_Sub2) this).anIntArray9700[i] = ((Class282_Sub15_Sub2) this).anIntArray9698[i] & ~0x7f;
	}

	void method15174(int i, int i_144_) {
		((Class282_Sub15_Sub2) this).anIntArray9701[i] = i_144_;
	}

	void method15175(int i) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2) != 0) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 82)); class282_sub13 != null; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(629639645))) {
				if ((((Class282_Sub13) class282_sub13).anInt7582 * 846406309 == i) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][(((Class282_Sub13) class282_sub13).anInt7569 * 1293000467)]) == null && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 < 0))
					((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	void method15176(int i) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2) != 0) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 117)); class282_sub13 != null; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(-472460550))) {
				if ((((Class282_Sub13) class282_sub13).anInt7582 * 846406309 == i) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][(((Class282_Sub13) class282_sub13).anInt7569 * 1293000467)]) == null && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 < 0))
					((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	void method15177(int i) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x2) != 0) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 72)); class282_sub13 != null; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(1842961123))) {
				if ((((Class282_Sub13) class282_sub13).anInt7582 * 846406309 == i) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[i][(((Class282_Sub13) class282_sub13).anInt7569 * 1293000467)]) == null && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 < 0))
					((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	void method15178(int i) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x4) != 0) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 101)); null != class282_sub13; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(-636599739))) {
				if (846406309 * ((Class282_Sub13) class282_sub13).anInt7582 == i)
					((Class282_Sub13) class282_sub13).anInt7583 = 0;
			}
		}
	}

	void method15179(int i) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9707[i] & 0x4) != 0) {
			for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8097((byte) 69)); null != class282_sub13; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) (((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721)).aClass482_9754.method8067(-321653351))) {
				if (846406309 * ((Class282_Sub13) class282_sub13).anInt7582 == i)
					((Class282_Sub13) class282_sub13).anInt7583 = 0;
			}
		}
	}

	void method15180(int i) {
		int i_145_ = i & 0xf0;
		if (128 == i_145_) {
			int i_146_ = i & 0xf;
			int i_147_ = i >> 8 & 0x7f;
			int i_148_ = i >> 16 & 0x7f;
			method15109(i_146_, i_147_, i_148_, (byte) -3);
		} else if (144 == i_145_) {
			int i_149_ = i & 0xf;
			int i_150_ = i >> 8 & 0x7f;
			int i_151_ = i >> 16 & 0x7f;
			if (i_151_ > 0)
				method15107(i_149_, i_150_, i_151_, -739448548);
			else
				method15109(i_149_, i_150_, 64, (byte) -14);
		} else if (160 == i_145_) {
			int i_152_ = i & 0xf;
			int i_153_ = i >> 8 & 0x7f;
			int i_154_ = i >> 16 & 0x7f;
			method15188(i_152_, i_153_, i_154_, 1251880068);
		} else if (176 == i_145_) {
			int i_155_ = i & 0xf;
			int i_156_ = i >> 8 & 0x7f;
			int i_157_ = i >> 16 & 0x7f;
			if (i_156_ == 0)
				((Class282_Sub15_Sub2) this).anIntArray9700[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9700[i_155_] & ~0x1fc000) + (i_157_ << 14);
			if (32 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9700[i_155_] = ((i_157_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9700[i_155_] & ~0x3f80));
			if (1 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9720[i_155_] = ((i_157_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9720[i_155_] & ~0x3f80));
			if (i_156_ == 33)
				((Class282_Sub15_Sub2) this).anIntArray9720[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9720[i_155_] & ~0x7f) + i_157_;
			if (5 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9703[i_155_] = ((i_157_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9703[i_155_] & ~0x3f80));
			if (i_156_ == 37)
				((Class282_Sub15_Sub2) this).anIntArray9703[i_155_] = (i_157_ + (((Class282_Sub15_Sub2) this).anIntArray9703[i_155_] & ~0x7f));
			if (i_156_ == 7)
				((Class282_Sub15_Sub2) this).anIntArray9711[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9711[i_155_] & ~0x3f80) + (i_157_ << 7);
			if (39 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9711[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9711[i_155_] & ~0x7f) + i_157_;
			if (i_156_ == 10)
				((Class282_Sub15_Sub2) this).anIntArray9691[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9691[i_155_] & ~0x3f80) + (i_157_ << 7);
			if (i_156_ == 42)
				((Class282_Sub15_Sub2) this).anIntArray9691[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9691[i_155_] & ~0x7f) + i_157_;
			if (11 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9697[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9697[i_155_] & ~0x3f80) + (i_157_ << 7);
			if (i_156_ == 43)
				((Class282_Sub15_Sub2) this).anIntArray9697[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9697[i_155_] & ~0x7f) + i_157_;
			if (64 == i_156_) {
				if (i_157_ >= 64)
					((Class282_Sub15_Sub2) this).anIntArray9707[i_155_] |= 0x1;
				else
					((Class282_Sub15_Sub2) this).anIntArray9707[i_155_] &= ~0x1;
			}
			if (i_156_ == 65) {
				if (i_157_ >= 64)
					((Class282_Sub15_Sub2) this).anIntArray9707[i_155_] |= 0x2;
				else {
					method15117(i_155_, 560389005);
					((Class282_Sub15_Sub2) this).anIntArray9707[i_155_] &= ~0x2;
				}
			}
			if (i_156_ == 99)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] = ((i_157_ << 7) + (((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] & 0x7f));
			if (i_156_ == 98)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] = (i_157_ + (((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] & 0x3f80));
			if (101 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] = (16384 + (((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] & 0x7f) + (i_157_ << 7));
			if (i_156_ == 100)
				((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9708[i_155_] & 0x3f80) + 16384 + i_157_;
			if (i_156_ == 120)
				method15111(i_155_, (byte) 64);
			if (i_156_ == 121)
				method15114(i_155_, 51239021);
			if (123 == i_156_)
				method15192(i_155_, 970901284);
			if (i_156_ == 6) {
				int i_158_ = ((Class282_Sub15_Sub2) this).anIntArray9708[i_155_];
				if (16384 == i_158_)
					((Class282_Sub15_Sub2) this).anIntArray9709[i_155_] = (i_157_ << 7) + ((((Class282_Sub15_Sub2) this).anIntArray9709[i_155_]) & ~0x3f80);
			}
			if (38 == i_156_) {
				int i_159_ = ((Class282_Sub15_Sub2) this).anIntArray9708[i_155_];
				if (16384 == i_159_)
					((Class282_Sub15_Sub2) this).anIntArray9709[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9709[i_155_] & ~0x7f) + i_157_;
			}
			if (16 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9704[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9704[i_155_] & ~0x3f80) + (i_157_ << 7);
			if (48 == i_156_)
				((Class282_Sub15_Sub2) this).anIntArray9704[i_155_] = (((Class282_Sub15_Sub2) this).anIntArray9704[i_155_] & ~0x7f) + i_157_;
			if (i_156_ == 81) {
				if (i_157_ >= 64)
					((Class282_Sub15_Sub2) this).anIntArray9707[i_155_] |= 0x4;
				else {
					method15118(i_155_, (byte) 73);
					((Class282_Sub15_Sub2) this).anIntArray9707[i_155_] &= ~0x4;
				}
			}
			if (17 == i_156_)
				method15104(i_155_, ((((Class282_Sub15_Sub2) this).anIntArray9723[i_155_]) & ~0x3f80) + (i_157_ << 7), (byte) -59);
			if (i_156_ == 49)
				method15104(i_155_, i_157_ + ((((Class282_Sub15_Sub2) this).anIntArray9723[i_155_]) & ~0x7f), (byte) 18);
		} else if (192 == i_145_) {
			int i_160_ = i & 0xf;
			int i_161_ = i >> 8 & 0x7f;
			method15189(i_160_, i_161_ + (((Class282_Sub15_Sub2) this).anIntArray9700[i_160_]), -1447168848);
		} else if (208 == i_145_) {
			int i_162_ = i & 0xf;
			int i_163_ = i >> 8 & 0x7f;
			method15121(i_162_, i_163_, (byte) 82);
		} else if (224 == i_145_) {
			int i_164_ = i & 0xf;
			int i_165_ = (i >> 8 & 0x7f) + (i >> 9 & 0x3f80);
			method15091(i_164_, i_165_, (byte) 1);
		} else {
			i_145_ = i & 0xff;
			if (i_145_ == 255)
				method15116(true, -1751067350);
		}
	}

	synchronized void method12242(int[] is, int i, int i_166_) {
		if (((Class282_Sub15_Sub2) this).aClass80_9715.method1409()) {
			int i_167_ = (((Class282_Sub15_Sub2) this).anInt9695 * -33380583 * (((Class80) ((Class282_Sub15_Sub2) this).aClass80_9715).anInt785) / Class253.anInt3129);
			do {
				long l = ((long) i_167_ * (long) i_166_ + (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710));
				if ((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - l >= 0L) {
					((Class282_Sub15_Sub2) this).aLong9710 = l * 8282832003758463051L;
					break;
				}
				int i_168_ = (int) (((((Class282_Sub15_Sub2) this).aLong9693 * 6737959858605438665L) - (5773041712000823651L * ((Class282_Sub15_Sub2) this).aLong9710) + (long) i_167_ - 1L) / (long) i_167_);
				((Class282_Sub15_Sub2) this).aLong9710 += 8282832003758463051L * ((long) i_168_ * (long) i_167_);
				((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_168_);
				i += i_168_;
				i_166_ -= i_168_;
				method15127(1959431155);
			} while (((Class282_Sub15_Sub2) this).aClass80_9715.method1409());
		}
		((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721.method12230(is, i, i_166_);
	}

	void method15181(int i, int i_169_) {
		((Class282_Sub15_Sub2) this).anIntArray9723[i] = i_169_;
		((Class282_Sub15_Sub2) this).anIntArray9712[i] = (int) (2097152.0 * Math.pow(2.0, (double) i_169_ * 5.4931640625E-4) + 0.5);
	}

	synchronized boolean method15182(Class282_Sub7 class282_sub7, Class317 class317, Class250 class250, int i, int i_170_) {
		class282_sub7.method12165();
		boolean bool = true;
		int[] is = null;
		if (i > 0)
			is = new int[] { i };
		for (Class282_Sub47 class282_sub47 = (Class282_Sub47) ((Class282_Sub7) class282_sub7).aClass465_7521.method7750(772569462); class282_sub47 != null; class282_sub47 = (Class282_Sub47) ((Class282_Sub7) class282_sub7).aClass465_7521.method7751((byte) 16)) {
			int i_171_ = (int) (-3442165056282524525L * class282_sub47.aLong3379);
			Class282_Sub14 class282_sub14 = (Class282_Sub14) ((Class282_Sub15_Sub2) this).aClass465_9694.method7754((long) i_171_);
			if (class282_sub14 == null) {
				class282_sub14 = Class96_Sub21.method14676(class317, i_171_, 450746801);
				if (class282_sub14 == null) {
					bool = false;
					continue;
				}
				((Class282_Sub15_Sub2) this).aClass465_9694.method7765(class282_sub14, (long) i_171_);
			}
			if (!class282_sub14.method12216(class250, ((byte[]) class282_sub47.anObject8068), is, -1083943154))
				bool = false;
		}
		if (bool)
			class282_sub7.method12167();
		return bool;
	}

	boolean method15183(Class282_Sub13 class282_sub13, int[] is, int i, int i_172_) {
		((Class282_Sub13) class282_sub13).anInt7584 = -431461175 * (Class253.anInt3129 / 100);
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0 && (null == (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568) || ((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15319((byte) -37))) {
			class282_sub13.method12213(-2085241547);
			class282_sub13.method4991(-371378792);
			if (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0 && (class282_sub13 == (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)])))
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[((Class282_Sub13) class282_sub13).anInt7582 * 846406309][-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567] = null;
			return true;
		}
		int i_173_ = -1958822223 * ((Class282_Sub13) class282_sub13).anInt7575;
		if (i_173_ > 0) {
			i_173_ -= (int) ((16.0 * Math.pow(2.0, ((double) (((Class282_Sub15_Sub2) this).anIntArray9703[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * 4.921259842519685E-4))) + 0.5);
			if (i_173_ < 0)
				i_173_ = 0;
			((Class282_Sub13) class282_sub13).anInt7575 = i_173_ * 1359923793;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15308(method15120(class282_sub13, -1769335625), (short) 3883);
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		boolean bool = false;
		((Class282_Sub13) class282_sub13).anInt7581 += -903904431;
		((Class282_Sub13) class282_sub13).anInt7571 += ((Class110) class110).anInt1101 * -370064085;
		double d = (5.086263020833333E-6 * (double) (((-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574 * (((Class282_Sub13) class282_sub13).anInt7575 * -1958822223)) >> 12) + (1293000467 * (((Class282_Sub13) class282_sub13).anInt7569) - 60 << 8)));
		if (1284723511 * ((Class110) class110).anInt1096 > 0) {
			if (-1649764267 * ((Class110) class110).anInt1099 > 0)
				((Class282_Sub13) class282_sub13).anInt7576 += (int) ((128.0 * Math.pow(2.0, d * (double) ((((Class110) class110).anInt1099) * -1649764267))) + 0.5) * 1188817509;
			else
				((Class282_Sub13) class282_sub13).anInt7576 += 1844785792;
			if ((1284723511 * ((Class110) class110).anInt1096 * (((Class282_Sub13) class282_sub13).anInt7576 * 69449581)) >= 819200)
				bool = true;
		}
		if (((Class110) class110).aByteArray1097 != null) {
			if (1684458023 * ((Class110) class110).anInt1095 > 0)
				((Class282_Sub13) class282_sub13).anInt7577 += (int) (128.0 * Math.pow(2.0, d * (double) (1684458023 * (((Class110) class110).anInt1095))) + 0.5) * 35369279;
			else
				((Class282_Sub13) class282_sub13).anInt7577 += 232300416;
			for (/**/; ((((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) && (-474562881 * ((Class282_Sub13) class282_sub13).anInt7577 > ((((Class110) class110).aByteArray1097[2 + 1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7578 += -1958397046) {
				/* empty */
			}
			if ((((Class110) class110).aByteArray1097.length - 2 == ((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) && (((Class110) class110).aByteArray1097[(1159387405 * ((Class282_Sub13) class282_sub13).anInt7578 + 1)]) == 0)
				bool = true;
		}
		if (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 >= 0 && null != ((Class110) class110).aByteArray1094 && (0 == ((((Class282_Sub15_Sub2) this).anIntArray9707[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) & 0x1)) && (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 < 0 || (class282_sub13 != (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)])))) {
			if (((Class110) class110).anInt1098 * 1351159779 > 0)
				((Class282_Sub13) class282_sub13).anInt7579 += (int) (128.0 * Math.pow(2.0, d * (double) (1351159779 * (((Class110) class110).anInt1098))) + 0.5) * 523944391;
			else
				((Class282_Sub13) class282_sub13).anInt7579 += -1654594688;
			for (/**/; ((-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 > ((((Class110) class110).aByteArray1094[2 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7580 += -1517554074) {
				/* empty */
			}
			if (((Class110) class110).aByteArray1094.length - 2 == -9799685 * ((Class282_Sub13) class282_sub13).anInt7580)
				bool = true;
		}
		if (bool) {
			((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15322(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, (byte) 34);
			if (null != is)
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12230(is, i, i_172_);
			else
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12231(i_172_);
			if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15311((byte) 33))
				((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass282_Sub15_Sub4_9755.method15275(((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568);
			class282_sub13.method12213(-2085241547);
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((((Class282_Sub13) class282_sub13).anInt7567 * -2127113783 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)] = null;
			}
			return true;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15321(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, method15108(class282_sub13, -1910058765), method15122(class282_sub13, -473194076), 473105857);
		return false;
	}

	int method15184(Class282_Sub13 class282_sub13) {
		if ((((Class282_Sub15_Sub2) this).anIntArray9702[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) == 0)
			return 0;
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		int i = ((((((Class282_Sub15_Sub2) this).anIntArray9697[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]) * (((Class282_Sub15_Sub2) this).anIntArray9711[846406309 * ((Class282_Sub13) class282_sub13).anInt7582])) + 4096) >> 13);
		i = i * i + 16384 >> 15;
		i = 16384 + (((Class282_Sub13) class282_sub13).anInt7570 * 1470346527 * i) >> 15;
		i = 1824241431 * ((Class282_Sub15_Sub2) this).anInt9692 * i + 128 >> 8;
		i = i * (((Class282_Sub15_Sub2) this).anInt9690 * 513424527) >> 8;
		i = (i * (((Class282_Sub15_Sub2) this).anIntArray9702[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]) + 128) >> 8;
		if (((Class110) class110).anInt1096 * 1284723511 > 0)
			i = (int) (((double) i * Math.pow(0.5, ((double) (69449581 * ((Class282_Sub13) class282_sub13).anInt7576) * 1.953125E-5 * (double) (1284723511 * (((Class110) class110).anInt1096))))) + 0.5);
		if (null != ((Class110) class110).aByteArray1097) {
			int i_174_ = ((Class282_Sub13) class282_sub13).anInt7577 * -474562881;
			int i_175_ = (((Class110) class110).aByteArray1097[1 + (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405)]);
			if (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) {
				int i_176_ = ((((Class110) class110).aByteArray1097[1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8;
				int i_177_ = (((((Class110) class110).aByteArray1097[2 + (((Class282_Sub13) class282_sub13).anInt7578 * 1159387405)]) & 0xff) << 8);
				i_175_ += ((((Class110) class110).aByteArray1097[(((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) + 3]) - i_175_) * (i_174_ - i_176_) / (i_177_ - i_176_);
			}
			i = i * i_175_ + 32 >> 6;
		}
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 > 0 && ((Class110) class110).aByteArray1094 != null) {
			int i_178_ = 761835511 * ((Class282_Sub13) class282_sub13).anInt7579;
			int i_179_ = (((Class110) class110).aByteArray1094[1 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]);
			if (-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) {
				int i_180_ = (((((Class110) class110).aByteArray1094[(((Class282_Sub13) class282_sub13).anInt7580 * -9799685)]) & 0xff) << 8);
				int i_181_ = (((((Class110) class110).aByteArray1094[(((Class282_Sub13) class282_sub13).anInt7580 * -9799685) + 2]) & 0xff) << 8);
				i_179_ += ((((Class110) class110).aByteArray1094[3 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) - i_179_) * (i_178_ - i_180_) / (i_181_ - i_180_);
			}
			i = i_179_ * i + 32 >> 6;
		}
		return i;
	}

	boolean method15185(Class282_Sub13 class282_sub13, int[] is, int i, int i_182_) {
		((Class282_Sub13) class282_sub13).anInt7584 = -431461175 * (Class253.anInt3129 / 100);
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0 && (null == (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568) || ((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15319((byte) -102))) {
			class282_sub13.method12213(-2085241547);
			class282_sub13.method4991(-371378792);
			if (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0 && (class282_sub13 == (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)])))
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[((Class282_Sub13) class282_sub13).anInt7582 * 846406309][-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567] = null;
			return true;
		}
		int i_183_ = -1958822223 * ((Class282_Sub13) class282_sub13).anInt7575;
		if (i_183_ > 0) {
			i_183_ -= (int) ((16.0 * Math.pow(2.0, ((double) (((Class282_Sub15_Sub2) this).anIntArray9703[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * 4.921259842519685E-4))) + 0.5);
			if (i_183_ < 0)
				i_183_ = 0;
			((Class282_Sub13) class282_sub13).anInt7575 = i_183_ * 1359923793;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15308(method15120(class282_sub13, -837360272), (short) 3289);
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		boolean bool = false;
		((Class282_Sub13) class282_sub13).anInt7581 += -903904431;
		((Class282_Sub13) class282_sub13).anInt7571 += ((Class110) class110).anInt1101 * -370064085;
		double d = (5.086263020833333E-6 * (double) (((-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574 * (((Class282_Sub13) class282_sub13).anInt7575 * -1958822223)) >> 12) + (1293000467 * (((Class282_Sub13) class282_sub13).anInt7569) - 60 << 8)));
		if (1284723511 * ((Class110) class110).anInt1096 > 0) {
			if (-1649764267 * ((Class110) class110).anInt1099 > 0)
				((Class282_Sub13) class282_sub13).anInt7576 += (int) ((128.0 * Math.pow(2.0, d * (double) ((((Class110) class110).anInt1099) * -1649764267))) + 0.5) * 1188817509;
			else
				((Class282_Sub13) class282_sub13).anInt7576 += 1844785792;
			if ((1284723511 * ((Class110) class110).anInt1096 * (((Class282_Sub13) class282_sub13).anInt7576 * 69449581)) >= 819200)
				bool = true;
		}
		if (((Class110) class110).aByteArray1097 != null) {
			if (1684458023 * ((Class110) class110).anInt1095 > 0)
				((Class282_Sub13) class282_sub13).anInt7577 += (int) (128.0 * Math.pow(2.0, d * (double) (1684458023 * (((Class110) class110).anInt1095))) + 0.5) * 35369279;
			else
				((Class282_Sub13) class282_sub13).anInt7577 += 232300416;
			for (/**/; ((((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) && (-474562881 * ((Class282_Sub13) class282_sub13).anInt7577 > ((((Class110) class110).aByteArray1097[2 + 1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7578 += -1958397046) {
				/* empty */
			}
			if ((((Class110) class110).aByteArray1097.length - 2 == ((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) && (((Class110) class110).aByteArray1097[(1159387405 * ((Class282_Sub13) class282_sub13).anInt7578 + 1)]) == 0)
				bool = true;
		}
		if (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 >= 0 && null != ((Class110) class110).aByteArray1094 && (0 == ((((Class282_Sub15_Sub2) this).anIntArray9707[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) & 0x1)) && (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 < 0 || (class282_sub13 != (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)])))) {
			if (((Class110) class110).anInt1098 * 1351159779 > 0)
				((Class282_Sub13) class282_sub13).anInt7579 += (int) (128.0 * Math.pow(2.0, d * (double) (1351159779 * (((Class110) class110).anInt1098))) + 0.5) * 523944391;
			else
				((Class282_Sub13) class282_sub13).anInt7579 += -1654594688;
			for (/**/; ((-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 > ((((Class110) class110).aByteArray1094[2 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7580 += -1517554074) {
				/* empty */
			}
			if (((Class110) class110).aByteArray1094.length - 2 == -9799685 * ((Class282_Sub13) class282_sub13).anInt7580)
				bool = true;
		}
		if (bool) {
			((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15322(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, (byte) -17);
			if (null != is)
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12230(is, i, i_182_);
			else
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12231(i_182_);
			if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15311((byte) 33))
				((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass282_Sub15_Sub4_9755.method15275(((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568);
			class282_sub13.method12213(-2085241547);
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((((Class282_Sub13) class282_sub13).anInt7567 * -2127113783 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)] = null;
			}
			return true;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15321(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, method15108(class282_sub13, 1249176505), method15122(class282_sub13, -76687160), -510780144);
		return false;
	}

	int method15186(Class282_Sub13 class282_sub13) {
		int i = (((Class282_Sub15_Sub2) this).anIntArray9691[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]);
		if (i < 8192)
			return (32 + (((Class282_Sub13) class282_sub13).anInt7572 * -1677730315 * i) >> 6);
		return (16384 - (32 + (16384 - i) * (128 - -1677730315 * (((Class282_Sub13) class282_sub13).anInt7572)) >> 6));
	}

	synchronized void method15187() {
		method15102(true, -1682906105);
	}

	void method15188(int i, int i_184_, int i_185_, int i_186_) {
		/* empty */
	}

	void method15189(int i, int i_187_, int i_188_) {
		if (i_187_ != ((Class282_Sub15_Sub2) this).anIntArray9699[i]) {
			((Class282_Sub15_Sub2) this).anIntArray9699[i] = i_187_;
			for (int i_189_ = 0; i_189_ < 128; i_189_++)
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[i][i_189_] = null;
		}
	}

	boolean method15190(Class282_Sub13 class282_sub13) {
		if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568 == null) {
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)] = null;
			}
			return true;
		}
		return false;
	}

	boolean method15191(Class282_Sub13 class282_sub13) {
		if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568 == null) {
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)] = null;
			}
			return true;
		}
		return false;
	}

	void method15192(int i, int i_190_) {
		for (Class282_Sub13 class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8097((byte) 21)); null != class282_sub13; class282_sub13 = ((Class282_Sub13) ((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass482_9754.method8067(-250860907))) {
			if ((i < 0 || i == (846406309 * ((Class282_Sub13) class282_sub13).anInt7582)) && (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 < 0)) {
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9713[846406309 * ((Class282_Sub13) class282_sub13).anInt7582][((Class282_Sub13) class282_sub13).anInt7569 * 1293000467] = null;
				((Class282_Sub13) class282_sub13).anInt7579 = 0;
			}
		}
	}

	boolean method15193(Class282_Sub13 class282_sub13, int[] is, int i, int i_191_) {
		((Class282_Sub13) class282_sub13).anInt7584 = -431461175 * (Class253.anInt3129 / 100);
		if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0 && (null == (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568) || ((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15319((byte) -113))) {
			class282_sub13.method12213(-2085241547);
			class282_sub13.method4991(-371378792);
			if (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 > 0 && (class282_sub13 == (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567)])))
				((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[((Class282_Sub13) class282_sub13).anInt7582 * 846406309][-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567] = null;
			return true;
		}
		int i_192_ = -1958822223 * ((Class282_Sub13) class282_sub13).anInt7575;
		if (i_192_ > 0) {
			i_192_ -= (int) ((16.0 * Math.pow(2.0, ((double) (((Class282_Sub15_Sub2) this).anIntArray9703[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)]) * 4.921259842519685E-4))) + 0.5);
			if (i_192_ < 0)
				i_192_ = 0;
			((Class282_Sub13) class282_sub13).anInt7575 = i_192_ * 1359923793;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15308(method15120(class282_sub13, -838186396), (short) 15937);
		Class110 class110 = ((Class282_Sub13) class282_sub13).aClass110_7586;
		boolean bool = false;
		((Class282_Sub13) class282_sub13).anInt7581 += -903904431;
		((Class282_Sub13) class282_sub13).anInt7571 += ((Class110) class110).anInt1101 * -370064085;
		double d = (5.086263020833333E-6 * (double) (((-1612704189 * ((Class282_Sub13) class282_sub13).anInt7574 * (((Class282_Sub13) class282_sub13).anInt7575 * -1958822223)) >> 12) + (1293000467 * (((Class282_Sub13) class282_sub13).anInt7569) - 60 << 8)));
		if (1284723511 * ((Class110) class110).anInt1096 > 0) {
			if (-1649764267 * ((Class110) class110).anInt1099 > 0)
				((Class282_Sub13) class282_sub13).anInt7576 += (int) ((128.0 * Math.pow(2.0, d * (double) ((((Class110) class110).anInt1099) * -1649764267))) + 0.5) * 1188817509;
			else
				((Class282_Sub13) class282_sub13).anInt7576 += 1844785792;
			if ((1284723511 * ((Class110) class110).anInt1096 * (((Class282_Sub13) class282_sub13).anInt7576 * 69449581)) >= 819200)
				bool = true;
		}
		if (((Class110) class110).aByteArray1097 != null) {
			if (1684458023 * ((Class110) class110).anInt1095 > 0)
				((Class282_Sub13) class282_sub13).anInt7577 += (int) (128.0 * Math.pow(2.0, d * (double) (1684458023 * (((Class110) class110).anInt1095))) + 0.5) * 35369279;
			else
				((Class282_Sub13) class282_sub13).anInt7577 += 232300416;
			for (/**/; ((((Class282_Sub13) class282_sub13).anInt7578 * 1159387405 < ((Class110) class110).aByteArray1097.length - 2) && (-474562881 * ((Class282_Sub13) class282_sub13).anInt7577 > ((((Class110) class110).aByteArray1097[2 + 1159387405 * (((Class282_Sub13) class282_sub13).anInt7578)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7578 += -1958397046) {
				/* empty */
			}
			if ((((Class110) class110).aByteArray1097.length - 2 == ((Class282_Sub13) class282_sub13).anInt7578 * 1159387405) && (((Class110) class110).aByteArray1097[(1159387405 * ((Class282_Sub13) class282_sub13).anInt7578 + 1)]) == 0)
				bool = true;
		}
		if (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 >= 0 && null != ((Class110) class110).aByteArray1094 && (0 == ((((Class282_Sub15_Sub2) this).anIntArray9707[((Class282_Sub13) class282_sub13).anInt7582 * 846406309]) & 0x1)) && (-2127113783 * ((Class282_Sub13) class282_sub13).anInt7567 < 0 || (class282_sub13 != (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)])))) {
			if (((Class110) class110).anInt1098 * 1351159779 > 0)
				((Class282_Sub13) class282_sub13).anInt7579 += (int) (128.0 * Math.pow(2.0, d * (double) (1351159779 * (((Class110) class110).anInt1098))) + 0.5) * 523944391;
			else
				((Class282_Sub13) class282_sub13).anInt7579 += -1654594688;
			for (/**/; ((-9799685 * ((Class282_Sub13) class282_sub13).anInt7580 < ((Class110) class110).aByteArray1094.length - 2) && (761835511 * ((Class282_Sub13) class282_sub13).anInt7579 > ((((Class110) class110).aByteArray1094[2 + -9799685 * (((Class282_Sub13) class282_sub13).anInt7580)]) & 0xff) << 8)); ((Class282_Sub13) class282_sub13).anInt7580 += -1517554074) {
				/* empty */
			}
			if (((Class110) class110).aByteArray1094.length - 2 == -9799685 * ((Class282_Sub13) class282_sub13).anInt7580)
				bool = true;
		}
		if (bool) {
			((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15322(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, (byte) 53);
			if (null != is)
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12230(is, i, i_191_);
			else
				((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method12231(i_191_);
			if (((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15311((byte) 33))
				((Class282_Sub15_Sub3) ((Class282_Sub15_Sub2) this).aClass282_Sub15_Sub3_9721).aClass282_Sub15_Sub4_9755.method15275(((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568);
			class282_sub13.method12213(-2085241547);
			if (((Class282_Sub13) class282_sub13).anInt7579 * 761835511 >= 0) {
				class282_sub13.method4991(-371378792);
				if ((((Class282_Sub13) class282_sub13).anInt7567 * -2127113783 > 0) && (((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(846406309 * ((Class282_Sub13) class282_sub13).anInt7582)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)]) == class282_sub13)
					((Class282_Sub15_Sub2) this).aClass282_Sub13ArrayArray9714[(((Class282_Sub13) class282_sub13).anInt7582 * 846406309)][(((Class282_Sub13) class282_sub13).anInt7567 * -2127113783)] = null;
			}
			return true;
		}
		((Class282_Sub13) class282_sub13).aClass282_Sub15_Sub5_7568.method15321(((Class282_Sub13) class282_sub13).anInt7584 * 1985703289, method15108(class282_sub13, -1444643670), method15122(class282_sub13, -800792783), -2020377886);
		return false;
	}

	int method15194() {
		return 1824241431 * ((Class282_Sub15_Sub2) this).anInt9692;
	}

	int method15195(Class282_Sub13 class282_sub13) {
		int i = (((Class282_Sub15_Sub2) this).anIntArray9691[846406309 * ((Class282_Sub13) class282_sub13).anInt7582]);
		if (i < 8192)
			return (32 + (((Class282_Sub13) class282_sub13).anInt7572 * -1677730315 * i) >> 6);
		return (16384 - (32 + (16384 - i) * (128 - -1677730315 * (((Class282_Sub13) class282_sub13).anInt7572)) >> 6));
	}
}
