
/* Class477 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Iterator;

public class Class477 implements Iterable {
	public Class282_Sub50 aClass282_Sub50_5629 = new Class282_Sub50();
	static int anInt5630;
	Class282_Sub50 aClass282_Sub50_5631;
	static Class539[] aClass539Array5632;

	public Iterator method7934() {
		return new Class469(this);
	}

	public void method7935(byte i) {
		while (aClass282_Sub50_5629.aClass282_Sub50_8119 != aClass282_Sub50_5629)
			aClass282_Sub50_5629.aClass282_Sub50_8119.method13452((byte) -5);
	}

	public void method7936(Class282_Sub50 class282_sub50, int i) {
		if (class282_sub50.aClass282_Sub50_8118 != null)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = aClass282_Sub50_5629.aClass282_Sub50_8118;
		class282_sub50.aClass282_Sub50_8119 = aClass282_Sub50_5629;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	public Class282_Sub50 method7937(int i) {
		Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119;
		if (class282_sub50 == aClass282_Sub50_5629)
			return null;
		class282_sub50.method13452((byte) -5);
		return class282_sub50;
	}

	public Class282_Sub50 method7938() {
		return method7946(null, -508721714);
	}

	public int method7939(int i) {
		int i_0_ = 0;
		for (Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119; aClass282_Sub50_5629 != class282_sub50; class282_sub50 = class282_sub50.aClass282_Sub50_8119)
			i_0_++;
		return i_0_;
	}

	public Class282_Sub50 method7940() {
		Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119;
		if (class282_sub50 == aClass282_Sub50_5629)
			return null;
		class282_sub50.method13452((byte) -5);
		return class282_sub50;
	}

	public Class477() {
		aClass282_Sub50_5629.aClass282_Sub50_8119 = aClass282_Sub50_5629;
		aClass282_Sub50_5629.aClass282_Sub50_8118 = aClass282_Sub50_5629;
	}

	public Class282_Sub50 method7941(byte i) {
		return method7946(null, -2037217557);
	}

	public Iterator method7942() {
		return new Class469(this);
	}

	public static void method7943(Class282_Sub50 class282_sub50, Class282_Sub50 class282_sub50_1_) {
		if (null != class282_sub50.aClass282_Sub50_8118)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = class282_sub50_1_.aClass282_Sub50_8118;
		class282_sub50.aClass282_Sub50_8119 = class282_sub50_1_;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	public static void method7944(Class282_Sub50 class282_sub50, Class282_Sub50 class282_sub50_2_) {
		if (null != class282_sub50.aClass282_Sub50_8118)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = class282_sub50_2_.aClass282_Sub50_8118;
		class282_sub50.aClass282_Sub50_8119 = class282_sub50_2_;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	public static void method7945(Class282_Sub50 class282_sub50, Class282_Sub50 class282_sub50_3_) {
		if (null != class282_sub50.aClass282_Sub50_8118)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = class282_sub50_3_.aClass282_Sub50_8118;
		class282_sub50.aClass282_Sub50_8119 = class282_sub50_3_;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	Class282_Sub50 method7946(Class282_Sub50 class282_sub50, int i) {
		Class282_Sub50 class282_sub50_4_;
		if (class282_sub50 == null)
			class282_sub50_4_ = aClass282_Sub50_5629.aClass282_Sub50_8119;
		else
			class282_sub50_4_ = class282_sub50;
		if (aClass282_Sub50_5629 == class282_sub50_4_) {
			((Class477) this).aClass282_Sub50_5631 = null;
			return null;
		}
		((Class477) this).aClass282_Sub50_5631 = class282_sub50_4_.aClass282_Sub50_8119;
		return class282_sub50_4_;
	}

	public static void method7947(Class282_Sub50 class282_sub50, Class282_Sub50 class282_sub50_5_) {
		if (null != class282_sub50.aClass282_Sub50_8118)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = class282_sub50_5_;
		class282_sub50.aClass282_Sub50_8119 = class282_sub50_5_.aClass282_Sub50_8119;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	public static void method7948(Class282_Sub50 class282_sub50, Class282_Sub50 class282_sub50_6_) {
		if (null != class282_sub50.aClass282_Sub50_8118)
			class282_sub50.method13452((byte) -5);
		class282_sub50.aClass282_Sub50_8118 = class282_sub50_6_;
		class282_sub50.aClass282_Sub50_8119 = class282_sub50_6_.aClass282_Sub50_8119;
		class282_sub50.aClass282_Sub50_8118.aClass282_Sub50_8119 = class282_sub50;
		class282_sub50.aClass282_Sub50_8119.aClass282_Sub50_8118 = class282_sub50;
	}

	public Class282_Sub50 method7949() {
		Class282_Sub50 class282_sub50 = ((Class477) this).aClass282_Sub50_5631;
		if (aClass282_Sub50_5629 == class282_sub50) {
			((Class477) this).aClass282_Sub50_5631 = null;
			return null;
		}
		((Class477) this).aClass282_Sub50_5631 = class282_sub50.aClass282_Sub50_8119;
		return class282_sub50;
	}

	public Class282_Sub50 method7950() {
		Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119;
		if (class282_sub50 == aClass282_Sub50_5629)
			return null;
		class282_sub50.method13452((byte) -5);
		return class282_sub50;
	}

	public int method7951() {
		int i = 0;
		for (Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119; aClass282_Sub50_5629 != class282_sub50; class282_sub50 = class282_sub50.aClass282_Sub50_8119)
			i++;
		return i;
	}

	Class282_Sub50 method7952(Class282_Sub50 class282_sub50) {
		Class282_Sub50 class282_sub50_7_;
		if (class282_sub50 == null)
			class282_sub50_7_ = aClass282_Sub50_5629.aClass282_Sub50_8119;
		else
			class282_sub50_7_ = class282_sub50;
		if (aClass282_Sub50_5629 == class282_sub50_7_) {
			((Class477) this).aClass282_Sub50_5631 = null;
			return null;
		}
		((Class477) this).aClass282_Sub50_5631 = class282_sub50_7_.aClass282_Sub50_8119;
		return class282_sub50_7_;
	}

	public Class282_Sub50 method7953() {
		Class282_Sub50 class282_sub50 = ((Class477) this).aClass282_Sub50_5631;
		if (aClass282_Sub50_5629 == class282_sub50) {
			((Class477) this).aClass282_Sub50_5631 = null;
			return null;
		}
		((Class477) this).aClass282_Sub50_5631 = class282_sub50.aClass282_Sub50_8119;
		return class282_sub50;
	}

	public Class282_Sub50 method7954() {
		Class282_Sub50 class282_sub50 = ((Class477) this).aClass282_Sub50_5631;
		if (aClass282_Sub50_5629 == class282_sub50) {
			((Class477) this).aClass282_Sub50_5631 = null;
			return null;
		}
		((Class477) this).aClass282_Sub50_5631 = class282_sub50.aClass282_Sub50_8119;
		return class282_sub50;
	}

	public Class282_Sub50 method7955(int i) {
		Class282_Sub50 class282_sub50 = ((Class477) this).aClass282_Sub50_5631;
		if (aClass282_Sub50_5629 == class282_sub50) {
			((Class477) this).aClass282_Sub50_5631 = null;
			return null;
		}
		((Class477) this).aClass282_Sub50_5631 = class282_sub50.aClass282_Sub50_8119;
		return class282_sub50;
	}

	public Class282_Sub50 method7956() {
		Class282_Sub50 class282_sub50 = ((Class477) this).aClass282_Sub50_5631;
		if (aClass282_Sub50_5629 == class282_sub50) {
			((Class477) this).aClass282_Sub50_5631 = null;
			return null;
		}
		((Class477) this).aClass282_Sub50_5631 = class282_sub50.aClass282_Sub50_8119;
		return class282_sub50;
	}

	public int method7957() {
		int i = 0;
		for (Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119; aClass282_Sub50_5629 != class282_sub50; class282_sub50 = class282_sub50.aClass282_Sub50_8119)
			i++;
		return i;
	}

	public int method7958() {
		int i = 0;
		for (Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119; aClass282_Sub50_5629 != class282_sub50; class282_sub50 = class282_sub50.aClass282_Sub50_8119)
			i++;
		return i;
	}

	public int method7959() {
		int i = 0;
		for (Class282_Sub50 class282_sub50 = aClass282_Sub50_5629.aClass282_Sub50_8119; aClass282_Sub50_5629 != class282_sub50; class282_sub50 = class282_sub50.aClass282_Sub50_8119)
			i++;
		return i;
	}

	public Iterator iterator() {
		return new Class469(this);
	}

	public static void method7960(Class317 class317, byte i) {
		Class16.anInt140 = class317.method5610("p11_full", -1671336895) * 1669464149;
		Class16.anInt136 = class317.method5610("p12_full", 568750453) * 1957167017;
		Class395.anInt4788 = class317.method5610("b12_full", 776026920) * 2033224767;
	}

	static final void method7961(Class527 class527, int i) {
		int i_8_ = (((Class527) class527).anIntArray6999[(((Class527) class527).anInt7012 -= 141891001) * 1942118537]);
		Class118 class118 = Class117.method1981(i_8_, (byte) 26);
		Class98 class98 = Class468_Sub8.aClass98Array7889[i_8_ >> 16];
		Class204.method3366(class118, class98, class527, (byte) -87);
	}

	static final void method7962(Class527 class527, byte i) {
		if (client.aBool7310) {
			if (i != -1) {
				for (;;) {
					/* empty */
				}
			}
			Class361.aClass361_4175.method6257(187369662);
		}
	}
}
