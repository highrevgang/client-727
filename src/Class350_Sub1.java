/* Class350_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class350_Sub1 extends Class350 {
	public int anInt7755;
	public int anInt7756;
	public static Class406 aClass406_7757;

	Class350_Sub1(Class356 class356, Class353 class353, int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		super(class356, class353, i, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_);
		anInt7755 = 1943314903 * i_7_;
		anInt7756 = i_8_ * 478363769;
	}

	public Class60 method24(int i) {
		return Class60.aClass60_603;
	}

	public Class60 method25() {
		return Class60.aClass60_603;
	}

	public static Class350 method12511(RsByteBuffer class282_sub35) {
		Class350 class350 = Class383.method6512(class282_sub35, 88062096);
		int i = class282_sub35.readIntLE();
		int i_9_ = class282_sub35.readIntLE();
		int i_10_ = class282_sub35.readBigSmart(1954140362);
		return new Class350_Sub1(class350.aClass356_4094, class350.aClass353_4087, class350.anInt4090 * -1967081549, class350.anInt4089 * -1196256967, 329542577 * class350.anInt4093, class350.anInt4088 * 323608093, -1921815535 * class350.anInt4092, 985690519 * class350.anInt4086, -771513131 * class350.anInt4091, i, i_9_, i_10_);
	}

	public static Class350 method12512(RsByteBuffer class282_sub35) {
		Class350 class350 = Class383.method6512(class282_sub35, 88062096);
		int i = class282_sub35.readIntLE();
		int i_11_ = class282_sub35.readIntLE();
		int i_12_ = class282_sub35.readBigSmart(2102309053);
		return new Class350_Sub1(class350.aClass356_4094, class350.aClass353_4087, class350.anInt4090 * -1967081549, class350.anInt4089 * -1196256967, 329542577 * class350.anInt4093, class350.anInt4088 * 323608093, -1921815535 * class350.anInt4092, 985690519 * class350.anInt4086, -771513131 * class350.anInt4091, i, i_11_, i_12_);
	}

	public static Class350 method12513(RsByteBuffer class282_sub35) {
		Class350 class350 = Class383.method6512(class282_sub35, 88062096);
		int i = class282_sub35.readIntLE();
		int i_13_ = class282_sub35.readIntLE();
		int i_14_ = class282_sub35.readBigSmart(2108324006);
		return new Class350_Sub1(class350.aClass356_4094, class350.aClass353_4087, class350.anInt4090 * -1967081549, class350.anInt4089 * -1196256967, 329542577 * class350.anInt4093, class350.anInt4088 * 323608093, -1921815535 * class350.anInt4092, 985690519 * class350.anInt4086, -771513131 * class350.anInt4091, i, i_13_, i_14_);
	}

	public static Class350 method12514(RsByteBuffer class282_sub35) {
		Class350 class350 = Class383.method6512(class282_sub35, 88062096);
		int i = class282_sub35.readIntLE();
		int i_15_ = class282_sub35.readIntLE();
		int i_16_ = class282_sub35.readBigSmart(1990558306);
		return new Class350_Sub1(class350.aClass356_4094, class350.aClass353_4087, class350.anInt4090 * -1967081549, class350.anInt4089 * -1196256967, 329542577 * class350.anInt4093, class350.anInt4088 * 323608093, -1921815535 * class350.anInt4092, 985690519 * class350.anInt4086, -771513131 * class350.anInt4091, i, i_15_, i_16_);
	}

	public static Class350 method12515(RsByteBuffer class282_sub35) {
		Class350 class350 = Class383.method6512(class282_sub35, 88062096);
		int i = class282_sub35.readIntLE();
		int i_17_ = class282_sub35.readIntLE();
		int i_18_ = class282_sub35.readBigSmart(2086278906);
		return new Class350_Sub1(class350.aClass356_4094, class350.aClass353_4087, class350.anInt4090 * -1967081549, class350.anInt4089 * -1196256967, 329542577 * class350.anInt4093, class350.anInt4088 * 323608093, -1921815535 * class350.anInt4092, 985690519 * class350.anInt4086, -771513131 * class350.anInt4091, i, i_17_, i_18_);
	}

	static final void method12516(int i) {
		for (Class282_Sub31 class282_sub31 = ((Class282_Sub31) Class282_Sub31.aClass482_7775.method8097((byte) 85)); class282_sub31 != null; class282_sub31 = (Class282_Sub31) Class282_Sub31.aClass482_7775.method8067(486596704))
			Class465.method7772(class282_sub31, false, 1535779425);
		for (Class282_Sub31 class282_sub31 = ((Class282_Sub31) Class282_Sub31.aClass482_7776.method8097((byte) 127)); class282_sub31 != null; class282_sub31 = (Class282_Sub31) Class282_Sub31.aClass482_7776.method8067(2087235894))
			Class465.method7772(class282_sub31, true, 1535779425);
	}
}
