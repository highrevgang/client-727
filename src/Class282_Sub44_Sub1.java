/* Class282_Sub44_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class282_Sub44_Sub1 extends Class282_Sub44 {
	int anInt9460;

	public boolean method13403() {
		Class282_Sub47 class282_sub47 = ((Class282_Sub47) (client.aClass465_7208.method7754((long) (651363737 * ((Class282_Sub44_Sub1) this).anInt9460))));
		if (null != class282_sub47) {
			Class1.method252(Class397.aClass397_4799, anInt8063 * 587626901, -1, ((Class521_Sub1_Sub1_Sub2) class282_sub47.anObject8068), (((Class282_Sub44_Sub1) this).anInt9460 * 651363737), 1441602525);
			return true;
		}
		return false;
	}

	public boolean method13401(byte i) {
		Class282_Sub47 class282_sub47 = ((Class282_Sub47) (client.aClass465_7208.method7754((long) (651363737 * ((Class282_Sub44_Sub1) this).anInt9460))));
		if (null != class282_sub47) {
			if (i >= -1)
				throw new IllegalStateException();
			Class1.method252(Class397.aClass397_4799, anInt8063 * 587626901, -1, ((Class521_Sub1_Sub1_Sub2) class282_sub47.anObject8068), (((Class282_Sub44_Sub1) this).anInt9460 * 651363737), 1712110950);
			return true;
		}
		return false;
	}

	public boolean method13402() {
		Class282_Sub47 class282_sub47 = ((Class282_Sub47) (client.aClass465_7208.method7754((long) (651363737 * ((Class282_Sub44_Sub1) this).anInt9460))));
		if (null != class282_sub47) {
			Class1.method252(Class397.aClass397_4799, anInt8063 * 587626901, -1, ((Class521_Sub1_Sub1_Sub2) class282_sub47.anObject8068), (((Class282_Sub44_Sub1) this).anInt9460 * 651363737), 2031707814);
			return true;
		}
		return false;
	}

	public Class282_Sub44_Sub1(int i, int i_0_, int i_1_) {
		super(i, i_0_);
		((Class282_Sub44_Sub1) this).anInt9460 = i_1_ * -412057943;
	}
}
